<?php
require_once dirname(__file__) . '/QR/GenerateQrCode.php';
require_once dirname(__file__) . '/QR/Tag.php';
use Salla\ZATCA\GenerateQrCode;
use Salla\ZATCA\Tag;
class QR
{
    public static function get_base($seller,$seller_tax,$date,$total_amount,$tax_amount)
    {

        $generatedString = GenerateQrCode::fromArray(array(
            new Tag(1,$seller),
            new Tag(2,$seller_tax),
            new Tag(3,$date), //'2021-07-12T14:25:09Z'
            new Tag(4,$total_amount),
            new Tag(5,$tax_amount)
        ))->toBase64();
        return '<img height="160" src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=' .$generatedString. '&choe=UTF-8" >';

    }
}

?>