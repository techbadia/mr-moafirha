<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_serv_action_types extends CI_Model {
    
	public function GetRow($id) {
        $query = $this->db->query("select * from serv_action_types where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function Get_all() {
        $query = $this->db->query("select * from serv_action_types order by id asc");
        return $query->result();
    }

    public function GetMultiRow() {
        $query = $this->db->query("select * from serv_action_types order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('serv_action_types', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('serv_action_types', $data); 
    }
}

?>