<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_serv_notifications extends CI_Model {
    
	public function GetBeforeDueDate() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $TodayDate = date("Y-m-d");
        $query = $this->db->query("select * from serv_notifications where user_id =1 AND due_date >='$TodayDate' AND done=0 order by id desc");
        return $query->result();
    }

    public function GetMultiRow() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from serv_notifications where user_id =$StaffID order by id asc");
        return $query->result();
    }

}

?>