<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_serv_events extends CI_Model {
    
	public function GetRow($id) {
        $query = $this->db->query("select * from serv_events where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from serv_events where company_id=$company_id AND deleted=0 order by id asc");
        return $query->result();
    }

    public function Get_all() {
        $company_id = intval($this->session->userdata('company_id'));
        $TodayDate = date("Y-m-d");
        $SQL = "select * from serv_events where company_id=$company_id ";
        $SQL .= "AND start_date <='$TodayDate' AND end_date >='$TodayDate' AND deleted=0 order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function Get_old() {
        $company_id = intval($this->session->userdata('company_id'));
        $TodayDate = date("Y-m-d");
        $SQL = "select * from serv_events where company_id=$company_id ";
        $SQL .= "AND end_date <'$TodayDate' AND deleted=0 order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function Get_current() {
        $company_id = intval($this->session->userdata('company_id'));
        $TodayDate = date("Y-m-d");
        $SQL = "select * from serv_events where company_id=$company_id ";
        $SQL .= "AND start_date <='$TodayDate' AND end_date >='$TodayDate' AND deleted=0 order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function Get_coming() {
        $company_id = intval($this->session->userdata('company_id'));
        $TodayDate = date("Y-m-d");
        $SQL = "select * from serv_events where company_id=$company_id ";
        $SQL .= "AND start_date >'$TodayDate' AND deleted=0 order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetDeleted() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from serv_events where company_id=$company_id AND deleted=1 order by id asc");
        return $query->result();
    }

    
    public function InsertRecord($data) {
        $this->db->insert('serv_events', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('serv_events', $data); 
    }

    public function GetLatest($start_date, $end_date) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from serv_events where company_id=$company_id AND start_date='$start_date' ";
        $SQL .= "AND end_date='$end_date' order by id desc";
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }
}

?>