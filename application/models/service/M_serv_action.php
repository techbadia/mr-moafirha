<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_serv_action extends CI_Model {
    
	public function GetRow($id) {
        $query = $this->db->query("select * from serv_action where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from serv_action where company_id=$company_id order by id asc");
        return $query->result();
    }

    public function GetDeleted() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from serv_action where company_id=$company_id AND deleted=1 order by id asc");
        return $query->result();
    }

////////////////////
    public function Get_done() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from serv_action where company_id=$company_id AND done=1 order by id asc");
        return $query->result();
    }

    public function Get_onhold() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from serv_action where company_id=$company_id AND done=0 order by id asc");
        return $query->result();
    }

    public function Get_expired_onhold() {
        $company_id = intval($this->session->userdata('company_id'));
        $TodayDate = date("Y-m-d");
        $query = $this->db->query("select * from serv_action where company_id=$company_id AND done=0 AND end_date<'$TodayDate' order by id asc");
        return $query->result();
    }

    public function Get_expired_done() {
        $company_id = intval($this->session->userdata('company_id'));
        $TodayDate = date("Y-m-d");
        $query = $this->db->query("select * from serv_action where company_id=$company_id AND done=1 AND end_date<'$TodayDate' order by id asc");
        return $query->result();
    }

    public function Get_coming() {
        $company_id = intval($this->session->userdata('company_id'));
        $TodayDate = date("Y-m-d");
        $query = $this->db->query("select * from serv_action where company_id=$company_id AND done=1 AND start_date >'$TodayDate' order by id asc");
        return $query->result();
    }

    public function Get_all() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from serv_action where company_id=$company_id order by id asc");
        return $query->result();
    }

    public function Get_Mine() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from serv_action where to_user=$StaffID AND done=0 order by id asc");
        return $query->result();
    }

    public function GetToUser() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from serv_action where to_user=$StaffID order by id asc");
        return $query->result();
    }

    public function GetFromUser() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from serv_action where from_user=$StaffID order by id asc");
        return $query->result();
    }

    public function GetByType($type_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from serv_action where company_id=$company_id AND type_id=$type_id order by id asc");
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('serv_action', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('serv_action', $data); 
    }

    public function PendingActions()
    {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from serv_action where to_user=$StaffID AND done=0");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

    public function GetCountByProject($project_id)
    {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from serv_action where project_id=$project_id");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
}

?>