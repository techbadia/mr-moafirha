<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_serv_events_users extends CI_Model {
    
	public function GetRow($id) {
        $query = $this->db->query("select * from serv_events_users where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function Get_all() {
        $query = $this->db->query("select * from serv_events_users order by id asc");
        return $query->result();
    }

    public function Get_Count($event_id) {
        $query = $this->db->query("select * from serv_events_users where event_id=$event_id order by id asc");
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

    public function Get_mine() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from serv_events_users where user_id=$StaffID order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('serv_events_users', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('serv_events_users', $data); 
    }

    public function CountMyEvents($event_id, $user_id)
    {
        $query = $this->db->query("select * from serv_events_users where event_id=$event_id AND user_id=$user_id");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
}

?>