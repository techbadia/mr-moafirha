<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_serv_message extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from serv_message where id=".$id);
        $row = $query->row();
        return $row;
    }

	public function GetInbox() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from serv_message where to_user=$StaffID AND draft=0 AND deleted=0 order by id desc");
        return $query->result();
    }

    public function GetInboxUnReaded() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from serv_message where to_user=$StaffID AND readed=0 order by id desc");
        return $query->result();
    }
	
    public function GetOutBox() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from serv_message where from_user=$StaffID AND deleted=0 order by id desc");
        return $query->result();
    }

    public function GetMarked() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from serv_message where to_user=$StaffID AND marked=1 AND deleted=0 order by id desc");
        return $query->result();
    }

    public function GetDraft() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from serv_message where from_user=$StaffID AND draft=1 AND deleted=0 order by id desc");
        return $query->result();
    }

    public function GetDeleted() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $SQL = "select * from serv_message where from_user=$StaffID AND deleted=1 ";
        $SQL .= "OR to_user=$StaffID AND deleted=1 order by id desc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

	public function CountUnReadedMyMessage($to_user)
    {
        $query = $this->db->query("select id, to_user from serv_message where readed=0 AND to_user=$to_user");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

    public function InsertRecord($data) {
        $this->db->insert('serv_message', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('serv_message', $data); 
    }

    public function GetJSON($id) {
        $SQL = "SELECT serv_message.id,usr_users.fullname, serv_message.subject, serv_message.message, serv_message.thedate, serv_message.image ";
        $SQL .= "FROM usr_users INNER JOIN serv_message ON (usr_users.id = serv_message.from_user) WHERE serv_message.id =".$id;
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }
}

?>