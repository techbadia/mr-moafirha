<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sale_returns_items extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from sale_returns_items where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow($bill_id) {
        $query = $this->db->query("select * from sale_returns_items where bill_id=$bill_id order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('sale_returns_items', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('sale_returns_items', $data); 
    }
    
    public function GetTotalQuantity($product_id, $fromdate, $todate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "SELECT SUM(sale_returns_items.quantity) AS quantity FROM sale_returns INNER JOIN ";
        $SQL .= "sale_returns_items ON (sale_returns.id = sale_returns_items.bill_id) WHERE sale_returns.thedate ";
        $SQL .= "BETWEEN '$fromdate' AND '$todate' AND sale_returns.company_id = $company_id AND sale_returns_items.store_type_id = $product_id";
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }
    
}

?>