<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sale_customer extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from sale_customer where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetRowByName($ar_title) {
        $query = $this->db->query("select * from sale_customer where ar_title='$ar_title'");
        $row = $query->row();
        return $row;
    }
    
    public function GetOnlyOne($ID) {
        $company_id = intval($this->session->userdata('company_id'));
        $QueryString = "CALL sale_customer_GetOnlyOne(?)";
        $data = array(
            'id' => $ID,
            'company_id' => $company_id
        );
        $query = $this->db->query($QueryString, $data);
        $res = $query->result();
        //add this two line 
        $query->next_result(); 
        $query->free_result(); 
        //end of new code

        if ($query) {
            return $res;
        }
        return NULL;
    }
    
    public function GetEmptyRows() {
        //$company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select id, company_id, ar_title, en_title, phone, mobile1, mobile2, address, 
        email, commercial_register, tax_card, person, person_phone, credit_limit, active from 
        sale_customer where id=0");
        return $query->result();
    }
    
         public function GetMultiRow() {
        $query = $this->db->query("select sale_customer.*,fin_treeaccount.account_no,fin_treeaccount.title from sale_customer JOIN fin_treeaccount on (fin_treeaccount.id =sale_customer.id)");
        return $query->result();
    }
    
    public function GetSearch($Text) {
        $query = $this->db->query("select id, company_id, ar_title, en_title, phone, mobile1, mobile2, address, 
        email, commercial_register, tax_card, person, person_phone, credit_limit, active from sale_customer 
        where ar_title like '%".$Text."%' OR "
        . " en_title like '%".$Text."%' OR "
        . " mobile1 like '%".$Text."%' OR "
        . " mobile2 like '%".$Text."%' OR "
        . " address like '%".$Text."%' OR "
        . " email like '%".$Text."%' OR "
        . " person like '%".$Text."%' OR "
        . " person_phone like '%".$Text."%'");
        return $query->result();
    }
    
    public function GetMultiRowCount()
    {
        $query = $this->db->query("select id, company_id, ar_title, en_title from sale_customer");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function GetMultiRowPage($start=0, $limit=100) {
        $query = $this->db->query("select id, company_id, ar_title, en_title, phone, mobile1, mobile2, address, 
        email, commercial_register, tax_card, person, person_phone, credit_limit, active from sale_customer 
         order by ar_title asc limit ".intval($start).", ".$limit);
        return $query->result();
    }
    
    public function GetCustomerByBranche($OrderBy) {
        $query = $this->db->query("select id, company_id, ar_title, en_title from sale_customer "
        . " order by ".$OrderBy." asc");
        return $query->result();
    }
    
    public function InsertRecord($id, $company_id, $user_id, $ar_title, $en_title, $phone, $mobile1, $mobile2, $address, 
    $email, $commercial_register, $tax_card, $person, $person_phone, $credit_limit, $active, $deleted) {
        $company_id = intval($this->session->userdata('company_id'));
        $user_id = intval($this->session->userdata('StaffID'));
        $data = array(
            'id' => $id ,
            'company_id' => $company_id,
            'user_id' => $user_id,
            'ar_title' => $ar_title ,
            'en_title' => $en_title ,
            'phone' => $phone ,
            'mobile1' => $mobile1 ,
            'mobile2' => $mobile2 ,
            'address' => $address ,
            'email' => $email,
            'commercial_register' => $commercial_register,
            'tax_card' => $tax_card,
            'person' => $person,
            'person_phone' => $person_phone,
            'credit_limit' => $credit_limit,
            'active' => $active,
            'deleted' => $deleted
       );
    
        $this->db->insert('sale_customer', $data); 
    }
    
    public function UpdateRecord($id, $ar_title, $en_title, $phone, $mobile1, $mobile2, $address, 
    $email, $commercial_register, $tax_card, $person, $person_phone, $credit_limit, $active) {
        $company_id = intval($this->session->userdata('company_id'));
        $user_id = intval($this->session->userdata('StaffID'));
       $data = array(
            'id' => $id ,
            'ar_title' => $ar_title ,
            'en_title' => $en_title ,
            'phone' => $phone ,
            'mobile1' => $mobile1 ,
            'mobile2' => $mobile2 ,
            'address' => $address ,
            'email' => $email,
            'commercial_register' => $commercial_register,
            'tax_card' => $tax_card,
            'person' => $person,
            'person_phone' => $person_phone,
            'credit_limit' => $credit_limit,
            'active' => $active
        );
            
        $this->db->where('id', $id);
        $this->db->update('sale_customer', $data); 
    }
    
    public function CheckDuplicate($ar_title, $en_title, $phone)
    {
        $query = $this->db->query("select id, company_id, ar_title, en_title, phone from sale_customer where 
        ar_title='".$ar_title."' AND en_title='".$en_title."' AND phone='".$phone."'");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function CheckTitle($ar_title)
    {
        $query = $this->db->query("select id, company_id, ar_title from sale_customer where 
        ar_title='".$ar_title."'");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function GetByTitle($ar_title) {
        $query = $this->db->query("select id, company_id, ar_title from sale_customer where "
        . "ar_title='".$ar_title."'");
        $row = $query->row();
        return $row;
    }
    
    public function CheckID($id)
    {
        $query = $this->db->query("select id, company_id from sale_customer where 
        id=".$id);
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

    public function UpdateDelete($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('sale_customer', $data); 
    }
    
    public function CheckPhone($phone)
    {
        $query = $this->db->query("select id, ar_title, en_title, phone, address from sale_customer where 
        phone='".$phone."'");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

    public function GetCustomerData($phone) {
        $query = $this->db->query("select * from sale_customer where phone='".$phone."'");
        $row = $query->row();
        return $row;
    }

}

?>