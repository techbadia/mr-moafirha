<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sale_returns extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from sale_returns where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetLatestRecord($company_id, $customer_id, $user_id)
    {
        $query = $this->db->query("select * from sale_returns where 
        company_id=$company_id AND customer_id=$customer_id AND user_id=$user_id order by id desc");
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sale_returns where company_id=$company_id order by id asc");
        return $query->result();
    }
    
    public function GetReport($customer_id, $fromdate, $todate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from sale_returns where company_id=$company_id ";
        if($customer_id > 0)
        {
            $SQL .= "AND customer_id=$customer_id ";
        }
        $SQL .= "AND thedate between '$fromdate' AND '$todate'";
        $SQL .= " order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetDeleted() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sale_returns where company_id=$company_id AND deleted=1 order by id asc");
        return $query->result();
    }

    public function GetFullPaid() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sale_returns where company_id=$company_id AND paid=totalvalue order by id asc");
        return $query->result();
    }

    public function GetPartPaid() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sale_returns where company_id=$company_id AND paid < totalvalue order by id asc");
        return $query->result();
    }

    public function GetNotPaid() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sale_returns where company_id=$company_id AND paid=0 order by id asc");
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('sale_returns', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('sale_returns', $data); 
    }

    public function GetInvoiceHeader($id) {
        $SQL = "SELECT sale_returns.id, sale_returns.company_id, sale_returns.customer_id, fin_treeaccount.title AS 
        customer_title, fin_treeaccount.title_en AS customer_title_en, sale_returns.user_id, 
        sale_returns.thedate, sale_returns.totalvalue, sale_returns.paid, sale_returns.remaining, sale_returns.discount, 
        sale_returns.notes, sale_returns.image, sale_returns.tree_id, fin_treeaccount1.title AS account_title, 
        fin_treeaccount1.title_en AS account_title_en FROM fin_treeaccount INNER JOIN sale_returns ON 
        (fin_treeaccount.id = sale_returns.customer_id) INNER JOIN fin_treeaccount fin_treeaccount1 ON 
        (sale_returns.tree_id = fin_treeaccount1.id) WHERE sale_returns.id =".$id;
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }

    public function GetInvoiceDetails($id) {
        $SQL = "SELECT sale_returns_items.id, sale_returns_items.bill_id, sale_returns_items.store_type_id, 
        product.title, product.title_en, sale_returns_items.quantity, sale_returns_items.unitprice, 
        (sale_returns_items.quantity * sale_returns_items.unitprice) AS subtotal FROM product INNER JOIN 
        sale_returns_items ON (product.id = sale_returns_items.store_type_id) WHERE sale_returns_items.bill_id =".$id;
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
}

?>