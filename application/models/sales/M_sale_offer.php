<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sale_offer extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from sale_offer where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetLatestRecord($company_id, $customer_id, $user_id)
    {
        $query = $this->db->query("select * from sale_offer where 
        company_id=$company_id AND customer_id=$customer_id AND user_id=$user_id order by id desc");
        $row = $query->row();
        return $row;
    }

    public function GetUnDeleted() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sale_offer where company_id=$company_id AND deleted=0 order by id asc");
        return $query->result();
    }

    public function GetDeleted() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sale_offer where company_id=$company_id AND deleted=1 order by id asc");
        return $query->result();
    }
    
    public function GetNotExpired() {
        $company_id = intval($this->session->userdata('company_id'));
        $Today = date("Y-m-d");
        $SQL = "select * from sale_offer where company_id=$company_id AND expired_date >='$Today'";
        $SQL .= " order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetExpired() {
        $company_id = intval($this->session->userdata('company_id'));
        $Today = date("Y-m-d");
        $SQL = "select * from sale_offer where company_id=$company_id AND expired_date <'$Today'";
        $SQL .= " order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetByProject($project_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from sale_offer where company_id=$company_id ";
        $SQL .= "AND project_id=$project_id order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('sale_offer', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('sale_offer', $data); 
    }

    public function GetInvoiceHeader($id) {
        $SQL = "SELECT sale_offer.id, sale_offer.company_id, sale_offer.customer_id, sale_customer.ar_title AS 
        supplier_title, sale_customer.en_title AS supplier_title_en, sale_offer.user_id, 
        sale_offer.thedate, sale_offer.totalvalue, sale_offer.paid, sale_offer.remaining, sale_offer.discount, 
        sale_offer.over_cost, sale_offer.tax, sale_offer.notes, sale_offer.image, sale_offer.tree_id FROM 
        sale_customer INNER JOIN sale_offer ON 
        (sale_customer.id = sale_offer.customer_id) WHERE sale_offer.id =".$id;
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }

    public function GetInvoiceDetails($id) {
        $SQL = "SELECT sale_offer_items.id, sale_offer_items.bill_id, sale_offer_items.store_type_id, 
        product.title, product.title_en, sale_offer_items.quantity, sale_offer_items.unitprice, 
        (sale_offer_items.quantity * sale_offer_items.unitprice) AS subtotal FROM product INNER JOIN 
        sale_offer_items ON (product.id = sale_offer_items.store_type_id) WHERE sale_offer_items.bill_id =".$id;
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
}

?>