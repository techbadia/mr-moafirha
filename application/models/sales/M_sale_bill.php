<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sale_bill extends CI_Model {

    public function GetRow($id) {
        $query = $this->db->query("select * from sale_bill where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetLatestRecord($company_id, $customer_id, $user_id)
    {
        $query = $this->db->query("select * from sale_bill where
        company_id=$company_id AND customer_id=$customer_id AND user_id=$user_id order by id desc");
        $row = $query->row();
        return $row;
    }

	public function CheckLatestRecord($company_id, $customer_id, $user_id)
    {
        $query = $this->db->query("select * from sale_bill where
        company_id=$company_id AND customer_id=$customer_id AND user_id=$user_id order by id desc");
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

	public function GetBillCounts()
    {
        $SQL = "select * from sale_bill";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function GetLatestBill()
    {
        $query = $this->db->query("select * from sale_bill order by id desc");
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sale_bill where company_id=$company_id order by id asc");
        return $query->result();
    }

    public function GetToday() {
        $company_id = intval($this->session->userdata('company_id'));
        $Today = date("Y-m-d");
        $query = $this->db->query("select * from sale_bill where company_id=$company_id AND thedate='$Today' order by id asc");
        return $query->result();
    }

    public function GetReport($customer_id, $fromdate, $todate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from sale_bill where company_id=$company_id ";
        if($customer_id > 0)
        {
            $SQL .= "AND customer_id=$customer_id ";
        }
        $SQL .= "AND thedate between '$fromdate' AND '$todate'";
        $SQL .= " order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetByUser($user_id, $fromdate, $todate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from sale_bill where company_id=$company_id ";
        if($user_id > 0)
        {
            $SQL .= "AND user_id=$user_id ";
        }
        $SQL .= "AND thedate between '$fromdate' AND '$todate'";
        $SQL .= " order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetBySalesPerson($salesperson_id, $fromdate, $todate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from sale_bill where company_id=$company_id ";
        if($salesperson_id > 0)
        {
            $SQL .= "AND salesperson_id=$salesperson_id ";
        }
        $SQL .= "AND thedate between '$fromdate' AND '$todate'";
        $SQL .= " order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetDeleted() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sale_bill where company_id=$company_id AND deleted=1 order by id asc");
        return $query->result();
    }

    public function GetFullPaid() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sale_bill where company_id=$company_id AND paid=totalvalue order by id asc");
        return $query->result();
    }

    public function GetPartPaid() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sale_bill where company_id=$company_id AND paid < totalvalue order by id asc");
        return $query->result();
    }

    public function GetNotPaid() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sale_bill where company_id=$company_id AND paid=0 order by id asc");
        return $query->result();
    }

    public function GetByProject($project_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from sale_bill where company_id=$company_id ";
        $SQL .= "AND project_id=$project_id order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('sale_bill', $data);
        return  $this->db->insert_id();
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('sale_bill', $data);
    }

    public function GetInvoiceHeader($id) {
        // $SQL = "SELECT sale_bill.id, sale_bill.company_id, sale_bill.customer_id, sale_bill.user_id,
        // sale_bill.thedate, sale_bill.totalvalue, sale_bill.paid, sale_bill.remaining, sale_bill.discount, sale_bill.tax,
        // sale_bill.notes, sale_bill.image WHERE sale_bill.id =".$id;
        $this->db->where("id",$id);
        $query = $this->db->get("sale_bill");
        $row = $query->row();
        return $row;
    }
    // public function GetInvoiceHeader($id) {
    //     $SQL = "SELECT sale_bill.id, sale_bill.company_id, sale_bill.customer_id, fin_treeaccount.title AS
    //     customer_title, fin_treeaccount.title_en AS customer_title_en, sale_bill.user_id,
    //     sale_bill.thedate, sale_bill.totalvalue, sale_bill.paid, sale_bill.remaining, sale_bill.discount, sale_bill.tax,
    //     sale_bill.notes, sale_bill.image, sale_bill.tree_id1, fin_treeaccount1.title AS account_title,
    //     fin_treeaccount1.title_en AS account_title_en FROM fin_treeaccount INNER JOIN sale_bill ON
    //     (fin_treeaccount.id = sale_bill.customer_id) INNER JOIN fin_treeaccount  ON
    //     (sale_bill.tree_id = fin_treeaccount.id) WHERE sale_bill.id =".$id;
    //     $query = $this->db->query($SQL);
    //     $row = $query->row();
    //     return $row;
    // }

    // public function GetInvoiceDetails($id) {
    //     $SQL = "SELECT sale_bill_items.id, sale_bill_items.bill_id, sale_bill_items.store_type_id,
    //     product.title, product.title_en, sale_bill_items.quantity, sale_bill_items.unitprice,
    //     (sale_bill_items.quantity * sale_bill_items.unitprice) AS subtotal FROM product INNER JOIN
    //     sale_bill_items ON (product.id = sale_bill_items.store_type_id) WHERE sale_bill_items.bill_id =".$id;
    //     $query = $this->db->query($SQL);
    //     return $query->result();
    // }
    public function GetInvoiceDetails($id) {
        // $SQL = "SELECT sale_bill_items.id, sale_bill_items.bill_id, sale_bill_items.store_type_id,
        // product.title, product.title_en, sale_bill_items.quantity, sale_bill_items.unitprice,
        // (sale_bill_items.quantity * sale_bill_items.unitprice) AS subtotal FROM product INNER JOIN
        // sale_bill_items ON (product.id = sale_bill_items.store_type_id) WHERE sale_bill_items.bill_id =".$id;
        // $query = $this->db->query($SQL);
        $this->db->where("bill_id",$id);
        $query = $this->db->get("sale_bill_items");
        return $query->result();
    }

    public function GetItemsPrice($product_id, $customer_id, $fromdate, $todate)
    {
        $SQL = "SELECT product.id AS product_id, product.title AS product_ar_title, product.title_en AS product_en_title, ";
        $SQL .= "sale_bill_items.unitprice, sale_customer.id AS customer_id, sale_customer.ar_title, ";
        $SQL .= "sale_customer.en_title, sale_customer.phone, sale_bill_items.id AS bill_id, sale_bill.thedate ";
        $SQL .= ", sale_bill.totalvalue FROM sale_bill INNER JOIN sale_bill_items ON (sale_bill.id = sale_bill_items.bill_id) INNER JOIN product ON ";
        $SQL .= "(sale_bill_items.store_type_id = product.id) INNER JOIN sale_customer ON (sale_bill.customer_id = sale_customer.id) ";
        $SQL .= "WHERE sale_bill.thedate BETWEEN '$fromdate' AND '$todate' ";
        if($product_id > 0)
        {
            $SQL .= "AND product.id =$product_id ";
        }
        if($customer_id > 0)
        {
            $SQL .= "AND sale_customer.id=$customer_id ";
        }
        $SQL .= " GROUP BY product_id, customer_id";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetNotDelivered() {
        $SQL = "SELECT sale_bill.id, sale_bill.company_id, sale_bill.customer_id, sale_bill.customer_name, ";
        $SQL .= "sale_bill.customer_phone, sale_bill.project_id, sale_bill.salesperson_id, sale_bill.user_id, ";
        $SQL .= "sale_bill.delivery_id, sale_bill.thedate, sale_bill.expired_date, sale_bill.totalvalue, ";
        $SQL .= "sale_bill.paid, sale_bill.remaining, sale_bill.discount, sale_bill.tax, sale_bill.notes, ";
        $SQL .= "sale_bill.image, sale_bill.tree_id, sale_bill.tree_id1, sale_bill.paid1, sale_bill.paid2, ";
        $SQL .= "sale_bill.paid3, sale_bill.pos_store_id, sale_bill.deleted, sale_bill_items.store_type_id, ";
        $SQL .= "sale_bill_items.location_id, sale_bill_items.quantity, sale_bill_items.received FROM sale_bill ";
        $SQL .= "INNER JOIN sale_bill_items ON (sale_bill.id = sale_bill_items.bill_id) WHERE ";
        $SQL .= "sale_bill_items.quantity > sale_bill_items.received";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetInvoiceByCustomer($customer_id) {
        $query = $this->db->query("select * from sale_bill where customer_id=$customer_id order by id asc");
        return $query->result();
    }

    public function GetNotFullPaidInvoice($customer_id) {
        $query = $this->db->query("select * from sale_bill where customer_id=$customer_id AND remaining>0 order by id asc");
        return $query->result();
    }

}

?>
