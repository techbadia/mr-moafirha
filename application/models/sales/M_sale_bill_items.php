<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sale_bill_items extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from sale_bill_items where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow($bill_id) {
        $query = $this->db->query("select * from sale_bill_items where bill_id=$bill_id order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('sale_bill_items', $data); 
        return  $this->db->insert_id();
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('sale_bill_items', $data); 
    }
    
    public function GetTotalQuantity($product_id, $fromdate, $todate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "SELECT SUM(sale_bill_items.quantity) AS quantity FROM sale_bill INNER JOIN ";
        $SQL .= "sale_bill_items ON (sale_bill.id = sale_bill_items.bill_id) WHERE sale_bill.thedate ";
        $SQL .= "BETWEEN '$fromdate' AND '$todate' AND sale_bill.company_id = $company_id AND sale_bill_items.store_type_id = $product_id";
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }

    public function GetItems($BillID) {
        $query = $this->db->query("select * from sale_bill_items WHERE bill_id=".$BillID." ORDER BY id");
        return $query->result();
    }
    
}

?>