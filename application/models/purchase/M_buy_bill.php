<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_buy_bill extends CI_Model {

    public function GetRow($id) {
        $query = $this->db->query("select * from buy_bill where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetLatestRecord($company_id, $supplier_id, $user_id)
    {
        $query = $this->db->query("select * from buy_bill where
        company_id=$company_id AND supplier_id=$supplier_id AND user_id=$user_id order by id desc");
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from buy_bill where company_id=$company_id order by id asc");
        return $query->result();
    }
    

    public function GetReport($supplier_id, $fromdate, $todate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from buy_bill where company_id=$company_id ";
        if($supplier_id > 0)
        {
            $SQL .= "AND supplier_id=$supplier_id ";
        }
        $SQL .= "AND thedate between '$fromdate' AND '$todate'";
        $SQL .= " order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetDeleted() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from buy_bill where company_id=$company_id AND deleted=1 order by id asc");
        return $query->result();
    }

    public function GetFullPaid() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from buy_bill where company_id=$company_id AND paid=totalvalue order by id asc");
        return $query->result();
    }

    public function GetPartPaid() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from buy_bill where company_id=$company_id AND paid < totalvalue order by id asc");
        return $query->result();
    }

    public function GetNotPaid() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from buy_bill where company_id=$company_id AND paid=0 order by id asc");
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('buy_bill', $data);
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('buy_bill', $data);
    }

    public function GetInvoiceHeader($id) {
        $SQL = "SELECT buy_bill.id, buy_bill.company_id, buy_bill.supplier_id, buy_manufacturer.title AS
        supplier_title, buy_manufacturer.title_en AS supplier_title_en, buy_bill.user_id,
        buy_bill.thedate, buy_bill.totalvalue, buy_bill.paid, buy_bill.remaining, buy_bill.discount, buy_bill.tax,
        buy_bill.notes, buy_bill.image, buy_bill.tree_id, fin_treeaccount.title AS account_title,
        fin_treeaccount.title_en AS account_title_en FROM buy_manufacturer INNER JOIN buy_bill ON
        (buy_manufacturer.id = buy_bill.supplier_id) INNER JOIN fin_treeaccount ON
        (buy_bill.tree_id = fin_treeaccount.id) WHERE buy_bill.id =".$id;
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }

    public function GetInvoiceDetails($id) {
        $SQL = "SELECT buy_bill_items.id, buy_bill_items.bill_id, buy_bill_items.store_type_id,
        product.title, product.title_en, buy_bill_items.quantity, buy_bill_items.unitprice,
        (buy_bill_items.quantity * buy_bill_items.unitprice) AS subtotal FROM product INNER JOIN
        buy_bill_items ON (product.id = buy_bill_items.store_type_id) WHERE buy_bill_items.bill_id =".$id;
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetItemsPrice($product_id, $supplier_id, $fromdate, $todate)
    {
        $SQL = "SELECT product.id AS product_id, product.title AS product_ar_title, product.title_en AS product_en_title, ";
        $SQL .= "buy_bill_items.unitprice, buy_manufacturer.id AS supplier_id, buy_manufacturer.title AS supplier_ar_title, ";
        $SQL .= "buy_manufacturer.title_en AS supplier_en_title, buy_manufacturer.phone, buy_bill_items.id AS bill_id, buy_bill.thedate ";
        $SQL .= ", buy_bill.totalvalue FROM buy_bill INNER JOIN buy_bill_items ON (buy_bill.id = buy_bill_items.bill_id) INNER JOIN product ON ";
        $SQL .= "(buy_bill_items.store_type_id = product.id) INNER JOIN buy_manufacturer ON (buy_bill.supplier_id = buy_manufacturer.id) ";
        $SQL .= "WHERE buy_bill.thedate BETWEEN '$fromdate' AND '$todate' ";
        if($product_id > 0)
        {
            $SQL .= "AND product.id =$product_id ";
        }
        if($supplier_id > 0)
        {
            $SQL .= "AND buy_manufacturer.id=$supplier_id ";
        }
        $SQL .= " GROUP BY product_id, supplier_id";
        $query = $this->db->query($SQL);
        return $query->result();
    }

}

?>
