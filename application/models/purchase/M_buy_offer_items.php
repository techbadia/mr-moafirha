<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_buy_offer_items extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from buy_offer_items where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow($bill_id) {
        $query = $this->db->query("select * from buy_offer_items where bill_id=$bill_id order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('buy_offer_items', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('buy_offer_items', $data); 
    }
    
}

?>