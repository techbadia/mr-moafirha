<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_buy_bill_items extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from buy_bill_items where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow($bill_id) {
        $query = $this->db->query("select * from buy_bill_items where bill_id=$bill_id order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('buy_bill_items', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('buy_bill_items', $data); 
    }

    public function GetTotalQuantity($product_id, $fromdate, $todate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "SELECT SUM(buy_bill_items.quantity) AS quantity FROM buy_bill INNER JOIN ";
        $SQL .= "buy_bill_items ON (buy_bill.id = buy_bill_items.bill_id) WHERE buy_bill.thedate ";
        $SQL .= "BETWEEN '$fromdate' AND '$todate' AND buy_bill.company_id = $company_id AND buy_bill_items.store_type_id = $product_id";
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }
    
}

?>