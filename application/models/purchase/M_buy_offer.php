<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_buy_offer extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from buy_offer where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetLatestRecord($company_id, $supplier_id, $user_id)
    {
        $query = $this->db->query("select * from buy_offer where 
        company_id=$company_id AND supplier_id=$supplier_id AND user_id=$user_id order by id desc");
        $row = $query->row();
        return $row;
    }

    public function GetUnDeleted() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from buy_offer where company_id=$company_id AND deleted=0 order by id asc");
        return $query->result();
    }

    public function GetDeleted() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from buy_offer where company_id=$company_id AND deleted=1 order by id asc");
        return $query->result();
    }
    
    public function GetNotExpired() {
        $company_id = intval($this->session->userdata('company_id'));
        $Today = date("Y-m-d");
        $SQL = "select * from buy_offer where company_id=$company_id AND expired_date >='$Today'";
        $SQL .= " order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetExpired() {
        $company_id = intval($this->session->userdata('company_id'));
        $Today = date("Y-m-d");
        $SQL = "select * from buy_offer where company_id=$company_id AND expired_date <'$Today'";
        $SQL .= " order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('buy_offer', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('buy_offer', $data); 
    }

    public function GetInvoiceHeader($id) {
        $SQL = "SELECT buy_offer.id, buy_offer.company_id, buy_offer.supplier_id, buy_manufacturer.title AS 
        supplier_title, buy_manufacturer.title_en AS supplier_title_en, buy_offer.user_id, 
        buy_offer.thedate, buy_offer.totalvalue, buy_offer.paid, buy_offer.remaining, buy_offer.discount, 
        buy_offer.over_cost,  buy_offer.tax, buy_offer.notes, buy_offer.image, buy_offer.tree_id FROM 
        buy_manufacturer INNER JOIN buy_offer ON 
        (buy_manufacturer.id = buy_offer.supplier_id) WHERE buy_offer.id =".$id;
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }

    public function GetInvoiceDetails($id) {
        $SQL = "SELECT buy_offer_items.id, buy_offer_items.bill_id, buy_offer_items.store_type_id, 
        product.title, product.title_en, buy_offer_items.quantity, buy_offer_items.unitprice, 
        (buy_offer_items.quantity * buy_offer_items.unitprice) AS subtotal FROM product INNER JOIN 
        buy_offer_items ON (product.id = buy_offer_items.store_type_id) WHERE buy_offer_items.bill_id =".$id;
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
}

?>