<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_buy_manufacturer extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from buy_manufacturer where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        //$company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from buy_manufacturer order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('buy_manufacturer', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('buy_manufacturer', $data); 
    }

    public function CheckDuplicate($title, $title_en)
    {
        //$company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from buy_manufacturer where title='$title' 
        AND title_en='$title_en' order by id asc";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }
    
}

?>