<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_buy_returns extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from buy_returns where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetLatestRecord($company_id, $supplier_id, $user_id)
    {
        $query = $this->db->query("select * from buy_returns where 
        company_id=$company_id AND supplier_id=$supplier_id AND user_id=$user_id order by id desc");
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from buy_returns where company_id=$company_id order by id asc");
        return $query->result();
    }

    public function GetReport($supplier_id, $fromdate, $todate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from buy_returns where company_id=$company_id ";
        if($supplier_id > 0)
        {
            $SQL .= "AND supplier_id=$supplier_id ";
        }
        $SQL .= "AND thedate between '$fromdate' AND '$todate'";
        $SQL .= " order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function GetDeleted() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from buy_returns where company_id=$company_id AND deleted=1 order by id asc");
        return $query->result();
    }

    public function GetFullPaid() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from buy_returns where company_id=$company_id AND paid=totalvalue order by id asc");
        return $query->result();
    }

    public function GetPartPaid() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from buy_returns where company_id=$company_id AND paid < totalvalue order by id asc");
        return $query->result();
    }

    public function GetNotPaid() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from buy_returns where company_id=$company_id AND paid=0 order by id asc");
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('buy_returns', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('buy_returns', $data); 
    }

    public function GetInvoiceHeader($id) {
        $SQL = "SELECT buy_returns.id, buy_returns.company_id, buy_returns.supplier_id, buy_manufacturer.title AS 
        supplier_title, buy_manufacturer.title_en AS supplier_title_en, buy_returns.user_id, 
        buy_returns.thedate, buy_returns.totalvalue, buy_returns.paid, buy_returns.remaining, buy_returns.discount, 
        buy_returns.notes, buy_returns.image, buy_returns.tree_id, fin_treeaccount.title AS account_title, 
        fin_treeaccount.title_en AS account_title_en FROM buy_manufacturer INNER JOIN buy_returns ON 
        (buy_manufacturer.id = buy_returns.supplier_id) INNER JOIN fin_treeaccount ON 
        (buy_returns.tree_id = fin_treeaccount.id) WHERE buy_returns.id =".$id;
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }

    public function GetInvoiceDetails($id) {
        $SQL = "SELECT buy_returns_items.id, buy_returns_items.bill_id, buy_returns_items.store_type_id, 
        product.title, product.title_en, buy_returns_items.quantity, buy_returns_items.unitprice, 
        (buy_returns_items.quantity * buy_returns_items.unitprice) AS subtotal FROM product INNER JOIN 
        buy_returns_items ON (product.id = buy_returns_items.store_type_id) WHERE buy_returns_items.bill_id =".$id;
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
}

?>