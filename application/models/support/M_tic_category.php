<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_tic_category extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from tic_category where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetAnothetic_categorys($id) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from tic_category where company_id=$company_id AND id <> $id order by id asc");
        return $query->result();
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from tic_category where company_id=$company_id order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('tic_category', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('tic_category', $data); 
    }

    public function tic_ticket_Count($category_id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from tic_ticket where company_id=$company_id AND category_id=$category_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }
    
}

?>