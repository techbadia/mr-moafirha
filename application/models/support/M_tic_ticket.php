<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_tic_ticket extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from tic_ticket where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetAnothetic_tickets($id) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from tic_ticket where company_id=$company_id AND id <> $id order by id asc");
        return $query->result();
    }
    
    public function GetMultiRow($fromdate, $todate, $category_id, $priority_id, $status_id, $customer_id, $user_id, $assign_to) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from tic_ticket where company_id=$company_id AND thedate between '$fromdate' AND '$todate' ";
        if($category_id > 0)
        {
            $SQL .= "AND category_id=$category_id ";
        }
        if($priority_id > 0)
        {
            $SQL .= "AND priority_id=$priority_id ";
        }
        if($status_id > 0)
        {
            $SQL .= "AND status_id=$status_id ";
        }
        if($customer_id > 0)
        {
            $SQL .= "AND customer_id=$customer_id ";
        }
        if($user_id > 0)
        {
            $SQL .= "AND user_id=$user_id ";
        }
        if($assign_to > 0)
        {
            $SQL .= "AND assign_to=$assign_to ";
        }
        $SQL .= "order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('tic_ticket', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('tic_ticket', $data); 
    }
    
    public function CollectAssignTo() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select assign_to from tic_ticket where company_id=$company_id group by assign_to");
        return $query->result();
    }

    public function CollectAssignTo_Count($assign_to)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from tic_ticket where company_id=$company_id AND assign_to=$assign_to";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

}

?>