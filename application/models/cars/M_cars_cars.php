<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_cars_cars extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from cars_cars where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetLatest($customer_id, $chassis_no, $model_id) {
        $query = $this->db->query("select * from cars_cars where 
        customer_id=$customer_id AND chassis_no='$chassis_no' AND model_id=$model_id");
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from cars_cars where company_id=$company_id order by id asc");
        return $query->result();
    }

    public function GetCustomerCars($customer_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from cars_cars where company_id=$company_id AND customer_id=$customer_id order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('cars_cars', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('cars_cars', $data); 
    }

    public function ModelCount($model_id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from cars_cars where company_id=$company_id AND model_id=$model_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }
    

    
}

?>