<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_cars_cars_images extends CI_Model {

    public function GetImages($car_id) {
        $query = $this->db->query("select * from cars_cars_images where car_id=$car_id order by id asc");
        return $query->result();
    }

    
    public function InsertRecord($data) {
        $this->db->insert('cars_cars_images', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('cars_cars_images', $data); 
    }
    
}

?>