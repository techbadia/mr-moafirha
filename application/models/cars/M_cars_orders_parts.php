<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_cars_orders_parts extends CI_Model {
    
    public function GetRow($ID) {
        $query = $this->db->query("select id, invoice_id, part_name, qunatity, unit_price, image from cars_orders_parts where id=".$ID);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $query = $this->db->query("select id, invoice_id, part_name, qunatity, unit_price, image from cars_orders_parts order by id asc");
        return $query->result();
    }
    
    public function GetByRequest($invoice_id) {
        $query = $this->db->query("select id, invoice_id, part_name, qunatity, unit_price, image from "
        . "cars_orders_parts where invoice_id=".$invoice_id." order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('cars_orders_parts', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('cars_orders_parts', $data); 
    }
    
    
    public function GetItemsCount($invoice_id)
    {
        $query = $this->db->query("select id, invoice_id, part_name, qunatity, unit_price, image from cars_orders_parts 
            where invoice_id=".$invoice_id);
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function GetItems($invoice_id)
    {
        $query = $this->db->query("select id, invoice_id, part_name, qunatity, unit_price, image from cars_orders_parts 
            where invoice_id=".$invoice_id);
        return $query->result();
    }
    
}

?>