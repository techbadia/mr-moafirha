<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_cars_orders extends CI_Model {
    
    public function GetRow($ID) {
        $query = $this->db->query("select * from cars_orders where id=".$ID);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_orders order by id asc";
        }
        else
        {
            $SQL = "select * from cars_orders where company_id=$company_id order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function GetMultiRowByStatus($status_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_orders where status_id=".$status_id." order by id asc";
        }
        else
        {
            $SQL = "select * from cars_orders where company_id=$company_id AND status_id=".$status_id." order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function GetMultiRowByArea($area_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_orders where area_id=".$area_id." order by id asc";
        }
        else
        {
            $SQL = "select * from cars_orders where company_id=$company_id AND area_id=".$area_id." order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function GetMultiRowByCustomer($customer_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_orders where customer_id=".$customer_id." order by id asc";
        }
        else
        {
            $SQL = "select * from cars_orders where company_id=$company_id AND customer_id=".$customer_id." order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetCountByCustomer($customer_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_orders where customer_id=".$customer_id." order by id asc";
        }
        else
        {
            $SQL = "select * from cars_orders where company_id=$company_id AND customer_id=".$customer_id." order by id asc";
        }
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

    public function GetMultiRowByService($service_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "SELECT cars_orders.id, cars_orders.user_id, cars_orders.company_id, cars_orders.customer_id, cars_orders.teamwork_id, "
            . "cars_orders.status_id, cars_orders.area_id, cars_orders.source, cars_orders.details, "
            . "cars_orders.thedate, cars_orders.thetime, cars_orders.insert_date, "
            . "cars_orders.insert_time, cars_orders.deleted FROM cars_orders INNER JOIN cars_orders_items ON "
            . "(cars_orders.id = cars_orders_items.invoice_id) WHERE cars_orders_items.service_id =".$service_id." order by cars_orders.id asc";
        }
        else
        {
            $SQL = "SELECT cars_orders.id, cars_orders.user_id, cars_orders.company_id, cars_orders.customer_id, cars_orders.teamwork_id, "
            . "cars_orders.status_id, cars_orders.area_id, cars_orders.source, cars_orders.details, "
            . "cars_orders.thedate, cars_orders.thetime, cars_orders.insert_date, "
            . "cars_orders.insert_time, cars_orders.deleted FROM cars_orders INNER JOIN cars_orders_items ON "
            . "(cars_orders.id = cars_orders_items.invoice_id) WHERE cars_orders_items.service_id =".$service_id." AND cars_orders.company_id=$company_id order by cars_orders.id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function Get_teamwork_id($teamwork_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_orders where teamwork_id=".$teamwork_id." order by id asc";
        }
        else
        {
            $SQL = "select * from cars_orders where company_id=$company_id AND teamwork_id=".$teamwork_id." order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function GetMultiRow_teamwork_id($teamwork_id) {
        $TheDate = date("Y-m-d");
        $FirstDay = date('Y-m-01', strtotime($TheDate));
        $LastDay = date('Y-m-t', strtotime($TheDate));
        $query = $this->db->query("select * from cars_orders where teamwork_id=".$teamwork_id." AND "
                . "thedate between '".$FirstDay."' AND '".$LastDay."' order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('cars_orders', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('cars_orders', $data); 
    }
    
    public function GetCount_teamwork_id($teamwork_id)
    {
        $query = $this->db->query("select id, teamwork_id from cars_orders 
            where teamwork_id=".$teamwork_id);
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function GetItems($invoice_id)
    {
        $query = $this->db->query("select * from cars_orders_items 
            where invoice_id=".$invoice_id);
        return $query->result();
    }
    
    public function GetLatest($user_id, $customer_id, $teamwork_id) {
        $query = $this->db->query("select * from cars_orders "
        . "where user_id=".$user_id." AND customer_id=".$customer_id." AND teamwork_id=".$teamwork_id." order by id desc");
        $row = $query->row();
        return $row;
    }

    public function GetAffiliate() {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_orders order by id asc";
        }
        else
        {
            $SQL = "select * from cars_orders where company_id=$company_id order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetFilter($fromdate, $todate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_orders where thedate between '$fromdate' AND '$todate' order by id asc";
        }
        else
        {
            $SQL = "select * from cars_orders where company_id=$company_id AND thedate between '$fromdate' AND '$todate' order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetHead($ID) {
        $SQL = "SELECT cars_orders.id, sale_customer.id AS sale_customer_id, cars_orders.company_id, ";
        $SQL .="cars_orders.user_id, cars_orders.affiliate_id, cars_orders.customer_id, ";
        $SQL .="sale_customer.ar_title AS customer_ar_title, sale_customer.en_title AS customer_en_title, cars_orders.teamwork_id, ";
        $SQL .="cars_orders.status_id, set_request_status.ar_title AS status_ar_title, set_request_status.en_title AS status_en_title, ";
        $SQL .="cars_orders.area_id, set_area.ar_title AS area_ar_title, set_area.en_title AS area_en_title, cars_orders.source, ";
        $SQL .="cars_orders.details, cars_orders.thedate, cars_orders.thetime, cars_orders.insert_date, cars_orders.insert_time, ";
        $SQL .="cars_orders.closed_date, cars_orders.closed_time, ";
        $SQL .="set_teamwork.ar_title as teamwork_ar_title, set_teamwork.en_title as teamwork_en_title FROM sale_customer ";
        $SQL .="INNER JOIN cars_orders ON (sale_customer.id = cars_orders.customer_id) INNER JOIN set_request_status ON ";
        $SQL .="(cars_orders.status_id = set_request_status.id) INNER JOIN set_area ON (cars_orders.area_id = set_area.id) ";
        $SQL .="INNER JOIN set_teamwork ON (cars_orders.teamwork_id = set_teamwork.id) where cars_orders.id= $ID";
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }

    public function GetDetails($ID) {
        $SQL = "SELECT cars_orders_items.id, cars_orders_items.invoice_id, cars_orders_items.service_id, ";
        $SQL .="cars_service.ar_title, cars_service.en_title, cars_orders_items.qunatity as a_qunatity, cars_orders_items.unit_price ";
        $SQL .="FROM cars_service INNER JOIN cars_orders_items ON (cars_service.id = cars_orders_items.service_id) ";
        $SQL .="WHERE cars_orders_items.invoice_id = $ID";
        $query = $this->db->query($SQL);
        return $query->result();
    }
}

?>