<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_cars_invoice extends CI_Model {
    
    public function GetRow($ID) {
        $query = $this->db->query("select * from cars_invoice where id=".$ID);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_invoice order by id asc";
        }
        else
        {
            $SQL = "select * from cars_invoice where company_id=$company_id order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function GetMultiRowByStatus($status_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_invoice where status_id=".$status_id." order by id asc";
        }
        else
        {
            $SQL = "select * from cars_invoice where company_id=$company_id AND status_id=".$status_id." order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function GetMultiRowByArea($area_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_invoice where area_id=".$area_id." order by id asc";
        }
        else
        {
            $SQL = "select * from cars_invoice where company_id=$company_id AND area_id=".$area_id." order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function GetMultiRowByCustomer($customer_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_invoice where customer_id=".$customer_id." order by id asc";
        }
        else
        {
            $SQL = "select * from cars_invoice where company_id=$company_id AND customer_id=".$customer_id." order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetCountByCustomer($customer_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_invoice where customer_id=".$customer_id." order by id asc";
        }
        else
        {
            $SQL = "select * from cars_invoice where company_id=$company_id AND customer_id=".$customer_id." order by id asc";
        }
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

    public function GetMultiRowByService($service_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "SELECT cars_invoice.id, cars_invoice.user_id, cars_invoice.company_id, cars_invoice.customer_id, cars_invoice.teamwork_id, "
            . "cars_invoice.status_id, cars_invoice.area_id, cars_invoice.source, cars_invoice.details, "
            . "cars_invoice.thedate, cars_invoice.thetime, cars_invoice.insert_date, "
            . "cars_invoice.insert_time, cars_invoice.deleted FROM cars_invoice INNER JOIN cars_invoice_items ON "
            . "(cars_invoice.id = cars_invoice_items.invoice_id) WHERE cars_invoice_items.service_id =".$service_id." order by cars_invoice.id asc";
        }
        else
        {
            $SQL = "SELECT cars_invoice.id, cars_invoice.user_id, cars_invoice.company_id, cars_invoice.customer_id, cars_invoice.teamwork_id, "
            . "cars_invoice.status_id, cars_invoice.area_id, cars_invoice.source, cars_invoice.details, "
            . "cars_invoice.thedate, cars_invoice.thetime, cars_invoice.insert_date, "
            . "cars_invoice.insert_time, cars_invoice.deleted FROM cars_invoice INNER JOIN cars_invoice_items ON "
            . "(cars_invoice.id = cars_invoice_items.invoice_id) WHERE cars_invoice_items.service_id =".$service_id." AND cars_invoice.company_id=$company_id order by cars_invoice.id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function Get_teamwork_id($teamwork_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_invoice where teamwork_id=".$teamwork_id." order by id asc";
        }
        else
        {
            $SQL = "select * from cars_invoice where company_id=$company_id AND teamwork_id=".$teamwork_id." order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function GetMultiRow_teamwork_id($teamwork_id) {
        $TheDate = date("Y-m-d");
        $FirstDay = date('Y-m-01', strtotime($TheDate));
        $LastDay = date('Y-m-t', strtotime($TheDate));
        $query = $this->db->query("select * from cars_invoice where teamwork_id=".$teamwork_id." AND "
                . "thedate between '".$FirstDay."' AND '".$LastDay."' order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('cars_invoice', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('cars_invoice', $data); 
    }
    
    public function GetCount_teamwork_id($teamwork_id)
    {
        $query = $this->db->query("select id, teamwork_id from cars_invoice 
            where teamwork_id=".$teamwork_id);
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function GetItems($invoice_id)
    {
        $query = $this->db->query("select * from cars_invoice_items 
            where invoice_id=".$invoice_id);
        return $query->result();
    }
    
    public function GetLatest($user_id, $customer_id, $teamwork_id) {
        $query = $this->db->query("select * from cars_invoice "
        . "where user_id=".$user_id." AND customer_id=".$customer_id." AND teamwork_id=".$teamwork_id." order by id desc");
        $row = $query->row();
        return $row;
    }

    public function GetAffiliate() {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_invoice order by id asc";
        }
        else
        {
            $SQL = "select * from cars_invoice where company_id=$company_id order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetFilter($fromdate, $todate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from cars_invoice where thedate between '$fromdate' AND '$todate' order by id asc";
        }
        else
        {
            $SQL = "select * from cars_invoice where company_id=$company_id AND thedate between '$fromdate' AND '$todate' order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetHead($ID) {
        $SQL = "SELECT cars_invoice.id, sale_customer.id AS sale_customer_id, cars_invoice.company_id, ";
        $SQL .="cars_invoice.user_id, cars_invoice.affiliate_id, cars_invoice.customer_id, ";
        $SQL .="sale_customer.ar_title AS customer_ar_title, sale_customer.en_title AS customer_en_title, cars_invoice.teamwork_id, ";
        $SQL .="cars_invoice.status_id, set_request_status.ar_title AS status_ar_title, set_request_status.en_title AS status_en_title, ";
        $SQL .="cars_invoice.area_id, set_area.ar_title AS area_ar_title, set_area.en_title AS area_en_title, cars_invoice.source, ";
        $SQL .="cars_invoice.details, cars_invoice.thedate, cars_invoice.thetime, cars_invoice.insert_date, cars_invoice.insert_time, ";
        $SQL .="set_teamwork.ar_title as teamwork_ar_title, set_teamwork.en_title as teamwork_en_title FROM sale_customer ";
        $SQL .="INNER JOIN cars_invoice ON (sale_customer.id = cars_invoice.customer_id) INNER JOIN set_request_status ON ";
        $SQL .="(cars_invoice.status_id = set_request_status.id) INNER JOIN set_area ON (cars_invoice.area_id = set_area.id) ";
        $SQL .="INNER JOIN set_teamwork ON (cars_invoice.teamwork_id = set_teamwork.id) where cars_invoice.id= $ID";
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }

    public function GetDetails($ID) {
        $SQL = "SELECT cars_invoice_items.id, cars_invoice_items.invoice_id, cars_invoice_items.service_id, ";
        $SQL .="services.ar_title, services.en_title, cars_invoice_items.qunatity, cars_invoice_items.unit_price ";
        $SQL .="FROM services INNER JOIN cars_invoice_items ON (services.id = cars_invoice_items.service_id) ";
        $SQL .="WHERE cars_invoice_items.invoice_id = $ID";
        $query = $this->db->query($SQL);
        return $query->result();
    }
}

?>