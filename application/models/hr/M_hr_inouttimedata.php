<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_hr_inouttimedata extends CI_Model {
    
	public function GetRow($id) {
        $query = $this->db->query("select * from hr_inouttimedata where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from hr_inouttimedata where company_id=$company_id order by id asc");
        return $query->result();
    }

    public function GetReport($employee_id, $fromdate, $todate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from hr_inouttimedata where company_id=$company_id ";
        if($employee_id > 0)
        {
            $SQL .="AND employee_id=$employee_id ";
        }
        $SQL .="AND in_date between '$fromdate' AND '$todate'";
        $SQL .= " order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('hr_inouttimedata', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('hr_inouttimedata', $data); 
    }

    public function CheckEmployee($employee_id, $fromdate, $todate)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from hr_inouttimedata where company_id=$company_id ";
        $SQL .= "AND employee_id=$employee_id AND in_date BETWEEN '$fromdate' AND '$todate'";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }
}

?>