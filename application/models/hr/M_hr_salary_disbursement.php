<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_hr_salary_disbursement extends CI_Model {
   
      public function GetCountRow($emp_id,$month,$year) {
          
        $query = $this->db->query("select count(*) as cnt from hr_salary_disbursement where emp_id=".$emp_id." AND month=".$month." AND year=".$year);
        $row = $query->row();
        return $row;
    }
    
    
       public function InsertRecord($data) {
        $this->db->insert('hr_salary_disbursement', $data); 
    }
    
     public function GetLatestRecord(){
         $query = $this->db->query("select max(id) as max from hr_salary_disbursement");
        $row = $query->row();
        return $row;
     }
   
   
}