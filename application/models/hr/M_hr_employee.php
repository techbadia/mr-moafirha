<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_hr_employee extends CI_Model {

    public function GetRow($id) {
        $query = $this->db->query("select * from hr_employee where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetAnotheStores($id) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from hr_employee where company_id=$company_id AND id <> $id order by id asc");
        return $query->result();
    }

    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from hr_employee where company_id= ".$company_id." order by id asc");

        if(!empty($_POST['company_id'] ))
        {

        if($_POST['company_id'] == "all")
        {
          $query = $this->db->query("select * from hr_employee order by id asc");
        }else {
          $query = $this->db->query("select * from hr_employee where company_id= ".$_POST['company_id']." order by id asc");
        }
      }
        return $query->result();
    }

    public function GetReport($country_id, $education_id, $job_id, $job_type_id, $workgroup_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from hr_employee where company_id=$company_id";
        if($country_id > 0)
        {
            $SQL .= " AND country_id=$country_id";
        }
        if($education_id > 0)
        {
            $SQL .= " AND education_id=$education_id";
        }
        if($job_id > 0)
        {
            $SQL .= " AND job_id=$job_id";
        }
        if($job_type_id > 0)
        {
            $SQL .= " AND job_type_id=$job_type_id";
        }
        if($workgroup_id > 0)
        {
            $SQL .= " AND workgroup_id=$workgroup_id";
        }
        $SQL .= " order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('hr_employee', $data);
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('hr_employee', $data);
    }

    public function StoreCount($category_id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from product where company_id=$company_id AND category_id=$category_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function CountryCount($country_id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from hr_employee where company_id=$company_id AND country_id=$country_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }
    //EducationCount
    public function EducationCount($education_id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from hr_employee where company_id=$company_id AND education_id=$education_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function JobCount($job_id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from hr_employee where company_id=$company_id AND job_id=$job_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function JobTypeCount($job_type_id )
    {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from hr_employee where company_id=$company_id AND job_type_id =$job_type_id ";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }
}

?>
