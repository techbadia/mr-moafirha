<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_Pos extends CI_Model
{

    public function products($page_index, $limit)
    {
        $store =  $this->store();

        $this->db->select('
                           quantity,product_id as product_id ,barcode,batch_number,product.title as prod_title,
                           product.title_en as prod_title_en,store_category.title as cat_title,store_category.title_en as cat_title_en,
                           store_brand.title as brand_title,store_brand.title_en as brand_title_en,
                           details,details_en,cost_price,lowest_price,price,image,demand,category_id,brand_id
                           ');
        $this->db->from('store_quantity');
        $this->db->join('product', 'product.id = store_quantity.product_id');
        $this->db->join('store_category', 'store_category.id = product.category_id');
        $this->db->join('store_brand', 'store_brand.id = product.brand_id');
        $this->db->where([
            'store_quantity.store_id' => $store->store_id,
            // 'store_quantity.company_id' => $store->company_id,
            'product.deleted' => 0,
            'price >' => '0'
        ]);
        if ($this->input->get('category_id') != 'all') {
            $this->db->where('product.category_id', $this->input->get('category_id'));
        }
        if ($this->input->get('brand_id') != 'all') {
            $this->db->where('product.brand_id', $this->input->get('brand_id'));
        }


        return $this->db->limit($page_index, $limit)->get()->result();
    }
    public function list()
    {
        $store =  $this->store();

        $term = $this->input->get('term');
        $this->db->from('store_quantity');
        $this->db->join('product', 'product.id = store_quantity.product_id');
        $this->db->where([
            'store_quantity.store_id' => $store->store_id,
            // 'store_quantity.company_id' => $store->company_id,
            'product.deleted' => 0,
            'price >' => '0'
        ]);
        $this->db->group_start()
            ->like('product.title', "$term")
            ->or_like('product.barcode', "$term")
            ->group_end();
        return $this->db->limit(15)->get()->result();
    }
    public function get_prod_id($id)
    {
        $store =  $this->store();

        $this->db->select('
                           quantity,product_id as product_id ,barcode,batch_number,product.title as prod_title,
                           product.title_en as prod_title_en,store_category.title as cat_title,store_category.title_en as cat_title_en,
                           store_brand.title as brand_title,store_brand.title_en as brand_title_en,
                           details,details_en,cost_price,lowest_price,price,image,demand,category_id,brand_id
                           ');
        $this->db->from('store_quantity');
        $this->db->join('product', 'product.id = store_quantity.product_id');
        $this->db->join('store_category', 'store_category.id = product.category_id');
        $this->db->join('store_brand', 'store_brand.id = product.brand_id');
        $this->db->where([
            'store_quantity.store_id' => $store->store_id,
            // 'store_quantity.company_id' => $store->company_id,
            'product.deleted' => 0,
            'store_quantity.product_id' => $id
        ]);

        return $this->db->get()->row();
    }
    public function categories()
    {
        $company_id = $this->session->userdata('company_id');
        $query = $this->db->query("SELECT * FROM `store_category` WHERE company_id ='$company_id'");
        return $query->result();
    }

    public function brandes()
    {
        $company_id = $this->session->userdata('company_id');
        $query = $this->db->query("SELECT * FROM `store_brand` WHERE company_id ='$company_id'");
        return $query->result();
    }
    public function store()
    {
        $company_id = $this->session->userdata('company_id');
        $user_id = $this->session->userdata('StaffID');
        $query = $this->db->query("SELECT * FROM `usr_users_store` WHERE user_id='$user_id' limit 1");
        $query->row()->company_id =$company_id;
        return $query->row();
    }

    public function orders()
    {
        $store =  $this->store();
        // var_dump($store->store_id);
        $this->db->from('sale_bill');
        $this->db->where([
            'sale_bill.pos_store_id' => $store->store_id,
        ]);
        return $this->db->get()->result();
    }
    public function get_order($id = null)
    {
        $store =  $this->store();
        $this->db->select('
        app_company.ar_title as companyName,
        app_company.phone as companyPhone,
        app_company.tax_number as tax_number,
        ,sale_bill.*');
        $this->db->from('sale_bill');
        $this->db->join('app_company', 'app_company.id = sale_bill.company_id');
        $this->db->where([
            'sale_bill.id' => $id,
            'sale_bill.pos_store_id' => $store->store_id,
            // 'sale_bill.company_id' => $store->company_id,
        ]);
        if ($order = $this->db->get()->row()) {

            $order->items =   $this->db->from('sale_bill_items')
                ->where('bill_id', $id)->get()->result();

            return $order;
        }
        return null;
    }
    public function update_prodcut($product_id, $quantity)
    {
        $store =  $this->store();
        $query = $this->db->from('store_quantity')->where([
            'store_id' => $store->store_id,
            'product_id' => $product_id,
        ])->get()->row();
        $quantity = intval($query->quantity) - intval($quantity);
        $this->db->where('id', $query->id);
        $this->db->update('store_quantity', ['quantity' => $quantity]);
    }
    public function get_products()
    {
        $store =  $this->store();
        $this->db->select('
        store_quantity.quantity as quantity,product.*');
        $this->db->from('store_quantity');
        $this->db->join('product', 'product.id = store_quantity.product_id');
        $this->db->where([
            'store_quantity.store_id' => $store->store_id,
            // 'store_quantity.company_id' => $store->company_id,
        ]);
        $this->db->order_by('store_quantity.quantity','DESC');
        return $this->db->get()->result();
    }
}
