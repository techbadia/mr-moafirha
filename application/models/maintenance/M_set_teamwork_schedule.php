<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_set_teamwork_schedule extends CI_Model {
    
    public function GetMultiRow() {
        $query = $this->db->query("select id, teamwork_id, customer, phone, address, thedate, schedule from set_teamwork_schedule order by id asc");
        return $query->result();
    }
    
    
    public function InsertRecord($teamwork_id, $customer, $phone, $address, $thedate, $schedule) {
        $data = array(
            'teamwork_id' => $teamwork_id,
            'customer' => $customer,
            'phone' => $phone,
            'address' => $address,
            'thedate' => $thedate,
            'schedule' => $schedule
        );
    
        $this->db->insert('set_teamwork_schedule', $data); 
        //$this->db->cache_delete('usr_usersgroup');
    }
    
}

?>