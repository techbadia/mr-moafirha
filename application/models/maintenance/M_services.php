<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_services extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from services where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetAnotheStores($id) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from services where company_id=$company_id AND id <> $id order by id asc");
        return $query->result();
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from services where company_id=$company_id order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('services', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('services', $data); 
    }

    public function GetCount($country_id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from hr_employee where company_id=$company_id AND country_id=$country_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }
    
}

?>