<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_data_teamwork_items_revenues extends CI_Model {
    
    public function GetRow($ID) {
        $query = $this->db->query("select id, item_id, amount, thedate, thetime, details from data_teamwork_items_revenues where id=".$ID);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $query = $this->db->query("select id, item_id, amount, thedate, thetime, details from data_teamwork_items_revenues order by id asc");
        return $query->result();
    }
    
    public function GetItem($item_id) {
        $query = $this->db->query("select id, item_id, amount, thedate, thetime, details from "
        . "data_teamwork_items_revenues where item_id=".$item_id." order by id asc");
        return $query->result();
    }
    
    public function GetMultiRowSearch($fromdate, $todate, $item_id) {
        $SQL = "select id, item_id, amount, thedate, thetime, details from "
        . "data_teamwork_items_revenues where thedate between '".$fromdate."' AND '".$todate."'";
        if(intval($item_id) > 0)
        {
            $SQL .= " AND item_id=".$item_id;
        }
        $SQL .= " order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function InsertRecord($item_id, $amount, $thedate, $thetime, $details) {
        $data = array(
            'item_id' => $item_id,
            'amount' => $amount,
            'thedate' => $thedate,
            'thetime' => $thetime,
            'details' => $details
        );
    
        $this->db->insert('data_teamwork_items_revenues', $data); 
        //$this->db->cache_delete('usr_usersgroup');
    }
    
    public function UpdateRecord($id, $item_id, $amount, $thedate, $thetime, $details) {
        $data = array(
            'item_id' => $item_id,
            'amount' => $amount,
            'thedate' => $thedate,
            'thetime' => $thetime,
            'details' => $details
        );
            
        $this->db->where('id', $id);
        $this->db->update('data_teamwork_items_revenues', $data); 
        //$this->db->cache_delete('usr_usersgroup');
    }
    
    public function GetItemsCount($item_id)
    {
        $query = $this->db->query("select id, item_id, amount, thedate, thetime from data_teamwork_items_revenues 
            where item_id=".$item_id);
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function GetItems($item_id)
    {
        $TheDate = date("Y-m-d");
        $FirstDay = date('Y-m-01', strtotime($TheDate));
        $LastDay = date('Y-m-t', strtotime($TheDate));
        $query = $this->db->query("select SUM(amount) as amount from data_teamwork_items_revenues 
            where item_id=".$item_id." AND thedate between '".$FirstDay."' AND '".$LastDay."'");
        $row = $query->row();
        return $row;
    }
    
    
    public function CheckDuplicate($item_id, $amount, $thedate)
    {
        $query = $this->db->query("select id, item_id, amount, thedate, thetime from 
            data_teamwork_items_revenues where item_id=".$item_id." AND amount=".$amount." AND thedate='".$thedate."'");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
}

?>