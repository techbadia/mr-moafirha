<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_set_teamwork_tasks extends CI_Model {
    
    public function GetRow($ID) {
        $query = $this->db->query("select id, teamwork_id, invoice_id, thedate, thetime, closed, close_date, close_time from set_teamwork_tasks where id=".$ID);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $query = $this->db->query("select id, teamwork_id, invoice_id, thedate, thetime, closed, close_date, close_time from set_teamwork_tasks order by id asc");
        return $query->result();
    }
    
    public function GetByTeamWork($teamwork_id) {
        $query = $this->db->query("select id, teamwork_id, invoice_id, thedate, thetime, closed, close_date, close_time from "
        . "set_teamwork_tasks where teamwork_id=".$teamwork_id." order by id asc");
        return $query->result();
    }
    
    public function GetOPened() {
        $query = $this->db->query("select id, teamwork_id, invoice_id, thedate, thetime, closed, "
        . "close_date, close_time from set_teamwork_tasks where closed=0 order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($teamwork_id, $invoice_id, $thedate, $thetime, $closed) {
        $data = array(
            'teamwork_id' => $teamwork_id,
            'invoice_id' => $invoice_id,
            'thedate' => $thedate,
            'thetime' => $thetime,
            'closed' => $closed
        );
    
        $this->db->insert('set_teamwork_tasks', $data); 
        //$this->db->cache_delete('usr_usersgroup');
    }
    
    public function UpdateRecord($id, $teamwork_id, $invoice_id, $thedate, $thetime, $closed) {
        $data = array(
            'teamwork_id' => $teamwork_id,
            'invoice_id' => $invoice_id,
            'thedate' => $thedate,
            'thetime' => $thetime,
            'closed' => $closed
        );
            
        $this->db->where('id', $id);
        $this->db->update('set_teamwork_tasks', $data); 
        //$this->db->cache_delete('usr_usersgroup');
    }
    
    public function UpdateClose($id, $closed, $close_date, $close_time) {
        $data = array(
            'closed' => $closed,
            'close_date' => $close_date,
            'close_time' => $close_time
        );
            
        $this->db->where('id', $id);
        $this->db->update('set_teamwork_tasks', $data); 
    }
}

?>