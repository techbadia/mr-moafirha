<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_data_request_items extends CI_Model {
    
    public function GetRow($ID) {
        $query = $this->db->query("select * from data_request_items where id=".$ID);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $query = $this->db->query("select * from data_request_items order by id asc");
        return $query->result();
    }
    
    public function GetByRequest($request_id) {
        $query = $this->db->query("select * from data_request_items where request_id=".$request_id." order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('data_request_items', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('data_request_items', $data); 
    }
    

    public function CheckDuplicate($request_id, $service_id)
    {
        $query = $this->db->query("select id, request_id, service_id from data_request_items where 
        request_id=".$request_id." AND service_id=".$service_id);
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function GetOrderService($request_id, $service_id) {
        $query = $this->db->query("select id, request_id, service_id, qunatity, unit_price from "
        . "data_request_items where request_id=$request_id AND service_id=$service_id order by id asc");
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
}

?>