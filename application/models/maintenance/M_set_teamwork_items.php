<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_set_teamwork_items extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from set_teamwork_items where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetAnotheStores($id) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from set_teamwork_items where company_id=$company_id AND id <> $id order by id asc");
        return $query->result();
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from set_teamwork_items where company_id=$company_id order by id asc");
        return $query->result();
    }
    
    public function GetRevenues($item_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("SELECT data_teamwork_items_revenues.amount FROM set_teamwork_items INNER "
        . "JOIN data_teamwork_items_revenues ON (set_teamwork_items.id = data_teamwork_items_revenues.item_id) "
        . "WHERE set_teamwork_items.id =".$item_id);
        return $query->result();
    }
    
    public function GetExpenses($item_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("SELECT data_teamwork_items_expenses.amount FROM set_teamwork_items INNER "
        . "JOIN data_teamwork_items_expenses ON (set_teamwork_items.id = data_teamwork_items_expenses.item_id) "
        . "WHERE set_teamwork_items.id =".$item_id);
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('set_teamwork_items', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('set_teamwork_items', $data); 
    }

    public function GetCount($country_id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from hr_employee where company_id=$company_id AND country_id=$country_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }
    
}

?>