<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_data_request_web extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from data_request_web where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $query = $this->db->query("select * from data_request_web converted=0 order by id asc");
        return $query->result();
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('data_request_web', $data); 
    }
    
}

?>