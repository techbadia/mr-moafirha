<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_data_invoice_expenses extends CI_Model {
    
    public function GetRow($ID) {
        $query = $this->db->query("select id, invoice_id, amount, tax, thedate, thetime, details from data_invoice_expenses where id=".$ID);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $query = $this->db->query("select id, invoice_id, amount, tax, thedate, thetime, details from data_invoice_expenses order by id asc");
        return $query->result();
    }
    
    public function GetByInvoice($invoice_id) {
        $query = $this->db->query("select id, invoice_id, amount, tax, thedate, thetime, details from "
        . "data_invoice_expenses WHERE invoice_id=".$invoice_id." order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($invoice_id, $amount, $tax, $thedate, $thetime, $details) {
        $data = array(
            'invoice_id' => $invoice_id,
            'amount' => $amount,
            'tax' => $tax,
            'thedate' => $thedate,
            'thetime' => $thetime,
            'details' => $details
        );
    
        $this->db->insert('data_invoice_expenses', $data); 
        //$this->db->cache_delete('usr_usersgroup');
    }
    
    public function UpdateRecord($id, $invoice_id, $amount, $tax, $thedate, $thetime, $details) {
        $data = array(
            'invoice_id' => $invoice_id,
            'amount' => $amount,
            'tax' => $tax,
            'thedate' => $thedate,
            'thetime' => $thetime,
            'details' => $details
        );
            
        $this->db->where('id', $id);
        $this->db->update('data_invoice_expenses', $data); 
        //$this->db->cache_delete('usr_usersgroup');
    }
    
    public function GetItemsCount($invoice_id)
    {
        $TheDate = date("Y-m-d");
        $FirstDay = date('Y-m-01', strtotime($TheDate));
        $LastDay = date('Y-m-t', strtotime($TheDate));
        $query = $this->db->query("select id, invoice_id, amount, thedate, thetime from data_invoice_expenses 
            where invoice_id=".$invoice_id." AND thedate Between '".$FirstDay."' AND '".$LastDay."'");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function GetItems($invoice_id)
    {
        $TheDate = date("Y-m-d");
        $FirstDay = date('Y-m-01', strtotime($TheDate));
        $LastDay = date('Y-m-t', strtotime($TheDate));
        $query = $this->db->query("select id, invoice_id, amount, thedate, thetime from data_invoice_expenses 
            where invoice_id=".$invoice_id." AND thedate Between '".$FirstDay."' AND '".$LastDay."'");
        return $query->result();
    }
    
}

?>