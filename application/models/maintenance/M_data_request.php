<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_data_request extends CI_Model {
    
    public function GetRow($ID) {
        $query = $this->db->query("select * from data_request where id=".$ID);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow($customer_id, $status_id, $area_id, $fromdate, $todate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from data_request where thedate between '$fromdate' AND '$todate' AND company_id=$company_id";
        if($customer_id > 0)
        {
            $SQL .=" AND customer_id=$customer_id";
        }
        if($status_id > 0)
        {
            $SQL .=" AND status_id=$status_id";
        }
        if($area_id > 0)
        {
            $SQL .=" AND area_id=$area_id";
        }
        $SQL .=" order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function GetMultiRowByStatus($status_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from data_request where status_id=".$status_id." order by id asc";
        }
        else
        {
            $SQL = "select * from data_request where company_id=$company_id AND status_id=$status_id order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function GetMultiRowByArea($area_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from data_request where area_id=".$area_id." order by id asc";
        }
        else
        {
            $SQL = "select * from data_request where company_id=$company_id AND area_id=$area_id order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetMultiRowByCustomer($customer_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from data_request where customer_id=".$customer_id." order by id asc";
        }
        else
        {
            $SQL = "select * from data_request where company_id=$company_id AND customer_id=$customer_id order by id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetCountByCustomer($customer_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "select * from data_request where customer_id=".$customer_id." order by id asc";
        }
        else
        {
            $SQL = "select * from data_request where company_id=$company_id AND customer_id=$customer_id order by id asc";
        }
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function GetMultiRowByService($service_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "";
        if($company_id == 0)
        {
            $SQL = "SELECT data_request.id, data_request.user_id, data_request.company_id, data_request.customer_id, "
            . "data_request.status_id, data_request.area_id, data_request.source, data_request.details, "
            . "data_request.thedate, data_request.thetime, data_request.insert_date, "
            . "data_request.insert_time FROM data_request INNER JOIN data_request_items ON "
            . "(data_request.id = data_request_items.request_id) WHERE data_request_items.service_id =".$service_id." order by data_request.id asc";
        }
        else
        {
            $SQL = "SELECT data_request.id, data_request.user_id, data_request.company_id, data_request.customer_id, "
            . "data_request.status_id, data_request.area_id, data_request.source, data_request.details, "
            . "data_request.thedate, data_request.thetime, data_request.insert_date, "
            . "data_request.insert_time FROM data_request INNER JOIN data_request_items ON "
            . "(data_request.id = data_request_items.request_id) WHERE data_request_items.service_id =$service_id AND data_request.company_id=$company_id order by data_request.id asc";
        }
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('data_request', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('data_request', $data); 
    }
    
    public function UpdateStatus($id, $status_id) {
        $data = array(
            'status_id' => $status_id
        );
            
        $this->db->where('id', $id);
        $this->db->update('data_request', $data); 
    }
    

    public function CheckDuplicate($customer_id, $area_id, $thedate, $thetime)
    {
        $query = $this->db->query("select id, customer_id, area_id, thedate, thetime from data_request where 
        customer_id=".$customer_id." AND area_id=".$area_id." AND thedate='".$thedate."' AND thetime='".$thetime."'");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function GetLatest($customer_id, $area_id, $thedate, $thetime) {
        $query = $this->db->query("select id, customer_id, area_id, thedate, thetime from data_request where 
        customer_id=".$customer_id." AND area_id=".$area_id." AND thedate='".$thedate."' AND thetime='".$thetime."' order by id desc");
        $row = $query->row();
        return $row;
    }
    
    public function GetHead($ID) {
        $SQL = "SELECT sale_customer.id as sale_customer_id, data_request.id, data_request.company_id, data_request.user_id, ";
        $SQL .="data_request.affiliate_id, data_request.customer_id, sale_customer.ar_title AS customer_ar_title, ";
        $SQL .="sale_customer.en_title AS customer_en_title, data_request.status_id, set_request_status.ar_title AS status_ar_title, ";
        $SQL .="set_request_status.en_title AS status_en_title, data_request.area_id, set_area.ar_title AS area_ar_title, ";
        $SQL .="set_area.en_title AS area_en_title, data_request.source, data_request.details, data_request.thedate, data_request.thetime, ";
        $SQL .="data_request.insert_date, data_request.insert_time FROM sale_customer INNER JOIN data_request ON ";
        $SQL .="(sale_customer.id = data_request.customer_id) ";
        $SQL .="INNER JOIN set_request_status ON (data_request.status_id = set_request_status.id)";
        $SQL .="INNER JOIN set_area ON (data_request.area_id = set_area.id) WHERE data_request.id = $ID";
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }

    public function GetDetails($ID) {
        $SQL = "SELECT data_request_items.id, data_request_items.request_id, data_request_items.service_id, ";
        $SQL .="services.ar_title, services.en_title, data_request_items.qunatity as r_quantity, data_request_items.unit_price ";
        $SQL .="FROM services INNER JOIN data_request_items ON (services.id = data_request_items.service_id) ";
        $SQL .="WHERE data_request_items.request_id = $ID";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetAreaCount($area_id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select id, area_id from data_request where 
        area_id=$area_id AND company_id=$company_id group by area_id");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

    public function GetServiceCount($service_id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select id, request_id, service_id from data_request_items where service_id=$service_id group by request_id");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

}

?>