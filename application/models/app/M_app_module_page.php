<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_app_module_page extends CI_Model {

	public function GetRow($id) {
        $query = $this->db->query("select * from app_module_page where id=".$id);
        $row = $query->row();
        return $row;
    }

	public function GetByModule($module_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from app_module_page where module_id=$module_id order by orders asc");
        return $query->result();
    }

    public function GetSubMenu($module_id) {
        $query = $this->db->query("select * from app_module_page where module_id=".$module_id." AND enabled=1 order by
        orders asc");
        return $query->result();
    }

    public function GetSubMenuCount($module_id) {
        $query = $this->db->query("select * from app_module_page where module_id=".$module_id." AND enabled=1
        order by id, orders asc");
        $RecordCount = 0;
        $RecordCount = $query->num_rows();
        return $RecordCount;
    }

    public function GetMultiRow() {
        $query = $this->db->query("select * from app_module_page where enabled=1 order by id asc");
        return $query->result();
    }

    public function GetAllPages() {
        $query = $this->db->query("SELECT app_module_page.id, app_module_page.ar_title, app_module_page.en_title,
            app_module_page.enabled, app_module.enabled AS
        module_enabled, app_module_page.target_blank FROM app_module INNER JOIN app_module_page ON
        (app_module.id = app_module_page.module_id) where app_module.enabled=1 AND app_module_page.enabled=1");
        return $query->result();
    }
    public function GetAllPagesForCheck() {

        $query = $this->db->query("SELECT id FROM  app_module_page where perm_checked = 0");
        return $query->result();
    }

    public function GetAllPagesCount() {
        $query = $this->db->query("SELECT app_module_page.id, app_module_page.ar_title, app_module_page.en_title,
        app_module_page.enabled, app_module.enabled AS
        module_enabled, app_module_page.target_blank  FROM app_module INNER JOIN app_module_page ON
        (app_module.id = app_module_page.module_id) where app_module.enabled=1 AND app_module_page.enabled=1");
        $RecordCount = 0;
        $RecordCount = $query->num_rows();
        return $RecordCount;
    }

    public function GetPageData($id) {
        $query = $this->db->query("select id, orders, module_id, table_view, target_blank, report, enabled,
        class_name, ar_title, ar_details, en_title, en_details from app_module_page where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetPageDataByClass($class_name) {
        $query = $this->db->query("select id, orders, module_id, table_view, target_blank, report, enabled,
        class_name, ar_title, ar_details, en_title, en_details from app_module_page where class_name='".$class_name."'");
        $row = $query->row();
        return $row;
    }

    public function GetCount($class_name)
    {
        $SQL = "select id, orders, module_id, table_view, target_blank, report, enabled,
        class_name, ar_title, ar_details, en_title, en_details from app_module_page where class_name='".$class_name."'";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function GetPageByModule($module_id)
    {
        $SQL = "select * from app_module_page where module_id=$module_id";
        $query = $this->db->query($SQL);
        return $query->result();
    }
    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('app_module_page', $data);
    }
}

?>
