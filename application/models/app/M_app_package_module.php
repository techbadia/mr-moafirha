<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_app_package_module extends CI_Model {

    public function GetRow($id) {
        $query = $this->db->query("select * from app_package_module where id=".$id);
        $row = $query->row();
        return $row;
    }

	public function GetByPackage($package_id) {
        $query = $this->db->query("select * from app_package_module where package_id=$package_id order by id asc");
        return $query->result();
    }

	public function GetLatest($package_id, $module_id) {
        $query = $this->db->query("select * from app_package_module where package_id=$package_id AND module_id=$module_id order by id desc");
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from app_package_module where company_id=$company_id order by id asc");
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('app_package_module', $data);
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('app_package_module', $data);
    }

	public function GetCount($package_id)
    {
        $SQL = "select * from app_package_module where package_id=$package_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

	public function GetMainModules($package_id) {
        $query = $this->db->query("select * from app_package_module where parent_id=0 AND package_id=$package_id order by order_by asc");
        // $apps = $query->result();
        // foreach ($apps as $app) {
        //   $data['order_by'] = $app->id;
        //   $this->db->where('id', $app->id);
        //   $this->db->update('app_package_module', $data);
        // }
        return $query->result();
    }

	public function GetSubModules($package_id, $module_id) {
        $query = $this->db->query("select * from app_package_module where parent_id=$module_id AND package_id=$package_id order by id asc");
        return $query->result();
    }

	public function GetSubModulesCount($package_id, $module_id)
    {
        $SQL = "select * from app_package_module where parent_id=$module_id AND package_id=$package_id order by id asc";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

}

?>
