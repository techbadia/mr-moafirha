<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_app_package extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from app_package where id=".$id);
        $row = $query->row();
        return $row;
    }
	
	public function GetByTitle($title) {
        $query = $this->db->query("select * from app_package where title='$title'");
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from app_package where company_id=$company_id order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('app_package', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('app_package', $data); 
    }

}

?>