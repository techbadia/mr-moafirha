<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_app_package_module_page extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from app_package_module_page where id=".$id);
        $row = $query->row();
        return $row;
    }
    
	public function GetByModule($app_package_module_id) {
        $SQL = "select app_package_module_page.id, app_package_module_page.app_package_module_id, ";
        $SQL .= "app_package_module_page.page_id, app_package_module_page.ar_title, ";
        $SQL .= "app_package_module_page.en_title, app_module_page.orders ";
        $SQL .= "FROM app_module_page INNER JOIN app_package_module_page ON (app_module_page.id = app_package_module_page.page_id) ";
        $SQL .= "where app_package_module_page.app_package_module_id=$app_package_module_id ";
        $SQL .= "ORDER BY app_module_page.orders";
        $query = $this->db->query($SQL);
        return $query->result();
    }
	
    public function CheckPagesCount($app_package_module_id)
    {
        $query = $this->db->query("select id from app_package_module_page where app_package_module_id=".$app_package_module_id);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from app_package_module_page order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('app_package_module_page', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('app_package_module_page', $data); 
    }

}

?>