<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_app_module extends CI_Model {
    
	public function GetRow($id) {
        $query = $this->db->query("select * from app_module where id=".$id);
        $row = $query->row();
        return $row;
    }
	
	public function GetMain() {
        $query = $this->db->query("select * from app_module where parent_id=0 order by id asc");
        return $query->result();
    }
	
	public function GetByFolder($folder) {
        $query = $this->db->query("select id, parent_id, folder, ar_title, en_title, enabled, icon_name, 
        orders from app_module where folder='".$folder."'");
        $row = $query->row();
        return $row;
    }

    public function GetByFolderCount($folder)
    {
        $query = $this->db->query("select id, parent_id, folder, ar_title, en_title, enabled, icon_name, 
        orders from app_module where folder='".$folder."'");
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }
	
    public function GetMultiRow() {
        $query = $this->db->query("select id, parent_id, folder, ar_title, en_title, enabled, icon_name, 
        orders from app_module where enabled=1 AND parent_id=0 order by orders asc");
        return $query->result();
    }

    public function GetAllRows() {
        $query = $this->db->query("select id, parent_id, folder, ar_title, en_title, enabled, icon_name, 
        orders from app_module where enabled=1 order by id asc, parent_id asc");
        return $query->result();
    }

	public function CheckSubModulesCount($parent_id)
    {
        $query = $this->db->query("select id, parent_id, folder, ar_title, en_title, enabled, icon_name, 
        orders from app_module where parent_id=$parent_id AND  enabled=1");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

    public function GetSubModules($parent_id) {
        $query = $this->db->query("select id, parent_id, folder, ar_title, en_title, enabled, icon_name, 
        orders from app_module where parent_id=$parent_id AND  enabled=1");
        return $query->result();
    }
	
	public function GetSubCount($parent_id)
    {
        $SQL = "select * from app_module where parent_id=$parent_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }
	
	public function GetAllSub($parent_id) {
        $query = $this->db->query("select * from app_module where parent_id=$parent_id order by id asc");
        return $query->result();
    }
	
}

?>