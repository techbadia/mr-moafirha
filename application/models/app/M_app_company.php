<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_app_company extends CI_Model {
    
	public function GetRow($id) {
        $query = $this->db->query("select * from app_company where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow() {
        $query = $this->db->query("select * from app_company order by id asc");
        return $query->result();
    }

    public function GetUserCompany($id) {
        $query = $this->db->query("select * from app_company where id=$id");
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('app_company', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('app_company', $data); 
    }

    public function GetLatest($ar_title, $en_title) {
        $query = $this->db->query("select * from app_company where ar_title='$ar_title' AND en_title='$en_title' order by id desc");
        $row = $query->row();
        return $row;
    }

    public function UpdateHeader($company_id, $header) {
        $data = array(
            'header' => $header ,
        );
        $this->db->where('company_id', $company_id);
        $this->db->update('app_company', $data); 
    }

    public function UpdateFooter($company_id, $footer) {
        $data = array(
             'footer' => $footer ,
        );
        $this->db->where('company_id', $company_id);
        $this->db->update('app_company', $data); 
     }
	
}

?>