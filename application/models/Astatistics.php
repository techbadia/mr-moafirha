<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Astatistics extends CI_Model {
    
    public function Count_set_area() {
        $query = $this->db->query("select COUNT(id) as records from set_area");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_request_status() {
        $query = $this->db->query("select COUNT(id) as records from set_request_status");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_service() {
        $query = $this->db->query("select COUNT(id) as records from services");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_teamwork() {
        $query = $this->db->query("select COUNT(id) as records from set_teamwork");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_teamwork_items() {
        $query = $this->db->query("select COUNT(id) as records from set_teamwork_items");
        $row = $query->row();
        return $row;
    }
    
    public function Count_usr_users() {
        $query = $this->db->query("select COUNT(id) as records from usr_users");
        $row = $query->row();
        return $row;
    }
    
    public function Count_data_request_status($status_id) {
        $TheDate = date("Y-m-d");
        $FirstDay = date('Y-m-01', strtotime($TheDate));
        $LastDay = date('Y-m-t', strtotime($TheDate));
        $query = $this->db->query("select COUNT(id) as records from data_request where status_id=".$status_id." "
                . "AND thedate between '".$FirstDay."' AND '".$LastDay."'");
        $row = $query->row();
        return $row;
    }
    
    public function Count_data_request_set_area($area_id) {
        $TheDate = date("Y-m-d");
        $FirstDay = date('Y-m-01', strtotime($TheDate));
        $LastDay = date('Y-m-t', strtotime($TheDate));
        $query = $this->db->query("select COUNT(id) as records from data_request where area_id=".$area_id." "
                . "AND thedate between '".$FirstDay."' AND '".$LastDay."'");
        $row = $query->row();
        return $row;
    }
    
    public function Count_data_request_items_set_service($service_id) {
        $TheDate = date("Y-m-d");
        $FirstDay = date('Y-m-01', strtotime($TheDate));
        $LastDay = date('Y-m-t', strtotime($TheDate));
        $query = $this->db->query("select COUNT(data_request_items.id) as records from data_request INNER JOIN "
                . "data_request_items ON (data_request.id = data_request_items.request_id) where "
                . "service_id=".$service_id." AND data_request.thedate BETWEEN '".$FirstDay."' AND '".$LastDay."'");
        $row = $query->row();
        return $row;
    }
    
    public function Count_data_invoice_teamwork_id($teamwork_id) {
        $TheDate = date("Y-m-d");
        $FirstDay = date('Y-m-01', strtotime($TheDate));
        $LastDay = date('Y-m-t', strtotime($TheDate));
        $query = $this->db->query("select COUNT(id) as records from data_invoice where teamwork_id=".$teamwork_id." "
                . "AND thedate between '".$FirstDay."' AND '".$LastDay."'");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_provenance() {
        $query = $this->db->query("select COUNT(id) as records from set_provenance");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_period() {
        $query = $this->db->query("select COUNT(id) as records from set_period");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_mov_location() {
        $query = $this->db->query("select COUNT(id) as records from set_mov_location");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_mov_section() {
        $query = $this->db->query("select COUNT(id) as records from set_mov_section");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_mov_floor() {
        $query = $this->db->query("select COUNT(id) as records from set_mov_floor");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_mov_gallery() {
        $query = $this->db->query("select COUNT(id) as records from set_mov_gallery");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_mov_room() {
        $query = $this->db->query("select COUNT(id) as records from set_mov_room");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_mov_movetype() {
        $query = $this->db->query("select COUNT(id) as records from set_mov_movetype");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_mov_curator() {
        $query = $this->db->query("select COUNT(id) as records from set_mov_curator");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_research_method() {
        $query = $this->db->query("select COUNT(id) as records from set_research_method");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_research_researcher() {
        $query = $this->db->query("select COUNT(id) as records from set_research_researcher");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_production_maker() {
        $query = $this->db->query("select COUNT(id) as records from set_production_maker");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_acquisition_accession() {
        $query = $this->db->query("select COUNT(id) as records from set_acquisition_accession");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_acquisition_authorized() {
        $query = $this->db->query("select COUNT(id) as records from set_acquisition_authorized");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_acquisition_method() {
        $query = $this->db->query("select COUNT(id) as records from set_acquisition_method");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_acquisition_source() {
        $query = $this->db->query("select COUNT(id) as records from set_acquisition_source");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_conservation_condition() {
        $query = $this->db->query("select COUNT(id) as records from set_conservation_condition");
        $row = $query->row();
        return $row;
    }
    
public function Count_set_conservation_deterioration() {
        $query = $this->db->query("select COUNT(id) as records from set_conservation_deterioration");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_conservation_hazard() {
        $query = $this->db->query("select COUNT(id) as records from set_conservation_hazard");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_conservation_packing() {
        $query = $this->db->query("select COUNT(id) as records from set_conservation_packing");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_conservation_recommendation() {
        $query = $this->db->query("select COUNT(id) as records from set_conservation_recommendation");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_conservation_treatment() {
        $query = $this->db->query("select COUNT(id) as records from set_conservation_treatment");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_insurance_appraisal_type() {
        $query = $this->db->query("select COUNT(id) as records from set_insurance_appraisal_type");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_insurance_insurer() {
        $query = $this->db->query("select COUNT(id) as records from set_insurance_insurer");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_insurance_organization() {
        $query = $this->db->query("select COUNT(id) as records from set_insurance_organization");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_insurance_valuation_type() {
        $query = $this->db->query("select COUNT(id) as records from set_insurance_valuation_type");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_loans_lender_requester() {
        $query = $this->db->query("select COUNT(id) as records from set_loans_lender_requester");
        $row = $query->row();
        return $row;
    }
    
    public function Count_set_loans_status() {
        $query = $this->db->query("select COUNT(id) as records from set_loans_status");
        $row = $query->row();
        return $row;
    }
    
    
    
    public function Count_set_exhibition_organization() {
        $query = $this->db->query("select COUNT(id) as records from set_exhibition_organization");
        $row = $query->row();
        return $row;
    }
    
    
    public function Count_set_exhibition_venues() {
        $query = $this->db->query("select COUNT(id) as records from set_exhibition_venues");
        $row = $query->row();
        return $row;
    }
    
    public function Count_Received() {
        $query = $this->db->query("select COUNT(received) as received from catalogue where received=1");
        $row = $query->row();
        return $row;
    }
    
    public function Count_UnReceived() {
        $query = $this->db->query("select COUNT(received) as received from catalogue where received=0");
        $row = $query->row();
        return $row;
    }
    
    public function Statistics_set_committee() {
        $query = $this->db->query("SELECT set_committee.id, set_committee.ar_title, set_committee.en_title, "
                . "COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter FROM set_committee INNER JOIN set_comingfrom ON "
                . "(set_committee.id = set_comingfrom.committee_id) INNER JOIN catalogue ON "
                . "(set_comingfrom.id = catalogue.comingfromid) GROUP BY set_committee.id, "
                . "set_committee.ar_title ORDER BY records");
        return $query->result();
    }
    
    public function Statistics_set_comingfrom() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, set_comingfrom.id, "
                . "set_comingfrom.ar_title, set_comingfrom.en_title "
                . "FROM set_comingfrom LEFT OUTER JOIN catalogue ON "
                . "(set_comingfrom.id = catalogue.comingfromid) GROUP BY set_comingfrom.en_title "
                . "ORDER BY records");
        return $query->result();
    }
    
    public function Statistics_set_collection() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, set_collection.id, "
                . "set_collection.ar_title, set_collection.en_title "
                . "FROM set_collection LEFT OUTER JOIN catalogue ON "
                . "(set_collection.id = catalogue.collectionid) GROUP BY set_collection.en_title "
                . "ORDER BY records");
        return $query->result();
    }
    
    public function Statistics_set_techniques() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, set_techniques.id, "
                . "set_techniques.ar_title, set_techniques.en_title "
                . "FROM set_techniques LEFT OUTER JOIN catalogue ON "
                . "(set_techniques.id = catalogue.techniqueid) GROUP BY set_techniques.en_title "
                . "ORDER BY records");
        return $query->result();
    }
    
    public function Statistics_set_objecttype() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, set_objecttype.id, "
                . "set_objecttype.en_title, set_objecttype.ar_title "
                . "FROM set_objecttype INNER JOIN set_objectsubtype ON "
                . "(set_objecttype.id = set_objectsubtype.objecttype_id) "
                . "INNER JOIN catalogue ON (set_objectsubtype.id = catalogue.code) "
                . "GROUP BY set_objecttype.ar_title ORDER BY records");
        return $query->result();
    }
    
    public function Statistics_set_objectsubtype() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, set_objectsubtype.id, set_objectsubtype.ar_title, set_objectsubtype.en_title "
                . "FROM set_objectsubtype INNER JOIN catalogue ON (set_objectsubtype.id = catalogue.code) "
                . "GROUP BY set_objectsubtype.en_title ORDER BY records");
        return $query->result();
    }
    
    public function Statistics_set_carriedby() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, set_carriedby.id, 
            set_carriedby.ar_title, set_carriedby.en_title FROM set_carriedby INNER JOIN 
            catalogue ON (set_carriedby.id = catalogue.carriedby) GROUP BY set_carriedby.id, 
            set_carriedby.en_title ORDER BY records");
        return $query->result();
    }
    
    public function Statistics_set_cataloguefor() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, set_cataloguefor.id, set_cataloguefor.ar_title, 
            set_cataloguefor.en_title FROM set_cataloguefor INNER JOIN catalogue ON 
            (set_cataloguefor.id = catalogue.cataloguefor) GROUP BY set_cataloguefor.en_title 
            ORDER BY records");
        return $query->result();
    }
    
    public function Statistics_set_theme() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, set_theme.id, set_theme.ar_title, "
                . "set_theme.en_title FROM set_themesub INNER JOIN catalogue ON "
                . "(set_themesub.id = catalogue.topicid) INNER JOIN set_theme ON "
                . "(set_themesub.themeid = set_theme.id) GROUP BY set_theme.ar_title, "
                . "set_theme.en_title ORDER BY records");
        return $query->result();
    }
    
    public function Statistics_set_themesub() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, set_themesub.id, set_themesub.ar_title, "
                . "set_themesub.en_title FROM set_themesub LEFT OUTER JOIN catalogue ON "
                . "(set_themesub.id = catalogue.topicid) INNER JOIN set_theme ON "
                . "(set_themesub.themeid = set_theme.id) GROUP BY set_themesub.ar_title, "
                . "set_themesub.en_title ORDER BY records");
        return $query->result();
    }
    
    public function Statistics_set_material() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, set_material.id, set_material.ar_title, "
                . "set_material.en_title FROM set_material INNER JOIN catalogue ON "
                . "(set_material.id = catalogue.materialid) GROUP BY set_material.en_title ORDER BY records");
        return $query->result();
    }
    
    public function Statistics_set_provenance() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, set_provenance.id, set_provenance.ar_title, "
                . "set_provenance.en_title FROM set_provenance LEFT OUTER JOIN catalogue ON "
                . "(set_provenance.id = catalogue.provenanceid) GROUP BY set_provenance.en_title "
                . "ORDER BY records");
        return $query->result();
    }
    
    public function Statistics_set_period() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, set_period.id, set_period.ar_title, "
                . "set_period.en_title FROM set_period LEFT OUTER JOIN catalogue ON "
                . "(set_period.id = catalogue.periodid) GROUP BY set_period.en_title ORDER BY records");
        return $query->result();
    }
    
    public function Statistics_set_mov_location() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_mov_location.id, set_mov_location.ar_title, set_mov_location.en_title FROM set_mov_location LEFT "
                . "OUTER JOIN movement ON (set_mov_location.id = movement.locationid) INNER JOIN "
                . "catalogue ON (movement.id = catalogue.id) GROUP BY set_mov_location.en_title");
        return $query->result();
    }
    
    public function Statistics_set_mov_section() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_mov_section.id, set_mov_section.ar_title, set_mov_section.en_title FROM set_mov_section INNER JOIN "
                . "movement ON (set_mov_section.id = movement.sectionid) INNER JOIN catalogue ON "
                . "(movement.id = catalogue.id) GROUP BY set_mov_section.en_title");
        return $query->result();
    }
    
    public function Statistics_set_mov_floor() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_mov_floor.id, set_mov_floor.ar_title, set_mov_floor.en_title FROM movement INNER JOIN "
                . "catalogue ON (movement.id = catalogue.id) INNER JOIN set_mov_floor ON "
                . "(movement.floorid = set_mov_floor.id) GROUP BY set_mov_floor.ar_title, set_mov_floor.en_title");
        return $query->result();
    }
    
    public function Statistics_set_mov_gallery() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_mov_gallery.id, set_mov_gallery.ar_title, set_mov_gallery.en_title FROM movement "
                . "INNER JOIN catalogue ON (movement.id = catalogue.id) INNER JOIN set_mov_gallery ON "
                . "(movement.galleryid = set_mov_gallery.id) GROUP BY set_mov_gallery.id");
        return $query->result();
    }
    
    public function Statistics_set_mov_room() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_mov_room.id, set_mov_room.ar_title, set_mov_room.en_title FROM movement INNER JOIN "
                . "catalogue ON (movement.id = catalogue.id) INNER JOIN set_mov_room ON "
                . "(movement.roomid = set_mov_room.id) GROUP BY set_mov_room.id");
        return $query->result();
    }
    
    public function Statistics_set_mov_movetype() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_mov_movetype.id, set_mov_movetype.ar_title, set_mov_movetype.en_title FROM movement "
                . "INNER JOIN catalogue ON (movement.id = catalogue.id) INNER JOIN set_mov_movetype ON "
                . "(movement.movetypeid = set_mov_movetype.id) GROUP BY set_mov_movetype.id");
        return $query->result();
    }
    
    public function Statistics_set_mov_curator() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_mov_curator.id, set_mov_curator.ar_title, set_mov_curator.en_title FROM movement "
                . "INNER JOIN catalogue ON (movement.id = catalogue.id) INNER JOIN set_mov_curator ON "
                . "(movement.curatorid = set_mov_curator.id) GROUP BY set_mov_curator.id");
        return $query->result();
    }
    
    public function Statistics_set_research_method() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_research_method.id, set_research_method.ar_title, set_research_method.en_title FROM "
                . "catalogue INNER JOIN research ON (catalogue.id = research.id) INNER JOIN "
                . "set_research_method ON (research.methodid = set_research_method.id) GROUP BY "
                . "set_research_method.id");
        return $query->result();
    }
    
    public function Statistics_set_research_researcher() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_research_researcher.id, set_research_researcher.ar_title, "
                . "set_research_researcher.en_title FROM catalogue INNER JOIN research ON "
                . "(catalogue.id = research.id) INNER JOIN set_research_researcher ON "
                . "(research.researchid = set_research_researcher.id) GROUP BY set_research_researcher.id");
        return $query->result();
    }
    
    public function Statistics_set_production_maker() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_production_maker.id, set_production_maker.ar_title, set_production_maker.en_title "
                . "FROM catalogue INNER JOIN production ON (catalogue.id = production.id) INNER JOIN "
                . "set_production_maker ON (production.makerid = set_production_maker.id) GROUP BY "
                . "set_production_maker.id");
        return $query->result();
    }
    
    public function Statistics_set_acquisition_accession() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_acquisition_accession.id, set_acquisition_accession.ar_title, "
                . "set_acquisition_accession.en_title FROM catalogue INNER JOIN acquisition ON "
                . "(catalogue.id = acquisition.id) INNER JOIN set_acquisition_accession ON "
                . "(acquisition.accessionid = set_acquisition_accession.id) GROUP BY "
                . "set_acquisition_accession.id");
        return $query->result();
    }
    
    public function Statistics_set_acquisition_authorized() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_acquisition_authorized.id, set_acquisition_authorized.ar_title, "
                . "set_acquisition_authorized.en_title FROM catalogue INNER JOIN acquisition ON "
                . "(catalogue.id = acquisition.id) INNER JOIN set_acquisition_authorized ON "
                . "(acquisition.authorisorid = set_acquisition_authorized.id) GROUP BY "
                . "set_acquisition_authorized.id");
        return $query->result();
    }
    
    public function Statistics_set_acquisition_method() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_acquisition_method.id, set_acquisition_method.ar_title, set_acquisition_method.en_title "
                . "FROM catalogue INNER JOIN acquisition ON (catalogue.id = acquisition.id) INNER JOIN "
                . "set_acquisition_method ON (acquisition.methodid = set_acquisition_method.id) GROUP BY "
                . "set_acquisition_method.id");
        return $query->result();
    }
    
    public function Statistics_set_acquisition_source() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_acquisition_source.id, set_acquisition_source.ar_title, set_acquisition_source.en_title "
                . "FROM catalogue INNER JOIN acquisition ON (catalogue.id = acquisition.id) INNER JOIN "
                . "set_acquisition_source ON (acquisition.sourceid = set_acquisition_source.id) GROUP BY "
                . "set_acquisition_source.id");
        return $query->result();
    }
    
    public function Statistics_set_conservation_condition() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_conservation_condition.id, set_conservation_condition.ar_title, "
                . "set_conservation_condition.en_title FROM catalogue INNER JOIN conservation ON "
                . "(catalogue.id = conservation.id) INNER JOIN set_conservation_condition ON "
                . "(conservation.conditionid = set_conservation_condition.id) GROUP BY "
                . "set_conservation_condition.id");
        return $query->result();
    }
    
    public function Statistics_set_conservation_deterioration() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_conservation_deterioration.id, set_conservation_deterioration.ar_title, "
                . "set_conservation_deterioration.en_title FROM catalogue INNER JOIN conservation ON "
                . "(catalogue.id = conservation.id) INNER JOIN set_conservation_deterioration ON "
                . "(conservation.deteriorationid = set_conservation_deterioration.id) GROUP BY "
                . "set_conservation_deterioration.id");
        return $query->result();
    }
    
    public function Statistics_set_conservation_hazard() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_conservation_hazard.id, set_conservation_hazard.ar_title, "
                . "set_conservation_hazard.en_title FROM catalogue INNER JOIN conservation ON "
                . "(catalogue.id = conservation.id) INNER JOIN set_conservation_hazard ON "
                . "(conservation.hazardid = set_conservation_hazard.id) GROUP BY set_conservation_hazard.id");
        return $query->result();
    }
    
    public function Statistics_set_conservation_packing() {
        $query = $this->db->query("SELECT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS counter, "
                . "set_conservation_packing.id, set_conservation_packing.en_title, "
                . "set_conservation_packing.ar_title FROM catalogue INNER JOIN conservation ON "
                . "(catalogue.id = conservation.id) INNER JOIN set_conservation_packing ON "
                . "(conservation.packingid = set_conservation_packing.id) GROUP BY set_conservation_packing.id");
        return $query->result();
    }
    
    public function Statistics_set_conservation_recommendation() {
        $query = $this->db->query("SELECT DISTINCT COUNT(catalogue.id) AS records, SUM(catalogue.counter) "
                . "AS counter, set_conservation_recommendation.id, set_conservation_recommendation.ar_title, "
                . "set_conservation_recommendation.en_title FROM catalogue INNER JOIN conservation ON "
                . "(catalogue.id = conservation.id) INNER JOIN set_conservation_recommendation ON "
                . "(conservation.recomendationid = set_conservation_recommendation.id) GROUP BY "
                . "set_conservation_recommendation.id");
        return $query->result();
    }
    
    public function Statistics_set_conservation_treatment() {
        $query = $this->db->query("SELECT DISTINCT COUNT(catalogue.id) AS records, SUM(catalogue.counter) "
                . "AS counter, set_conservation_treatment.id, set_conservation_treatment.ar_title, "
                . "set_conservation_treatment.en_title FROM catalogue INNER JOIN conservation ON "
                . "(catalogue.id = conservation.id) INNER JOIN set_conservation_treatment ON "
                . "(conservation.treatmentid = set_conservation_treatment.id) GROUP BY "
                . "set_conservation_treatment.id");
        return $query->result();
    }
    
    public function Statistics_set_insurance_appraisal_type() {
        $query = $this->db->query("SELECT DISTINCT COUNT(catalogue.id) AS records, SUM(catalogue.counter) AS "
                . "counter, set_insurance_appraisal_type.id, set_insurance_appraisal_type.ar_title, "
                . "set_insurance_appraisal_type.en_title FROM catalogue INNER JOIN insurance ON "
                . "(catalogue.id = insurance.id) INNER JOIN set_insurance_appraisal_type ON "
                . "(insurance.appraisal_type_id = set_insurance_appraisal_type.id) GROUP BY "
                . "set_insurance_appraisal_type.id");
        return $query->result();
    }
    
    public function Statistics_set_insurance_insurer() {
        $query = $this->db->query("SELECT DISTINCT COUNT(catalogue.id) AS records, SUM(catalogue.counter) "
                . "AS counter, set_insurance_insurer.id, set_insurance_insurer.ar_title, "
                . "set_insurance_insurer.en_title FROM catalogue INNER JOIN insurance ON "
                . "(catalogue.id = insurance.id) INNER JOIN set_insurance_insurer ON "
                . "(insurance.insurerid = set_insurance_insurer.id) GROUP BY set_insurance_insurer.id");
        return $query->result();
    }
    
    public function Statistics_set_insurance_organization() {
        $query = $this->db->query("SELECT DISTINCT COUNT(catalogue.id) AS records, SUM(catalogue.counter) "
                . "AS counter, set_insurance_organization.id, set_insurance_organization.ar_title, "
                . "set_insurance_organization.en_title FROM catalogue INNER JOIN insurance ON "
                . "(catalogue.id = insurance.id) INNER JOIN set_insurance_organization ON "
                . "(insurance.organization_id = set_insurance_organization.id) GROUP BY "
                . "set_insurance_organization.id");
        return $query->result();
    }
    
    public function Statistics_set_insurance_valuation_type() {
        $query = $this->db->query("SELECT DISTINCT COUNT(catalogue.id) AS records, SUM(catalogue.counter) "
                . "AS counter, set_insurance_valuation_type.id, set_insurance_valuation_type.ar_title, "
                . "set_insurance_valuation_type.en_title FROM catalogue INNER JOIN insurance ON "
                . "(catalogue.id = insurance.id) INNER JOIN set_insurance_valuation_type ON "
                . "(insurance.valuation_type_id = set_insurance_valuation_type.id) GROUP BY "
                . "set_insurance_valuation_type.id");
        return $query->result();
    }
    
    public function Statistics_set_loans_lender_requester() {
        $query = $this->db->query("SELECT DISTINCT COUNT(catalogue.id) AS records, SUM(catalogue.counter) "
                . "AS counter, set_loans_lender_requester.id, set_loans_lender_requester.ar_title, "
                . "set_loans_lender_requester.en_title FROM catalogue INNER JOIN loans ON "
                . "(catalogue.id = loans.id) INNER JOIN set_loans_lender_requester ON "
                . "(loans.lenderrequester = set_loans_lender_requester.id) GROUP BY "
                . "set_loans_lender_requester.id");
        return $query->result();
    }
    
    public function Statistics_set_loans_status() {
        $query = $this->db->query("SELECT DISTINCT COUNT(catalogue.id) AS records, SUM(catalogue.counter) "
                . "AS counter, set_loans_status.id, set_loans_status.ar_title, set_loans_status.en_title "
                . "FROM catalogue INNER JOIN loans ON (catalogue.id = loans.id) INNER JOIN set_loans_status ON "
                . "(loans.`status` = set_loans_status.id), set_loans_status set_loans_status1 GROUP BY "
                . "set_loans_status.id");
        return $query->result();
    }
    
    public function Statistics_set_exhibition_name() {
        $query = $this->db->query("SELECT DISTINCT COUNT(catalogue.id) AS records, "
                . "SUM(catalogue.counter) AS counter, set_exhibition_name.id, set_exhibition_name.ar_title, "
                . "set_exhibition_name.en_title FROM set_exhibition_name INNER JOIN exhibition ON "
                . "(set_exhibition_name.id = exhibition.exhibitionid) INNER JOIN catalogue ON "
                . "(exhibition.id = catalogue.id) GROUP BY set_exhibition_name.id");
        return $query->result();
    }
    
    public function Statistics_set_exhibition_organization() {
        $query = $this->db->query("SELECT DISTINCT COUNT(catalogue.id) AS records, SUM(catalogue.counter) "
                . "AS counter, set_exhibition_organization.id, set_exhibition_organization.ar_title, "
                . "set_exhibition_organization.en_title FROM exhibition INNER JOIN catalogue ON "
                . "(exhibition.id = catalogue.id) INNER JOIN set_exhibition_organization ON "
                . "(exhibition.organizerid = set_exhibition_organization.id) GROUP BY "
                . "set_exhibition_organization.id");
        return $query->result();
    }
    
    public function Statistics_set_exhibition_venues() {
        $query = $this->db->query("SELECT DISTINCT COUNT(catalogue.id) AS records, SUM(catalogue.counter) "
                . "AS counter, set_exhibition_venues.id, set_exhibition_venues.ar_title, "
                . "set_exhibition_venues.en_title FROM exhibition INNER JOIN catalogue ON "
                . "(exhibition.id = catalogue.id) INNER JOIN set_exhibition_venues ON "
                . "(exhibition.venueid = set_exhibition_venues.id) GROUP BY set_exhibition_venues.id");
        return $query->result();
    }
    
    public function GetTableRows($table, $id)
    {
        $query = $this->db->query("select id from ".$table." where id=".$id);
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function GetLibraryRows($period, $object_type, $theme, $material)
    {
        $query = $this->db->query("select period, object_type, theme, material from library where "
                . "period LIKE '%".$period."%' OR object_type LIKE '%".$object_type."%' OR "
                . "theme LIKE '%".$theme."%' OR material LIKE '%".$material."%'");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function GetEducationRows($period, $object_type, $theme, $material)
    {
        $query = $this->db->query("select period, object_type, theme, material from education where "
                . "period LIKE '%".$period."%' OR object_type LIKE '%".$object_type."%' OR "
                . "theme LIKE '%".$theme."%' OR material LIKE '%".$material."%'");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
}

?>