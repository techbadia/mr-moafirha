<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sub_centers extends CI_Model {

    public function GetRow($id) {
        $query = $this->db->query("select * from sub_centers where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetAnotheStores($id) {
        //$company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sub_centers where id <> $id order by id asc");
        return $query->result();
    }

    public function GetMultiRow() {

        //$company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sub_centers where parent_id = ".$_GET['main']." order by id asc");
        return $query->result();
    }
    public function GetMultiRowCenter($id) {

        //$company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from sub_centers where parent_id = ".$id." order by id asc");
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('sub_centers', $data);
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('sub_centers', $data);
    }
    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('sub_centers');
    }

    public function StoreCount($brand_id)
    {
        //$company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from product where brand_id=$brand_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

}

?>
