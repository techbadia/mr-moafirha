<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_usr_trace extends CI_Model {
    
	public function GetRow($id) {
        $query = $this->db->query("select * from usr_trace where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function Get_all() {
        $query = $this->db->query("select * from usr_trace order by id asc");
        return $query->result();
    }

    public function Get_Mine() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from usr_trace where user_id=$StaffID order by id asc");
        return $query->result();
    }

    
    public function InsertRecord($data) {
        $this->db->insert('usr_trace', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('usr_trace', $data); 
    }
}

?>