<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_usr_usersgroup extends CI_Model {
    
    public function GetRow($ID) {
        $query = $this->db->query("select * from usr_usersgroup where id=".$ID);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $query = $this->db->query("select * from usr_usersgroup order by id asc");
        return $query->result();
    }
    
    public function GetPdfRow($group_id) {
        $query = $this->db->query("SELECT usr_usersgroup.id, usr_usersgroup.ar_title, 
        usr_usersgroup.en_title, 
        app_module_pages.title AS page_title, 
        usr_usersprivileges.enabled_add, usr_usersprivileges.enabled_edit, usr_usersprivileges.enabled_delete, 
        usr_usersprivileges.enabled_view FROM usr_usersgroup INNER JOIN usr_usersprivileges ON 
        (usr_usersgroup.id = usr_usersprivileges.group_id) INNER JOIN app_module_pages ON 
        (usr_usersprivileges.page_id = app_module_pages.id) INNER JOIN app_module ON 
        (app_module_pages.module_id = app_module.id) WHERE usr_usersgroup.id =".$group_id." AND 
        app_module_pages.enabled = '1' ORDER BY app_module.id, app_module_pages.orders ASC");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('usr_usersgroup', $data); 
    }
    
    public function UpdateRecord($id, $ar_title, $en_title) {
        $data = array(
            'ar_title' => $ar_title,
            'en_title' => $en_title
        );
            
        $this->db->where('id', $id);
        $this->db->update('usr_usersgroup', $data); 
        //$this->db->cache_delete('usr_usersgroup');
    }
    
    public function CheckDuplicate($ar_title)
    {
        $query = $this->db->query("select id, ar_title from usr_usersgroup where 
        ar_title='".$ar_title."'");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function WhatsID($ar_title) {
        //$company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select id, ar_title from usr_usersgroup 
        where ar_title='".$ar_title."'");
        $row = $query->row();
        return $row;
    }

    public function UpdateDelete($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('usr_usersgroup', $data); 
    }
    
}

?>