<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_usr_users extends CI_Model {
    
    public function GetRow($ID) {
        $query = $this->db->query("select * from usr_users where id=".$ID);
        $row = $query->row();
        return $row;
    }
    
    public function GetUserRow($username, $password)
    {
        $query = $this->db->query("select id, company_id, group_id, fullname, email, username, 
        password, active, image, branches_lookup from usr_users where username='".$username."' AND password='".$password."' AND 
        active='True'");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function CheckUser($username, $password) {
        $query = $this->db->query("select id, company_id, group_id, fullname, email, username, 
        password, active, image, branches_lookup from usr_users where username='".$username."' AND password='".$password."' AND 
        active='True'");
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $query = $this->db->query("select * from usr_users order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('usr_users', $data); 
		//$this->db->cache_delete('usr_users');
    }
    
    public function UpdateRecord1($id, $data) {
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update('usr_users', $data); 
    }

    public function UpdateRecord($id, $group_id, $fullname, $email, $username, $password, $active, $branches_lookup, $branch, $image)
    {
        $data = array();
        if (($password !="") && ($image !=""))
        {
            $data = array(
                'group_id' => $group_id ,
                'fullname' => $fullname ,
                'email' => $email ,
                'username' => $username ,
                'password' => $password ,
                'active' => $active,
                'branches_lookup' => $branches_lookup,
				'branch' => $branch,
                'image' => $image
            );
        }
        if (($password !="") && ($image ==""))
        {
            $data = array(
                'group_id' => $group_id ,
                'fullname' => $fullname ,
                'email' => $email ,
                'username' => $username ,
                'password' => $password ,
                'active' => $active,
                'branches_lookup' => $branches_lookup,
				'branch' => $branch,
            );
        }
        if (($password =="") && ($image !=""))
        {
            $data = array(
                'group_id' => $group_id ,
                'fullname' => $fullname ,
                'email' => $email ,
                'username' => $username ,
                'active' => $active,
                'branches_lookup' => $branches_lookup,
				'branch' => $branch,
                'image' => $image
            );
        }
        if (($password =="") && ($image ==""))
        {
            $data = array(
                'group_id' => $group_id ,
                'fullname' => $fullname ,
                'email' => $email ,
                'username' => $username ,
                'active' => $active,
                'branches_lookup' => $branches_lookup,
				'branch' => $branch,
            );
        }
        $this->db->where('id', $id);
        $this->db->update('usr_users', $data); 
    }
    
    public function UpdateWithoutPassword($id, $group_id, $fullname, $email, $username, $active, $branches_lookup, $branch, $image) {
       
       $data = array(
            'group_id' => $group_id ,
            'fullname' => $fullname ,
            'email' => $email ,
            'username' => $username ,
            'active' => $active,
            'branches_lookup' => $branches_lookup,
			'branch' => $branch,
            'image' => $image
        );
            
        $this->db->where('id', $id);
        $this->db->update('usr_users', $data); 
    }

    public function UpdateDelete($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('usr_users', $data); 
    }
    
    public function CheckDuplicate($username, $fullname)
    {
        $query = $this->db->query("select id, username, fullname from usr_users where 
        username='".$username."' OR fullname='".$fullname."'");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

    public function GroupCount($group_id)
    {
        $query = $this->db->query("select id, group_id from usr_users where 
        group_id=$group_id");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
}

?>