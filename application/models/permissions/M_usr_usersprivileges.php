<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_usr_usersprivileges extends CI_Model {
    
    public function InsertRecord($data) {
        $this->db->insert('usr_usersprivileges', $data); 
    }

    public function GetData($group_id, $page_id) {
        $query = $this->db->query("select * from usr_usersprivileges where group_id=$group_id AND page_id=$page_id");
        $row = $query->row();
        return $row;
    }
    
    public function UpdateRecord($id, $enabled_add, $enabled_edit, $enabled_delete, 
            $enabled_view, $enabled_export) {
       if ($enabled_add =="")
        {
            $enabled_add = 0;
        }
        if ($enabled_edit =="")
        {
            $enabled_edit = 0;
        }
        if ($enabled_delete =="")
        {
            $enabled_delete = 0;
        }
        if ($enabled_view =="")
        {
            $enabled_view = 0;
        }
        if ($enabled_export =="")
        {
            $enabled_export = 0;
        }
       $data = array(
            'enabled_add' => $enabled_add,
            'enabled_edit' => $enabled_edit,
            'enabled_delete' => $enabled_delete,
            'enabled_view' => $enabled_view,
            'enabled_export' => $enabled_export
        );
            
        $this->db->where('id', $id);
        $this->db->update('usr_usersprivileges', $data); 
		//$this->db->cache_delete('usr_usersprivileges');
    }
    
    
    public function GetGroupRow($group_id) {
        $query = $this->db->query("select id, group_id, page_id, enabled_add, enabled_edit, enabled_delete, 
            enabled_view, enabled_export from usr_usersprivileges where group_id=".$group_id." order by id asc");
        return $query->result();
    }
    
    public function CheckView($group_id, $page_id)
    {
        $query = $this->db->query("select id, company_id, group_id, page_id, enabled_add, enabled_edit, enabled_delete, 
            enabled_view, enabled_export from 
        usr_usersprivileges where group_id=$group_id AND page_id=$page_id AND enabled_view=1");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function CheckExport($group_id, $page_id)
    {
        $query = $this->db->query("select id, group_id, page_id, enabled_add, enabled_edit, enabled_delete, 
            enabled_view, enabled_export from 
        usr_usersprivileges where group_id=".$group_id." AND page_id=".$page_id." AND enabled_export=1");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function CheckAdd($group_id, $page_id)
    {
        $query = $this->db->query("select id, group_id, page_id, enabled_add, enabled_edit, enabled_delete, enabled_view from 
        usr_usersprivileges where group_id=".$group_id." AND page_id=".$page_id." AND enabled_add=1");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function CheckEdit($group_id, $page_id)
    {
        $query = $this->db->query("select id, group_id, page_id, enabled_add, enabled_edit, enabled_delete, enabled_view from 
        usr_usersprivileges where group_id=".$group_id." AND page_id=".$page_id." AND enabled_edit=1");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
    public function CheckDelete($group_id, $page_id)
    {
        $query = $this->db->query("select id, group_id, page_id, enabled_add, enabled_edit, enabled_delete, enabled_view from 
        usr_usersprivileges where group_id=".$group_id." AND page_id=".$page_id." AND enabled_delete=1");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    
}

?>