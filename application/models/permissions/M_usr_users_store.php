<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_usr_users_store extends CI_Model {
    
	public function GetRow($id) {
        $query = $this->db->query("select * from usr_users_store where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetForUser() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from usr_users_store where user_id=$StaffID order by id asc");
        return $query->result();
    }

    public function CheckStore($user_id, $store_id)
    {
        $query = $this->db->query("select id, user_id, store_id from usr_users_store where 
        store_id=$store_id AND user_id=$user_id");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

    public function Get_Mine() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from usr_users_store where user_id=$StaffID order by id asc");
        return $query->result();
    }

    
    public function InsertRecord($data) {
        $this->db->insert('usr_users_store', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('usr_users_store', $data); 
    }
}

?>