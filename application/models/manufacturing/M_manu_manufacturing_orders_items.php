<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_manu_manufacturing_orders_items extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from manu_manufacturing_orders_items where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetAnotheStores($id) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from manu_manufacturing_orders_items where company_id=$company_id AND id <> $id order by id asc");
        return $query->result();
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from manu_manufacturing_orders_items where company_id=$company_id order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('manu_manufacturing_orders_items', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('manu_manufacturing_orders_items', $data); 
    }

    public function GetCount($store_type_id)
    {
        $SQL = "SELECT SUM(`quantity`) as quantity FROM `manu_manufacturing_orders_items` where store_type_id=$store_type_id";
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }
    
}

?>