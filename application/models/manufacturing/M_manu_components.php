<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_manu_components extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from manu_components where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetLatestRecord($company_id, $user_id) {
        $query = $this->db->query("select * from manu_components where company_id=$company_id AND user_id=$user_id order by id desc");
        $row = $query->row();
        return $row;
    }

    public function GetAnotheStores($id) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from manu_components where company_id=$company_id AND id <> $id order by id asc");
        return $query->result();
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from manu_components where company_id=$company_id order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('manu_components', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('manu_components', $data); 
    }

    public function GetCount($country_id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from hr_employee where company_id=$company_id AND country_id=$country_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function GetInvoiceHeader($id) {
        $SQL = "SELECT manu_components.id, manu_components.company_id, manu_components.user_id, 
        manu_components.thedate, manu_components.over_cost, manu_components.notes FROM 
        manu_components WHERE manu_components.id =".$id;
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }

    public function GetInvoiceDetails($id) {
        $SQL = "SELECT manu_components_items.id, manu_components_items.bill_id, manu_components_items.store_type_id, 
        product.title, product.title_en, manu_components_items.quantity, manu_components_items.unitprice, 
        (manu_components_items.quantity * manu_components_items.unitprice) AS subtotal FROM product INNER JOIN 
        manu_components_items ON (product.id = manu_components_items.store_type_id) WHERE manu_components_items.bill_id =".$id;
        $query = $this->db->query($SQL);
        return $query->result();
    }
    
}

?>