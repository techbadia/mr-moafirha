<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_bill extends CI_Model
{
    public function GetAllProductByStore($StoreId){
        $this->db->select('product.id,barcode,title,title_en,details,details_en,price,image,catalogue,quantity,store_id');
        $this->db->from('product');
        $this->db->where('store_id', $StoreId);
        $this->db->join('store_quantity', 'store_quantity.product_id = product.id');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;

    }
    public function GetProductByBarcode($StoreId,$barcode){
        $this->db->select('product.id,barcode,title,title_en,details,details_en,price,image,catalogue,quantity,store_id');
        $this->db->from('product');
        $this->db->where('store_id', $StoreId);
        $this->db->where('barcode', $barcode);
        $this->db->join('store_quantity', 'store_quantity.product_id = product.id');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;

    }

    public function GetProductByID($StoreId,$id){
        $this->db->select('product.id,barcode,title,title_en,details,details_en,price,image,catalogue,quantity,store_id');
        $this->db->from('product');
        $this->db->where('store_id', $StoreId);
        $this->db->where('product.id', $id);
        $this->db->join('store_quantity', 'store_quantity.product_id = product.id');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;

    }


    public function InsertRecord($data) {
        $this->db->insert('sale_bill', $data);
        return  $this->db->insert_id();
    }
    public function fin_journal_main_insert($data) {
        $this->db->insert('fin_journal_main', $data);
        return  $this->db->insert_id();
    }
    public function fin_journal_insert($data) {
        $this->db->insert('fin_journal', $data);
    }


}