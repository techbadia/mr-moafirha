<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_account extends CI_Model {
    
    public function GetUserByToken($token){
        $this->db->select('*');
        $this->db->from('usr_token');
        $this->db->where('token', $token);
        $this->db->where('is_active', 1);
        $this->db->join('usr_users', 'usr_users.id = usr_token.usr_user_id');
        $this->db->join('usr_users_store', 'usr_users.id = usr_users_store.user_id');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
        
    }

    public function inset_token($data){
         $this->db->insert('usr_token', $data);
        
    }
    
    
    public function logout($id){
         $data =array(
             'end_dt'=>date('Y/m/d H:i:s'),
             'is_active'=>'0'
             );
         $this->db->where('usr_token_id', $id);
        $this->db->update('usr_token', $data);
    }
    
    
    public function GetUsers($id){
        $this->db->select('id,fullname');
        $this->db->from('usr_users');
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
        
    }
    
    public function GetCustomer(){
        $this->db->select('id,ar_title,en_title');
        $this->db->from('sale_customer');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
        
    }

    public function GetCustomerById($id){
        $this->db->select('*');
        $this->db->from('sale_customer');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->result_array();
        if (count($result))
        return $result['0'];
        else  return $result;

    }

    
    
    public function MessageSent($id){
        $this->db->select('serv_message.*,usr_users.fullname');
        $this->db->from('serv_message');
        $this->db->join('usr_users', 'usr_users.id = serv_message.to_user');
        $this->db->where('from_user', $id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
        
    }
    
    
    
    public function MessageReceive($id){
        $this->db->select('serv_message.*,usr_users.fullname');
        $this->db->from('serv_message');
        $this->db->join('usr_users', 'usr_users.id = serv_message.from_user');
        $this->db->where('to_user', $id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
        
    }
    
    

    public function inset_Message($data){
         $this->db->insert('serv_message', $data);
        
    }

    public function fin_settings($company_id) {
        $this->db->select('*');
        $this->db->from('fin_settings');
        $this->db->where('company_id', $company_id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;

    }
    public function GetMain_Sub($id,$company_id)
    {
        $this->db->select('id,account_no,title,title_en');
        $this->db->from('fin_treeaccount');
        $this->db->where('company_id', $company_id);
        $this->db->where('id', $id)
        ->or_where('parent_id', $id);
        $this->db->order_by("id", "asc");
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;

    }
    

}

?>