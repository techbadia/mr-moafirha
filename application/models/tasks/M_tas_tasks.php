<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_tas_tasks extends CI_Model {
    
	public function GetRow($id) {
        $query = $this->db->query("select * from tas_tasks where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from tas_tasks where company_id=$company_id order by id asc");
        return $query->result();
    }

    public function GetLatestRow($from_user) {
        $query = $this->db->query("select * from tas_tasks where from_user=".$from_user);
        $row = $query->row();
        return $row;
    }
   
    public function InsertRecord($data) {
        $this->db->insert('tas_tasks', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('tas_tasks', $data); 
    }

    public function another_onhold() {
        $company_id = intval($this->session->userdata('company_id'));
        $StaffID = intval($this->session->userdata('StaffID'));
        $SQL = "SELECT tas_tasks_type1_levels.id, tas_tasks_type1_levels.task_id, tas_tasks.from_user, tas_tasks_type1_levels.to_user, ";
        $SQL .= "tas_tasks_type1_levels.details, tas_tasks_type1_levels.start_date, tas_tasks_type1_levels.start_time, tas_tasks_type1_levels.end_date, ";
        $SQL .= "tas_tasks_type1_levels.end_time, tas_tasks_type1_levels.user_start_date, tas_tasks_type1_levels.user_start_time, ";
        $SQL .= "tas_tasks_type1_levels.user_end_date, tas_tasks_type1_levels.user_end_time, tas_tasks_type1_levels.`status` FROM tas_tasks ";
        $SQL .= "INNER JOIN tas_tasks_type1_levels ON (tas_tasks.id = tas_tasks_type1_levels.task_id) where tas_tasks_type1_levels.`status` = 0 ";
        $SQL .= "AND tas_tasks.from_user=".$StaffID;
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function another_process() {
        $company_id = intval($this->session->userdata('company_id'));
        $StaffID = intval($this->session->userdata('StaffID'));
        $SQL = "SELECT tas_tasks_type1_levels.id, tas_tasks_type1_levels.task_id, tas_tasks.from_user, tas_tasks_type1_levels.to_user, ";
        $SQL .= "tas_tasks_type1_levels.details, tas_tasks_type1_levels.start_date, tas_tasks_type1_levels.start_time, tas_tasks_type1_levels.end_date, ";
        $SQL .= "tas_tasks_type1_levels.end_time, tas_tasks_type1_levels.user_start_date, tas_tasks_type1_levels.user_start_time, ";
        $SQL .= "tas_tasks_type1_levels.user_end_date, tas_tasks_type1_levels.user_end_time, tas_tasks_type1_levels.`status` FROM tas_tasks ";
        $SQL .= "INNER JOIN tas_tasks_type1_levels ON (tas_tasks.id = tas_tasks_type1_levels.task_id) where tas_tasks_type1_levels.`status` = 1 ";
        $SQL .= "AND tas_tasks.from_user=".$StaffID;
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function another_late() {
        $company_id = intval($this->session->userdata('company_id'));
        $StaffID = intval($this->session->userdata('StaffID'));
        $SQL = "SELECT tas_tasks_type1_levels.id, tas_tasks_type1_levels.task_id, tas_tasks.from_user, tas_tasks_type1_levels.to_user, ";
        $SQL .= "tas_tasks_type1_levels.details, tas_tasks_type1_levels.start_date, tas_tasks_type1_levels.start_time, tas_tasks_type1_levels.end_date, ";
        $SQL .= "tas_tasks_type1_levels.end_time, tas_tasks_type1_levels.user_start_date, tas_tasks_type1_levels.user_start_time, ";
        $SQL .= "tas_tasks_type1_levels.user_end_date, tas_tasks_type1_levels.user_end_time, tas_tasks_type1_levels.`status` FROM tas_tasks ";
        $SQL .= "INNER JOIN tas_tasks_type1_levels ON (tas_tasks.id = tas_tasks_type1_levels.task_id) where tas_tasks_type1_levels.`status` = 1 ";
        $SQL .= "AND tas_tasks.from_user=".$StaffID." AND tas_tasks_type1_levels.user_end_date > tas_tasks_type1_levels.end_date";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function another_failed() {
        $company_id = intval($this->session->userdata('company_id'));
        $StaffID = intval($this->session->userdata('StaffID'));
        $SQL = "SELECT tas_tasks_type1_levels.id, tas_tasks_type1_levels.task_id, tas_tasks.from_user, tas_tasks_type1_levels.to_user, ";
        $SQL .= "tas_tasks_type1_levels.details, tas_tasks_type1_levels.start_date, tas_tasks_type1_levels.start_time, tas_tasks_type1_levels.end_date, ";
        $SQL .= "tas_tasks_type1_levels.end_time, tas_tasks_type1_levels.user_start_date, tas_tasks_type1_levels.user_start_time, ";
        $SQL .= "tas_tasks_type1_levels.user_end_date, tas_tasks_type1_levels.user_end_time, tas_tasks_type1_levels.`status` FROM tas_tasks ";
        $SQL .= "INNER JOIN tas_tasks_type1_levels ON (tas_tasks.id = tas_tasks_type1_levels.task_id) where tas_tasks_type1_levels.`status` = 2 ";
        $SQL .= "AND tas_tasks.from_user=".$StaffID;
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function another_cancel() {
        $company_id = intval($this->session->userdata('company_id'));
        $StaffID = intval($this->session->userdata('StaffID'));
        $SQL = "SELECT tas_tasks_type1_levels.id, tas_tasks_type1_levels.task_id, tas_tasks.from_user, tas_tasks_type1_levels.to_user, ";
        $SQL .= "tas_tasks_type1_levels.details, tas_tasks_type1_levels.start_date, tas_tasks_type1_levels.start_time, tas_tasks_type1_levels.end_date, ";
        $SQL .= "tas_tasks_type1_levels.end_time, tas_tasks_type1_levels.user_start_date, tas_tasks_type1_levels.user_start_time, ";
        $SQL .= "tas_tasks_type1_levels.user_end_date, tas_tasks_type1_levels.user_end_time, tas_tasks_type1_levels.`status` FROM tas_tasks ";
        $SQL .= "INNER JOIN tas_tasks_type1_levels ON (tas_tasks.id = tas_tasks_type1_levels.task_id) where tas_tasks_type1_levels.`status` = 3 ";
        $SQL .= "AND tas_tasks.from_user=".$StaffID;
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function another_finish() {
        $company_id = intval($this->session->userdata('company_id'));
        $StaffID = intval($this->session->userdata('StaffID'));
        $SQL = "SELECT tas_tasks_type1_levels.id, tas_tasks_type1_levels.task_id, tas_tasks.from_user, tas_tasks_type1_levels.to_user, ";
        $SQL .= "tas_tasks_type1_levels.details, tas_tasks_type1_levels.start_date, tas_tasks_type1_levels.start_time, tas_tasks_type1_levels.end_date, ";
        $SQL .= "tas_tasks_type1_levels.end_time, tas_tasks_type1_levels.user_start_date, tas_tasks_type1_levels.user_start_time, ";
        $SQL .= "tas_tasks_type1_levels.user_end_date, tas_tasks_type1_levels.user_end_time, tas_tasks_type1_levels.`status` FROM tas_tasks ";
        $SQL .= "INNER JOIN tas_tasks_type1_levels ON (tas_tasks.id = tas_tasks_type1_levels.task_id) where tas_tasks_type1_levels.`status` = 4 ";
        $SQL .= "AND tas_tasks.from_user=".$StaffID;
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function mytask_onhold()
    {
        $company_id = intval($this->session->userdata('company_id'));
        $StaffID = intval($this->session->userdata('StaffID'));
        $SQL = "SELECT id, task_id, to_user, details, start_date, start_time, end_date, end_time, status FROM tas_tasks_type1_levels ";
        $SQL .= " where status=0 AND to_user=".$StaffID;
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function mytask_process()
    {
        $company_id = intval($this->session->userdata('company_id'));
        $StaffID = intval($this->session->userdata('StaffID'));
        $SQL = "SELECT id, task_id, to_user, details, start_date, start_time, end_date, end_time, status FROM tas_tasks_type1_levels ";
        $SQL .= " where status=1 AND to_user=".$StaffID;
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function mytask_late()
    {
        $company_id = intval($this->session->userdata('company_id'));
        $StaffID = intval($this->session->userdata('StaffID'));
        $SQL = "SELECT id, task_id, to_user, details, start_date, start_time, end_date, end_time, status, user_start_date ";
        $SQL .= ", user_start_time, user_end_date, user_end_time FROM tas_tasks_type1_levels where to_user=".$StaffID;
        $SQL .= " AND user_end_date > end_date";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function mytask_failed()
    {
        $company_id = intval($this->session->userdata('company_id'));
        $StaffID = intval($this->session->userdata('StaffID'));
        $SQL = "SELECT id, task_id, to_user, details, start_date, start_time, end_date, end_time, status, user_start_date, ";
        $SQL .= "user_start_time, user_end_date, user_end_time FROM tas_tasks_type1_levels ";
        $SQL .= " where status=2 AND to_user=".$StaffID;
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function mytask_cancel()
    {
        $company_id = intval($this->session->userdata('company_id'));
        $StaffID = intval($this->session->userdata('StaffID'));
        $SQL = "SELECT id, task_id, to_user, details, start_date, start_time, end_date, end_time, status, user_start_date, ";
        $SQL .= "user_start_time, user_end_date, user_end_time FROM tas_tasks_type1_levels ";
        $SQL .= " where status=3 AND to_user=".$StaffID;
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function mytask_finish()
    {
        $company_id = intval($this->session->userdata('company_id'));
        $StaffID = intval($this->session->userdata('StaffID'));
        $SQL = "SELECT id, task_id, to_user, details, start_date, start_time, end_date, end_time, status, user_start_date, ";
        $SQL .= "user_start_time, user_end_date, user_end_time FROM tas_tasks_type1_levels ";
        $SQL .= " where status=4 AND to_user=".$StaffID;
        $query = $this->db->query($SQL);
        return $query->result();
    }
}

?>