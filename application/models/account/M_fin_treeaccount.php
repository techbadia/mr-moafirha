<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_fin_treeaccount extends CI_Model
{

     public function GetRow($id)
    {
        $query = $this->db->query("select * from fin_treeaccount where id=" . $id);
        $row = $query->row();
        return $row;
    }


    public function GetRowSon($id)
    {
        $query = $this->db->query("select sum(startamount) as startamount,sum(startamount) as startamount2 from fin_treeaccount where parent_id=" . $id);
        $row = $query->row();
        return $row;
    }


    public function CheckRow($id)
    {
        $query = $this->db->query("select * from fin_treeaccount where id=" . $id);
        $RecordCount = 0;
        if ($query->num_rows() >= 1) {
            $RecordCount = $query->num_rows();
        } else {
            $RecordCount = 0;
        }
        return $RecordCount;
    }

    public function FindLatestRow($title, $title_en)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from fin_treeaccount where
        title='" . $title . "' AND title_en='" . $title_en . "'  order by id desc");
        $row = $query->row();
        return $row;
    }

    public function GetParentList()
    {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from fin_treeaccount where parent_id>0  order by id asc");
        return $query->result();
    }
    public function GetMultiRow2()
    {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from fin_treeaccount   order by account_no asc");

        $result = $query->result();
        return $result;
    }
    public function GetMultiRow($level_no = 4)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $level_no = ($level_no === false) ? false : (int)$level_no;
        $and_level = ($level_no !== false) ?  "where level_no=$level_no" : '';
        $query = $this->db->query("select * from fin_treeaccount  $and_level order by id asc");

        $result = $query->result();
        if ($result) {
            $ids=array();
            foreach ($result as $key => $row) {
                if (in_array($row->account_no, $ids)) {
                    unset($result[$key]);
                    $this->db->query("DELETE FROM fin_treeaccount where id=$row->id");
                    continue;
                }
                $ids[] = $row->account_no;
            }
        }
        return $result;
    }

    public function GetBasicAccounts()
    {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from fin_treeaccount where  basic=1 order by id asc");
        return $query->result();
    }

    public function GetDeleted()
    {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from fin_treeaccount where  deleted=1 order by id asc");
        return $query->result();
    }

    public function GetMain_Sub($id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from fin_treeaccount where
          id=$id OR   parent_id=$id order by id asc");
        return $query->result();
    }

    public function GetSubAccounts($id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from fin_treeaccount where
       parent_id=$id order by id asc");
        return $query->result();
    }

    public function GetSubAccountsCount($account_id)
    {
        $SQL = "select * from fin_treeaccount where parent_id=$account_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
        if ($query->num_rows() >= 1) {
            $RecordCount = $query->num_rows();
        } else {
            $RecordCount = 0;
        }
        return $RecordCount;
    }

    public function GetByCategory($category_id)
    {
      $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from fin_treeaccount where
        category_id=$category_id order by id asc");
        return $query->result();
    }

    public function GetByLevel($level_no)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from fin_treeaccount where
   level_no=$level_no order by id asc");
        return $query->result();
    }

    public function GetSubByLevel($level_no, $parent_id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from fin_treeaccount where
       level_no=$level_no AND parent_id=$parent_id order by account_no asc");
        return $query->result();
    }


    public function GetMaxAccount($id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select max(account_no) as max from fin_treeaccount where
       parent_id=$id ");
        return $query->result();
    }

   public function InsertRecord($data)
    {
        $this->db->insert('fin_treeaccount', $data);
    }

    public function UpdateRecord($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('fin_treeaccount', $data);
    }

    public function DeleteForever($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('fin_treeaccount');
    }

    public function DeleteRecords($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('fin_treeaccount', $data);
    }

    public function GetAccountID($ar_title)
    {
        $query = $this->db->query("select * from fin_treeaccount where title='$ar_title'");
        $row = $query->row();
        return $row;
    }

    public function CheckLoanFind($account_id)
    {
        $SQL = "select * from fin_treeaccount where loan_user_id=$account_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
        if ($query->num_rows() >= 1) {
            $RecordCount = $query->num_rows();
        } else {
            $RecordCount = 0;
        }
        return $RecordCount;
    }

    public function GetLoanAccount($account_id)
    {
        $query = $this->db->query("select * from fin_treeaccount where loan_user_id=" . $account_id);
        $row = $query->row();
        return $row;
    }

    public function CheckCustodyFind($account_id)
    {
        $SQL = "select * from fin_treeaccount where custody_user_id=$account_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
        if ($query->num_rows() >= 1) {
            $RecordCount = $query->num_rows();
        } else {
            $RecordCount = 0;
        }
        return $RecordCount;
    }

    public function GetCustodyAccount($account_id)
    {
        $query = $this->db->query("select * from fin_treeaccount where custody_user_id=" . $account_id);
        $row = $query->row();
        return $row;
    }

    public function GetByAccountNo($account_no){
        $query = $this->db->query("select * from fin_treeaccount where account_no=" . $account_no);
        $row = $query->row();
        return $row;
    }


    public function fin_treeaccount_by_level($level)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from fin_treeaccount where
        company_id=$company_id AND level_no=$level  order by account_no asc");
        return $query->result();
    }
}
