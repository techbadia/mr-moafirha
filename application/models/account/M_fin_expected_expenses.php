<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_fin_expected_expenses extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from fin_expected_expenses where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from fin_expected_expenses where company_id=$company_id order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('fin_expected_expenses', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('fin_expected_expenses', $data); 
    }

    public function CheckDupicate($account_id)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from fin_expected_expenses where company_id=$company_id AND account_id=$account_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }
    
}

?>