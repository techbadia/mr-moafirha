<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_fin_journal extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from fin_journal where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow($main_id) {
        $query = $this->db->query("select * from fin_journal where main_id=$main_id order by id asc");
        return $query->result();
    }

    public function GetModalDetails() {
        $CurrentJournalID = 0;
        $CurrentJournalID = intval($this->session->userdata('Fin_journal_View_ID'));
        $query = $this->db->query("select * from fin_journal where main_id=$CurrentJournalID order by id asc");
        return $query->result();
    }
    
    public function GetRowDetails($main_id) {
        $SQL = "SELECT fin_journal.id, fin_journal.main_id, fin_journal.account_id, 
        fin_treeaccount.title, fin_treeaccount.title_en, fin_journal.debit, fin_journal.creditor 
        FROM fin_treeaccount INNER JOIN fin_journal ON (fin_treeaccount.id = fin_journal.account_id) 
        WHERE fin_journal.main_id =".$main_id." order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }
    

    public function InsertRecord($data) {
        $this->db->insert('fin_journal', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('fin_journal', $data); 
    }

    public function DeleteRecords($main_id, $data) {
        $this->db->where('main_id', $main_id);
        $this->db->update('fin_journal', $data); 
    }

    public function GetSum_Debit($account_id) {
        $query = $this->db->query("select SUM(debit) as debit from fin_journal where account_id=".$account_id);
        $row = $query->row();
        return $row;
    }

    public function GetSum_Creditor($account_id) {
        $query = $this->db->query("select SUM(creditor) as creditor from fin_journal where account_id=".$account_id);
        $row = $query->row();
        return $row;
    }

    public function GetSum($account_id)
    {
        $SQL = "select * from fin_journal where account_id=$account_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        
    }
    
    public function GetDetailsByAccount($account_id) {
        $SQL = "SELECT * from fin_journal where account_id =$account_id order by id asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetAccountActions($account_id) {
        $query = $this->db->query("select * from fin_journal where account_id=$account_id order by id asc");
        return $query->result();
    }

    public function GetRealtedJournalStartDate($accountid, $fromdate) {
        $SQL = "SELECT * FROM fin_journal_main INNER JOIN fin_journal ON 
        (fin_journal_main.id = fin_journal.main_id) WHERE fin_journal.account_id = $accountid 
        AND fin_journal_main.thedate < '$fromdate'";

        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetRealtedJournalStartDateCount($accountid, $fromdate) {
        $SQL = "SELECT * FROM fin_journal_main INNER JOIN fin_journal ON 
        (fin_journal_main.id = fin_journal.main_id) WHERE fin_journal.account_id = $accountid 
        AND fin_journal_main.thedate < '$fromdate'";

        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }
}

?>