<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_fin_treecategory extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from fin_treecategory where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $query = $this->db->query("select * from fin_treecategory order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('fin_treecategory', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('fin_treecategory', $data); 
    }
    
}

?>