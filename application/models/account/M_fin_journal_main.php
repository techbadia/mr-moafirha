<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_fin_journal_main extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from fin_journal_main where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetRow_bill_id_table_name($bill_id, $table_name) {
        $query = $this->db->query("select * from fin_journal_main where bill_id=".$bill_id." AND table_name='$table_name'");
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from fin_journal_main where company_id=$company_id order by id asc");
        return $query->result();
    }

    public function GetByTable($table_name) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from fin_journal_main where 
        company_id=$company_id AND table_name='$table_name' order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('fin_journal_main', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('fin_journal_main', $data); 
    }
    

    public function DelRecord($id) {

       $this->db->where('id', $id);
        $this->db->delete('fin_journal_main');
        
         $this->db->where('main_id', $id);
        $this->db->delete('fin_journal');

    }

    public function DeleteRecords($bill_id, $table_name, $data) {
        $this->db->where('bill_id', $bill_id);
        $this->db->where('table_name', $table_name);
        $this->db->update('fin_journal_main', $data); 
    }
    
    public function GetLatestRecord($company_id, $user_id) {
        $SQL = "select * from fin_journal_main where company_id=$company_id AND user_id=$user_id order by id desc";
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }

    public function GetDelivery($delivery_id)
    {
        $SQL = "select * from fin_journal_main where delivery_id=$delivery_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function GetAccountAction($acount_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "SELECT fin_treeaccount.id, fin_treeaccount.company_id, fin_treeaccount.title, fin_treeaccount.title_en, ";
        $SQL .= "fin_journal_main.details, fin_journal_main.thedate, fin_journal.debit, fin_journal.creditor FROM ";
        $SQL .= "fin_journal_main INNER JOIN fin_journal ON (fin_journal_main.id = fin_journal.main_id) INNER JOIN ";
        $SQL .= "fin_treeaccount ON (fin_journal.account_id = fin_treeaccount.id) WHERE fin_treeaccount.id = $acount_id AND ";
        $SQL .= "fin_treeaccount.company_id = $company_id";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetAccountAction_RangDate($acount_id, $fromdate, $todate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "SELECT fin_treeaccount.id, fin_treeaccount.company_id, fin_treeaccount.title,fin_journal.main_id, fin_treeaccount.title_en, ";
        $SQL .= "fin_journal_main.details, fin_journal_main.thedate, fin_journal.debit, fin_journal.creditor FROM ";
        $SQL .= "fin_journal_main INNER JOIN fin_journal ON (fin_journal_main.id = fin_journal.main_id) INNER JOIN ";
        $SQL .= "fin_treeaccount ON (fin_journal.account_id = fin_treeaccount.id) WHERE fin_treeaccount.id = $acount_id AND ";
        $SQL .= "fin_treeaccount.company_id = $company_id AND fin_journal_main.thedate between '$fromdate' AND '$todate' ORDER BY fin_journal_main.thedate";
        $query = $this->db->query($SQL);
        return $query->result();
    }


    public function GetProject_earning($project_id, $treasury_id, $bank_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "SELECT fin_treeaccount.id, fin_treeaccount.company_id, fin_treeaccount.title, fin_treeaccount.title_en, ";
        $SQL .= "fin_journal_main.details, fin_journal_main.thedate, fin_journal.debit, fin_journal.creditor FROM fin_journal_main INNER JOIN fin_journal ON ";
        $SQL .= "(fin_journal_main.id = fin_journal.main_id) INNER JOIN fin_treeaccount ON (fin_journal.account_id = fin_treeaccount.id) ";
        $SQL .= "WHERE fin_treeaccount.company_id=$company_id AND fin_journal.project_id=$project_id ";
        $SQL .= "AND fin_journal.account_id=$treasury_id AND fin_journal.debit > 0 OR ";
        $SQL .= "fin_treeaccount.company_id=$company_id AND fin_journal.project_id=$project_id ";
        $SQL .= "AND fin_journal.account_id=$bank_id AND fin_journal.debit > 0";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetProject_expenses($project_id, $fin_treeaccount_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "SELECT fin_treeaccount.id, fin_treeaccount.company_id, fin_treeaccount.title, fin_treeaccount.title_en, ";
        $SQL .= "fin_journal_main.details, fin_journal_main.thedate, fin_journal.debit, fin_journal.creditor, fin_journal.project_id ";
        $SQL .= "FROM fin_journal_main INNER JOIN fin_journal ON (fin_journal_main.id = fin_journal.main_id) INNER JOIN ";
        $SQL .= "fin_treeaccount ON (fin_journal.account_id = fin_treeaccount.id) WHERE fin_treeaccount.company_id = $company_id AND ";
        $SQL .= "fin_journal.project_id = $project_id AND fin_journal.account_id = $fin_treeaccount_id AND fin_journal.debit > 0";
        $query = $this->db->query($SQL);
        return $query->result();
    }

}

?>