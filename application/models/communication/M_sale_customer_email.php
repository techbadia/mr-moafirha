<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sale_customer_email extends CI_Model {
	
    public function GetMultiRow() {
        $query = $this->db->query("select * from sale_customer_email order by id asc");
        return $query->result();
    }

    public function GetToday() {
        $Today = date("Y-m-d");
        $query = $this->db->query("select * from sale_customer_email where readed=0 or 
        thedate='$Today' order by id asc");
        return $query->result();
    }

    public function GetRow($id) {
        $SQL = "SELECT sale_customer_email.id, sale_customer_email.customer_id, sale_customer.ar_title, sale_customer.en_title, ";
        $SQL .= "sale_customer_email.thedate, sale_customer_email.thetime, sale_customer_email.subject, sale_customer_email.message, ";
        $SQL .= "sale_customer_email.readed, sale_customer.phone FROM sale_customer INNER JOIN sale_customer_email ON ";
        $SQL .= "(sale_customer.id = sale_customer_email.customer_id) where sale_customer_email.id=$id";
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('sale_customer_email', $data); 
    }
}

?>