<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sale_customer_schedule extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from sale_customer_schedule where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow() {
        $query = $this->db->query("select * from sale_customer_schedule order by id asc");
        return $query->result();
    }

    public function GetByCustomer($customer_id) {
        $query = $this->db->query("select * from sale_customer_schedule where customer_id=$customer_id order by id asc");
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('sale_customer_schedule', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('store', $data); 
    }
}

?>