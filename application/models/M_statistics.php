<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_statistics extends CI_Model {
    
    public function CheckTableID($id, $table) {
        $SQL = "select * from $table where id=$id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function TotalUsers() {
        $SQL = "select * from usr_users where active='True'";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function TotalCustomers() {
        $SQL = "select * from sale_customer";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function TotalSuppliers() {
        $SQL = "select * from buy_manufacturer";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function GetTotalSales() {
        $Today = date("Y-m-d");
        if($this->session->userdata('statistics_filter') == "Today")
        {
            $StartData = $Today;
            $EndDate = $Today;
        }
        else if($this->session->userdata('statistics_filter') == "Month")
        {
            $StartData = date('Y-m-01', strtotime($Today));
            $EndDate = date('Y-m-t', strtotime($Today));
        }
        else
        {
            $StartData = date('Y-01-01', strtotime($Today));
            $EndDate = date('Y-12-31', strtotime($Today));
        }
        
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select SUM(totalvalue) as totalvalue from sale_bill where thedate between '$StartData' AND '$EndDate' 
        AND company_id=$company_id");
        $row = $query->row();
        return $row;
    }

    public function GetTotalSales_Paid() {
        $Today = date("Y-m-d");
        if($this->session->userdata('statistics_filter') == "Today")
        {
            $StartData = $Today;
            $EndDate = $Today;
        }
        else if($this->session->userdata('statistics_filter') == "Month")
        {
            $StartData = date('Y-m-01', strtotime($Today));
            $EndDate = date('Y-m-t', strtotime($Today));
        }
        else
        {
            $StartData = date('Y-01-01', strtotime($Today));
            $EndDate = date('Y-12-31', strtotime($Today));
        }
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select SUM(paid) as paid from sale_bill where thedate between '$StartData' AND '$EndDate' 
        AND company_id=$company_id");
        $row = $query->row();
        return $row;
    }

    public function GetTotalSales_Remaining() {
        $Today = date("Y-m-d");
        if($this->session->userdata('statistics_filter') == "Today")
        {
            $StartData = $Today;
            $EndDate = $Today;
        }
        else if($this->session->userdata('statistics_filter') == "Month")
        {
            $StartData = date('Y-m-01', strtotime($Today));
            $EndDate = date('Y-m-t', strtotime($Today));
        }
        else
        {
            $StartData = date('Y-01-01', strtotime($Today));
            $EndDate = date('Y-12-31', strtotime($Today));
        }
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select SUM(remaining) as remaining from sale_bill where thedate between '$StartData' AND '$EndDate' 
        AND company_id=$company_id");
        $row = $query->row();
        return $row;
    }

    public function GetTotalSales_Month($StartData, $EndDate) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select SUM(totalvalue) as totalvalue from sale_bill where thedate between '$StartData' AND '$EndDate' 
        AND company_id=$company_id");
        $row = $query->row();
        return $row;
    }

    public function GetTotalSales_Paid_Month($StartData, $EndDate) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select SUM(paid) as paid from sale_bill where thedate between '$StartData' AND '$EndDate' 
        AND company_id=$company_id");
        $row = $query->row();
        return $row;
    }

    public function GetTotalSales_Remaining_Month($StartData, $EndDate) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select SUM(remaining) as remaining from sale_bill where thedate between '$StartData' AND '$EndDate' 
        AND company_id=$company_id");
        $row = $query->row();
        return $row;
    }

    public function GetTotalPurchase() {
        $Today = date("Y-m-d");
        if($this->session->userdata('statistics_filter') == "Today")
        {
            $StartData = $Today;
            $EndDate = $Today;
        }
        else if($this->session->userdata('statistics_filter') == "Month")
        {
            $StartData = date('Y-m-01', strtotime($Today));
            $EndDate = date('Y-m-t', strtotime($Today));
        }
        else
        {
            $StartData = date('Y-01-01', strtotime($Today));
            $EndDate = date('Y-12-31', strtotime($Today));
        }
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select SUM(totalvalue) as totalvalue from buy_bill where thedate between '$StartData' AND '$EndDate' 
        AND company_id=$company_id");
        $row = $query->row();
        return $row;
    }

    public function GetTotalPurchase_Paid() {
        $Today = date("Y-m-d");
        if($this->session->userdata('statistics_filter') == "Today")
        {
            $StartData = $Today;
            $EndDate = $Today;
        }
        else if($this->session->userdata('statistics_filter') == "Month")
        {
            $StartData = date('Y-m-01', strtotime($Today));
            $EndDate = date('Y-m-t', strtotime($Today));
        }
        else
        {
            $StartData = date('Y-01-01', strtotime($Today));
            $EndDate = date('Y-12-31', strtotime($Today));
        }
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select SUM(paid) as paid from buy_bill where thedate between '$StartData' AND '$EndDate' 
        AND company_id=$company_id");
        $row = $query->row();
        return $row;
    }

    public function GetTotalPurchase_Remaining() {
        $Today = date("Y-m-d");
        if($this->session->userdata('statistics_filter') == "Today")
        {
            $StartData = $Today;
            $EndDate = $Today;
        }
        else if($this->session->userdata('statistics_filter') == "Month")
        {
            $StartData = date('Y-m-01', strtotime($Today));
            $EndDate = date('Y-m-t', strtotime($Today));
        }
        else
        {
            $StartData = date('Y-01-01', strtotime($Today));
            $EndDate = date('Y-12-31', strtotime($Today));
        }
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select SUM(remaining) as remaining from buy_bill where thedate between '$StartData' AND '$EndDate' 
        AND company_id=$company_id");
        $row = $query->row();
        return $row;
    }

    public function GetTotalPurchase_Month($StartData, $EndDate) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select SUM(totalvalue) as totalvalue from buy_bill where thedate between '$StartData' AND '$EndDate' 
        AND company_id=$company_id");
        $row = $query->row();
        return $row;
    }

    public function GetTotalPurchase_Paid_Month($StartData, $EndDate) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select SUM(paid) as paid from buy_bill where thedate between '$StartData' AND '$EndDate' 
        AND company_id=$company_id");
        $row = $query->row();
        return $row;
    }

    public function GetTotalPurchase_Remaining_Month($StartData, $EndDate) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select SUM(remaining) as remaining from buy_bill where thedate between '$StartData' AND '$EndDate' 
        AND company_id=$company_id");
        $row = $query->row();
        return $row;
    }

    //revenues
    public function GetTotalService_Revenues($StartData, $EndDate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "SELECT data_invoice_items.qunatity, data_invoice_items.unit_price FROM data_invoice INNER JOIN data_invoice_items ON ";
        $SQL .= "(data_invoice.id = data_invoice_items.invoice_id) WHERE data_invoice.thedate BETWEEN '$StartData' AND '$EndDate' ";
        $SQL .= " AND data_invoice.company_id = $company_id";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetTotalService_Expenses($StartData, $EndDate) {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "SELECT SUM(data_invoice_expenses.amount) AS amount FROM data_invoice INNER JOIN data_invoice_expenses ON ";
        $SQL .= "(data_invoice.id = data_invoice_expenses.invoice_id) WHERE data_invoice_expenses.thedate BETWEEN '$StartData' ";
        $SQL .= "AND '$EndDate' AND data_invoice.company_id = $company_id";
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }


}

?>