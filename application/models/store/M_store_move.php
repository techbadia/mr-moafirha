<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_store_move extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from store_move where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from store_move where company_id=$company_id order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('store_move', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('store_move', $data); 
    }
    
}

?>