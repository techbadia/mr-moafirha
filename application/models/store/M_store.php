<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_store extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from store where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetLatestRow($title, $email, $phone) {
        $query = $this->db->query("select * from store where title='$title', email='$email', phone='$phone'");
        $row = $query->row();
        return $row;
    }

    public function GetAnotheStores($id) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from store where id <> $id order by id asc");
        return $query->result();
    }
    
    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from store order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('store', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('store', $data); 
    }

    public function StoreCount()
    {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from store";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }
    
}

?>