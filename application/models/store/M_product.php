<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_product extends CI_Model {

    public function GetRow($id) {
        $query = $this->db->query("select * from product where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetByBarcode($barcode) {
        $query = $this->db->query("select * from product where barcode='".$barcode."'");
        $row = $query->row();
        return $row;
    }
    public function GetByTitle($title) {
        $query = $this->db->query("select * from product where title='".$title."'");
        $row = $query->row();
        return $row;
    }

    public function GetLatestProduct($category_id, $barcode, $title) {
        $query = $this->db->query("select * from product where category_id=$category_id AND barcode='$barcode' AND title='$title' order by id desc");
        $row = $query->row();
        return $row;
    }

    public function CheckBarcode($barcode) {
        $SQL = "select * from product where barcode='".$barcode."'";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function GetMultiRow() {
        $query = $this->db->query("select * from product order by id asc");
        return $query->result();
    }

    public function GetUnFinished() {
        $query = $this->db->query("select * from product where finished=0 order by id asc");
        return $query->result();
    }

    public function GetFinished() {
        $query = $this->db->query("select * from product where finished=0 order by id asc");
        return $query->result();
    }


    public function InsertRecord($data) {
        $this->db->insert('product', $data);
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('product', $data);
    }

    public function ProductByBrand($brand_id) {
        $SQL = "select * from product where brand_id=".$brand_id;
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function ProductByCategory($category_id) {
        $SQL = "select * from product where category_id=".$category_id." OR category1_id=".$category_id;
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function GetProductArray($postData=array()) {
        $response = array();

        if(isset($postData['barcode']) ){
            // Select record
            $this->db->select('*');
            $this->db->where('barcode', $postData['barcode']);
            $records = $this->db->get('product');
            $response = $records->result_array();
        }
        return $response;
    }

    public function GetTopSale_Quantity() {
        $SQL = "SELECT product.id, product.barcode, product.title, product.title_en, ";
        $SQL .= "sale_bill_items.quantity, sale_bill_items.unitprice FROM product INNER JOIN sale_bill_items ";
        $SQL .= "ON (product.id = sale_bill_items.store_type_id) ORDER BY sale_bill_items.quantity DESC limit 50";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function GetTopSale_Price() {
        $SQL = "SELECT product.id, product.barcode, product.title, product.title_en, ";
        $SQL .= "sale_bill_items.quantity, sale_bill_items.unitprice FROM product INNER JOIN sale_bill_items ";
        $SQL .= "ON (product.id = sale_bill_items.store_type_id) ORDER BY sale_bill_items.unitprice DESC limit 50";
        $query = $this->db->query($SQL);
        return $query->result();
    }
}

?>
