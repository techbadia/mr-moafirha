<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_store_category extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from store_category where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetAnotheStores($id) {
        //$company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from store_category where id <> $id order by id asc");
        return $query->result();
    }
    
    public function GetMultiRow() {
        //$company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from store_category order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('store_category', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('store_category', $data); 
    }

    public function StoreCount($category_id)
    {
        //$company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from product where category_id=$category_id";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }
    
}

?>