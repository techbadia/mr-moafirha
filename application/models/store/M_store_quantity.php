<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_store_quantity extends CI_Model {

    public function GetRow($id) {
        $query = $this->db->query("select * from store_quantity where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetStoreTotalQunatity($store_id) {
        $query = $this->db->query("SELECT count(quantity) as quantity FROM store_quantity WHERE store_id=".$store_id);
        $row = $query->row();
        return $row;
    }

    public function GetStoreProduct($store_id, $product_id) {
        $query = $this->db->query("select * from store_quantity where
        store_id=".$store_id." AND product_id=".$product_id);
        // var_dump($query);die();
        $row = $query->row();
        return $row;
    }

    public function CountStoreProduct($store_id, $product_id) {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from store_quantity where
        store_id=".$store_id." AND product_id=".$product_id);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function GetByShop() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from store_quantity where company_id=".$company_id);
        $row = $query->row();
        return $row;
    }

    public function CountByShop() {
        $company_id = intval($this->session->userdata('company_id'));
        $SQL = "select * from store_quantity where company_id=".$company_id;
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function GetTotalQuantity($product_id) {
        //$company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select SUM(quantity) AS quantity from store_quantity where
         product_id=".$product_id);
        $row = $query->row();
        return $row;
    }

    public function GetTotalQuantityByStore($store_id, $product_id) {
        //$company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select id, quantity, store_id, product_id from store_quantity where store_id=$store_id AND product_id=".$product_id);
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow() {
        //$company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from store_quantity order by id asc");
        return $query->result();
    }

    public function GetQuantityLessZero() {
        //$company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from store_quantity where quantity<0 order by id asc");
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('store_quantity', $data);
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('store_quantity', $data);
    }

    public function CountFillStores($Store_id, $Product_id) {
        $SQL = "";
        if ($this->session->userdata('lang') == "ar")
        {
            $SQL = "SELECT store.id, product.barcode, product.title, store.title AS store_title, ";
            $SQL .= "store_quantity.quantity FROM store INNER JOIN store_quantity ON (store.id = store_quantity.store_id) ";
            $SQL .= "INNER JOIN product ON (store_quantity.product_id = product.id) WHERE store.id=$Store_id AND product.id = $Product_id GROUP BY store.id";
        }
        else
        {
            $SQL = "SELECT store.id, product.barcode, product.title_en, store.title_en AS store_title, ";
            $SQL .= "store_quantity.quantity FROM store INNER JOIN store_quantity ON (store.id = store_quantity.store_id) ";
            $SQL .= "INNER JOIN product ON (store_quantity.product_id = product.id) WHERE store.id=$Store_id AND product.id = $Product_id GROUP BY store.id";
        }
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    }

    public function FillStores($Store_id, $Product_id)
    {
        $SQL = "";
        if ($this->session->userdata('lang') == "ar")
        {
            $SQL = "SELECT store.id, product.barcode, product.title, store.title AS store_title, ";
            $SQL .= "store_quantity.quantity FROM store INNER JOIN store_quantity ON (store.id = store_quantity.store_id) ";
            $SQL .= "INNER JOIN product ON (store_quantity.product_id = product.id) WHERE store.id=$Store_id AND product.id = $Product_id GROUP BY store.id";
        }
        else
        {
            $SQL = "SELECT store.id, product.barcode, product.title_en, store.title_en AS store_title, ";
            $SQL .= "store_quantity.quantity FROM store INNER JOIN store_quantity ON (store.id = store_quantity.store_id) ";
            $SQL .= "INNER JOIN product ON (store_quantity.product_id = product.id) WHERE store.id=$Store_id AND product.id = $Product_id GROUP BY store.id";
        }
        $query = $this->db->query($SQL);
        $row = $query->row();
        return $row;
    }

    public function CheckIfHaveQuantity($type_id, $location_id)
    {
        $query = $this->db->query("select id, type_id, location_id, quantity from store_quantity where
        type_id=".$type_id." AND location_id=".$location_id);

        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
}

?>
