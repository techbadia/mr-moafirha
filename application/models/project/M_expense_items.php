<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_expense_items extends CI_Model {

	public function GetRow($id) {
        $query = $this->db->query("select * from expense_items where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetAll() {
        $query = $this->db->query("select * from expense_items");
        return $query->result();
    }

    public function GetByProject_Building($project_id) {
        $query = $this->db->query("select * from expense_items where sub_project_id=$project_id group by building_no order by id asc");
        return $query->result();
    }
		public function GetForSubProject($project_id) {
				$StaffID = intval($this->session->userdata('StaffID'));
				$TodayDate = date("Y-m-d");
				$query = $this->db->query("select * from proj_project_files where sub_project_id=$project_id order by id asc");
				return $query->result();
		}
    public function GetByProject_Floor($project_id) {
        $query = $this->db->query("select * from expense_items where project_id=$project_id group by floor_no order by id asc");
        return $query->result();
    }

    public function GetByProject_Room($project_id) {
        $query = $this->db->query("select * from expense_items where project_id=$project_id group by room_no order by id asc");
        return $query->result();
    }

    public function GetByRoom($room_no) {
        $query = $this->db->query("select * from expense_items where room_no='$room_no' order by id asc");
        return $query->result();
    }

    public function InsertRecord($data) {
       $this->db->insert('expense_items', $data);
       return    $insert_id = $this->db->insert_id();

    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('expense_items', $data);
    }
    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('expense_items');
    }

}

?>
