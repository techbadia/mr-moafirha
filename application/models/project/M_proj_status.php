<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_proj_status extends CI_Model {
    
	public function GetRow($id) {
        $query = $this->db->query("select * from proj_status where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $query = $this->db->query("select * from proj_status order by id asc");
        return $query->result();
    }

    public function Get_Mine() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $TodayDate = date("Y-m-d");
        $query = $this->db->query("select * from proj_status where to_user=$StaffID AND end_date >= '$TodayDate' order by id asc");
        return $query->result();
    }

    
    public function InsertRecord($data) {
        $this->db->insert('proj_status', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('proj_status', $data); 
    }

    public function PendingActions()
    {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from proj_status where to_user=$StaffID AND done=0");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
}

?>