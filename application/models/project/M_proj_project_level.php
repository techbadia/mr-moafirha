<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_proj_project_level extends CI_Model {
    
	public function GetRow($id) {
        $query = $this->db->query("select * from proj_project_level where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $query = $this->db->query("select * from proj_project_level order by id asc");
        return $query->result();
    }

    public function GetForProject($project_id) {
        $StaffID = intval($this->session->userdata('StaffID'));
        $TodayDate = date("Y-m-d");
        $query = $this->db->query("select * from proj_project_level where project_id=$project_id order by id asc");
        return $query->result();
    }

    public function Get_all() {
        $query = $this->db->query("select * from proj_project_level order by id asc");
        return $query->result();
    }

    public function Get_Mine() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $TodayDate = date("Y-m-d");
        $query = $this->db->query("select * from proj_project_level where to_user=$StaffID AND end_date >= '$TodayDate' order by id asc");
        return $query->result();
    }

    
    public function InsertRecord($data) {
        $this->db->insert('proj_project_level', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('proj_project_level', $data); 
    }

    public function PendingActions()
    {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from proj_project_level where to_user=$StaffID AND done=0");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

    public function GetCountByProject($project_id)
    {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from proj_project_level where project_id=$project_id");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
}

?>