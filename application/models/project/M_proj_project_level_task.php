<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_proj_project_level_task extends CI_Model {
    
	public function GetRow($id) {
        $query = $this->db->query("select * from proj_project_level_task where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function Get_all() {
        $query = $this->db->query("select * from proj_project_level_task order by id asc");
        return $query->result();
    }

    public function GetMultiRow() {
        $query = $this->db->query("select * from proj_project_level_task order by id asc");
        return $query->result();
    }

    public function GetForProject($project_id) {
        $query = $this->db->query("select * from proj_project_level_task where project_id=$project_id order by id asc");
        return $query->result();
    }

    public function GetUsersByProject($project_id) {
        $query = $this->db->query("select * from proj_project_level_task where project_id=$project_id group by user_id order by id asc limit 3");
        return $query->result();
    }

    public function GetCountUsersByProject($project_id)
    {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from proj_project_level_task where project_id=$project_id group by user_id order by id asc");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

    public function Get_Mine() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $TodayDate = date("Y-m-d");
        $query = $this->db->query("select * from proj_project_level_task where user_id=$StaffID AND 
        end_date >= '$TodayDate' AND status_id>1 AND status_id<5 order by id asc");
        return $query->result();
    }

    
    public function InsertRecord($data) {
        $this->db->insert('proj_project_level_task', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('proj_project_level_task', $data); 
    }

    public function GetCountByProject($project_id)
    {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from proj_project_level_task where project_id=$project_id");
        
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
}

?>