<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_proj_project_devices extends CI_Model {
    
	public function GetRow($id) {
        $query = $this->db->query("select * from proj_project_devices where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetByProject($project_id) {
        $query = $this->db->query("select * from proj_project_devices where project_id=$project_id order by id asc");
        return $query->result();
    }

    public function GetByProject_Building($project_id) {
        $query = $this->db->query("select * from proj_project_devices where project_id=$project_id group by building_no order by id asc");
        return $query->result();
    }

    public function GetByProject_Floor($project_id) {
        $query = $this->db->query("select * from proj_project_devices where project_id=$project_id group by floor_no order by id asc");
        return $query->result();
    }

    public function GetByProject_Room($project_id) {
        $query = $this->db->query("select * from proj_project_devices where project_id=$project_id group by room_no order by id asc");
        return $query->result();
    }

    public function GetByRoom($room_no) {
        $query = $this->db->query("select * from proj_project_devices where room_no='$room_no' order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('proj_project_devices', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('proj_project_devices', $data); 
    }

}

?>