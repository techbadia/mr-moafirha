<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_proj_project extends CI_Model {

	public function GetRow($id) {
        $query = $this->db->query("select * from proj_project where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetLatest($customer_id, $status_id, $manager_id) {
        $query = $this->db->query("select * from proj_project where
        customer_id=$customer_id AND status_id=$status_id AND manager_id=$manager_id order by id desc");
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from proj_project where company_id=$company_id order by id asc");
        return $query->result();
    }

    public function Get_Mine() {
        $StaffID = intval($this->session->userdata('StaffID'));
        $TodayDate = date("Y-m-d");
        $query = $this->db->query("select * from proj_project where to_user=$StaffID AND end_date >= '$TodayDate' order by id asc");
        return $query->result();
    }


    public function InsertRecord($data) {
        $this->db->insert('proj_project', $data);
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('proj_project', $data);
    }
    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('proj_project'); 
    }

    public function PendingActions()
    {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from proj_project where to_user=$StaffID AND done=0");

        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
}

?>
