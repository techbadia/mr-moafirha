<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_proj_project_files extends CI_Model {

	public function GetRow($id) {
        $query = $this->db->query("select * from proj_project_files where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow() {
        $query = $this->db->query("select * from proj_project_files order by id asc");
        return $query->result();
    }

    public function GetForProject($project_id) {
        $StaffID = intval($this->session->userdata('StaffID'));
        $TodayDate = date("Y-m-d");
        $query = $this->db->query("select * from proj_project_files where project_id=$project_id and sub_project_id IS NULL order by id asc");
        return $query->result();
    }
    public function GetForSubProject($project_id) {
        $StaffID = intval($this->session->userdata('StaffID'));
        $TodayDate = date("Y-m-d");
        $query = $this->db->query("select * from proj_project_files where sub_project_id=$project_id order by id asc");
        return $query->result();
    }

    public function GetForLevel($level_id) {
        $StaffID = intval($this->session->userdata('StaffID'));
        $TodayDate = date("Y-m-d");
        $query = $this->db->query("select * from proj_project_files where level_id=$level_id order by id asc");
        return $query->result();
    }

    public function GetForTask($task_id) {
        $StaffID = intval($this->session->userdata('StaffID'));
        $TodayDate = date("Y-m-d");
        $query = $this->db->query("select * from proj_project_files where task_id=$task_id order by id asc");
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('proj_project_files', $data);
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('proj_project_files', $data);
    }

    public function PendingActions()
    {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from proj_project_files where to_user=$StaffID AND done=0");

        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }

    public function GetFileCount($project_id)
    {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from proj_project_files where project_id=$project_id and sub_project_id IS NULL");
        // var_dump($project_id);die($project_id);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
    public function GetSubFileCount($project_id)
    {
        $StaffID = intval($this->session->userdata('StaffID'));
        $query = $this->db->query("select * from proj_project_files where sub_project_id=$project_id");

        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
            {
                $RecordCount = 0;
            }
        return $RecordCount;
    }
}

?>
