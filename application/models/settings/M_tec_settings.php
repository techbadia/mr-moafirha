<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_tec_settings extends CI_Model {

    public function GetRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from tec_settings where setting_id=1");
        $row = $query->row();
        return $row;
    }

    public function InsertRecord($data) {
        $this->db->insert('tec_settings', $data);
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('setting_id', $id);
        $this->db->update('tec_settings', $data);
    }

}

?>
