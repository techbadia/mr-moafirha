<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_settings_notifications extends CI_Model {
    
    public function GetRow() {
        $company_id = intval($this->session->userdata('company_id'));
        $query = $this->db->query("select * from settings_notifications where company_id=$company_id");
        $row = $query->row();
        return $row;
    }

    public function InsertRecord($data) {
        $this->db->insert('settings_notifications', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('settings_notifications', $data); 
    }
    
}

?>