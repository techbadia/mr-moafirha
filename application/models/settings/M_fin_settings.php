<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_fin_settings extends CI_Model {

    public function GetRow() {
        $company_id = intval($this->session->userdata('company_id'));
        // var_dump($company_id);die();
        $query = $this->db->query("select * from fin_settings where company_id=$company_id");
        $row = $query->row();
        return $row;
    }

    public function InsertRecord($data) {
        $this->db->insert('fin_settings', $data);
    }

    public function UpdateRecord($id, $data) {
      $query = $this->db->query("select * from fin_settings");
      $rows = $query->result();
      foreach ($rows as $row) {
        // code...
        $this->db->where('id', $row->id);
        $this->db->update('fin_settings', $data);
      }
    }

}

?>
