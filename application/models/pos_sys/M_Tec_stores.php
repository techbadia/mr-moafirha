<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Tec_stores extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from tec_stores where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function FindByName($name) {
        $query = $this->db->query("select * from tec_stores where name='$name.'");
        $row = $query->row();
        return $row;
    }

    public function CheckByTitle($name) {
        $SQL = "select * from tec_stores where name='".$name."'";
        $query = $this->db->query($SQL);
        $RecordCount = 0;
         if ($query->num_rows() >= 1)
         {
            $RecordCount = $query->num_rows();
         }
         else
         {
                $RecordCount = 0;
         }
        return $RecordCount ;
    } 
    
    public function GetMultiRow() {
        $query = $this->db->query("select * from tec_stores order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('tec_stores', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('tec_stores', $data); 
    }
    
}

?>