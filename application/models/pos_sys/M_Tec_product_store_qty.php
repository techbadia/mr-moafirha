<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Tec_product_store_qty extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from tec_product_store_qty where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $query = $this->db->query("select * from tec_product_store_qty order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('tec_product_store_qty', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('tec_product_store_qty', $data); 
    }
    
}

?>