<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Tec_products extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from tec_products where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $query = $this->db->query("select * from tec_products order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('tec_products', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('tec_products', $data); 
    }
    
}

?>