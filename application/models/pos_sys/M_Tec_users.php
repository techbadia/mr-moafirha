<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Tec_users extends CI_Model {
    
    public function GetRow($id) {
        $query = $this->db->query("select * from tec_users where id=".$id);
        $row = $query->row();
        return $row;
    }
    
    public function GetMultiRow() {
        $query = $this->db->query("select * from tec_users order by id asc");
        return $query->result();
    }

    public function GetStoreUsers($store_id) {
        $query = $this->db->query("select * from tec_users where store_id=$store_id order by id asc");
        return $query->result();
    }
    
    public function InsertRecord($data) {
        $this->db->insert('tec_users', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('tec_users', $data); 
    }
    
}

?>