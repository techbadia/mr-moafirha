<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_app_element extends CI_Model {
    
    public function GetApp() {
        $query = $this->db->query("select * from app");
        $row = $query->row();
        return $row;
    }

    public function GetRow($id) {
        $query = $this->db->query("select * from app_element where id=".$id);
        $row = $query->row();
        return $row;
    }

    public function GetMultiRow() {
        $query = $this->db->query("select * from app_element order by id asc");
        return $query->result();
    }

    public function InsertRecord($data) {
        $this->db->insert('app_element', $data); 
    }

    public function UpdateRecord($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('app_element', $data); 
    }
}

?>