<?php
$lang['Dashboard'] = "اللوحة الرئيسية";
$lang['Branches'] = "الفروع";
$lang['Branch'] = "الفرع";
$lang['ChangeHeader'] = "صورة الهيدر";
$lang['ChangeFooter'] = "صورة الفوتر";
$lang['AddBrancheMessage'] = "بعد تسجيل الفرع الجديد يجب أن تقوم بضبط اعدادات الحسابات وإعدادات القبول وإعدادات التنبيهات ويجب رفع صورة الهيدر والفوتر للمطبوعات";
$lang['Open'] = "فتح";
$lang['Date'] = "التاريخ";
$lang['Name \ phone \ mobile \ email \ P name \ P mobile'] = "الاسم / الهاتف / الجوال / البريد الالكتروني / اسم المسئول / هاتف المسئول";
$lang['Number'] = "رقم السند";
$lang['company_id'] = "الفرع";
$lang['Site Name'] = "اسم الشركة";
$lang['AppTitle'] = "نظام تخطيط الموارد";
$lang['sub_center_code'] = "اسم الملف";
$lang['Description'] = "الوصف";
$lang['Client'] = "العميل";
$lang['Main_account'] = "الحساب الرئيسي";
$lang['Sub_account'] = "الحساب الفرعي";
$lang['AddNew'] = "إضافة";
$lang['Net'] = "الباقي";
$lang['actions'] = "العمليات";
$lang['supplier'] = "المورد";
$lang['Account name / client / supplier'] = "اسم الحساب / العميل / المورد";
$lang['sub_center_id'] = "مركز التكلفة";
$lang['sub_center'] = "مركز التكلفة الفرعي";
$lang['AddRecord'] = "إضافة سجل";
$lang['AddInvoice'] = "إضافة فاتورة جديدة";
$lang['cost_center_name'] = "اسم مركز التكلفة";
$lang['cost_center_name_en'] = "اسم مركز التكلفة بالانجليزية";
$lang['sub_center_name'] = "اسم مركز التكلفة الفرعي";
$lang['sub_center_name_en'] = "اسم مركز التكلفة الفرعي بالانجليزية";
$lang['cost_center_dept'] = "مدين";
$lang['sub_center_details'] = "الوصف بالعربي";
$lang['sub_center_details_en'] = "الوصف بالانجليزية";
$lang['cost_center_creditor'] = "دائن";
$lang['sub_center_parent_id'] = "مركز التكلفة الرئيسي";
$lang['sub_center_dept'] = "مدين";
$lang['sub_center_creditor'] = "دائن";
$lang['cost_center_net'] = "صافي";
$lang['sub_center_net'] = "صافي";
$lang['cost_center_subs'] = "مراكز التكلفة الفرعية";
$lang['If you delete main cost center its subs will be deleted too .Are you sure ?'] = "اذا قمت بحذف مركز التكلفة سوف يتم حذف فرعياته ايضا . هل انت متأكد ؟";
$lang['Edit'] = "تعديل";
$lang['sub_center'] = "مركز التكلفة الفرعي";
$lang['cost_center'] = "مركز التكلفة ";
$lang['Data_base_name'] = "اسم قاعدة البيانات";
$lang['Data_password'] = "كلمة مرور قاعدة البيانات";
$lang['Data_user_name'] = "اسم المستخدم الخاص بقاعدة البيانات";
$lang['expense_item'] = "بنود المصروفات";
$lang['choose_expense_item'] = "اختر بنود المصروفات";
$lang['expense_items_name_en'] = " اسم البند بالانجليزية";
$lang['Delete'] = "حذف";
$lang['proj_sub_project_incomes'] = "اجمالي الايرادات";
$lang['Add_new'] = "اضافة جديد";
$lang['amount'] = "المبلغ";
$lang['expense_items_name'] = "اسم البند";
$lang['Undo'] = "إستعادة";
$lang['expense_items'] = "بنود المصروفات";
$lang['Deleted'] = "السجلات المحذوفة";
$lang['DeleteQuestion'] = "حذف السجل ؟";
$lang['DeleteOk'] = "تنفيذ";
$lang['Yes'] = "نعم";
$lang['No'] = "لا";
$lang['View'] = "معاينة";
$lang['Print'] = "طباعة";
$lang['show_in_card'] = "عرض في grid";
$lang['show_in_table'] = "عرض في جدول";
$lang['PDF'] = "PDF";
$lang['Are you sure?'] = "هل انت متأكد؟";
$lang['Excel'] = "Excel";
$lang['Export'] = "تصدير البيانات";
$lang['Search'] = "بحث";
$lang['Home'] = "الرئيسية";
$lang['Save'] = "حفظ";
$lang['SaveAndAdd'] = "حفظ وإضافة جديد";
$lang['Back'] = "عودة";
$lang['Lock'] = "إغلاق مؤقت";
$lang['Exit'] = "خروج";
$lang['Email'] = "بريد إلكتروني";
$lang['Phone'] = "هاتف";
$lang['ChangeImage'] = "تغيير الصورة";
$lang['ChangeFile'] = "تغيير الملف";
$lang['ChangeCatalogue'] = "تغيير الكتالوج";
$lang['ChangePassword'] = "تغيير كلمة المرور";
$lang['ApplicationStatistics'] = "إحصائيات التطبيق";

$lang['set_area'] = "المناطق الجغرافية";
$lang['set_area_title'] = "اسم المنطقة";
$lang['set_request_status'] = "أنواع حالات الطلب";
$lang['set_request_status_title'] = "حالة الطلب";
$lang['set_request_status_count'] = "عدد حالات الطلب";
$lang['set_service'] = "قائمة الخدمات";
$lang['set_service_title'] = "اسم الخدمة";
$lang['set_service_price'] = "سعر الخدمة";
$lang['set_teamwork'] = "فرق العمل";
$lang['set_teamwork_title'] = "فريق العمل";
$lang['set_teamwork_items'] = "عناصر فرق العمل";
$lang['set_teamwork_items_title'] = "اسم العنصر";
$lang['set_teamwork_items_details'] = "وصف العنصر";
$lang['usr_users'] = "المستخدمين";
$lang['request_status_statistics'] = "إحصائية حالات طلب الخدمة";
$lang['general_statistics'] = "إحصائيات عامة";
$lang['set_area_statistics'] = "إحصائيات مناطق الطلب";
$lang['services_statistics'] = "إحصائيات طلب الخدمات";
$lang['CurrentMonth'] = "الشهر الحالي";
$lang['teamwork_invoice_statistics'] = "إحصائيات عدد فواتير فرق العمل";
$lang['teamwork_invoice_amount_statistics'] = "إحصائيات قيمة فواتير فرق العمل";
$lang['teamwork_invoice_expenses_statistics'] = "إحصائيات مصروفات فواتير فرق العمل";
$lang['data_teamwork_items_revenues_statistics'] = "إحصائيات إيرادات عناصر فرق العمل";
$lang['data_teamwork_items_expenses_statistics'] = "إحصائيات مصروفات عناصر فرق العمل";


$lang['users_group'] = "مجموعات العمل";
$lang['users_group_ar_title'] = "الاسم العربي";
$lang['users_group_en_title'] = "الاسم الانجليزي";
$lang['GroupName'] = "مجموعة العمل";
$lang['users_group_users'] = "عدد المستخدمين";
$lang['fullname'] = "الاسم";
$lang['email'] = "بريد إلكتروني";
$lang['active'] = "فعال";
$lang['users'] = "بيانات المستخدمين";
$lang['active'] = "فعال";
$lang['username'] = "اسم المستخدم";
$lang['password'] = "كلمة المرور";
$lang['image'] = "الصورة";
$lang['UserStore'] = "حدد المخازن التي يمكن البيع منها";
$lang['UserAccounts'] = "حدد الخزن والبنوك التي يمكنه التعامل معها";

$lang['Logo'] = "صورة الشعار";
$lang['crm_link'] = "رابط CRM";
$lang['data_request'] = "طلبات الخدمات";
$lang['data_request_user_id'] = "بواسطة";
$lang['data_request_customer_id'] = "العميل";
$lang['data_request_status_id'] = "الحالة";
$lang['data_request_area_id'] = "المنطقة";
$lang['data_request_source'] = "المصدر";
$lang['data_request_details'] = "التفاصيل";
$lang['data_request_thedate'] = "تاريخ الزيارة";
$lang['data_request_thetime'] = "وقت الزيارة";
$lang['data_request_Visiting'] = "توقيت الزيارة";
$lang['data_request_insert_date'] = "تاريخ التسجيل";
$lang['data_request_insert_time'] = "وقت التسجيل";
$lang['data_request_All_area'] = "كافة المناطق";
$lang['data_request_All_customers'] = "كافة العملاء";
$lang['data_request_All_status'] = "كافة الحالات";
$lang['data_request_All_TeamWork'] = "كافة فرق العمل";
$lang['data_request_All_Categories'] = "كافة التصنيفات";
$lang['data_request_All_Priorities'] = "كافة الدرجات";
$lang['data_request_All_Users'] = "كافة المستخدمين";
$lang['data_request_source1'] = "التطبيق";
$lang['data_request_source2'] = "الموقع";
$lang['data_request_source3'] = "الجوال";
$lang['data_request_convert'] = "تحويل لفاتورة";

$lang['data_request_web'] = "طلبات الصيانة من موقع الويب";


$lang['data_customer'] = "بيانات العملاء";
$lang['data_customer_fullname'] = "الاسم";
$lang['data_customer_address'] = "العنوان";
$lang['data_customer_phone'] = "تليفون";
$lang['data_customer_email'] = "بريد إلكتروني";

$lang['data_invoice'] = "فواتير الخدمات";
$lang['pdf_data_invoice'] = "فاتورة الخدمات";
$lang['data_invoice_teamwork_id'] = "فريق العمل";
$lang['data_invoice_services_title'] = "الخدمة";
$lang['data_invoice_services_price'] = "سعر الوحدة";
$lang['data_invoice_Done'] = "إذا تم التنفيذ يتم إيداع مبلغ الفاتورة في حساب";

$lang['set_area_income'] = "إيرادات المنطقة";
$lang['set_service_income'] = "إيرادات الخدمة";
$lang['set_teamwork_revenues'] = "إيرادات";
$lang['set_teamwork_expenses'] = "مصروفات";
$lang['set_teamwork_net'] = "الربح";

$lang['Other_revenues'] = "إيرادات آخرى";
$lang['Other_expenses'] = "مصروفات آخرى";
$lang['data_teamwork_items_revenues'] = "إيرادات آخرى لفرق العمل";
$lang['amount'] = "المبلغ";
$lang['thedate'] = "التاريخ";
$lang['thetime'] = "الوقت";
$lang['details'] = "البيان";
$lang['data_teamwork_items_expenses'] = "مصروفات آخرى لفرق العمل";

$lang['qunatity'] = "الكمية";

$lang['data_account'] = "سجلات الإيرادات والمصروفات";
$lang['data_account_item_id'] = "عنصر فريق العمل";
$lang['data_account_invoice_id'] = "رقم الفاتورة";
$lang['data_account_depit'] = "وارد";
$lang['data_account_credit'] = "منصرف";

$lang['from_date'] = "من تاريخ";
$lang['to_date'] = "إلى تاريخ";
$lang['SAR'] = "ر.س";
$lang['Total'] = "الإجمالي";
$lang['Convert'] = "تحويل لفاتورة";
$lang['AssignToTeamWork'] = "إسناد إلى فريق عمل";
$lang['schedule'] = "جدولة الزيارات كل كم يوم";
$lang['TeamWorkTasks'] = "الإطلاع على المهام الجاري تنفيذها لفرق العمل";
$lang['closed'] = "مغلقة";
$lang['Close'] = "إغلاق";
$lang['set_teamwork_tasks'] = "مهام فريق العمل";
$lang['ServiceList'] = "الخدمات المطلوبة";
$lang['tasks'] = "المهام";
$lang['revenues'] = "الإيرادات";
$lang['expenses'] = "المصروفات";
$lang['CloseTask'] = "إغلاق المهمة";
$lang['close_date'] = "تاريخ الإغلاق";
$lang['close_time'] = "وقت الإغلاق";
$lang['Add_Cost'] = "تحميل مصروفات";
$lang['InvoiceExpenses'] = "مصروفات الفاتورة";
$lang['InvoiceNet'] = "صافي قيمة الفاتورة";
$lang['InvoiceNo'] = "فاتورة رقم";
$lang['Tax'] = "نسبة ضريبة القيمة المضافة";
$lang['amount_tax'] = "المبلغ مشتملاً على الضريبة";

$lang['profile_UserProfile'] = "الملف الشخصي";
$lang['profile_Message'] = "رسالة";


$lang['serv_message'] = "الرسائل";
$lang['tas_tasks'] = "المهام العامة";
$lang['proj_project_level_task'] = "مهام المشاريع";
$lang['schedule_of_work'] = "جدول الأعمال";
$lang['Recent_Notifications'] = "أحدث التنبيهات";
$lang['DueIn'] = "مستحق في ";
$lang['Day'] = "يوم";
$lang['UpComingEvents'] = "الأحداث القادمة";
$lang['Welcome'] = "مرحباً";
$lang['QuickActions'] = "طلبات معلقة";
$lang['PendingAction'] = "طلب معلق";
$lang['routine_tasks'] = "المهام الروتينية";
$lang['project_tasks'] = "مهام المشاريع";
$lang['Actions'] = "العمليات";
$lang['SeeAll'] = "معاينة الكل";
$lang['ShortCut'] = "قائمة العمليات السريعة";
$lang['ShortCut_account'] = "إدارة الحسابات";
$lang['ShortCut_account_mon_exchange_bonds'] = "إضافة سند صرف خزينة";
$lang['ShortCut_account_mon_receipts'] = "إضافة سند قبض خزينة";
$lang['ShortCut_account_ban_exchange_bonds'] = "إضافة سند صرف بنك";
$lang['ShortCut_account_ban_receipts'] = "إضافة سند قبض بنك";
$lang['ShortCut_purchase'] = "إدارة المشتريات";
$lang['ShortCut_purchase_buy'] = "تسجيل فاتورة مشتريات";
$lang['ShortCut_purchase_buy_return'] = "تسجيل فاتورة مردودات مشتريات";
$lang['ShortCut_project'] = "إدارة المشاريع";
$lang['ShortCut_sub_project'] = "إدارة المشاريع الفرعية";
$lang['ShortCut_sales'] = "إدارة المبيعات";
$lang['proj_sup_end'] = "انهاء";
$lang['no'] = "لا";
$lang['yes'] = "نعم";
$lang['finished'] = "منتهي";
$lang['file'] = "ملف";
$lang['ShortCut_sales_add'] = "تسجيل فاتورة مبيعات";
$lang['ShortCut_sales_return'] = "تسجيل فاتورة مردودات مبيعات";

$lang['ShortCut_Statistics'] = "الإحصائيات";
$lang['ShortCut_Statistics_Account'] = "إحصائيات الحسابات";
$lang['ShortCut_Statistics_Account1'] = "رصيد الخزينة";
$lang['ShortCut_Statistics_Account2'] = "رصيد البنوك";
$lang['ShortCut_Statistics_Purchase'] = "إحصائيات المشتريات";
$lang['ShortCut_Statistics_Sales'] = "إحصائيات المبيعات";
$lang['ShortCut_Statistics_Store'] = "إحصائيات المخازن";
$lang['ShortCut_Statistics_HR'] = "إحصائيات الموارد البشرية";
$lang['ShortCut_Statistics_HR1'] = "إحصائيات الجنسيات";
$lang['ShortCut_Statistics_HR2'] = "إحصائيات التعليم";
$lang['ShortCut_Statistics_HR3'] = "إحصائيات الوظائف";
$lang['ShortCut_Statistics_HR4'] = "إحصائيات التعاقد";
$lang['ShortCut_Statistics_Manufacturing'] = "إحصائيات التصنيع";
$lang['ShortCut_Statistics_Service'] = "إحصائيات الخدمات";
$lang['ShortCut_Statistics_Technical'] = "إحصائيات الدعم الفني";

$lang['ShortCut_Reports'] = "التقارير";
$lang['ShortCut_Reports_Account'] = "تقارير الحسابات";
$lang['ShortCut_Reports_Account_1'] = "تقرير الخزن";
$lang['ShortCut_Reports_Account_2'] = "كشف حساب";
$lang['ShortCut_Reports_Account_3'] = "تقرير المصروفات";
$lang['ShortCut_Reports_Account_4'] = "تقرير المدينون";
$lang['ShortCut_Reports_Account_5'] = "تقرير الدائنون";
$lang['ShortCut_Reports_Account_6'] = "تقرير أوراق القبض";
$lang['ShortCut_Reports_Account_7'] = "تقرير أوراق الدفع";
$lang['ShortCut_Reports_Account_8'] = "تقرير العهد";
$lang['ShortCut_Reports_Account_9'] = "تقرير الضريبة المستحقة على المبيعات";
$lang['ShortCut_Reports_Account_10'] = "تقرير الضريبة علي القيمة المضافة";
$lang['ShortCut_Reports_Purchase'] = "تقارير المشتريات";
$lang['ShortCut_Reports_Purchase_1'] = "تقرير فواتير المشتريات";
$lang['ShortCut_Reports_Purchase_2'] = "كشف حساب المورد";
$lang['ShortCut_Reports_Purchase_3'] = "أرصدة الموردين";
$lang['ShortCut_Reports_Purchase_4'] = "تقرير مردودات المشتريات";
$lang['ShortCut_Reports_Purchase_5'] = "تقرير المشتريات";
$lang['ShortCut_Reports_Purchase_6'] = "قائمة الموردين";
$lang['ShortCut_Reports_Purchase_7'] = "تقرير أسعار الشراء";
$lang['ShortCut_Reports_Purchase_8'] = "تقرير مقارنة أسعار الشراء";
$lang['ShortCut_Reports_Purchase_9'] = "تقرير مقارنة عروض الأسعار";
$lang['ShortCut_Reports_Sales'] = "تقارير المبيعات";
$lang['ShortCut_Reports_Sales_1'] = "تقرير فواتير المبيعات";
$lang['ShortCut_Reports_Sales_2'] = "كشف حساب العميل";
$lang['ShortCut_Reports_Sales_3'] = "أرصدة العملاء";
$lang['ShortCut_Reports_Sales_4'] = "تقرير مردودات المبيعات";
$lang['ShortCut_Reports_Sales_5'] = "تقرير مبيعات الموظف";
$lang['ShortCut_Reports_Sales_6'] = "تقرير مبيعات المندوب";
$lang['ShortCut_Reports_Sales_7'] = "تقرير أسعار البيع ";
$lang['ShortCut_Reports_Sales_8'] = "تقرير مقارنة أسعار البيع";
$lang['ShortCut_Reports_Sales_9'] = "تقرير مقارنة عروض الأسعار";
$lang['ShortCut_Reports_Sales_10'] = "تقرير الأكثر مبيعاً بالكمية";
$lang['ShortCut_Reports_Sales_11'] = "تقرير الأكثر مبيعاً بالأسعار";
$lang['ShortCut_Reports_Store'] = "تقارير المخازن";
$lang['ShortCut_Reports_Store_1'] = "تقرير حركة الصنف";
$lang['ShortCut_Reports_Store_2'] = "تقرير فهرس الأصناف";
$lang['ShortCut_Reports_Store_3'] = "تقرير أرصدة الأصناف";
$lang['ShortCut_Reports_Store_4'] = "أصناف مطلوب توريدها";
$lang['ShortCut_Reports_Store_5'] = "أصناف رصيدها سالب";
$lang['ShortCut_Reports_Store_6'] = "تقرير الأرصدة بالمخازن";
$lang['ShortCut_Reports_HR'] = "تقارير الموارد البشرية";
$lang['ShortCut_Reports_HR_1'] = "تقرير بيانات الموظفين";
$lang['ShortCut_Reports_HR_2'] = "تقرير الحضور والإنصراف";
$lang['ShortCut_Reports_HR_3'] = "تقرير المرتبات";
$lang['ShortCut_Reports_HR_4'] = "تقرير الإجازات";
$lang['ShortCut_Reports_Manufacturing'] = "تقرير التصنيع";
$lang['ShortCut_Reports_Service'] = "تقارير الخدمات";
$lang['ShortCut_Reports_Service_1'] = "تقرير طلبات الخدمات";
$lang['ShortCut_Reports_Service_2'] = "تقرير فواتير الخدمات";
$lang['ShortCut_Reports_Technical'] = "تقرير الدعم الفني";

$lang['Account_statement'] = "كشف حساب";

$lang['Today'] = "اليوم";
$lang['Month'] = "الشهر";
$lang['Year'] = "السنة";

//fin_treeaccount
$lang['fin_treeaccount'] = "شجرة الحسابات";
$lang['Page_fin_treeaccount'] = "من خلال هذه الصفحة يمكنك التحكم في شجرة الحسابات";
$lang['fin_treeaccount_category_id'] = "التصنيف";
$lang['fin_treeaccount_parent_id'] = "الحساب الرئيسي";
$lang['fin_treeaccount_account_no'] = "رقم الحساب";
$lang['fin_treeaccount_account_name'] = "اسم الحساب";
$lang['fin_treeaccount_title'] = "الاسم العربي";
$lang['fin_treeaccount_title_en'] = "الاسم الانجليزي";
$lang['fin_treeaccount_startamount'] = "الرصيد الإفتتاحي";
$lang['fin_treeaccount_basic'] = "حساب أساسي";
$lang['fin_treeaccount_level_no'] = "رقم المستوى";
$lang['fin_treeaccount_table'] = "العرض الجدولي";
$lang['fin_treeaccount_treeview'] = "العرض الشجري";
$lang['fin_treeaccount_depreciation'] = "معدل الإهلاك";
$lang['fin_treeaccount_thedate'] = "تاريخ الشراء";
$lang['fin_treeaccount_image'] = "وثيقة الشراء";
$lang['fin_treeaccount_purchasing_price'] = "سعر الشراء";

//fin_journal
$lang['Fin_journal'] = "قيد";
$lang['Page_fin_journal'] = "من خلال هذه الصفحة يمكنك الإطلاع على قيود اليومية";
$lang['fin_journal_account_id'] = "اسم الحساب";
$lang['fin_journal_debit'] = "مدين";
$lang['fin_journal_creditor'] = "دائن";
$lang['fin_journal_details'] = "البيان";
$lang['fin_journal_thedate'] = "التاريخ";
$lang['fin_journal_image'] = "صورة القيد";
$lang['fin_journal_bill_id'] = "رقم الفاتورة";
$lang['fin_journal_table_name'] = "نوع القيد";
$lang['fin_journal_automatic'] = "قيد تلقائي";
$lang['fin_journal_manual'] = "قيد يدوي";
$lang['fin_journal_debit_total'] = "إجمالي المدين";
$lang['fin_journal_creditor_total'] = "إجمالي الدائن";
$lang['fin_journal_Alert'] = "يجب أن يتساوى الطرفين المدين والدائن ويجب أن تكون القيمة أكبر من صفر";
$lang['fin_journal_Total'] = "المجموع";
$lang['fin_journal_delete'] = "تأكيد عملية الحذف ؟";
$lang['fin_journal_delete_all'] = "حذف كافة القيود اليومية";
//balance_sheet
$lang['balance_sheet'] = "الميزانية العمومية";
$lang['balancesheet_Assets'] = "الأصول";
$lang['balancesheet_TotalAssets'] = "إجمالي الأصول";
$lang['balancesheet_Liabilities'] = "الخصوم";
$lang['balancesheet_TotalLiabilities'] = "إجمالي الخصوم";
$lang['balancesheet_StockholdersEquity'] = "حقوق الملكية";
$lang['RetainedEarnings'] = "الأرباح المكتسبة";
$lang['balancesheet_TotalStockholdersEquity'] = "إجمالي حقوق الملكية";
$lang['TotalLiabilitiesEquity'] = "إجمالي الخصوم وحقوق الملكية";

$lang['Trial_Balance'] = "ميزان المراجعة";

//profit_loss
$lang['profit_loss'] = "الأرباح والخسائر";
$lang['Revenue'] = "الإيرادات";
$lang['Expenses'] = "المصروفات";
$lang['Total'] = "الإجمالي";

//buy_manufacturer
$lang['buy_manufacturer'] = "بيانات الموردين";
$lang['Page_buy_manufacturer'] = "من خلال هذه الصفحة يمكنك إدارة بيانات الموردين";
$lang['buy_manufacturer_title'] = "الاسم العربي";
$lang['buy_manufacturer_title_en'] = "الاسم الانجليزي";
$lang['buy_manufacturer_phone'] = "الهاتف";
$lang['buy_manufacturer_mobile'] = "المحمول";
$lang['buy_manufacturer_address'] = "العنوان";
$lang['buy_manufacturer_email'] = "البريد الإلكتروني";
$lang['buy_manufacturer_commercial_register'] = "رقم السجل التجاري";
$lang['buy_manufacturer_tax_card'] = "الرقم الضريبي";
$lang['buy_manufacturer_tax_card_date'] = "تاريخ الرقم الضريبي";
$lang['buy_manufacturer_person'] = "اسم المسئول";
$lang['buy_manufacturer_person_phone'] = "هاتف المسئول";
$lang['buy_manufacturer_credit_limit'] = "الحد الإئتماني";

//buy_bill
$lang['buy_bill'] = "فواتير المشتريات";
$lang['Page_buy_bill'] = "من خلال هذه الصفحة يمكنك إدارة فواتير المشتريات";
$lang['buy_bill_supplier_id'] = "المورد";
$lang['buy_bill_thedate'] = "التاريخ";
$lang['buy_bill_totalvalue'] = "إجمالي المبلغ";
$lang['buy_bill_paid'] = "المدفوع";
$lang['buy_bill_remaining'] = "المتبقي";
$lang['buy_bill_discount'] = "الخصم";
$lang['buy_bill_notes'] = "ملاحظات";
$lang['buy_bill_image'] = "صورة الفاتورة";
$lang['buy_bill_tree_id'] = "الدفع من حساب";

$lang['Pay'] = "سداد";
$lang['buy_bill_Pay'] = "سداد دفعة من فاتورة المشتريات";
$lang['buy_returns_Pay'] = "إستلام دفعة من فاتورة مردودات المشتريات";
$lang['sale_bill_pay'] = "سداد دفعة من فاتورة المبيعات";
$lang['sale_returns_pay'] = "سداد دفعة من فاتورة مردودات المبيعات";
$lang['To_be_paid'] = "المطلوب دفعه";
$lang['Debit_from_supplier_account'] = "الخصم من المورد";
$lang['Debit_from_customer_account'] = "الخصم من العميل";

$lang['buy_bill_all'] = "جميع الفواتير";
$lang['Opening credit'] = "رصيد افتتاحي";
$lang['Type'] = "النوع";
$lang['Sales'] = "مبيعات";
$lang['Purchases'] = "مشتريات";
$lang['buy_bill_not_paid'] = "غير مدفوعة";
$lang['buy_bill_part_paid'] = "مدفوعة جزئياً";
$lang['buy_bill_full_paid'] = "مدفوعة بالكامل";
$lang['buy_returns_not_paid'] = "غير مسددة";
$lang['buy_returns_part_paid'] = "مسددة جزئياً";
$lang['buy_returns_full_paid'] = "مسددة بالكامل";

$lang['buy_bill_items_store_type_id'] = "الصنف";
$lang['buy_bill_items_location_id'] = "المخزن";
$lang['buy_bill_items_quantity'] = "الكمية";
$lang['buy_bill_items_unitprice'] = "سعر الوحدة";

//buy_returns
$lang['buy_returns'] = "مردودات المشتريات";
$lang['Page_buy_returns'] = "من خلال هذه الصفحة يمكنك التحكم في فواتير مردودات المشتريات";
$lang['buy_returns_supplier_id'] = "المورد";
$lang['buy_returns_thedate'] = "التاريخ";
$lang['buy_returns_totalvalue'] = "إجمالي المبلغ";
$lang['buy_returns_paid'] = "المدفوع";
$lang['buy_returns_remaining'] = "المتبقي";
$lang['buy_returns_discount'] = "الخصم";
$lang['buy_returns_notes'] = "ملاحظات";
$lang['buy_returns_image'] = "صورة الفاتورة";
$lang['buy_returns_tree_id'] = "الإستلام في حساب";

$lang['buy_returns_items_store_type_id'] = "الصنف";
$lang['buy_returns_items_location_id'] = "المخزن";
$lang['buy_returns_items_quantity'] = "الكمية";
$lang['buy_returns_items_unitprice'] = "سعر الوحدة";

//buy_offer
$lang['buy_offer'] = "عروض الاسعار";
$lang['buy_offer_supplier_id'] = "المورد";
$lang['buy_offer_thedate'] = "التاريخ";
$lang['buy_offer_expired_date'] = "صالح حتى";
$lang['buy_offer_totalvalue'] = "إجمالي المبلغ";
$lang['buy_offer_paid'] = "المدفوع";
$lang['buy_offer_remaining'] = "المتبقي";
$lang['buy_offer_discount'] = "الخصم";
$lang['buy_offer_tax'] = "الضريبة";
$lang['buy_offer_over_cost'] = "مصروفات إضافية";
$lang['buy_offer_notes'] = "ملاحظات";
$lang['buy_offer_image'] = "ملف مرفق";
$lang['buy_offer_ChangeImage'] = "تغيير المرفق";
$lang['buy_offer_tree_id'] = "الدفع من حساب";
$lang['buy_offer_date_all'] = "جميع العروض";
$lang['buy_offer_date_expired'] = "المنتهي مدتها";
$lang['buy_offer_date_not_expired'] = "الغير منتهي مدتها";

$lang['buy_offer_items_store_type_id'] = "الصنف";
$lang['buy_offer_items_location_id'] = "المخزن";
$lang['buy_offer_items_quantity'] = "الكمية";
$lang['buy_offer_items_unitprice'] = "سعر الوحدة";

//expenses_report
$lang['expenses_report'] = "تقرير المصروفات";

//store
$lang['store'] = "قائمة المخازن";
$lang['Page_store'] = "من خلال هذه الصفحة يمكنك إدارة المخازن";
$lang['store_web_store'] = "هذا هو مخزن الموقع";
$lang['store_parent_id'] = "المخزن الرئيسي";
$lang['store_title'] = "الاسم العربي للموقع";
$lang['store_title_en'] = "الاسم الانجليزي للموقع";
$lang['store_address'] = "عنوان المخزن";
$lang['store_email'] = "بريد إلكتروني";
$lang['store_phone'] = "هاتف";
$lang['store_active'] = "فعال";
// $lang['store_user_id'] = "أمين المخزن";

//store_move
$lang['store_move'] = "نقل من مخزن إلى مخزن";
$lang['Page_store_move'] = "من خلال هذه الصفحة يمكنك متابعة عمليات نقل الأصناف بين المخازن";
$lang['store_move_user_id'] = "اسم المستخدم";
$lang['store_move_from_store'] = "من مخزن";
$lang['store_move_to_store'] = "إلى مخزن";
$lang['store_move_product_id'] = "الصنف";
$lang['store_move_product_name'] = "اسم الصنف";
$lang['store_move_product_code'] = "باركود الصنف";
$lang['store_move_quantity'] = "الكمية";
$lang['store_move_thedate'] = "التاريخ";
$lang['store_move_thetime'] = "الوقت";

//product
$lang['product'] = "بيانات الأصناف";
$lang['Page_product'] = "من خلال هذه الصفحة يمكنك الإطلاع على وتعديل وإضافة أصناف الموقع";
$lang['product_category_id'] = "التصنيف";
$lang['product_brand_id'] = "الماركة";
$lang['product_barcode'] = "الباركود";
$lang['product_batch_number'] = "باتش نمبر";
$lang['product_thetitle'] = "الصنف";
$lang['product_title'] = "الاسم العربي";
$lang['product_title_en'] = "الاسم الانجليزي";
$lang['product_details'] = "الوصف العربي";
$lang['product_details_en'] = "الوصف الانجليزي";
$lang['product_product_type'] = "نوع الصنف";
$lang['product_offer_enddate'] = "نهاية الخصم";
$lang['product_old_price'] = "قبل الخصم";
$lang['product_lowest_price'] = "أقل سعر بيع";
$lang['product_cost_price'] = "سعر التكلفة";
$lang['product_Price'] = "سعر الوحدة";
$lang['product_view_no'] = "المشاهدات";
$lang['product_image'] = "صورة الصنف";
$lang['product_catalogue'] = "الكتالوج";
$lang['product_finished'] = "منتج تام الصنع؟";
$lang['product_demand'] = "حد الطلب";

//store_quantity
$lang['store_quantity'] = "كميات الأصناف";
$lang['Page_store_quantity'] = "من خلال هذه الصفحة يمكنك التحكم في كميات الأصناف بالمخازن";
$lang['store_quantity_store_id'] = "المخزن";
$lang['store_quantity_product_id'] = "الصنف";
$lang['store_quantity_quantity'] = "الكمية";

//store_category
$lang['store_category'] = "تصنيف المنتجات";
$lang['store_category_title'] = "الاسم العربي";
$lang['store_category_title_en'] = "الاسم الانجليزي";
$lang['store_category_active'] = "تصنيف فعال";

$lang['product_count'] = "عدد المنتجات";

//store_brand
$lang['store_brand'] = "موديلات المنتجات";
$lang['store_brand_title'] = "الاسم العربي";
$lang['store_brand_title_en'] = "الاسم الانجليزي";
$lang['store_brand_active'] = "موديل فعال";

//expenses
$lang['expenses'] = "سجلات المصروفات";
$lang['Page_expenses'] = "من خلال هذه الصفحة يمكنك إدارة المصروفات";
$lang['expenses_fromaccount'] = "المصروف";
$lang['expenses_toaccount'] = "يدفع من حساب";
$lang['expenses_thevalue'] = "المبلغ";
$lang['expenses_details'] = "البيان";
$lang['expenses_thedate'] = "التاريخ";
$lang['expenses_bill_id'] = "رقم الفاتورة";
$lang['expenses_image'] = "صورة الإيصال";

//fin_settings
$lang['fin_settings'] = "إعدادات الحسابات";
$lang['settings_manage'] = "إدارة الإعدادات";
$lang['Page_fin_settings'] = "من خلال هذه الصفحة يمكنك ضبط إعدادات الحسابات";
$lang['acc_sale'] = "حساب المبيعات";
$lang['acc_website_sale'] = "حساب مبيعات موقع الويب";
$lang['acc_sale_reurn'] = "حساب مردودات المبيعات";
$lang['acc_buy'] = "حساب المشتريات";
$lang['acc_buy_reurn'] = "حساب مردودات المشتريات";
$lang['acc_treasury'] = "حساب الخزن";
$lang['acc_bank'] = "حساب البنوك";
$lang['acc_expenses'] = "حساب المصروفات";
$lang['acc_revenues'] = "حساب إيرادات الخدمات";
$lang['acc_debtors'] = "حساب المدينون";
$lang['acc_creditors'] = "حساب الدائنون";
$lang['acc_capture_papers'] = "حساب أوراق القبض";
$lang['acc_payment_papers'] = "حساب أوراق الدفع";
$lang['acc_inventory'] = "حساب المخزون";
$lang['acc_supplier'] = "حساب الموردين";
$lang['acc_customer'] = "حساب العملاء";
$lang['acc_material_store'] = "حساب مخزن الخامات";
$lang['acc_damaged_store'] = "حساب مخزن التالف";
$lang['acc_invoice_items'] = "عدد بنود الفاتورة";
$lang['acc_custody'] = "حساب العهد";
$lang['acc_hr_loan'] = "حساب سلف الموظفين";
$lang['acc_tax_sale'] = "حساب ضريبة المبيعات";
$lang['acc_tax_sale_percent'] = "نسبة ضريبة المبيعات";
$lang['acc_tax_purchase'] = "حساب ضريبة المشتريات";
$lang['acc_tax_purchase_percent'] = "نسبة ضريبة المشتريات";
$lang['custody_user_id'] = "المستخدم المسئول عن العهدة";
$lang['acc_affiliate'] = "حساب عمولة المسوقين";
$lang['acc_delivery'] = "حساب أجر التوصيل";
$lang['acc_factory_cost'] = "حساب تكلفة التصنيع";
$lang['acc_sales_commission'] = "حساب عمولة المبيعات";
$lang['acc_fixed_assets'] = "حساب الأصول الثابتة";
$lang['acc_current_assets'] = "حساب الأصول المتداولة";
$lang['fixed_assets'] = "الأصول الثابتة";
$lang['fixed_assets_total'] = "إجمالي الأصول الثابتة";
$lang['current_assets'] = "الأصول المتداولة";
$lang['current_assets_total'] = "إجمالي الأصول المتداولة";
$lang['Total_Assets'] = "إجمالي الأصول";
$lang['OwnerEquity_total'] = "إجمالي حقوق الملكية";
$lang['Liabilities_total'] = "إجمالي الخصوم";

$lang['fin_settings_purchase'] = "خيارات المشتريات";
$lang['fin_settings_purchase_details'] = "ضبط حسابات المشتريات مع حسابات شجرة الحسابات";
$lang['fin_settings_sale'] = "خيارات المبيعات";
$lang['fin_settings_sale_details'] = "ضبط حسابات المبيعات مع حسابات شجرة الحسابات";
$lang['fin_settings_money'] = "خيارات المالية";
$lang['fin_settings_money_details'] = "ضبط حسابات الخزن والبنوك";
$lang['fin_settings_hr_loan'] = "خيارات العهد والسلف";
$lang['fin_settings_hr_loan_details'] = "ضبط حسابات العهد والسلف";
$lang['fin_settings_tax'] = "خيارات الضرائب";
$lang['fin_settings_tax_details'] = "ضبط خيارات ضرائب المبيعات والمشتريات";
$lang['fin_settings_debit_balances'] = "أرصدة مدينة";
$lang['fin_settings_debit_balances_details'] = "ضبط خيارات بعض الأرصدة المدينة";
$lang['fin_settings_Credit_balances'] = "أرصدة دائنة";
$lang['fin_settings_Credit_balances_details'] = "ضبط خيارات بعض الأرصدة الدائنة";
$lang['fin_settings_store'] = "خيارات المخازن";
$lang['fin_settings_store_details'] = "ضبط خيارات المخازن";

$lang['usr_usersgroup'] = "مجموعات المستخدمين";
$lang['usr_users'] = "بيانات المستخدمين";
$lang['branches_lookup'] = "يمكنه التنقل بين الفروع ؟";

$lang['Debit'] = "مدين";
$lang['Creditor'] = "دائن";
$lang['ar_title'] = "الاسم العربي";
$lang['en_title'] = "الاسم الإنجليزي";
$lang['BankNumber'] = "رقم الحساب";
$lang['phone'] = "هاتف";
$lang['mobile1'] = "جوال";
$lang['mobile2'] = "واتس أب";
$lang['address'] = "العنوان";
$lang['credit_limit'] = "حد الإئتمان";
$lang['startamount'] = "الرصيد الإفتتاحي";
$lang['Balance'] = "الرصيد";
$lang['OneBalance'] = "رصيد";
$lang['ManualEntry'] = "قيود يدوية";
$lang['commercial_register'] = "سجل تجاري";
$lang['tax_card'] = "بطاقة ضريبية";
$lang['person'] = "الشخص المسئول";
$lang['person_phone'] = "تليفون المسئول";

//sale_customer
$lang['sale_customer'] = "بيانات العملاء";
$lang['sale_customer1'] = "بيانات العميل";
$lang['sale_fullname'] = "اسم العميل";
$lang['mobile1'] = "موبايل";
$lang['mobile2'] = "واتس أب";
$lang['address'] = "العنوان";
$lang['customers_phone_comment'] = "للبحث أدخل رقم التليفون ثم إضغط Tab";
$lang['sale_customer_Debt_scheduling'] = "جدولة المديونية";
$lang['payment_amount'] = "قيمة الدفعة";

$lang['orders_convert'] = "تحويل إلى فاتورة مبيعات";
//sale_bill
$lang['sale_bill'] = "فواتير المبيعات";
$lang['Page_sale_bill'] = "من خلال هذه الصفحة يمكنك إدارة فواتير المبيعات";
$lang['sale_bill_customer_id'] = "العميل";
$lang['sale_bill_thedate'] = "التاريخ";
$lang['sale_bill_totalvalue'] = "إجمالي المبلغ مع الضريبة";
$lang['sale_bill_totalitemsvalue'] = "اجمالي ثمن الفاتورة";
$lang['sale_bill_total_after_discount'] = "ثمن البنود بعد الخصم";
$lang['sale_bill_required_price'] = "مبلغ الضريبة المطلوب";
$lang['sale_bill_paid'] = "المدفوع";
$lang['sale_bill_total_paid'] = "إجمالي المدفوع";
$lang['sale_bill_advance_payment'] = "دفعة مقدمة";
$lang['sale_bill_remaining'] = "المتبقي";
$lang['sale_bill_discount'] = "الخصم";
$lang['sale_bill_notes'] = "ملاحظات";
$lang['sale_bill_image'] = "صورة الفاتورة";
$lang['sale_bill_tree_id'] = "الإيداع في حساب";
$lang['subtotal'] = "المجموع الفرعي";
$lang['invoice_items'] = "عدد بنود الفاتورة";
$lang['JustDoNot'] = "فقط لاغير";
$lang['ReceviesSign'] = "توقيع العميل";
$lang['DriverSign'] = "توقيع السائق";
$lang['SalesmanSign'] = "توقيع البائع";
$lang['SendMessageToStore'] = "إرسال رسالة لأمين المخزن";
$lang['sale_bill_customer_name'] = "في حالة البيع النقدي أدخل اسم العميل";
$lang['sale_bill_customer_phone'] = "في حالة البيع النقدي أدخل هاتف العميل";
$lang['sale_bill_Cash_sales'] = "حساب المبيعات النقدية";
$lang['received'] = "المستلم";
$lang['Quantities_not_fully_delivered'] = "كميات غير مسلمة بالكامل";
$lang['Delivery'] = "تسليم";


$lang['sale_bill_items_store_type_id'] = "الصنف";
$lang['sale_bill_items_location_id'] = "المخزن";
$lang['sale_bill_items_quantity'] = "الكمية";
$lang['sale_bill_items_available'] = "المتاح";
$lang['sale_bill_items_unitprice'] = "سعر الوحدة";
$lang['sale_bill_items_price'] = "السعر";
$lang['sale_bill_items_total'] = "الإجمالي";
$lang['Payment_options'] = "خيارات السداد";
$lang['Payment_options_details'] = "يمكنك السداد بعدة طرق في نفس الوقت";
$lang['sale_bill_treasury'] = "حساب الخزينة";
$lang['sale_bill_bank'] = "حساب البنك أو الشبكة";
$lang['sale_bill_customer_money'] = "رصيد العميل";

//sale_returns
$lang['sale_returns'] = "مردودات المبيعات";
$lang['Page_sale_returns'] = "من خلال هذه الصفحة يمكنك إدارة فواتير مردودات المبيعات";
$lang['sale_returns_customer_id'] = "العميل";
$lang['sale_returns_thedate'] = "التاريخ";
$lang['sale_returns_totalvalue'] = "إجمالي المبلغ";
$lang['sale_returns_paid'] = "المدفوع";
$lang['sale_returns_remaining'] = "المتبقي";
$lang['sale_returns_discount'] = "الخصم";
$lang['sale_returns_notes'] = "ملاحظات";
$lang['sale_returns_image'] = "صورة الفاتورة";
$lang['sale_returns_tree_id'] =  "الدفع من حساب";

$lang['sale_returns_items_store_type_id'] = "الصنف";
$lang['sale_returns_items_location_id'] = "المخزن";
$lang['sale_returns_items_quantity'] = "الكمية";
$lang['sale_returns_items_unitprice'] = "سعر الوحدة";

//sale_offer
$lang['sale_offer'] = "عروض المبيعات";
$lang['Page_sale_offer'] = "من خلال هذه الصفحة يمكنك إدارة فواتير المبيعات";
$lang['sale_offer_customer_id'] = "العميل";
$lang['sale_offer_thedate'] = "التاريخ";
$lang['sale_offer_totalvalue'] = "إجمالي المبلغ";
$lang['sale_offer_paid'] = "المدفوع";
$lang['sale_offer_remaining'] = "المتبقي";
$lang['sale_offer_discount'] = "الخصم";
$lang['sale_offer_notes'] = "ملاحظات";
$lang['sale_offer_image'] = "صورة الفاتورة";
$lang['sale_offer_tree_id'] = "الإيداع في حساب";

$lang['sale_offer_items_store_type_id'] = "الصنف";
$lang['sale_offer_items_location_id'] = "المخزن";
$lang['sale_offer_items_quantity'] = "الكمية";
$lang['sale_offer_items_unitprice'] = "سعر الوحدة";

//hr
//hr_inouttimedata
$lang['Absence'] = "غياب";
$lang['Absence_Vacation'] = "غياب أو إجازة";
$lang['hr_employee_id'] = "اسم الموظف";
$lang['hr_in_date'] = "تاريخ الحضور";
$lang['hr_in_time'] = "وقت الحضور";
$lang['hr_out_date'] = "تاريخ الإنصراف";
$lang['hr_out_time'] = "وقت الإنصراف";

//tas_tasks
$lang['tasks_from_user'] = "من المستخدم";
$lang['tasks_thedate'] = "التاريخ";
$lang['tasks_thetime'] = "الوقت";
$lang['tasks_title'] = "اسم المهمة";
$lang['tasks_details'] = "تفاصيل المهمة";
$lang['tasks_task_type'] = "نوع المهمة";
$lang['tasks_status'] = "الحالة";
$lang['tasks_levels'] = "المراحل";
$lang['tasks_to_user'] = "المكلف بالتنفيذ";
$lang['tasks_start_date'] = "تاريخ البدء";
$lang['tasks_start_time'] = "توقيت البدء";
$lang['tasks_end_date'] = "تاريخ الإنتهاء";
$lang['tasks_end_time'] = "توقيت الإنتهاء";
$lang['tasks_days'] = "كل كم يوم؟";
$lang['tasks_doloop'] = "عدد مرات التكرار";

$lang['tasks_table_title'] = "المهمة";
$lang['tasks_table_fromuser'] = "القائم بالتكليف";
$lang['tasks_table_touser'] = "موجهة للمستخدم";
$lang['tasks_table_levels'] = "المرحلة";
$lang['tasks_table_start'] = "البدء";
$lang['tasks_table_end'] = "الإنتهاء";

$lang['tasks_start'] = "بدء";
$lang['tasks_Failed'] = "فشل";
$lang['tasks_cancel'] = "إلغاء";
$lang['tasks_Time_spent'] = "الوقت المستغرق";

// proj_status
$lang['proj_status'] = "حالات المشاريع";
$lang['proj_status_ar_title'] = "الاسم العربي";
$lang['proj_status_en_title'] = "الاسم الانجليزي";
$lang['proj_status_projects'] = "المشاريع";
$lang['proj_status_Incoming'] = "الوارد";
$lang['proj_status_Outgoing'] = "المنصرف";

// proj_project
$lang['proj_project'] = "المشاريع";
$lang['proj_project_customer_id'] = "اسم العميل";
$lang['proj_project_status_id'] = "حالة المشروع";
$lang['proj_project_manager_id'] = "مدير المشروع";
$lang['proj_project_title'] = "اسم المشروع";
$lang['proj_project_ar_title'] = "الاسم العربي";
$lang['proj_project_en_title'] = "الاسم الإنجليزي";
$lang['proj_project_ar_details'] = "الوصف العربي";
$lang['proj_project_en_details'] = "الوصف الإنجليزي";
$lang['proj_project_start_date'] = "تاريخ البدء";
$lang['proj_project_end_date'] = "تاريخ التسليم";
$lang['proj_project_labor_cost'] = "تكلفة العمالة";
$lang['proj_project_material_cost'] = "تكلفة الخامات";
$lang['proj_project_total_cost'] = "إجمالي التكلفة";
$lang['proj_project_amount'] = "قيمة التعاقد";
$lang['proj_project_image'] = "لوجو المشروع";
$lang['proj_project_remaining'] = "الوقت المتبقي";
$lang['proj_project_convert'] = "بدء التنفيذ";
$lang['individual'] = "فرد";
$lang['proj_sub_project_customer'] = "المستأجر";
$lang['company'] = "شركة";
$lang['proj_sup_project_customer_name'] = "اسم المستأجر";
$lang['proj_sup_project_customer_id_number'] = "رقم الهوية";
$lang['proj_sup_project_customer_address'] = "العنوان";
$lang['proj_sup_project_customer_mobile'] = "الهاتف";
$lang['proj_sup_project_customer_national_number'] = "الرقم القومي";

$lang['proj_project_devices'] = "المعدات";
$lang['proj_sub_projects'] = "المشاريع الفرعية";
$lang['proj_project_project_id'] = "المشروع الرئيسي";
$lang['proj_project_barcode'] = "باركود";
$lang['files'] = "الملفات";
$lang['proj_project_details'] = "الوصف";
$lang['proj_project_plugin_date'] = "تاريخ التركيب";
$lang['proj_project_building_no'] = "رقم المبنى";
$lang['proj_project_floor_no'] = "رقم الدور";
$lang['proj_project_room_no'] = "رقم الغرفة";
$lang['proj_sup_project_name'] = "اسم المشروع الفرعي";
$lang['proj_sub_project_date_start'] = "تاريخ البداية";
$lang['proj_sub_project_date_end'] = "تاريخ النهاية";
$lang['proj_sub_project_total'] = "اجمالي القيمة";
$lang['proj_sub_project_paid'] = "الدفعات المسددة";
$lang['proj_sub_project_expenses'] = "اجمالي المصروفات";
$lang['proj_sub_project_net'] = "الصافي";
$lang['proj_sup_project_customer_type'] = "نوع المستأجر";
$lang['proj_sub_project_remain'] = "المتبقي";

$lang['proj_project_Earnings'] = "الإيرادات";
$lang['proj_project_Expenses'] = "مصروفات التشغيل";
$lang['proj_project_Labor'] = "الأجور";
$lang['proj_project_Material'] = "فواتير الخامات";
$lang['Progress'] = "معدل التقدم";
$lang['proj_project_TotalExpenses'] = "المصروفات الرئيسية";
$lang['proj_sub_project_TotalExpenses'] = "المصروفات الفرعية";
$lang['proj_project_Net'] = "الصافي";
$lang['proj_project_AddStaff'] = "إضافة موظف للمشروع";
// proj_project_files
$lang['proj_project_files'] = "ملفات المشروع";
$lang['multiple_files'] = "(يدعم تعدد الملفات)";
$lang['Choose_files'] = "إختر الملفات";
$lang['Choose'] = "إختر";

//proj_project_files
$lang['proj_project_files'] = "ملفات المشاريع";
$lang['proj_project_files_project_id'] = "المشروع";
$lang['proj_project_files_project_sub_id'] = "المشروع الفرعي";
$lang['proj_project_files_level_id'] = "المرحلة";
$lang['proj_project_files_task_id'] = "المهمة";
$lang['proj_project_files_ar_title'] = "الاسم العربي";
$lang['proj_project_files_en_title'] = "الاسم الانجليزي";
$lang['proj_project_files_image'] = "الملف";

//proj_project_level
$lang['proj_project_level'] = "مراحل المشاريع";
$lang['proj_project_level_project_id'] = "المشروع";
$lang['proj_project_level_manager_id'] = "مسئول المرحلة";
$lang['proj_project_level_status_id'] = "حالة المرحلة";
$lang['proj_project_level_ar_title'] = "الاسم العربي";
$lang['proj_project_level_en_title'] = "الاسم الانجليزي";
$lang['proj_project_level_start_date'] = "تاريخ البدء";
$lang['proj_project_level_end_date'] = "تاريخ التسليم";

//proj_project_level
$lang['proj_project_level_task'] = "مهام المشاريع";
$lang['proj_project_level_task_project_id'] = "المشروع";
$lang['proj_project_level_task_level_id'] = "المرحلة";
$lang['proj_project_level_task_user_id'] = "مسئول المرحلة";
$lang['proj_project_level_task_status_id'] = "حالة المهمة";
$lang['proj_project_level_task_ar_details'] = "الوصف العربي";
$lang['proj_project_level_task_en_details'] = "الوصف الانجليزي";
$lang['proj_project_level_task_start_date'] = "تاريخ البدء";
$lang['proj_project_level_task_end_date'] = "تاريخ التسليم";

//serv_message
$lang['serv_message'] = "صندوق البريد";
$lang['serv_message_new'] = "رسالة جديدة";
$lang['serv_message_update'] = "تحرير الرسالة";
$lang['serv_message_from_user'] = "المرسل";
$lang['serv_message_to_user'] = "المستلم";
$lang['serv_message_subject'] = "عنوان الرسالة";
$lang['serv_message_message'] = "الرسالة";
$lang['serv_message_thedate'] = "التاريخ والتوقيت";
$lang['serv_message_index'] = "الوارد";
$lang['serv_message_image'] = "المرفق";
$lang['serv_message_marked'] = "المميزة";
$lang['serv_message_draft'] = "المحفوظة مؤقتاً";
$lang['serv_message_sent'] = "الصادر";
$lang['serv_message_trash'] = "المحذوفة";
$lang['serv_message_replay'] = "الرد";
$lang['serv_message_send_replay'] = "الرد على الرسالة";
$lang['serv_message_submit'] = "إرسال الرسالة";
$lang['serv_message_save_draft'] = "حفظ مؤقت";
$lang['serv_message_filter'] = "فرز الرسائل";

$lang['serv_events'] = "الأحداث";
$lang['serv_events_start_date'] = "من تاريخ";
$lang['serv_events_end_date'] = "إلى تاريخ";
$lang['serv_events_details'] = "تفاصيل الحدث";
$lang['serv_events_is_scheduled'] = "حدث مجدول";
$lang['serv_events_scheduled_month'] = "جدولة شهرية";
$lang['serv_events_scheduled_year'] = "جدولة سنوية";
$lang['serv_events_count'] = "عدد المدعوين";
$lang['serv_events_scheduled'] = "مجدولة";
$lang['serv_events_scheduled_month'] = "جدولة شهرية";
$lang['serv_events_scheduled_year'] = "جدولة سنوية";
$lang['serv_events_all'] = "كافة الأحداث";
$lang['serv_events_old'] = "الأحداث الماضية";
$lang['serv_events_current'] = "الأحداث الحالية";
$lang['serv_events_upcoming'] = "الأحداث القادمة";
$lang['serv_events_invitees'] = "الأعضاء المدعوين";

$lang['serv_action'] = "الطلبات";
$lang['serv_action_from_user'] = "طلب من";
$lang['serv_action_to_user'] = "طلب إلى";
$lang['serv_action_type_id'] = "نوع الطلب";
$lang['serv_action_project_id'] = "المشروع";
$lang['serv_action_add_date'] = "تاريخ التسجيل";
$lang['serv_action_add_time'] = "توقيت التسجيل";
$lang['serv_action_start_date'] = "الرد من تاريخ";
$lang['serv_action_end_date'] = "إلى تاريخ";
$lang['serv_action_details'] = "التفاصيل";
$lang['serv_action_done'] = "تم الرد";
$lang['serv_action_onhold'] = "طلبات معلقة";
$lang['serv_action_expired_onhold'] = "طلبات منتهية ومعلقة";
$lang['serv_action_expired_done'] = "طلبات منتهية ومجابة";
$lang['serv_action_all'] = "جميع الطلبات";
$lang['serv_action_coming'] = "طلبات قادمة";

$lang['profile_custody'] = "العهدة العينية";
$lang['profile_financial_custody'] = "العهدة المالية";

$lang['profile_menu_send_email'] = "إرسال رسالة";
$lang['profile_menu_send_action'] = "إرسال طلب";
$lang['profile_menu_send_vacation'] = "طلب إجازة";
$lang['profile_menu_send_advance'] = "طلب سلفة";

$lang['Orders_filter'] = "فرز الطلبات";
$lang['Outbound_requests'] = "الطلبات الصادرة";
$lang['Incoming_requests'] = "الطلبات الواردة";

$lang['Technical_support'] = "الدعم الفني";

$lang['settings_acceptance_pulic'] = "إعدادات القبول العامة";
$lang['settings_acceptance_arabic_required'] = "ضرورة تسجيل البيانات باللغة العربية";
$lang['settings_acceptance_english_required'] = "ضرورة تسجيل البيانات باللغة الإنجليزية";
$lang['settings_acceptance_upload_required'] = "ضرورة رفع الملفات والصور";
$lang['settings_acceptance_fin_option1'] = "السماح برصيد إفتتاحي صفر";
$lang['settings_acceptance_fin_option2'] = "السماح برصيد صفر للخزينة";
$lang['settings_acceptance_fin_option3'] = "السماح برصيد صفر للبنوك";
$lang['settings_acceptance_purchase_option1'] = "السماح بشراء كميات أكبر من الحد الأقصى للصنف";
$lang['settings_acceptance_sales_option1'] = "السماح ببيع أصناف رصيدها صفر";
$lang['settings_acceptance_sales_option2'] = "أقصى نسبة خصم مسموح به";
$lang['settings_acceptance_store_option1'] = "منع خروج أصناف تحت حد الطلب";
$lang['settings_acceptance_hr_option1'] = "الحد الأقصى للسلفة";
$lang['settings_acceptance_hr_option2'] = "أيام الإجازة السنوية المدفوعة";
$lang['settings_acceptance_hr_option3'] = "أيام الإجازة السنوية الغير مدفوعة";
$lang['settings_acceptance_custody_option1'] = "الحد الأقصى للعهدة";
$lang['settings_acceptance_custody_option2'] = "الحد الأقصى للإنفاق";
$lang['settings_acceptance_service_option1'] = "تبليغ بالايميل";
$lang['settings_acceptance_maintenance_option1'] = "التسعير المرن للخدمات";
$lang['settings_acceptance_manufacturing_option1'] = "رفض أمر التصنيع في حالة نقص الخامات";

$lang['settings_notifications_fin_option1'] = "مديونية عميل أكثر من";
$lang['settings_notifications_fin_option2'] = "مديونية للمورد أكثر من";
$lang['settings_notifications_fin_option3'] = "رصيد الخزينة أقل من";
$lang['settings_notifications_fin_option4'] = "رصيد البنك أقل من";
$lang['settings_notifications_purchase_option1'] = "مبالغ متبقية للمورد تزيد عن";
$lang['settings_notifications_sales_option1'] = "مبالغ مبيعات متبقية تزيد عن";
$lang['settings_notifications_store_option1'] = "الأرصدة الأقل من";
$lang['settings_notifications_hr_option1'] = "عدد أيام الغياب";
$lang['settings_notifications_hr_option2'] = "عدد ساعات التأخير";
$lang['settings_notifications_hr_option3'] = "تسجيل جزاءات";
$lang['settings_notifications_hr_option4'] = "تسجيل مكافأة";
$lang['settings_notifications_custody_option1'] = "مبالغ تزيد عن";
$lang['settings_notifications_custody_option2'] = "مصروفات تزيد عن";
$lang['settings_notifications_service_option1'] = "رسائل لابد الرد عليها";
$lang['settings_notifications_maintenance_option1'] = "المناطق الأقل";
$lang['settings_notifications_maintenance_option2'] = "الخدمات الأقل";
$lang['settings_notifications_manufacturing_option1'] = "نقص خامات التصنيع";

$lang['fin_expected_expenses'] = "المصروفات المتوقعة";
$lang['fin_expected_expenses_account_id'] = "حساب المصروف";
$lang['fin_expected_expenses_amount'] = "المبلغ الشهري";
$lang['fin_expected_expenses_actual'] = "المتوسط الفعلي";

$lang['fin_expected_revenue'] = "الإيرادات المتوقعة";
$lang['fin_expected_revenue_account_id'] = "حساب الإيراد";
$lang['fin_expected_revenue_amount'] = "المبلغ الشهري";
$lang['fin_expected_revenue_actual'] = "المتوسط الفعلي";

$lang['Pointer'] = "المؤشر";
$lang['fin_liquidity_risk_Pointer'] = "مؤشر مخاطر السيولة";

$lang['Flash_success'] = "تم حفظ البيانات بنجاح";
$lang['Flash_error'] = "البيانات المدخلة لايمكن حفظها برجاء تصحيح البيانات";
$lang['Flash_warning'] = "إستكمال البيانات مهم جداً للتقارير والإحصائيات";
$lang['Flash_info'] = "البيانات المدخلة لايمكن حفظها برجاء تصحيح البيانات";

$lang['fin_treecategory'] = "تصنيف الحساب";
$lang['fin_treecategory_all'] = "جميع الحسابات";
$lang['fin_treecategory_1'] = "الأصول";
$lang['fin_treecategory_2'] = "الخصوم";
$lang['fin_treecategory_3'] = "حقوق الملكية";
$lang['fin_treecategory_4'] = "الإيرادات";
$lang['fin_treecategory_5'] = "المصروفات";

$lang['hr_country'] = "قائمة الدول";
$lang['hr_country_ar_title'] = "الاسم العربي";
$lang['hr_country_en_title'] = "الاسم الانجليزي";
$lang['staff_count'] = "عدد الموظفين";

$lang['hr_education'] = "المؤهلات الدراسية";
$lang['hr_education_ar_title'] = "الاسم العربي";
$lang['hr_education_en_title'] = "الاسم الانجليزي";

$lang['hr_holidays'] = "الأعياد الرسمية";
$lang['hr_holidays_ar_title'] = "الاسم العربي";
$lang['hr_holidays_en_title'] = "الاسم الانجليزي";
$lang['hr_holidays_fromdate'] = "من تاريخ";
$lang['hr_holidays_todate'] = "إلى تاريخ";

$lang['hr_job'] = "المسميات الوظيفية";
$lang['hr_job_ar_title'] = "الاسم العربي";
$lang['hr_job_en_title'] = "الاسم الانجليزي";

$lang['hr_job_type'] = "أنواع الوظائف";
$lang['hr_job_type_ar_title'] = "الاسم العربي";
$lang['hr_job_type_en_title'] = "الاسم الانجليزي";

$lang['hr_weekend'] = "الإجازات الإسبوعية";
$lang['hr_weekend_title'] = "اليوم";
$lang['hr_weekend_dayoffer'] = "يحسب بمرتب كم يوم إذا حضر للعمل";

$lang['Saturday'] = "السبت";
$lang['Sunday'] = "الأحد";
$lang['Monday'] = "الإثنين";
$lang['Tuesday'] = "الثلاثاء";
$lang['Wednesday'] = "الأربعاء";
$lang['Thursday'] = "الخميس";
$lang['Friday'] = "الجمعة";

$lang['hr_workgroup'] = "مجموعات العمل";
$lang['hr_workgroup_ar_title'] = "الاسم العربي";
$lang['hr_workgroup_en_title'] = "الاسم الانجليزي";
$lang['hr_workgroup_dayoutwork'] = "عدد أيام الخصم لغياب يوم واحد";

$lang['hr_worktime'] = "أنواع الحركة";
$lang['hr_worktime_ar_title'] = "الاسم العربي";
$lang['hr_worktime_en_title'] = "الاسم الانجليزي";

// workgrouptime
$lang['hr_workgrouptime'] = "منظم مواعيد العمل";
$lang['hr_workgrouptime_workgroup_id'] = "مجموعة العمل";
$lang['hr_workgrouptime_worktime_id'] = "الحركة";
$lang['hr_workgrouptime_fromhoure'] = "من الساعة";
$lang['hr_workgrouptime_tohoure'] = "إلى الساعة";
$lang['hr_workgrouptime_timevalue'] = "إضافة أو خصم";

//hr_vacation_types
$lang['hr_vacation_types'] = "أنواع الإجازات";
$lang['hr_vacation_types_ar_title'] = "الاسم العربي";
$lang['hr_vacation_types_en_title'] = "الاسم الانجليزي";

// hr_vacation
$lang['hr_vacation'] = "الإجازات";
$lang['hr_vacation_employee_id'] = "اسم الموظف";
$lang['hr_vacation_type_id'] = "نوع الإجازة";
$lang['hr_vacation_from_date'] = "من تاريخ";
$lang['hr_vacation_to_date'] = "إلى تاريخ";
$lang['hr_vacation_details'] = "ملاحظات على الإجازة";
$lang['hr_vacation_accept'] = "الموقف من قبول الإجازة";

// hr_employee_subsalary
$lang['hr_employee_subsalary'] = "البدلات والمكافأت والسلف";
$lang['hr_employee_subsalary_employee_id'] = "اسم الموظف";
$lang['hr_employee_subsalary_actionid'] = "نوع العملية";
$lang['hr_employee_subsalary_action1'] = "مكافأة";
$lang['hr_employee_subsalary_action2'] = "خصم";
$lang['hr_employee_subsalary_action3'] = "سلفة";
$lang['hr_employee_subsalary_thedate'] = "التاريخ";
$lang['hr_employee_subsalary_thevalue'] = "المبلغ";
$lang['hr_employee_subsalary_notes'] = "ملاحظات";
$lang['hr_employee_subsalary_tree_id'] = "في حالة تسجيل سلفة برجاء تحديد مصدر الصرف";
$lang['hr_employee_workdays'] = "أيام العمل";
$lang['hr_employee_day_salary'] = "أجر اليوم";
$lang['hr_employee_workdays_salary'] = "أجر العمل";

// employee
$lang['employee'] = "بيانات الموظفين";
$lang['employee_country_id'] = "الجنسية";
$lang['employee_education_id'] = "المستوى التعليمي";
$lang['employee_job_id'] = "الوظيفة";
$lang['employee_job_type_id'] = "نوع التعاقد";
$lang['employee_workgroup_id'] = "مجموعة العمل";
$lang['employee_ar_name'] = "الاسم العربي";
$lang['employee_en_name'] = "الاسم الانجليزي";
$lang['employee_pasport_no'] = "رقم جواز السفر";
$lang['employee_phone'] = "التليفون";
$lang['employee_email'] = "بريد إلكتروني";
$lang['employee_basic_salary'] = "الراتب المستحق";
$lang['employee_salary_days'] = "لعدد كم يوم";
$lang['employee_add_money'] = "بدلات مستمرة";
$lang['employee_deviceno'] = "رقم جهاز البصمة";
$lang['employee_deleted'] = "يعمل حالياً";
$lang['employee_count'] = "عدد الموظفين";
$lang['employee_loan'] = "السلفة";
$lang['employee_custody'] = "العهدة";
$lang['employee_another_address'] = "عنوان آخر";
$lang['employee_another_phone'] = "هاتف آخر";
$lang['employee_another_email'] = "بريد إلكتروني آخر";
// employee_files
$lang['hr_employee_files'] = "وثائق الموظفين";
$lang['hr_employee_files_employee_id'] = "اسم الموظف";
$lang['hr_employee_files_title'] = "اسم الملف";
$lang['hr_employee_files_image'] = "الملف";
// manu_components
$lang['manu_components'] = "مكونات التصنيع";
$lang['manu_components_date_all'] = "كافة السجلات";
$lang['manu_components_user_id'] = "مقدم الأمر";
$lang['manu_components_thedate'] = "التاريخ";
$lang['manu_components_over_cost'] = "مصروفات إضافية";
$lang['manu_components_notes'] = "ملاحظات";

// manu_manufacturing_orders
$lang['manu_manufacturing_orders'] = "أوامر التصنيع";
$lang['manu_manufacturing_orders_date_all'] = "كافة السجلات";
$lang['manu_manufacturing_orders_user_id'] = "مقدم الأمر";
$lang['manu_manufacturing_orders_thedate'] = "التاريخ";
$lang['manu_manufacturing_orders_over_cost'] = "مصروفات إضافية";
$lang['manu_manufacturing_orders_notes'] = "ملاحظات";
$lang['manu_manufacturing_orders_count'] = "الكمية المصنعة";

//statistics
$lang['statistics_Users'] = "عدد المستخدمين";
$lang['statistics_Users_note'] = "المستخدمين الفعالين";
$lang['statistics_Customers'] = "عدد العملاء";
$lang['statistics_Customers_note'] = "مديونية : ";
$lang['statistics_Suppliers'] = "عدد الموردين";
$lang['statistics_Suppliers_note'] = "مديونية : ";
$lang['statistics_Sales'] = "قيمة المبيعات";
$lang['statistics_Sales_note_today'] = "المبيعات خلال اليوم الحالي";
$lang['statistics_Sales_note_month'] = "المبيعات خلال الشهر الحالي";
$lang['statistics_Sales_note_year'] = "المبيعات خلال السنة الحالية";
$lang['statistics_Sales_paid'] = "المبيعات المدفوعة";
$lang['statistics_Sales_remaining'] = "المبيعات المتبقية";
$lang['statistics_Purchase'] = "المشتريات";
$lang['statistics_Purchase_note_today'] = "المشتريات خلال اليوم الحالي";
$lang['statistics_Purchase_note_month'] = "المشتريات خلال الشهر الحالي";
$lang['statistics_Purchase_note_year'] = "المشتريات خلال السنة الحالية";
$lang['statistics_Purchase_paid'] = "المشتريات المدفوعة";
$lang['statistics_Purchase_remaining'] = "المشتريات المتبقية";

$lang['mon_exchange_bonds'] = "سندات الصرف";
$lang['mon_receipts'] = "سندات القبض";

$lang['tickets_count'] = "عدد التذاكر";

$lang['tic_category'] = "تصنيفات التذاكر";
$lang['tic_category_ar_title'] = "الاسم العربي للتصنيف";
$lang['tic_category_en_title'] = "الاسم الانجليزي للتصنيف";
$lang['tic_category_name'] = "التصنيف";

$lang['tic_priority'] = "درجات الأهمية";
$lang['tic_priority_ar_title'] = "الاسم العربي للدرجة";
$lang['tic_priority_en_title'] = "الاسم الانجليزي للدرجة";
$lang['tic_priority_name'] = "درجة الأهمية";

$lang['tic_status'] = "حالات التذاكر";
$lang['tic_status_ar_title'] = "الاسم العربي للحالة";
$lang['tic_status_en_title'] = "الاسم الانجليزي للحالة";
$lang['tic_status_name'] = "الحالة";

$lang['tic_ticket'] = "تذاكر الدعم الفني";
$lang['tic_ticket_category_id'] = "التصنيف";
$lang['tic_ticket_priority_id'] = "درجة الأهمية";
$lang['tic_ticket_status_id'] = "الحالة";
$lang['tic_ticket_customer_id'] = "اسم العميل";
$lang['tic_ticket_user_id'] = "موظف الدعم";
$lang['tic_ticket_assign_to'] = "موجهة إلى";
$lang['tic_ticket_details'] = "التفاصيل";

$lang['Serial'] = "مسلسل";
$lang['Initial_Balance'] = "الرصيد الإفتتاحي";
$lang['Purchases_quantity'] = "كمية المشتريات";
$lang['Quantity_purchase_returns'] = "كمية مردودات المشتريات";
$lang['sales_quantity'] = "كمية المبيعات";
$lang['Quantity_sales_returns'] = "كمية مردودات المبيعات";
$lang['Current_Balance'] = "صافي الحركة";

$lang['pdf_sale_bill'] = "فاتورة مبيعات";
$lang['pdf_sale_returns'] = "فاتورة مردودات مبيعات";
$lang['pdf_sale_offer'] = "عرض سعر مبيعات";
$lang['pdf_buy_bill'] = "فاتورة مشتريات";
$lang['pdf_buy_returns'] = "فاتورة مردودات مشتريات";
$lang['pdf_buy_offer'] = "عرض سعر توريد";
$lang['pdf_Invoice_Editor'] = "محرر الفاتورة";

$lang['January'] = "يناير";
$lang['February'] = "فبراير";
$lang['March'] = "مارس";
$lang['April'] = "أبريل";
$lang['May'] = "مايو";
$lang['June'] = "يونيو";
$lang['July'] = "يوليو";
$lang['August'] = "أغسطس";
$lang['September'] = "سبتمبر";
$lang['October'] = "أكتوبر";
$lang['November'] = "نوفمبر";
$lang['December'] = "ديسمبر";

$lang['highcharts_sales'] = "المبيعات";
$lang['highcharts_Purchase'] = "المشتريات";
$lang['highcharts_sales_paid'] = "النقدي";
$lang['highcharts_sales_remaining'] = "المتبقي";

$lang['Currency'] = "العملة";

$lang['buy_bill_ar_text'] = "ترويسة فاتورة المشتريات العربي";
$lang['buy_bill_en_text'] = "ترويسة فاتورة المشتريات الإنجليزي";
$lang['buy_bill_orientation'] = "إتجاه الطباعة";
$lang['buy_bill_size'] = "حجم الصفحة";
$lang['buy_returns_ar_text'] = "ترويسة فاتورة مردودات المشتريات العربي";
$lang['buy_returns_en_text'] = "ترويسة فاتورة مردودات المشتريات الإنجليزي";
$lang['buy_returns_orientation'] = "إتجاه الطباعة";
$lang['buy_returns_size'] = "حجم الصفحة";
$lang['sale_bill_ar_text'] = "ترويسة فاتورة المبيعات العربي";
$lang['sale_bill_en_text'] = "ترويسة فاتورة المبيعات الإنجليزي";
$lang['sale_bill_orientation'] = "إتجاه الطباعة";
$lang['sale_bill_size'] = "حجم الصفحة";
$lang['sale_returns_ar_text'] = "ترويسة فاتورة مردودات المبيعات العربي";
$lang['sale_returns_en_text'] = "ترويسة فاتورة مردودات المبيعات الإنجليزي";
$lang['sale_returns_orientation'] = "إتجاه الطباعة";
$lang['sale_returns_size'] = "حجم الصفحة";
$lang['data_invoice_ar_text'] = "ترويسة فاتورة الخدمات العربي";
$lang['data_invoice_en_text'] = "ترويسة فاتورة الخدمات الإنجليزي";
$lang['data_invoice_orientation'] = "إتجاه الطباعة";
$lang['data_invoice_size'] = "حجم الصفحة";
$lang['Portrait'] = "عمودي";
$lang['Landscape'] = "أفقي";

$lang['Commission'] = "نسبة العمولة";
$lang['salesperson_id'] = "مندوب المبيعات";

$lang['DataManagement'] = "إدارة البيانات";
$lang['Databse_Optimize'] = "تحسين أداء الجداول";
$lang['Databse_Optimize_Message'] = "تم تحسين أداء الجداول";
$lang['Databse_Repair'] = "إصلاح الجداول";
$lang['Databse_Repair_Message'] = "تم إصلاح الجداول";
$lang['DataDelete'] = "حذف سجلات";
$lang['DataDelete_Message'] = "تم حذف السجل";
$lang['TableName'] = "اسم الجدول";
$lang['RecordNumber'] = "رقم السجل";
$lang['EnterRecordNumber'] = "أدخل رقم السجل";
$lang['DontNeedRecordNumber'] = "رقم السجل غير مهم";
$lang['DatabaseBackeup'] = "النسخ الإحتياطي";
$lang['DatabaseBackeup_Message'] = "تم تنفيذ النسخ الإحتياطي";
$lang['POS_Management'] = "نقاط البيع";
$lang['POS_Management_Details'] = "إدارة وتشغيل نقاط البيع";

$lang['pos_name'] = "اسم المتجر";
$lang['pos_code'] = "الكود";
$lang['pos_email'] = "بريد إلكتروني";
$lang['pos_phone'] = "هاتف";
$lang['pos_address1'] = "عنوان1";
$lang['pos_address2'] = "عنوان2";
$lang['pos_city'] = "المدينة";
$lang['pos_state'] = "المنطقة";
$lang['pos_postal_code'] = "الرمز البريدي";
$lang['pos_country'] = "الدولة";
$lang['pos_currency_code'] = "العملة";
$lang['pos_receipt_header'] = "أعلى الفاتورة";
$lang['pos_receipt_footer'] = "أسفل الفاتورة";
$lang['tec_users'] = "المستخدمين";
$lang['tec_users_first_name'] = "الاسم الأول";
$lang['tec_users_last_name'] = "الاسم الأخير";
$lang['tec_users_active'] = "فعال";
$lang['tec_users_phone'] = "الهاتف";
$lang['tec_users_gender'] = "الجنس";
$lang['tec_users_gender_male'] = "ذكر";
$lang['tec_users_gender_female'] = "أنثى";

//cars_model
$lang['cars_model'] = "موديل السيارة";
$lang['cars_model_title'] = "الاسم العربي";
$lang['cars_model_title_en'] = "الاسم الانجليزي";
$lang['cars_model_active'] = "نوع فعال";
$lang['cars_count'] = "عدد السيارات";

//cars_cars
$lang['cars_cars'] = "بيانات السيارة";
$lang['cars_cars_chassis_no'] = "رقم الشاسية";
$lang['cars_cars_model_id'] = "النوع";
$lang['cars_cars_model_name'] = "الموديل";
$lang['cars_cars_plate_No'] = "رقم اللوحة";
$lang['cars_cars_meter_reading'] = "قراءة العداد";
$lang['cars_cars_motor_number'] = "رقم الموتور";
$lang['cars_cars_color'] = "اللون";
$lang['cars_cars_oil_change_date'] = "تاريخ تغيير الزيت";
$lang['cars_cars_oil_change_every'] = "تغيير الزيت كل كم يوم";
$lang['cars_cars_notes'] = "ملاحظات";
$lang['cars_cars_images'] = "صور السيارة";

$lang['closed_date'] = "تاريخ التسليم";
$lang['closed_time'] = "توقيت التسليم";

$lang['cars_orders'] = "أوامر الصيانة";
$lang['cars_order'] = "أمر الصيانة";
$lang['cars_orders_change_status'] = "تغيير الحالة";
$lang['cars_orders_part_name'] = "قطعة الغيار";
$lang['cars_orders_add_parts'] = "إضافة قطع غيار";

$lang['invoice_sale_start'] = "بادئة ترقيم فواتير المبيعات";
$lang['invoice_sale_return_start'] = "بادئة ترقيم فواتير مردودات المبيعات";
$lang['invoice_buy_start'] = "بادئة ترقيم فواتير المشتريات";
$lang['invoice_buy_return_start'] = "بادئة ترقيم فواتير مردودات المشتريات";
$lang['invoice_service_start'] = "بادئة ترقيم فواتير الخدمات";

$lang['Alerts'] = "تنبيهات";
$lang['Delete_app_company'] = "حذف الفروع : ينتج عنه حذف كافة السجلات والفواتير المرتبطة بالفرع";
$lang['Delete_usr_users'] = "حذف المستخدم : ينتج عنه حذف كافة السجلات والفواتير التي قام المستخدم بتسجيلها";
$lang['Delete_fin_treeaccount'] = "حذف الحساب : ينتج عنه حذف كافة المعاملات المالية وقيود اليومية الخاصة بالحساب ، مما ينتج عنه تأثير مباشر في الأرصدة.";
$lang['Delete_buy_manufacturer'] = "حذف المورد : ينتج عنه حذف كافة المعاملات المالية وقيود اليومية الخاصة بالمورد وكذلك حذف فواتير المشتريات الخاصة بالمورد.";
$lang['Delete_buy_bill'] = "حذف فاتورة مشتريات : ينتج عنه حذف كافة المعاملات المالية وقيود اليومية الخاصة بالمورد والخزينة والبنك وكذلك التأثير على رصيد الأصناف في المخزن.";
$lang['Delete_buy_offer'] = "حذف عرض السعر : لا يؤثر حذف عرض الأسعار على أي حسابات أو أرصدة.";
$lang['Delete_buy_returns'] = "حذف مردودات المشتريات : يؤثر حذف فاتورة مردودات المشتريات على رصيد الأصناف وحساب المورد والخزينة والبنك وقيود اليومية.";
$lang['Delete_sale_customer'] = "حذف بيانات العميل : يؤثر حذف بيانات العميل على رصيد الأصناف والخزينة والبنك وقيود اليومية.";
$lang['Delete_sale_bill'] = "حذف فاتورة المبيعات : يؤثر حذف فاتورة المبيعات على رصيد الأصناف وحساب العميل والخزينة والبنك وقيود اليومية.";
$lang['Delete_sale_offer'] = "حذف عرض السعر : لا يؤثر حذف عرض الأسعار على أي حسابات أو أرصدة.";
$lang['Delete_sale_returns'] = "حذف مردودات المبيعات : يؤثر حذف فاتورة مردودات المبيعات على رصيد الأصناف وحساب العميل والخزينة والبنك وقيود اليومية.";
$lang['Delete_store'] = "حذف بيانات المخزون : يؤثر حذف بيانات المخازن على رصيد الأصناف والفواتير والخزينة والبنك وقيود اليومية.";
$lang['Delete_store_category'] = "حذف بيانات تصنيفات الأصناف : يؤثر حذف بيانات التصنيفات على رصيد الأصناف والفواتير والخزينة والبنك وقيود اليومية.";
$lang['Delete_store_brand'] = "حذف بيانات موديلات الأصناف : يؤثر حذف بيانات الموديلات على رصيد الأصناف والفواتير والخزينة والبنك وقيود اليومية.";
$lang['Delete_product'] = "حذف بيانات الأصناف : يؤثر حذف بيانات الأصناف على رصيد الأصناف والفواتير والخزينة والبنك وقيود اليومية.";
$lang['Delete_cars_cars'] = "حذف بيانات السيارات : يؤثر حذف بيانات السيارات على أوامر الصيانة وفواتير الصيانة والخزينة والبنك وقيود اليومية.";

$lang['sale_customer_email'] = "رسائل العملاء";
$lang['Send_Message_To_Customers'] = "إرسال رسالة للعملاء";
$lang['Send_Notification_To_Customers'] = "إرسال تنبيه وإعلام للعملاء";



$lang['period_movement'] = "حركة الفترة";

$lang['SupplierSign'] = "توقيع المورد";
$lang['payy'] = "صرف";
$lang['Paid'] = "تم الصرف";
$lang['salary_disbursement'] ="صرف الراتب";
$lang['all'] ="الجميع";
$lang['All'] ="الكل";
$lang['PayToAll'] ="صرف للجميع";

?>
