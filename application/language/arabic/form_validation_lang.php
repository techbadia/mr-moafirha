<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');


$lang['required']  = "حقل %s مطلوب.";
$lang['isset']  = "يجب أن يحتوي %s قيمة.";
$lang['valid_email']  = "يجب أن يحتوي %s عنوان بريد إلكتروني صحيح.";
$lang['valid_emails']  = "يجب أن يحتوي الحقل %s على عناوين بريد إلكتروني صحيحة.";
$lang['valid_url']  = "يجب أن يحتوي الحقل %s عنوان URL صحيح.";
$lang['valid_ip']  = "يجب أن يحتوي الحقل %s عنوان IP صحيح.";
$lang['min_length']  = "يجب أن يكون عدد أحرف %s على الأقل %s.";
$lang['max_length']  = "يجب أن لا يزيد عدد أحرف %s عن %s.";
$lang['exact_length']  = "يجب أن يتكون %s من %s أحرف بالضبط.";
$lang['alpha']  = "يجب أن يحتوي %s أحرفاً فقط.";
$lang['alpha_numeric']  = "يجب أن يحتوي %s أحرفاً إنجليزية وأرقاماً فقط.";
$lang['alpha_dash']  = "يجب أن يحتوي %s على أرقام وأحرف وعلامتي - و _ فقط.";
$lang['numeric']  = "يجب أن يحتوي %s على أعداد فقط.";
$lang['is_numeric']  = "يجب أن يحتوي %s أعداد فقط.";
$lang['integer']  = "يجب أن يحتوي %s على عدد صحيح.";
$lang['regex_match']  = "الحقل %s ليس مكتوباً بالنسق الصحيح.";
$lang['matches']  = "قيمة %s لا تطابق قيمة %s.";
$lang['is_unique']  = "يجب ان يحتوي %s على قيمة فريدة فقط.";
$lang['is_natural']  = "يجب أن يحتوي %s أرقام موجبة فقط.";
$lang['is_natural_no_zero'] = "يجب أن يحتوي %s رقماً أكبر من الصفر.";
$lang['decimal']  = "يجب ان يحتوي %s على رقم عشري..";
$lang['less_than']  = "يجب ان يحتوي %1 على عدد اصغر من %s.";
$lang['greater_than']  = "يجب ان يحتوي %1 على عدد اكبر من %s.";
