<?php
$lang['Dashboard'] = "Dashboard";
$lang['Branches'] = "Branches";
$lang['Branch'] = "Branch";
$lang['ChangeHeader'] = "Header image";
$lang['ChangeFooter'] = "Footer image";
$lang['AddBrancheMessage'] = "After registering the new branch, you must adjust the account settings, acceptance settings, and alerts settings, and the header and biller image must be uploaded to publications.";
$lang['Open'] = "Open";
$lang['AppTitle'] = "ERP System";
$lang['AddNew'] = "Add";
$lang['AddRecord'] = "Add record";
$lang['AddInvoice'] = "Add Invoice";
$lang['Edit'] = "Edit";
$lang['Delete'] = "Delete";
$lang['Undo'] = "Restore";
$lang['Deleted'] = "Deleted records";
$lang['DeleteQuestion'] = "Delete ?";
$lang['DeleteOk'] = "Go";
$lang['Yes'] = "Yes";
$lang['No'] = "No";
$lang['View'] = "View";
$lang['Print'] = "Print";
$lang['PDF'] = "PDF";
$lang['Excel'] = "Excel";
$lang['Export'] = "Export";
$lang['Search'] = "Search";
$lang['Home'] = "Home";
$lang['Save'] = "Save";
$lang['SaveAndAdd'] = "Save and add new";
$lang['Back'] = "Back";
$lang['Lock'] = "Lock";
$lang['Exit'] = "Exit";
$lang['Email'] = "Email";
$lang['Phone'] = "Phone";
$lang['ChangeImage'] = "Change Image";
$lang['ChangeFile'] = "Change File";
$lang['ChangeCatalogue'] = "Change Catalogue";
$lang['ChangePassword'] = "Change Password";
$lang['ApplicationStatistics'] = "Application Statistics";

$lang['set_area'] = "المناطق الجغرافية";
$lang['set_area_title'] = "اسم المنطقة";
$lang['set_request_status'] = "أنواع حالات الطلب";
$lang['set_request_status_title'] = "حالة الطلب";
$lang['set_service'] = "قائمة الخدمات";
$lang['set_service_title'] = "اسم الخدمة";
$lang['set_service_price'] = "سعر الخدمة";
$lang['set_teamwork'] = "فرق العمل";
$lang['set_teamwork_title'] = "فريق العمل";
$lang['set_teamwork_items'] = "عناصر فرق العمل";
$lang['set_teamwork_items_title'] = "اسم العنصر";
$lang['set_teamwork_items_details'] = "وصف العنصر";
$lang['usr_users'] = "المستخدمين";
$lang['request_status_statistics'] = "إحصائية حالات طلب الخدمة";
$lang['general_statistics'] = "إحصائيات عامة";
$lang['set_area_statistics'] = "إحصائيات مناطق الطلب";
$lang['services_statistics'] = "إحصائيات طلب الخدمات";
$lang['CurrentMonth'] = "الشهر الحالي";
$lang['teamwork_invoice_statistics'] = "إحصائيات عدد فواتير فرق العمل";
$lang['teamwork_invoice_amount_statistics'] = "إحصائيات قيمة فواتير فرق العمل";
$lang['teamwork_invoice_expenses_statistics'] = "إحصائيات مصروفات فواتير فرق العمل";
$lang['data_teamwork_items_revenues_statistics'] = "إحصائيات إيرادات عناصر فرق العمل";
$lang['data_teamwork_items_expenses_statistics'] = "إحصائيات مصروفات عناصر فرق العمل";


$lang['users_group'] = "Work Groups";
$lang['users_group_ar_title'] = "Arabic name";
$lang['users_group_en_title'] = "English name";
$lang['GroupName'] = "Work Group";
$lang['users_group_users'] = "Users";
$lang['fullname'] = "Full name";
$lang['email'] = "Email";
$lang['active'] = "Active";
$lang['users'] = "Users data";
$lang['active'] = "Active";
$lang['username'] = "User name";
$lang['password'] = "Password";
$lang['image'] = "Image";
$lang['UserStore'] = "Determine which stores can be sold from";
$lang['UserAccounts'] = "Determine which treasury, banks he can handle";

$lang['data_request'] = "Service Requests";
$lang['data_request_user_id'] = "by";
$lang['data_request_customer_id'] = "Customer";
$lang['data_request_status_id'] = "Status";
$lang['data_request_area_id'] = "region";
$lang['data_request_source'] = "Source";
$lang['data_request_details'] = "Details";
$lang['data_request_thedate'] = "Visited date";
$lang['data_request_thetime'] = "Visiting Time";
$lang['data_request_Visiting'] = "Visit date";
$lang['data_request_insert_date'] = "Date of registration";
$lang['data_request_insert_time'] = "Recording Time";
$lang['data_request_All_area'] = "All Areas";
$lang['data_request_All_customers'] = "All Customers";
$lang['data_request_All_status'] = "All Status";
$lang['data_request_All_TeamWork'] = "All TeamWorks";
$lang['data_request_All_Categories'] = "All Categories";
$lang['data_request_All_Priorities'] = "All Priorities";
$lang['data_request_All_Users'] = "All Users";
$lang['data_request_source1'] = "App";
$lang['data_request_source2'] = "Website";
$lang['data_request_source3'] = "Phone";
$lang['data_request_convert'] = "Convert to Invoice";

$lang['data_request_web'] = "Website Requests";

$lang['data_customer'] = "Customers data";
$lang['data_customer_fullname'] = "Full name";
$lang['data_customer_address'] = "Address";
$lang['data_customer_phone'] = "Phone";
$lang['data_customer_email'] = "Email";

$lang['data_invoice'] = "Services invoices";
$lang['pdf_data_invoice'] = "Services invoice";
$lang['data_invoice_teamwork_id'] = "Teamwork";
$lang['data_invoice_services_title'] = "Service";
$lang['data_invoice_services_price'] = "Unit price";
$lang['data_invoice_Done'] = "If the execution is completed, the invoice amount is deposited into an account";

$lang['set_area_income'] = "Region revenue";
$lang['set_service_income'] = "Service Revenue";
$lang['set_teamwork_revenues'] = 'Staff Revenue';
$lang['set_teamwork_expenses'] = "Staff Expenses";
$lang['set_teamwork_net'] = "Profit";

$lang['Other_revenues'] = "Other revenues";
$lang['Other_expenses'] = "Other expenses";
$lang['data_teamwork_items_revenues'] = "Other revenue for teams";
$lang['amount'] = "Amount";
$lang['thedate'] = "Date";
$lang['thetime'] = "Time";
$lang['details'] = "Details";
$lang['data_teamwork_items_expenses'] = "Other expenses for teams";

$lang['qunatity'] = "Qunatity";

$lang['data_account'] = "Records of income and expenses";
$lang['data_account_item_id'] = "Team work element";
$lang['data_account_invoice_id'] = "Invoice number";
$lang['data_account_depit'] = "Depit";
$lang['data_account_credit'] = "Credit";

$lang['from_date'] = "From date";
$lang['to_date'] = "To date";
$lang['Total'] = "Total";
$lang['Convert'] = "Convert";
$lang['AssignToTeamWork'] = "Assigning to a working group";
$lang['schedule'] = "Schedule visits every day";
$lang['TeamWorkTasks'] = "See the tasks being performed by work teams";
$lang['closed'] = "Closed";
$lang['Close'] = "Close";
$lang['set_teamwork_tasks'] = "Team work tasks";
$lang['ServiceList'] = "Required services";
$lang['tasks'] = "Tasks";
$lang['revenues'] = "Revenues";
$lang['expenses'] = "Expenses";
$lang['CloseTask'] = "Close the task";
$lang['close_date'] = "Closing date";
$lang['close_time'] = "Closing time";
$lang['Add_Cost'] = "Add Expenses";
$lang['InvoiceExpenses'] = "Invoice expenses";
$lang['InvoiceNet'] = "Net value of the invoice";
$lang['InvoiceNo'] = "Invoice number";
$lang['Tax'] = "Rate of value-added tax";
$lang['amount_tax'] = "Amount including tax";

$lang['profile_UserProfile'] = "Profile";
$lang['profile_Message'] = "Message";


$lang['serv_message'] = "Messages";
$lang['tas_tasks'] = "Tasks";
$lang['proj_project_level_task'] = "Project Tasks";
$lang['schedule_of_work'] = "Agenda";
$lang['Recent_Notifications'] = "Recent Alerts";
$lang['DueIn'] = "Due In";
$lang['Day'] = "Day";
$lang['UpComingEvents'] = "Upcoming Events";
$lang['Welcome'] = "Welcome";
$lang['QuickActions'] = "Pending Orders";
$lang['PendingAction'] = "Pending Request";
$lang['routine_tasks'] = "routine_tasks'";
$lang['project_tasks'] = "Project Tasks";
$lang['SeeAll'] = "View All";
$lang['ShortCut'] = "Quick Action List";
$lang['ShortCut_account'] = "Manage Accounts";
$lang['ShortCut_account_mon_exchange_bonds'] = "Adding a treasury exchange bond";
$lang['ShortCut_account_mon_receipts'] = "Adding a treasury receipt voucher";
$lang['ShortCut_account_ban_exchange_bonds'] = "Add a bank exchange bond";
$lang['ShortCut_account_ban_receipts'] = "Add a bank receipt voucher";
$lang['ShortCut_purchase'] = "Purchasing Management";
$lang['ShortCut_purchase_buy'] = "Post a Purchase Invoice";
$lang['ShortCut_purchase_buy_return'] = "Post a Purchase Return Invoice";
$lang['ShortCut_project'] = "Project Management";

$lang['ShortCut_Statistics'] = "Statistics";
$lang['ShortCut_Statistics_Account'] = "Accounts statistics";
$lang['ShortCut_Statistics_Account1'] = "Treasury balance";
$lang['ShortCut_Statistics_Account2'] = "Banks balance";
$lang['ShortCut_Statistics_Purchase'] = "Purchase Statistics";
$lang['ShortCut_Statistics_Sales'] = "Sales Statistics";
$lang['ShortCut_Statistics_Store'] = "Store Statistics";
$lang['ShortCut_Statistics_HR'] = "HR Statistics";
$lang['ShortCut_Statistics_HR1'] = "Nationality Statistics";
$lang['ShortCut_Statistics_HR2'] = "Education Statistics";
$lang['ShortCut_Statistics_HR3'] = "Jobs Statistics";
$lang['ShortCut_Statistics_HR4'] = "Contract Statistics";
$lang['ShortCut_Statistics_Manufacturing'] = "Manufacturing Statistics";
$lang['ShortCut_Statistics_Service'] = "Service Statistics";
$lang['ShortCut_Statistics_Technical'] = "Technical support Statistics";

$lang['ShortCut_Reports'] = "Reports";
$lang['ShortCut_Reports_Account'] = "Accounts Reports";
$lang['ShortCut_Reports_Account_1'] = "Storage Report";
$lang['ShortCut_Reports_Account_2'] = "Banks Report";
$lang['ShortCut_Reports_Account_3'] = "Expense Report";
$lang['ShortCut_Reports_Account_4'] = "Debtors Report";
$lang['ShortCut_Reports_Account_5'] = "Creditors Report";
$lang['ShortCut_Reports_Account_6'] = "Receipt Report";
$lang['ShortCut_Reports_Account_7'] = "Payment Report";
$lang['ShortCut_Reports_Account_8'] = "Testament Report";
$lang['ShortCut_Reports_Account_9'] = "Sales Tax Report";
$lang['ShortCut_Reports_Account_10'] = "VAT Report";
$lang['ShortCut_Reports_Purchase'] = "Purchase Reports";
$lang['ShortCut_Reports_Purchase_1'] = "Purchase Invoice Report";
$lang['ShortCut_Reports_Purchase_2'] = "Supplier Account Statement";
$lang['ShortCut_Reports_Purchase_3'] = "Supplier Balances";
$lang['ShortCut_Reports_Purchase_4'] = "Purchase Returns Report";
$lang['ShortCut_Reports_Purchase_5'] = "Purchase Report";
$lang['ShortCut_Reports_Purchase_6'] = "Supplier List";
$lang['ShortCut_Reports_Purchase_7'] = "Purchase Price Report";
$lang['ShortCut_Reports_Purchase_8'] = "Purchase Price Comparison Report";
$lang['ShortCut_Reports_Purchase_9'] = "Quotations Comparison Report";
$lang['ShortCut_Reports_Sales'] = "Sales Reports";
$lang['ShortCut_Reports_Sales_1'] = "Sales Invoice Report";
$lang['ShortCut_Reports_Sales_2'] = "Client Account Statement";
$lang['ShortCut_Reports_Sales_3'] = "Customer Balances";
$lang['ShortCut_Reports_Sales_4'] = "Sales revenue report";
$lang['ShortCut_Reports_Sales_5'] = "Employee sales report";
$lang['ShortCut_Reports_Sales_6'] = "Sales representative report";
$lang['ShortCut_Reports_Sales_7'] = "Sales Price Report";
$lang['ShortCut_Reports_Sales_8'] = "Sales Price Comparison Report";
$lang['ShortCut_Reports_Sales_9'] = "Quotations Comparison Report";
$lang['ShortCut_Reports_Sales_10'] = "Top sales by quantity";
$lang['ShortCut_Reports_Sales_11'] = "Top sales by price";
$lang['ShortCut_Reports_Store'] = "Store Reports";
$lang['ShortCut_Reports_Store_1'] = "Item Actions Report";
$lang['ShortCut_Reports_Store_2'] = "Item Index Report";
$lang['ShortCut_Reports_Store_3'] = "Item Balances Report";
$lang['ShortCut_Reports_Store_4'] = "Items to be supplied";
$lang['ShortCut_Reports_Store_5'] = "Items with negative balance";
$lang['ShortCut_Reports_Store_6'] = "Balances in stores Report";
$lang['ShortCut_Reports_HR'] = "HR Reports";
$lang['ShortCut_Reports_HR_1'] = "Employee Data Report";
$lang['ShortCut_Reports_HR_2'] = "Attendance Report";
$lang['ShortCut_Reports_HR_3'] = "Salary Report";
$lang['ShortCut_Reports_HR_4'] = "Vacations Report";
$lang['ShortCut_Reports_Manufacturing'] = "Manufacturing Report";
$lang['ShortCut_Reports_Service'] = "Service Reports";
$lang['ShortCut_Reports_Service_1'] = "Service Requests Report";
$lang['ShortCut_Reports_Service_2'] = "Service billing report";
$lang['ShortCut_Reports_Technical'] = "Technical support Report";

$lang['Account_statement'] = "Account statement";

$lang['Today'] = "Day";
$lang['Month'] = "Month";
$lang['Year'] = "Year";

//fin_treeaccount
$lang['fin_treeaccount'] = "Accounts tree";
$lang['Page_fin_treeaccount'] = "From this page you can control the chart of accounts";
$lang['fin_treeaccount_category_id'] = "Category";
$lang['fin_treeaccount_parent_id'] = "Main account";
$lang['fin_treeaccount_account_no'] = "Account number";
$lang['fin_treeaccount_title'] = "Arabic name";
$lang['fin_treeaccount_title_en'] = "English name";
$lang['fin_treeaccount_startamount'] = "Initial Balance";
$lang['fin_treeaccount_basic'] = "Basic account";
$lang['fin_treeaccount_level_no'] = "Level number";
$lang['fin_treeaccount_table'] = "Tabular view";
$lang['fin_treeaccount_treeview'] = "Tree view";
$lang['fin_treeaccount_depreciation'] = "Depreciation";
$lang['fin_treeaccount_thedate'] = "Purchase date";
$lang['fin_treeaccount_image'] = "Purchase document";
$lang['fin_treeaccount_purchasing_price'] = "Purchasing price";

//fin_journal
$lang['fin_journal'] = "Daily restrictions";
$lang['Page_fin_journal'] = "Through this page, you can view the journal entries";
$lang['fin_journal_account_id'] = "Account name";
$lang['fin_journal_debit'] = "Debtor";
$lang['fin_journal_creditor'] = "Creditor";
$lang['fin_journal_details'] = "Details";
$lang['fin_journal_thedate'] = "Date";
$lang['fin_journal_image'] = "Entry image";
$lang['fin_journal_bill_id'] = "Invoice number";
$lang['fin_journal_table_name'] = "Type";
$lang['fin_journal_automatic'] = "Automatic entry";
$lang['fin_journal_manual'] = "Manual entry";
$lang['fin_journal_debit_total'] = "Total debtor";
$lang['fin_journal_creditor_total'] = "Total creditor";
$lang['fin_journal_Alert'] = "The debtor and the creditor parties must be equal and the value must be greater than zero";
$lang['fin_journal_Total'] = "Total";
$lang['fin_journal_delete'] = "Do you want to delete?";
$lang['fin_journal_delete_all'] = "Delete All Daily restrictions";



//balance_sheet
$lang['balance_sheet'] = "Balance sheet";
$lang['balancesheet_Assets'] = "Assets";
$lang['balancesheet_TotalAssets'] = "Total assets";
$lang['balancesheet_Liabilities'] = "Liabilities";
$lang['balancesheet_TotalLiabilities'] = "Total liabilities";
$lang['balancesheet_StockholdersEquity'] = "Property rights";
$lang['RetainedEarnings'] = "Earned profits";
$lang['balancesheet_TotalStockholdersEquity'] = "Total Equity";
$lang['TotalLiabilitiesEquity'] = "Total liabilities and equity";

$lang['Trial_Balance'] = "Trial Balance";

//profit_loss
$lang['profit_loss'] = "profits and losses";
$lang['Revenue'] = "Revenues";
$lang['Expenses'] = "Expenses";
$lang['Total'] = "Total";

//buy_manufacturer
$lang['buy_manufacturer'] = "Supplier records";
$lang['Page_buy_manufacturer'] = "From this page you can manage supplier data";
$lang['buy_manufacturer_title'] = "Arabic name";
$lang['buy_manufacturer_title_en'] = "English name";
$lang['buy_manufacturer_phone'] = "Phone";
$lang['buy_manufacturer_mobile'] = "Mobile";
$lang['buy_manufacturer_address'] = "Address";
$lang['buy_manufacturer_email'] = "Email";
$lang['buy_manufacturer_commercial_register'] = "Commercial Registration No";
$lang['buy_manufacturer_tax_card'] = "Tax Number";
$lang['buy_manufacturer_person'] = "Responsible name";
$lang['buy_manufacturer_person_phone'] = "Official phone";
$lang['buy_manufacturer_credit_limit'] = "Credit limit";

//buy_bill
$lang['buy_bill'] = "Purchase bills";
$lang['Page_buy_bill'] = "From this page you can manage purchase invoices";
$lang['buy_bill_supplier_id'] = "Supplier";
$lang['buy_bill_thedate'] = "Date";
$lang['buy_bill_totalvalue'] = "Total amount";
$lang['buy_bill_paid'] = "Paid up";
$lang['buy_bill_remaining'] = "Remaining";
$lang['buy_bill_discount'] = "Discount";
$lang['buy_bill_notes'] = "Notes";
$lang['buy_bill_image'] = "Invoice image";
$lang['buy_bill_tree_id'] = "Pay from an account";

$lang['Pay'] = "Pay";
$lang['buy_bill_Pay'] = "Make a payment from the purchase invoice";
$lang['buy_returns_Pay'] = "Receive payment from the purchase returns invoice";
$lang['sale_bill_pay'] = "Make a payment from the sales invoice";
$lang['sale_returns_pay'] = "Make a payment from the sales returns invoice";
$lang['To_be_paid'] = "To be paid";
$lang['Debit_from_supplier_account'] = "Discount from supplier";
$lang['Debit_from_customer_account'] = "Discount from the customer";

$lang['buy_bill_all'] = "All invoices";
$lang['buy_bill_not_paid'] = "Unpaid";
$lang['buy_bill_part_paid'] = "Partially paid";
$lang['buy_bill_full_paid'] = "Paid in full";
$lang['buy_returns_not_paid'] = "Unpaid";
$lang['buy_returns_part_paid'] = "Partially paid";
$lang['buy_returns_full_paid'] = "Paid in full";

$lang['buy_bill_items_store_type_id'] = "Item";
$lang['buy_bill_items_location_id'] = "Store";
$lang['buy_bill_items_quantity'] = "Quantity";
$lang['buy_bill_items_unitprice'] = "Unit price";

//buy_returns
$lang['buy_returns'] = "Returns purchases";
$lang['Page_buy_returns'] = "From this page, you can control the invoices for purchase returns";
$lang['buy_returns_supplier_id'] = "Supplier";
$lang['buy_returns_thedate'] = "Date";
$lang['buy_returns_totalvalue'] = "Total amount";
$lang['buy_returns_paid'] = "Paid";
$lang['buy_returns_remaining'] = "Remaining";
$lang['buy_returns_discount'] = "Discount";
$lang['buy_returns_notes'] = "Notes";
$lang['buy_returns_image'] = "Invoice image";
$lang['buy_returns_tree_id'] = "Receipt in an account";

$lang['buy_returns_items_store_type_id'] = "Item";
$lang['buy_returns_items_location_id'] = "Store";
$lang['buy_returns_items_quantity'] = "Quantity";
$lang['buy_returns_items_unitprice'] = "Unit price";

//buy_offer
$lang['buy_offer'] = "Quotations";
$lang['buy_offer_supplier_id'] = "Supplier";
$lang['buy_offer_thedate'] = "Date";
$lang['buy_offer_expired_date'] = "Expired date";
$lang['buy_offer_totalvalue'] = "Total amount";
$lang['buy_offer_paid'] = "Paid";
$lang['buy_offer_remaining'] = "Remaining";
$lang['buy_offer_discount'] = "Discount";
$lang['buy_offer_tax'] = "Tax";
$lang['buy_offer_over_cost'] = "Additional expenses";
$lang['buy_offer_notes'] = "Notes";
$lang['buy_offer_image'] = "Attached file";
$lang['buy_offer_ChangeImage'] = "Attachment change";
$lang['buy_offer_tree_id'] = "Pay from an account";
$lang['buy_offer_date_all'] = "All offers";
$lang['buy_offer_date_expired'] = "Outgoing";
$lang['buy_offer_date_not_expired'] = "Unexpired period";
$lang['period_movement'] = "period movement";

$lang['buy_offer_items_store_type_id'] = "Item";
$lang['buy_offer_items_location_id'] = "Store";
$lang['buy_offer_items_quantity'] = "Quantity";
$lang['buy_offer_items_unitprice'] = "Unit price";

//expenses_report
$lang['expenses_report'] = "Expense report";

//store
$lang['store'] = "Stores list";
$lang['Page_store'] = "From this page you can manage stores";
$lang['store_web_store'] = "This is the site store";
$lang['store_parent_id'] = "Main store";
$lang['store_title'] = "Location arabic name";
$lang['store_title_en'] = "Location english name";
$lang['store_address'] = "Store Address ";
$lang['store_email'] = "Email";
$lang['store_phone'] = "Phone";
$lang['store_active'] = "Active";
// $lang['store_user_id'] = "Storekeeper";

//store_move
$lang['store_move'] = "Transfer from store to store";
$lang['Page_store_move'] = "From this page you can follow the transfer of items between stores";
$lang['store_move_user_id'] = "User";
$lang['store_move_from_store'] = "From store";
$lang['store_move_to_store'] = "To store";
$lang['store_move_product_id'] = "Item";
$lang['store_move_product_code'] = "Barcode";
$lang['store_move_quantity'] = "Quantity";
$lang['store_move_thedate'] = "Date";
$lang['store_move_thetime'] = "Time";

//product
$lang['product'] = "Products";
$lang['Page_product'] = "من خلال هذه الصفحة يمكنك الإطلاع على وتعديل وإضافة أصناف الموقع";
$lang['product_category_id'] = "Category";
$lang['product_brand_id'] = "Brand";
$lang['product_barcode'] = "Barcode";
$lang['product_batch_number'] = "Batch No.";
$lang['product_thetitle'] = "Item";
$lang['product_title'] = "Arabic name";
$lang['product_title_en'] = "English name";
$lang['product_details'] = "Arabic details";
$lang['product_details_en'] = "English details";
$lang['product_product_type'] = "Product type";
$lang['product_offer_enddate'] = "Discount end date";
$lang['product_old_price'] = "Before discount";
$lang['product_lowest_price'] = "Minimum unit price";
$lang['product_cost_price'] = "Cost price";
$lang['product_Price'] = "Unit price";
$lang['product_view_no'] = "View No.";
$lang['product_image'] = "Product image";
$lang['product_catalogue'] = "Catalogue";
$lang['product_finished'] = "Finished product";
$lang['product_demand'] = "Demand limit";

//store_quantity
$lang['store_quantity'] = "Items quantity";
$lang['Page_store_quantity'] = "Through this page you can control the quantities of items in stores";
$lang['store_quantity_store_id'] = "Store";
$lang['store_quantity_product_id'] = "Item";
$lang['store_quantity_quantity'] = "Quantity";

//store_category
$lang['store_category'] = "Products category";
$lang['store_category_title'] = "Arabic name";
$lang['store_category_title_en'] = "English name";
$lang['store_category_active'] = "Active";

$lang['product_count'] = "Number of products";

//store_brand
$lang['store_brand'] = "Product models";
$lang['store_brand_title'] = "Arabic name";
$lang['store_brand_title_en'] = "English name";
$lang['store_brand_active'] = "Active";

//expenses
$lang['expenses'] = "Expense records";
$lang['Page_expenses'] = "From this page you can manage your expenses";
$lang['expenses_fromaccount'] = "Outlay";
$lang['expenses_toaccount'] = "Pay from an account";
$lang['expenses_thevalue'] = "Amount";
$lang['expenses_details'] = "Details";
$lang['expenses_thedate'] = "Date";
$lang['expenses_bill_id'] = "Invoice number";
$lang['expenses_image'] = "Receipt image";

//fin_settings
$lang['fin_settings'] = "Accounts settings";
$lang['settings_manage'] = "Manage settings";
$lang['Page_fin_settings'] = "From this page you can adjust account settings";
$lang['acc_sale'] = "Sales account";
$lang['acc_website_sale'] = "Website sales account";
$lang['acc_sale_reurn'] = "Sales returns account";
$lang['acc_buy'] = "Purchasing account";
$lang['acc_buy_reurn'] = "Purchase returns account";
$lang['acc_treasury'] = "Treasury account";
$lang['acc_bank'] = "Banks account";
$lang['acc_expenses'] = "Expense account";
$lang['acc_revenues'] = "Service revenue account";
$lang['acc_debtors'] = "Debtors account";
$lang['acc_creditors'] = "Creditors account";
$lang['acc_capture_papers'] = "Receipt papers accounts";
$lang['acc_payment_papers'] = "Payment papers accounts";
$lang['acc_inventory'] = "Inventory account";
$lang['acc_supplier'] = "Supplier account";
$lang['acc_customer'] = "Customer account";
$lang['acc_material_store'] = "Ores store account";
$lang['acc_damaged_store'] = "Corrupted store account";
$lang['acc_invoice_items'] = "Number of invoice lines";
$lang['acc_custody'] = "Covenant Account";
$lang['acc_hr_loan'] = "Staff loan Account";
$lang['acc_tax_sale'] = "Sales Tax Account";
$lang['acc_tax_sale_percent'] = "Sales Tax percent";
$lang['acc_tax_purchase'] = "VAT Account";
$lang['acc_tax_purchase_percent'] = "VAT percent";
$lang['custody_user_id'] = "User responsible for custody";
$lang['acc_affiliate'] = "Marketers commission account";
$lang['acc_delivery'] = "Delivery fee account";
$lang['acc_factory_cost'] = "Manufacturing cost account";
$lang['acc_sales_commission'] = "Sales commission account";
$lang['acc_fixed_assets'] = "Fixed assets account";
$lang['acc_current_assets'] = "Current assets account";
$lang['fixed_assets'] = "Fixed assets";
$lang['fixed_assets_total'] = "Fixed assets total";
$lang['current_assets'] = "Current assets";
$lang['current_assets_total'] = "Current assets total";
$lang['Total_Assets'] = "Total Assets";
$lang['OwnerEquity_total'] = "Owner's Equity total";
$lang['Liabilities_total'] = "Liabilities total";

$lang['fin_settings_purchase'] = "purchase options";
$lang['fin_settings_purchase_details'] = "purchase options management";
$lang['fin_settings_sale'] = "sales options";
$lang['fin_settings_sale_details'] = "sales options management";
$lang['fin_settings_money'] = "financial options";
$lang['fin_settings_money_details'] = "financial options management";
$lang['fin_settings_hr_loan'] = "loan options";
$lang['fin_settings_hr_loan_details'] = "loan options management";
$lang['fin_settings_tax'] = "tax options";
$lang['fin_settings_tax_details'] = "tax options management";
$lang['fin_settings_debit_balances'] = "debit balances options";
$lang['fin_settings_debit_balances_details'] = "debit balances options management";
$lang['fin_settings_Credit_balances'] = "credit balances options";
$lang['fin_settings_Credit_balances_details'] = "credit balances options management";
$lang['fin_settings_store'] = "store options";
$lang['fin_settings_store_details'] = "store options management";

$lang['usr_usersgroup'] = "User groups";
$lang['usr_users'] = "User data";
$lang['branches_lookup'] = "Can move between branches?";

$lang['Debit'] = "Debit";
$lang['Creditor'] = "Creditor";
$lang['ar_title'] = "Arabic name";
$lang['en_title'] = "English name";
$lang['BankNumber'] = "Account number";
$lang['phone'] = "Phone";
$lang['mobile1'] = "Mobile";
$lang['mobile2'] = "whatsapp";
$lang['address'] = "Address";
$lang['credit_limit'] = "Credit limit";
$lang['startamount'] = "Initial Balance";
$lang['Balance'] = "Balance";
$lang['OneBalance'] = "Balance";
$lang['ManualEntry'] = "Manual restrictions";
$lang['commercial_register'] = "Commercial Record";
$lang['tax_card'] = "Tax card";
$lang['person'] = "Responsible person";
$lang['person_phone'] = "Official phone";

//sale_customer
$lang['sale_customer'] = "Customer data";
$lang['sale_customer1'] = "Customer data";
$lang['sale_fullname'] = "Customer name";
$lang['mobile1'] = "Mobile";
$lang['mobile2'] = "whatsapp";
$lang['address'] = "Address";
$lang['customers_phone_comment'] = "To search, enter the phone number, then press Tab";
$lang['sale_customer_Debt_scheduling'] = "Debt scheduling";
$lang['payment_amount'] = "payment amount";

$lang['orders_convert'] = "Transfer to sales invoice";
//sale_bill
$lang['sale_bill'] = "Sales invoices";
$lang['Page_sale_bill'] = "From this page you can manage your sales invoices";
$lang['sale_bill_customer_id'] = "Client";
$lang['sale_bill_thedate'] = "Date";
$lang['sale_bill_totalvalue'] = "Total amount";
$lang['sale_bill_paid'] = "Paid";
$lang['sale_bill_total_paid'] = "Total paid";
$lang['sale_bill_advance_payment'] = "Advance payment";
$lang['sale_bill_remaining'] = "Remaining";
$lang['sale_bill_discount'] = "Discount";
$lang['sale_bill_notes'] = "Notes";
$lang['sale_bill_image'] = "Invoice image";
$lang['sale_bill_tree_id'] = "Deposit into an account";
$lang['subtotal'] = "Sub total";
$lang['invoice_items'] = "Items count";
$lang['JustDoNot'] = "Only";
$lang['ReceviesSign'] = "Client signature";
$lang['DriverSign'] = "Driver signature";
$lang['SalesmanSign'] = "Sales man signature";
$lang['SendMessageToStore'] = "Send a message to the storekeeper";
$lang['sale_bill_customer_name'] = "In case of cash sale, enter customer name";
$lang['sale_bill_customer_phone'] = "In case of cash sale, enter customer phone";
$lang['sale_bill_Cash_sales'] = "Cash sales account";
$lang['received'] = "received";
$lang['Quantities_not_fully_delivered'] = "Quantities not fully delivered";
$lang['Delivery'] = "Delivery";

$lang['sale_bill_items_store_type_id'] = "Item";
$lang['sale_bill_items_location_id'] = "Store";
$lang['sale_bill_items_quantity'] = "Quantity";
$lang['sale_bill_items_available'] = "Available";
$lang['sale_bill_items_unitprice'] = "Unit price";
$lang['sale_bill_items_price'] = "Price";
$lang['sale_bill_items_total'] = "Total";
$lang['Payment_options'] = "Payment options";
$lang['Payment_options_details'] = "You can pay in multiple ways at the same time";
$lang['sale_bill_treasury'] = "Treasury account";
$lang['sale_bill_bank'] = "Bank or network account";
$lang['sale_bill_customer_money'] = "Customer balance";

//sale_returns
$lang['sale_returns'] = "Sales returns";
$lang['Page_sale_returns'] = "From this page you can manage the invoices for sales returns";
$lang['sale_returns_customer_id'] = "Customer";
$lang['sale_returns_thedate'] = "Date";
$lang['sale_returns_totalvalue'] = "Total amount";
$lang['sale_returns_paid'] = "Paid";
$lang['sale_returns_remaining'] = "Remaining";
$lang['sale_returns_discount'] = "Discount";
$lang['sale_returns_notes'] = "Notes";
$lang['sale_returns_image'] = "Invoice image";
$lang['sale_returns_tree_id'] =  "Pay from an account";

$lang['sale_returns_items_store_type_id'] = "Item";
$lang['sale_returns_items_location_id'] = "Store";
$lang['sale_returns_items_quantity'] = "Quantity";
$lang['sale_returns_items_unitprice'] = "Unit price";

//sale_offer
$lang['sale_offer'] = "Sales Offers";
$lang['Page_sale_offer'] = "From this page you can manage your sales offers";
$lang['sale_offer_customer_id'] = "Customer";
$lang['sale_offer_thedate'] = "Date";
$lang['sale_offer_totalvalue'] = "Total amount";
$lang['sale_offer_paid'] = "Paid";
$lang['sale_offer_remaining'] = "Remaining";
$lang['sale_offer_discount'] = "Discount";
$lang['sale_offer_notes'] = "Notes";
$lang['sale_offer_image'] = "Invoice image";
$lang['sale_offer_tree_id'] = "Deposit into an account";

$lang['sale_offer_items_store_type_id'] = "Item";
$lang['sale_offer_items_location_id'] = "Store";
$lang['sale_offer_items_quantity'] = "Quantity";
$lang['sale_offer_items_unitprice'] = "Unit price";

//hr
//hr_inouttimedata
$lang['Absence'] = "Absence";
$lang['Absence_Vacation'] = "Absence or vacation";
$lang['hr_employee_id'] = "Employee name";
$lang['hr_in_date'] = "Attendance date";
$lang['hr_in_time'] = "Attendance time";
$lang['hr_out_date'] = "Departure date";
$lang['hr_out_time'] = "Departure time";

//tas_tasks
$lang['tasks_from_user'] = "From user";
$lang['tasks_thedate'] = "Date";
$lang['tasks_thetime'] = "Time";
$lang['tasks_title'] = "Task name";
$lang['tasks_details'] = "Task details";
$lang['tasks_task_type'] = "Task type";
$lang['tasks_status'] = "Status";
$lang['tasks_levels'] = "Levels";
$lang['tasks_to_user'] = "Executor";
$lang['tasks_start_date'] = "Start date";
$lang['tasks_start_time'] = "Start time";
$lang['tasks_end_date'] = "Expiry date";
$lang['tasks_end_time'] = "Expiry time";
$lang['tasks_days'] = "Every how many days?";
$lang['tasks_doloop'] = "Number of iterations";

$lang['tasks_table_title'] = "Task";
$lang['tasks_table_fromuser'] = "Caretaker";
$lang['tasks_table_touser'] = "User-oriented";
$lang['tasks_table_levels'] = "Level";
$lang['tasks_table_start'] = "Start";
$lang['tasks_table_end'] = "End";

$lang['tasks_start'] = "Start";
$lang['tasks_Failed'] = "Failure";
$lang['tasks_cancel'] = "Cancellation";
$lang['tasks_Time_spent'] = "Time spent";

// proj_status
$lang['proj_status'] = "Project cases";
$lang['proj_status_ar_title'] = "Arabic name";
$lang['proj_status_en_title'] = "English name";
$lang['proj_status_projects'] = "Projects";
$lang['proj_status_Incoming'] = "Incoming";
$lang['proj_status_Outgoing'] = "Outgoing";

// proj_project
$lang['proj_project'] = "Projects";
$lang['proj_project_customer_id'] = "Customer name";
$lang['proj_project_status_id'] = "Project state";
$lang['proj_project_manager_id'] = "Project manager";
$lang['proj_project_title'] = "Project name";
$lang['proj_project_ar_title'] = "Arabic name";
$lang['proj_project_en_title'] = "English name";
$lang['proj_project_ar_details'] = "Arabic details";
$lang['proj_project_en_details'] = "English details";
$lang['proj_project_start_date'] = "Start date";
$lang['proj_project_end_date'] = "Delivery date";
$lang['proj_project_labor_cost'] = "Labor cost";
$lang['proj_project_material_cost'] = "Materials cost";
$lang['proj_project_total_cost'] = "Total cost";
$lang['proj_project_amount'] = "Contract value";
$lang['proj_project_image'] = "Project Logo";
$lang['proj_project_remaining'] = "Remaining time";
$lang['proj_project_convert'] = "Implementation start";

$lang['proj_project_devices'] = "Devices";
$lang['proj_project_project_id'] = "Project";
$lang['proj_project_barcode'] = "Barcode";
$lang['proj_project_details'] = "Details";
$lang['proj_project_plugin_date'] = "Plugin date";
$lang['proj_project_building_no'] = "Building No.";
$lang['proj_project_floor_no'] = "Floor No.";
$lang['proj_project_room_no'] = "Room No.";

$lang['proj_project_Earnings'] = "Revenues";
$lang['proj_project_Expenses'] = "Operating expenses";
$lang['proj_project_Labor'] = "Wages";
$lang['proj_project_Material'] = "Bills of materials";
$lang['Progress'] = "Rate of progress";
$lang['proj_project_TotalExpenses'] = "Expenses";
$lang['proj_project_Net'] = "Net";
$lang['proj_project_AddStaff'] = "Add an employee to the project";
// proj_project_files
$lang['proj_project_files'] = "Project files";
$lang['multiple_files'] = "(Supports multiple files)";
$lang['Choose_files'] = "Select files";

//proj_project_files
$lang['proj_project_files'] = "Project files";
$lang['proj_project_files_project_id'] = "Project";
$lang['proj_project_files_level_id'] = "Level";
$lang['proj_project_files_task_id'] = "Task";
$lang['proj_project_files_ar_title'] = "Arabic name";
$lang['proj_project_files_en_title'] = "English name";
$lang['proj_project_files_image'] = "File";

//proj_project_level
$lang['proj_project_level'] = "Projects levels";
$lang['proj_project_level_project_id'] = "Project";
$lang['proj_project_level_manager_id'] = "Stage administrator";
$lang['proj_project_level_status_id'] = "Phase condition";
$lang['proj_project_level_ar_title'] = "Arabic name";
$lang['proj_project_level_en_title'] = "English name";
$lang['proj_project_level_start_date'] = "Starting date";
$lang['proj_project_level_end_date'] = "Delivery date";

//proj_project_level
$lang['proj_project_level_task'] = "مهام المشاريع";
$lang['proj_project_level_task_project_id'] = "المشروع";
$lang['proj_project_level_task_level_id'] = "المرحلة";
$lang['proj_project_level_task_user_id'] = "مسئول المرحلة";
$lang['proj_project_level_task_status_id'] = "حالة المهمة";
$lang['proj_project_level_task_ar_details'] = "الوصف العربي";
$lang['proj_project_level_task_en_details'] = "الوصف الانجليزي";
$lang['proj_project_level_task_start_date'] = "تاريخ البدء";
$lang['proj_project_level_task_end_date'] = "تاريخ التسليم";

//serv_message
$lang['serv_message'] = "Mail box";
$lang['serv_message_new'] = "New message";
$lang['serv_message_update'] = "Edit the message";
$lang['serv_message_from_user'] = "Sender";
$lang['serv_message_to_user'] = "Recipient";
$lang['serv_message_subject'] = "Subject";
$lang['serv_message_message'] = "Message";
$lang['serv_message_thedate'] = "Date, Time";
$lang['serv_message_index'] = "Incoming";
$lang['serv_message_image'] = "Attached";
$lang['serv_message_marked'] = "Marked";
$lang['serv_message_draft'] = "Draft";
$lang['serv_message_sent'] = "Sent";
$lang['serv_message_trash'] = "Trash";
$lang['serv_message_replay'] = "Replay";
$lang['serv_message_send_replay'] = "Send replay";
$lang['serv_message_submit'] = "Send message";
$lang['serv_message_save_draft'] = "Temporary save";
$lang['serv_message_filter'] = "Filter";

$lang['serv_events'] = "Events";
$lang['serv_events_start_date'] = "From date";
$lang['serv_events_end_date'] = "To date";
$lang['serv_events_details'] = "Event details";
$lang['serv_events_is_scheduled'] = "Scheduled event";
$lang['serv_events_scheduled_month'] = "Monthly scheduling";
$lang['serv_events_scheduled_year'] = "Annual scheduling";
$lang['serv_events_count'] = "Number of invitees";
$lang['serv_events_scheduled'] = "Scheduled";
$lang['serv_events_scheduled_month'] = "Monthly scheduling";
$lang['serv_events_scheduled_year'] = "Annual scheduling";
$lang['serv_events_all'] = "All events";
$lang['serv_events_old'] = "Past events";
$lang['serv_events_current'] = "Current events";
$lang['serv_events_upcoming'] = "Upcoming events";
$lang['serv_events_invitees'] = "Invited Members";

$lang['serv_action'] = "Requests";
$lang['serv_action_from_user'] = "request from";
$lang['serv_action_to_user'] = "request to";
$lang['serv_action_type_id'] = "Request Type";
$lang['serv_action_project_id'] = "The Project";
$lang['serv_action_add_date'] = "Registration Date";
$lang['serv_action_add_time'] = "Recording Time";
$lang['serv_action_start_date'] = "Reply from date";
$lang['serv_action_end_date'] = "to date";
$lang['serv_action_details'] = "details";
$lang['serv_action_done'] = "answered";
$lang['serv_action_onhold'] = "Pending Requests";
$lang['serv_action_expired_onhold'] = "Completed and Pending Requests";
$lang['serv_action_expired_done'] = "completed requests and answered";
$lang['serv_action_all'] = "All Orders";
$lang['serv_action_coming'] = "Incoming Requests";

$lang['profile_custody'] = "in-kind custody";
$lang['profile_financial_custody'] = "Financial Custody";

$lang['profile_menu_send_email'] = "Send Message";
$lang['profile_menu_send_action'] = "Submit Request";
$lang['profile_menu_send_vacation'] = "Leave Request";
$lang['profile_menu_send_advance'] = "Advance Request";

$lang['Orders_filter'] = "Sort Orders";
$lang['Outbound_requests'] = "Outbound Orders";
$lang['Incoming_requests'] = "Incoming Requests";

$lang['Technical_support'] = "Technical Support";

$lang['settings_acceptance_pulic'] = "General Acceptance Settings";
$lang['settings_acceptance_arabic_required'] = "The need to record data in Arabic";
$lang['settings_acceptance_english_required'] = "The data must be recorded in English";
$lang['settings_acceptance_upload_required'] = "Upload files and images";
$lang['settings_acceptance_fin_option1'] = "Allow a zero opening balance";
$lang['settings_acceptance_fin_option2'] = "Allow zero balance to the safe";
$lang['settings_acceptance_fin_option3'] = "Allow zero balance for banks";
$lang['settings_acceptance_purchase_option1'] = "Allow purchase of quantities greater than the maximum for the item";
$lang['settings_acceptance_sales_option1'] = "Allow items with zero balance to be sold";
$lang['settings_acceptance_sales_option2'] = "Maximum discount allowed";
$lang['settings_acceptance_store_option1'] = "Prevent items under the order limit from leaving";
$lang['settings_acceptance_hr_option1'] = "Maximum Advance Limit";
$lang['settings_acceptance_hr_option2'] = "annual paid vacation days";
$lang['settings_acceptance_hr_option3'] = "Unpaid annual leave days";
$lang['settings_acceptance_custody_option1'] = "Maximum Custody";
$lang['settings_acceptance_custody_option2'] = "Maximum spend";
$lang['settings_acceptance_service_option1'] = "Email notification";
$lang['settings_acceptance_maintenance_option1'] = "Flexible pricing for services";
$lang['settings_acceptance_manufacturing_option1'] = "Manufacturing order rejected in case of material shortage";

$lang['settings_notifications_fin_option1'] = "Overdue Customer";
$lang['settings_notifications_fin_option2'] = "Supplier owes more than";
$lang['settings_notifications_fin_option3'] = "Treasury balance is less than";
$lang['settings_notifications_fin_option4'] = "Bank balance is less than";
$lang['settings_notifications_purchase_option1'] = "Additional amounts remaining for the supplier";
$lang['settings_notifications_sales_option1'] = "Residual Sales Amounts In More Than";
$lang['settings_notifications_store_option1'] = "Balances Less Than";
$lang['settings_notifications_hr_option1'] = "Number of days of absence";
$lang['settings_notifications_hr_option2'] = "Hours of delay";
$lang['settings_notifications_hr_option3'] = "Penalties Register";
$lang['settings_notifications_hr_option4'] = "Sign Up Bonus";
$lang['settings_notifications_custody_option1'] = "Over Amounts";
$lang['settings_notifications_custody_option2'] = "Overheads";
$lang['settings_notifications_service_option1'] = "Messages must be answered";
$lang['settings_notifications_maintenance_option1'] = "Less Areas";
$lang['settings_notifications_maintenance_option2'] = "Less services";
$lang['settings_notifications_manufacturing_option1'] = "Shortage of manufacturing materials";

$lang['fin_expected_expenses'] = "expected expense";
$lang['fin_expected_expenses_account_id'] = "Expense Account";
$lang['fin_expected_expenses_amount'] = "monthly amount";
$lang['fin_expected_expenses_actual'] = "Actual Average";

$lang['fin_expected_revenue'] = "expected revenue";
$lang['fin_expected_revenue_account_id'] = "Revenue Calculation";
$lang['fin_expected_revenue_amount'] = "monthly amount";
$lang['fin_expected_revenue_actual'] = "actual average";

$lang['Pointer'] = "Pointer";
$lang['fin_liquidity_risk_Pointer'] = "Liquidity Risk Index";

$lang['Flash_success'] = "The data was saved successfully";
$lang['Flash_error'] = "The entered data cannot be saved. Please correct the information";
$lang['Flash_warning'] = "Completing data is very important for reports and statistics";
$lang['Flash_info'] = "The entered data cannot be saved. Please correct the information";

$lang['fin_treecategory'] = "Account Category";
$lang['fin_treecategory_all'] = "All Accounts";
$lang['fin_treecategory_1'] = "Assets";
$lang['fin_treecategory_2'] = "Liabilities";
$lang['fin_treecategory_3'] = "Equity";
$lang['fin_treecategory_4'] = "Revenue";
$lang['fin_treecategory_5'] = "Expenses";

$lang['hr_country'] = "Country List";
$lang['hr_country_en_title'] = "Arabic Name";
$lang['hr_country_en_title'] = "English Name";
$lang['staff_count'] = "number of employees";

$lang['hr_education'] = "Academic qualifications";
$lang['hr_education_ar_title'] = "Arabic Name";
$lang['hr_education_en_title'] = "English Name";

$lang['hr_holidays'] = "Public Holidays";
$lang['hr_holidays_ar_title'] = "Arabic Name";
$lang['hr_holidays_en_title'] = "English Name";
$lang['hr_holidays_fromdate'] = "From date";
$lang['hr_holidays_todate'] = "To Date";

$lang['hr_job'] = "Job Titles";
$lang['hr_job_ar_title'] = "Arabic Name";
$lang['hr_job_en_title'] = "English Name";

$lang['hr_job_type'] = "Job Types";
$lang['hr_job_type_ar_title'] = "Arabic Name";
$lang['hr_job_type_en_title'] = "English Name";

$lang['hr_weekend'] = "weekends off";
$lang['hr_weekend_title'] = "Today";
$lang['hr_weekend_dayoffer'] = "It is calculated according to the salary of how many days if he comes to work";

$lang['Saturday'] = "Saturday";
$lang['Sunday'] = "Sunday";
$lang['Monday'] = "Monday";
$lang['Tuesday'] = "Tuesday";
$lang['Wednesday'] = "Wednesday";
$lang['Thursday'] = "Thursday";
$lang['Friday'] = "Friday";

$lang['hr_workgroup'] = "Workgroups";
$lang['hr_workgroup_ar_title'] = "Arabic Name";
$lang['hr_workgroup_en_title'] = "English Name";
$lang['hr_workgroup_dayoutwork'] = "The number of discount days for the absence of one day";

$lang['hr_worktime'] = "Attendance Types";
$lang['hr_worktime_ar_title'] = "Arabic Name";
$lang['hr_worktime_en_title'] = "English Name";

// workgrouptime
$lang['hr_workgrouptime'] = "Work Timing Organizer";
$lang['hr_workgrouptime_workgroup_id'] = "Workgroup";
$lang['hr_workgrouptime_worktime_id'] = "Traffic";
$lang['hr_workgrouptime_fromhoure'] = "From the Hour";
$lang['hr_workgrouptime_tohoure'] = "To Hourly";
$lang['hr_workgrouptime_timevalue'] = "add or discount";

//hr_vacation_types
$lang['hr_vacation_types'] = "Vacation Types";
$lang['hr_vacation_types_en_title'] = "Arabic Name";
$lang['hr_vacation_types_en_title'] = "English Name";

// hr_vacation
$lang['hr_vacation'] = "Vacations";
$lang['hr_vacation_employee_id'] = "Employee Name";
$lang['hr_vacation_type_id'] = "Leave Type";
$lang['hr_vacation_from_date'] = "from date";
$lang['hr_vacation_to_date'] = "To Date";
$lang['hr_vacation_details'] = "Leave Notes";
$lang['hr_vacation_accept'] = "The position on accepting the leave";

// hr_employee_subsalary
$lang['hr_employee_subsalary'] = "Allowances, Bonuses and Advances";
$lang['hr_employee_subsalary_employee_id'] = "Employee Name";
$lang['hr_employee_subsalary_actionid'] = "operation type";
$lang['hr_employee_subsalary_action1'] = "bonus";
$lang['hr_employee_subsalary_action2'] = "discount";
$lang['hr_employee_subsalary_action3'] = 'Advance';
$lang['hr_employee_subsalary_thedate'] = "date";
$lang['hr_employee_subsalary_thevalue'] = "amount";
$lang['hr_employee_subsalary_notes'] = "Notes";
$lang['hr_employee_subsalary_tree_id'] = "In the case of registering a loan, please specify the source of disbursement";
$lang['hr_employee_workdays'] = "Work days";
$lang['hr_employee_day_salary'] = "Day salary";
$lang['hr_employee_workdays_salary'] = "Work salary";

// employee
$lang ['employee'] = "Employee Information";
$lang['employee_country_id'] = "Nationality";
$lang['employee_education_id'] = "Education level";
$lang['employee_job_id'] = "Job";
$lang['employee_job_type_id'] = "Contract Type";
$lang['employee_workgroup_id'] = "Workgroup";
$lang['employee_ar_name'] = "Arabic Name";
$lang['employee_en_name'] = "English Name";
$lang['employee_pasport_no'] = "Passport Number";
$lang['employee_phone'] = "phone";
$lang['employee_email'] = "Email Address";
$lang['employee_basic_salary'] = "salary due";
$lang['employee_salary_days'] = "for how many days";
$lang['employee_add_money'] = "ongoing allowances";
$lang['employee_deviceno'] = "fingerprint device number";
$lang['employee_deleted'] = "Currently working";
$lang['employee_count'] = "Number of Employees";
$lang['employee_loan'] = "Loan";
$lang['employee_custody'] = "Custody";
$lang['employee_another_address'] = "Another address";
$lang['employee_another_phone'] = "Another phone";
$lang['employee_another_email'] = "Another email";
// employee_files
$lang ['hr_employee_files'] = "Employee Documents";
$lang['hr_employee_files_employee_id'] = "employee name";
$lang['hr_employee_files_title'] = "filename";
$lang['hr_employee_files_image'] = "file";
// manu_components
$lang ['manu_components'] = "Manufacturing components";
$lang['manu_components_date_all'] = "All Records";
$lang['manu_components_user_id'] = "command submitter";
$lang['manu_components_thedate'] = "date";
$lang['manu_components_over_cost'] = "Additional Charges";
$lang['manu_components_notes'] = "Notes";

// manu_manufacturing_orders
$lang ['manu_manufacturing_orders'] = "Manufacturing Orders";
$lang['manu_manufacturing_orders_date_all'] = "all records";
$lang['manu_manufacturing_orders_user_id'] = "Order Submitter";
$lang['manu_manufacturing_orders_thedate'] = "date";
$lang['manu_manufacturing_orders_over_cost'] = "Additional expenses";
$lang['manu_manufacturing_orders_notes'] = "Notes";
$lang['manu_manufacturing_orders_count'] = "Manufactured Quantity";

//statistics
$lang ['statistics_Users'] = "Number of Users";
$lang['statistics_Users_note'] = "Active Users";
$lang['statistics_Customers'] = "Number of Clients";
$lang['statistics_Customers_note'] = "Liability:";
$lang['statistics_Suppliers'] = "number of suppliers";
$lang['statistics_Suppliers_note'] = "Liability:";
$lang['statistics_Sales'] = "Sales Amount";
$lang['statistics_Sales_note_today'] = "Current Day Sales";
$lang['statistics_Sales_note_month'] = "Sales during the current month";
$lang['statistics_Sales_note_year'] = "Current Year Sales";
$lang['statistics_Sales_paid'] = 'Paid Sales';
$lang['statistics_Sales_remaining'] = "Remaining Sales";
$lang['statistics_Purchase'] = "Purchases";
$lang['statistics_Purchase_note_today'] = "Current Day Purchases";
$lang['statistics_Purchase_note_month'] = "Purchases during the current month";
$lang['statistics_Purchase_note_year'] = "Purchases during the current year";
$lang['statistics_Purchase_paid'] = 'Paid Purchase';
$lang['statistics_Purchase_remaining'] = 'Remaining Purchases';

$lang['mon_exchange_bonds'] = "Exchange vouchers";
$lang['mon_receipts'] = "Receipts";

$lang['tickets_count'] = "Tickets count";

$lang['tic_category'] = "Tickets category";
$lang['tic_category_ar_title'] = "Arabic name";
$lang['tic_category_en_title'] = "English Name";
$lang['tic_category_name'] = "Category";

$lang['tic_priority'] = "Tickets priority";
$lang['tic_priority_ar_title'] = "Arabic name";
$lang['tic_priority_en_title'] = "English Name";
$lang['tic_priority_name'] = "Priority";

$lang['tic_status'] = "Tickets status";
$lang['tic_status_ar_title'] = "Arabic name";
$lang['tic_status_en_title'] = "English Name";
$lang['tic_status_name'] = "Status";

$lang['tic_ticket'] = "Tickets";
$lang['tic_ticket_category_id'] = "Category";
$lang['tic_ticket_priority_id'] = "Priority";
$lang['tic_ticket_status_id'] = "Status";
$lang['tic_ticket_customer_id'] = "Customer name";
$lang['tic_ticket_user_id'] = "technical support user";
$lang['tic_ticket_assign_to'] = "Assign to";
$lang['tic_ticket_details'] = "Details";

$lang['Serial'] = "Serial";
$lang['Initial_Balance'] = "Initial Balance";
$lang['Purchases_quantity'] = "purchases quantity";
$lang['Quantity_purchase_returns'] = "Quantity of purchase returns";
$lang['sales_quantity'] = "sales quantity";
$lang['Quantity_sales_returns'] = "Quantity of sales returns";
$lang['Current_Balance'] = "Net balance";

$lang['pdf_sale_bill'] = "Sales Invoice";
$lang['pdf_sale_returns'] = "Sales Return Invoice";
$lang['pdf_sale_offer'] = "Sales Quote";
$lang['pdf_buy_bill'] = "Purchase Invoice";
$lang['pdf_buy_returns'] = "Purchase returns invoice";
$lang['pdf_buy_offer'] = "Supply Quotation";
$lang['pdf_Invoice_Editor'] = "Invoice Editor";

$lang['January'] = "January";
$lang['February'] = "February";
$lang['March'] = "March";
$lang['April'] = "April";
$lang['May'] = "May";
$lang['June'] = "June";
$lang['July'] = "July";
$lang['August'] = "August";
$lang['September'] = "September";
$lang['October'] = "October";
$lang['November'] = "November";
$lang['December'] = "December";

$lang['highcharts_sales'] = "Sales";
$lang['highcharts_Purchase'] = "Purchase";
$lang['highcharts_sales_paid'] = "Paid";
$lang['highcharts_sales_remaining'] = "Remaining";

$lang['Currency'] = "Currency";

$lang['buy_bill_ar_text'] = "Arabic purchase invoice text";
$lang['buy_bill_en_text'] = "English purchase invoice text";
$lang['buy_bill_orientation'] = "Page orientation";
$lang['buy_bill_size'] = "Page size";
$lang['buy_returns_ar_text'] = "Arabic purchase returns invoice text";
$lang['buy_returns_en_text'] = "English purchase returns invoice text";
$lang['buy_returns_orientation'] = "Page orientation";
$lang['buy_returns_size'] = "Page size";
$lang['sale_bill_ar_text'] = "Arabic sales invoice text";
$lang['sale_bill_en_text'] = "English sales invoice text";
$lang['sale_bill_orientation'] = "Page orientation";
$lang['sale_bill_size'] = "Page size";
$lang['sale_returns_ar_text'] = "Arabic sales returns invoice text";
$lang['sale_returns_en_text'] = "English sales returns invoice text";
$lang['sale_returns_orientation'] = "Page orientation";
$lang['sale_returns_size'] = "Page size";
$lang['data_invoice_ar_text'] = "Arabic service invoice text";
$lang['data_invoice_en_text'] = "English service invoice text";
$lang['data_invoice_orientation'] = "Page orientation";
$lang['data_invoice_size'] = "Page size";
$lang['Portrait'] = "Portrait";
$lang['Landscape'] = "Landscape";

$lang['Commission'] = "Commission";
$lang['salesperson_id'] = "Salesperson";

$lang['DataManagement'] = "Data Management";
$lang['Databse_Optimize'] = "Optimize Tables";
$lang['Databse_Optimize_Message'] = "Repair Tables Success!";
$lang['Databse_Repair'] = "Repair Tables";
$lang['Databse_Repair_Message'] = "Repair Tables Success!";
$lang['DataDelete'] = "Delete Rows";
$lang['DataDelete_Message'] = "Delete Rows Success!";
$lang['TableName'] = "Table Name";
$lang['RecordNumber'] = "Record Number";
$lang['EnterRecordNumber'] = "Enter Record Number";
$lang['DontNeedRecordNumber'] = "Do not enter the record number";
$lang['DatabaseBackeup'] = "Database Backeup";
$lang['DatabaseBackeup_Message'] = "Database Backeup Success!";
$lang['POS_Management'] = "POS";
$lang['POS_Management_Details'] = "POS Management and update";

$lang['pos_name'] = "Store name";
$lang['pos_code'] = "Code";
$lang['pos_email'] = "Email";
$lang['pos_phone'] = "Phone";
$lang['pos_address1'] = "Address1";
$lang['pos_address2'] = "Address2";
$lang['pos_city'] = "City";
$lang['pos_state'] = "State";
$lang['pos_postal_code'] = "Postal code";
$lang['pos_country'] = "Country";
$lang['pos_currency_code'] = "Currency code";
$lang['pos_receipt_header'] = "Receipt header";
$lang['pos_receipt_footer'] = "Receipt footer";
$lang['tec_users'] = "Users";
$lang['tec_users_first_name'] = "First name";
$lang['tec_users_last_name'] = "Last name";
$lang['tec_users_active'] = "Active";
$lang['tec_users_phone'] = "Phone";
$lang['tec_users_gender'] = "Gender";
$lang['tec_users_gender_male'] = "Male";
$lang['tec_users_gender_female'] = "Female";

//cars_model
$lang['cars_model'] = "Car Type";
$lang['cars_model_title'] = "Arabic name";
$lang['cars_model_title_en'] = "English name";
$lang['cars_model_active'] = "Active type";
$lang['cars_count'] = "car numbers";

//cars_cars
$lang['cars_cars'] = "Auto data";
$lang['cars_cars_chassis_no'] = "Chassis Number";
$lang['cars_cars_model_id'] = "Type";
$lang['cars_cars_model_name'] = "Model";
$lang['cars_cars_plate_No'] = "Plate Number";
$lang['cars_cars_meter_reading'] = "Meter reading";
$lang['cars_cars_motor_number'] = "Motor number";
$lang['cars_cars_color'] = "Color";
$lang['cars_cars_oil_change_date'] = "Oil change date";
$lang['cars_cars_oil_change_every'] = "oil change every how many days";
$lang['cars_cars_notes'] = "Notes";
$lang['cars_cars_images'] = "Car images";

$lang['closed_date'] = "Delivery date";
$lang['closed_time'] = "Delivery time";

$lang['cars_orders'] = "Maintenance orders";
$lang['cars_order'] = "Maintenance order";
$lang['cars_orders_change_status'] = "Change status";
$lang['cars_orders_part_name'] = "Spare parts";
$lang['cars_orders_add_parts'] = "Add spare part";

$lang['invoice_sale_start'] = "Sales invoice start with";
$lang['invoice_sale_return_start'] = "Sales returns invoice start with";
$lang['invoice_buy_start'] = "Purchase invoice start with";
$lang['invoice_buy_return_start'] = "Purchase returns invoice start with";
$lang['invoice_service_start'] = "Service invoice start with";


$lang['Alerts'] = "Alerts";
$lang['Delete_app_company'] = "Delete branches: This results in deleting all records and invoices related to the branch";
$lang['Delete_usr_users'] = "User Delete: This results in deleting all records and invoices that the user has registered";
$lang['Delete_fin_treeaccount'] = "Deleting the account: results in deleting all financial transactions and journal entries for the account, which has a direct effect on the balances.";
$lang['Delete_buy_manufacturer'] = "Supplier Deletion: This results in deleting all financial transactions and journal entries for the supplier, as well as deleting the supplier's purchase invoices.";
$lang['Delete_buy_bill'] = "Deleting a purchase invoice: This results in deleting all financial transactions and daily restrictions of the supplier, the treasury and the bank, as well as affecting the balance of items in the warehouse.";
$lang['Delete_buy_offer'] = "Delete quotations: Deleting a quotation does not affect any accounts or balances.";
$lang['Delete_buy_returns'] = "Deleting Purchase Returns: Deleting the purchase returns invoice affects the items balance, supplier account, treasury, bank, and journal entries.";
$lang['Delete_sale_customer'] = "Deleting customer data: Deleting customer data affects the balance of items, treasury, bank, and journal entries.";
$lang['Delete_sale_bill'] = "Sales invoice deletion: Deleting sales invoice affects items balance, customer account, treasury, bank, and journal entries.";
$lang['Delete_sale_offer'] = "Delete quotations: Deleting a quotation does not affect any accounts or balances.";
$lang['Delete_sale_returns'] = "Deleting sales returns: Deleting the sales returns invoice affects the items balance, customer account, treasury, bank, and journal entries.";
$lang['Delete_store'] = "Deleting stock data: Deleting warehouse data affects the balance of items, invoices, treasury, bank, and journal entries.";
$lang['Delete_store_category'] = "Deleting item classification data: Deleting classifications data affects the balance of items, invoices, treasury, bank, and journal entries.";
$lang['Delete_store_brand'] = "Deleting item model data: Deleting model data affects the items balance, invoices, treasury, bank, and journal entries.";
$lang['Delete_product'] = "Deleting item data: Deleting item data affects the balance of items, invoices, treasury, bank, and journal entries.";
$lang['Delete_cars_cars'] = "Deleting vehicle data: Deleting cars data affects maintenance orders, maintenance bills, treasury, bank, and journal entries.";
$lang['sale_customer_email'] = "Customers emails";
$lang['Send_Message_To_Customers'] = "Send Message To Customers";
$lang['Send_Notification_To_Customers'] = "Send Notification To Customers";

$lang['SupplierSign'] = "Supplier signature";

$lang['payy'] = "pay";
$lang['Paid'] = "Paid";

$lang['salary_disbursement'] ="salary disbursement";
$lang['all'] ="all";
$lang['PayToAll'] ="Pay To All";

?>