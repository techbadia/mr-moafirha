<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custody_deposit extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }
	}  
	
	public function index()
	{
		$data['DataRows'] = $this->M_fin_journal_main->GetMultiRow();
		$data['content_page'] = "custody/fin_journal_insert";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "account/fin_journal_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		$this->form_validation->set_rules("details","details","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$company_id = intval($this->session->userdata('company_id'));
			$user_id = intval($this->session->userdata('StaffID'));
			$automatic = 0;
			$deleted = 0;
			$details = set_value("details");
			$thedate = set_value("thedate");
			

			$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/fin_journal/";
			$ImageDirectory = $UploadPath;
			
			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
			}

			$bill_id = date("Y").date("m").date("d").date("h").date("i").date("s");
			$table_name = "manual";
			$bond_no = "manual";
			
			$fin_journal_array = array();
			$fin_journal_array = array(
				'company_id' => $company_id,
				'user_id' => $user_id,
				'details' => $details,
				'thedate' => $thedate,
				'bill_id' => $bill_id,
				'table_name' => $table_name,
				'bond_no' => $bond_no,
				'image' => $NewFileName,
				'automatic' => $automatic,
				'deleted' => $deleted,
			);
			$this->M_fin_journal_main->InsertRecord($fin_journal_array);
			$LatestRecord = $this->M_fin_journal_main->GetLatestRecord($company_id, $user_id);
			$main_id = $LatestRecord->id;

			$FromAccountArray = array();
			for ($i = 1; $i <= 1; $i++)
			{
				$account_id = "fromaccount".$i;
				$account_id = set_value($account_id);
				$debit = "debit".$i;
				$debit = set_value($debit);
				$creditor = 0;
				
				if($debit > 0)
				{
					$FromAccountArray = array(
						'main_id' => $main_id,
						'account_id' => $account_id,
						'debit' => $debit,
						'creditor' => $creditor,
					);
					$this->M_fin_journal->InsertRecord($FromAccountArray);
				}
			}

			$ToAccountArray = array();
			for ($i = 1; $i <= 1; $i++)
			{
				$account_id = "toaccount".$i;
				$account_id = set_value($account_id);
				$debit = 0;
				$creditor = "creditor".$i;
				$creditor = set_value($creditor);
				
				if($creditor > 0)
				{
					$ToAccountArray = array(
						'main_id' => $main_id,
						'account_id' => $account_id,
						'debit' => $debit,
						'creditor' => $creditor,
					);
					$this->M_fin_journal->InsertRecord($ToAccountArray);
				}
			}
			redirect ('account/fin_journal');
		}
	}

	public function updateform($rid)
	{
		$DataRow = $this->M_fin_journal_main->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['user_id'] = $DataRow->user_id;
		$data['details'] = $DataRow->details;
		$data['thedate'] = $DataRow->thedate;
		$data['bill_id'] = $DataRow->bill_id;
		$data['table_name'] = $DataRow->table_name;
		$data['bond_no'] = $DataRow->bond_no;
		$data['image'] = $DataRow->image;
		$data['automatic'] = $DataRow->automatic;
		$data['deleted'] = $DataRow->deleted;

		$data['DataRows'] = $this->M_fin_journal->GetMultiRow($rid);

		$data['ControllerName'] = $this->router->fetch_class();

		$data['content_page'] = "account/fin_journal_update";
        $this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("details","details","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$account_id = set_value("account_id");
			$debit = set_value("debit");
			$creditor = set_value('creditor');
			$details = set_value('details');
			$thedate = set_value('thedate');

			$NewData = array();
			$NewData = array(
				'account_id' => $account_id,
				'debit' => $debit,
				'creditor' => $creditor,
				'details' => $details,
				'thedate' => $thedate,
			);
			

			$this->M_fin_journal->UpdateRecord($id, $NewData);
			redirect ('account/fin_journal');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_fin_journal->UpdateRecord($rid, $NewData);
        redirect("fin_journal_main");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_fin_journal->UpdateRecord($rid, $NewData);

        redirect("fin_journal_main");
	}

	public function view_main()
	{
		$id = 0;
		$this->session->unset_userdata('Fin_journal_View_ID');
		$id = $this->input->get('id');
		$MainDataRow = $this->M_fin_journal_main->GetRow($id);
		$data['id'] = $MainDataRow->id;
		$data['company_id'] = $MainDataRow->company_id;
		$data['user_id'] = $MainDataRow->user_id;
		$data['details'] = $MainDataRow->details;
		$data['thedate'] = $MainDataRow->thedate;
		$data['bill_id'] = $MainDataRow->bill_id;
		$data['table_name'] = $MainDataRow->table_name;
		$data['bond_no'] = $MainDataRow->bond_no;
		$data['image'] = $MainDataRow->image;
		$data['automatic'] = $MainDataRow->automatic;
		$data['deleted'] = $MainDataRow->deleted;

		//$DataRows = $this->M_fin_journal->GetMultiRow($id);
		//$this->session->unset_userdata('Fin_journal_View_ID');
		
		$this->session->set_userdata('Fin_journal_View_ID', $id);

		echo json_encode($MainDataRow); 
		
    	exit();
	}

	public function view_details()
	{
		$id = $this->input->get('id');
		//$id = intval($this->session->userdata('Fin_journal_View_ID'));
		$DetailsRows = $this->M_fin_journal->GetRowDetails($id);
		echo json_encode($DetailsRows); 
		exit();
	}


}
