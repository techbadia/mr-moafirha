<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custody_new extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }
	}
	
	public function index()
	{
		$data['DataRows'] = $this->M_fin_treeaccount->GetMultiRow();
		$data['content_page'] = "custody/custody_new";
		$this->load->view('page', $data);
	}

	public function tree()
	{
		$data['DataRows'] = $this->M_fin_treeaccount->GetMultiRow();
		$data['content_page'] = "custody/fin_treeaccount_treeview";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "custody/custody_new_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("title_en","title_en","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = set_value("company_id");
			$category_id = set_value("category_id");
			$parent_id = set_value('parent_id');
			$account_no = set_value('account_no');
			$title = set_value('title');
			$title_en = set_value('title_en');
			$startamount = set_value('startamount');
			$basic = set_value('basic');
			$level_no = set_value('level_no');
			$custody_user_id = set_value('custody_user_id');
			$deleted = set_value('deleted');

			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'category_id' => $category_id,
				'parent_id' => $parent_id,
				'account_no' => $account_no,
				'title' => $title,
				'title_en' => $title_en,
				'startamount' => $startamount,
				'basic' => $basic,
				'level_no' => $level_no,
				'custody_user_id' => $custody_user_id,
				'deleted' => $deleted,
			);

			$this->M_fin_treeaccount->InsertRecord($NewData);
			
			redirect ('custody/custody_new');
		}
	}


	public function updateform($rid)
	{
		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['custody_user_id'] = $DataRow->custody_user_id;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "custody/custody_new_update";
        $this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("title_en","title_en","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = set_value("company_id");
			$category_id = set_value("category_id");
			$parent_id = set_value('parent_id');
			$account_no = set_value('account_no');
			$title = set_value('title');
			$title_en = set_value('title_en');
			$startamount = set_value('startamount');
			$basic = set_value('basic');
			$level_no = set_value('level_no');
			$custody_user_id = set_value('custody_user_id');
			$deleted = set_value('deleted');

			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'category_id' => $category_id,
				'parent_id' => $parent_id,
				'account_no' => $account_no,
				'title' => $title,
				'title_en' => $title_en,
				'startamount' => $startamount,
				'basic' => $basic,
				'level_no' => $level_no,
				'custody_user_id' => $custody_user_id,
				'deleted' => $deleted,
			);
			

			$this->M_fin_treeaccount->UpdateRecord($id, $NewData);
			
			redirect ('custody/custody_new');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_fin_treeaccount->UpdateRecord($rid, $NewData);

        redirect ('custody/custody_new');
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_fin_treeaccount->UpdateRecord($rid, $NewData);

        redirect ('custody/custody_new');
	}

	public function view($accountid)
    {
        $Page = "custody/report/account_balance_view/".$accountid;
        redirect ($Page);
    }
}
