<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		
		$this->load->model("settings/M_settings_acceptance");
		$this->load->model("settings/M_settings_notifications");
		
		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', "ar");
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		
	}
	
	public function index()
	{
 
		$this->load->helper('cookie');

		$this->config->load('settings/config_setting');
		$statistics_page = "";
		$Config_File = "";
		$Config_File_Lang = "";
		//$Segment1 = $this->uri->segment(1);
		//$this->config->set_item('Package', $Segment1);
		$Package = $this->config->item('Package');

		if ($this->config->item('MainLanguage') == "ar")
		{
			$Config_File_Lang = "_ar";
		}
		else
		{
			$Config_File_Lang = "_en";
		}
		
		$AppData = $this->M_app_element->GetApp();
		$CookiePackageName = $AppData->PackageName;
		$pos_available = $AppData->pos; // POS available
		$OpenSegment = $this->uri->segment(1);
		if(($CookiePackageName != $OpenSegment) && ($CookiePackageName != "") && ($OpenSegment != ""))
		{
			$CookiePackageName = $OpenSegment;
		}
		$PackageName = "";
		if($CookiePackageName == "")
		{
			switch ($OpenSegment) {
				case "erp":
					$CookiePackageName = $OpenSegment;
					break;
				case "custody":
					$CookiePackageName = $OpenSegment;
					break;
				case "hospital":
					$CookiePackageName = $OpenSegment;
					break;
				case "hr":
					$CookiePackageName = $OpenSegment;
					break;
				case "restaurant":
					$CookiePackageName = $OpenSegment;
					break;
				case "maintenance":
					$CookiePackageName = $OpenSegment;
					break;
				case "follow":
					$CookiePackageName = $OpenSegment;
					break;
				case "mbo":
					$CookiePackageName = $OpenSegment;
					break;
				case "pharmacy":
					$CookiePackageName = $OpenSegment;
					break;
				case "pos":
					$CookiePackageName = $OpenSegment;
					break;
				case "pm":
					$CookiePackageName = $OpenSegment;
					break;
				case "school":
					$CookiePackageName = $OpenSegment;
					break;
				case "training":
					$CookiePackageName = $OpenSegment;
					break;
				case "cars":
					$CookiePackageName = $OpenSegment;
					break;
				case "car_rental":
					$CookiePackageName = $OpenSegment;
					break;
				case "login":
					//$CookiePackageName = $OpenSegment;
					break;
				default:
					$CookiePackageName = $OpenSegment;
			}

			$cookie= array(
				'name'   => 'Package',
				'value'  => $CookiePackageName,                            
				'expire' => time()+86400,                                                                                   
				'domain' => base_url(),
				'path'   => '/',
			);
			set_cookie($cookie);

			$PackageName = $CookiePackageName;
			$this->session->set_userdata('PackageName', $PackageName);
			$this->session->set_userdata('Package', $PackageName);
		}
		else
		{
			$PackageName = $CookiePackageName;
			$this->session->set_userdata('PackageName', $PackageName);
			$this->session->set_userdata('Package', $PackageName);
		}
		//
		
		$this->config->set_item('Package', $PackageName); 
		//echo $PackageName;
		switch ($PackageName) {
			case "erp":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			case "custody":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			case "hospital":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			case "hr":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			case "restaurant":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			case "maintenance":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			case "follow":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			case "mbo":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			case "pharmacy":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			case "pos":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			case "pm":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			case "school":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			case "training":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			case "cars":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			case "car_rental":
				$statistics_page = "statistics_".$PackageName;
				$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
				$this->config->load($Config_File);
				$this->session->set_userdata('PackageName', $PackageName);
				break;
			default:
				redirect ($PackageName.'login/');
		}

		$PackageData = $this->M_app_package->GetByTitle($PackageName);
		$PackageID = $PackageData->id;
		$package_title = $PackageData->title;
		$this->session->set_userdata('PackageID', $PackageID);
		
		$this->LoadLanguage();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
			redirect ($PackageName.'login/');
        }
		$this->session->set_userdata('statistics_filter', "Month");
		$this->session->set_userdata('statistics_page', $statistics_page);

		if($PackageName == "maintenance")
		{
			$this->load->model('maintenance/M_set_teamwork_tasks');
			$this->load->model('maintenance/M_set_teamwork_schedule');
			$this->load->model('maintenance/M_set_teamwork');
			$this->load->model('maintenance/M_set_teamwork_items');
			$this->load->model('maintenance/M_data_teamwork_items_revenues');
			$this->load->model('maintenance/M_data_teamwork_items_expenses');
			$this->load->model('maintenance/M_set_request_status');
			$this->load->model('maintenance/M_set_area');
			$this->load->model('maintenance/M_services');
			$this->load->model('maintenance/M_data_invoice');

		}

		$AppData = $this->M_app_element->GetApp();
		$CookiePackageName = $AppData->PackageName;
		$this->session->set_userdata('PackageName', $CookiePackageName);

		//POS
		if($pos_available == 1)
		{
			$this->Get_POS_Sale();
		}

		$data['content_page'] = $statistics_page;
		$this->load->view('home', $data);
	}

	public function ChangeLanguage($lang)
	{
		$this->session->set_userdata('lang', $lang);
		if($lang == "ar")
		{
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
		{
			$this->session->set_userdata('HTML', "lang='en'");
		}
		
		$DataRow = $this->M_fin_settings->GetRow();
		$currency_id = $DataRow->currency_id;
		$CurrencyData = $this->M_currencies->GetRow($currency_id);
		if ($this->session->userdata('lang') == "ar")
		{
			$this->session->set_userdata('currency_title', $CurrencyData->title);
			$this->session->set_userdata('currency_symbol', $CurrencyData->symbol);
		}
		else
		{
			$this->session->set_userdata('currency_title', $CurrencyData->title_en);
			$this->session->set_userdata('currency_symbol', $CurrencyData->symbol_en);
		}

		$this->load->library('user_agent');
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function LoadSettings()
	{

	}

	public function filter_today()
	{
		$this->LoadLanguage();
		$this->session->set_userdata('statistics_filter', "Today");
		$statistics_page = $this->session->userdata('statistics_page');
		//$data['content_page'] = $statistics_page;
		//$this->load->view('home', $data);
		$this->load->library('user_agent');
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function filter_month()
	{
		$this->LoadLanguage();
		$this->session->set_userdata('statistics_filter', "Month");
		$statistics_page = $this->session->userdata('statistics_page');
		//$data['content_page'] = $statistics_page;
		//$this->load->view('home', $data);
		$this->load->library('user_agent');
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function filter_year()
	{
		$this->LoadLanguage();
		$this->session->set_userdata('statistics_filter', "Year");
		
		$statistics_page = $this->session->userdata('statistics_page');
		//$data['content_page'] = $statistics_page;
		//$this->load->view('home', $data);
		$this->load->library('user_agent');
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function LoadLanguage()
	{
		$this->config->load('settings/config_setting');
		$Package = $this->config->item('Package');

		if ($this->config->item('MainLanguage') == "ar")
		{
			$Config_File_Lang = "_ar";
		}
		else
		{
			$Config_File_Lang = "_en";
		}

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
	}

	public function ChangeCompany()
	{
		$this->form_validation->set_rules("company_id","company_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$company_id = set_value('company_id');
			$this->session->set_userdata('company_id', $company_id);
		}

		$CompanyData = $this->M_app_company->GetRow($company_id);
		$this->session->set_userdata('header', $CompanyData->header);
		$this->session->set_userdata('footer', $CompanyData->footer);

		$DataRow = $this->M_fin_settings->GetRow();
		$this->session->set_userdata('acc_sale', $DataRow->acc_sale);
		$this->session->set_userdata('acc_website_sale', $DataRow->acc_website_sale);
		$this->session->set_userdata('acc_sale_reurn', $DataRow->acc_sale_reurn);
		$this->session->set_userdata('acc_buy', $DataRow->acc_buy);
		$this->session->set_userdata('acc_buy_reurn', $DataRow->acc_buy_reurn);
		$this->session->set_userdata('acc_treasury', $DataRow->acc_treasury);
		$this->session->set_userdata('acc_bank', $DataRow->acc_bank);
		$this->session->set_userdata('acc_expenses', $DataRow->acc_expenses);
		$this->session->set_userdata('acc_revenues', $DataRow->acc_revenues);
		$this->session->set_userdata('acc_debtors', $DataRow->acc_debtors);
		$this->session->set_userdata('acc_creditors', $DataRow->acc_creditors);
		$this->session->set_userdata('acc_capture_papers', $DataRow->acc_capture_papers);
		$this->session->set_userdata('acc_payment_papers', $DataRow->acc_payment_papers);
		$this->session->set_userdata('acc_inventory', $DataRow->acc_inventory);
		$this->session->set_userdata('acc_supplier', $DataRow->acc_supplier);
		$this->session->set_userdata('acc_customer', $DataRow->acc_customer);
		$this->session->set_userdata('acc_material_store', $DataRow->acc_material_store);
		$this->session->set_userdata('acc_damaged_store', $DataRow->acc_damaged_store);
		$this->session->set_userdata('acc_invoice_items', $DataRow->acc_invoice_items);
		$this->session->set_userdata('acc_custody', $DataRow->acc_custody);
		$this->session->set_userdata('acc_affiliate', $DataRow->acc_affiliate);
		$this->session->set_userdata('acc_delivery', $DataRow->acc_delivery);
		$this->session->set_userdata('acc_factory_cost', $DataRow->acc_factory_cost);
		$this->session->set_userdata('acc_operating_expenses', $DataRow->acc_operating_expenses);
		$this->session->set_userdata('acc_salary', $DataRow->acc_salary);
		$this->session->set_userdata('acc_tax_sale', $DataRow->acc_tax_sale);
		$this->session->set_userdata('acc_tax_sale_percent', $DataRow->acc_tax_sale_percent);
		$this->session->set_userdata('acc_tax_purchase', $DataRow->acc_tax_purchase);
		$this->session->set_userdata('acc_tax_purchase_percent', $DataRow->acc_tax_purchase_percent);
		$this->session->set_userdata('currency_id', $DataRow->currency_id);
		$this->session->set_userdata('acc_sales_commission', $DataRow->acc_sales_commission);
		$this->session->set_userdata('acc_fixed_assets', $DataRow->acc_fixed_assets);
		$this->session->set_userdata('acc_current_assets', $DataRow->acc_current_assets);

		$currency_id = $DataRow->currency_id;
		$CurrencyData = $this->M_currencies->GetRow($currency_id);
		if ($this->session->userdata('lang') == "ar")
		{
			$this->session->set_userdata('currency_title', $CurrencyData->title);
			$this->session->set_userdata('currency_symbol', $CurrencyData->symbol);
		}
		else
		{
			$this->session->set_userdata('currency_title', $CurrencyData->title_en);
			$this->session->set_userdata('currency_symbol', $CurrencyData->symbol_en);
		}

		$this->session->set_userdata('header', $DataRow->header);
		$this->session->set_userdata('footer', $DataRow->footer);
		
		$this->Load_Acceptance();
		$this->Notifications();
		$this->Load_Printing();
				
		$this->load->library('user_agent');
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function Load_Acceptance()
	{
		$DataRow = $this->M_settings_acceptance->GetRow();
		//$this->session->set_userdata('company_id', $DataRow->company_id);
		$this->session->set_userdata('Acc_arabic_required', $DataRow->arabic_required);
		$this->session->set_userdata('Acc_english_required', $DataRow->english_required);
		$this->session->set_userdata('Acc_upload_required', $DataRow->upload_required);
		$this->session->set_userdata('Acc_fin_option1', $DataRow->fin_option1);
		$this->session->set_userdata('Acc_fin_option2', $DataRow->fin_option2);
		$this->session->set_userdata('Acc_fin_option3', $DataRow->fin_option3);
		$this->session->set_userdata('Acc_purchase_option1', $DataRow->purchase_option1);
		$this->session->set_userdata('Acc_sales_option1', $DataRow->sales_option1);
		$this->session->set_userdata('Acc_sales_option2', $DataRow->sales_option2);
		$this->session->set_userdata('Acc_store_option1', $DataRow->store_option1);
		$this->session->set_userdata('Acc_hr_option1', $DataRow->hr_option1);
		$this->session->set_userdata('Acc_hr_option2', $DataRow->hr_option2);
		$this->session->set_userdata('Acc_hr_option3', $DataRow->hr_option3);
		$this->session->set_userdata('Acc_custody_option1', $DataRow->custody_option1);
		$this->session->set_userdata('Acc_custody_option2', $DataRow->custody_option2);
		$this->session->set_userdata('Acc_service_option1', $DataRow->service_option1);
		$this->session->set_userdata('Acc_maintenance_option1', $DataRow->maintenance_option1);
		$this->session->set_userdata('Acc_manufacturing_option1', $DataRow->manufacturing_option1);
	}

	public function Notifications()
	{
		$DataRow = $this->M_settings_notifications->GetRow();
		//$this->session->set_userdata('company_id', $DataRow->company_id);
		$this->session->set_userdata('Not_fin_option1', $DataRow->fin_option1);
		$this->session->set_userdata('Not_fin_option2', $DataRow->fin_option2);
		$this->session->set_userdata('Not_fin_option3', $DataRow->fin_option3);
		$this->session->set_userdata('Not_fin_option4', $DataRow->fin_option4);
		$this->session->set_userdata('Not_purchase_option1', $DataRow->purchase_option1);
		$this->session->set_userdata('Not_sales_option1', $DataRow->sales_option1);
		$this->session->set_userdata('Not_store_option1', $DataRow->store_option1);
		$this->session->set_userdata('Not_hr_option1', $DataRow->hr_option1);
		$this->session->set_userdata('Not_hr_option2', $DataRow->hr_option2);
		$this->session->set_userdata('Not_hr_option3', $DataRow->hr_option3);
		$this->session->set_userdata('Not_hr_option4', $DataRow->hr_option4);
		$this->session->set_userdata('Not_custody_option1', $DataRow->custody_option1);
		$this->session->set_userdata('Not_custody_option2', $DataRow->custody_option2);
		$this->session->set_userdata('Not_service_option1', $DataRow->service_option1);
		$this->session->set_userdata('Not_maintenance_option1', $DataRow->maintenance_option1);
		$this->session->set_userdata('Not_maintenance_option2', $DataRow->maintenance_option2);
		$this->session->set_userdata('Not_manufacturing_option1', $DataRow->manufacturing_option1);
	}

	public function Load_Printing()
	{
		$DataRow = $this->M_settings_print->GetRow();
		//$this->session->set_userdata('company_id', $DataRow->company_id);
		$this->session->set_userdata('buy_bill_ar_text', $DataRow->buy_bill_ar_text);
		$this->session->set_userdata('buy_bill_en_text', $DataRow->buy_bill_en_text);
		$this->session->set_userdata('buy_bill_orientation', $DataRow->buy_bill_orientation);
		$this->session->set_userdata('buy_bill_size', $DataRow->buy_bill_size);
		$this->session->set_userdata('buy_returns_ar_text', $DataRow->buy_returns_ar_text);
		$this->session->set_userdata('buy_returns_en_text', $DataRow->buy_returns_en_text);
		$this->session->set_userdata('buy_returns_orientation', $DataRow->buy_returns_orientation);
		$this->session->set_userdata('buy_returns_size', $DataRow->buy_returns_size);
		$this->session->set_userdata('sale_bill_ar_text', $DataRow->sale_bill_ar_text);
		$this->session->set_userdata('sale_bill_en_text', $DataRow->sale_bill_en_text);
		$this->session->set_userdata('sale_bill_orientation', $DataRow->sale_bill_orientation);
		$this->session->set_userdata('sale_bill_size', $DataRow->sale_bill_size);
		$this->session->set_userdata('sale_returns_ar_text', $DataRow->sale_returns_ar_text);
		$this->session->set_userdata('sale_returns_en_text', $DataRow->sale_returns_en_text);
		$this->session->set_userdata('sale_returns_orientation', $DataRow->sale_returns_orientation);
		$this->session->set_userdata('sale_returns_size', $DataRow->sale_returns_size);
		$this->session->set_userdata('data_invoice_ar_text', $DataRow->data_invoice_ar_text);
		$this->session->set_userdata('data_invoice_en_text', $DataRow->data_invoice_en_text);
		$this->session->set_userdata('data_invoice_orientation', $DataRow->data_invoice_orientation);
		$this->session->set_userdata('data_invoice_size', $DataRow->data_invoice_size);
	}

	public function Get_POS_Sale()
	{
		$this->load->model('sales/M_sale_bill');
		$this->load->model('pos_sys/M_Tec_sales');
		$this->load->model('pos_sys/M_Tec_sale_items');
		$this->load->model('pos_sys/M_Tec_stores');
		$this->load->model("sales/M_sale_customer");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("account/M_fin_journal");
		$this->load->model("store/M_store_quantity");
		
		$BillCounts = $this->M_sale_bill->GetBillCounts();
		if($BillCounts > 0) {
		$LatestBill = $this->M_sale_bill->GetLatestBill();
		$pos_store_id = intval($LatestBill->pos_store_id);
		$user_id = intval($this->session->userdata('StaffID'));
		$CheckNewBill = $this->M_Tec_sales->CheckNewBill($pos_store_id);
		if($CheckNewBill > 0)
		{
			$POS_Bill = $this->M_Tec_sales->GetMultiRow($pos_store_id);
			foreach($POS_Bill as $POS_Bill_Row) {

				$id = $POS_Bill_Row->id;
				$thedate = $POS_Bill_Row->date;
				$customer_id = $POS_Bill_Row->customer_id;
				$customer_name = $POS_Bill_Row->customer_name;
				$total_discount = $POS_Bill_Row->total_discount;
				$total_tax = $POS_Bill_Row->total_tax;
				$grand_total = $POS_Bill_Row->grand_total;
				$paid = $POS_Bill_Row->paid;
				$created_by = $POS_Bill_Row->created_by;
				$store_id = $POS_Bill_Row->store_id;

				$POS_Store_Data = $this->M_Tec_stores->GetRow($store_id);
				$tree_id = $POS_Store_Data->tree_id;
				$POS_Name = $POS_Store_Data->name;
				///// insert sale_bill
				$company_id = intval($this->session->userdata('company_id'));
				$CustomerData = $this->M_sale_customer->GetRow($customer_id);
				$customer_name = "";
				$customer_phone = "";
				$project_id = 0;
				$remaining = $grand_total - $paid;
				$discount = 0;

				$NewData = array();
				$NewData = array(
					'company_id' => $company_id,
					'customer_id' => $customer_id,
					'customer_name' => $customer_name,
					'customer_phone' => $customer_phone,
					'project_id' => $project_id,
					'user_id' => 0,
					'thedate' => $thedate,
					'totalvalue' => $grand_total,
					'paid' => $paid,
					'remaining' => $remaining,
					'discount' => $total_discount,
					'tax' => $total_tax,
					'notes' => "",
					'image' => "",
					'tree_id' => $tree_id,
					'tree_id1' => 0,
					'paid1' => $grand_total,
					'paid2' => 0,
					'paid3' => 0,
				);
				$this->M_sale_bill->InsertRecord($NewData);
				$totalvalue = $grand_total;
				$paid1 = $grand_total;
				$paid2 = 0;
				$paid3 = 0;
				$tree_id1 = 0;
				
				$LatestRecord = $this->M_sale_bill->GetLatestRecord($company_id, $customer_id, $user_id);
				$bill_id = $LatestRecord->id;

				$details = "فاتورة مبيعات من نقطة البيع ".$POS_Name;
				$table_name = "sale_bill";
				$bond_no = $bill_id;
				$image = "";

				$TaxAmount = $grand_total * ($total_tax / 100);

				$this->fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $image);

				//المدفوع في حساب الخزينة
				$account_id = $tree_id;
				$debit = $paid1;
				$creditor = 0;
				$this->fin_journal_insert($account_id, $debit, $creditor);

				$account_id = intval($this->session->userdata('acc_sale'));
				$debit = 0;
				$creditor = ($totalvalue - $TaxAmount);
				$details = "فاتورة مبيعات من نقطة البيع ".$POS_Name;
				$table_name = "sale_bill";
				$bond_no = $bill_id;
				$image = "";
				// إلى حساب المبيعات
				$this->fin_journal_insert($account_id, $debit, $creditor);

				//قيد ضريبة المبيعات
				$account_id = intval($this->session->userdata('acc_tax_sale'));
				$debit = 0;
				$creditor = $TaxAmount;
				$details = "فاتورة مبيعات من نقطة البيع ".$POS_Name;
				$table_name = "sale_bill";
				$bond_no = $bill_id;
				$image = "";
				$this->fin_journal_insert($account_id, $debit, $creditor);

				$Get_POS_Bill_Items = $this->M_Tec_sale_items->GetItems($id);
				foreach($Get_POS_Bill_Items as $Get_POS_Bill_Items_Row) {

					$store_type_id = $Get_POS_Bill_Items_Row->product_id;
					$location_id = $store_id;
					$quantity = $Get_POS_Bill_Items_Row->quantity;
					$unitprice = $Get_POS_Bill_Items_Row->unit_price;
					
					$Old_FromStoreData = $this->M_store_quantity->GetStoreProduct($location_id, $store_type_id);
					$Old_FromStore_Quantity = $Old_FromStoreData->quantity;
					$Old_FromStore_id = $Old_FromStoreData->id;
					$New_FromStore_Quantity = $Old_FromStore_Quantity - $quantity;
					$FromStoreArrayData = array();
					$FromStoreArrayData = array(
						'store_id' => $location_id,
						'product_id' => $store_type_id,
						'quantity' => $New_FromStore_Quantity,
					);
					$this->M_store_quantity->UpdateRecord($Old_FromStore_id, $FromStoreArrayData);

					$ItemsArray = array();
					$ItemsArray = array(
						'bill_id' => $bill_id,
						'store_type_id' => $store_type_id,
						'location_id' => $location_id,
						'quantity' => $quantity,
						'unitprice' => $unitprice,
					);
					$this->M_sale_bill_items->InsertRecord($ItemsArray);

				}
			}
		}
		}
	}


	public function fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $image)
	{
		$company_id = intval($this->session->userdata('company_id'));
		$user_id = intval($this->session->userdata('StaffID'));
		$automatic = 1;
		$deleted = 0;

		$fin_journal_array = array();
		$fin_journal_array = array(
			'company_id' => $company_id,
			'user_id' => $user_id,
			'details' => $details,
			'thedate' => $thedate,
			'bill_id' => $bill_id,
			'table_name' => $table_name,
			'bond_no' => $bond_no,
			'image' => $image,
			'automatic' => $automatic,
			'deleted' => $deleted,
		);

		$this->M_fin_journal_main->InsertRecord($fin_journal_array);
	}

	public function fin_journal_insert($account_id, $debit, $creditor)
	{
		$company_id = intval($this->session->userdata('company_id'));
		$user_id = intval($this->session->userdata('StaffID'));

		$MainData = $this->M_fin_journal_main->GetLatestRecord($company_id, $user_id);
		$main_id = $MainData->id;

		$fin_journal_array = array();
		$fin_journal_array = array(
			'main_id' => $main_id,
			'account_id' => $account_id,
			'debit' => $debit,
			'creditor' => $creditor,
		);

		$this->M_fin_journal->InsertRecord($fin_journal_array);
	}

	public function push_messages()
	{
		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', "ar");
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "communication/push_messages";
		$this->load->view('page', $data);
	}

	public function push_messages_send()
	{
		$this->form_validation->set_rules("subject","subject","required");
		$this->form_validation->set_rules("message","message","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$thedate = date("Y-m-d");
            $thetime = date("H:i:s");
			$subject = set_value('subject');
			$message = set_value('message');


			$NewData = array();
			foreach ($_REQUEST['customer_id'] as $customer_id)
            {
                $NewData = array(
					'customer_id' => $customer_id,
					'thedate' => $thedate,
					'thetime' => $thetime,
					'subject' => $subject,
					'message' => $message,
				);
				$this->M_push_messages->InsertRecord($NewData);
            }
			
			redirect (base_url());
		}
	}

	public function push_notifications()
	{
		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', "ar");
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "communication/push_notifications";
		$this->load->view('page', $data);
	}

	public function push_notifications_send()
	{
		$this->form_validation->set_rules("subject","subject","required");
		$this->form_validation->set_rules("message","message","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$thedate = date("Y-m-d");
            $thetime = date("H:i:s");
			$subject = set_value('subject');
			$message = set_value('message');


			$NewData = array();
			$NewData = array(
				'thedate' => $thedate,
				'thetime' => $thetime,
				'subject' => $subject,
				'message' => $message,
			);
			$this->M_push_notifications->InsertRecord($NewData);
			
			redirect (base_url());
		}
	}
	public function sss(){
	    echo '<pre>';
	    print_r($_SESSION);
	}
}
