<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cars_invoice extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }
        $this->load->model("sales/M_sale_customer");
		$this->load->model("cars/M_cars_cars");
		$this->load->model("cars/M_cars_service");
		$this->load->model("cars/M_cars_invoice");
		$this->load->model("cars/M_cars_invoice_items");
		$this->load->model("cars/M_cars_invoice_parts");
		$this->load->model("cars/M_cars_model");

		$this->load->model("maintenance/M_set_request_status");
		$this->load->model("maintenance/M_set_area");
		$this->load->model("maintenance/M_set_teamwork");
	}
	
	public function index()
	{
		$fromdate = date("Y-m-d");
		$todate = date("Y-m-d");
		$area_id = 0;
		$customer_id = 0;
		$teamwork_id = 0;
		$status_id = 0;

		$data['fromdate'] = $fromdate;
		$data['todate'] = $todate;
		$data['area_id'] = $area_id;
		$data['customer_id'] = $customer_id;
		$data['teamwork_id'] = $teamwork_id;
		$data['status_id'] = $status_id;
        
        $data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
		$data['Area'] = $this->M_set_area->GetMultiRow();
		$data['TeamWork'] = $this->M_set_teamwork->GetMultiRow();
        
		$data['DataRows'] = $this->M_cars_invoice->GetMultiRow($customer_id, $teamwork_id, $status_id, $area_id, $fromdate, $todate);
		$data['content_page'] = "cars/cars_invoice";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
		$data['Area'] = $this->M_set_area->GetMultiRow();
		$data['TeamWork'] = $this->M_set_teamwork->GetMultiRow();

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "cars/cars_invoice_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		$this->load->helper('security');
		$this->load->library("form_validation");
		
		$this->form_validation->set_rules("car_id","car_id","required");
		$this->form_validation->set_rules("status_id","status_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
			$company_id = intval($this->session->userdata('company_id'));
			$user_id = intval($this->session->userdata('StaffID'));
			$affiliate_id = 0;
			$teamwork_id = set_value('teamwork_id');
			$status_id = set_value('status_id');
			$area_id = set_value('area_id');
			$car_id = set_value('car_id');
			$source = 1;
            $details = set_value('details');
			$thedate = date("Y-m-d");
            $thetime = date("h:m");
			$insert_date = date("Y-m-d");
            $insert_time = date("h:m");
			$schedule = 0;
			$deleted = 0;

			$CarData = $this->M_cars_cars->GetRow($car_id);
			$customer_id = $CarData->customer_id;

			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'user_id' => $user_id,
				'affiliate_id' => $affiliate_id,
				'customer_id' => $customer_id,
				'teamwork_id' => $teamwork_id,
				'status_id' => $status_id,
				'area_id' => $area_id,
				'car_id' => $car_id,
				'source' => $source,
				'details' => $details,
                'thedate' => $thedate,
				'thetime' => $thetime,
				'insert_date' => $insert_date,
				'insert_time' => $insert_time,
				'schedule' => $schedule,
				'deleted' => $deleted,
			);

			$this->M_cars_invoice->InsertRecord($NewData);

			$LatestInvoice = $this->M_cars_invoice->GetLatest($user_id, $customer_id, $teamwork_id);
			$invoice_id = $LatestInvoice->id;

			$ServiceList = $this->M_cars_service->GetMultiRow();
			foreach($ServiceList as $ServiceList_Row) {
				$id = $ServiceList_Row->id;

				$service_id = "service_id".$id;
				$unit_price = "unit_price".$id;
				$qunatity = "qunatity".$id;

				$service_id = set_value($service_id);
				$unit_price = set_value($unit_price);
				$qunatity = set_value($qunatity);
				if($qunatity > 0)
				{
					$NewData = array();
					$NewData = array(
						'invoice_id' => $invoice_id,
						'service_id' => $service_id,
						'qunatity' => $qunatity,
						'unit_price' => $unit_price,
					);

					$this->M_cars_invoice_items->InsertRecord($NewData);
				}
			}
			
			redirect ('cars/cars_invoice');
		}
	}

	public function updateform($rid)
	{
		$data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
		$data['Area'] = $this->M_set_area->GetMultiRow();
		$data['TeamWork'] = $this->M_set_teamwork->GetMultiRow();

		$DataRow = $this->M_cars_invoice->GetRow($rid);
        $data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['user_id'] = $DataRow->user_id;
		$data['affiliate_id'] = $DataRow->affiliate_id;
		$data['customer_id'] = $DataRow->customer_id;
		$data['status_id'] = $DataRow->status_id;
		$data['area_id'] = $DataRow->area_id;
		$data['source'] = $DataRow->source;
		$data['details'] = $DataRow->details;
		$data['thedate'] = $DataRow->thedate;
		$data['thetime'] = $DataRow->thetime;
		$data['insert_date'] = $DataRow->insert_date;
		$data['insert_time'] = $DataRow->insert_time;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "cars/cars_invoice_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("status_id","status_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');

			$DataRow = $this->M_cars_service->GetRow($id);


			$status_id = set_value('status_id');
			$closed_date = date("Y-m-d");
            $closed_time = date("h:m");
			$tree_id = set_value('tree_id');

			$NewData = array();

			if($status_id == 5)
			{
				$NewData = array(
					'status_id' => $status_id,
					'closed_date' => $closed_date,
					'closed_time' => $closed_time,
				);
			}
			else
			{
				$NewData = array(
					'status_id' => $status_id,
				);
			}

			$this->M_cars_service->UpdateRecord($id, $NewData);

			/////////////////////////////////
			if($status_id == 5)
			{
				//M_cars_service_items
				$qunatity = 0;
				$unit_price = 0;
				$totalvalue = 0;
				$TotalItems = $this->M_cars_service_items->GetByRequest($id);
				foreach($TotalItems as $TotalItems_Row) {
					$qunatity = $TotalItems_Row->qunatity;
					$unit_price = $TotalItems_Row->unit_price;
					$totalvalue += $qunatity * $unit_price;
				}
				$details = $DataRow->details;
				$table_name = "data_invoice";
	
				$this->fin_journal_main_insert($details, $closed_date, $id, $table_name, $id, "");
				//من حساب الخزينة
				$account_id = $tree_id;
				$debit = $totalvalue;
				$creditor = 0;
				$this->fin_journal_insert($account_id, $debit, $creditor);
				//إلى حساب الخدمات
				$account_id = intval($this->session->userdata('acc_revenues'));
				$debit = 0;
				$creditor = $totalvalue;
				$this->fin_journal_insert($account_id, $debit, $creditor);
			}
			
			/////////////////////////////////
			
			redirect ('cars/cars_invoice');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_cars_service->UpdateRecord($rid, $NewData);

        redirect("cars/cars_invoice");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_cars_service->UpdateRecord($rid, $NewData);

        redirect("cars/cars_invoice");
    }
    
    public function filter()
	{
        $this->form_validation->set_rules("fromdate","fromdate","required");
        if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$fromdate = set_value("fromdate");
			$todate = set_value("todate");
			$area_id = set_value("area_id");
			$customer_id = set_value("customer_id");
			$teamwork_id = 0;
			$status_id = set_value("status_id");
	
			$data['fromdate'] = $fromdate;
			$data['todate'] = $todate;
			$data['area_id'] = $area_id;
			$data['customer_id'] = $customer_id;
			$data['teamwork_id'] = $teamwork_id;
			$data['status_id'] = $status_id;
	
			$data['Customer'] = $this->M_sale_customer->GetMultiRow();
			$data['Status'] = $this->M_set_request_status->GetMultiRow();
        	$data['Area'] = $this->M_set_area->GetMultiRow();
			$data['TeamWork'] = $this->M_set_teamwork->GetMultiRow();

			$data['DataRows'] = $this->M_cars_service->GetMultiRow($customer_id, $teamwork_id, $status_id, $area_id, $fromdate, $todate);
			$data['content_page'] = "cars/cars_invoice";
			$this->load->view('page', $data);
		}
    }
    

	public function view_main()
	{
		$id = $this->input->get('id');
		$MainDataRow = $this->M_cars_service->GetHead($id);
		echo json_encode($MainDataRow); 
    	exit();
	}

	public function view_details()
	{
		$id = $this->input->get('id');
		//$id = intval($this->session->userdata('Fin_journal_View_ID'));
		$DetailsRows = $this->M_cars_service->GetDetails($id);
		echo json_encode($DetailsRows); 
		exit();
	}

	public function expenses($invoice_id)
	{
		$data['invoice_id'] = $invoice_id;
		$data['status_name'] = $this->lang->line('data_invoice');
		$data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
		$data['Area'] = $this->M_set_area->GetMultiRow();
		$data['Service'] = $this->M_services->GetMultiRow();
		
		$data['content_page'] = "cars/cars_invoice_expenses_insert";
		$this->load->view('page', $data);
	}


	public function fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $image)
	{
		$company_id = intval($this->session->userdata('company_id'));
		$user_id = intval($this->session->userdata('StaffID'));
		$automatic = 1;
		$deleted = 0;

		$fin_journal_array = array();
		$fin_journal_array = array(
			'company_id' => $company_id,
			'user_id' => $user_id,
			'details' => $details,
			'thedate' => $thedate,
			'bill_id' => $bill_id,
			'table_name' => $table_name,
			'bond_no' => $bond_no,
			'image' => $image,
			'automatic' => $automatic,
			'deleted' => $deleted,
		);

		$this->M_fin_journal_main->InsertRecord($fin_journal_array);
	}

	public function fin_journal_insert($account_id, $debit, $creditor)
	{
		$company_id = intval($this->session->userdata('company_id'));
		$user_id = intval($this->session->userdata('StaffID'));

		$MainData = $this->M_fin_journal_main->GetLatestRecord($company_id, $user_id);
		$main_id = $MainData->id;

		$fin_journal_array = array();
		$fin_journal_array = array(
			'main_id' => $main_id,
			'account_id' => $account_id,
			'debit' => $debit,
			'creditor' => $creditor,
		);

		$this->M_fin_journal->InsertRecord($fin_journal_array);
	}

	public function pdf($invoice_id)
	{
		$Alignment = "";
		$Direction = "";
		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
			$Alignment = "right";
			$Direction = "rtl";
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
			$Alignment = "left";
			$Direction = "ltr";
		}

		$this->load->model("settings/M_fin_settings");
		$company_id = intval($this->session->userdata('company_id'));
		$DataRow = $this->M_app_company->GetRow($company_id);
		$HeaderImage = base_url()."upload/settings/".$DataRow->header;
		$FooterImage = base_url()."upload/settings/".$DataRow->footer;

		$Directionality = $this->session->userdata('Direction');
		$PageTitle = lang('pdf_data_invoice');

		$PageSize = $this->session->userdata('data_invoice_size');
		$PageOrientation = $this->session->userdata('data_invoice_orientation');
		$PageFormat = $PageSize."-".$PageOrientation;

		require_once 'vendor/autoload.php';
		$mpdf = new \Mpdf\Mpdf([
			'mode' => 'utf-8',
			'format' => $PageFormat,
			//'margin_left' => 5,
			//'margin_right' => 5,
			'margin_top' => 25,
			'margin_bottom' => 25,
			'margin_header' => 5,
			'margin_footer' => 5,
		]);
		$mpdf->allow_charset_conversion=true;
		$mpdf->SetTitle($PageTitle);
		$mpdf->SetDirectionality($Directionality);
		$mpdf->autoScriptToLang = true;
		$mpdf->autoLangToFont = true;
		$mpdf->debug = true;
		ob_end_clean();
		header("Content-Encoding: None", true);
		$mpdf->SetHTMLHeader('
		<div style="text-align: center; font-weight: bold;">
			<img src="'.$HeaderImage.'">
		</div>');
		$mpdf->SetHTMLFooter('
		<table width="100%" style="vertical-align: bottom; font-family: serif; 
			font-size: 8pt; color: #000000; font-weight: bold;">
			<tr>
				<td colspan="2"><img src="'.$FooterImage.'"></td>
			</tr>
			<tr>
				<td width="50%">{DATE j-m-Y}</td>
				<td width="50%" align="left" lang="en">{PAGENO}/{nbpg}</td>
			</tr>
		</table>');

		$DataRow = $this->M_cars_invoice->GetRow($invoice_id);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$company_id = $DataRow->company_id;
		// Car Data
		$car_id = $DataRow->car_id;
		$CarData = $this->M_cars_cars->GetRow($car_id);
		$model_id = $CarData->model_id;
		$ModelData = $this->M_cars_model->GetRow($model_id);
		if ($this->session->userdata('lang') == "ar")
		{
			$ModelName = $ModelData->title;
		}
		else
		{
			$ModelName = $ModelData->title_en;
		}

		$CompanyData = $this->M_app_company->GetRow($company_id);
		$tax_number = $CompanyData->tax_number;
		$customer_id = $DataRow->customer_id;
		$CustomerData = $this->M_sale_customer->GetRow($customer_id);
		if ($this->session->userdata('lang') == "ar")
		{
			$CustomerName = $CustomerData->ar_title;
		}
		else
		{
			$CustomerName = $CustomerData->en_title;
		}
		$user_id = $DataRow->user_id;
		$UserData = $this->M_usr_users->GetRow($user_id);
		$InvoiceEditor = $UserData->fullname;

		$MultiRows = $this->M_cars_invoice_items->GetByRequest($invoice_id);
		$PartsRows = $this->M_cars_invoice_parts->GetByRequest($invoice_id);

		$GetTotalValue = $this->M_cars_invoice_items->GetByRequest($invoice_id);
		$Items_totalvalue = 0;
		$Parts_totalvalue = 0;
		$totalvalue = 0;
		foreach($GetTotalValue as $GetTotalValue_Row) {
			$qunatity = $GetTotalValue_Row->qunatity;
			$unit_price = $GetTotalValue_Row->unit_price;
			$Items_totalvalue += ($qunatity * $unit_price);
		}
		$PartsTotalValue = $this->M_cars_invoice_parts->GetByRequest($invoice_id);
		foreach($PartsTotalValue as $PartsTotalValue_Row) {
			$qunatity = $PartsTotalValue_Row->qunatity;
			$unit_price = $PartsTotalValue_Row->unit_price;
			$Parts_totalvalue += ($qunatity * $unit_price);
		}
		$totalvalue = $Items_totalvalue + $Parts_totalvalue;
		$html = '';
		$html .= '<div align="center" style="font-size:18px">';
		$html .= '<br><br><strong>'.$PageTitle.'</strong><br>';
		$html .= '<table cellspacing="0" cellpadding="5" border="0" width="98%">';
		$html .= '<tr>';
		$html .= '<td>'.$this->lang->line('buy_manufacturer_tax_card').' : </td>';
		$html .= '<td style="text-align:'.$Alignment.'">'.$tax_number.'</td>';
		$html .= '<td>'.$this->lang->line('data_account_invoice_id').' : </td>';
		$html .= '<td style="text-align:'.$Alignment.'">'.$invoice_id.'</td>';
		$html .= '<td>'.$this->lang->line('sale_fullname').' : </td>';
		$html .= '<td style="text-align:'.$Alignment.'">'.$CustomerName.'</td>';
		$html .= '</tr>';

		$html .= '<tr><td>'.$this->lang->line('cars_cars_chassis_no').' : </td>';
		$html .= '<td style="text-align:'.$Alignment.'">'.$CarData->chassis_no.'</td>';
		$html .= '<td>'.$this->lang->line('cars_cars_plate_No').' : </td>';
		$html .= '<td style="text-align:'.$Alignment.'">'.$CarData->plate_No.'</td>';
		$html .= '<td>'.$this->lang->line('cars_cars_color').' : </td>';
		$html .= '<td style="text-align:'.$Alignment.'">'.$CarData->color.'</td>';
		$html .= '</tr>';

		$html .= '<tr><td>'.$this->lang->line('cars_cars_model_id').' : </td>';
		$html .= '<td style="text-align:'.$Alignment.'">'.$ModelName.'</td>';
		$html .= '<td>'.$this->lang->line('cars_cars_model_name').' : </td>';
		$html .= '<td style="text-align:'.$Alignment.'">'.$CarData->model_name.'</td>';
		$html .= '<td>'.$this->lang->line('cars_cars_meter_reading').' : </td>';
		$html .= '<td style="text-align:'.$Alignment.'">'.$CarData->meter_reading.'</td>';
		$html .= '</tr>';

		$html .= '<tr>';
		$html .= '<td>'.$this->lang->line('thedate').' : </td>';
		$html .= '<td>'.$DataRow->thedate.'</td>';
		$html .= '<td>'.$this->lang->line('sale_bill_totalvalue').' : </td>';
		$html .= '<td>'.$totalvalue.'</td>';
		$html .= '<td>'.$this->lang->line('pdf_Invoice_Editor').' : </td>';
		$html .= '<td>'.$InvoiceEditor.'</td>';
		$html .= '</tr>';
		$html .= '</table>';
		//$html .= $PageTitle.'<br>('.$invoice_id.')-'.$DataRow->thedate."<br>";
		$html .= '</div><br>';
		$html .= '<table cellspacing="0" cellpadding="5" border="1" width="98%">';
		$html .= '<thead>';
		$html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
		$html .= '<th width="15%">'.$this->lang->line('Serial').'</th>';
		$html .= '<th width="40%" align="right">'.$this->lang->line('data_invoice_services_title').'</th>';
		$html .= '<th width="15%">'.$this->lang->line('qunatity').'</th>';
		$html .= '<th width="15%">'.$this->lang->line('set_service_price').'</th>';
		$html .= '<th width="15%">'.$this->lang->line('Total').'</th>';
		$html .= '</tr>';
		$html .= '</thead>';
		$html .= '<tbody>';
		$Serial = 0;
		$TotalValue =0;
		$AllQuantity = 0;
		$FinalBigQ = 0;
		$FinalSmallQ = 0;
		foreach($MultiRows as $MultiRows_Row) {
			$Serial += 1;
			$invoice_id	 = $MultiRows_Row->invoice_id;
			$service_id	 = $MultiRows_Row->service_id;
			$ServiceData = $this->M_cars_service->GetRow($service_id);
			if ($this->session->userdata('lang') == "ar")
			{
				$ServiceName = $ServiceData->ar_title;
			}
			else
			{
				$ServiceName = $ServiceData->en_title;
			}
			$html .= '<tr>';
			$html .= '<td style="background-color:#EBEBEB; font-weight:bold;" align="center">'.$Serial.'</td>';
			$html .= '<td>'.$ServiceName.'</td>';
			$html .= '<td align="center">'.$MultiRows_Row->qunatity.'</td>';
			$html .= '<td align="center">'.$MultiRows_Row->unit_price.'</td>';
			$SubTotal = doubleval($MultiRows_Row->qunatity) * doubleval($MultiRows_Row->unit_price);
			$html .= '<td align="center">'.$SubTotal.'</td>';
			$html .= '</tr>';
			//$TotalValue += $SubTotal;
		}
		$html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
		$html .= '<td></td>';
		$html .= '<td colspan="3">'.$this->lang->line('sale_bill_totalvalue').'</td>';
		$html .= '<td align="center">'.$Items_totalvalue.'</td>';
		$html .= '</tr>';
		$html .= '<tr style="border:none !important;">';
		$html .= '<td colspan="5" style="border:none !important;">';
		$ArabicMoney = $this->makeNumber2Text($Items_totalvalue);
		$html .= $ArabicMoney.' '.$this->lang->line('JustDoNot');
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '</tbody>';
		$html .= '</table>';
		$html .= '<br>';

		$html .= '<table cellspacing="0" cellpadding="5" border="1" width="98%">';
		$html .= '<thead>';
		$html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
		$html .= '<th width="15%">'.$this->lang->line('Serial').'</th>';
		$html .= '<th width="40%" align="right">'.$this->lang->line('cars_orders_part_name').'</th>';
		$html .= '<th width="15%">'.$this->lang->line('qunatity').'</th>';
		$html .= '<th width="15%">'.$this->lang->line('buy_bill_items_unitprice').'</th>';
		$html .= '<th width="15%">'.$this->lang->line('Total').'</th>';
		$html .= '</tr>';
		$html .= '</thead>';
		$html .= '<tbody>';
		
		foreach($PartsRows as $MultiRows_Row) {
			$Serial += 1;
			$invoice_id	 = $MultiRows_Row->invoice_id;
			$ServiceData = $this->M_cars_service->GetRow($invoice_id);
			$ServiceName = $MultiRows_Row->part_name;
			$html .= '<tr>';
			$html .= '<td style="background-color:#EBEBEB; font-weight:bold;" align="center">'.$Serial.'</td>';
			$html .= '<td align="right">'.$ServiceName.'</td>';
			$html .= '<td align="center">'.$MultiRows_Row->qunatity.'</td>';
			$html .= '<td align="center">'.$MultiRows_Row->unit_price.'</td>';
			$SubTotal = doubleval($MultiRows_Row->qunatity) * doubleval($MultiRows_Row->unit_price);
			$html .= '<td align="center">'.$SubTotal.'</td>';
			$html .= '</tr>';
			//$TotalValue += $SubTotal;
		}
		$html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
		$html .= '<td></td>';
		$html .= '<td colspan="3">'.$this->lang->line('sale_bill_totalvalue').'</td>';
		$html .= '<td align="center">'.$Parts_totalvalue.'</td>';
		$html .= '</tr>';
		$html .= '<tr style="border:none !important;">';
		$html .= '<td colspan="5" style="border:none !important;">';
		$ArabicMoney = $this->makeNumber2Text($Parts_totalvalue);
		$html .= $ArabicMoney.' '.$this->lang->line('JustDoNot');
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '</tbody>';
		$html .= '</table>';

		$html .= '<br>';
		$html .= '<center><strong>'.$this->lang->line('sale_bill_totalvalue')." : ".$totalvalue.'</strong></center>';
		$html .= '<br>';
		$html .= '<table cellspacing="0" cellpadding="5" border="0" width="98%" align="center">';
		$html .= '<tr>';
		$html .= '<td align="right">'.$this->lang->line('IReceived').'</td>';
		$html .= '<td align="center"></td>';
		$html .= '<td align="left">'.$InvoiceEditor.'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td align="right">'.$this->lang->line('ReceviesSign').'</td>';
		$html .= '<td align="center"></td>';
		$html .= '<td align="left">'.$this->lang->line('SalesmanSign').'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td align="right">..........................................</td>';
		$html .= '<td align="center"></td>';
		$html .= '<td align="left">..........................................</td>';
		$html .= '</tr>';
		$html .= '</table>';
		
		$html = iconv("utf-8","UTF-8//IGNORE",$html);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}

	public function makeNumber2Text($numberValue){
	
		//$this->cache->clean();
		ini_set("memory_limit","512M");
		
		$textResult = ''; // so i can use .=
		$numberValue = "$numberValue";
		
		if($numberValue[0] == '-'){
			$textResult .= 'سالب ';
			$numberValue = substr($numberValue,1);
		}
		
		$numberValue = (int) $numberValue;    
		$def = array(    "0" => 'صفر',
						"1" => 'واحد',
						"2" => 'اثنان',
						"3" => 'ثلاث',
						"4" => 'اربع',
						"5" => 'خمس',
						"6" => 'ست',
						"7" => 'سبع',
						"8" => 'ثمان',
						"9" => 'تسع',
						"10" => 'عشر',
						"11" => 'أحد عشر',
						"12" => 'اثنا عشر',
						"100" => 'مائة',
						"200" => 'مئتان',
						"1000" => 'ألف',
						"2000" => 'ألفين',
						"1000000" => 'مليون',
						"2000000" => 'مليونان');
	
		// check for defind values    
		if(isset($def[$numberValue])) {
			// checking for numbers from 2 to 10 :reson = 2 to 10 uses 'ة' at the end
			if($numberValue < 11 && $numberValue > 2){
				$textResult .= $def[$numberValue].'ة';
			}
			else{
				// the rest of the defined numbers
				$textResult .= $def[$numberValue];
			}
		}
		else{
			$tensCheck = $numberValue%10;
			$numberValue = "$numberValue";
			
			for($x = strlen($numberValue); $x > 0; $x--){
				$places[$x] = $numberValue[strlen($numberValue)-$x];
			}
	
			switch(count($places)){
				case 2: // 2 numbers
				case 1: // or 1 number
				{
					$textResult .= ($places[1] != 0) ? $def[$places[1]].(($places[1] > 2 || $places[2] == 1) ? 'ة' : '').(($places[2] != 1) ? ' و' : ' ') : '';
					$textResult .= (($places[2] > 2) ? $def[$places[2]].'ون' : $def[10].(($places[2] != 2) ? '' : 'ون'));                
				}
				break;
				case 3: // 3 numbers
				{
					$lastTwo = (int) $places[2].$places[1];
					$textResult .= ($places[3] > 2) ? $def[$places[3]].' '.$def[100] : $def[(int) $places[3]."00"];
					if($lastTwo != 0){
						$textResult .= ' و'.$this->makeNumber2Text($lastTwo);
					}
				}
				break; 
				case 4: // 4 numbrs
				{
					$lastThree = (int) $places[3].$places[2].$places[1];
					$textResult .= ($places[4] > 2) ? $def[$places[4]].'ة الاف' : $def[(int) $places[4]."000"];
					if($lastThree != 0){
						$textResult .= ' و'.$this->makeNumber2Text($lastThree);
					}
				}
				break;
				case 5: // 5 numbers
				{    
					$lastThree = (int) $places[3].$places[2].$places[1];
					$textResult .= $this->makeNumber2Text((int) $places[5].$places[4]).((((int) $places[5].$places[4]) != 10) ? ' الفاً' : ' الاف');
					if($lastThree != 0){
						$textResult .= ' و'.$this->makeNumber2Text($lastThree);
					}
				}
				break;
				case 6: // 6 numbers
				{    
					$lastThree = (int) $places[3].$places[2].$places[1];
					$textResult .= $this->makeNumber2Text((int) $places[6].$places[5].$places[4]).((((int) $places[5].$places[4]) != 10) ? ' الفاً' : ' الاف');
					if($lastThree != 0){
						$textResult .= ' و'.$this->makeNumber2Text($lastThree);
					}
				}
				break;
				case 7: // 7 numbers 1 mill
				{    
					$textResult .= ($places[7] > 2) ? $def[$places[7]].' ملايين' : $def[(int) $places[7]."000000"];
					$textResult .= ' و';
					$textResult .= $this->makeNumber2Text((int) $places[6].$places[5].$places[4].$places[3].$places[2].$places[1]);
				}
				break;
				case 8: // 8 numbers 10 mill
				case 9: // 9 numbers 100 mill
				{    
					$places[9] = (isset($places[9])) ? $places[9] : '';
					$firstThree = (int) $places[9].$places[8].$places[7];
					$textResult .=     $this->makeNumber2Text($firstThree);
					$textResult .=    ($firstThree < 11) ? ' ملايين ' : ' مليونا ';
					if(((int) $places[6].$places[5].$places[4].$places[3].$places[2].$places[1]) != 0){
						$textResult .= ' و';
						$textResult .=    $this->makeNumber2Text((int) $places[6].$places[5].$places[4].$places[3].$places[2].$places[1]);
					}
				}
				break;
				default:
				{
					$textResult = 'هذا رقم كبير .. ';
				}
			}
	
		}
		return $textResult;
	}
}
