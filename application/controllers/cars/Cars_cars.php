<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cars_cars extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		$this->load->model("cars/M_cars_model");
        $this->load->model("cars/M_cars_cars");
        $this->load->model("cars/M_cars_cars_images");
        $this->load->model("sales/M_sale_customer");
	}
	
	public function index()
	{
		$data['DataRows'] = $this->M_cars_cars->GetMultiRow();
		$data['content_page'] = "cars/cars_cars";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "cars/cars_cars_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("chassis_no","chassis_no","required");
		$this->form_validation->set_rules("plate_No","plate_No","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
			$customer_id = set_value('customer_id');
            $chassis_no = set_value('chassis_no');
            $model_id = set_value('model_id');
            $model_name = set_value('model_name');
			$plate_No = set_value('plate_No');
            $meter_reading = set_value('meter_reading');
            $motor_number = set_value('motor_number');
            $color = set_value('color');
            $oil_change_date = set_value('oil_change_date');
            $oil_change_every = set_value('oil_change_every');
            $notes = set_value('notes');
			$deleted = 0;
            
            $category_id = 1; //
            $parent_id = $customer_id; //
            $account_no = $chassis_no;
            $ar_title = $customer_id." : ".$chassis_no;
            $en_title = $customer_id." : ".$chassis_no;
            $thevalue = 0;
            
            $TreeAccount_array = array();
            $TreeAccount_array = array(
                'category_id' => 1,
                'parent_id' => $parent_id,
                'account_no' => $account_no,
                'title' => $ar_title,
                'title_en' => $en_title,
                'startamount' => $thevalue,
                'basic' => 0,
                'level_no' => 3,
                'deleted' => 0,
            );
            $this->M_fin_treeaccount->InsertRecord($TreeAccount_array);
            $AccountData = $this->M_fin_treeaccount->GetAccountID($ar_title); 
            $AccountID = $AccountData->id;

			$NewData = array();
			$NewData = array(
                'id' => $AccountID,
                'company_id' => $company_id,
                'customer_id' => $customer_id,
				'chassis_no' => $chassis_no,
				'model_id' => $model_id,
                'model_name' => $model_name,
                'plate_No' => $plate_No,
                'meter_reading' => $meter_reading,
                'motor_number' => $motor_number,
                'color' => $color,
                'oil_change_date' => $oil_change_date,
                'oil_change_every' => $oil_change_every,
                'notes' => $notes,
				'deleted' => $deleted,
			);

			$this->M_cars_cars->InsertRecord($NewData);
			
            $LatestCar = $this->M_cars_cars->GetLatest($customer_id, $chassis_no, $model_id);
            $car_id = $LatestCar->id;

            //M_cars_cars_images
            $count = count($_FILES['files']['name']);
            for($i=0;$i<$count;$i++){
                if(!empty($_FILES['files']['name'][$i])){
                    $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                    $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                    $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                    $_FILES['file']['size'] = $_FILES['files']['size'][$i];
          
                    $config['upload_path'] = 'upload/cars/'; 
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                    $config['max_size'] = '50000'; // max_size in kb
                    //$config['file_name'] = $_FILES['files']['name'][$i];
                    $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
                    $exts = explode(".", $_FILES['files']['name'][$i]);
                    $exts = end($exts);
                    $exts = strtolower($exts);
                    $config['file_name'] = $filename.".".$exts;
                    $config['file_ext_tolower'] = TRUE;
                    $config['remove_spaces'] = TRUE;

                    $this->load->library('upload',$config); 
                    $this->upload->initialize($config);
                    if($this->upload->do_upload('file')){
                        $uploadData = $this->upload->data();
                        $filename = $uploadData['file_name'];
            
                        $FileData = array();
                        $FileData = array(
                            'car_id' => $car_id,
                            'image' => $filename,
                        );
                        $this->M_cars_cars_images->InsertRecord($FileData);
                    }
                }
            }

			redirect ('cars/cars_cars');
		}
	}

	public function updateform($rid)
	{
		$DataRow = $this->M_cars_cars->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['customer_id'] = $DataRow->customer_id;
		$data['chassis_no'] = $DataRow->chassis_no;
        $data['model_id'] = $DataRow->model_id;
        $data['model_name'] = $DataRow->model_name;
        $data['plate_No'] = $DataRow->plate_No;
        $data['meter_reading'] = $DataRow->meter_reading;
        $data['motor_number'] = $DataRow->motor_number;
        $data['color'] = $DataRow->color;
        $data['oil_change_date'] = $DataRow->oil_change_date;
        $data['oil_change_every'] = $DataRow->oil_change_every;
        $data['notes'] = $DataRow->notes;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "cars/cars_cars_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("chassis_no","chassis_no","required");
		$this->form_validation->set_rules("plate_No","plate_No","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
			$customer_id = set_value('customer_id');
            $chassis_no = set_value('chassis_no');
            $model_id = set_value('model_id');
            $model_name = set_value('model_name');
			$plate_No = set_value('plate_No');
            $meter_reading = set_value('meter_reading');
            $motor_number = set_value('motor_number');
            $color = set_value('color');
            $oil_change_date = set_value('oil_change_date');
            $oil_change_every = set_value('oil_change_every');
            $notes = set_value('notes');
			$deleted = set_value('deleted');

			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'customer_id' => $customer_id,
				'chassis_no' => $chassis_no,
				'model_id' => $model_id,
                'model_name' => $model_name,
                'plate_No' => $plate_No,
                'meter_reading' => $meter_reading,
                'motor_number' => $motor_number,
                'color' => $color,
                'oil_change_date' => $oil_change_date,
                'oil_change_every' => $oil_change_every,
                'notes' => $notes,
				'deleted' => $deleted,
			);
			

			$this->M_cars_cars->UpdateRecord($id, $NewData);
			
			redirect ('cars/cars_cars');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_cars_cars->UpdateRecord($rid, $NewData);
        $this->db->delete('cars_cars', array('id' => $rid));
        redirect("cars/cars_cars");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_cars_cars->UpdateRecord($rid, $NewData);

        redirect("cars/cars_cars");
	}

    
}
