<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Exception\RequestException;
use Twilio\Rest\Client;
require 'vendor/autoload.php';

class Whatsapp_chat extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		$this->load->model("sales/M_sale_customer");
	}
	
	public function index()
	{
		
		$data['Customers'] = $this->M_sale_customer->GetMultiRow();
		$data['content_page'] = "support/whatsapp_chat";
		$this->load->view('page', $data);
    }

    public function recipient($recipient_number)
    {
        $this->session->set_userdata('recipient_number', $recipient_number);
        $this->load->library('user_agent');
		redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function sendWhatsAppMessage() {
        $recipient = $this->input->post('recipient');
        $message = $this->input->post('recipient');
        $twilio_whatsapp_number = TWILIO_WHATSAPP_NUMBER;
        $account_sid = TWILIO_SID;
        $auth_token = TWILIO_AUTH_TOKEN;
        $client = new Client($account_sid, $auth_token);
        return $client->messages->create("whatsapp:$recipient", array('from' => "whatsapp:$twilio_whatsapp_number", 'body' => $message));
    }

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "store/store_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("title_en","title_en","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $user_id = set_value("user_id");
			$web_store = set_value("web_store");
			$title = set_value('title');
			$title_en = set_value('title_en');
			$email = set_value('email');
			$phone = set_value('phone');
			$deleted = set_value('deleted');


			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'user_id' => $user_id,
				'web_store' => $web_store,
				'title' => $title,
				'title_en' => $title_en,
				'email' => $email,
				'phone' => $phone,
				'deleted' => $deleted,
			);

			$this->M_store->InsertRecord($NewData);
			
			redirect ('store/store');
		}
	}

	public function updateform($rid)
	{
		$DataRow = $this->M_store->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
        $data['user_id'] = $DataRow->user_id;
		$data['web_store'] = $DataRow->web_store;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['email'] = $DataRow->email;
		$data['phone'] = $DataRow->phone;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "store/store_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("title_en","title_en","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $user_id = set_value("user_id");
			$web_store = set_value("web_store");
			$title = set_value('title');
			$title_en = set_value('title_en');
			$email = set_value('email');
			$phone = set_value('phone');
			$deleted = set_value('deleted');

			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'user_id' => $user_id,
				'web_store' => $web_store,
				'title' => $title,
				'title_en' => $title_en,
				'email' => $email,
				'phone' => $phone,
				'deleted' => $deleted,
			);
			

			$this->M_store->UpdateRecord($id, $NewData);
			
			redirect ('store/store');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_store->UpdateRecord($rid, $NewData);

        redirect("store/store");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_store->UpdateRecord($rid, $NewData);

        redirect("store/store");
	}
}
