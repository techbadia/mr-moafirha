<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tic_ticket extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
        $this->load->model("support/M_tic_category");
        $this->load->model("support/M_tic_priority");
        $this->load->model("support/M_tic_status");
        $this->load->model("support/M_tic_ticket");
        $this->load->model("sales/M_sale_customer");
        $this->load->model("permissions/M_usr_users");

        
	}
	
	public function index()
	{
        $this->session->set_userdata('Filter', "");

        $data['Category'] = $this->M_tic_category->GetMultiRow();
        $data['Priority'] = $this->M_tic_priority->GetMultiRow();
        $data['Status'] = $this->M_tic_status->GetMultiRow();
        $data['Customer'] = $this->M_sale_customer->GetMultiRow();
        $data['User'] = $this->M_usr_users->GetMultiRow();

		$fromdate = date("Y-m-d");
		$todate = date("Y-m-d");
		$category_id = 0;
		$priority_id = 0;
        $status_id = 0;
        $customer_id = 0;
        $user_id = 0;
        $assign_to = 0;

		$data['fromdate'] = $fromdate;
		$data['todate'] = $todate;
		$data['category_id'] = $category_id;
		$data['priority_id'] = $priority_id;
        $data['status_id'] = $status_id;
        $data['customer_id'] = $customer_id;
        $data['user_id'] = $user_id;
        $data['assign_to'] = $assign_to;
        
        $data['DataRows'] = $this->M_tic_ticket->GetMultiRow($fromdate, $todate, $category_id, $priority_id, $status_id, 
        $customer_id, $user_id, $assign_to);
		$data['content_page'] = "support/tic_ticket";
		$this->load->view('page', $data);
	}


    public function filter()
    {
        $this->session->set_userdata('Filter', "");

        $data['Category'] = $this->M_tic_category->GetMultiRow();
        $data['Priority'] = $this->M_tic_priority->GetMultiRow();
        $data['Status'] = $this->M_tic_status->GetMultiRow();
        $data['Customer'] = $this->M_sale_customer->GetMultiRow();
        $data['User'] = $this->M_usr_users->GetMultiRow();

        $this->form_validation->set_rules("fromdate","fromdate","required");
        if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
        }
        else
        {
            $fromdate = set_value("fromdate");
			$todate = set_value("todate");
            $category_id = set_value("category_id");
            $priority_id = set_value("priority_id");
            $status_id = set_value("status_id");
			$customer_id = set_value("customer_id");
			$user_id = set_value("user_id");
			$assign_to = set_value("assign_to");
	
			$data['fromdate'] = $fromdate;
            $data['todate'] = $todate;
            $data['category_id'] = $category_id;
            $data['priority_id'] = $priority_id;
            $data['status_id'] = $status_id;
            $data['customer_id'] = $customer_id;
            $data['user_id'] = $user_id;
            $data['assign_to'] = $assign_to;

            $data['DataRows'] = $this->M_tic_ticket->GetMultiRow($fromdate, $todate, $category_id, $priority_id, $status_id, 
            $customer_id, $user_id, $assign_to);
            $data['content_page'] = "support/tic_ticket";
            $this->load->view('page', $data);
        }

    }

	public function insertform()
	{

        $this->session->set_userdata('Filter', "");

        $data['Category'] = $this->M_tic_category->GetMultiRow();
        $data['Priority'] = $this->M_tic_priority->GetMultiRow();
        $data['Status'] = $this->M_tic_status->GetMultiRow();
        $data['Customer'] = $this->M_sale_customer->GetMultiRow();
        $data['User'] = $this->M_usr_users->GetMultiRow();

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "support/tic_ticket_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
        $this->session->set_userdata('Filter', "");
		//brand
		$this->form_validation->set_rules("details","details","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
			$category_id = set_value('category_id');
            $priority_id = set_value('priority_id');
            $status_id = set_value('status_id');
            $customer_id = set_value('customer_id');
            $user_id = set_value('user_id');
            $assign_to = set_value('assign_to');
            $details = set_value('details');
            $thedate = date("Y-m-d");
            $thetime = date("h:m");
			$deleted = set_value('deleted');


			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
				'category_id' => $category_id,
                'priority_id' => $priority_id,
                'status_id' => $status_id,
                'customer_id' => $customer_id,
                'user_id' => $user_id,
                'assign_to' => $assign_to,
                'details' => $details,
                'thedate' => $thedate,
                'thetime' => $thetime,
				'deleted' => $deleted,
			);

			$this->M_tic_ticket->InsertRecord($NewData);
            
            $action = $this->input->post('action');
            if($action == 'Save')
            {
                redirect ('support/tic_ticket');
            }
            else
            {
                redirect ('support/tic_ticket/insertform');
            }
		}
	}

	public function updateform($rid)
	{
        $this->session->set_userdata('Filter', "");

        $data['Category'] = $this->M_tic_category->GetMultiRow();
        $data['Priority'] = $this->M_tic_priority->GetMultiRow();
        $data['Status'] = $this->M_tic_status->GetMultiRow();
        $data['Customer'] = $this->M_sale_customer->GetMultiRow();
        $data['User'] = $this->M_usr_users->GetMultiRow();

		$DataRow = $this->M_tic_ticket->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
        $data['priority_id'] = $DataRow->priority_id;
        $data['status_id'] = $DataRow->status_id;
        $data['customer_id'] = $DataRow->customer_id;
        $data['user_id'] = $DataRow->user_id;
        $data['assign_to'] = $DataRow->assign_to;
        $data['details'] = $DataRow->details;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "support/tic_ticket_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
        $this->session->set_userdata('Filter', "");

		$this->form_validation->set_rules("details","details","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
			$category_id = set_value('category_id');
            $priority_id = set_value('priority_id');
            $status_id = set_value('status_id');
            $customer_id = set_value('customer_id');
            $assign_to = set_value('assign_to');
            $details = set_value('details');
			$deleted = set_value('deleted');


			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
				'category_id' => $category_id,
                'priority_id' => $priority_id,
                'status_id' => $status_id,
                'customer_id' => $customer_id,
                'assign_to' => $assign_to,
                'details' => $details,
				'deleted' => $deleted,
			);
			

            $this->M_tic_ticket->UpdateRecord($id, $NewData);
            
            if($status_id == 2)
            {
                $user_id = intval($this->session->userdata('StaffID'));
                $affiliate_id = 0;
                $thedate = date("Y-m-d");
                $thetime = date("h:m");
                $insert_date = date("Y-m-d");
                $insert_time = date("h:m");

                $NewData = array();
                $NewData = array(
                    'company_id' => $company_id,
                    'user_id' => $user_id,
                    'affiliate_id' => $affiliate_id,
                    'customer_id' => $customer_id,
                    'status_id' => 1,
                    'area_id' => 1,
                    'source' => 1,
                    'details' => $details,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                    'insert_date' => $insert_date,
                    'insert_time' => $insert_time,
                    'deleted' => $deleted,
                );

                $this->M_data_request->InsertRecord($NewData);
            }
			
			redirect ('support/tic_ticket');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_tic_ticket->UpdateRecord($rid, $NewData);

        redirect("support/tic_ticket");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_tic_ticket->UpdateRecord($rid, $NewData);

        redirect("support/tic_ticket");
    }
    
    public function category($category_id)
    {
        $data['Category'] = $this->M_tic_category->GetMultiRow();
        $data['Priority'] = $this->M_tic_priority->GetMultiRow();
        $data['Status'] = $this->M_tic_status->GetMultiRow();
        $data['Customer'] = $this->M_sale_customer->GetMultiRow();
        $data['User'] = $this->M_usr_users->GetMultiRow();

        $CategoryData = $this->M_tic_category->GetRow($category_id);
        if ($this->session->userdata('lang') == "ar")
        {
            $FilterName = $CategoryData->ar_title;
        }
        else
        {
            $FilterName = $CategoryData->en_title;
        }
        $this->session->set_userdata('Filter', $FilterName);

        $fromdate = date("2021-01-01");
		$todate = date("Y-m-d");
        $priority_id = 0;
        $status_id = 0;
        $customer_id = 0;
        $user_id = 0;
        $assign_to = 0;

        $data['fromdate'] = $fromdate;
        $data['todate'] = $todate;
        $data['category_id'] = $category_id;
        $data['priority_id'] = $priority_id;
        $data['status_id'] = $status_id;
        $data['customer_id'] = $customer_id;
        $data['user_id'] = $user_id;
        $data['assign_to'] = $assign_to;

        $data['DataRows'] = $this->M_tic_ticket->GetMultiRow($fromdate, $todate, $category_id, $priority_id, $status_id, 
        $customer_id, $user_id, $assign_to);
        $data['content_page'] = "support/tic_ticket";
        $this->load->view('page', $data);
    }

    public function priority($priority_id)
    {
        $data['Category'] = $this->M_tic_category->GetMultiRow();
        $data['Priority'] = $this->M_tic_priority->GetMultiRow();
        $data['Status'] = $this->M_tic_status->GetMultiRow();
        $data['Customer'] = $this->M_sale_customer->GetMultiRow();
        $data['User'] = $this->M_usr_users->GetMultiRow();

        $PriorityData = $this->M_tic_priority->GetRow($priority_id);
        if ($this->session->userdata('lang') == "ar")
        {
            $FilterName = $PriorityData->ar_title;
        }
        else
        {
            $FilterName = $PriorityData->en_title;
        }
        $this->session->set_userdata('Filter', $FilterName);

        $fromdate = date("2021-01-01");
        $todate = date("Y-m-d");
        $category_id = 0;
        //$priority_id = 0;
        $status_id = 0;
        $customer_id = 0;
        $user_id = 0;
        $assign_to = 0;

        $data['fromdate'] = $fromdate;
        $data['todate'] = $todate;
        $data['category_id'] = $category_id;
        $data['priority_id'] = $priority_id;
        $data['status_id'] = $status_id;
        $data['customer_id'] = $customer_id;
        $data['user_id'] = $user_id;
        $data['assign_to'] = $assign_to;

        $data['DataRows'] = $this->M_tic_ticket->GetMultiRow($fromdate, $todate, $category_id, $priority_id, $status_id, 
        $customer_id, $user_id, $assign_to);
        $data['content_page'] = "support/tic_ticket";
        $this->load->view('page', $data);
    }

    public function status($status_id)
    {
        $data['Category'] = $this->M_tic_category->GetMultiRow();
        $data['Priority'] = $this->M_tic_priority->GetMultiRow();
        $data['Status'] = $this->M_tic_status->GetMultiRow();
        $data['Customer'] = $this->M_sale_customer->GetMultiRow();
        $data['User'] = $this->M_usr_users->GetMultiRow();

        $StatusData = $this->M_tic_status->GetRow($status_id);
        if ($this->session->userdata('lang') == "ar")
        {
            $FilterName = $StatusData->ar_title;
        }
        else
        {
            $FilterName = $StatusData->en_title;
        }
        $this->session->set_userdata('Filter', $FilterName);

        $fromdate = date("2021-01-01");
        $todate = date("Y-m-d");
        $category_id = 0;
        $priority_id = 0;
        //$status_id = 0;
        $customer_id = 0;
        $user_id = 0;
        $assign_to = 0;

        $data['fromdate'] = $fromdate;
        $data['todate'] = $todate;
        $data['category_id'] = $category_id;
        $data['priority_id'] = $priority_id;
        $data['status_id'] = $status_id;
        $data['customer_id'] = $customer_id;
        $data['user_id'] = $user_id;
        $data['assign_to'] = $assign_to;

        $data['DataRows'] = $this->M_tic_ticket->GetMultiRow($fromdate, $todate, $category_id, $priority_id, $status_id, 
        $customer_id, $user_id, $assign_to);
        $data['content_page'] = "support/tic_ticket";
        $this->load->view('page', $data);
    }

    public function assign_to($assign_to)
    {
        $data['Category'] = $this->M_tic_category->GetMultiRow();
        $data['Priority'] = $this->M_tic_priority->GetMultiRow();
        $data['Status'] = $this->M_tic_status->GetMultiRow();
        $data['Customer'] = $this->M_sale_customer->GetMultiRow();
        $data['User'] = $this->M_usr_users->GetMultiRow();

        $UserData = $this->M_usr_users->GetRow($assign_to);
        $FilterName = $UserData->fullname;
        $this->session->set_userdata('Filter', $FilterName);

        $fromdate = date("2021-01-01");
        $todate = date("Y-m-d");
        $category_id = 0;
        $priority_id = 0;
        $status_id = 0;
        $customer_id = 0;
        $user_id = 0;
        //$assign_to = 0;

        $data['fromdate'] = $fromdate;
        $data['todate'] = $todate;
        $data['category_id'] = $category_id;
        $data['priority_id'] = $priority_id;
        $data['status_id'] = $status_id;
        $data['customer_id'] = $customer_id;
        $data['user_id'] = $user_id;
        $data['assign_to'] = $assign_to;

        $data['DataRows'] = $this->M_tic_ticket->GetMultiRow($fromdate, $todate, $category_id, $priority_id, $status_id, 
        $customer_id, $user_id, $assign_to);
        $data['content_page'] = "support/tic_ticket";
        $this->load->view('page', $data);
    }
}
