<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tec_stores extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		$this->salt_length = 10;
		$this->hash_method    = 'sha1';
        $this->default_rounds = 8;
        $this->random_rounds  = FALSE;
        $this->min_rounds     = 5;
        $this->max_rounds     = 9;

		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		$this->load->model("pos_sys/M_Tec_stores");
		$this->load->model("pos_sys/M_Tec_users");
		$this->load->model("store/M_store");
	}
	
	public function index()
	{
		
		$data['DataRows'] = $this->M_Tec_stores->GetMultiRow();
		$data['content_page'] = "pos_sys/tec_stores";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "pos_sys/tec_stores_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("name","name","required");
		$this->form_validation->set_rules("code","code","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
			$name = set_value("name");
            $code = set_value("code");
			$email = set_value('email');
			$phone = set_value('phone');
			$address1 = set_value('address1');
			$address2 = set_value('address2');
			$city = set_value('city');
            $state = set_value('state');
            $postal_code = set_value('postal_code');
            $country = set_value('country');
            $currency_code = set_value('currency_code');
            $receipt_header = set_value('receipt_header');
            $receipt_footer = set_value('receipt_footer');


            $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./pos/uploads/";
			$ImageDirectory = $UploadPath;
			
			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
            }

			//Insert into store
			$company_id = intval($this->session->userdata('company_id'));
            $user_id = intval($this->session->userdata('StaffID'));
			$web_store = 1;
            $parent_id = 0;
			$title = $name;
			$title_en = $name;
			//$email = set_value('email');
			//$phone = set_value('phone');
			$deleted = 0;


			$StoreData = array();
			$StoreData = array(
                'company_id' => $company_id,
                'user_id' => $user_id,
				'web_store' => $web_store,
                'parent_id' => $parent_id,
				'title' => $title,
				'title_en' => $title_en,
				'email' => $email,
				'phone' => $phone,
				'deleted' => $deleted,
			);

			$this->M_store->InsertRecord($StoreData);
			$LatestStore = $this->M_store->GetLatestRow($title, $email, $phone);
			$store_id = $LatestStore->id;
			// End insert into store

			//Insert into tec_stores
			$NewData = array();
			$NewData = array(
				'id' => $store_id,
				'name' => $name,
                'code' => $code,
                'logo' => $NewFileName,
				'email' => $email,
				'phone' => $phone,
				'address1' => $address1,
				'address2' => $address2,
				'city' => $city,
                'state' => $state,
                'postal_code' => $postal_code,
                'country' => $country,
                'currency_code' => $currency_code,
                'receipt_header' => $receipt_header,
                'receipt_footer' => $receipt_footer,
			);

			$this->M_Tec_stores->InsertRecord($NewData);
			// End Insert into tec_stores
			
			redirect ('pos_sys/tec_stores');
		}
	}

	public function updateform($rid)
	{
		$DataRow = $this->M_Tec_stores->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['name'] = $DataRow->name;
        $data['code'] = $DataRow->code;
		$data['email'] = $DataRow->email;
        $data['phone'] = $DataRow->phone;
		$data['address1'] = $DataRow->address1;
		$data['address2'] = $DataRow->address2;
		$data['city'] = $DataRow->city;
		$data['state'] = $DataRow->state;
		$data['postal_code'] = $DataRow->postal_code;
        $data['country'] = $DataRow->country;
        $data['currency_code'] = $DataRow->currency_code;
        $data['receipt_header'] = $DataRow->receipt_header;
        $data['receipt_footer'] = $DataRow->receipt_footer;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "pos_sys/tec_stores_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("name","name","required");
		$this->form_validation->set_rules("code","code","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $name = set_value("name");
            $code = set_value("code");
			$email = set_value('email');
			$phone = set_value('phone');
			$address1 = set_value('address1');
			$address2 = set_value('address2');
			$city = set_value('city');
            $state = set_value('state');
            $postal_code = set_value('postal_code');
            $country = set_value('country');
            $currency_code = set_value('currency_code');
            $receipt_header = set_value('receipt_header');
            $receipt_footer = set_value('receipt_footer');
            $ChangeImage = set_value('ChangeImage');


			$NewData = array();
            if ($ChangeImage =="True")
            {
                $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
                $exts = explode(".", $_FILES['image']['name']);
                $exts = end($exts);
                $exts = strtolower($exts);
                $UploadPath = "./pos/uploads/";
                $ImageDirectory = $UploadPath;
                
                if (!is_dir($ImageDirectory)) {
                    mkdir($ImageDirectory, 0777, TRUE);

                }

                $config['upload_path'] = $ImageDirectory;
                $config['allowed_types'] = "*";
                $config['file_name'] = $filename.".".$exts;
                $config['overwrite'] = TRUE;
                $config['file_ext_tolower'] = TRUE;
                $config['remove_spaces'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $NewFileName = $filename.".".$exts;
                if (! $this->upload->do_upload('image', $NewFileName))
                {
                    $error = array('error' => $this->upload->display_errors());
                    $NewFileName ="";
                }
                else {
                    $this->upload->do_upload('image', $NewFileName);
                }
                
                $NewData = array(
                    'name' => $name,
                    'code' => $code,
                    'logo' => $NewFileName,
                    'email' => $email,
                    'phone' => $phone,
                    'address1' => $address1,
                    'address2' => $address2,
                    'city' => $city,
                    'state' => $state,
                    'postal_code' => $postal_code,
                    'country' => $country,
                    'currency_code' => $currency_code,
                    'receipt_header' => $receipt_header,
                    'receipt_footer' => $receipt_footer,
                );
            }
            else
            {
                $NewData = array(
                    'name' => $name,
                    'code' => $code,
                    'email' => $email,
                    'phone' => $phone,
                    'address1' => $address1,
                    'address2' => $address2,
                    'city' => $city,
                    'state' => $state,
                    'postal_code' => $postal_code,
                    'country' => $country,
                    'currency_code' => $currency_code,
                    'receipt_header' => $receipt_header,
                    'receipt_footer' => $receipt_footer,
                );
            }

			$this->M_Tec_stores->UpdateRecord($id, $NewData);
			
			redirect ('pos_sys/tec_stores');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_Tec_stores->UpdateRecord($rid, $NewData);

        redirect("pos_sys/tec_stores");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_Tec_stores->UpdateRecord($rid, $NewData);

        redirect("pos_sys/tec_stores");
	}

	public function tec_users($rid)
	{
		$data['id'] = $rid;

		$data['DataRows'] = $this->M_Tec_users->GetStoreUsers($rid);

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "pos_sys/tec_users";
		$this->load->view('page', $data);
		
	}

	public function tec_users_insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "pos_sys/tec_users_insert";
		$this->load->view('page', $data);
	}

	public function tec_users_insert_data()
	{
		$this->form_validation->set_rules("username","username","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
			$username = set_value("username");
            $password = $this->hash_password(set_value("password"));
			$email = set_value('email');
			$created_on = set_value('created_on');
			$last_login = set_value('last_login');
			$active = set_value('active');
			$first_name = set_value('first_name');
			$last_name = set_value('last_name');
			$phone = set_value('phone');
			$gender = set_value('gender');
            $group_id = set_value('group_id');
			$store_id = set_value('store_id');

			$NewData = array();
			$NewData = array(
				'username' => $username,
                'password' => $password,
				'email' => $email,
				'created_on' => $created_on,
				'last_login' => $last_login,
				'active' => $active,
				'first_name' => $first_name,
				'last_name' => $last_name,
				'phone' => $phone,
				'gender' => $gender,
                'group_id' => $group_id,
				'store_id' => $store_id,
			);

			$this->M_Tec_users->InsertRecord($NewData);
			
			redirect ('pos_sys/tec_stores/');
		}
	}

	public function tec_users_updateform($rid)
	{
		$DataRow = $this->M_Tec_users->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['username'] = $DataRow->username;
        $data['email'] = $DataRow->email;
		$data['active'] = $DataRow->active;
        $data['first_name'] = $DataRow->first_name;
		$data['last_name'] = $DataRow->last_name;
		$data['phone'] = $DataRow->phone;
		$data['gender'] = $DataRow->gender;
		$data['store_id'] = $DataRow->store_id;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "pos_sys/tec_users_update";
		$this->load->view('page', $data);
	}

	public function tec_users_update_data()
	{
		$this->form_validation->set_rules("username","username","required");
		$this->form_validation->set_rules("first_name","first_name","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $username = set_value("username");
            $password = $this->hash_password(set_value("password"));
			$email = set_value('email');
			$active = set_value('active');
			$first_name = set_value('first_name');
			$last_name = set_value('last_name');
			$phone = set_value('phone');
            $gender = set_value('gender');
            $store_id = set_value('store_id');

            $ChangePassword = set_value('ChangePassword');


			$NewData = array();
            if ($ChangePassword =="True")
            {
                $NewData = array(
                    'username' => $username,
                    'password' => $password,
                    'email' => $email,
                    'active' => $active,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'phone' => $phone,
                    'gender' => $gender,
                    'store_id' => $store_id,
                );
            }
            else
            {
                $NewData = array(
                    'username' => $username,
                    'email' => $email,
                    'active' => $active,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'phone' => $phone,
                    'gender' => $gender,
                    'store_id' => $store_id,
                );
            }

			$this->M_Tec_users->UpdateRecord($id, $NewData);
			
			redirect ('pos_sys/tec_stores');
		}
	}

	public function hash_password($password, $salt = false, $use_sha1_override = false)
    {
        if (empty($password)) {
            return false;
        }

        //bcrypt
        if ($use_sha1_override === false && $this->hash_method == 'bcrypt') {
            return $this->bcrypt->hash($password);
        }

        if ($this->store_salt && $salt) {
            return sha1($password . $salt);
        }
        $salt = $this->salt();
        return $salt . substr(sha1($salt . $password), 0, -$this->salt_length);
    }

	public function salt()
    {
        return substr(md5(uniqid(rand(), true)), 0, $this->salt_length);
    }


}
