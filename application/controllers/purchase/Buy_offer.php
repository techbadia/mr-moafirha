<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buy_offer extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		
		$this->load->model("purchase/M_buy_offer");
		$this->load->model("purchase/M_buy_offer_items");
		$this->load->model("purchase/M_buy_manufacturer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");
	}
	
	public function index()
	{
		$this->session->set_userdata('Filter', lang('buy_offer_date_all'));

		$data['DataRows'] = $this->M_buy_offer->GetUnDeleted();
		$data['content_page'] = "purchase/buy_offer";
		$this->load->view('page', $data);
	}

	public function deleted()
	{
		$this->session->set_userdata('Filter', lang('Deleted'));

		$data['DataRows'] = $this->M_buy_offer->GetDeleted();
		$data['content_page'] = "purchase/buy_offer";
		$this->load->view('page', $data);
	}

	public function date_not_expired()
	{
		$this->session->set_userdata('Filter', lang('buy_offer_date_not_expired'));

		$data['DataRows'] = $this->M_buy_offer->GetNotExpired();
		$data['content_page'] = "purchase/buy_offer";
		$this->load->view('page', $data);
	}

	public function date_expired()
	{
		$this->session->set_userdata('Filter', lang('buy_offer_date_expired'));

		$data['DataRows'] = $this->M_buy_offer->GetExpired();
		$data['content_page'] = "purchase/buy_offer";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$this->session->set_userdata('Filter', "");

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "purchase/buy_offer_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		$this->session->set_userdata('Filter', "");

		$company_id = intval($this->session->userdata('company_id'));
		$StaffID = intval($this->session->userdata('StaffID'));

		$this->form_validation->set_rules("supplier_id","supplier_id","required");
		$this->form_validation->set_rules("tree_id","tree_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = $company_id;
			$supplier_id = set_value("supplier_id");
			$user_id = $StaffID;
			$thedate = set_value('thedate');
			$expired_date = set_value('expired_date');
			$totalvalue = set_value('totalvalue');
			$paid = set_value('paid');
			$remaining = set_value('remaining');
			$discount = set_value('discount');
			$over_cost = set_value('over_cost');
			$tax = set_value('tax');
			$notes = set_value('notes');
			$tree_id = set_value('tree_id');

			$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/buy_offer/";
			$ImageDirectory = $UploadPath;
			
			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
			}

			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'supplier_id' => $supplier_id,
				'user_id' => $user_id,
				'thedate' => $thedate,
				'expired_date' => $expired_date,
				'totalvalue' => $totalvalue,
				'paid' => $paid,
				'remaining' => $remaining,
				'discount' => $discount,
				'over_cost' => $over_cost,
				'tax' => $tax,
				'notes' => $notes,
				'image' => $NewFileName,
				'tree_id' => $tree_id,
			);

			$this->M_buy_offer->InsertRecord($NewData);

			$LatestRecord = $this->M_buy_offer->GetLatestRecord($company_id, $supplier_id, $user_id);
			$bill_id = $LatestRecord->id;

			$InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
			//M_product
			$barcode = "barcode";
			$location_id = "location_id";
			$quantity = "quantity";
			for ($i = 1; $i <= $InvoiceItems; $i++) {
				$quantity = "quantity".$i;
				$quantity = $this->input->post($quantity);
				if($quantity > 0)
				{
					$barcode = "barcode".$i;
					$barcode = $this->input->post($barcode);
					$ProductData = $this->M_product->GetByBarcode($barcode);
					$product_id = $ProductData->id;
					$location_id = "location_id".$i;
					$location_id = $this->input->post($location_id);
					$unitprice = "unitprice".$i;
					$unitprice = $this->input->post($unitprice);

					//M_buy_offer_items
					$ItemsArray = array();
					$ItemsArray = array(
						'bill_id' => $bill_id,
						'store_type_id' => $product_id,
						'location_id' => $location_id,
						'quantity' => $quantity,
						'unitprice' => $unitprice,
					);
					$this->M_buy_offer_items->InsertRecord($ItemsArray);
				}
				
			}

			redirect ('purchase/buy_offer');
		}
	}

	public function updateform($rid)
	{
		$this->session->set_userdata('Filter', "");

		$DataRow = $this->M_buy_offer->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['supplier_id'] = $DataRow->supplier_id;
		$data['user_id'] = $DataRow->user_id;
		$data['thedate'] = $DataRow->thedate;
		$data['expired_date'] = $DataRow->expired_date;
		$data['totalvalue'] = $DataRow->totalvalue;
		$data['paid'] = $DataRow->paid;
		$data['remaining'] = $DataRow->remaining;
		$data['discount'] = $DataRow->discount;
		$data['over_cost'] = $DataRow->over_cost;
		$data['tax'] = $DataRow->tax;
		$data['notes'] = $DataRow->notes;
		$data['image'] = $DataRow->image;
		$data['tree_id'] = $DataRow->tree_id;
		$data['deleted'] = $DataRow->deleted;

		$data['DetailsRows'] = $this->M_buy_offer_items->GetMultiRow($rid);

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "purchase/buy_offer_update";
		$this->load->view('page', $data);
	}

	public function Update_Data()
	{
		$this->session->set_userdata('Filter', "");

		$company_id = intval($this->session->userdata('company_id'));
		$StaffID = intval($this->session->userdata('StaffID'));

		$this->form_validation->set_rules("supplier_id","supplier_id","required");
		$this->form_validation->set_rules("tree_id","tree_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = $company_id;
			$supplier_id = set_value("supplier_id");
			$user_id = $StaffID;
			$thedate = set_value('thedate');
			$expired_date = set_value('expired_date');
			$totalvalue = set_value('totalvalue');
			$paid = set_value('paid');
			$remaining = set_value('remaining');
			$discount = set_value('discount');
			$over_cost = set_value('over_cost');
			$tax = set_value('tax');
			$notes = set_value('notes');
			$tree_id = set_value('tree_id');
			$ChangeImage = set_value('ChangeImage');

			if($ChangeImage == "True")
			{
				$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
				$exts = explode(".", $_FILES['image']['name']);
				$exts = end($exts);
				$exts = strtolower($exts);
				$UploadPath = "./upload/buy_offer/";
				$ImageDirectory = $UploadPath;
				
				if (!is_dir($ImageDirectory)) {
					mkdir($ImageDirectory, 0777, TRUE);
	
				}

				$config['upload_path'] = $ImageDirectory;
				$config['allowed_types'] = "*";
				$config['file_name'] = $filename.".".$exts;
				$config['overwrite'] = TRUE;
				$config['file_ext_tolower'] = TRUE;
				$config['remove_spaces'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				$NewFileName = $filename.".".$exts;
				if (! $this->upload->do_upload('image', $NewFileName))
				{
					$error = array('error' => $this->upload->display_errors());
					$NewFileName ="";
				}
				else {
					$this->upload->do_upload('image', $NewFileName);
				}

				$NewData = array();
				$NewData = array(
					'company_id' => $company_id,
					'supplier_id' => $supplier_id,
					'user_id' => $user_id,
					'thedate' => $thedate,
					'expired_date' => $expired_date,
					'totalvalue' => $totalvalue,
					'paid' => $paid,
					'remaining' => $remaining,
					'discount' => $discount,
					'over_cost' => $over_cost,
					'tax' => $tax,
					'notes' => $notes,
					'image' => $NewFileName,
					'tree_id' => $tree_id,
				);
			}
			else
			{
				$NewData = array();
				$NewData = array(
					'company_id' => $company_id,
					'supplier_id' => $supplier_id,
					'user_id' => $user_id,
					'thedate' => $thedate,
					'expired_date' => $expired_date,
					'totalvalue' => $totalvalue,
					'paid' => $paid,
					'remaining' => $remaining,
					'discount' => $discount,
					'over_cost' => $over_cost,
					'tax' => $tax,
					'notes' => $notes,
					'tree_id' => $tree_id,
				);
			}

			$this->M_buy_offer->UpdateRecord($id, $NewData);
			$bill_id = $id;
			$DetailsRows = $this->M_buy_offer_items->GetMultiRow($id);
			$i = 0;
			$InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
			//M_product
			$barcode = "barcode";
			$location_id = "location_id";
			$quantity = "quantity";
			$item = "item";
			foreach($DetailsRows as $DetailsRows_Row) {
				$i += 1;
				$quantity = "quantity".$i;
				$quantity = $this->input->post($quantity);
				$item = "item".$i;
				$item = $this->input->post($item);
				if($quantity > 0)
				{
					$barcode = "barcode".$i;
					$barcode = $this->input->post($barcode);
					$ProductData = $this->M_product->GetByBarcode($barcode);
					$product_id = $ProductData->id;
					$location_id = "location_id".$i;
					$location_id = $this->input->post($location_id);
					$unitprice = "unitprice".$i;
					$unitprice = $this->input->post($unitprice);

					//M_buy_offer_items
					$ItemsArray = array();
					$ItemsArray = array(
						'bill_id' => $bill_id,
						'store_type_id' => $product_id,
						'location_id' => $location_id,
						'quantity' => $quantity,
						'unitprice' => $unitprice,
					);
					$this->M_buy_offer_items->UpdateRecord($item, $ItemsArray);
				}
				
			}

			redirect ('purchase/buy_offer');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_buy_offer->UpdateRecord($rid, $NewData);

        redirect("purchase/buy_offer");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_buy_offer->UpdateRecord($rid, $NewData);

        redirect("purchase/buy_offer");
	}

	public function FillProductsList($barcode){
        $CodeCount = $this->M_product->CheckBarcode($barcode);
        if($CodeCount > 0)
        {
            $TypeName = $this->M_product->GetByBarcode($barcode);
            echo $TypeName->title;
        }
        else
        {
            echo "-";
        }
	}

	public function view_main()
	{
		$id = $this->input->get('id');
		$MainDataRow = $this->M_buy_offer->GetInvoiceHeader($id);
		echo json_encode($MainDataRow); 
    	exit();
	}

	public function view_details()
	{
		$id = $this->input->get('id');
		//$id = intval($this->session->userdata('Fin_journal_View_ID'));
		$DetailsRows = $this->M_buy_offer->GetInvoiceDetails($id);
		echo json_encode($DetailsRows); 
		exit();
	}
}
