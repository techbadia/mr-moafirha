<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buy_manufacturer extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		
		$this->load->model("purchase/M_buy_manufacturer");
		$this->load->model("account/M_fin_treeaccount");
	} 
	
	public function index()
	{
		$data['DataRows'] = $this->M_buy_manufacturer->GetMultiRow();
		$data['content_page'] = "purchase/buy_manufacturer";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "purchase/buy_manufacturer_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("title_en","title_en","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = intval($this->session->userdata('company_id'));
			$user_id = intval($this->session->userdata('StaffID'));
			$title = set_value('title');
			$title_en = set_value('title_en');
			$phone = set_value('phone');
			$mobile = set_value('mobile');
			$address = set_value('address');
			$email = set_value('email');
			$commercial_register = set_value('commercial_register');
			$tax_card = set_value('tax_card');
			$person = set_value('person');
			$person_phone = set_value('person_phone');
			$credit_limit = set_value('credit_limit');
			$deleted = set_value('deleted');

			$CheckDuplicate = $this->M_buy_manufacturer->CheckDuplicate($title, $title_en);
			$supplier_id = 0;
			if($CheckDuplicate == 0)
			{
			    $parent_id =intval($this->session->userdata('acc_supplier'));
                $max=$this->M_fin_treeaccount->GetMaxAccount($parent_id)[0];


                $my_account= '';
                if(!is_null($max->max)){
                    $my_account=$max->max + 1;
                }else{
                    $ParentAccountData = $this->M_fin_treeaccount->GetRow($parent_id);
                    $my_account=$ParentAccountData->account_no + 1;
                }
                
				$AddData = array();
				$AddData = array(
					'company_id' => $company_id,
					'category_id' => 2,
					'parent_id' => intval($this->session->userdata('acc_supplier')),
					'account_no' => $my_account,
					'title' => $title,
					'title_en' => $title_en,
					'startamount' => set_value('startamount'),
					'basic' => 0,
					'level_no' => 4,
					'deleted' => 0,
				);
				$this->M_fin_treeaccount->InsertRecord($AddData);
				$GetData = $this->M_fin_treeaccount->FindLatestRow($title, $title_en);
				$supplier_id = $GetData->id;
			}
			else
			{
				redirect ('purchase/buy_manufacturer');
			}

			$NewData = array();
			$NewData = array(
				'id' => $supplier_id,
				'company_id' => $company_id,
				'user_id' => $user_id,
				'title' => $title,
				'title_en' => $title_en,
				'phone' => $phone,
				'mobile' => $mobile,
				'address' => $address,
				'email' => $email,
				'commercial_register' => $commercial_register,
				'tax_card' => $tax_card,
				'person' => $person,
				'person_phone' => $person_phone,
				'credit_limit' => $credit_limit,
				'deleted' => $deleted,
			);

			$this->M_buy_manufacturer->InsertRecord($NewData);
			
			redirect ('purchase/buy_manufacturer');
		}
	}

	public function updateform($rid)
	{
		$DataRow = $this->M_buy_manufacturer->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['user_id'] = $DataRow->user_id;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['phone'] = $DataRow->phone;
		$data['mobile'] = $DataRow->mobile;
		$data['address'] = $DataRow->address;
		$data['email'] = $DataRow->email;
		$data['commercial_register'] = $DataRow->commercial_register;
		$data['tax_card'] = $DataRow->tax_card;
		$data['person'] = $DataRow->person;
		$data['person_phone'] = $DataRow->person_phone;
		$data['credit_limit'] = $DataRow->credit_limit;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "purchase/buy_manufacturer_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("title_en","title_en","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = intval($this->session->userdata('company_id'));
			$user_id = intval($this->session->userdata('StaffID'));
			$title = set_value('title');
			$title_en = set_value('title_en');
			$phone = set_value('phone');
			$mobile = set_value('mobile');
			$address = set_value('address');
			$email = set_value('email');
			$commercial_register = set_value('commercial_register');
			$tax_card = set_value('tax_card');
			$person = set_value('person');
			$person_phone = set_value('person_phone');
			$credit_limit = set_value('credit_limit');
			$deleted = set_value('deleted');

			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'user_id' => $user_id,
				'title' => $title,
				'title_en' => $title_en,
				'phone' => $phone,
				'mobile' => $mobile,
				'address' => $address,
				'email' => $email,
				'commercial_register' => $commercial_register,
				'tax_card' => $tax_card,
				'person' => $person,
				'person_phone' => $person_phone,
				'credit_limit' => $credit_limit,
				'deleted' => $deleted,
			);
			

			$this->M_buy_manufacturer->UpdateRecord($id, $NewData);
			
			redirect ('purchase/buy_manufacturer');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_buy_manufacturer->UpdateRecord($rid, $NewData);
		$this->M_fin_treeaccount->DeleteRecords($rid, $NewData);
        redirect("purchase/buy_manufacturer");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_buy_manufacturer->UpdateRecord($rid, $NewData);
		$this->M_fin_treeaccount->DeleteRecords($rid, $NewData);
        redirect("purchase/buy_manufacturer");
	}
}
