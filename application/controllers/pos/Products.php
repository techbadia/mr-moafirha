<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products extends CI_Controller
{

    var $data = array();
    public function __construct()
    {
        parent::__construct();
        $this->salt_length = 10;
        $this->hash_method    = 'sha1';
        $this->default_rounds = 8;
        $this->random_rounds  = FALSE;
        $this->min_rounds     = 5;
        $this->max_rounds     = 9;

        $StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0) {
            redirect('login');
        }

        $this->config->load('settings/config_setting');

        if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar")) {
            $this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
            $this->session->set_userdata('Alignment', "right");
            $this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
        } else {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
            $this->session->set_userdata('Alignment', "left");
            $this->session->set_userdata('HTML', "lang='en'");
        }
        $this->load->model("pos/M_Pos");
        $this->load->model("store/M_store_category");
		$this->load->model("store/M_store_brand");
    }
    public function index()
    {
        $data['DataRows'] = $this->M_Pos->get_products();
        $data['content_page'] = "pos/products";
        $this->load->view('page', $data);
    }
}
