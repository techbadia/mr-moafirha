<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pos extends CI_Controller
{

	var $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->salt_length = 10;
		$this->hash_method    = 'sha1';
		$this->default_rounds = 8;
		$this->random_rounds  = FALSE;
		$this->min_rounds     = 5;
		$this->max_rounds     = 9;

		$StaffID = intval($this->session->userdata('StaffID'));
		if ($StaffID == 0) {
			redirect('login');
		}

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar")) {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
			$this->lang->load('mywords', 'arabic');
			$this->session->set_userdata('CSSFile', ".rtl");
			$this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		} else {
			$this->lang->load('mywords', 'english');
			$this->session->set_userdata('CSSFile', "");
			$this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		$this->load->model("pos/M_Pos");
		$this->load->model("store/M_store");
		$this->load->model('sales/M_sale_bill');
		$this->load->model('sales/M_sale_bill_items');
	}

	public function index()
	{
		// if ($this->input->post()) {
		// 	$this->create_order();
		// 	redirect(current_url(), 'refresh');
		// }
		$data['categories'] = $this->M_Pos->categories();
		$data['brandes'] = $this->M_Pos->brandes();

		$this->load->view('pos/pos', $data);
	}
	public function create()
	{

		$store = $this->M_Pos->store();

		$NewData = array(
			'company_id' => $store->company_id,
			'project_id' => 0,
			'user_id' => intval($this->session->userdata('StaffID')),
			'thedate' => date('Y-m-d'),
			'totalvalue' => str_replace(',', '', $this->input->post('payment_amount')),
			'remaining' => 0,
			'discount' => str_replace(',', '', $this->input->post('discount_amount')),
			'discount_type' => $this->input->post('discount_type'),
			// 'tax' => $total_tax,
			'image' => "",
			// 'tree_id' => $tree_id,
			'pos_store_id' => $store->store_id,
			'tree_id1' => 0,
			'paid' => str_replace(',', '', $this->input->post('payment_amount')),
			'paid1' => str_replace(',', '', $this->input->post('payment_amount')),
			'paid2' => 0,
			'paid3' => 0,
			'payment_amount' => str_replace(',', '', $this->input->post('payment_amount')),
			'payment_method' => $this->input->post('payment_method'),
			'payment_card_type' => $this->input->post('payment_card_type'),
			'payment_cheque_number' => $this->input->post('payment_cheque_number'),
			'payment_bank_account_number' => $this->input->post('payment_bank_account_number'),
			'notes' => $this->input->post('payment_note'),
			'sale_note' => $this->input->post('sale_note'),
			'staff_note' => $this->input->post('staff_note'),
			'status' => $this->input->post('status'),
		);
		$bill_id = $this->M_sale_bill->InsertRecord($NewData);
		foreach ($this->input->post('products') as $key => $value) {
			$ItemsArray = array(
				'bill_id' => $bill_id,
				'store_type_id' => $store->store_id,
				'location_id' => $value['product_id'],
				'quantity' => $value['quantity'],
				'unitprice' => $value['unit_price'],
				'line_discount_type' => $value['line_discount_type'],
				'price' => $value['unit_price'] * $value['quantity'],
			);
			
			$this->M_Pos->update_prodcut($value['product_id'],$value['quantity']);
			$this->M_sale_bill_items->InsertRecord($ItemsArray);
		}
		$data['order'] = $this->M_Pos->get_order($bill_id);
		$return =['html_content'=>$this->load->view('pos/print', $data,true),'print_type'=>'printerprinter','success'=>true];
		echo json_encode($return);
	}
	public function get_featured_products()
	{
		# code...
	}
	public function list()
	{
		$products = [];

		foreach ($this->M_Pos->list() as $prod) {
			$products[] = [
				'enable_stock' => true,
				'image_url' => '',
				'name' => $prod->title,
				'product_id' => $prod->product_id,
				'qty_available' => $prod->quantity,
				'selling_price' => $prod->price,
				'sub_sku' => $prod->barcode,
				'type' => '',
				'unit' => '',
				'variation' => '',
				'variation_id' => $prod->product_id,
			];
		}
		echo json_encode($products);
	}
	public function get_product_suggestion()
	{
		$limit = 20;
		if (isset($_GET["page"])) {
			$page  = $_GET["page"];
		} else {
			$page = 1;
		}
		$page_index = ($page - 1) * $limit;
		$products = '';

		foreach ($this->M_Pos->products($limit, $page_index) as $key => $product) {
			$products .= "<div class='col-md-3 col-xs-4 product_list no-print'>
			<div class='product_box' data-variation_id='$product->product_id' title='Product  ($product->product_id) '>
			<div class='image-container' style='background-image: url(http://ultimatepos.2030saudivision.com/uploads/media/1632910115_70295895_translogo.png);
				background-repeat: no-repeat; 
				background-position: center;
				background-size: contain;'>
			</div>
			<div class='text_div'>
				<small class='text text-muted'>$product->prod_title </small>
				<small class='text-muted'>($product->barcode)</small>
			</div>
			</div>	
		</div>";
		}

		echo $products;
	}
	public function get_product_row($id)
	{
		$data = ['success' => false];
		if ($prod = $this->M_Pos->get_prod_id($id)) {
			$data['html_content'] = "<tr class='product_row' data-row_index='$id'>
			<td>
				<div title='تعديل السعر'>
					<span class='text-link text-info cursor-pointer' data-toggle='modal' data-target='#row_edit_product_price_modal_$id'>
					$prod->prod_title <br/>$prod->barcode &nbsp;
						<i class='fa fa-info-circle'></i></span>
				</div>
				<input type='hidden' class='enable_sr_no' value='$id'>
				<input type='hidden' class='product_type' name='products[$id][product_type]' value='single'>
				<div class='modal fade row_edit_product_price_model' id='row_edit_product_price_modal_$id' tabindex='-1' role='dialog'>
					<div class='modal-dialog' role='document'>
						<div class='modal-content'>
							<div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
								<h4 class='modal-title' id='myModalLabel'>$prod->prod_title - $prod->barcode</h4>
							</div>
							<div class='modal-body'>
								<div class='row'>
									<div class='form-group col-xs-12 '><label>سعر الوحدة</label><input type='text' name='products[$id][unit_price]' class='form-control pos_unit_price input_number mousetrap' value='$prod->price'></div>
									<div class='form-group col-xs-12 col-sm-6 '><label>نوع الخصم</label><select class='form-control row_discount_type' name='products[$id][line_discount_type]'>
											<option value='fixed' selected='selected'>خصك ثابت</option>
											<option value='percentage'>نسبة المئوية</option>
										</select></div>
									<div class='form-group col-xs-12 col-sm-6 '><label>مبلغ الخصم</label><input class='form-control input_number row_discount_amount' name='products[$id][line_discount_amount]' type='text' value='0.00'></div>
									<div class='form-group col-xs-12'> <label>الوصف</label> <textarea class='form-control' name='products[$id][sell_line_note]' rows='3'></textarea>
										<p class='help-block'>أضف رقم IMEI للمنتج أو الرقم التسلسلي أو أي معلومات أخرى هنا										.</p>
									</div>
								</div>
							</div>
							<div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Close</button></div>
						</div>
					</div>
				</div><!-- Description modal end -->
			</td>
			<td><input type='hidden' name='products[$id][product_id]' class='form-control product_id' value='$id'>
				<div class='input-group input-number'>
			
				<input type='text' data-min='1' class='form-control pos_quantity input_number mousetrap input_quantity'  data-rule-min='1' data-rule-max='$prod->quantity' value='1' name='products[$id][quantity]' data-allow-overselling='false' data-decimal=0 data-rule-abs_digit='true' data-msg-abs_digit='Decimal value not allowed' data-rule-required='true' data-msg-required='This field is required'>
				<span class='input-group-btn'>
				<button type='button' class='btn btn-default btn-flat quantity-up'>
				<i class='fa fa-plus text-success'></i></button></span></div>
				<input type='hidden' name='products[$id][product_unit_id]'  value='1'>
				<br>
			</td>
			<td class='hide'><input type='text' name='products[$id][unit_price_inc_tax]' class='form-control pos_unit_price_inc_tax input_number' value='$prod->price'></td>
			<td class='text-center'><input type='hidden' class='form-control pos_line_total ' value='$prod->price'><span class='display_currency pos_line_total_text ' data-currency_symbol='true'>2600</span></td>
			<td class='text-center v-center'><i class='fa fa-times text-danger pos_remove_row cursor-pointer' aria-hidden='true'></i></td>
		</tr>";
			$data['success'] = true;
		}
		echo	json_encode($data);
	}
}
