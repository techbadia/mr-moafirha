<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fin_treeaccount extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();

		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }
	}

	public function index()
	{
		// $this->db->distinct();
		// $this->db->select('company_id');
		// $query = $this->db->get('fin_treeaccount');
		// // var_dump($query->num_rows());die();
		// if($query->num_rows() > 1)
		// {
		// 	$this->RenameAccounts();
		// }
		$data['DataRows'] = $this->M_fin_treeaccount->GetMultiRow(false);
		$data['content_page'] = "account/fin_treeaccount";
		$this->load->view('page', $data);
	}
	private function RenameAccounts()
	{
		$data['DataRows'] = $this->M_fin_treeaccount->GetMultiRow(false);
		foreach ($data['DataRows'] as $account) {
			$company = $this->M_app_company->GetRow($account->company_id);
			$dataArr['title'] = $account->title." - ".$company->ar_title;
			$dataArr['title_en'] =  $account->title_en." - ".$company->en_title;
			$this->db->where('id',$account->id);
			$this->db->update('fin_treeaccount', $dataArr);
		}
	}
	public function tree()
	{
		$this->session->set_userdata('Filter', "");
		$data['DataRows'] = $this->M_fin_treeaccount->GetMultiRow();
		$data['content_page'] = "account/fin_treeaccount_treeview";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$this->session->set_userdata('Filter', "");
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "account/fin_treeaccount_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		$this->session->set_userdata('Filter', "");
		//brand
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("title_en","title_en","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
					$adQu = set_value('adQu');

			$company_id = set_value("company_id");
			$category_id = set_value("category_id");
			$parent_id = set_value('parent_id');
			$account_no = set_value('account_no');
			$title = set_value('title');
			$title_en = set_value('title_en');
			$startamount = set_value('startamount');
			$basic = set_value('basic');
			$level_no = set_value('level_no');
			$deleted = set_value('deleted');

			$account_no_exists = $this->M_fin_treeaccount->GetByAccountNo($account_no);
			if($account_no_exists){
				echo 'رقم الحساب مستخدم من قبل';
			}else{

				$NewData = array();
				$NewData = array(
					'company_id' => $company_id,
					'category_id' => $category_id,
					'parent_id' => $parent_id,
					'account_no' => $account_no,
					'title' => $title,
					'title_en' => $title_en,
					'startamount' => $startamount,
					'basic' => $basic,
					'level_no' => $level_no,
					'deleted' => $deleted,
				);

				$this->M_fin_treeaccount->InsertRecord($NewData);
				if($adQu == 1)redirect ('account/fin_treeaccount/tree');
				else
				redirect ('account/fin_treeaccount');
			}

		}
	}


	public function updateform($rid)
	{
		$this->session->set_userdata('Filter', "");
		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "account/fin_treeaccount_update";
        $this->load->view('page', $data);

	}

	public function Update_Data()
	{
		$this->session->set_userdata('Filter', "");
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("title_en","title_en","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = set_value("company_id");
			$category_id = set_value("category_id");
			$parent_id = set_value('parent_id');
			$account_no = set_value('account_no');
			$title = set_value('title');
			$title_en = set_value('title_en');
			$startamount = set_value('startamount');
			$basic = set_value('basic');
			$level_no = set_value('level_no');
			$deleted = set_value('deleted');
			$account_no_exists = $this->M_fin_treeaccount->GetByAccountNo($account_no);
			$old_data = $this->M_fin_treeaccount->GetRow($id);
			if($account_no_exists && $old_data->account_no != $account_no){
				echo 'رقم الحساب مستخدم من قبل';
			}else{
				$NewData = array();
				$NewData = array(
					'company_id' => $company_id,
					'category_id' => $category_id,
					'parent_id' => $parent_id,
					'account_no' => $account_no,
					'title' => $title,
					'title_en' => $title_en,
					'startamount' => $startamount,
					'basic' => $basic,
					'level_no' => $level_no,
					'deleted' => $deleted,
				);


				$this->M_fin_treeaccount->UpdateRecord($id, $NewData);

				redirect ('account/fin_treeaccount');
			}

		}
	}

	public function delete($rid, $forever = '')
	{
		if($forever == 'forever'){
			$this->M_fin_treeaccount->DeleteForever($rid);
		}else{
			$deleted = 1;
			$NewData = array();
			$NewData = array(
				'deleted' => $deleted,
			);

			$this->M_fin_treeaccount->UpdateRecord($rid, $NewData);
		}



        redirect ('account/fin_treeaccount');
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_fin_treeaccount->UpdateRecord($rid, $NewData);

        redirect ('account/fin_treeaccount');
	}

	public function view($accountid)
    {
		$this->session->set_userdata('Filter', "");
        $Page = "account/report/account_balance_view/".$accountid;
        redirect ($Page);
	}

	public function view_account($rid)
	{
		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['deleted'] = $DataRow->deleted;

		$account_id = $rid;
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();

		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/fin_treeaccount_report";
        $this->load->view('page', $data);
	}

	public function search_account()
	{
		$this->form_validation->set_rules("fromdate","fromdate","required");
		$this->form_validation->set_rules("todate","todate","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$rid = set_value('accountid');
			$Search = set_value("Search");
			$fromdate = set_value("fromdate");
			$todate = set_value('todate');

			$DataRow = $this->M_fin_treeaccount->GetRow($rid);
			$data['id'] = $DataRow->id;
			$data['company_id'] = $DataRow->company_id;
			$data['category_id'] = $DataRow->category_id;
			$data['parent_id'] = $DataRow->parent_id;
			$data['account_no'] = $DataRow->account_no;
			$data['title'] = $DataRow->title;
			$data['title_en'] = $DataRow->title_en;
			$data['startamount'] = $DataRow->startamount;
			$data['basic'] = $DataRow->basic;
			$data['level_no'] = $DataRow->level_no;
			$data['deleted'] = $DataRow->deleted;

			$account_id = $rid;
			$data['accountid'] = $rid;
			$data['Search'] = 1;
			$data['fromdate'] = $fromdate;
			$data['todate'] = $todate;
			$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

			$data['ControllerName'] = $this->router->fetch_class();

			///$this->load->view('account/fin_treeaccount_report', $data);
			$data['content_page'] = "account/fin_treeaccount_report";
        	$this->load->view('page', $data);
		}
	}

	public function deleted()
	{
		$this->session->set_userdata('Filter', lang('Deleted'));
		$data['DataRows'] = $this->M_fin_treeaccount->GetDeleted();
		$data['content_page'] = "account/fin_treeaccount";
		$this->load->view('page', $data);
	}

	public function category($category_id=1)
	{
		$Lang_Word = "fin_treecategory_".$category_id;
		$this->session->set_userdata('Filter', lang($Lang_Word));

		$data['DataRows'] = $this->M_fin_treeaccount->GetByCategory($category_id);
		$data['content_page'] = "account/fin_treeaccount";
		$this->load->view('page', $data);
	}
}
