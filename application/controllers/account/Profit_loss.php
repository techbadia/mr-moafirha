<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profit_loss extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }
		$this->load->model('settings/M_fin_settings');
		$this->load->model('app/M_app_company');
	}  
	
	public function index()
	{
		$data['DataRows'] = $this->M_fin_treeaccount->GetMultiRow();
		$this->Create_PDF();
	}
	
	public function Create_PDF()
	{
		$Expenses = $this->M_fin_treeaccount->GetByCategory(5);
		$Revenue = $this->M_fin_treeaccount->GetByCategory(4);

		$company_id = intval($this->session->userdata('company_id'));
		$DataRow = $this->M_app_company->GetRow($company_id);
		$this->session->set_userdata('header', $DataRow->header);
		$this->session->set_userdata('footer', $DataRow->footer);
		
		$HeaderImage = base_url()."upload/settings/".$this->session->userdata('header');
		$FooterImage = base_url()."upload/settings/".$this->session->userdata('footer');
		$Directionality = $this->session->userdata('Direction');
		$PageTitle = lang('profit_loss');


		require_once 'vendor/autoload.php';
		$mpdf = new \Mpdf\Mpdf([
			'mode' => 'utf-8',
			//'format' => 'A4'.($orientation == 'L' ? '-L' : ''),
			//'orientation' => $orientation,
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 45,
			'margin_bottom' => 55,
			'margin_header' => 5,
			'margin_footer' => 5,
			]);
		$mpdf->allow_charset_conversion=true;
		//$mpdf->charset_in='cp1252';
		$mpdf->SetTitle($PageTitle);
		$mpdf->SetDirectionality($Directionality);
		$mpdf->autoScriptToLang = true;
		$mpdf->autoLangToFont = true;
		$mpdf->debug = true;
		//$mpdf->defaultPageNumStyle = 'arabic-indic';
		
		ob_end_clean();
		header("Content-Encoding: None", true);

		$mpdf->SetHTMLHeader('
		<div style="text-align: center; font-weight: bold;">
			<img src="'.$HeaderImage.'">
		</div>');

		$mpdf->SetHTMLFooter('
		<table width="100%" style="vertical-align: bottom; font-family: serif; 
			font-size: 8pt; color: #000000; font-weight: bold;">
			<tr>
				<td colspan="2"><img src="'.$FooterImage.'"></td>
			</tr>
			<tr>
				<td width="50%">{DATE j-m-Y}</td>
				<td width="50%" align="left" lang="en">{PAGENO}/{nbpg}</td>
			</tr>
		</table>');

		$html = '';
		$html .= '<div align="center" style="font-size:18px; margin-top: 50px;"><strong>'.$PageTitle.'</strong></div><br>';
		$html .= '<table cellspacing="0" cellpadding="5" border="0" width="98%" dir="rtl">';
		$html .= '<tr>';
		$html .= '<td width="50%" valign="top">';

		$html .= '<table cellspacing="0" cellpadding="5" border="1" width="98%" dir="rtl">';
		$html .= '<tr>';
		$html .= '<td colspan="2">'.lang('Expenses').'</td>';
		$html .= '</tr>';
		$TotalExpenses = 0;
		$ExpensesValue = 0;
		foreach($Expenses as $Expenses_Row) {
			$ExpensesValue = doubleval($Expenses_Row->startamount);
			$account_id = $Expenses_Row->id;
			$CheckJournal = $this->M_fin_journal->GetSum($account_id);
			if($CheckJournal > 0)
			{
				$TotalExpenses += doubleval($Expenses_Row->startamount);
				$Creditor = $this->M_fin_journal->GetSum_Creditor($account_id);
				$TotalExpenses += doubleval($Creditor->creditor);

				$ExpensesValue += doubleval($Creditor->creditor);
			}
			else
			{
				$TotalExpenses += doubleval($Expenses_Row->startamount);
			}
			$html .= '<tr>';
			$html .= '<td>'.$ExpensesValue.'</td>';
			if ($this->session->userdata('lang') == "ar")
			{
				$ExpensesTitle = $Expenses_Row->title;
			}
			else {
				$ExpensesTitle = $Expenses_Row->title_en;
			}
			$html .= '<td>'.$ExpensesTitle.'</td>';
			$html .= '</tr>';
		}
		$html .= '<tr>';
		$html .= '<td>'.$TotalExpenses.'</td>';
		$html .= '<td>'.lang('Total').'</td>';
		$html .= '</tr>';
		$html .= '</table>';

		$html .= '</td>';
		$html .= '<td width="50%" valign="top">';
		
		$html .= '<table cellspacing="0" cellpadding="5" border="1" width="98%" dir="rtl">';
		$html .= '<tr>';
		$html .= '<td colspan="2">'.lang('Revenue').'</td>';
		$html .= '</tr>';
		$TotalRevenue = 0;
		$RevenueValue = 0;
		foreach($Revenue as $Revenue_Row) {
			$RevenueValue = doubleval($Revenue_Row->startamount);
			$account_id = $Revenue_Row->id;
			$CheckJournal = $this->M_fin_journal->GetSum($account_id);
			if($CheckJournal > 0)
			{
				$TotalRevenue += doubleval($Revenue_Row->startamount);
				$Debit = $this->M_fin_journal->GetSum_Debit($account_id);
				$TotalRevenue += doubleval($Debit->debit);

				$RevenueValue += doubleval($Debit->debit);
			}
			else
			{
				$TotalRevenue += doubleval($Revenue_Row->startamount);
			}
			$html .= '<tr>';
			$html .= '<td>'.$RevenueValue.'</td>';
			if ($this->session->userdata('lang') == "ar")
			{
				$RevenueTitle = $Revenue_Row->title;
			}
			else {
				$RevenueTitle = $Revenue_Row->title_en;
			}
			$html .= '<td>'.$RevenueTitle.'</td>';
			$html .= '</tr>';
		}
		$html .= '<tr>';
		$html .= '<td>'.$TotalRevenue.'</td>';
		$html .= '<td>'.lang('Total').'</td>';
		$html .= '</tr>';
		$html .= '</table>';

		$html .= '</td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '<br>';
		$html .= '<div style="text-align:center">'.lang('proj_project_Net')." : ";
		$NetValue = $TotalRevenue - $TotalExpenses;
		$html .= $NetValue;
		$html .= '</div>';

		$html = iconv("utf-8","UTF-8//IGNORE",$html);
		$mpdf->WriteHTML($html);
		
		$mpdf->Output();
		exit;
	}
}
