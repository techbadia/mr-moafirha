<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            
            
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }
	}
	
	public function index()
	{
		$data['DataRows'] = $this->M_fin_treeaccount->GetMultiRow();
		$data['content_page'] = "account/fin_treeaccount";
		$this->load->view('page', $data);
	}
	
	public function account_1()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Account_1'));
		$rid = intval($this->session->userdata('acc_treasury'));

		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['deleted'] = $DataRow->deleted;

		$account_id = $rid;
		$data['main_accountid'] = $rid;
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();
		
		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/fin_treeaccount_report_advanced";
        $this->load->view('page', $data);
	}

	public function account_2()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Account_2'));
		$rid = intval($this->session->userdata('acc_bank'));

		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$DataRow = $this->M_fin_treeaccount->GetRowSon($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['deleted'] = $DataRow->deleted;

		$account_id = $rid;
		$data['main_accountid'] = $rid;
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();
		
		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/fin_treeaccount_report_advanced";
        $this->load->view('page', $data);
	}

	public function account_3()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Account_3'));
		$rid = intval($this->session->userdata('acc_expenses'));

		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['deleted'] = $DataRow->deleted;

		$account_id = $rid;
		$data['main_accountid'] = $rid;
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();
		
		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/fin_treeaccount_report_advanced";
        $this->load->view('page', $data);
	}

	public function account_4()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Account_4'));
		$rid = intval($this->session->userdata('acc_debtors'));

		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['deleted'] = $DataRow->deleted;

		$account_id = $rid;
		$data['main_accountid'] = $rid;
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();
		
		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/fin_treeaccount_report_advanced";
        $this->load->view('page', $data);
	}

	public function account_5()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Account_5'));
		$rid = intval($this->session->userdata('acc_creditors'));

		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['deleted'] = $DataRow->deleted;

		$account_id = $rid;
		$data['main_accountid'] = $rid;
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();
		
		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/fin_treeaccount_report_advanced";
        $this->load->view('page', $data);
	}

	public function account_6()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Account_6'));
		$rid = intval($this->session->userdata('acc_capture_papers'));

		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['deleted'] = $DataRow->deleted;

		$account_id = $rid;
		$data['main_accountid'] = $rid;
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();
		
		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/fin_treeaccount_report_advanced";
        $this->load->view('page', $data);
	}

	public function account_7()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Account_7'));
		$rid = intval($this->session->userdata('acc_payment_papers'));

		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['deleted'] = $DataRow->deleted;

		$account_id = $rid;
		$data['main_accountid'] = $rid;
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();
		
		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/fin_treeaccount_report_advanced";
        $this->load->view('page', $data);
	}

	public function account_8()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Account_8'));
		$rid = intval($this->session->userdata('acc_custody'));

		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['deleted'] = $DataRow->deleted;

		$account_id = $rid;
		$data['main_accountid'] = $rid;
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();
		
		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/fin_treeaccount_report_advanced";
        $this->load->view('page', $data);
	}

	public function account_9()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Account_9'));
		$rid = intval($this->session->userdata('acc_tax_sale'));

		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['deleted'] = $DataRow->deleted;

		$account_id = $rid;
		$data['main_accountid'] = $rid;
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();
		
		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/fin_treeaccount_report_advanced";
        $this->load->view('page', $data);
	}

	public function account_10()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Account_10'));
		$rid = intval($this->session->userdata('acc_tax_purchase'));

		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['deleted'] = $DataRow->deleted;

		$account_id = $rid;
		$data['main_accountid'] = $rid;
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();
		
		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/fin_treeaccount_report_advanced";
        $this->load->view('page', $data);
	}

		public function search_account()
	{
		$this->form_validation->set_rules("fromdate","fromdate","required");
		$this->form_validation->set_rules("todate","todate","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
		    
		    
			
			$rid = set_value('main_accountid');

			$accountid = set_value('accountid');
			$Search = set_value("Search");
			$fromdate = set_value("fromdate");
			$todate = set_value('todate');

			$DataRow = $this->M_fin_treeaccount->GetRow($accountid);
			$data['id'] = $DataRow->id;
			$data['company_id'] = $DataRow->company_id;
			$data['category_id'] = $DataRow->category_id;
			$data['parent_id'] = $DataRow->parent_id;
			$data['account_no'] = $DataRow->account_no;
			$data['title'] = $DataRow->title;
			$data['title_en'] = $DataRow->title_en;
			$data['startamount'] = $DataRow->startamount;
			$data['basic'] = $DataRow->basic;
			$data['level_no'] = $DataRow->level_no;
			$data['deleted'] = $DataRow->deleted;

			$account_id = $accountid;
			$data['main_accountid'] = $rid;
			$data['accountid'] = $accountid;
			$data['Search'] = 1;
			$data['fromdate'] = $fromdate;
			$data['todate'] = $todate;
			$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

			$data['ControllerName'] = $this->router->fetch_class();
			
			///$this->load->view('account/fin_treeaccount_report', $data);
			$data['content_page'] = "account/fin_treeaccount_report_advanced";
        	$this->load->view('page', $data);
		}
	}

	public function purchase_1()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Purchase_1'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Purchase_1');

		$this->load->model("purchase/M_buy_bill");
		$this->load->model("purchase/M_buy_bill_items");
		$this->load->model("purchase/M_buy_manufacturer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");

		$data['fromdate'] = date('Y-m-d');
		$fromdate = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$todate = date('Y-m-d');
		$data['supplier_id'] = 0;
		$supplier_id = 0;

		$data['DataRows'] = $this->M_buy_bill->GetReport($supplier_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/purchase_1";
		$this->load->view('page', $data);
	}

	public function purchase_1_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Purchase_1'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Purchase_1');

		$this->load->model("purchase/M_buy_bill");
		$this->load->model("purchase/M_buy_bill_items");
		$this->load->model("purchase/M_buy_manufacturer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");

		$fromdate = set_value("fromdate");
		$todate = set_value('todate');
		$supplier_id = set_value('supplier_id');

		$data['fromdate'] = $fromdate;
		$data['todate'] = $todate;
		$data['supplier_id'] = $supplier_id;
		

		$data['DataRows'] = $this->M_buy_bill->GetReport($supplier_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/purchase_1";
		$this->load->view('page', $data);
	}

	public function purchase_2()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Purchase_2'));
		$rid = intval($this->session->userdata('acc_supplier'));

		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['deleted'] = $DataRow->deleted;

		$account_id = $rid;
		$data['main_accountid'] = $rid;
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();
		
		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/fin_treeaccount_report_advanced";
        $this->load->view('page', $data);
	}

	public function purchase_3()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Purchase_3'));

		$this->load->model("purchase/M_buy_manufacturer");
		$this->load->model("account/M_fin_treeaccount");

		$rid = intval($this->session->userdata('acc_supplier'));

		$data['DataRows'] = $this->M_buy_manufacturer->GetMultiRow();

		$account_id = $rid;
		$data['title'] = $this->lang->line('ShortCut_Reports_Purchase_3');
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = "2021-01-01";
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();
		
		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/reports/purchase_3";
        $this->load->view('page', $data);
	}

	public function purchase_4()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Purchase_4'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Purchase_4');

		$this->load->model("purchase/M_buy_returns");
		$this->load->model("purchase/M_buy_returns_items");
		$this->load->model("purchase/M_buy_manufacturer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");

		$data['fromdate'] = date('Y-m-d');
		$fromdate = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$todate = date('Y-m-d');
		$data['supplier_id'] = 0;
		$supplier_id = 0;

		$data['DataRows'] = $this->M_buy_returns->GetReport($supplier_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/purchase_4";
		$this->load->view('page', $data);
	}

	public function purchase_4_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Purchase_4'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Purchase_4');

		$this->load->model("purchase/M_buy_returns");
		$this->load->model("purchase/M_buy_returns_items");
		$this->load->model("purchase/M_buy_manufacturer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");

		$fromdate = set_value("fromdate");
		$todate = set_value('todate');
		$supplier_id = set_value('supplier_id');

		$data['fromdate'] = $fromdate;
		$data['todate'] = $todate;
		$data['supplier_id'] = $supplier_id;
		

		$data['DataRows'] = $this->M_buy_returns->GetReport($supplier_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/purchase_4";
		$this->load->view('page', $data);
	}

	public function purchase_5()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Purchase_5'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Purchase_5');

		$this->load->model("purchase/M_buy_bill");
		$this->load->model("purchase/M_buy_bill_items");
		$this->load->model("purchase/M_buy_manufacturer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");

		$data['fromdate'] = date('Y-m-d');
		$fromdate = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$todate = date('Y-m-d');
		$data['supplier_id'] = 0;
		$supplier_id = 0;

		$data['DataRows'] = $this->M_buy_bill->GetReport($supplier_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/purchase_5";
		$this->load->view('page', $data);
	}

	public function purchase_5_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Purchase_5'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Purchase_5');

		$this->load->model("purchase/M_buy_bill");
		$this->load->model("purchase/M_buy_bill_items");
		$this->load->model("purchase/M_buy_manufacturer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");

		$fromdate = set_value("fromdate");
		$todate = set_value('todate');
		$supplier_id = set_value('supplier_id');

		$data['fromdate'] = $fromdate;
		$data['todate'] = $todate;
		$data['supplier_id'] = $supplier_id;
		

		$data['DataRows'] = $this->M_buy_bill->GetReport($supplier_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/purchase_5";
		$this->load->view('page', $data);
	}

	public function purchase_7()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Purchase_7'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Purchase_7');

		$this->load->model("purchase/M_buy_bill");
		$this->load->model("purchase/M_buy_manufacturer");
		$this->load->model("store/M_product");

		$data['fromdate'] = date('Y-m-d');
		$fromdate = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$todate = date('Y-m-d');
		$data['supplier_id'] = 0;
		$supplier_id = 0;
		$data['product_id'] = 0;
		$product_id = 0;

		$data['DataRows'] = $this->M_buy_bill->GetItemsPrice($product_id, $supplier_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/purchase_7";
		$this->load->view('page', $data);
	}

	public function purchase_7_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Purchase_7'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Purchase_7');

		$this->load->model("purchase/M_buy_bill");
		$this->load->model("purchase/M_buy_manufacturer");
		$this->load->model("store/M_product");

		$fromdate = set_value("fromdate");
		$todate = set_value('todate');
		$supplier_id = set_value('supplier_id');
		$product_id = set_value('product_id');

		$data['fromdate'] = $fromdate;
		$data['todate'] = $todate;
		$data['supplier_id'] = $supplier_id;
		$data['product_id'] = $product_id;

		$data['DataRows'] = $this->M_buy_bill->GetItemsPrice($product_id, $supplier_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/purchase_7";
		$this->load->view('page', $data);
	}

	public function sales_1()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Sales_1'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Sales_1');

		$this->load->model("sales/M_sale_bill");
		$this->load->model("sales/M_sale_bill_items");
		$this->load->model("sales/M_sale_customer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");

		$data['fromdate'] = date('Y-m-d');
		$fromdate = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$todate = date('Y-m-d');
		$data['customer_id'] = 0;
		$customer_id = 0;

		$data['DataRows'] = $this->M_sale_bill->GetReport($customer_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/sales_1";
		$this->load->view('page', $data);
	}

	public function sales_1_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Sales_1'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Sales_1');

		$this->load->model("sales/M_sale_bill");
		$this->load->model("sales/M_sale_bill_items");
		$this->load->model("sales/M_sale_customer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");

		$fromdate = set_value("fromdate");
		$todate = set_value('todate');
		$customer_id = set_value('customer_id');

		$data['fromdate'] = $fromdate;
		$data['todate'] = $todate;
		$data['customer_id'] = $customer_id;
		

		$data['DataRows'] = $this->M_sale_bill->GetReport($customer_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/sales_1";
		$this->load->view('page', $data);
	}

	public function sales_2()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Sales_2'));
		$rid = intval($this->session->userdata('acc_customer'));

		$DataRow = $this->M_fin_treeaccount->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['parent_id'] = $DataRow->parent_id;
		$data['account_no'] = $DataRow->account_no;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['startamount'] = $DataRow->startamount;
		$data['basic'] = $DataRow->basic;
		$data['level_no'] = $DataRow->level_no;
		$data['deleted'] = $DataRow->deleted;

		$account_id = $rid;
		$data['main_accountid'] = $rid;
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();
		
		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/fin_treeaccount_report_advanced";
        $this->load->view('page', $data);
	}

	public function sales_3()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Sales_3'));

		$this->load->model("sales/M_sale_customer");
		$this->load->model("account/M_fin_treeaccount");

		$rid = intval($this->session->userdata('acc_supplier'));

		$data['DataRows'] = $this->M_sale_customer->GetMultiRow();

		$account_id = $rid;
		$data['title'] = $this->lang->line('ShortCut_Reports_Sales_3');
		$data['accountid'] = $rid;
		$data['Search'] = 0;
		$data['fromdate'] = "2021-01-01";
		$data['todate'] = date('Y-m-d');
		$data['Journal'] = $this->M_fin_journal->GetAccountActions($account_id);

		$data['ControllerName'] = $this->router->fetch_class();
		
		//$this->load->view('account/fin_treeaccount_report', $data);
		$data['content_page'] = "account/reports/sales_3";
        $this->load->view('page', $data);
	}

	public function sales_4()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Sales_4'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Sales_4');

		$this->load->model("sales/M_sale_returns");
		$this->load->model("sales/M_sale_returns_items");
		$this->load->model("sales/M_sale_customer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");

		$data['fromdate'] = date('Y-m-d');
		$fromdate = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$todate = date('Y-m-d');
		$data['customer_id'] = 0;
		$customer_id = 0;

		$data['DataRows'] = $this->M_sale_returns->GetReport($customer_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/sales_4";
		$this->load->view('page', $data);
	}

	public function sales_4_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Sales_4'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Sales_4');

		$this->load->model("sales/M_sale_returns");
		$this->load->model("sales/M_sale_returns_items");
		$this->load->model("sales/M_sale_customer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");

		$fromdate = set_value("fromdate");
		$todate = set_value('todate');
		$customer_id = set_value('customer_id');

		$data['fromdate'] = $fromdate;
		$data['todate'] = $todate;
		$data['customer_id'] = $customer_id;
		

		$data['DataRows'] = $this->M_sale_returns->GetReport($customer_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/sales_4";
		$this->load->view('page', $data);
	}

	public function sales_5()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Sales_5'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Sales_5');

		$this->load->model("sales/M_sale_bill");
		$this->load->model("sales/M_sale_bill_items");
		$this->load->model("sales/M_sale_customer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");

		$data['fromdate'] = date('Y-m-d');
		$fromdate = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$todate = date('Y-m-d');
		$data['user_id'] = 0;
		$user_id = 0;

		$data['DataRows'] = $this->M_sale_bill->GetByUser($user_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/sales_5";
		$this->load->view('page', $data);
	}

	public function sales_5_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Sales_5'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Sales_5');

		$this->load->model("sales/M_sale_bill");
		$this->load->model("sales/M_sale_bill_items");
		$this->load->model("sales/M_sale_customer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");

		$data['fromdate'] = set_value("fromdate");
		$fromdate = set_value("fromdate");
		$data['todate'] = set_value("todate");
		$todate = set_value("todate");
		$data['user_id'] = set_value("user_id");
		$user_id = set_value("user_id");

		$data['DataRows'] = $this->M_sale_bill->GetByUser($user_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/sales_5";
		$this->load->view('page', $data);
	}

	public function sales_6()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Sales_6'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Sales_6');

		$this->load->model("sales/M_sale_bill");
		$this->load->model("sales/M_sale_bill_items");
		$this->load->model("sales/M_sale_customer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");

		$data['fromdate'] = date('Y-m-d');
		$fromdate = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$todate = date('Y-m-d');
		$data['salesperson_id'] = 0;
		$salesperson_id = 0;

		$data['DataRows'] = $this->M_sale_bill->GetBySalesPerson($salesperson_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/sales_6";
		$this->load->view('page', $data);
	}

	public function sales_6_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Sales_6'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Sales_6');

		$this->load->model("sales/M_sale_bill");
		$this->load->model("sales/M_sale_bill_items");
		$this->load->model("sales/M_sale_customer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");

		$data['fromdate'] = set_value("fromdate");
		$fromdate = set_value("fromdate");
		$data['todate'] = set_value("todate");
		$todate = set_value("todate");
		$data['salesperson_id'] = set_value("salesperson_id");
		$salesperson_id = set_value("salesperson_id");

		$data['DataRows'] = $this->M_sale_bill->GetBySalesPerson($salesperson_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/sales_6";
		$this->load->view('page', $data);
	}

	public function sales_7()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Sales_7'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Sales_7');

		$this->load->model("sales/M_sale_bill");
		$this->load->model("sales/M_sale_customer");
		$this->load->model("store/M_product");

		$data['fromdate'] = date('Y-m-d');
		$fromdate = date('Y-m-d');
		$data['todate'] = date('Y-m-d');
		$todate = date('Y-m-d');
		$data['customer_id'] = 0;
		$customer_id = 0;
		$data['product_id'] = 0;
		$product_id = 0;

		$data['DataRows'] = $this->M_sale_bill->GetItemsPrice($product_id, $customer_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/sales_7";
		$this->load->view('page', $data);
	}

	public function sales_7_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Sales_7'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Sales_7');

		$this->load->model("sales/M_sale_bill");
		$this->load->model("sales/M_sale_customer");
		$this->load->model("store/M_product");

		$fromdate = set_value("fromdate");
		$todate = set_value('todate');
		$customer_id = set_value('customer_id');
		$product_id = set_value('product_id');

		$data['fromdate'] = $fromdate;
		$data['todate'] = $todate;
		$data['customer_id'] = $customer_id;
		$data['product_id'] = $product_id;

		$data['DataRows'] = $this->M_sale_bill->GetItemsPrice($product_id, $customer_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/sales_7";
		$this->load->view('page', $data);
	}

	public function sales_10()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Sales_10'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Sales_10');

		$this->load->model("store/M_product");

		$data['DataRows'] = $this->M_product->GetTopSale_Quantity();
		$data['content_page'] = "account/reports/sales_10";
		$this->load->view('page', $data);
	}

	public function sales_11()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Sales_11'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Sales_11');

		$this->load->model("store/M_product");

		$data['DataRows'] = $this->M_product->GetTopSale_Price();
		$data['content_page'] = "account/reports/sales_11";
		$this->load->view('page', $data);
	}

	public function store_1()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Store_1'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Store_1');

		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');

		$this->load->model("store/M_store_category");
		$this->load->model("store/M_store_brand");
		$this->load->model("store/M_product");
		$this->load->model("store/M_store_quantity");
		$this->load->model("store/M_store");
		$this->load->model("purchase/M_buy_bill_items");
		$this->load->model("purchase/M_buy_returns_items");
		$this->load->model("sales/M_sale_bill_items");
		$this->load->model("sales/M_sale_returns_items");

		$data['DataRows'] = $this->M_product->GetMultiRow();
		$data['content_page'] = "account/reports/store_1";
		$this->load->view('page', $data);
	}

	public function store_1_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Store_1'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Store_1');

		$data['fromdate'] = set_value('fromdate');
		$data['todate'] = set_value('todate');
		
		$this->load->model("store/M_store_category");
		$this->load->model("store/M_store_brand");
		$this->load->model("store/M_product");
		$this->load->model("store/M_store_quantity");
		$this->load->model("store/M_store");
		$this->load->model("purchase/M_buy_bill_items");
		$this->load->model("purchase/M_buy_returns_items");
		$this->load->model("sales/M_sale_bill_items");
		$this->load->model("sales/M_sale_returns_items");

		$data['DataRows'] = $this->M_product->GetMultiRow();
		$data['content_page'] = "account/reports/store_1";
		$this->load->view('page', $data);
	}

	public function store_2()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Store_2'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Store_2');

		$this->load->model("store/M_store_category");
		$this->load->model("store/M_store_brand");
		$this->load->model("store/M_product");
		$this->load->model("store/M_store_quantity");
		$this->load->model("store/M_store");

		$data['DataRows'] = $this->M_product->GetMultiRow();
		$data['content_page'] = "account/reports/store_2";
		$this->load->view('page', $data);
	}

	public function store_3()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Store_3'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Store_3');

		$this->load->model("store/M_store_category");
		$this->load->model("store/M_store_brand");
		$this->load->model("store/M_product");
		$this->load->model("store/M_store_quantity");
		$this->load->model("store/M_store");

		$data['store_id'] = 0;

		$data['DataRows'] = $this->M_product->GetMultiRow();
		$data['content_page'] = "account/reports/store_3";
		$this->load->view('page', $data);
	}

	public function store_3_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Store_3'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Store_3');

		$this->load->model("store/M_store_category");
		$this->load->model("store/M_store_brand");
		$this->load->model("store/M_product");
		$this->load->model("store/M_store_quantity");
		$this->load->model("store/M_store");

		$data['store_id'] = set_value('store_id');

		$data['DataRows'] = $this->M_product->GetMultiRow();
		$data['content_page'] = "account/reports/store_3";
		$this->load->view('page', $data);
	}

	public function store_4()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Store_4'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Store_4');

		$this->load->model("store/M_store_category");
		$this->load->model("store/M_store_brand");
		$this->load->model("store/M_product");
		$this->load->model("store/M_store_quantity");

		$data['DataRows'] = $this->M_store_quantity->GetQuantityLessZero();
		$data['content_page'] = "account/reports/store_4";
		$this->load->view('page', $data);
	}

	public function store_5()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Store_5'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Store_5');

		$this->load->model("store/M_store");
		$this->load->model("store/M_product");
		$this->load->model("store/M_store_move");
		$this->load->model("store/M_store_quantity");

		$data['DataRows'] = $this->M_store_quantity->GetQuantityLessZero();
		$data['content_page'] = "account/reports/store_5";
		$this->load->view('page', $data);
	}

	public function store_6()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_Store_6'));
		$data['title'] = $this->lang->line('ShortCut_Reports_Store_6');

		$this->load->model("store/M_store_category");
		$this->load->model("store/M_store_brand");
		$this->load->model("store/M_product");
		$this->load->model("store/M_store_quantity");
		$this->load->model("store/M_store");

		$data['DataRows'] = $this->M_product->GetMultiRow();
		$data['content_page'] = "account/reports/store_6";
		$this->load->view('page', $data);
	}

	public function hr_1()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_HR_1'));
		$data['title'] = $this->lang->line('ShortCut_Reports_HR_1');

		$this->load->model("hr/M_hr_country");
        $this->load->model("hr/M_hr_education");
        $this->load->model("hr/M_hr_job");
        $this->load->model("hr/M_hr_job_type");
        $this->load->model("hr/M_hr_workgroup");
        $this->load->model("hr/M_hr_employee");
        $this->load->model("account/M_fin_treeaccount");
		$this->load->model("account/M_fin_journal");
		
		$data['country_id'] = 0;
		$data['education_id'] = 0;
		$data['job_id'] = 0;
		$data['job_type_id'] = 0;
		$data['workgroup_id'] = 0;

		$data['DataRows'] = $this->M_hr_employee->GetMultiRow();
		$data['content_page'] = "account/reports/hr_1";
		$this->load->view('page', $data);
	}

	public function hr_1_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_HR_1'));
		$data['title'] = $this->lang->line('ShortCut_Reports_HR_1');

		$this->load->model("hr/M_hr_country");
        $this->load->model("hr/M_hr_education");
        $this->load->model("hr/M_hr_job");
        $this->load->model("hr/M_hr_job_type");
        $this->load->model("hr/M_hr_workgroup");
        $this->load->model("hr/M_hr_employee");
        $this->load->model("account/M_fin_treeaccount");
		$this->load->model("account/M_fin_journal");
		
		$data['country_id'] = set_value('country_id');
		$country_id = set_value('country_id');
		$data['education_id'] = set_value('education_id');
		$education_id = set_value('education_id');
		$data['job_id'] = set_value('job_id');
		$job_id = set_value('job_id');
		$data['job_type_id'] = set_value('job_type_id');
		$job_type_id = set_value('job_type_id');
		$data['workgroup_id'] = set_value('workgroup_id');
		$workgroup_id = set_value('workgroup_id');

		$data['DataRows'] = $this->M_hr_employee->GetReport($country_id, $education_id, $job_id, $job_type_id, $workgroup_id);
		$data['content_page'] = "account/reports/hr_1";
		$this->load->view('page', $data);
	}

	public function hr_2()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_HR_2'));
		$data['title'] = $this->lang->line('ShortCut_Reports_HR_2');

		$this->load->model("hr/M_hr_inouttimedata");
        $this->load->model("hr/M_hr_employee");
        $this->load->model("hr/M_hr_workgrouptime");
        $this->load->model("hr/M_hr_weekend");
		
		$data['employee_id'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');

		$data['DataRows'] = $this->M_hr_inouttimedata->GetMultiRow();
		$data['content_page'] = "account/reports/hr_2";
		$this->load->view('page', $data);
	}

	public function hr_2_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_HR_2'));
		$data['title'] = $this->lang->line('ShortCut_Reports_HR_2');

		$this->load->model("hr/M_hr_inouttimedata");
        $this->load->model("hr/M_hr_employee");
        $this->load->model("hr/M_hr_workgrouptime");
        $this->load->model("hr/M_hr_weekend");
		
		$data['employee_id'] = set_value('employee_id');
		$employee_id = set_value('employee_id');
		$data['fromdate'] = set_value('fromdate');
		$fromdate = set_value('fromdate');
		$data['todate'] = set_value('todate');
		$todate = set_value('todate');

		$data['DataRows'] = $this->M_hr_inouttimedata->GetReport($employee_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/hr_2";
		$this->load->view('page', $data);
	}

	public function hr_3()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_HR_3'));
		$data['title'] = $this->lang->line('ShortCut_Reports_HR_3');

		$this->load->model("hr/M_hr_employee");
		$this->load->model("hr/M_hr_vacation");
		$this->load->model("hr/M_hr_inouttimedata");
		$this->load->model("hr/M_hr_workgrouptime");
        $this->load->model("hr/M_hr_weekend");
		$this->load->model("hr/M_hr_employee_subsalary");

		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');

		$data['DataRows'] = $this->M_hr_employee->GetMultiRow();
		$data['content_page'] = "account/reports/hr_3";
		$this->load->view('page', $data);
	}

	public function hr_3_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_HR_3'));
		$data['title'] = $this->lang->line('ShortCut_Reports_HR_3');

		$this->load->model("hr/M_hr_employee");
		$this->load->model("hr/M_hr_vacation");
		$this->load->model("hr/M_hr_inouttimedata");
		$this->load->model("hr/M_hr_workgrouptime");
        $this->load->model("hr/M_hr_weekend");
		$this->load->model("hr/M_hr_employee_subsalary");

		$data['fromdate'] = set_value('fromdate');
		$fromdate = set_value('fromdate');
		$data['todate'] = set_value('todate');
		$todate = set_value('todate');

		$data['DataRows'] = $this->M_hr_employee->GetMultiRow();
		$data['content_page'] = "account/reports/hr_3";
		$this->load->view('page', $data);
	}

	public function hr_4()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_HR_4'));
		$data['title'] = $this->lang->line('ShortCut_Reports_HR_4');

		$this->load->model("hr/M_hr_vacation");
        $this->load->model("hr/M_hr_vacation_types");
        $this->load->model("hr/M_hr_employee");
		
		$data['employee_id'] = 0;
		$data['fromdate'] = date('Y-m-d');
		$data['todate'] = date('Y-m-d');

		$data['DataRows'] = $this->M_hr_vacation->GetMultiRow();
		$data['content_page'] = "account/reports/hr_4";
		$this->load->view('page', $data);
	}

	public function hr_4_search()
	{
		$this->session->set_userdata('ReportName', $this->lang->line('ShortCut_Reports_HR_4'));
		$data['title'] = $this->lang->line('ShortCut_Reports_HR_4');

		$this->load->model("hr/M_hr_vacation");
        $this->load->model("hr/M_hr_vacation_types");
        $this->load->model("hr/M_hr_employee");
		
		$data['employee_id'] = set_value('employee_id');
		$employee_id = set_value('employee_id');
		$data['fromdate'] = set_value('fromdate');
		$fromdate = set_value('fromdate');
		$data['todate'] = set_value('todate');
		$todate = set_value('todate');

		$data['DataRows'] = $this->M_hr_vacation->GetReport($employee_id, $fromdate, $todate);
		$data['content_page'] = "account/reports/hr_4";
		$this->load->view('page', $data);
	}
}
