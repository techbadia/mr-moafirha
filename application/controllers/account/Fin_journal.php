<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fin_journal extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();

		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {/*
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
              if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                  else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
           */ }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }
				$this->load->model("centers/M_center");
    		$this->load->model("centers/M_sub_centers");
	}

	public function index()
	{
		$data['DataRows'] = $this->M_fin_journal_main->GetMultiRow();
		$data['content_page'] = "account/fin_journal";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$centers = $this->M_center->GetMultiRow();
		foreach ($centers as $center) {
			$sub_centers = $this->M_sub_centers->GetMultiRowCenter($center->id);
			$center->subs = $sub_centers;
		}
		$data['centers'] = $centers;

		$data['content_page'] = "account/fin_journal_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//$this->form_validation->set_rules();
		// var_dump(set_value('acc_invoice_items_de')."-".set_value('acc_invoice_items_cr'));die();

		if (1!=1) { //$this->form_validation->run() == FALSE
            //$this->load->view('index', $data);
           // echo validation_errors();
		}
		else
		{
			$company_id = intval($this->session->userdata('company_id'));
			$user_id = intval($this->session->userdata('StaffID'));
			$automatic = 0;
			$deleted = 0;
			$details = @set_value("details");
			$thedate = set_value("thedate");
			$sub_center_id = set_value("sub_center_id1");
			$sub_center_cred_id = set_value("sub_center_cred_id1");
			$sub_center_id = explode("_",$sub_center_id);
			$sub_center_cred_id = explode("_",$sub_center_cred_id);


			if(!empty($_FILES['image']['name'])){
				$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
				$exts = explode(".", $_FILES['image']['name']);
				$exts = end($exts);
				$exts = strtolower($exts);
				$UploadPath = "./upload/fin_journal/";
				$ImageDirectory = $UploadPath;

				if (!is_dir($ImageDirectory)) {
					mkdir($ImageDirectory, 0777, TRUE);
				}

				$config['upload_path'] = $ImageDirectory;
				$config['allowed_types'] = "*";
				$config['file_name'] = $filename.".".$exts;
				$config['overwrite'] = TRUE;
				$config['file_ext_tolower'] = TRUE;
				$config['remove_spaces'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				$NewFileName = $filename.".".$exts;
				if (! $this->upload->do_upload('image', $NewFileName))
				{
					$error = array('error' => $this->upload->display_errors());
					$NewFileName ="";
				}
			}else{
				$NewFileName ="";
			}

			$bill_id = date("Y").date("m").date("d").date("h").date("i").date("s");
			$table_name = "manual";
			$bond_no = "manual";
			// var_dump($sub_center_cred_id[0]);die();
			$fin_journal_array = array();
			$fin_journal_array = array(
				'company_id' => $company_id,
				'user_id' => $user_id,
				'details' => $details,
				'sub_center_id' => $sub_center_id[1],
				'sub_center_type' => $sub_center_id[0],
				'sub_center_cred_id' => $sub_center_cred_id[1],
				'sub_center_cred_type' => $sub_center_cred_id[0],
				'thedate' => $thedate,
				'bill_id' => $bill_id,
				'table_name' => $table_name,
				'bond_no' => $bond_no,
				// 'sub_center_id' => $sub_center_id,
				'image' => $NewFileName,
				'automatic' => $automatic,
				'deleted' => $deleted,
			);
			$this->M_fin_journal_main->InsertRecord($fin_journal_array);
			$LatestRecord = $this->M_fin_journal_main->GetLatestRecord($company_id, $user_id);
			$main_id = $LatestRecord->id;

			$InvoiceItems = intval(set_value('acc_invoice_items_de'));
			$FromAccountArray = array();
			for ($i = 1; $i <= $InvoiceItems; $i++)
			{
				$account_id = "fromaccount".$i;
				$account_id = set_value($account_id);
				$debit = "debit".$i;
				$debit = set_value($debit);
				$creditor = 0;
				$details = "details".$i;
				$details = set_value($details);
				$subCenterId = "sub_center_id".$i;
				$subCenterCredId = "sub_center_cred_id".$i;
				$sub_center_id = set_value($subCenterId);
				$sub_center_id = explode("_",$sub_center_id);

				// $sub_center_cred_id = set_value($subCenterCredId);
				// var_dump($sub_center_id);die();
				// if($debit > 0)
				// {
					$FromAccountArray = array(
						'main_id' => $main_id,
						'account_id' => $account_id,
						'debit' => $debit,
						'sub_center_id' => $sub_center_id[1],
						'sub_center_type' => $sub_center_id[0],
						'creditor' => $creditor,
						'details' => $details,
					);
					$this->M_fin_journal->InsertRecord($FromAccountArray);
			//	}
			}
			$InvoiceItems = intval(set_value('acc_invoice_items_cr'));

			$ToAccountArray = array();
			for ($i = 1; $i <= $InvoiceItems; $i++)
			{
				$subCenterId = "sub_center_id".$i;
				$subCenterCredId = "sub_center_cred_id".$i;
				$sub_center_id = set_value($subCenterId);
				$sub_center_id = explode("_",$sub_center_id);

				$account_id = "toaccount".$i;
				$account_id = set_value($account_id);
				$debit = 0;
				$creditor = "creditor".$i;
				$creditor = set_value($creditor);
				$details = "details".$i;
				$details = set_value($details);
				$subCenterId = "sub_center_id".$i;
				$subCenterCredId = "sub_center_cred_id".$i;
				// $sub_center_id = set_value($subCenterId);

				$sub_center_cred_id = set_value($subCenterCredId);
				$sub_center_cred_id = explode("_",$sub_center_cred_id);
				// if($creditor > 0)
				// {
					$ToAccountArray = array(
						'main_id' => $main_id,
						'account_id' => $account_id,
						// 'sub_center_id' => $sub_center_id,
						'sub_center_cred_id' => $sub_center_cred_id[1],
						'sub_center_cred_type' => $sub_center_cred_id[0],
						'debit' => $debit,
						'creditor' => $creditor,
						'details' => $details,
					);
					$this->M_fin_journal->InsertRecord($ToAccountArray);
			//	}
			}
			redirect ('account/fin_journal');
		}
	}





	public function updateform($rid)
	{
		$DataRow = $this->M_fin_journal_main->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['user_id'] = $DataRow->user_id;
		$data['details'] = $DataRow->details;
		$data['thedate'] = $DataRow->thedate;
		$data['bill_id'] = $DataRow->bill_id;
		$data['table_name'] = $DataRow->table_name;
		$data['bond_no'] = $DataRow->bond_no;
		$data['image'] = $DataRow->image;
		$data['automatic'] = $DataRow->automatic;
		$data['deleted'] = $DataRow->deleted;

		$data['DataRows'] = $this->M_fin_journal->GetMultiRow($rid);

		$data['ControllerName'] = $this->router->fetch_class();

		$data['content_page'] = "account/fin_journal_update";
        $this->load->view('page', $data);

	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("details","details","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$account_id = set_value("account_id");
			$debit = set_value("debit");
			$creditor = set_value('creditor');
			$details = set_value('details');
			$thedate = set_value('thedate');

			$NewData = array();
			$NewData = array(
				'account_id' => $account_id,
				'debit' => $debit,
				'creditor' => $creditor,
				'details' => $details,
				'thedate' => $thedate,
			);


			$this->M_fin_journal->UpdateRecord($id, $NewData);
			redirect ('account/fin_journal');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);
		$this->load->model("M_fin_journal_main");
        $this->M_fin_journal_main->DelRecord($rid);
        redirect($_SERVER['HTTP_REFERER']);
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_fin_journal->UpdateRecord($rid, $NewData);

        redirect("fin_journal_main");
	}

	public function view_main()
	{
		$id = 0;
		$this->session->unset_userdata('Fin_journal_View_ID');
		$id = $this->input->get('id');
		$MainDataRow = $this->M_fin_journal_main->GetRow($id);
		$data['id'] = $MainDataRow->id;
		$data['company_id'] = $MainDataRow->company_id;
		$data['user_id'] = $MainDataRow->user_id;
		$data['details'] = $MainDataRow->details;
		$data['thedate'] = $MainDataRow->thedate;
		$data['bill_id'] = $MainDataRow->bill_id;
		$data['table_name'] = $MainDataRow->table_name;
		$data['bond_no'] = $MainDataRow->bond_no;
		$data['image'] = $MainDataRow->image;
		$data['automatic'] = $MainDataRow->automatic;
		$data['deleted'] = $MainDataRow->deleted;

		//$DataRows = $this->M_fin_journal->GetMultiRow($id);
		//$this->session->unset_userdata('Fin_journal_View_ID');

		$this->session->set_userdata('Fin_journal_View_ID', $id);

		echo json_encode($MainDataRow);

    	exit();
	}

	public function view_details()
	{
		$id = $this->input->get('id');
		//$id = intval($this->session->userdata('Fin_journal_View_ID'));
		$DetailsRows = $this->M_fin_journal->GetRowDetails($id);
		echo json_encode($DetailsRows);
		exit();
	}

	public function view_pdf($rid, $type=1)
	{
		$MainDataRow = $this->M_fin_journal_main->GetRow($rid);
		$data['id'] = $MainDataRow->id;
		$data['company_id'] = $MainDataRow->company_id;
		$data['user_id'] = $MainDataRow->user_id;
		$data['details'] = $MainDataRow->details;
		$data['thedate'] = $MainDataRow->thedate;
		$data['bill_id'] = $MainDataRow->bill_id;
		$data['table_name'] = $MainDataRow->table_name;
		$data['bond_no'] = $MainDataRow->bond_no;
		$data['image'] = $MainDataRow->image;
		$data['automatic'] = $MainDataRow->automatic;
		$data['deleted'] = $MainDataRow->deleted;

		$DetailsRows = $this->M_fin_journal->GetRowDetails($rid);

		$Alignment = "";
		$Direction = "";
		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
			$Alignment = "right";
			$Direction = "rtl";
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
			$Alignment = "left";
			$Direction = "ltr";
		}

		$this->load->model("settings/M_fin_settings");
		$company_id = intval($this->session->userdata('company_id'));
		$DataRow = $this->M_app_company->GetRow($company_id);
		$HeaderImage = base_url()."upload/settings/".$DataRow->header;
		$FooterImage = base_url()."upload/settings/".$DataRow->footer;

		$Directionality = $this->session->userdata('Direction');
		if($type == 1)
		{
			$PageTitle = lang('fin_journal');
		}
		else if($type == 2)
		{
			$PageTitle = lang('mon_exchange_bonds');
		}
		else
		{
			$PageTitle = lang('mon_receipts');
		}


		$PageSize = $this->session->userdata('sale_bill_size');
		$PageOrientation = $this->session->userdata('sale_bill_orientation');
		$PageFormat = $PageSize."-".$PageOrientation;

		require_once 'vendor/autoload.php';
		$mpdf = new \Mpdf\Mpdf([
			'mode' => 'utf-8',
			'format' => $PageFormat,
			//'margin_left' => 5,
			//'margin_right' => 5,
			'margin_top' => 25,
			'margin_bottom' => 25,
			'margin_header' => 5,
			'margin_footer' => 5,
		]);

		$mpdf->allow_charset_conversion=true;
		$mpdf->SetTitle($PageTitle);
		$mpdf->SetDirectionality($Directionality);
		$mpdf->autoScriptToLang = true;
		$mpdf->autoLangToFont = true;
		$mpdf->debug = true;
		ob_end_clean();
		header("Content-Encoding: None", true);
		$mpdf->SetHTMLHeader('
		<div style="text-align: center; font-weight: bold;">
			<img src="'.$HeaderImage.'">
		</div>');
		$mpdf->SetHTMLFooter('
		<table width="100%" style="vertical-align: bottom; font-family: serif;
			font-size: 8pt; color: #000000; font-weight: bold;">
			<tr>
				<td colspan="2"><img src="'.$FooterImage.'"></td>
			</tr>
			<tr>
				<td width="50%">{DATE j-m-Y}</td>
				<td width="50%" align="left" lang="en">{PAGENO}/{nbpg}</td>
			</tr>
		</table>');

		$id_length = 5;
		$row_id = $str = substr(str_repeat(0, $id_length) . $MainDataRow->id, -$id_length);
		$html = '';
		$html .= '<div align="center" style="font-size:18px">';
		$html .= '<strong>'.$PageTitle.'</strong><br><small>'.$row_id.'</strong><br>';
		$html .= '<table cellspacing="0" cellpadding="5" border="0" width="98%">';
		$html .= '<tr>';
		$html .= '<td width="20%">'.$this->lang->line('fin_journal_details').' : </td>';
		$html .= '<td width="80%">'.$MainDataRow->details.'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td width="20%">'.$this->lang->line('fin_journal_thedate').' : </td>';
		$html .= '<td width="80%">'.$MainDataRow->thedate.'</td>';
		$html .= '</tr>';
		$html .= '</table>';
		//$html .= $PageTitle.'<br>('.$invoice_id.')-'.$DataRow->thedate."<br>";
		$html .= '</div><br>';
		$html .= '<table cellspacing="0" cellpadding="5" border="1" width="98%">';
		$html .= '<thead>';
		$html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
		$html .= '<th align="right">'.$this->lang->line('fin_treeaccount_account_no').'</th>';
		$html .= '<th align="right">'.$this->lang->line('fin_journal_account_id').'</th>';
		$html .= '<th align="center">'.$this->lang->line('fin_journal_debit').'</th>';
		$html .= '<th align="center">'.$this->lang->line('fin_journal_creditor').'</th>';
		$html .= '<th align="center">'.$this->lang->line('sub_center_id').'</th>';
		$html .= '<th align="right">'.$this->lang->line('fin_journal_details').'</th>';
		$html .= '</tr>';
		$html .= '</thead>';
		$html .= '<tbody>';
		$Serial = 0;
		$TotalValue =0;
		$AllQuantity = 0;
		$FinalBigQ = 0;
		$FinalSmallQ = 0;
		foreach($DetailsRows as $MultiRows_Row)
		{
			$centerTitle = "";
			// print_r($MultiRows_Row);die()
			// $subCenterId = (!empty($MultiRows_Row->sub_center_id))?$MultiRows_Row->sub_center_id:$MultiRows_Row->sub_center_cred_id;
			// $subCenterType = (!empty($MultiRows_Row->sub_center_type))?$MultiRows_Row->sub_center_type:$MultiRows_Row->sub_center_cred_type;
			if(!empty($MultiRows_Row->sub_center_id)){

			if($MultiRows_Row->sub_center_type == "main")
			{
				$center = $this->M_center->GetRow($MultiRows_Row->sub_center_id);
			}else {
				$center = $this->M_sub_centers->GetRow($MultiRows_Row->sub_center_id);
				// code...
			}
		}
			if(!empty($MultiRows_Row->sub_center_cred_id)){

			if($MultiRows_Row->sub_center_type == "main")
			{
				$centerCred = $this->M_center->GetRow($MultiRows_Row->sub_center_cred_id);
			}else {
				$centerCred = $this->M_sub_centers->GetRow($MultiRows_Row->sub_center_cred_id);
				// code...
			}
		}
			$html .= '<tr>';
			if ($this->session->userdata('lang') == "ar")
			{
				$AccountTitla = $MultiRows_Row->title;
			}
			else
			{
				$AccountTitla = $MultiRows_Row->title_en;
			}
			if ($this->session->userdata('lang') == "ar")
			{
				$centerTitle = $center->name_ar;
				$centerTitleCred = $centerCred->name_ar;
			}
			else
			{
				$centerTitle = $center->name_en;
				$centerTitleCred = $centerCred->name_en;
			}
			if(intval($MultiRows_Row->debit) > 0)
			{
				$centerTitle = $centerTitle;
			}else {

				$centerTitle = $centerTitleCred;


			}
			$centerTitle = (!empty($centerTitle))?$centerTitle:"---";
			$html .= '<td align="right">'.$MultiRows_Row->account_no.'</td>';
			$html .= '<td align="right">'.$AccountTitla.'</td>';
			$html .= '<td align="center">'.number_format($MultiRows_Row->debit,2).'</td>';
			$html .= '<td align="center">'.number_format($MultiRows_Row->creditor,2).'</td>';
			$html .= '<td align="center">'.$centerTitle.'</td>';
			$html .= '<td align="right">'.$MultiRows_Row->details.'</td>';
			$html .= '</tr>';
		}

		$html .= '</tbody>';
		$html .= '</table>';
		$html .= '<br>';


		$html = iconv("utf-8","UTF-8//IGNORE",$html);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}
}
