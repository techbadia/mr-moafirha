<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Report extends CI_Controller {

    //var $data = array();
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');     
        $this->load->model('M_mon_treasury');
        $this->load->model('M_ban_banks');
        $this->load->model('purchase/M_buy_bill');
        $this->load->model('purchase/M_buy_returns');
    }   

        
	public function index()
	{
		ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $this->load->model("M_app_module");
        $this->load->model("M_app_module");
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
	    if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            $data['MyAlarmMenu'] = $this->M_ser_alarms->GetAlarmMenu($MemberID, $thedate);
            $data['MyAlarmCount'] = $this->M_ser_alarms->GetAlarmMenuCount($MemberID, $thedate);
            $data['MyMessageMenu'] = $this->M_ser_message->GetMessageMenu($MemberID);
            $data['MyMessageCount'] = $this->M_ser_message->GetMessageMenuCount($MemberID);
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('GroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->model('M_per_employee');
	        $data['MultiRows'] = $this->M_per_employee->GetMultiRow();
	        
			$this->load->model('M_per_employee_subsalary');
			$this->load->model('M_per_workgroup');
			
	        $data['RowCount'] = 0;
			$this->load->view('report', $data);
        }
        else {
            $this->load->view('login', $data);
        }
	}

	public function view()
    {
    	ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $this->load->model("M_app_module");
        $this->load->model("M_app_module");
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            $data['MyAlarmMenu'] = $this->M_ser_alarms->GetAlarmMenu($MemberID, $thedate);
            $data['MyAlarmCount'] = $this->M_ser_alarms->GetAlarmMenuCount($MemberID, $thedate);
            $data['MyMessageMenu'] = $this->M_ser_message->GetMessageMenu($MemberID);
            $data['MyMessageCount'] = $this->M_ser_message->GetMessageMenuCount($MemberID);
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('GroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
			
			$this->load->model('M_per_employee');
	        $this->load->model('M_per_worktime');
	        $this->load->model('M_per_workgrouptime');
	        
	        $data['MultiRows'] = $this->M_per_employee->GetMultiRow();
	        
	        $employee_id = set_value('employee_id');
			$data['employee_id'] = $employee_id;
			
	        $fromdate = set_value('fromdate');
	        $todate = set_value('todate');
	        
	        $EmployeeName = $this->M_per_employee->GetRow($employee_id);
	        $data['ArabicName'] = $EmployeeName->ar_name;
	        $data['BasicSalary'] = $EmployeeName->basic_salary;
			$data['job_id'] = $EmployeeName->job_id;
			$data['WorkGroupID'] = $EmployeeName->workgroup_id;
	        $data['fromdate'] = $fromdate;
	        $data['todate'] = $todate;
	        
	        $this->load->helper('date');
	        
	        
	        $this->load->model('M_per_inouttimedata');
	        $data['DaysCount'] = $this->M_per_inouttimedata->GetDays($employee_id, $fromdate, $todate);
	        $SearchDays = $this->M_per_inouttimedata->GetDays($employee_id, $fromdate, $todate);
	        $data['TransactionsRows'] = $this->M_per_inouttimedata->GetReport($employee_id, $fromdate, $todate);
	        $data['RowCount'] = 1;
	        
	        $this->load->model('M_per_weekend');
	        $this->load->model('M_per_employee_subsalary');
			$this->load->model('M_per_workgroup');
			$this->load->model('M_per_job');
			
	        $WeekEndWorked = 0;
	        $WeekEndOffer = "";
	        $DayName = "";
	        $DayDate = date("j/n/Y", strtotime('+0 day', strtotime($fromdate)));
	        for ($x = 0; $x <= ($SearchDays - 1); $x++) {
	            $DayDate = date("Y-m-d", strtotime('+'.$x.' day', strtotime($fromdate)));
	            $DayName = date('l',strtotime($DayDate));
	            $WeekEndRecord = $this->M_per_weekend->Getper_weekend($DayName);
	            $WeekEndOffer = $this->M_per_weekend->GetDayOffer($DayName);
	            
	            if ($WeekEndRecord >0)
	            {
	                $WeekEndWorked += $WeekEndOffer->dayoffer;
	            }
	        }
	        $data['WeekEndWorked'] = $WeekEndWorked;
	        
	        $StartWordDate = $this->M_per_inouttimedata->GetStartDate($employee_id, $fromdate, $todate);
	        $EndWordDate = $this->M_per_inouttimedata->GetEndDate($employee_id, $fromdate, $todate);
	        
	        $StartWordDateCount = $this->M_per_inouttimedata->GetStartDateCount($employee_id, $fromdate, $todate);
			$EndWordDateCount = $this->M_per_inouttimedata->GetEndDateCount($employee_id, $fromdate, $todate);
			
			if (($StartWordDateCount >0) AND ($EndWordDateCount))
			{
				$FirstDate = ($StartWordDate->thedate);
		        $FirstDate = str_replace('-', '/', $FirstDate);
		        $FirstDate = new DateTime($FirstDate);
		
		        $LastDate = ($EndWordDate->thedate);
		        $LastDate = str_replace('-', '/', $LastDate);
		        $LastDate = new DateTime($LastDate);
		        
		        
		        $dDiff = $FirstDate->diff($LastDate);
		        $days = $dDiff->days;
		        
		        $data['days'] = $days;
		        $days +=1;
		        $WeekEndDays = 0;
		        //$FirstDate = date("j/n/Y", strtotime('+0 day', strtotime($FirstDate)));
		        for ($x = 0; $x <= ($days - 1); $x++) {
		            $DayDate = date("Y-m-d", strtotime('+'.$x.' day', strtotime($fromdate)));
		            $DayName = date('l',strtotime($DayDate));
		            $WeekEndRecord = $this->M_per_weekend->Getper_weekend($DayName);
		            if ($WeekEndRecord >0)
		            {
		                $WeekEndDays += 1;
		            }
		        }
		        $data['WeekEndDays'] = $WeekEndDays;
		        
		        $HolydayDays = 0;
		        $this->load->model('M_per_holidays');
		        for ($x = 0; $x <= ($days - 1); $x++) {
		            $DayDate = date("Y-m-d", strtotime('+'.$x.' day', strtotime($fromdate)));
		            $HolidayRecord = $this->M_per_holidays->GetDays($DayDate);
		            if ($HolidayRecord >0)
		            {
		                $HolydayDays += 1;
		            }
		        }
		        $data['HolydayDays'] = $HolydayDays;
		        
		        $this->load->view('report', $data);
			}
			else
				{
					$data['days'] = 0;
					$data['WeekEndDays'] = 0;
					$data['HolydayDays'] = 0;
					$data['WeekEndDays'] = 0;
					$data['Action1'] = 0;
					$data['Action2'] = 0;
					$data['Action3'] = 0;
					$data['Action1'] = 0;
					$this->load->view('report', $data);
			}
        }
        else
            {
                $this->load->view('login');
            }
    }

    public function customer_statment()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
        $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $fromdate = $thedate;
            $todate = $thedate;
            $accountid = $thedate;
            
            $this->load->model("M_fin_journal");
            $data['MultiRows'] = $this->M_fin_journal->GetAcountReport($accountid, $fromdate, $todate);
            
            $AllSalesMainAccount = intval($this->session->userdata('appsetting_sale_field1'));
            
            $this->load->model("M_fin_treeaccount");
            $data['AllAcounts'] = $this->M_fin_treeaccount->GetChildAccount($AllSalesMainAccount);
            
            
            
            $this->load->model("M_fin_treecategory");
            $data['AccountCategory'] = $this->M_fin_treecategory->GetMultiRow();
            
            $details = " كشف حساب عميل";
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $data['fromdate'] = date("Y-m-d");
            $data['todate'] = date("Y-m-d");
            $data['accountid'] = 0;
            
			$data['Search'] =0;
            $this->load->view('report_customer_statment', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function customer_statment_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
        $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        $data['fromdate'] = set_value('fromdate');
        $data['todate'] = set_value('todate');
        $data['accountid'] = set_value('accountid');
        
        $fromdate = set_value('fromdate');
        $todate = set_value('todate');
        $accountid = set_value('accountid');
        $this->load->model("M_fin_treeaccount");
        $Account = $this->M_fin_treeaccount->GetRow($accountid);
		$data['AccountCategoryID'] = $Account->category_id;
        $data['AccountName'] = $Account->title;
        $data['StartAmount'] = $Account->startamount;
		
        $this->load->model("M_fin_journal");
        $data['MultiRows'] = $this->M_fin_journal->GetAcountReport($accountid, $fromdate, $todate);
        
        $this->load->model("M_fin_treeaccount");
        //$data['AllAccounts'] = $this->M_fin_treeaccount->GetMultiRow();
        $this->load->model("M_bas_branches");
        
        $data['Search'] = 1;
        
        $data['Debit'] = $this->M_fin_journal->GetDebit_Ledger($accountid, $fromdate, $todate);
        $data['Creditor'] = $this->M_fin_journal->GetCreditor_Ledger($accountid, $fromdate, $todate);
        
        $this->load->view('report_customer_statment', $data);
    }
    
    public function supplier_statment()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
        $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $fromdate = $thedate;
            $todate = $thedate;
            $accountid = $thedate;
            
            $this->load->model("M_fin_journal");
            $data['MultiRows'] = $this->M_fin_journal->GetAcountReport($accountid, $fromdate, $todate);
            
            $AllSalesMainAccount = intval($this->session->userdata('appsetting_buy_field1'));
            
            $this->load->model("M_fin_treeaccount");
            $data['AllAcounts'] = $this->M_fin_treeaccount->GetChildAccount($AllSalesMainAccount);
            
            
            
            $this->load->model("M_fin_treecategory");
            $data['AccountCategory'] = $this->M_fin_treecategory->GetMultiRow();
            
            $details = " كشف حساب مورد";
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $data['fromdate'] = date("Y-m-d");
            $data['todate'] = date("Y-m-d");
            $data['accountid'] = 0;
            
			$data['Search'] =0;
            $this->load->view('report_supplier_statment', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function supplier_statment_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
        $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        $data['fromdate'] = set_value('fromdate');
        $data['todate'] = set_value('todate');
        $data['accountid'] = set_value('accountid');
        
        $fromdate = set_value('fromdate');
        $todate = set_value('todate');
        $accountid = set_value('accountid');
        $this->load->model("M_fin_treeaccount");
        $Account = $this->M_fin_treeaccount->GetRow($accountid);
		$data['AccountCategoryID'] = $Account->category_id;
        $data['AccountName'] = $Account->title;
        $data['StartAmount'] = $Account->startamount;
		
        $this->load->model("M_fin_journal");
        $data['MultiRows'] = $this->M_fin_journal->GetAcountReport($accountid, $fromdate, $todate);
        
        $this->load->model("M_fin_treeaccount");
        //$data['AllAccounts'] = $this->M_fin_treeaccount->GetMultiRow();
        $this->load->model("M_bas_branches");
        
        $data['Search'] = 1;
        
        $data['Debit'] = $this->M_fin_journal->GetDebit_Ledger($accountid, $fromdate, $todate);
        $data['Creditor'] = $this->M_fin_journal->GetCreditor_Ledger($accountid, $fromdate, $todate);
        
        $this->load->view('report_supplier_statment', $data);
    }
    
    public function customer_balance()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $data['fromdate'] = $thedate;
            $data['todate'] = $thedate;
            $data['customer_id'] = 0;
            $data['check_customer_id'] = "";
            $data['check_fromdate'] = "";
            
            $this->load->model('M_sale_customer');
            $data['MultiRows'] = $this->M_sale_customer->GetEmptyRows();
            $data['Search'] = "False";
            $details = $this->lang->line('Sale_Report3');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_customer_balance', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function account_balance_view($account_id)
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $data['fromdate'] = $thedate;
            $data['todate'] = $thedate;
            $data['customer_id'] = $account_id;
            $data['check_customer_id'] = "True";
            $data['check_fromdate'] = "";
            
            $this->load->model("M_fin_journal");
            $data['MultiRows'] = $this->M_fin_journal->GetAcountReport($account_id, $thedate, $thedate);
            $data['Search'] = "False";
            $details = $this->lang->line('Sale_Report3');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            //echo $account_id;
            $this->load->view('report_account_balance', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function customer_balance_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->helper('security');
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("customer_id","customer_id","required");
            
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            }
            
            $data['fromdate'] = set_value('fromdate');
            $data['todate'] = set_value('todate');
            $data['customer_id'] = set_value('customer_id');
            $customer_id = set_value('customer_id');
            $data['check_customer_id'] = set_value('check_customer_id');
            $check_customer_id = set_value('check_customer_id');
            $data['check_fromdate'] = set_value('check_fromdate');
            
            $this->load->model('M_sale_customer');
            if($check_customer_id == "True")
            {
                $data['MultiRows'] = $this->M_sale_customer->GetOnlyOne($customer_id);
            }
            else
            {
                $data['MultiRows'] = $this->M_sale_customer->GetMultiRow();
            }
            
            $data['Search'] = "False";
            $details = $this->lang->line('Sale_Report3');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_customer_balance', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function account_balance_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->helper('security');
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("customer_id","customer_id","required");
            
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            }
            
            $data['fromdate'] = set_value('fromdate');
            $fromdate = set_value('fromdate');
            $data['todate'] = set_value('todate');
            $todate = set_value('todate');
            $data['customer_id'] = set_value('customer_id');
            $customer_id = set_value('customer_id');
            $data['check_customer_id'] = set_value('check_customer_id');
            $check_customer_id = set_value('check_customer_id');
            $data['check_fromdate'] = set_value('check_fromdate');
            
            $this->load->model("M_fin_journal");
            $data['MultiRows'] = $this->M_fin_journal->GetAcountReport($customer_id, $fromdate, $todate);
            
            $data['Search'] = "False";
            $details = $this->lang->line('Sale_Report3');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_account_balance', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function customer_sale()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $data['fromdate'] = $thedate;
            $data['todate'] = $thedate;
            $data['customer_id'] = 0;
            $data['user_id'] = 0;
            $data['delegate_id'] = 0;
            $data['check_user_id'] = "";
            $data['check_delegate_id'] = "";
            $data['check_customer_id'] = "";
            $data['check_fromdate'] = "";
            
            $this->load->model('M_sale_delegates');
            $this->load->model('M_sale_bill');
            $this->load->model('M_sale_returns');
            $this->load->model('M_sale_customer');
            $data['MultiRows'] = $this->M_sale_customer->GetEmptyRows();
            $data['Search'] = "False";
            $details = $this->lang->line('Sale_Report3');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_customer_sale', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function customer_sale_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->helper('security');
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("customer_id","customer_id","required");
            
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            }
            
            $data['fromdate'] = set_value('fromdate');
            $data['todate'] = set_value('todate');
            
            $data['check_customer_id'] = set_value('check_customer_id');
            $check_customer_id = set_value('check_customer_id');
            if($check_customer_id == "True")
            {
                $data['customer_id'] = set_value('customer_id');
                $customer_id = set_value('customer_id');
            }
            else
            {
                $data['customer_id'] = 0;
                $customer_id = 0;
            }
            $data['check_fromdate'] = set_value('check_fromdate');
            $data['user_id'] = set_value('user_id');
            $data['delegate_id'] = set_value('delegate_id');
            $data['check_user_id'] = set_value('check_user_id');
            $data['check_delegate_id'] = set_value('check_delegate_id');
            
            $this->load->model('M_sale_delegates');
            $this->load->model('M_sale_bill');
            $this->load->model('M_sale_returns');
            $this->load->model('M_sale_customer');

            if($check_customer_id == "True")
            {
                $data['MultiRows'] = $this->M_sale_customer->GetOnlyOne($customer_id);
            }
            else
            {
				$AllSalesMainAccount = intval($this->session->userdata('appsetting_sale_field1'));
				//$AllAccounts = $this->M_fin_treeaccount->GetChildAccount($AllSalesMainAccount);
                $data['MultiRows'] = $this->M_fin_treeaccount->GetChildAccount($AllSalesMainAccount);
            }
            $data['Search'] = "False";
            $details = $this->lang->line('Sale_Report4');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_customer_sale', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function customer_sale_item()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $data['fromdate'] = $thedate;
            $data['todate'] = $thedate;
            $data['customer_id'] = 0;
            $data['check_customer_id'] = "True";
            $data['check_fromdate'] = "";
            
            $this->load->model('M_war_types');
            $this->load->model('M_sale_bill');
            $this->load->model('M_sale_bill_items');
            $this->load->model('M_sale_returns');
            $this->load->model('M_sale_returns_items');
            $this->load->model('M_sale_customer');
            $data['MultiRows'] = $this->M_war_types->FillList();
            $data['Search'] = "False";
            $details = $this->lang->line('Sale_Report5');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_customer_sale_item', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function customer_sale_item_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->helper('security');
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("customer_id","customer_id","required");
            
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            }
            
            
            $data['check_fromdate'] = set_value('check_fromdate');
            $check_fromdate = set_value('check_fromdate');
            if($check_fromdate == "True")
            {
                $data['fromdate'] = set_value('fromdate');
                $data['todate'] = set_value('todate');
            }
            else
            {
                $data['fromdate'] = $thedate;
                $data['todate'] = $thedate;
            }
            $data['check_customer_id'] = set_value('check_customer_id');
            $data['customer_id'] = set_value('customer_id');
            
            $this->load->model('M_war_types');
            $this->load->model('M_sale_bill');
            $this->load->model('M_sale_bill_items');
            $this->load->model('M_sale_returns');
            $this->load->model('M_sale_returns_items');
            $this->load->model('M_sale_customer');
            $data['MultiRows'] = $this->M_war_types->FillList();
            $data['Search'] = "False";
            $details = $this->lang->line('Sale_Report5');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_customer_sale_item', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function items_sales()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $data['fromdate'] = $thedate;
            $data['todate'] = $thedate;
            $data['type_id'] = 0;
            $data['check_type_id'] = "";
            $data['check_fromdate'] = "";
            
            $this->load->model('M_war_types');
            $this->load->model('M_sale_bill');
            $this->load->model('M_sale_bill_items');
            $this->load->model('M_sale_returns');
            $this->load->model('M_sale_returns_items');
            $this->load->model('M_sale_customer');
            $data['MultiRows'] = $this->M_war_types->FillList();
            $data['Search'] = "False";
            $details = $this->lang->line('Sale_Report6');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_items_sales', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function items_sales_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->helper('security');
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("type_id","type_id","required");
            
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            }
            
            
            $data['check_fromdate'] = set_value('check_fromdate');
            $check_fromdate = set_value('check_fromdate');
            if($check_fromdate == "True")
            {
                $data['fromdate'] = set_value('fromdate');
                $data['todate'] = set_value('todate');
            }
            else
            {
                $data['fromdate'] = $thedate;
                $data['todate'] = $thedate;
            }
            $data['check_type_id'] = set_value('check_type_id');
            $check_type_id = set_value('check_type_id');
            if($check_type_id == "True")
            {
                $data['type_id'] = set_value('type_id');
                $type_id = set_value('type_id');
            }
            else
            {
                $data['type_id'] = 0;
                $type_id = 0;
            }
            
            $this->load->model('M_war_types');
            $this->load->model('M_sale_bill');
            $this->load->model('M_sale_bill_items');
            $this->load->model('M_sale_returns');
            $this->load->model('M_sale_returns_items');
            $this->load->model('M_sale_customer');
            if($type_id == 0)
            {
                $data['MultiRows'] = $this->M_war_types->FillList();
            }
            else
            {
                $data['MultiRows'] = $this->M_war_types->FillOnlyOne($type_id);
            }
            
            $data['Search'] = "False";
            $details = $this->lang->line('Sale_Report6');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_items_sales', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function customer_list()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->model('M_sale_customer');
            $data['MultiRows'] = $this->M_sale_customer->GetMultiRow();
            $data['Search'] = "False";
            $details = $this->lang->line('Sale_Report7');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_customer_list', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function customer_list_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->helper('security');
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("search","search","required");
            
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            }
            else {
                $search = set_value('search');
            }
            $this->load->model('M_sale_customer');
            $data['MultiRows'] = $this->M_sale_customer->GetSearch($search);
            $data['Search'] = "True";
            $details = "عرض بيانات العملاء";
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_customer_list', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function money_report()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
        $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            
            $fromdate = $thedate;
            $todate = $thedate;
            $accountid = $thedate;
            $this->load->model("M_fin_journal");
            $data['MultiRows'] = $this->M_fin_journal->GetAcountReport($accountid, $fromdate, $todate);
            
            $AllSalesMainAccount = intval($this->session->userdata('appsetting_sale_field1'));
            
            $this->load->model("M_fin_treeaccount");
            $data['AllAcounts'] = $this->M_fin_treeaccount->GetChildAccount($AllSalesMainAccount);
            
            $this->load->model("M_fin_treecategory");
            $data['AccountCategory'] = $this->M_fin_treecategory->GetMultiRow();
            
            $details = $this->lang->line('Money_Report1');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $data['fromdate'] = date("Y-m-d");
            $data['todate'] = date("Y-m-d");
            $data['accountid'] = 0;
            
			$data['Search'] =0;
            $this->load->view('report_money_report', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function money_report_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
        $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        $data['fromdate'] = set_value('fromdate');
        $data['todate'] = set_value('todate');
        $data['accountid'] = set_value('accountid');
        
        $fromdate = set_value('fromdate');
        $todate = set_value('todate');
        $accountid = set_value('accountid');
        $this->load->model("M_fin_treeaccount");
        $Account = $this->M_fin_treeaccount->GetRow($accountid);
		$data['AccountCategoryID'] = $Account->category_id;
        $data['AccountName'] = $Account->title;
        $data['StartAmount'] = $Account->startamount;
		
        $this->load->model("M_fin_journal");
        $data['MultiRows'] = $this->M_fin_journal->GetAcountReport($accountid, $fromdate, $todate);
        
        $this->load->model("M_fin_treeaccount");
        //$data['AllAccounts'] = $this->M_fin_treeaccount->GetMultiRow();
        $this->load->model("M_bas_branches");
        
        $data['Search'] = 1;
        
        $data['Debit'] = $this->M_fin_journal->GetDebit_Ledger($accountid, $fromdate, $todate);
        $data['Creditor'] = $this->M_fin_journal->GetCreditor_Ledger($accountid, $fromdate, $todate);
        
        $this->load->view('report_money_report', $data);
    }
    
    public function cash_balances()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
        $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);

            $this->load->model("M_mon_treasury");
            $this->load->model("M_ban_banks");
            $data['Treasury'] = $this->M_mon_treasury->GetMultiRow();
            $data['Banks'] = $this->M_ban_banks->GetMultiRow();

            $this->load->view('cash_balances_report', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function main_accounts()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
        $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            
            $this->load->model("M_fin_treeaccount");
            $data['MultiRows'] = $this->M_fin_treeaccount->GetMultiRow();
            
            $this->load->model("M_fin_treeaccount");
            $data['AllAcounts'] = $this->M_fin_treeaccount->GetMultiRow();
            
            $this->load->model("M_fin_treecategory");
            $data['AccountCategory'] = $this->M_fin_treecategory->GetMultiRow();
            
            $details = "عرض الحسابات الرئيسية";
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $data['fromdate'] = date("Y-m-d");
            $data['todate'] = date("Y-m-d");
            $data['accountid'] = 0;
            
			$data['Search'] =0;
            $this->load->view('report_main_accounts', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function main_accounts_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
        $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        $data['fromdate'] = set_value('fromdate');
        $data['todate'] = set_value('todate');
        $data['accountid'] = set_value('accountid');
        $accountid = set_value('accountid');

        
        $fromdate = set_value('fromdate');
        $todate = set_value('todate');

        $this->load->model("M_fin_treeaccount");
        $Account = $this->M_fin_treeaccount->GetRow($accountid);
		$data['AccountCategoryID'] = $Account->category_id;
        $data['AccountName'] = $Account->title;
        $data['StartAmount'] = $Account->startamount;
		
        $this->load->model("M_fin_journal");
        $this->load->model("M_fin_treeaccount");
        $data['MultiRows'] = $this->M_fin_treeaccount->GetOnlyOne($accountid);
        //echo $accountid;
        $this->load->model("M_fin_treeaccount");
        //$data['AllAccounts'] = $this->M_fin_treeaccount->GetMultiRow();
        $this->load->model("M_bas_branches");
        
        $data['Search'] = 1;
        
        $data['Debit'] = $this->M_fin_journal->GetDebit_Ledger($accountid, $fromdate, $todate);
        $data['Creditor'] = $this->M_fin_journal->GetCreditor_Ledger($accountid, $fromdate, $todate);
        
        $this->load->view('report_main_accounts', $data);
    }
    
    public function supplier_balance()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $data['fromdate'] = $thedate;
            $data['todate'] = $thedate;
            $data['customer_id'] = 0;
            $data['check_customer_id'] = "";
            $data['check_fromdate'] = "";
            
            $this->load->model('M_buy_manufacturer');
            $data['MultiRows'] = $this->M_buy_manufacturer->GetEmptyRows();
            $data['Search'] = "False";
            $details = $this->lang->line('Buy_Report3');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_supplier_balance', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function supplier_balance_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->helper('security');
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("customer_id","customer_id","required");
            
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            }
            
            $data['fromdate'] = set_value('fromdate');
            $data['todate'] = set_value('todate');
            $data['customer_id'] = set_value('customer_id');
            $customer_id = set_value('customer_id');
            $data['check_customer_id'] = set_value('check_customer_id');
            $check_customer_id = set_value('check_customer_id');
            $data['check_fromdate'] = set_value('check_fromdate');
            
            $this->load->model('M_buy_manufacturer');
            if($check_customer_id == "True")
            {
                $data['MultiRows'] = $this->M_buy_manufacturer->GetOnlyOne($customer_id);
            }
            else
            {
                $data['MultiRows'] = $this->M_buy_manufacturer->GetMultiRow();
            }
            
            $data['Search'] = "False";
            $details = $this->lang->line('Buy_Report3');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_supplier_balance', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function supplier_sale()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $data['fromdate'] = $thedate;
            $data['todate'] = $thedate;
            $data['customer_id'] = 0;
            $data['user_id'] = 0;
            $data['delegate_id'] = 0;
            $data['check_user_id'] = "";
            $data['check_delegate_id'] = "";
            $data['check_customer_id'] = "";
            $data['check_fromdate'] = "";
            
            $this->load->model('M_sale_delegates');
            $this->load->model('M_sale_bill');
            $this->load->model('M_sale_returns');
            $this->load->model('M_buy_manufacturer');
            
            $data['MultiRows'] = $this->M_buy_manufacturer->GetEmptyRows();
            $data['Search'] = "False";
            $details = $this->lang->line('Buy_Report4');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_supplier_sale', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function supplier_sale_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->helper('security');
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("customer_id","customer_id","required");
            
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            }
            
            $data['fromdate'] = set_value('fromdate');
            $data['todate'] = set_value('todate');
            
            $data['check_customer_id'] = set_value('check_customer_id');
            $check_customer_id = set_value('check_customer_id');
            if($check_customer_id == "True")
            {
                $data['customer_id'] = set_value('customer_id');
                $customer_id = set_value('customer_id');
            }
            else
            {
                $data['customer_id'] = 0;
                $customer_id = 0;
            }
            $data['check_fromdate'] = set_value('check_fromdate');
            $data['user_id'] = set_value('user_id');
            $data['delegate_id'] = set_value('delegate_id');
            $data['check_user_id'] = set_value('check_user_id');
            $data['check_delegate_id'] = set_value('check_delegate_id');
            
            $this->load->model('M_sale_delegates');
            $this->load->model('M_buy_bill');
            $this->load->model('M_buy_returns');
            $this->load->model('M_buy_manufacturer');

            if($check_customer_id == "True")
            {
                $data['MultiRows'] = $this->M_buy_manufacturer->GetOnlyOne($customer_id);
            }
            else
            {
				$AllSalesMainAccount = intval($this->session->userdata('appsetting_buy_field1'));
				//$AllAccounts = $this->M_fin_treeaccount->GetChildAccount($AllSalesMainAccount);
                $data['MultiRows'] = $this->M_fin_treeaccount->GetChildAccount($AllSalesMainAccount);
            }
            $data['Search'] = "False";
            $details = $this->lang->line('Buy_Report4');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_supplier_sale', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function supplier_sale_item()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $data['fromdate'] = $thedate;
            $data['todate'] = $thedate;
            $data['customer_id'] = 0;
            $data['check_customer_id'] = "True";
            $data['check_fromdate'] = "";
            
            $this->load->model('M_war_types');
            $this->load->model('M_buy_bill');
            $this->load->model('M_buy_bill_items');
            $this->load->model('M_buy_returns');
            $this->load->model('M_buy_returns_items');
            $this->load->model('M_buy_manufacturer');
            $data['MultiRows'] = $this->M_war_types->FillList();
            $data['Search'] = "False";
            $details = $this->lang->line('Buy_Report5');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_supplier_sale_item', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function supplier_sale_item_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->helper('security');
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("customer_id","customer_id","required");
            
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            }
            
            
            $data['check_fromdate'] = set_value('check_fromdate');
            $check_fromdate = set_value('check_fromdate');
            if($check_fromdate == "True")
            {
                $data['fromdate'] = set_value('fromdate');
                $data['todate'] = set_value('todate');
            }
            else
            {
                $data['fromdate'] = $thedate;
                $data['todate'] = $thedate;
            }
            $data['check_customer_id'] = set_value('check_customer_id');
            $data['customer_id'] = set_value('customer_id');
            
            $this->load->model('M_war_types');
            $this->load->model('M_buy_bill');
            $this->load->model('M_buy_bill_items');
            $this->load->model('M_buy_returns');
            $this->load->model('M_buy_returns_items');
            $this->load->model('M_buy_manufacturer');
            $data['MultiRows'] = $this->M_war_types->FillList();
            $data['Search'] = "False";
            $details = $this->lang->line('Buy_Report5');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_supplier_sale_item', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function items_buy()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $data['fromdate'] = $thedate;
            $data['todate'] = $thedate;
            $data['type_id'] = 0;
            $data['check_type_id'] = "";
            $data['check_fromdate'] = "";
            
            $this->load->model('M_war_types');
            $this->load->model('M_buy_bill');
            $this->load->model('M_buy_bill_items');
            $this->load->model('M_buy_returns');
            $this->load->model('M_buy_returns_items');
            $this->load->model('M_buy_manufacturer');
            $data['MultiRows'] = $this->M_war_types->FillList();
            $data['Search'] = "False";
            $details = $this->lang->line('Sale_Report6');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_items_buy', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function items_buy_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->helper('security');
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("type_id","type_id","required");
            
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            }
            
            
            $data['check_fromdate'] = set_value('check_fromdate');
            $check_fromdate = set_value('check_fromdate');
            if($check_fromdate == "True")
            {
                $data['fromdate'] = set_value('fromdate');
                $data['todate'] = set_value('todate');
            }
            else
            {
                $data['fromdate'] = $thedate;
                $data['todate'] = $thedate;
            }
            $data['check_type_id'] = set_value('check_type_id');
            $check_type_id = set_value('check_type_id');
            if($check_type_id == "True")
            {
                $data['type_id'] = set_value('type_id');
                $type_id = set_value('type_id');
            }
            else
            {
                $data['type_id'] = 0;
                $type_id = 0;
            }
            
            $this->load->model('M_war_types');
            $this->load->model('M_buy_bill');
            $this->load->model('M_buy_bill_items');
            $this->load->model('M_buy_returns');
            $this->load->model('M_buy_returns_items');
            $this->load->model('M_buy_manufacturer');
            if($type_id == 0)
            {
                $data['MultiRows'] = $this->M_war_types->FillList();
            }
            else
            {
                $data['MultiRows'] = $this->M_war_types->FillOnlyOne($type_id);
            }
            
            $data['Search'] = "False";
            $details = $this->lang->line('Sale_Report6');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_items_buy', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function supplier_list()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->model('M_buy_manufacturer');
            $data['MultiRows'] = $this->M_buy_manufacturer->GetMultiRow();
            $data['Search'] = "False";
            $details = $this->lang->line('Buy_Report7');
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_supplier_list', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
    public function supplier_list_search()
    {
        ini_set("memory_limit","512M");
        ini_set('max_execution_time', 300);
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('MemberID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->helper('security');
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("search","search","required");
            
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            }
            else {
                $search = set_value('search');
            }
            $this->load->model('M_buy_manufacturer');
            $data['MultiRows'] = $this->M_buy_manufacturer->GetSearch($search);
            $data['Search'] = "True";
            $details = "عرض بيانات الموردين";
            $this->load->model("M_ser_activities");
            $this->M_ser_activities->InsertRecord($details);
            
            $this->load->view('report_supplier_list', $data);
        }
        else {
            $this->load->view('login', $data);
        }
    }
    
}
