<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Balance_sheet extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }
		$this->load->model('settings/M_fin_settings');
		$this->load->model('app/M_app_company');
	}  
	
	public function index()
	{
		$data['DataRows'] = $this->M_fin_treeaccount->GetMultiRow();
		$this->Create_PDF();
	}


	public function Create_PDF()
	{
		$Assets = $this->M_fin_treeaccount->GetByCategory(1);
		$Liabilities = $this->M_fin_treeaccount->GetByCategory(2);
		$StockholdersEquity = $this->M_fin_treeaccount->GetByCategory(3);

		$company_id = intval($this->session->userdata('company_id'));
		$DataRow = $this->M_app_company->GetRow($company_id);
		$this->session->set_userdata('header', $DataRow->header);
		$this->session->set_userdata('footer', $DataRow->footer);
		
		$HeaderImage = base_url()."upload/settings/".$this->session->userdata('header');
		$FooterImage = base_url()."upload/settings/".$this->session->userdata('footer');
		$Directionality = $this->session->userdata('Direction');
		$PageTitle = lang('balance_sheet');


		require_once 'vendor/autoload.php';
		$mpdf = new \Mpdf\Mpdf([
			'mode' => 'utf-8',
			//'format' => 'A4'.($orientation == 'L' ? '-L' : ''),
			//'orientation' => $orientation,
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 25,
			'margin_bottom' => 25,
			'margin_header' => 5,
			'margin_footer' => 5,
			]);
		$mpdf->allow_charset_conversion=true;
		//$mpdf->charset_in='cp1252';
		$mpdf->SetTitle($PageTitle);
		$mpdf->SetDirectionality($Directionality);
		$mpdf->autoScriptToLang = true;
		$mpdf->autoLangToFont = true;
		$mpdf->debug = true;
		//$mpdf->defaultPageNumStyle = 'arabic-indic';
		
		ob_end_clean();
		header("Content-Encoding: None", true);

		$mpdf->SetHTMLHeader('
		<div style="text-align: center; font-weight: bold;">
			<img src="'.$HeaderImage.'">
		</div>');

		$mpdf->SetHTMLFooter('
		<table width="100%" style="vertical-align: bottom; font-family: serif; 
			font-size: 8pt; color: #000000; font-weight: bold;">
			<tr>
				<td colspan="2"><img src="'.$FooterImage.'"></td>
			</tr>
			<tr>
				<td width="50%">{DATE j-m-Y}</td>
				<td width="50%" align="left" lang="en">{PAGENO}/{nbpg}</td>
			</tr>
		</table>');
		
		$html = '';
		$html .= '<div align="center" style="font-size:18px; margin-top: 30px;"><strong>'.$PageTitle.'</strong><br>'.date("Y-m-d").'</div><br>';
		$html .= '<div style="float:right; width:50%">';
			//الأصول
			$html .= '<table cellspacing="0" cellpadding="5" border="1" width="98%" dir="rtl">';
			$html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
			$html .= '<td width="20%"></td>';
			$html .= '<td width="20%"></td>';
			$html .= '<td width="60%"><strong style="font-size: 18px; font-weight: bold">'.$this->lang->line('balancesheet_Assets').'</strong></td>';
			$html .= '</tr>';
			$html .= '<tbody>';
			//الأصول الثابتة
			$html .= '<tr>';
			$html .= '<td width="20%"></td>';
			$html .= '<td width="20%"></td>';
			$html .= '<td width="60%"><strong style="font-size: 18px; font-weight: bold; color:#FF0000">'.$this->lang->line('fixed_assets').'</strong></td>';
			$html .= '</tr>';
			$fixed_assets_total = 0;
			$fixed_assets_debit = 0;
			$fixed_assets_creditor = 0;
			$acc_fixed_assets = intval($this->session->userdata('acc_fixed_assets'));
			$FixedAssetsAccounts = $this->M_fin_treeaccount->GetSubAccounts($acc_fixed_assets);
			foreach($FixedAssetsAccounts as $FixedAssetsAccounts_Row) {
				$fixed_assets_debit = 0;
				$fixed_assets_creditor = 0;
				$acc_fixed_assets_id = $FixedAssetsAccounts_Row->id;
				$startamount = $FixedAssetsAccounts_Row->startamount;
				$acc_fixed_assets_name = "";
				if ($this->session->userdata('lang') == "ar")
				{
					$acc_fixed_assets_name = $FixedAssetsAccounts_Row->title;
				}
				else {
					$acc_fixed_assets_name = $FixedAssetsAccounts_Row->title_en;
				}
				//
				$CheckJournal = $this->M_fin_journal->GetSum($acc_fixed_assets_id);
				if($CheckJournal > 0)
				{
					$Debit = $this->M_fin_journal->GetSum_Debit($acc_fixed_assets_id);
					$fixed_assets_debit += doubleval($Debit->debit);
					$Creditor = $this->M_fin_journal->GetSum_Creditor($acc_fixed_assets_id);
					$fixed_assets_creditor += doubleval($Creditor->creditor);
				}
				$Level2 = $this->M_fin_treeaccount->GetSubAccounts($acc_fixed_assets_id);
				foreach($Level2 as $Level2_Row) {
					$startamount += $Level2_Row->startamount;
					$Level2_ID = $Level2_Row->id;
					$CheckJournal = $this->M_fin_journal->GetSum($Level2_ID);
					if($CheckJournal > 0)
					{
						$Debit = $this->M_fin_journal->GetSum_Debit($Level2_ID);
						$fixed_assets_debit += doubleval($Debit->debit);
						$Creditor = $this->M_fin_journal->GetSum_Creditor($Level2_ID);
						$fixed_assets_creditor += doubleval($Creditor->creditor);
					}

					$Level3 = $this->M_fin_treeaccount->GetSubAccounts($Level2_ID);
					foreach($Level3 as $Level3_Row) {
						$startamount += $Level3_Row->startamount;
						$Level3_ID = $Level3_Row->id;
						$CheckJournal = $this->M_fin_journal->GetSum($Level3_ID);
						if($CheckJournal > 0)
						{
							$Debit = $this->M_fin_journal->GetSum_Debit($Level3_ID);
							$fixed_assets_debit += doubleval($Debit->debit);
							$Creditor = $this->M_fin_journal->GetSum_Creditor($Level3_ID);
							$fixed_assets_creditor += doubleval($Creditor->creditor);
						}
					}
				}
				$Final_fixed_asset_value = $startamount + $fixed_assets_debit - $fixed_assets_creditor;
				$fixed_assets_total += $Final_fixed_asset_value;
				$html .= '<tr>';
				$html .= '<td width="20%" align="center"></td>';
				$html .= '<td width="20%" align="center">'.$Final_fixed_asset_value.'</td>';
				$html .= '<td width="60%"><strong style="font-size: 16px;">&nbsp;&nbsp;&nbsp;'.$acc_fixed_assets_name.'</strong></td>';
				$html .= '</tr>';
			}
			$html .= '<tr>';
			$html .= '<td width="20%" align="center" style="font-weight: bold; color:#0000FF">'.$fixed_assets_total.'</td>';
			$html .= '<td width="20%" align="center"></td>';
			$html .= '<td width="60%"><strong style="font-size: 18px; font-weight: bold; color:#0000FF">'.$this->lang->line('fixed_assets_total').'</strong></td>';
			$html .= '</tr>';

			//الأصول المتداولة
			$html .= '<tr>';
			$html .= '<td width="20%"></td>';
			$html .= '<td width="20%"></td>';
			$html .= '<td width="60%"><strong style="font-size: 18px; font-weight: bold; color:#FF0000">'.$this->lang->line('current_assets').'</strong></td>';
			$html .= '</tr>';
			$Current_assets_total = 0;
			$Current_assets_debit = 0;
			$Current_assets_creditor = 0;
			$acc_current_assets = intval($this->session->userdata('acc_current_assets'));
			$CurrentAssetsAccounts = $this->M_fin_treeaccount->GetSubAccounts($acc_current_assets);
			foreach($CurrentAssetsAccounts as $CurrentAssetsAccounts_Row) {
				$Current_assets_debit = 0;
				$Current_assets_creditor = 0;
				$acc_current_assets_id = $CurrentAssetsAccounts_Row->id;
				$startamount = $CurrentAssetsAccounts_Row->startamount;
				$acc_current_assets_name = "";
				if ($this->session->userdata('lang') == "ar")
				{
					$acc_current_assets_name = $CurrentAssetsAccounts_Row->title;
				}
				else {
					$acc_current_assets_name = $CurrentAssetsAccounts_Row->title_en;
				}
				//
				$CheckJournal = $this->M_fin_journal->GetSum($acc_current_assets_id);
				if($CheckJournal > 0)
				{
					$Debit = $this->M_fin_journal->GetSum_Debit($acc_current_assets_id);
					$Current_assets_debit += doubleval($Debit->debit);
					$Creditor = $this->M_fin_journal->GetSum_Creditor($acc_current_assets_id);
					$Current_assets_creditor += doubleval($Creditor->creditor);
				}
				$Level2 = $this->M_fin_treeaccount->GetSubAccounts($acc_current_assets_id);
				foreach($Level2 as $Level2_Row) {
					$startamount += $Level2_Row->startamount;
					$Level2_ID = $Level2_Row->id;
					$CheckJournal = $this->M_fin_journal->GetSum($Level2_ID);
					if($CheckJournal > 0)
					{
						$Debit = $this->M_fin_journal->GetSum_Debit($Level2_ID);
						$Current_assets_debit += doubleval($Debit->debit);
						$Creditor = $this->M_fin_journal->GetSum_Creditor($Level2_ID);
						$Current_assets_creditor += doubleval($Creditor->creditor);
					}

					$Level3 = $this->M_fin_treeaccount->GetSubAccounts($Level2_ID);
					foreach($Level3 as $Level3_Row) {
						$startamount += $Level3_Row->startamount;
						$Level3_ID = $Level3_Row->id;
						$CheckJournal = $this->M_fin_journal->GetSum($Level3_ID);
						if($CheckJournal > 0)
						{
							$Debit = $this->M_fin_journal->GetSum_Debit($Level3_ID);
							$Current_assets_debit += doubleval($Debit->debit);
							$Creditor = $this->M_fin_journal->GetSum_Creditor($Level3_ID);
							$Current_assets_creditor += doubleval($Creditor->creditor);
						}
					}
				}
				$Final_Current_asset_value = $startamount + $Current_assets_debit - $Current_assets_creditor;
				$Current_assets_total += $Final_Current_asset_value;
				$html .= '<tr>';
				$html .= '<td width="20%" align="center"></td>';
				$html .= '<td width="20%" align="center">'.$Final_Current_asset_value.'</td>';
				$html .= '<td width="60%"><strong style="font-size: 16px;">&nbsp;&nbsp;&nbsp;'.$acc_current_assets_name.'</strong></td>';
				$html .= '</tr>';
			}
			$html .= '<tr>';
			$html .= '<td width="20%" align="center" style="font-weight: bold; color:#0000FF">'.$Current_assets_total.'</td>';
			$html .= '<td width="20%" align="center"></td>';
			$html .= '<td width="60%"><strong style="font-size: 18px; font-weight: bold; color:#0000FF">'.$this->lang->line('current_assets_total').'</strong></td>';
			$html .= '</tr>';

			$Total_Assets = $fixed_assets_total + $Current_assets_total;
			$html .= '<tr>';
			$html .= '<td width="20%" align="center" style="font-weight: bold; color:#FF0000">'.$Total_Assets.'</td>';
			$html .= '<td width="20%"></td>';
			$html .= '<td width="60%"><strong style="font-size: 18px; font-weight: bold; color:#FF0000">'.$this->lang->line('Total_Assets').'</strong></td>';
			$html .= '</tr>';

			$html .= '</tbody>';
			$html .= '</table>';
		$html .= '</div>';
		$html .= '<div style="float:left; width:50%">';
			//الخصوم
			$html .= '<table cellspacing="0" cellpadding="5" border="1" width="98%" dir="rtl">';
			$html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
			$html .= '<td width="20%"></td>';
			$html .= '<td width="20%"></td>';
			$html .= '<td width="60%"><strong style="font-size: 18px; font-weight: bold">'.$this->lang->line('fin_treecategory_2').'</strong></td>';
			$html .= '</tr>';
			$html .= '<tbody>';
			//رأس المال وحقوق الملكية
			$html .= '<tr>';
			$html .= '<td width="20%"></td>';
			$html .= '<td width="20%"></td>';
			$html .= '<td width="60%"><strong style="font-size: 18px; font-weight: bold; color:#FF0000">'.$this->lang->line('fin_treecategory_3').'</strong></td>';
			$html .= '</tr>';
			$OwnerEquity_total = 0;
			$OwnerEquity_debit = 0;
			$OwnerEquity_creditor = 0;

			$OwnerEquityAccounts = $this->M_fin_treeaccount->GetByCategory(3);
			foreach($OwnerEquityAccounts as $OwnerEquityAccounts_Row) {
				$OwnerEquity_debit = 0;
				$OwnerEquity_creditor = 0;
				$acc_OwnerEquity_id = $OwnerEquityAccounts_Row->id;
				$startamount = $OwnerEquityAccounts_Row->startamount;
				$acc_OwnerEquity_name = "";
				if ($this->session->userdata('lang') == "ar")
				{
					$acc_OwnerEquity_name = $OwnerEquityAccounts_Row->title;
				}
				else {
					$acc_OwnerEquity_name = $OwnerEquityAccounts_Row->title_en;
				}
				//
				$CheckJournal = $this->M_fin_journal->GetSum($acc_OwnerEquity_id);
				if($CheckJournal > 0)
				{
					$Debit = $this->M_fin_journal->GetSum_Debit($acc_OwnerEquity_id);
					$OwnerEquity_debit += doubleval($Debit->debit);
					$Creditor = $this->M_fin_journal->GetSum_Creditor($acc_OwnerEquity_id);
					$OwnerEquity_creditor += doubleval($Creditor->creditor);
				}
				$Level2 = $this->M_fin_treeaccount->GetSubAccounts($acc_OwnerEquity_id);
				foreach($Level2 as $Level2_Row) {
					$startamount += $Level2_Row->startamount;
					$Level2_ID = $Level2_Row->id;
					$CheckJournal = $this->M_fin_journal->GetSum($Level2_ID);
					if($CheckJournal > 0)
					{
						$Debit = $this->M_fin_journal->GetSum_Debit($Level2_ID);
						$OwnerEquity_debit += doubleval($Debit->debit);
						$Creditor = $this->M_fin_journal->GetSum_Creditor($Level2_ID);
						$OwnerEquity_creditor += doubleval($Creditor->creditor);
					}

					$Level3 = $this->M_fin_treeaccount->GetSubAccounts($Level2_ID);
					foreach($Level3 as $Level3_Row) {
						$startamount += $Level3_Row->startamount;
						$Level3_ID = $Level3_Row->id;
						$CheckJournal = $this->M_fin_journal->GetSum($Level3_ID);
						if($CheckJournal > 0)
						{
							$Debit = $this->M_fin_journal->GetSum_Debit($Level3_ID);
							$OwnerEquity_debit += doubleval($Debit->debit);
							$Creditor = $this->M_fin_journal->GetSum_Creditor($Level3_ID);
							$OwnerEquity_creditor += doubleval($Creditor->creditor);
						}
					}
				}
				$Final_OwnerEquity_value = $startamount - $OwnerEquity_debit + $OwnerEquity_creditor;
				$OwnerEquity_total += $Final_OwnerEquity_value;
				$html .= '<tr>';
				$html .= '<td width="20%" align="center"></td>';
				$html .= '<td width="20%" align="center">'.$Final_OwnerEquity_value.'</td>';
				$html .= '<td width="60%"><strong style="font-size: 16px;">&nbsp;&nbsp;&nbsp;'.$acc_OwnerEquity_name.'</strong></td>';
				$html .= '</tr>';
			}
			$html .= '<tr>';
			$html .= '<td width="20%" align="center" style="font-weight: bold; color:#0000FF">'.$OwnerEquity_total.'</td>';
			$html .= '<td width="20%" align="center"></td>';
			$html .= '<td width="60%"><strong style="font-size: 18px; font-weight: bold; color:#0000FF">'.$this->lang->line('OwnerEquity_total').'</strong></td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td width="20%"></td>';
			$html .= '<td width="20%"></td>';
			$html .= '<td width="60%"><strong style="font-size: 18px; font-weight: bold; color:#FF0000">'.$this->lang->line('balancesheet_Liabilities').'</strong></td>';
			$html .= '</tr>';
			$Liabilities_total = 0;
			$Liabilities_debit = 0;
			$Liabilities_creditor = 0;

			$LiabilitiesAccounts = $this->M_fin_treeaccount->GetByCategory(2);
			foreach($LiabilitiesAccounts as $LiabilitiesAccounts_Row) {
				$Liabilities_debit = 0;
				$Liabilities_creditor = 0;
				$acc_Liabilities_id = $LiabilitiesAccounts_Row->id;
				$basic = $LiabilitiesAccounts_Row->basic;
				
				$startamount = $LiabilitiesAccounts_Row->startamount;
				$acc_Liabilities_name = "";
				if ($this->session->userdata('lang') == "ar")
				{
					$acc_Liabilities_name = $LiabilitiesAccounts_Row->title;
				}
				else {
					$acc_Liabilities_name = $LiabilitiesAccounts_Row->title_en;
				}
				//
				$CheckJournal = $this->M_fin_journal->GetSum($acc_Liabilities_id);
				if($CheckJournal > 0)
				{
					$Debit = $this->M_fin_journal->GetSum_Debit($acc_Liabilities_id);
					$Liabilities_debit += doubleval($Debit->debit);
					$Creditor = $this->M_fin_journal->GetSum_Creditor($acc_Liabilities_id);
					$Liabilities_creditor += doubleval($Creditor->creditor);
				}
				
				$Final_Liabilities_value = $startamount - $Liabilities_debit + $Liabilities_creditor;
				$Liabilities_total += $Final_Liabilities_value;
				$html .= '<tr>';
				$html .= '<td width="20%" align="center"></td>';
				$html .= '<td width="20%" align="center">'.$Final_Liabilities_value.'</td>';
				$html .= '<td width="60%"><strong style="font-size: 16px;">&nbsp;&nbsp;&nbsp;'.$acc_Liabilities_name.'</strong></td>';
				$html .= '</tr>';
			}
			$html .= '<tr>';
			$html .= '<td width="20%" align="center">'.$Liabilities_total.'</td>';
			$html .= '<td width="20%" align="center"></td>';
			$html .= '<td width="60%"><strong style="font-size: 18px; font-weight: bold; color:#0000FF">'.$this->lang->line('Liabilities_total').'</strong></td>';
			$html .= '</tr>';
			$Total_Liabilities = $OwnerEquity_total + $Liabilities_total;
			$html .= '<tr>';
			$html .= '<td width="20%" align="center" style="font-weight: bold; color:#FF0000">'.$Total_Liabilities.'</td>';
			$html .= '<td width="20%"></td>';
			$html .= '<td width="60%"><strong style="font-size: 18px; font-weight: bold; color:#FF0000">'.$this->lang->line('Liabilities_total').'</strong></td>';
			$html .= '</tr>';
			$html .= '</tbody>';
			$html .= '</table>';
		$html .= '</div>';
		
		$html .= '<br><br><br>';

		$html = iconv("utf-8","UTF-8//IGNORE",$html);
		$mpdf->WriteHTML($html);

		

		
		$mpdf->Output();
		exit;
	}
}
