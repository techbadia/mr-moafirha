<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
 
class Balance_excel extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
                        
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {

			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}

		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }
		$this->load->model('settings/M_fin_settings');
		$this->load->model('app/M_app_company');
	}  

	public function index()
	{
		$data['DataRows'] = $this->M_fin_treeaccount->GetMultiRow();
		
		
		
		$this->outExcel();
	}


	public function outExcel()
	{
	    $this->load->model('settings/M_fin_settings');
$this->load->model('M_fin_treeaccount');
$this->load->model('app/M_app_company');
$Assets = $this->M_fin_treeaccount->GetByCategory(1);
$Liabilities = $this->M_fin_treeaccount->GetByCategory(2);
$StockholdersEquity = $this->M_fin_treeaccount->GetByCategory(3);

$company_id = intval($this->session->userdata('company_id'));
$DataRow = $this->M_app_company->GetRow($company_id);
$this->session->set_userdata('header', $DataRow->header);
$this->session->set_userdata('footer', $DataRow->footer);

$HeaderImage = base_url() . "upload/settings/" . $this->session->userdata('header');
$FooterImage = base_url() . "upload/settings/" . $this->session->userdata('footer');
$Directionality = $this->session->userdata('Direction');
$PageTitle = $this->lang->line('Trial_Balance');


require_once 'vendor2/autoload.php';
	
	$spreadsheet = new Spreadsheet();
 $sheet = $spreadsheet->getActiveSheet();
 
 $sheet->mergeCells("A1:C1");
 $sheet->setCellValue('A1', '');
 $sheet->mergeCells("D1:E1");
  $sheet->setCellValue('D1', $this->lang->line('Initial_Balance'));
 $sheet->mergeCells("F1:G1");
  $sheet->setCellValue('F1', $this->lang->line('period_movement'));
 $sheet->mergeCells("H1:I1");
  $sheet->setCellValue('H1', $this->lang->line('fin_journal_Total'));
 $sheet->mergeCells("J1:K1");
  $sheet->setCellValue('J1', $this->lang->line('Balance'));

//////////
 $sheet->setCellValue('A2', '#');
 $sheet->setCellValue('B2', $this->lang->line('fin_treeaccount_account_no'));
 $sheet->setCellValue('C2', $this->lang->line('fin_journal_account_id'));
 $sheet->setCellValue('D2', $this->lang->line('fin_journal_debit'));
 $sheet->setCellValue('E2', $this->lang->line('fin_journal_creditor'));
 $sheet->setCellValue('F2', $this->lang->line('fin_journal_debit'));
 $sheet->setCellValue('G2', $this->lang->line('fin_journal_creditor'));
 $sheet->setCellValue('H2', $this->lang->line('fin_journal_debit'));
 $sheet->setCellValue('I2', $this->lang->line('fin_journal_creditor'));
 $sheet->setCellValue('J2', $this->lang->line('fin_journal_debit'));
 $sheet->setCellValue('K2', $this->lang->line('fin_journal_creditor'));

$sum1 = $sum2 = $sum3 = $sum4 = $sum5 = $sum5 = $sum6 = $sum7 = $sum8 = 0;
$i=1;$row=3;
 $fromDT=$this->input->post("fromDT");
   $toDT=$this->input->post("toDT"); 
$acc_fixed_assets = intval($this->session->userdata('acc_fixed_assets'));
$FixedAssetsAccounts = $this->M_fin_treeaccount->GetMultiRow2();
 $fromDT=$this->input->post("fromDT");
   $toDT=$this->input->post("toDT");
foreach ($FixedAssetsAccounts as $CurrentAssetsAccounts_Row) {
    $query = $this->db->query("select IFNULL(sum(debit), 0) as sum_debit from fin_journal JOIN fin_journal_main ON fin_journal_main.id=fin_journal.main_id where account_id=$CurrentAssetsAccounts_Row->id AND  thedate BETWEEN '$fromDT' AND  '$toDT' ");
    $res = $query->result()['0'];
    $query2 = $this->db->query("select IFNULL(sum(creditor), 0) as sum_creditor from fin_journal  JOIN fin_journal_main ON fin_journal_main.id=fin_journal.main_id where account_id=$CurrentAssetsAccounts_Row->id AND  thedate BETWEEN '$fromDT' AND  '$toDT' ");
    $res2 = $query2->result()['0'];
       // if (($CurrentAssetsAccounts_Row->startamount != 0) || ($res->sum_debit != 0) || ($res2->sum_creditor != 0)) {
           
             $sheet->setCellValue('A'.$row, $i);
             $sheet->setCellValue('B'.$row, $CurrentAssetsAccounts_Row->account_no);
             if ($this->session->userdata('lang') == "ar") {
            $acc_current_assets_name = $CurrentAssetsAccounts_Row->title;
        } else {
            $acc_current_assets_name = $CurrentAssetsAccounts_Row->title_en;
        }
             $sheet->setCellValue('C'.$row, $acc_current_assets_name);
             $sheet->setCellValue('D'.$row, $CurrentAssetsAccounts_Row->startamount);
                     $sum1 += $CurrentAssetsAccounts_Row->startamount;
             $sheet->setCellValue('E'.$row, "0");
             $sum2 += 0;
             $sheet->setCellValue('F'.$row, $res->sum_debit);
             $sum3 += $res->sum_debit;
             $sheet->setCellValue('G'.$row, $res2->sum_creditor);
             $sum4 += $res2->sum_creditor;
             $num1 = $CurrentAssetsAccounts_Row->startamount + $res->sum_debit;
             $sheet->setCellValue('H'.$row, $num1);
             $sum5 += $num1;
             $sheet->setCellValue('I'.$row, $res2->sum_creditor);
            $sum6 += $res2->sum_creditor;
            
            $num2 = $CurrentAssetsAccounts_Row->startamount + $res->sum_debit;
        if (($num2 - $res2->sum_creditor) > 0) {
           $num3= $num2 - $res2->sum_creditor;
            $sum7 += $num3;
            $sheet->setCellValue('J'.$row, $num3);
             $sheet->setCellValue('K'.$row, '0');
             $sum8 += 0;
        } else {
            $num4=$res2->sum_creditor - $num2;
            $sheet->setCellValue('J'.$row, '0');
             $sheet->setCellValue('K'.$row, $num4);
            $sum7 += 0;
            $sum8 += $num4;
        }
        
             
             
             
             
          ++$i;++$row;   
  //   }
    }
    
     $sheet->mergeCells("A".$row.":C".$row);
            $sheet->setCellValue('A'.$row, $this->lang->line('fin_journal_Total'));
            $sheet->setCellValue('D'.$row, $sum1);
            $sheet->setCellValue('E'.$row, $sum2);
            $sheet->setCellValue('F'.$row, $sum3);
            $sheet->setCellValue('G'.$row, $sum4);
            $sheet->setCellValue('H'.$row, $sum5);
            $sheet->setCellValue('I'.$row, $sum6);
            $sheet->setCellValue('J'.$row, $sum7);
            $sheet->setCellValue('K'.$row, $sum8);

 $writer = new Xlsx($spreadsheet);
 
 header('Content-Type: application/vnd.ms-excel');
 header('Content-Disposition: attachment;filename="'. $PageTitle .'.xlsx"'); 
 header('Cache-Control: max-age=0');

 $writer->save('php://output'); 
	}




}
