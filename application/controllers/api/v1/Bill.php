<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'Base.php';

class Bill extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("account/M_fin_treeaccount");
        $this->load->model("store/M_store_quantity");
        $this->load->model("pos_sys/M_Tec_product_store_qty");
        $this->load->model('sales/M_sale_bill_items');

    }

    public function add()
    {
        $user = $this->GetUserByToken();

        $company_id = intval($user['0']['company_id']);
        $MemberID = intval($user['0']['usr_user_id']);
        $this->form_validation->set_rules("customer_id", "customer_id", "required");
        $this->form_validation->set_rules("tree_id", "tree_id", "required");
        if ($this->form_validation->run() == FALSE) {
            $data["data"] = array();
            $data["status"] = false;
            $data["message"] = array_values($this->form_validation->error_array())['0'];
            header('Content-Type: application/json');
            echo json_encode($data);
            exit();
        } else {

            $customer=$this->M_account->GetCustomerById(set_value("customer_id"));
            $customer_id = set_value("customer_id");
            $customer_name = $customer['ar_title'];
            $customer_phone = $customer['phone'];
            $project_id = 0;
            $user_id = $MemberID;
            $thedate = date('Y/m/d');
            $paid1 = set_value('paid1');
            $paid2 = set_value('paid2');
            $paid3 = 0;
            $paid=$paid1+$paid2+$paid3;

            $remaining = 0;
            $discount = 0;
            $acc_treasury = $this->M_account->fin_settings($user['0']['company_id']);
            $tax = $acc_treasury['0']['acc_tax_purchase_percent'];
            $notes = set_value('notes');
            $tree_id = set_value('tree_id');
            $salesperson_id = $MemberID ;
            $storekeeper = set_value('storekeeper');
            $tree_id1 = set_value('tree_id1');
            $totalvalue = set_value('totalvalue');
            $NewFileName = "";
            if ($totalvalue != $paid) {
                $data["data"] = array();
                $data["status"] = false;
                $data["message"] = "يجب دفع كل المبلغ";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit();

            }
            $NewData = array(
                'company_id' => $company_id,
                'customer_id' => $customer_id,
                'customer_name' => $customer_name,
                'customer_phone' => $customer_phone,
                'project_id' => $project_id,
                'user_id' => $user_id,
                'thedate' => $thedate,
                'totalvalue' => $totalvalue,
                'paid' => $paid,
                'remaining' => $remaining,
                'discount' => $discount,
                'tax' => $tax,
                'notes' => $notes,
                'image' => $NewFileName,
                'tree_id' => $tree_id,
                'tree_id1' => $tree_id1,
                'paid1' => $paid1,
                'paid2' => $paid2,
                'paid3' => $paid3,
            );

            $M_bill_id =$this->M_bill->InsertRecord($NewData);
            $bill_id = $M_bill_id;
            $details = $notes;
            $table_name = "sale_bill";
            $bond_no = $bill_id;


            $fin_journal_main_id=$this->fin_journal_main_insert($company_id,$MemberID,$details, $thedate, $bill_id, $table_name, $bond_no, $NewFileName);


            // من حساب العميل
            if ($project_id > 0) {
                $account_id = $project_id;
            } else {
                $account_id = $customer_id;
            }
             $debit = $totalvalue + $totalvalue * ($tax / 100);
            $creditor = 0;
            $this->fin_journal_insert($fin_journal_main_id,$account_id, $debit, $creditor);


            $account_id = $acc_treasury['0']['acc_sale'];
            $TaxAmount = $totalvalue * ($tax / 100);
            $debit = 0;
            $creditor = ($totalvalue - $TaxAmount);
            // إلى حساب المبيعات
            $this->fin_journal_insert($fin_journal_main_id,$account_id, $debit, $creditor);
            //قيد ضريبة المبيعات
            $account_id = intval($acc_treasury['0']['acc_tax_sale']);
            $debit = 0;
            $creditor = ($totalvalue / (100 + $tax) * 100) * ($tax / 100);
            $this->fin_journal_insert($fin_journal_main_id,$account_id, $debit, $creditor);
            if ($totalvalue == $paid) {
                if ($paid1 > 0) {
                    //المدفوع في حساب الخزينة
                    $account_id = $tree_id;
                    $debit = $paid1;
                    $creditor = 0;
                    $this->fin_journal_insert($fin_journal_main_id,$account_id, $debit, $creditor);

                }
                if ($paid2 > 0) {
                    //المدفوع في حساب البنك
                    $account_id = $tree_id1;
                    $debit = $paid2;
                    $creditor = 0;
                    $this->fin_journal_insert($fin_journal_main_id,$account_id, $debit, $creditor);

                }
                if ($paid3 > 0) {
                    // المسدد من الرصيد السابق للعميل أو من دفعته المقدمة
                    $account_id = $customer_id;
                    $debit = $paid3;
                    $creditor = 0;
                    $this->fin_journal_insert($fin_journal_main_id,$account_id, $debit, $creditor);

                }
            }

            // حساب عمولة مندوب المبيعات
            if ($salesperson_id > 0) {
                $SalesPersonData = $this->M_fin_treeaccount->GetRow($salesperson_id);
                header('Content-Type: application/json');

                if (is_array($SalesPersonData) &&count($SalesPersonData)){
                    $Commission = $SalesPersonData->commission;
                    $CommissionValue = $totalvalue * $Commission;
                    $account_id = $salesperson_id;
                    $debit = 0;
                    $creditor = $CommissionValue;
                    $details = $notes;
                    $table_name = "sale_bill";
                    $bond_no = $bill_id;
                    $image = $NewFileName;
                    $this->fin_journal_insert($fin_journal_main_id,$account_id, $debit, $creditor);

                }


            }


            //تحديث كميات الأصناف بالمخزن
            $InvoiceItems = intval($acc_treasury['0']['acc_invoice_items']);
            $barcode = "barcode";
            $location_id = "location_id";
            $quantity = "quantity";
            $received = "received";
            $MessageBody = "";
            for ($i = 1; $i <= $this->input->post('RowsCount'); $i++) {
                $quantity = "quantity" . $i;
                $received = "received" . $i;
                $quantity = $this->input->post($quantity);
                $received = $this->input->post($received);
                if ($received > 0) {
                    $product_id = $this->input->post("product" . $i);
                    $location_id = $user['0']['store_id'];
                    $prdct=$this->M_bill->GetProductByID($location_id,$product_id);
                    $unitprice = $prdct['0']['price'];

                    /******************/
                    $Old_FromStoreData = $this->M_store_quantity->GetStoreProduct($location_id, $product_id);
                    $Old_FromStore_Quantity = $Old_FromStoreData->quantity;
                    $Old_FromStore_id = $Old_FromStoreData->id;
                    $New_FromStore_Quantity = $Old_FromStore_Quantity - $received;
                    $FromStoreArrayData = array();
                    $FromStoreArrayData = array(
                        'store_id' => $location_id,
                        'product_id' => $product_id,
                        'quantity' => $New_FromStore_Quantity,
                    );
                    $this->M_store_quantity->UpdateRecord($Old_FromStore_id, $FromStoreArrayData);
                    $POS_Quantity_Data = array();
                    $POS_Quantity_Data = array(
                        'product_id' => $product_id,
                        'store_id' => $location_id,
                        'quantity' => $New_FromStore_Quantity,
                    );
                    $this->M_Tec_product_store_qty->UpdateRecord($Old_FromStore_id, $POS_Quantity_Data);
                    //M_sale_bill_items
                    $ItemsArray = array();
                    $ItemsArray = array(
                        'bill_id' => $bill_id,
                        'store_type_id' => $product_id,
                        'location_id' => $location_id,
                        'quantity' => $quantity,
                        'unitprice' => $unitprice,
                        'received' => $received,
                    );
                    $this->M_sale_bill_items->InsertRecord($ItemsArray);
                    $MessageBody .= "الباركود: " . $barcode . " - اسم الصنف: " . $prdct['0']['title'] . " - الكمية: " . $quantity . "<br><hr>";
                }
                if ($storekeeper > 0) {
                    $readed = 0;
                    $marked = 0;
                    $deleted = 0;
                    $thetime = date("h:i:s");
                    $draft = 0;
                    $NewData = array(
                        'company_id' => $company_id,
                        'from_user' => $user_id,
                        'to_user' => $storekeeper,
                        'subject' => "تجهيز طلبية",
                        'message' => $MessageBody,
                        'image' => "",
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                        'readed' => $readed,
                        'marked' => $marked,
                        'draft' => $draft,
                        'deleted' => $deleted,
                    );
                    $this->M_account->inset_Message($NewData);
                }
            }
        }
        $data["data"] = array();
        $data["status"] = true;
        $data["message"] = "ok";
        header('Content-Type: application/json');
        echo json_encode($data);
        exit();
    }

    public function treasury()
    {

        $user = $this->GetUserByToken();

        $acc_treasury = $this->M_account->fin_settings($user['0']['company_id']);
        $toaccount = $this->M_account->GetMain_Sub($acc_treasury['0']['acc_treasury'],$user['0']['company_id']);
        if (count($toaccount)) {
            $data["status"] = true;
            $data["message"] = "ok";
            $data['data'] = $toaccount;
        } else {
            $data["status"] = false;
            $data["message"] = "empty";
            $data['data'] = array();
        }
        header('Content-Type: application/json');
        echo json_encode($data);

    }
    public function bank()
    {

        $user = $this->GetUserByToken();

        $acc_treasury = $this->M_account->fin_settings($user['0']['company_id']);
        $toaccount = $this->M_account->GetMain_Sub($acc_treasury['0']['acc_bank'],$user['0']['company_id']);
        if (count($toaccount)) {
            $data["status"] = true;
            $data["message"] = "ok";
            $data['data'] = $toaccount;
        } else {
            $data["status"] = false;
            $data["message"] = "empty";
            $data['data'] = array();
        }
        header('Content-Type: application/json');
        echo json_encode($data);

    }

    public function fin_journal_main_insert($company_id,$user_id,$details, $thedate, $bill_id, $table_name, $bond_no, $image)
    {
        $automatic = 1;
        $deleted = 0;
        $fin_journal_array = array();
        $fin_journal_array = array(
            'company_id' => $company_id,
            'user_id' => $user_id,
            'details' => $details,
            'thedate' => $thedate,
            'bill_id' => $bill_id,
            'table_name' => $table_name,
            'bond_no' => $bond_no,
            'image' => $image,
            'automatic' => $automatic,
            'deleted' => $deleted,
        );
        return $this->M_bill->fin_journal_main_insert($fin_journal_array);
    }

    public function fin_journal_insert($main_id,$account_id, $debit, $creditor)
    {
        $fin_journal_array = array();
        $fin_journal_array = array(
            'main_id' => $main_id,
            'account_id' => $account_id,
            'debit' => $debit,
            'creditor' => $creditor,
        );
        $this->M_bill->fin_journal_insert($fin_journal_array);
    }
}