<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    var $data = array();
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Riyadh');
        $this->config->load('settings/config_setting');
        $this->load->model("settings/M_settings_acceptance");
        $this->load->model("settings/M_settings_notifications");
        $this->load->model("api/M_account");
    }

    public function index()
    {

        $this->load->helper('cookie');
        $this->config->load('settings/config_setting');
        if (set_value('lang')!= null){
            $lang=set_value('lang');
        }else{
            $lang='ar';
        }

        if ( $lang=='ar')
        {
            $this->lang->load('mywords', 'arabic');
            $this->lang->load('form_validation_lang', 'arabic');
        }
        else
        {
            $Config_File_Lang = "_en";
            $this->lang->load('mywords', 'english');
            $this->lang->load('form_validation_lang', 'english');

        }
        $data["data"] =$lang;
        $this->form_validation->set_rules("username","username","required");
        $this->form_validation->set_rules("password","password","required");

        if ($this->form_validation->run() == FALSE) {
            $data["data"] = array();
            $data["status"] = false;
            $data["message"] = array_values($this->form_validation->error_array())['0'];

        }
        else
        {
            $username = set_value('username');
            $password = md5(set_value("password"));
            $UserRowNum = $this->M_usr_users->GetUserRow($username, $password);
            if ($UserRowNum >0)
            {
                $UserData = $this->M_usr_users->CheckUser($username, $password);
                $user['id']=$UserData->id;
                $user['fullname']=$UserData->fullname;
                $user['email']=$UserData->email;
                $user['username']=$UserData->username;
                $user['image']=base_url('upload/usr_users/'.$UserData->image);
                $token=$this->token($UserData->id,$UserData->username,$UserData->fullname);
                $user['token']=$token;
                $data["data"] =$user;
                $data["status"] = true;
                $data["message"] ='OK';


                $dt=array(
                    'usr_user_id'=>$UserData->id,
                    'token'=>$token,
                    'start_dt'=>date('Y/m/d H:i:s'),
                );
                $this->M_account->inset_token($dt);

            }
            else
            {
                if($lang=='ar'){
                    $data["message"] = "اسم المستخدم أو كلمة المرور غير صحيحة";
                }else{
                    $data["message"] = "Incorrect username or password";
                }
                $data["data"] = array();
                $data["status"] = false;

            }

        }

        header('Content-Type: application/json');
        echo json_encode($data);
    }


    public function token($user_id,$username,$fullname){
        $jwt = new JWT();
        $key='65JKmYHfDqALWEfgD8Eg';
        $alg = "HS256";
        $user=array(
            'user_id'=>$user_id,
            'username'=>$username,
            'fullname'=>$fullname,
            'date'=>date('Y/m/d H:i:s')
        );
        $token=$jwt->encode($user,$key,$alg);
        return $token;


    }


}
