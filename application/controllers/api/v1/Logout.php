<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Base.php';
class Logout extends Base {

    var $data = array();
    public function __construct() {
        parent::__construct();
    }

    public function index()
    {

        $user=$this->GetUserByToken();
        
        $this->M_account->logout($user['0']['usr_token_id']);
        $data["status"] = true;
        
        if($this->lang=='ar'){
            $data["message"] = "تم تسجيل الخروج";
            
        }else{
            $data["message"] = "Logout successfully";
        }
        $data['data'] =array();
        header('Content-Type: application/json');
        echo json_encode($data);
    }


}
