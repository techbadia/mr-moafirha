<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'Base.php';
class Users extends Base
{

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $user=$this->GetUserByToken();
        
        $data["status"] = true;
        $data["message"] = "ok";
        $data['data'] = $this->M_account->GetUsers($user['0']['usr_user_id']);
        header('Content-Type: application/json');
        echo json_encode($data);

    }


}