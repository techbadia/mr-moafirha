<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'Base.php';

class Message extends Base
{

    public function __construct()
    {
        parent::__construct();

    }

    public function sent()
    {
        $user = $this->GetUserByToken();
        $sent = $this->M_account->MessageSent($user['0']['usr_user_id']);
        if (count($sent)) {
            $i = 0;
            foreach ($sent as $item) {
                $msg['from_user'] = $item['from_user'];
                $msg['to_user'] = $item['to_user'];
                $msg['to_user_name'] = $item['fullname'];
                $msg['subject'] = $item['subject'];
                $msg['message'] = $item['message'];
                $msg['date'] = $item['thedate'];
                $msg['time'] = $item['thetime'];
                if (empty($item['image']) || $item['image'] == null || $item['image'] == '') {
                    $msg['image'] = null;
                } else {
                    $msg['image'] = base_url('/upload/serv_message/' . $item['image']);
                }
                $msgs[] = $msg;
            }

            $data["status"] = true;
            $data["message"] = "ok";
            $data['data'] = $msgs;

        } else {
            $data["status"] = false;
            $data["message"] = "empty";
            $data['data'] = array();
        }

        header('Content-Type: application/json');
        echo json_encode($data);

    }


    public function receive()
    {
        $user = $this->GetUserByToken();
        $sent = $this->M_account->MessageReceive($user['0']['usr_user_id']);
        if (count($sent)) {
            $i = 0;
            foreach ($sent as $item) {
                $msg['from_user'] = $item['from_user'];
                $msg['to_user'] = $item['to_user'];
                $msg['from_user_name'] = $item['fullname'];
                $msg['subject'] = $item['subject'];
                $msg['message'] = $item['message'];
                $msg['date'] = $item['thedate'];
                $msg['time'] = $item['thetime'];
                if (empty($item['image']) || $item['image'] == null || $item['image'] == '') {
                    $msg['image'] = null;
                } else {
                    $msg['image'] = base_url('/upload/serv_message/' . $item['image']);
                }

                $msgs[] = $msg;
            }

            $data["status"] = true;
            $data["message"] = "ok";
            $data['data'] = $msgs;

        } else {
            $data["status"] = false;
            $data["message"] = "empty";
            $data['data'] = array();
        }

        header('Content-Type: application/json');
        echo json_encode($data);

    }

    public function new_message()
    {
        $user = $this->GetUserByToken();

        $this->form_validation->set_rules("subject", "subject", "required");

        if ($this->form_validation->run() == FALSE) {
            $data["data"] = array();
            $data["status"] = false;
            $data["message"] = array_values($this->form_validation->error_array())['0'];
            header('Content-Type: application/json');
            echo json_encode($data);
            exit();
        } else {
            $company_id = intval($user['0']['company_id']);
            $from_user = intval($user['0']['usr_user_id']);
            $subject = set_value('subject');
            $message = set_value('message');
            $to_user = set_value('to_user');
            $thedate = date("Y-m-d");
            $thetime = date("h:i:s");
            $readed = 0;
            $marked = 0;
            $deleted = 0;
            if (!empty($_FILES)) {
                $filename = date("Y") . "-" . date("m") . "-" . date("d") . "-" . date("h") . "-" . date("i") . "-" . date("s");
                $exts = explode(".", $_FILES['image']['name']);
                $exts = end($exts);
                $exts = strtolower($exts);
                $UploadPath = "./upload/serv_message/";
                $ImageDirectory = $UploadPath;

                if (!is_dir($ImageDirectory)) {
                    mkdir($ImageDirectory, 0777, TRUE);

                }

                $config['upload_path'] = $ImageDirectory;
                $config['allowed_types'] = "*";
                $config['file_name'] = $filename . "." . $exts;
                $config['overwrite'] = TRUE;
                $config['file_ext_tolower'] = TRUE;
                $config['remove_spaces'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $NewFileName = $filename . "." . $exts;
                if (!$this->upload->do_upload('image', $NewFileName)) {
                    $data["data"] = array();
                    $data["status"] = false;
                    $data["message"] = array_values($this->upload->display_errors());
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    exit();

                } else {
                    $this->upload->do_upload('image', $NewFileName);
                }
            }else{
                $NewFileName='';
            }

            $draft = 0;
                $NewData = array(
                    'company_id' => $company_id,
                    'from_user' => $from_user,
                    'to_user' => $to_user,
                    'subject' => $subject,
                    'message' => $message,
                    'image' => $NewFileName,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                    'readed' => $readed,
                    'marked' => $marked,
                    'draft' => $draft,
                    'deleted' => $deleted,
                );

                $this->M_account->InsertRecord($NewData);

            $this->load->library('user_agent');
            $data["data"] = array();
            $data["status"] = true;
            $data["message"] = "ok";
            header('Content-Type: application/json');
            echo json_encode($data);
        }

    }
}