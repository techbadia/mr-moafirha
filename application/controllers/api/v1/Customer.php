<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'Base.php';
class Customer extends Base
{

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $this->GetUserByToken();
        
        $data["status"] = true;
        $data["message"] = "ok";
        $data['data'] = $this->M_account->GetCustomer();
        header('Content-Type: application/json');
        echo json_encode($data);

    }


}