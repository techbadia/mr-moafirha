<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'Base.php';
class Product extends Base
{
    public function __construct()
    {
        parent::__construct();

    }
    public function all()
    {
        $user=$this->GetUserByToken();
        $products=$this->M_bill->GetAllProductByStore($user['0']['store_id']);
        if (count($products)){
            $data["status"] = true;
            $data["message"] = "ok";
            $data['data'] = $products;
        }else{
            $data["status"] = false;
            $data["message"] = "empty";
            $data['data'] = array();
        }
        header('Content-Type: application/json');
        echo json_encode($data);

    }
    public function one()
    {
        $barcode=set_value('barcode');
        $user=$this->GetUserByToken();
        $product=$this->M_bill->GetProductByBarcode($user['0']['store_id'],$barcode);
        if (count($product)){
            $data["status"] = true;
            $data["message"] = "ok";
            $data['data'] = $product['0'];
        }else{
            $data["status"] = false;
            $data["message"] = "empty";
            $data['data'] = array();
        }
        header('Content-Type: application/json');
        echo json_encode($data);

    }
}