<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Base extends CI_Controller
{

    var $data = array();
    var $lng;

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Riyadh');
        $this->config->load('settings/config_setting');
        $this->load->model("settings/M_settings_acceptance");
        $this->load->model("settings/M_settings_notifications");
        $this->load->model("api/M_account");
        $this->load->model("api/M_bill");
        $this->load->helper('cookie');

        if (set_value('lang') != null) {
            $this->lng = set_value('lang');
        } else {
            $this->lng = 'ar';
        }

        if ( $this->lng=='ar')
        {
            $this->lang->load('mywords', 'arabic');
            $this->lang->load('form_validation_lang', 'arabic');
        }
        else
        {
            $this->lang->load('mywords', 'english');
            $this->lang->load('form_validation_lang', 'english');

        }

    }

    public function GetUserByToken()
    {

        $token = null;
        if (isset($_SERVER["HTTP_AUTHORIZATION"])) {
            $token = $_SERVER["HTTP_AUTHORIZATION"];
            $data = $this->M_account->GetUserByToken($token);
            if (count($data)) {
                return $data;

            } else {
                if ($this->lang == 'ar') {
                    $data["message"] = "غير مصرح";
                } else {
                    $data["message"] = "Unauthorized";
                }
                $data["data"] = array();
                $data["status"] = false;
                header('WWW-Authenticate: Basic realm="Test Authentication System"');
                header('HTTP/1.0 401 Unauthorized');
                header('Content-Type: application/json');
                echo json_encode($data);
                exit();
            }
        } else {
            if ($this->lang == 'ar') {
                $data["message"] = "غير مصرح";
            } else {
                $data["message"] = "Unauthorized";
            }
            $data["data"] = array();
            $data["status"] = false;
            header('WWW-Authenticate: Basic realm="Test Authentication System"');
            header('HTTP/1.0 401 Unauthorized');
            header('Content-Type: application/json');
            echo json_encode($data);
            exit();
        }


    }


}