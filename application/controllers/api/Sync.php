<?php
defined('BASEPATH') or exit('No direct script access allowed');
header('Access-Control-Allow-Origin: https://crm.2030saudi.com');
header("Access-Control-Allow-Methods: GET, OPTIONS");
class Sync extends CI_Controller
{

    var $data = array();
    var $lng;

    public function __construct()
    {
        parent::__construct();
        $company_id = intval($this->session->userdata('company_id'));

    }
    public function addErpInvoice()
    {
      $this->load->model("project/M_proj_project");
      $this->load->model("sales/M_sale_customer");
      $this->load->model('sales/M_sale_bill');
      $this->load->model('sales/M_sale_bill_items');
      $this->load->model("account/M_fin_treeaccount");
      $this->load->model("account/M_fin_journal");
      $this->load->model("store/M_store");
      $this->load->model("store/M_product");
      $this->load->model("store/M_store_quantity");
      $this->load->model("service/M_serv_message");
      $this->load->model("pos_sys/M_Tec_product_store_qty");
      $id = $_POST['id'];
      $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      $otherdbErb = $this->load->database('otherdbErb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      $otherdb->where("id",$id);
      $invoice = $otherdb->get('invoices')->row();
      $otherdb->where("id",$invoice->client_id);
      $crmUser = $otherdb->get('users')->row();
      $otherdb->where("invoice_id",$id);
      $crmItems = $otherdb->get('invoice_items')->result();
      $otherdbErb->where("email",$crmUser->email);
      $erbUser = $otherdbErb->get('sale_customer')->row();
            // $id = set_value('id');
            // $company_id = set_value("company_id");
            $customer_id = $erbUser->id;
            // $customer_name = set_value("customer_name");
            // $customer_phone = set_value("customer_phone");
            // $project_id = set_value("project_id");
            // $user_id = $MemberID;
            $thedate = $invoice->issue_date;
            $totalvalue = $invoice->total;
            $discount = $invoice->discount;
            $notes = $invoice->note;
            // $totalvalue = set_value('totalvalue');
            // $paid = set_value('paid');
            // $remaining = set_value('remaining');
            // $discount = set_value('discount');
            $tax = "15";
            // $notes = set_value('notes');
            // $tree_id = set_value('tree_id');
            // $salesperson_id = set_value('salesperson_id');
            // $storekeeper = set_value('storekeeper');
            // $tree_id1 = set_value('tree_id1');
            // $paid1 = set_value('paid1');
            // $paid2 = set_value('paid2');
            // $paid3 = set_value('paid3');

            $NewData = array();
            $NewData = array(
                // 'company_id' => $company_id,
                'customer_id' => $customer_id,
                // 'customer_name' => $customer_name,
                // 'customer_phone' => $customer_phone,
                // 'project_id' => $project_id,
                // 'user_id' => $user_id,
                'thedate' => $thedate,
                'totalvalue' => $totalvalue,
                'paid' => 0,
                'remaining' => $totalvalue,
                'discount' => $discount,
                'tax' => $tax,
                'notes' => $notes,
                // 'image' => $NewFileName,
                'tree_id' => 295,
                'tree_id1' => 291,
                'paid1' => 0,
                'paid2' => 0,
                // 'paid3' => $paid3,
            );
            //echo "<pre>";print_r($_POST);die("</pre>");
          // $bill_id =  $this->M_sale_bill->InsertRecord($NewData);
          $otherdbErb->insert('sale_bill', $NewData);
          $bill_id =  $otherdbErb->insert_id();
          // echo $bill_id;die();
            // $LatestRecord = $this->M_sale_bill->GetLatestRecord($company_id, $customer_id, $user_id);
            $bill_id = $otherdbErb->insert_id();
            $details = $notes;
            $table_name = "sale_bill";
            $bond_no = $bill_id;
            $image = $NewFileName;
            $TaxAmount = $totalvalue * ($tax / 100);
            $this->fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $NewFileName);
            if ($totalvalue == $paid) {
                if ($paid1 > 0) {
                    //المدفوع في حساب الخزينة
                    $account_id = $tree_id;
                    $debit = $paid1;
                    $creditor = 0;
                    $this->fin_journal_insert($account_id, $debit, $creditor);
                }
                if ($paid2 > 0) {
                    //المدفوع في حساب البنك
                    $account_id = $tree_id1;
                    $debit = $paid2;
                    $creditor = 0;
                    $this->fin_journal_insert($account_id, $debit, $creditor);
                }
                if ($paid3 > 0) {
                    // المسدد من الرصيد السابق للعميل أو من دفعته المقدمة
                    $account_id = $customer_id;
                    $debit = $paid3;
                    $creditor = 0;
                    $this->fin_journal_insert($account_id, $debit, $creditor);
                }
            } else {
                if ($paid > 0) {
                    if ($paid1 > 0) {
                        //المدفوع في حساب الخزينة
                        $account_id = $tree_id;
                        $debit = $paid1;// - $remaining;
                        $creditor = 0;
                        $this->fin_journal_insert($account_id, $debit, $creditor);
                    }
                    if ($paid2 > 0) {
                        //المدفوع في حساب البنك
                        $account_id = $tree_id1;
                        $debit = $paid2;
                        $creditor = 0;
                        $this->fin_journal_insert($account_id, $debit, $creditor);
                    }
                    if ($paid3 > 0) {
                        // المسدد من الرصيد السابق للعميل أو من دفعته المقدمة
                        $account_id = $customer_id;
                        $debit = $paid3;
                        $creditor = 0;
                        $this->fin_journal_insert($account_id, $debit, $creditor);
                    }
                    // من حساب العميل
                    if ($project_id > 0) {
                        $account_id = $project_id;
                    } else {
                        $account_id = $customer_id;
                    }
                    $debit = $remaining;
                    $creditor = 0;
                    $this->fin_journal_insert($account_id, $debit, $creditor);
                } else {
                    // من حساب العميل
                    if ($project_id > 0) {
                        $account_id = $project_id;
                    } else {
                        $account_id = $customer_id;
                    }
                    $account_id = $customer_id;
                    $debit = $totalvalue;
                    $creditor = 0;
                    $this->fin_journal_insert($account_id, $debit, $creditor);
                }
            }
            $account_id = intval($this->session->userdata('acc_sale'));
            $subtotal1 = set_value('subtotal1');
             $TaxAmount = $subtotal1 * ($tax / 100);
            $debit = 0;
            $creditor = ($totalvalue - $TaxAmount);
            $details = $notes;
            $table_name = "sale_bill";
            $bond_no = $bill_id;
            $image = $NewFileName;
            // إلى حساب المبيعات
            $this->fin_journal_insert($account_id, $debit, $creditor);
            //قيد ضريبة المبيعات
            $account_id = intval($this->session->userdata('acc_tax_sale'));
            $debit = 0;
            $creditor =($totalvalue/(100+$tax)*100) * ($tax / 100);
            $details = $notes;
            $table_name = "sale_bill";
            $bond_no = $bill_id;
            $image = $NewFileName;
            $this->fin_journal_insert($account_id, $debit, $creditor);
            // حساب عمولة مندوب المبيعات
            if ($salesperson_id > 0) {
                $SalesPersonData = $this->M_fin_treeaccount->GetRow($salesperson_id);
                $Commission = $SalesPersonData->commission;
                $CommissionValue = $totalvalue * $Commission;
                $account_id = $salesperson_id;
                $debit = 0;
                $creditor = $CommissionValue;
                $details = $notes;
                $table_name = "sale_bill";
                $bond_no = $bill_id;
                $image = $NewFileName;
                $this->fin_journal_insert($account_id, $debit, $creditor);
            }
            ///////////////////////////
            //تحديث كميات الأصناف بالمخزن
            // $InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
            //M_product
            $barcode = "barcode";
            $location_id = "location_id";
            $quantity = "quantity";
            $received = "received";
            $MessageBody = "";
            foreach ($crmItems as $item) {
                $quantity = $item->quantity;

                $received = 1;
                // $quantity = $this->input->post($quantity);
                // $received = $this->input->post($received);
                if ($received > 0) {
                    // $barcode = "barcode" . $i;
                    //echo $barcode;
                    // $barcode = $this->input->post($barcode);
                    $ProductData = $this->M_product->GetByTitle($item->item_name);
                    //var_dump($ProductData);
                    $product_id = $ProductData->id;
                    // $location_id = "location_id" . $i;
                    // $location_id = $this->input->post($location_id);
                    $unitprice = $ProductData->cost_price;
                    // echo json_encode($unitprice);
                    // $unitprice = $this->input->post($unitprice);
                    // $Old_FromStoreData = $this->M_store_quantity->GetStoreProduct($location_id, $product_id);
                    // $Old_FromStore_Quantity = $Old_FromStoreData->quantity;
                    // $Old_FromStore_id = $Old_FromStoreData->id;
                    // $New_FromStore_Quantity = $Old_FromStore_Quantity - $received;
                    // $FromStoreArrayData = array();
                    // $FromStoreArrayData = array(
                    //     'store_id' => $location_id,
                    //     'product_id' => $product_id,
                    //     'quantity' => $New_FromStore_Quantity,
                    // );
                    // $this->M_store_quantity->UpdateRecord($Old_FromStore_id, $FromStoreArrayData);
                    // $POS_Quantity_Data = array();
                    // $POS_Quantity_Data = array(
                    //     'product_id' => $product_id,
                    //     'store_id' => $location_id,
                    //     'quantity' => $New_FromStore_Quantity,
                    // );
                    // $this->M_Tec_product_store_qty->UpdateRecord($Old_FromStore_id, $POS_Quantity_Data);
                    //M_sale_bill_items
                    $ItemsArray = array();
                    $ItemsArray = array(
                        'bill_id' => $bill_id,
                        'store_type_id' => $product_id,
                        // 'location_id' => $location_id,
                        'quantity' => $quantity,
                        'unitprice' => $unitprice,
                        'received' => $received,
                    );
                    $this->M_sale_bill_items->InsertRecord($ItemsArray);
                    $MessageBody .= "الباركود: " . $barcode . " - اسم الصنف: " . $ProductData->title . " - الكمية: " . $quantity . "<br><hr>";
                }
                if ($storekeeper > 0) {
                    $readed = 0;
                    $marked = 0;
                    $deleted = 0;
                    $thetime = date("h:i:s");
                    $draft = 0;
                    $NewData = array(
                        'company_id' => $company_id,
                        'from_user' => $user_id,
                        'to_user' => $storekeeper,
                        'subject' => "تجهيز طلبية",
                        'message' => $MessageBody,
                        'image' => "",
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                        'readed' => $readed,
                        'marked' => $marked,
                        'draft' => $draft,
                        'deleted' => $deleted,
                    );
                    $this->M_serv_message->InsertRecord($NewData);
                }
            }
            echo json_encode("here");
            exit();

    }
    public function fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $image)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $user_id = intval($this->session->userdata('StaffID'));
        $automatic = 1;
        $deleted = 0;
        $fin_journal_array = array();
        $fin_journal_array = array(
            'company_id' => $company_id,
            'user_id' => $user_id,
            'details' => $details,
            'thedate' => $thedate,
            'bill_id' => $bill_id,
            'table_name' => $table_name,
            'bond_no' => $bond_no,
            'image' => $image,
            'automatic' => $automatic,
            'deleted' => $deleted,
        );
        $this->M_fin_journal_main->InsertRecord($fin_journal_array);
    }
    public function fin_journal_insert($account_id, $debit, $creditor)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $user_id = intval($this->session->userdata('StaffID'));
        $MainData = $this->M_fin_journal_main->GetLatestRecord($company_id, $user_id);
        $main_id = $MainData->id;
        $fin_journal_array = array();
        $fin_journal_array = array(
            'main_id' => $main_id,
            'account_id' => $account_id,
            'debit' => $debit,
            'creditor' => $creditor,
        );
        $this->M_fin_journal->InsertRecord($fin_journal_array);
    }
    public function syncClients2()
    {
      $token = $_POST["_token"];
      if(empty($token)) return false;
      header('Content-Type: application/json');
      $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      $otherdbErb = $this->load->database('otherdbErb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      $query = $otherdb->get('users')->result();
      $this->load->model("sales/M_sale_customer");
      foreach ($query as $single) {
        $otherdb->where("user_id",$single->id);
        $role = $otherdb->get("role_user");
        $role = $role->row();
        if($role->role_id == 3){
        $data = array();
        $data["ar_title"] = $single->name;
        $data["en_title"] = $single->name;
        $data["phone"] = $single->mobile;
        $data["mobile1"] = $single->mobile;
        $data["mobile2"] = $single->mobile;
        $otherdb->where("user_id",$single->id);
        $cd = $otherdb->get("client_details");
        $cd = $cd->row();
        $data["address"] = $cd->address;

        $otherdbErb->where("email",$single->email);
        $erbUser = $otherdbErb->get("sale_customer");
        $user = $erbUser->row();
        if(!empty($user))
        {
          // var_dump($user->id);die();
          $otherdbErb->where('id', $user->id);
          $otherdbErb->update('sale_customer', $data);
        }else {
          $data["email"] = $single->email;

          $otherdbErb->insert('sale_customer', $data);

        }
        }
      }
      $erbUsers = $otherdbErb->get("sale_customer");
      $users = $erbUsers->result();
      foreach ($users as $user) {
        $data = array();
        $data["name"] = $user->ar_title;
        $data["mobile"] = $user->phone;
        $otherdb->where("email",$user->email);
        $crmUser = $otherdb->get("users");

        $crmUser = $crmUser->row();
        if(!empty($crmUser))
        {
          // var_dump($crmUser->id);die();
          $otherdb->where('id', $crmUser->id);
          $otherdb->update('users', $data);
        }else {
          $data["email"] = $user->email;

          $otherdb->insert('users', $data);
          $insert_id = $otherdb->insert_id();
          $details['user_id'] = $insert_id;
          $details['company_name'] = $user->ar_title;
          $details['address'] = $user->address;
          $otherdb->insert('client_details', $details);
          $detailss['user_id'] = $insert_id;
          $detailss['role_id'] = 3;
          $otherdb->insert('role_user', $detailss);

        }

      }
      echo json_encode("here");
      exit();
    }
    public function syncClients()
    {
      $token = $_POST["_token"];
      if(empty($token)) return false;
      header('Content-Type: application/json');
      $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      $otherdbErb = $this->load->database('otherdbErb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

      $erbUsers = $otherdbErb->get("sale_customer");
      $users = $erbUsers->result();
      foreach ($users as $user) {
        $data = array();
        $data["name"] = $user->ar_title;
        $data["mobile"] = $user->phone;
        $otherdb->where("email",$user->email);
        $crmUser = $otherdb->get("users");

        $crmUser = $crmUser->row();
        if(!empty($crmUser))
        {
          // var_dump($crmUser->id);die();
          $otherdb->where('id', $crmUser->id);
          $otherdb->update('users', $data);
        }else {
          $data["email"] = $user->email;

          $otherdb->insert('users', $data);
          $insert_id = $otherdb->insert_id();
          $details['user_id'] = $insert_id;
          $details['company_name'] = $user->ar_title;
          $details['address'] = $user->address;

          $otherdb->insert('client_details', $details);
          $detailss['user_id'] = $insert_id;
          $detailss['role_id'] = 3;
          $otherdb->insert('role_user', $detailss);

        }
        $query = $otherdb->get('users')->result();
        $this->load->model("sales/M_sale_customer");
        foreach ($query as $single) {
          $otherdb->where("user_id",$single->id);
          $role = $otherdb->get("role_user");
          $role = $role->row();
          if($role->role_id == 3){
          $data = array();
          $data["ar_title"] = $single->name;
          $data["en_title"] = $single->name;
          $data["phone"] = $single->mobile;
          $data["mobile1"] = $single->mobile;
          $data["mobile2"] = $single->mobile;
          $otherdb->where("user_id",$single->id);
          $cd = $otherdb->get("client_details");
          $cd = $cd->row();
          $data["address"] = $cd->address;

          $otherdbErb->where("email",$single->email);
          $erbUser = $otherdbErb->get("sale_customer");
          $user = $erbUser->row();
          if(!empty($user))
          {
            // var_dump($user->id);die();
            $otherdbErb->where('id', $user->id);
            $otherdbErb->update('sale_customer', $data);
          }else {
            $data["email"] = $single->email;

            $otherdbErb->insert('sale_customer', $data);

          }
          }
        }
      }
      echo json_encode("here");
      exit();
    }
    public function syncEmployees()
    {
      $token = $_POST["_token"];
      if(empty($token)) return false;
      header('Content-Type: application/json');
      $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      $otherdbErb = $this->load->database('otherdbErb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

      $erbEmps = $otherdbErb->get("hr_employee");
      $emps = $erbEmps->result();
      foreach ($emps as $emp) {
        $data = array();
        $data["name"] = $emp->ar_name;
        $data["mobile"] = $emp->phone;


        $data["country_id"] = $emp->country_id;

        // $otherdbErb->where("id",$emp->job_id);
        // $erbJob = $otherdbErb->get("hr_job");
        // $erbJob = $erbJob->row();
        //
        // $otherdb->where("name",$erbJob->ar_title);
        // $crmjob = $otherdb->get("designations");
        // $crmjob = $crmjob->row();

        $otherdb->where("email",$emp->email);
        $crmEmp = $otherdb->get("users");
        $crmEmp = $crmEmp->row();

        if(!empty($crmEmp))
        {
          // var_dump($crmUser->id);die();
          $otherdb->where('id', $crmEmp->id);
          $otherdb->update('users', $data);
        }else {
          $data["email"] = $emp->email;

          $otherdb->insert('users', $data);
          $insert_id = $otherdb->insert_id();
          $otherdbErb->where("id",$emp->job_id);
          $erbJob = $otherdbErb->get("hr_job");
          $erbJob = $erbJob->row();

          $otherdb->where("name",$erbJob->ar_title);
          $crmjob = $otherdb->get("designations");
          $crmjob = $crmjob->row();

          $details['user_id'] = $insert_id;
          $details['designation_id'] = $crmjob->id;
          $details['address'] = $emp->another_address;
          $otherdb->insert('employee_details', $details);

          $detailss['user_id'] = $insert_id;
          $detailss['role_id'] = 2;
          $otherdb->insert('role_user', $detailss);

        }
        // $query = $otherdb->get('users')->result();
        // foreach ($query as $single) {
        //   $otherdb->where("user_id",$single->id);
        //   $role = $otherdb->get("role_user");
        //   $role = $role->row();
        //   $otherdb->where("user_id",$single->id);
        //   $job = $otherdb->get("employee_details");
        //   $job = $job->row();
        //   if($role->role_id == 2){
        //   $data = array();
        //   $data["ar_name"] = $single->name;
        //   $data["en_name"] = $single->name;
        //   $data["phone"] = $single->mobile;
        //
        //   $otherdb->where("user_id",$single->id);
        //   $cd = $otherdb->get("employee_details");
        //   $cd = $cd->row();
        //   $data["another_address"] = $cd->address;
        //
        //
        //   $data["country_id"] = $single->country_id;
        //
        //   $otherdb->where("id",$job->designation_id);
        //   $crmjob = $otherdb->get("designations");
        //   $crmjob = $crmjob->row();
        //
        //   $otherdbErb->where("ar_title",$crmjob->name);
        //   $erpJob = $otherdbErb->get("hr_job");
        //   $erpJob = $erpJob->row();
        //   $data["job_id"] = $erpJob->id;
        //
        //   $otherdbErb->where("email",$single->email);
        //   $erbUser = $otherdbErb->get("hr_employee");
        //   $user = $erbUser->row();
        //   if(!empty($user))
        //   {
        //     // var_dump($user->id);die();
        //     $otherdbErb->where('id', $user->id);
        //     $otherdbErb->update('hr_employee', $data);
        //   }else {
        //     $data["email"] = $single->email;
        //
        //     $otherdbErb->insert('hr_employee', $data);
        //
        //   }
        //   }
        // }
      }
      echo json_encode("here");
      exit();
    }
    public function syncEmployees2()
    {
      $token = $_POST["_token"];
      // if(empty($token)) return false;
      // header('Content-Type: application/json');
      $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      $otherdbErb = $this->load->database('otherdbErb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      $company_id = intval($this->session->userdata('company_id'));


        $query = $otherdb->get('users')->result();
        foreach ($query as $single) {
          $otherdb->where("user_id",$single->id);
          $role = $otherdb->get("role_user");
          $role = $role->row();
          $otherdb->where("user_id",$single->id);
          $job = $otherdb->get("employee_details");
          $job = $job->row();
          if($role->role_id == 2){
          $data = array();
          $data["ar_name"] = $single->name;
          $data["en_name"] = $single->name;
          $data["phone"] = $single->mobile;
          $data["company_id"] = $company_id;

          $otherdb->where("user_id",$single->id);
          $cd = $otherdb->get("employee_details");
          $cd = $cd->row();
          $data["another_address"] = $cd->address;


          $data["country_id"] = $single->country_id;

          $otherdb->where("id",$job->designation_id);
          $crmjob = $otherdb->get("designations");
          $crmjob = $crmjob->row();

          $otherdbErb->where("ar_title",$crmjob->name);
          $erpJob = $otherdbErb->get("hr_job");
          $erpJob = $erpJob->row();
          $data["job_id"] = $erpJob->id;

          $otherdbErb->where("email",$single->email);
          $erbUser = $otherdbErb->get("hr_employee");
          $user = $erbUser->row();
          if(!empty($user))
          {
            // var_dump($user->id);die();
            $otherdbErb->where('id', $user->id);
            $otherdbErb->update('hr_employee', $data);
          }else {
            $data["email"] = $single->email;

            $otherdbErb->insert('hr_employee', $data);

          }
          }
        }

      $erbEmps = $otherdbErb->get("hr_employee");
      $emps = $erbEmps->result();
      foreach ($emps as $emp) {
        $data = array();
        $data["name"] = $emp->ar_name;
        $data["mobile"] = $emp->phone;



        $data["country_id"] = $emp->country_id;

        // $otherdbErb->where("id",$emp->job_id);
        // $erbJob = $otherdbErb->get("hr_job");
        // $erbJob = $erbJob->row();
        //
        // $otherdb->where("name",$erbJob->ar_title);
        // $crmjob = $otherdb->get("designations");
        // $crmjob = $crmjob->row();

        $otherdb->where("email",$emp->email);
        $crmEmp = $otherdb->get("users");
        $crmEmp = $crmEmp->row();

        if(!empty($crmEmp))
        {
          // var_dump($crmUser->id);die();
          $otherdb->where('id', $crmEmp->id);
          $otherdb->update('users', $data);
        }else {
          $data["email"] = $emp->email;

          $otherdb->insert('users', $data);
          $insert_id = $otherdb->insert_id();
          $otherdbErb->where("id",$emp->job_id);
          $erbJob = $otherdbErb->get("hr_job");
          $erbJob = $erbJob->row();

          $otherdb->where("name",$erbJob->ar_title);
          $crmjob = $otherdb->get("designations");
          $crmjob = $crmjob->row();

          $details['user_id'] = $insert_id;
          $details['designation_id'] = $crmjob->id;
          $details['address'] = $emp->another_address;
          $otherdb->insert('employee_details', $details);

          $detailss['user_id'] = $insert_id;
          $detailss['role_id'] = 2;
          $otherdb->insert('role_user', $detailss);

        }
        }
      echo json_encode("here");
      exit();
    }
    public function syncProducts()
    {
      $company_id = intval($this->session->userdata('company_id'));
      // var_dump($company_id);die();
      // $token = $_POST["_token"];
      // if(empty($token)) return false;
      header('Content-Type: application/json');
      $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      $otherdbErb = $this->load->database('otherdbErb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      // from erp to crm
      if (!$otherdbErb->field_exists('old_pro_id', 'product'))
{
  $otherdbErb->query("ALTER TABLE product ADD old_pro_id INT NULL");
  // die("dads");

}
      if (!$otherdb->field_exists('old_pro_id', 'products'))
{
  $otherdb->query("ALTER TABLE products ADD old_pro_id INT NULL");

}
      $erbProducts = $otherdbErb->get("product");
      $products = $erbProducts->result();
      foreach ($products as $product) {
        $data = array();
        // $data["title"] = $product->ar_title;
        $data["price"] = $product->price;
        $data["old_pro_id"] = $product->id;
        $data["description"] = $product->details;
        $otherdb->where("old_pro_id",$product->id);
        $otherdb->or_where("id",$product->old_pro_id);
        $crmProduct = $otherdb->get("products");
        $crmProduct = $crmProduct->row();

        $otherdbErb->where("id",$product->category_id);
        $erbCategory = $otherdbErb->get("store_category");
        $erbCategory = $erbCategory->row();

        $otherdb->where("category_name",$erbCategory->title);
        $crmCategory = $otherdb->get("product_category");
        $crmCategory = $crmCategory->row();

        if(!empty($crmProduct))
        {
          // var_dump($crmUser->id);die();
          $otherdb->where('id', $crmProduct->id);
          $otherdb->update('products', $data);
        }else {
          if(empty($crmCategory))
          {
            $catData['category_name'] = $erbCategory->title;
            $otherdb->insert('product_category', $catData);
            $insert_id = $otherdb->insert_id();
            $data["category_id"] = $insert_id;
          }else {
            $data["category_id"] = $crmCategory->id;
            // code...
          }
          $data["name"] = $product->title;

          $otherdb->insert('products', $data);
          // $insert_id = $otherdb->insert_id();
        }
        }
        // from erp to crm end

        // from crm  to erp

        $query = $otherdb->get('products')->result();
        foreach ($query as $single) {
          $company_id = intval($this->session->userdata('company_id'));

          $data = array();
          // $data["title"] = $single->name;
          $data["details"] = $single->description;
          $data["price"] = $single->price;
          $data["old_pro_id"] = $single->id;
          $data["brand_id"] = "0";

          $otherdb->where("id",$single->category_id);
          $crmCategory = $otherdb->get("product_category");
          $crmCategory = $crmCategory->row();


          $otherdbErb->where("title",$crmCategory->category_name);

          $erbCategory = $otherdbErb->get("store_category");
          $erbCategory = $erbCategory->row();


          $otherdbErb->where("old_pro_id",$single->id);
          $otherdbErb->or_where("id",$single->old_pro_id);
          $erbProduct = $otherdbErb->get("product");
          $product = $erbProduct->row();
          if(!empty($product))
          {
            $otherdbErb->where('id', $product->id);
            $otherdbErb->update('product', $data);
          }else {
            $data["title"] = $single->name;
            if(empty($erbCategory))
            {
              $company_id = intval($this->session->userdata('company_id'));

              $catDatas['title'] = $crmCategory->category_name;
              // var_dump($catDatas);die();
              $otherdbErb->insert('store_category', $catDatas);
              $insert_id = $otherdbErb->insert_id();
              $data["category_id"] = $insert_id;
            }else {
              $data["category_id"] = $erbCategory->id;
              // code...
            }
            $otherdbErb->insert('product', $data);

          }


      }
      echo json_encode("here");
      exit();
    }
    public function syncProjects()
    {
      $company_id = intval($this->session->userdata('company_id'));
      // var_dump($company_id);die();
      // $token = $_POST["_token"];
      // if(empty($token)) return false;
      header('Content-Type: application/json');
      $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      $otherdbErb = $this->load->database('otherdbErb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      // from erp to crm
      if (!$otherdbErb->field_exists('old_pro_id', 'proj_project'))
{
  $otherdbErb->query("ALTER TABLE proj_project ADD old_pro_id INT NULL");
  // die("dads");

}
      if (!$otherdb->field_exists('old_pro_id', 'projects'))
{
  $otherdb->query("ALTER TABLE projects ADD old_pro_id INT NULL");

}
      $erbProjects = $otherdbErb->get("proj_project");
      $projects = $erbProjects->result();
      foreach ($projects as $project) {
        $data = array();
        $data["project_name"] = $project->ar_title;
        $data["notes"] = $project->ar_details;
        $data["project_summary"] = $project->ar_details;
        $data["start_date"] = $project->start_date;
        $data["deadline"] = $project->end_date;
        $data["project_budget"] = $project->total_cost;
        $data["old_pro_id"] = $project->id;
        $data["currency_id"] = 5;

        $otherdbErb->where("id",$project->customer_id);
        $erbUser = $otherdbErb->get("sale_customer");
        $erbUser = $erbUser->row();

        $otherdb->where("email",$erbUser->email);
        $crmUser = $otherdb->get("users");
        $crmUser = $crmUser->row();

        $data["client_id"] = $crmUser->id;

        $otherdb->where("old_pro_id",$project->id);
        $otherdb->or_where("id",$project->old_pro_id);
        $crmProject = $otherdb->get("projects");
        $crmProject = $crmProject->row();



        if(!empty($crmProject))
        {
          // var_dump($crmUser->id);die();
          $otherdb->where('id', $crmProject->id);
          $otherdb->update('projects', $data);
        }else {
          $otherdb->insert('projects', $data);
        }
        }
        // from erp to crm end
      // $crmProjects = $otherdb->get("projects");
      // $projects = $crmProjects->result();
      // foreach ($projects as $project) {
      //   $data = array();
      //   $data["ar_title"] = $project->project_name;
      //   $data["ar_details"] = $project->notes;
      //   $data["ar_details"] = $project->project_summary;
      //   $data["start_date"] = $project->start_date;
      //   $data["end_date"] = $project->deadline;
      //   $data["total_cost"] = $project->project_budget;
      //   $data["old_pro_id"] = $project->id;
      //
      //   $otherdb->where("id",$project->client_id);
      //   $crmUser = $otherdb->get("users");
      //   $crmUser = $crmUser->row();
      //
      //   $otherdbErb->where("email",$crmUser->email);
      //   $erbUser = $otherdbErb->get("users");
      //   $erbUser = $erbUser->row();
      //
      //   $data["customer_id"] = $erbUser->id;
      //
      //   $otherdbErb->where("old_pro_id",$project->id);
      //   $otherdbErb->or_where("id",$project->old_pro_id);
      //   $erbProject = $otherdbErb->get("projects");
      //   $erbProject = $erbProject->row();
      //
      //
      //
      //   if(!empty($erbProject))
      //   {
      //     // var_dump($crmUser->id);die();
      //     $otherdbErb->where('id', $erbProject->id);
      //     $otherdbErb->update('projects', $data);
      //   }else {
      //     $otherdbErb->insert('projects', $data);
      //   }
      //   }
        // from erp to crm end

        // from crm  to erp


      echo json_encode("here");
      exit();
    }
    public function syncProjects2()
    {
      $company_id = intval($this->session->userdata('company_id'));
      // var_dump($company_id);die();
      // $token = $_POST["_token"];
      // if(empty($token)) return false;
      header('Content-Type: application/json');
      $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      $otherdbErb = $this->load->database('otherdbErb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      // from erp to crm
      if (!$otherdbErb->field_exists('old_pro_id', 'proj_project'))
{
  $otherdbErb->query("ALTER TABLE proj_project ADD old_pro_id INT NULL");
  // die("dads");

}
      if (!$otherdb->field_exists('old_pro_id', 'projects'))
{
  $otherdb->query("ALTER TABLE projects ADD old_pro_id INT NULL");

}

      $crmProjects = $otherdb->get("projects");
      $projects = $crmProjects->result();
      foreach ($projects as $project) {
        $data = array();
        $data["ar_title"] = $project->project_name;
        $data["ar_details"] = (!empty($project->notes))?$project->notes:$project->project_name;
        $data["start_date"] = $project->start_date;
        $data["end_date"] = $project->deadline;
        $data["total_cost"] = $project->project_budget;
        $data["company_id"] = $company_id;
        $data["old_pro_id"] = $project->id;

        $otherdb->where("id",$project->client_id);
        $crmUser = $otherdb->get("users");
        $crmUser = $crmUser->row();

        $otherdbErb->where("email",$crmUser->email);
        $erbUser = $otherdbErb->get("sale_customer");
        $erbUser = $erbUser->row();

        $data["customer_id"] = $erbUser->id;

        $otherdbErb->where("old_pro_id",$project->id);
        $otherdbErb->or_where("id",$project->old_pro_id);
        $erbProject = $otherdbErb->get("proj_project");
        $erbProject = $erbProject->row();



        if(!empty($erbProject))
        {
          // var_dump($crmUser->id);die();
          $data["status_id"] = $erbProject->status_id;
          // $data["status_id"] = 2;

          $otherdbErb->where('id', $erbProject->id);
          $otherdbErb->update('proj_project', $data);
        }else {
          $data["status_id"] = 2;
          $otherdbErb->insert('proj_project', $data);
        }
        }
        // from erp to crm end

        // from crm  to erp
        $erbProjects = $otherdbErb->get("proj_project");
        $projects = $erbProjects->result();
        foreach ($projects as $project) {
          $data = array();
          $data["project_name"] = $project->ar_title;
          $data["notes"] = $project->ar_details;
          $data["project_summary"] = $project->ar_details;
          $data["start_date"] = $project->start_date;
          $data["deadline"] = $project->end_date;
          $data["project_budget"] = $project->total_cost;
          $data["old_pro_id"] = $project->id;
          $data["currency_id"] = 5;

          $otherdbErb->where("id",$project->customer_id);
          $erbUser = $otherdbErb->get("sale_customer");
          $erbUser = $erbUser->row();

          $otherdb->where("email",$erbUser->email);
          $crmUser = $otherdb->get("users");
          $crmUser = $crmUser->row();

          $data["client_id"] = $crmUser->id;

          $otherdb->where("old_pro_id",$project->id);
          $otherdb->or_where("id",$project->old_pro_id);
          $crmProject = $otherdb->get("projects");
          $crmProject = $crmProject->row();



          if(!empty($crmProject))
          {
            // var_dump($crmUser->id);die();
            $otherdb->where('id', $crmProject->id);
            $otherdb->update('projects', $data);
          }else {
            $otherdb->insert('projects', $data);
          }
          }
          // from erp to crm end

      echo json_encode("here");
      exit();
    }
    public function syncDesignation()
    {
      // $company_id = intval($this->session->userdata('company_id'));
      // var_dump($company_id);die();
      $token = $_POST["_token"];
      if(empty($token)) return false;
      header('Content-Type: application/json');
      $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      $otherdbErb = $this->load->database('otherdbErb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      // from erp to crm
      if (!$otherdbErb->field_exists('old_job_id', 'hr_job'))
{
  $otherdbErb->query("ALTER TABLE hr_job ADD old_job_id INT NULL");
  // die("dads");

}
      if (!$otherdb->field_exists('old_job_id', 'designations'))
{
  $otherdb->query("ALTER TABLE designations ADD old_job_id INT NULL");

}
      $erbJobs = $otherdbErb->get("hr_job");
      $jobs = $erbJobs->result();
      foreach ($jobs as $job) {
        $data = array();
        // $data["title"] = $product->ar_title;
        $data["name"] = $job->ar_title;
        $data["old_job_id"] = $job->id;
        $otherdb->where("old_job_id",$job->id);
        $otherdb->or_where("id",$job->old_job_id);
        $crmJob = $otherdb->get("designations");
        $crmJob = $crmJob->row();


        if(!empty($crmJob))
        {
          // var_dump($crmUser->id);die();
          $otherdb->where('id', $crmJob->id);
          $otherdb->update('designations', $data);
        }else {

          $otherdb->insert('designations', $data);
          // $insert_id = $otherdb->insert_id();
        }
        }
      // $crmJobs = $otherdb->get("designations");
      // $jobs = $crmJobs->result();
      // foreach ($jobs as $job) {
      //   $data = array();
      //   // $data["title"] = $product->ar_title;
      //   $company_id = $_SESSION['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
      //   $data["ar_title"] = $job->name;
      //   $data["company_id"] = intval($company_id);
      //   $data["old_job_id"] = $job->id;
      //   $otherdbErb->where("old_job_id",$job->id);
      //   $otherdbErb->or_where("id",$job->old_job_id);
      //   $erpJob = $otherdbErb->get("hr_job");
      //   $erpJob = $erpJob->row();
      //
      //
      //   if(!empty($erpJob))
      //   {
      //     // echo $_SESSION['company_id'];die();
      //     $otherdbErb->where('id', $erpJob->id);
      //     $otherdbErb->update('hr_job', $data);
      //   }else {
      //
      //     $otherdbErb->insert('hr_job', $data);
      //     // $insert_id = $otherdb->insert_id();
      //   }
      //   }
      //   // from erp to crm end
      //
      //   // from crm  to erp
      //


      echo json_encode("here");
      exit();
    }
    public function syncDesignation2()
    {
      $company_id = intval($this->session->userdata('company_id'));
      // var_dump($company_id);die();
      $token = $_POST["_token"];
      if(empty($token)) return false;
      header('Content-Type: application/json');
      $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      $otherdbErb = $this->load->database('otherdbErb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      // from erp to crm
      if (!$otherdbErb->field_exists('old_job_id', 'hr_job'))
{
  $otherdbErb->query("ALTER TABLE hr_job ADD old_job_id INT NULL");
  // die("dads");

}
      if (!$otherdb->field_exists('old_job_id', 'designations'))
{
  $otherdb->query("ALTER TABLE designations ADD old_job_id INT NULL");

}
      $crmJobs = $otherdb->get("designations");
      $jobs = $crmJobs->result();
      foreach ($jobs as $job) {
        $data = array();
        // $data["title"] = $product->ar_title;
        $data["ar_title"] = $job->name;
        $data["company_id"] = intval($company_id);
        $data["old_job_id"] = $job->id;
        $otherdbErb->where("old_job_id",$job->id);
        $otherdbErb->or_where("id",$job->old_job_id);
        $erpJob = $otherdbErb->get("hr_job");
        $erpJob = $erpJob->row();


        if(!empty($erpJob))
        {
          // var_dump($crmUser->id);die();
          $otherdbErb->where('id', $erpJob->id);
          $otherdbErb->update('hr_job', $data);
        }else {

          $otherdbErb->insert('hr_job', $data);
          // $insert_id = $otherdb->insert_id();
        }
        }
        // from erp to crm end

        // from crm  to erp


              $erbJobs = $otherdbErb->get("hr_job");
              $jobs = $erbJobs->result();
              foreach ($jobs as $job) {
                $data = array();
                // $data["title"] = $product->ar_title;
                $data["name"] = $job->ar_title;
                $data["old_job_id"] = $job->id;
                $otherdb->where("old_job_id",$job->id);
                $otherdb->or_where("id",$job->old_job_id);
                $crmJob = $otherdb->get("designations");
                $crmJob = $crmJob->row();


                if(!empty($crmJob))
                {
                  // var_dump($crmUser->id);die();
                  $otherdb->where('id', $crmJob->id);
                  $otherdb->update('designations', $data);
                }else {

                  $otherdb->insert('designations', $data);
                  // $insert_id = $otherdb->insert_id();
                }
                }

      echo json_encode("here");
      exit();
    }
    public function syncProducts2()
    {
      $company_id = intval($this->session->userdata('company_id'));
      // var_dump($company_id);die();
      $token = $_POST["_token"];
      if(empty($token)) return false;
      header('Content-Type: application/json');
      $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      $otherdbErb = $this->load->database('otherdbErb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
      // from erp to crm
      if (!$otherdbErb->field_exists('old_pro_id', 'product'))
{
  $otherdb->query("ALTER TABLE product ADD old_pro_id INT NULL");

}
      if (!$otherdb->field_exists('old_pro_id', 'products'))
{
  $otherdb->query("ALTER TABLE products ADD old_pro_id INT NULL");

}
      $erbProducts = $otherdbErb->get("product");
      $products = $erbProducts->result();
      foreach ($products as $product) {
        $data = array();
        // $data["title"] = $product->ar_title;
        $data["price"] = $product->price;
        $data["old_pro_id"] = $product->id;
        $data["description"] = $product->details;
        $otherdb->where("old_pro_id",$product->id);
        $otherdb->or_where("id",$product->old_pro_id);

        $crmProduct = $otherdb->get("products");
        $crmProduct = $crmProduct->row();

        $otherdbErb->where("id",$product->category_id);
        $erbCategory = $otherdbErb->get("store_category");
        $erbCategory = $erbCategory->row();

        $otherdb->where("category_name",$erbCategory->title);
        $crmCategory = $otherdb->get("product_category");
        $crmCategory = $crmCategory->row();

        if(!empty($crmProduct))
        {
          // var_dump($crmUser->id);die();
          $otherdb->where('id', $crmProduct->id);
          $otherdb->update('products', $data);
        }else {
          if(empty($crmCategory))
          {
            $catData['category_name'] = $erbCategory->title;
            $otherdb->insert('product_category', $catData);
            $insert_id = $otherdb->insert_id();
            $data["category_id"] = $insert_id;
          }else {
            $data["category_id"] = $crmCategory->id;
            // code...
          }
          $data["name"] = $product->title;

          $otherdb->insert('products', $data);
          // $insert_id = $otherdb->insert_id();
        }
        }
        // from erp to crm end

        // from crm  to erp

        $query = $otherdb->get('products')->result();
        foreach ($query as $single) {
          $company_id = intval($this->session->userdata('company_id'));

          $data = array();
          // $data["title"] = $single->name;
          $data["details"] = $single->description;
          $data["price"] = $single->price;
          $data["old_pro_id"] = $single->id;
          $data["brand_id"] = "0";

          $otherdb->where("id",$single->category_id);
          $crmCategory = $otherdb->get("product_category");
          $crmCategory = $crmCategory->row();


          $otherdbErb->where("title",$crmCategory->category_name);

          $erbCategory = $otherdbErb->get("store_category");
          $erbCategory = $erbCategory->row();


          $otherdbErb->where("old_pro_id",$single->id);
          $otherdbErb->or_where("id",$single->old_pro_id);

          $erbProduct = $otherdbErb->get("product");
          $product = $erbProduct->row();
          if(!empty($product))
          {
            $otherdbErb->where('id', $product->id);
            $otherdbErb->update('product', $data);
          }else {
            $data["title"] = $single->name;
            if(empty($erbCategory))
            {
              $company_id = intval($this->session->userdata('company_id'));

              $catDatas['title'] = $crmCategory->category_name;
              // var_dump($catDatas);die();
              $otherdbErb->insert('store_category', $catDatas);
              $insert_id = $otherdbErb->insert_id();
              $data["category_id"] = $insert_id;
            }else {
              $data["category_id"] = $erbCategory->id;
              // code...
            }
            $otherdbErb->insert('product', $data);

          }


      }
      echo json_encode("here");
      exit();
    }


}
