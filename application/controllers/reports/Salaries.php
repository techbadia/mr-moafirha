<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salaries extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();

		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}

		$this->load->model("purchase/M_buy_bill");
		$this->load->model("purchase/M_buy_bill_items");
		$this->load->model("purchase/M_buy_manufacturer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("centers/M_center");
		$this->load->model("centers/M_sub_centers");
		$this->load->model("store/M_store_quantity");
		$this->load->model("pos_sys/M_Tec_product_store_qty");
		$this->load->model("hr/M_hr_employee");
		$this->load->model("hr/M_hr_job");
		$this->load->model("hr/M_hr_employee_subsalary");
		$this->load->model("hr/M_hr_inouttimedata");
		$this->load->model("hr/M_hr_salary_disbursement");
		$this->load->model("hr/M_hr_workgroup");

	}

	public function index()
	{
		$this->session->set_userdata('Filter', "");
		$companies = $this->db->get('app_company');
		$data['companies'] = $companies->result();
		$data['DataWorkgroups'] = $this->M_hr_workgroup->GetMultiRow();

    if(!empty($_POST['company_id']))
    {

			$data['date'] = set_value('fromdate');
		 $data['workgroup_id']=$Workgroup=set_value('workgroup_id');
	 		$data['DataRows'] = $this->getEmp($Workgroup);
    }
    $centers = $this->M_center->GetMultiRow();
    foreach ($centers as $center) {
      $center->subs = $sub_centers;
    }
    $data['centers'] = $centers;
    if(!empty( $centers)){

    $centers1 = $centers[0];
    $sub_centerss = $this->M_sub_centers->GetMultiRowCenter($centers1->id);
    $data['sub_centerss'] = $sub_centerss;
    }



		$data['content_page'] = "reports/salaries";
		$this->load->view('page', $data);
	}
	public function getEmp($Workgroup)
	{

	    $employees=$this->M_hr_employee->GetMultiRow();
	     if($Workgroup == -1)
	     {
	         $employees=$this->M_hr_employee->GetMultiRow();
	     }else{
	         $employees=$this->M_hr_employee->GetReport(0,0,0,0,$Workgroup);
	     }
	         return $employees;

	}
}
