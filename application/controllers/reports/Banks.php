<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banks extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();

		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}

		$this->load->model("purchase/M_buy_bill");
		$this->load->model("purchase/M_buy_bill_items");
		$this->load->model("purchase/M_buy_manufacturer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("centers/M_center");
		$this->load->model("centers/M_sub_centers");
		$this->load->model("store/M_store_quantity");
		$this->load->model("pos_sys/M_Tec_product_store_qty");
	}

	public function index()
	{
		$this->session->set_userdata('Filter', "");
		$allArray = array();
		if(!empty($_GET['company_id']))
		{
			$this->db->select('id,ar_title,en_title');
			$companies = $this->db->get('app_company');
			$companies = $companies->result();
			$compArr = array();
			foreach ($companies as $company)
			{
				$compArr[$company->id] = ($this->session->userdata('lang') == "ar")?$company->ar_title:$company->en_title;
			}
			// $compArr = ($_GET['company_id'] == "all")?$compArr:$compArr[$_GET['company_id']];
			if($_GET['company_id'] == "all")
			{
				 $compArr =  $compArr;
			}else {
				$compNew = array();
				$compNew[$_GET['company_id']] = $compArr[$_GET['company_id']];
				$compArr = $compNew;
			}
			foreach ($compArr as $key => $value) {
				$this->db->select('acc_bank');
				$this->db->where("company_id",$key);
				$acc_bank = $this->db->get('fin_settings');
				$acc_bank = $acc_bank->row();
				$this->db->select('id,title,title_en');
				$this->db->where("id",$acc_bank->acc_bank);
				$pAccount = $this->db->get('fin_treeaccount');
				$pAccount = $pAccount->row();
				$allArray[$key]["company_name"] = $value;
				$allArray[$key]["parent_account"] =  ($this->session->userdata('lang') == "ar")?$pAccount->title:$pAccount->title_en;
				$this->db->select('id,title,title_en,startamount');
				$this->db->where("parent_id",$acc_bank->acc_bank);
				$this->db->where("company_id",$key);
				$accounts = $this->db->get('fin_treeaccount');
				$accounts = $accounts->result();
				foreach ($accounts as $account) {
					$buys = $this->prepareBuys($account->id);
					$sales = $this->prepareSales($account->id);
					$journals = $this->prepareJournals($account->id);
					$account->debt = $buys['dept'] + $sales['dept'] + $journals['dept'];
					$account->credit = $buys['creditor'] + $sales['creditor'] + $journals['creditor'];
					$account->net = $buys['net'] + $sales['net'] + $journals['net'];
				}
				$allArray[$key]["sub_accounts"] = $accounts;
			}
			// var_dump($allArray);die();

    }
		$data['all_data'] = $allArray;
    $centers = $this->M_center->GetMultiRow();
    foreach ($centers as $center) {
      $center->subs = $sub_centers;
    }
    $data['centers'] = $centers;
    if(!empty( $centers)){

    $centers1 = $centers[0];
    $sub_centerss = $this->M_sub_centers->GetMultiRowCenter($centers1->id);
    $data['sub_centerss'] = $sub_centerss;
    }


    $companies = $this->db->get('app_company');
		$data['companies'] = $companies->result();
		$data['content_page'] = "reports/banks";
		$this->load->view('page', $data);
	}
	private function prepareBuys($id)
	{
		$this->db->where("tree_id",$id);
		if(!empty($_GET["fromdate"]))
		{
			$this->db->where('thedate >=', $_GET["fromdate"]);
		}
		if(!empty($_GET["todate"]))
		{
			$this->db->where('thedate <=', $_GET["todate"]);
		}
		if(!empty($_GET["keyword"]))
		{
			$this->db->where("supplier_id in (select id from buy_manufacturer where title like '%".$_GET["keyword"]."%' OR title_en like '%".$_GET["keyword"]."%')");
		}
		$buys = $this->db->get('buy_bill');
		$buys = $buys->result();
		foreach ($buys as $buy) {
			$dept += $buy->totalvalue;
			$creditor += $buy->paid;
			$net += $buy->totalvalue - $buy->paid;
		}
		$return = array();
		$return['dept'] = $dept;
		$return['net'] = $net;
		$return['creditor'] = $creditor;

		return $return;
	}
	private function prepareSales($id)
	{
		$this->db->where("tree_id",$id);
		if(!empty($_GET["fromdate"]))
		{
			$this->db->where('thedate >=', $_GET["fromdate"]);
		}
		if(!empty($_GET["todate"]))
		{
			$this->db->where('thedate <=', $_GET["todate"]);
		}
		if(!empty($_GET["keyword"]))
		{
			$this->db->where("supplier_id in (select id from buy_manufacturer where title like '%".$_GET["keyword"]."%' OR title_en like '%".$_GET["keyword"]."%')");
		}
		$buys = $this->db->get('sale_bill');
		$buys = $buys->result();
		foreach ($buys as $buy) {
			$dept += $buy->totalvalue;
			$creditor += $buy->paid;
			$net += $buy->totalvalue - $buy->paid;
		}
		$return = array();
		$return['dept'] = $dept;
		$return['net'] = $net;
		$return['creditor'] = $creditor;

		return $return;
	}
	private function prepareJournals($id)
	{
		$this->db->where("account_id",$id);
		if(!empty($_GET["fromdate"]))
		{
			$this->db->where('main_id in (select id from fin_journal_main where thedate >="'.$_GET["fromdate"].'")');
		}
		if(!empty($_GET["todate"]))
		{
			$this->db->where('main_id in (select id from fin_journal_main where thedate <="'.$_GET["todate"].'")');

		}
		if(!empty($_GET["keyword"]))
		{
			$this->db->where("supplier_id in (select id from buy_manufacturer where title like '%".$_GET["keyword"]."%' OR title_en like '%".$_GET["keyword"]."%')");
		}
		$buys = $this->db->get('fin_journal');
		$buys = $buys->result();
		foreach ($buys as $buy) {
			$dept += $buy->debit;
			$creditor += $buy->creditor;
			$net += $buy->debit - $buy->creditor;
		}
		$return = array();
		$return['dept'] = $dept;
		$return['net'] = $net;
		$return['creditor'] = $creditor;

		return $return;
	}

}
