<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backup extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
// 		$StaffID = intval($this->session->userdata('StaffID'));
//         if ($StaffID <> 1)
//         {
//             redirect ('login');
//         }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		

        $this->session->keep_flashdata('success');
        $this->session->keep_flashdata('error');
        $this->session->keep_flashdata('warning');
        $this->session->keep_flashdata('info');
	}
	
	public function index()
	{
        ini_set('max_execution_time', 512);
		set_time_limit(512);
        
		$this->load->dbutil();
        $backup = $this->dbutil->backup();

        $data['content_page'] = "mydata/backup";
		$this->load->view('page', $data);

        // Load the file helper and write the file to your server
        $this->load->helper('file');
        $UploadPath = "./upload/backup/";
        $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s").".gz";
        if (!is_dir($UploadPath)) {
            mkdir($UploadPath, 0777, TRUE);
        }
        write_file($UploadPath.$filename, $backup);

        // Load the download helper and send the file to your desktop
        $this->load->helper('download');
        force_download($filename, $backup);
	}
}
