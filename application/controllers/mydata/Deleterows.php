<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deleterows extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
// 		$StaffID = intval($this->session->userdata('StaffID'));
//         if ($StaffID <> 1)
//         {
//             redirect ('login');
//         }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		

        $this->session->keep_flashdata('success');
        $this->session->keep_flashdata('error');
        $this->session->keep_flashdata('warning');
        $this->session->keep_flashdata('info');

        $this->load->model("store/M_store");
        $this->load->model("store/M_store_category");
        $this->load->model("store/M_store_brand");
        $this->load->model("store/M_product");
	}
	
	public function index()
	{
        ini_set('max_execution_time', 512);
		set_time_limit(512);
        
        $data['content_page'] = "mydata/deleterows";
		$this->load->view('page', $data);
	}

    public function DeleteRow()
    {
        $this->form_validation->set_rules("table","table","required");
		$this->form_validation->set_rules("id","id","required");

        if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
        else
        {
            $table = set_value('table');
            $id = set_value('id');
        
            switch ($table) {
                case "app_company":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_app_company($id, $table);
                    }
                    break;
                case "usr_users":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_usr_users($id, $table);
                    }
                    break;
                case "fin_treeaccount":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_fin_treeaccount($id, $table);
                    }
                    break;
                case "buy_manufacturer":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_buy_manufacturer($id, $table);
                    }
                    break;
                case "buy_bill":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_buy_bill($id, $table);
                    }
                    break;
                case "buy_offer":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_buy_offer($id, $table);
                    }
                    break;
                case "buy_returns":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_buy_returns($id, $table);
                    }
                    break;
                case "sale_customer":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_sale_customer($id, $table);
                    }
                    break;
                case "sale_bill":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_sale_bill($id, $table);
                    }
                    break;
                case "sale_offer":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_sale_offer($id, $table);
                    }
                    break;
                case "sale_returns":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_sale_returns($id, $table);
                    }
                    break;
                case "store":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_store($id, $table);
                    }
                    break;
                case "store_category":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_store_category($id, $table);
                    }
                    break;
                case "store_brand":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_store_brand($id, $table);
                    }
                    break;
                case "product":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_product($id, $table);
                    }
                    break;
                case "cars_cars":
                    $CheckResult = $this->M_statistics->CheckTableID($id, $table);
                    if($CheckResult > 0)
                    {
                        $this->Delete_cars_cars($id, $table);
                    }
                    break;
                    
                    
                case "fin_journal_delete_all":
                     $this->fin_journal_delete_all();
                    break;
                default:
                    //
            }
            
        }

        $data['content_page'] = "mydata/deleterows";
		$this->load->view('page', $data);
    }

    public function Delete_app_company($id, $table)
    {
        $this->db->delete('app_company', array('id' => $id));
        $this->db->delete('arc_category', array('company_id' => $id));
        $this->db->delete('arc_files', array('company_id' => $id));
        $this->db->delete('buy_bill', array('company_id' => $id));
        $this->db->delete('buy_manufacturer', array('company_id' => $id));
        $this->db->delete('buy_offer', array('company_id' => $id));
        $this->db->delete('buy_returns', array('company_id' => $id));
        $this->db->delete('cars_cars', array('company_id' => $id));
        $this->db->delete('cars_invoice', array('company_id' => $id));
        $this->db->delete('cars_model', array('company_id' => $id));
        $this->db->delete('cars_orders', array('company_id' => $id));
        $this->db->delete('cars_service', array('company_id' => $id));
        $this->db->delete('cov_blocked_projects', array('company_id' => $id));
        $this->db->delete('cov_data_covenant', array('company_id' => $id));
        $this->db->delete('cov_data_covenant_trace', array('company_id' => $id));
        $this->db->delete('cov_data_expenses', array('company_id' => $id));
        $this->db->delete('cov_data_expenses_trace', array('company_id' => $id));
        $this->db->delete('cov_projects', array('company_id' => $id));
        $this->db->delete('data_invoice', array('company_id' => $id));
        $this->db->delete('data_request', array('company_id' => $id));
        $this->db->delete('fin_expected_expenses', array('company_id' => $id));
        $this->db->delete('fin_expected_revenue', array('company_id' => $id));
        $this->db->delete('fin_journal_main', array('company_id' => $id));
        $this->db->delete('fin_settings', array('company_id' => $id));
        $this->db->delete('fin_treeaccount', array('company_id' => $id));
        $this->db->delete('hr_country', array('company_id' => $id));
        $this->db->delete('hr_dependents', array('company_id' => $id));
        $this->db->delete('hr_device', array('company_id' => $id));
        $this->db->delete('hr_education', array('company_id' => $id));
        $this->db->delete('hr_emergency_contact', array('company_id' => $id));
        $this->db->delete('hr_employee', array('company_id' => $id));
        $this->db->delete('hr_employee_device', array('company_id' => $id));
        $this->db->delete('hr_employee_files', array('company_id' => $id));
        $this->db->delete('hr_employee_money', array('company_id' => $id));
        $this->db->delete('hr_employee_subsalary', array('company_id' => $id));
        $this->db->delete('hr_holidays', array('company_id' => $id));
        $this->db->delete('hr_inouttimedata', array('company_id' => $id));
        $this->db->delete('hr_job', array('company_id' => $id));
        $this->db->delete('hr_job_type', array('company_id' => $id));
        $this->db->delete('hr_performance', array('company_id' => $id));
        $this->db->delete('hr_salary', array('company_id' => $id));
        $this->db->delete('hr_travel', array('company_id' => $id));
        $this->db->delete('hr_tra_employee', array('company_id' => $id));
        $this->db->delete('hr_vacation', array('company_id' => $id));
        $this->db->delete('hr_vacation_types', array('company_id' => $id));
        $this->db->delete('hr_weekend', array('company_id' => $id));
        $this->db->delete('hr_workgroup', array('company_id' => $id));
        $this->db->delete('hr_workgrouptime', array('company_id' => $id));
        $this->db->delete('hr_worktime', array('company_id' => $id));
        $this->db->delete('manu_components', array('company_id' => $id));
        $this->db->delete('manu_components_items', array('company_id' => $id));
        $this->db->delete('manu_manufacturing_orders', array('company_id' => $id));
        $this->db->delete('product', array('company_id' => $id));
        $this->db->delete('proj_project', array('company_id' => $id));
        $this->db->delete('proj_project_files', array('company_id' => $id));
        $this->db->delete('proj_project_level', array('company_id' => $id));
        $this->db->delete('proj_project_level_task', array('company_id' => $id));
        $this->db->delete('proj_status', array('company_id' => $id));
        $this->db->delete('sale_bill', array('company_id' => $id));
        $this->db->delete('sale_customer', array('company_id' => $id));
        $this->db->delete('sale_offer', array('company_id' => $id));
        $this->db->delete('sale_returns', array('company_id' => $id));
        $this->db->delete('serv_action', array('company_id' => $id));
        $this->db->delete('serv_events', array('company_id' => $id));
        $this->db->delete('serv_events_users', array('company_id' => $id));
        $this->db->delete('serv_message', array('company_id' => $id));
        $this->db->delete('serv_notifications', array('company_id' => $id));
        $this->db->delete('settings_acceptance', array('company_id' => $id));
        $this->db->delete('settings_notifications', array('company_id' => $id));
        $this->db->delete('settings_print', array('company_id' => $id));
        $this->db->delete('set_area', array('company_id' => $id));
        $this->db->delete('set_request_status', array('company_id' => $id));
        $this->db->delete('set_teamwork', array('company_id' => $id));
        $this->db->delete('set_teamwork_items', array('company_id' => $id));
        $this->db->delete('store', array('company_id' => $id));
        $this->db->delete('store_brand', array('company_id' => $id));
        $this->db->delete('store_category', array('company_id' => $id));
        $this->db->delete('store_move', array('company_id' => $id));
        $this->db->delete('store_quantity', array('company_id' => $id));
        $this->db->delete('tas_tasks', array('company_id' => $id));
        $this->db->delete('tas_tasks_transfer', array('company_id' => $id));
        $this->db->delete('tas_tasks_type1_levels', array('company_id' => $id));
        $this->db->delete('tic_category', array('company_id' => $id));
        $this->db->delete('tic_priority', array('company_id' => $id));
        $this->db->delete('tic_status', array('company_id' => $id));
        $this->db->delete('tic_ticket', array('company_id' => $id));
    }

    public function Delete_usr_users($id, $table)
    {
        $this->db->delete('usr_users', array('id' => $id));
        $this->db->delete('arc_files', array('user_id' => $id));
        $this->db->delete('buy_bill', array('user_id' => $id));
        $this->db->delete('buy_manufacturer', array('user_id' => $id));
        $this->db->delete('buy_offer', array('user_id' => $id));
        $this->db->delete('buy_returns', array('user_id' => $id));
        $this->db->delete('cars_invoice', array('user_id' => $id));
        $this->db->delete('cars_orders', array('user_id' => $id));
        $this->db->delete('cov_blocked_projects', array('user_id' => $id));
        $this->db->delete('cov_data_covenant', array('user_id' => $id));
        $this->db->delete('cov_data_covenant_trace', array('user_id' => $id));
        $this->db->delete('cov_data_expenses', array('user_id' => $id));
        $this->db->delete('cov_data_expenses_trace', array('user_id' => $id));
        $this->db->delete('data_invoice', array('user_id' => $id));
        $this->db->delete('data_request', array('user_id' => $id));
        $this->db->delete('fin_journal_main', array('user_id' => $id));
        $this->db->delete('manu_components', array('user_id' => $id));
        $this->db->delete('manu_manufacturing_orders', array('user_id' => $id));
        $this->db->delete('sale_bill', array('user_id' => $id));
        $this->db->delete('sale_customer', array('user_id' => $id));
        $this->db->delete('sale_offer', array('user_id' => $id));
        $this->db->delete('sale_returns', array('user_id' => $id));
        $this->db->delete('serv_action', array('from_user' => $id));
        $this->db->delete('serv_action', array('to_user' => $id));
        $this->db->delete('serv_events_users', array('user_id' => $id));
        $this->db->delete('serv_message', array('from_user' => $id));
        $this->db->delete('serv_message', array('to_user' => $id));
        $this->db->delete('serv_notifications', array('user_id' => $id));
        $this->db->delete('usr_users_store', array('user_id' => $id));
    }

    public function Delete_fin_treeaccount($id, $table)
    {
        $this->db->delete('fin_treeaccount', array('id' => $id));
        $this->db->delete('fin_journal', array('account_id' => $id));
    }

    public function Delete_buy_manufacturer($id, $table)
    {
        $this->db->delete('buy_manufacturer', array('id' => $id));
        $this->db->delete('fin_journal', array('account_id' => $id));
        $this->db->delete('buy_bill', array('supplier_id' => $id));
        $this->db->delete('buy_offer', array('supplier_id' => $id));
        $this->db->delete('buy_returns', array('supplier_id' => $id));
    }

    public function Delete_buy_bill($id, $table)
    {
        $this->load->model("sales/M_sale_customer");
        $this->load->model('purchase/M_buy_bill');
        $this->load->model('purchase/M_buy_bill_items');

        $this->load->model("account/M_fin_treeaccount");
        $this->load->model("account/M_fin_journal");
        $this->load->model("account/M_fin_journal_main");
        
        $this->load->model("store/M_store");
        $this->load->model("store/M_product");
        $this->load->model("store/M_store_quantity");

        $JournalMainData = $this->M_fin_journal_main->GetRow_bill_id_table_name($id, $table);
        $JournalMainID = $JournalMainData->id;
        $this->db->delete("fin_journal_main", array('bill_id' => $id, 'table_name' => $table));
        $this->db->delete("fin_journal", array('id' => $JournalMainID));

        $Items = $this->M_buy_bill_items->GetItems($id);
        foreach($Items as $Items_Row) {
            $store_type_id = $Items_Row->store_type_id;
            $location_id = $Items_Row->location_id;
            $RestoreQuantity = $Items_Row->quantity;
            $CurrentQuantity = $this->M_store_quantity->GetStoreProduct($store_type_id, $location_id); 
            if ($location_id >0)
            {
                $WeHave = $this->M_store_quantity->CheckIfHaveQuantity($store_type_id, $location_id);
                if ($WeHave >0)
                {
                    $OldQuantity = $CurrentQuantity->quantity;
                    $OldQuantityRowID = $CurrentQuantity->id;
                    $NewQuantity = $OldQuantity - $RestoreQuantity;
                    $NewData = array(
                        'company_id' => $store_type_id,
                        'customer_id' => $location_id,
                        'project_id' => $NewQuantity,
                    );
                    $this->M_store_quantity->UpdateRecord($OldQuantityRowID, $NewData);
                }
            }
        }
        $company_id = intval($this->session->userdata('company_id'));
        $this->db->delete('buy_bill', array('id' => $id, 'company_id' => $company_id));
        $this->db->delete('buy_bill_items', array('bill_id' => $id));
    }

    public function Delete_buy_offer($id, $table)
    {
        $this->load->model('purchase/M_buy_offer');
        $this->load->model('purchase/M_buy_offer_items');

        $company_id = intval($this->session->userdata('company_id'));
        $this->db->delete('buy_offer', array('id' => $id, 'company_id' => $company_id));
        $this->db->delete('buy_offer_items', array('bill_id' => $id));
    }

    public function Delete_buy_returns($id, $table)
    {
        $this->load->model("sales/M_sale_customer");
        $this->load->model('purchase/M_buy_returns');
        $this->load->model('purchase/M_buy_returns_items');

        $this->load->model("account/M_fin_treeaccount");
        $this->load->model("account/M_fin_journal");
        $this->load->model("account/M_fin_journal_main");
        
        $this->load->model("store/M_store");
        $this->load->model("store/M_product");
        $this->load->model("store/M_store_quantity");

        $JournalMainData = $this->M_fin_journal_main->GetRow_bill_id_table_name($id, $table);
        $JournalMainID = $JournalMainData->id;
        $this->db->delete("fin_journal_main", array('bill_id' => $id, 'table_name' => $table));
        $this->db->delete("fin_journal", array('id' => $JournalMainID));

        $Items = $this->M_buy_returns_items->GetItems($id);
        foreach($Items as $Items_Row) {
            $store_type_id = $Items_Row->store_type_id;
            $location_id = $Items_Row->location_id;
            $RestoreQuantity = $Items_Row->quantity;
            $CurrentQuantity = $this->M_store_quantity->GetStoreProduct($store_type_id, $location_id); 
            if ($location_id >0)
            {
                $WeHave = $this->M_store_quantity->CheckIfHaveQuantity($store_type_id, $location_id);
                if ($WeHave >0)
                {
                    $OldQuantity = $CurrentQuantity->quantity;
                    $OldQuantityRowID = $CurrentQuantity->id;
                    $NewQuantity = $OldQuantity + $RestoreQuantity;
                    $NewData = array(
                        'company_id' => $store_type_id,
                        'customer_id' => $location_id,
                        'project_id' => $NewQuantity,
                    );
                    $this->M_store_quantity->UpdateRecord($OldQuantityRowID, $NewData);
                }
            }
        }
        $company_id = intval($this->session->userdata('company_id'));
        $this->db->delete('buy_returns', array('id' => $id, 'company_id' => $company_id));
        $this->db->delete('buy_returns_items', array('bill_id' => $id));
    }

    public function Delete_sale_customer($id, $table)
    {
        $this->db->delete('sale_customer', array('id' => $id));
        $this->db->delete('fin_journal', array('account_id' => $id));
        $this->db->delete('sale_bill', array('customer_id' => $id));
        $this->db->delete('sale_offer', array('customer_id' => $id));
        $this->db->delete('sale_returns', array('customer_id' => $id));
    }

    public function Delete_sale_bill($id, $table)
    {
        $this->load->model("sales/M_sale_customer");
        $this->load->model('sales/M_sale_bill');
        $this->load->model('sales/M_sale_bill_items');

        $this->load->model("account/M_fin_treeaccount");
        $this->load->model("account/M_fin_journal");
        $this->load->model("account/M_fin_journal_main");
        
        $this->load->model("store/M_store");
        $this->load->model("store/M_product");
        $this->load->model("store/M_store_quantity");

        $JournalMainData = $this->M_fin_journal_main->GetRow_bill_id_table_name($id, $table);
        $JournalMainID = $JournalMainData->id;
        $this->db->delete("fin_journal_main", array('bill_id' => $id, 'table_name' => $table));
        $this->db->delete("fin_journal", array('id' => $JournalMainID));

        $Items = $this->M_sale_bill_items->GetItems($id);
        foreach($Items as $Items_Row) {
            $store_type_id = $Items_Row->store_type_id;
            $location_id = $Items_Row->location_id;
            $RestoreQuantity = $Items_Row->quantity;
            $CurrentQuantity = $this->M_store_quantity->GetStoreProduct($store_type_id, $location_id); 
            if ($location_id >0)
            {
                $WeHave = $this->M_store_quantity->CheckIfHaveQuantity($store_type_id, $location_id);
                if ($WeHave >0)
                {
                    $OldQuantity = $CurrentQuantity->quantity;
                    $OldQuantityRowID = $CurrentQuantity->id;
                    $NewQuantity = $OldQuantity + $RestoreQuantity;
                    $NewData = array(
                        'company_id' => $store_type_id,
                        'customer_id' => $location_id,
                        'project_id' => $NewQuantity,
                    );
                    $this->M_store_quantity->UpdateRecord($OldQuantityRowID, $NewData);
                }
            }
        }
        $company_id = intval($this->session->userdata('company_id'));
        $this->db->delete('sale_bill', array('id' => $id, 'company_id' => $company_id));
        $this->db->delete('sale_bill_items', array('bill_id' => $id));
    }

    public function Delete_sale_offer($id, $table)
    {
        $this->load->model('sales/M_sale_offer');
        $this->load->model('sales/M_sale_offer_items');

        $company_id = intval($this->session->userdata('company_id'));
        $this->db->delete('sale_offer', array('id' => $id, 'company_id' => $company_id));
        $this->db->delete('sale_offer_items', array('bill_id' => $id));
    }

    public function Delete_sale_returns($id, $table)
    {
        $this->load->model("sales/M_sale_customer");
        $this->load->model('sales/M_sale_returns');
        $this->load->model('sales/M_sale_returns_items');

        $this->load->model("account/M_fin_treeaccount");
        $this->load->model("account/M_fin_journal");
        $this->load->model("account/M_fin_journal_main");
        
        $this->load->model("store/M_store");
        $this->load->model("store/M_product");
        $this->load->model("store/M_store_quantity");

        $JournalMainData = $this->M_fin_journal_main->GetRow_bill_id_table_name($id, $table);
        $JournalMainID = $JournalMainData->id;
        $this->db->delete("fin_journal_main", array('bill_id' => $id, 'table_name' => $table));
        $this->db->delete("fin_journal", array('id' => $JournalMainID));

        $Items = $this->M_sale_returns_items->GetItems($id);
        foreach($Items as $Items_Row) {
            $store_type_id = $Items_Row->store_type_id;
            $location_id = $Items_Row->location_id;
            $RestoreQuantity = $Items_Row->quantity;
            $CurrentQuantity = $this->M_store_quantity->GetStoreProduct($store_type_id, $location_id); 
            if ($location_id >0)
            {
                $WeHave = $this->M_store_quantity->CheckIfHaveQuantity($store_type_id, $location_id);
                if ($WeHave >0)
                {
                    $OldQuantity = $CurrentQuantity->quantity;
                    $OldQuantityRowID = $CurrentQuantity->id;
                    $NewQuantity = $OldQuantity - $RestoreQuantity;
                    $NewData = array(
                        'company_id' => $store_type_id,
                        'customer_id' => $location_id,
                        'project_id' => $NewQuantity,
                    );
                    $this->M_store_quantity->UpdateRecord($OldQuantityRowID, $NewData);
                }
            }
        }
        $company_id = intval($this->session->userdata('company_id'));
        $this->db->delete('sale_returns', array('id' => $id, 'company_id' => $company_id));
        $this->db->delete('sale_returns_items', array('bill_id' => $id));
    }

    public function Delete_store($id, $table)
    {
        $this->db->delete('store', array('id' => $id));
        $this->db->delete('store_quantity', array('store_id' => $id));
        $this->db->delete('buy_bill_items', array('location_id' => $id));
        $this->db->delete('buy_returns_items', array('location_id' => $id));
        $this->db->delete('sale_bill', array('location_id' => $id));
        $this->db->delete('sale_returns_items', array('location_id' => $id));
    }

    public function Delete_store_category($id, $table)
    {
        $this->db->delete('store_category', array('id' => $id));
        $this->db->delete('product', array('category_id' => $id));
    }

    public function Delete_store_brand($id, $table)
    {
        $this->db->delete('store_brand', array('id' => $id));
        $this->db->delete('product', array('brand_id' => $id));
    }

    public function Delete_product($id, $table)
    {
        $this->db->delete('product', array('id' => $id));
        $this->db->delete('buy_bill_items', array('store_type_id' => $id));
        $this->db->delete('buy_offer_items', array('store_type_id' => $id));
        $this->db->delete('buy_returns_items', array('store_type_id' => $id));
        $this->db->delete('sale_bill_items', array('store_type_id' => $id));
        $this->db->delete('sale_offer_items', array('store_type_id' => $id));
        $this->db->delete('sale_returns_items', array('store_type_id' => $id));
    }

    public function Delete_cars_cars($id, $table)
    {
        $this->db->delete('cars_cars', array('id' => $id));
        $this->db->delete('cars_orders', array('car_id' => $id));
        $this->db->delete('cars_invoice', array('car_id' => $id));
    }
    
    public function fin_journal_delete_all()
    {
         $this->db->truncate('fin_journal');
         $this->db->truncate('fin_journal_main');
     
    }
    
    
}

