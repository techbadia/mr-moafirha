<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Optimize extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
// 		$StaffID = intval($this->session->userdata('StaffID'));
//         if ($StaffID <> 1)
//         {
//             redirect ('login');
//         }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		

        $this->session->keep_flashdata('success');
        $this->session->keep_flashdata('error');
        $this->session->keep_flashdata('warning');
        $this->session->keep_flashdata('info');
	}
	
	public function index()
	{
        ini_set('max_execution_time', 512);
		set_time_limit(512);
        
		$this->load->dbutil();
        
        $this->dbutil->optimize_table('app_company');
        $this->dbutil->optimize_table('app_module');
        $this->dbutil->optimize_table('app_module_page');
        $this->dbutil->optimize_table('app_package');
        $this->dbutil->optimize_table('app_package_module');
        $this->dbutil->optimize_table('app_package_module_page');
        $this->dbutil->optimize_table('app_updated_records');
        $this->dbutil->optimize_table('arc_category');
        $this->dbutil->optimize_table('arc_files');
        $this->dbutil->optimize_table('buy_bill');
        $this->dbutil->optimize_table('buy_bill_items');
        $this->dbutil->optimize_table('buy_manufacturer');
        $this->dbutil->optimize_table('buy_offer');
        $this->dbutil->optimize_table('buy_offer_items');
        $this->dbutil->optimize_table('buy_returns');
        $this->dbutil->optimize_table('buy_returns_items');
        $this->dbutil->optimize_table('ci_sessions');
        $this->dbutil->optimize_table('cov_blocked_projects');
        $this->dbutil->optimize_table('cov_data_covenant');
        $this->dbutil->optimize_table('cov_data_covenant_trace');
        $this->dbutil->optimize_table('cov_data_expenses');
        $this->dbutil->optimize_table('cov_data_expenses_trace');
        $this->dbutil->optimize_table('cov_projects');
        $this->dbutil->optimize_table('currencies');
        $this->dbutil->optimize_table('data_invoice');
        $this->dbutil->optimize_table('data_invoice_expenses');
        $this->dbutil->optimize_table('data_invoice_items');
        $this->dbutil->optimize_table('data_request');
        $this->dbutil->optimize_table('data_request_items');
        $this->dbutil->optimize_table('data_teamwork_items_expenses');
        $this->dbutil->optimize_table('data_teamwork_items_revenues');
        $this->dbutil->optimize_table('fin_expected_expenses');
        $this->dbutil->optimize_table('fin_expected_revenue');
        $this->dbutil->optimize_table('fin_journal');
        $this->dbutil->optimize_table('fin_journal_main');
        $this->dbutil->optimize_table('fin_settings');
        $this->dbutil->optimize_table('fin_treeaccount');
        $this->dbutil->optimize_table('fin_treeaccount_basic');
        $this->dbutil->optimize_table('fin_treecategory');
        $this->dbutil->optimize_table('hr_country');
        $this->dbutil->optimize_table('hr_dependents');
        $this->dbutil->optimize_table('hr_device');
        $this->dbutil->optimize_table('hr_education');
        $this->dbutil->optimize_table('hr_emergency_contact');
        $this->dbutil->optimize_table('hr_employee');
        $this->dbutil->optimize_table('hr_employee_device');
        $this->dbutil->optimize_table('hr_employee_files');
        $this->dbutil->optimize_table('hr_employee_money');
        $this->dbutil->optimize_table('hr_employee_subsalary');
        $this->dbutil->optimize_table('hr_holidays');
        $this->dbutil->optimize_table('hr_inouttimedata');
        $this->dbutil->optimize_table('hr_job');
        $this->dbutil->optimize_table('hr_job_type');
        $this->dbutil->optimize_table('hr_performance');
        $this->dbutil->optimize_table('hr_salary');
        $this->dbutil->optimize_table('hr_travel');
        $this->dbutil->optimize_table('hr_tra_employee');
        $this->dbutil->optimize_table('hr_vacation');
        $this->dbutil->optimize_table('hr_vacation_types');
        $this->dbutil->optimize_table('hr_weekend');
        $this->dbutil->optimize_table('hr_workgroup');
        $this->dbutil->optimize_table('hr_workgrouptime');
        $this->dbutil->optimize_table('hr_worktime');
        $this->dbutil->optimize_table('manu_components');
        $this->dbutil->optimize_table('manu_components_items');
        $this->dbutil->optimize_table('manu_manufacturing_orders');
        $this->dbutil->optimize_table('manu_manufacturing_orders_items');
        $this->dbutil->optimize_table('product');
        $this->dbutil->optimize_table('proj_project');
        $this->dbutil->optimize_table('proj_project_files');
        $this->dbutil->optimize_table('proj_project_level');
        $this->dbutil->optimize_table('proj_project_level_task');
        $this->dbutil->optimize_table('proj_status');
        $this->dbutil->optimize_table('sale_bill');
        $this->dbutil->optimize_table('sale_bill_items');
        $this->dbutil->optimize_table('sale_customer');
        $this->dbutil->optimize_table('sale_offer');
        $this->dbutil->optimize_table('sale_offer_items');
        $this->dbutil->optimize_table('sale_returns');
        $this->dbutil->optimize_table('sale_returns_items');
        $this->dbutil->optimize_table('services');
        $this->dbutil->optimize_table('serv_action');
        $this->dbutil->optimize_table('serv_action_types');
        $this->dbutil->optimize_table('serv_events');
        $this->dbutil->optimize_table('serv_events_users');
        $this->dbutil->optimize_table('serv_message');
        $this->dbutil->optimize_table('serv_notifications');
        $this->dbutil->optimize_table('settings_acceptance');
        $this->dbutil->optimize_table('settings_notifications');
        $this->dbutil->optimize_table('settings_print');
        $this->dbutil->optimize_table('set_area');
        $this->dbutil->optimize_table('set_request_status');
        $this->dbutil->optimize_table('set_teamwork');
        $this->dbutil->optimize_table('set_teamwork_items');
        $this->dbutil->optimize_table('set_teamwork_schedule');
        $this->dbutil->optimize_table('set_teamwork_tasks');
        $this->dbutil->optimize_table('store');
        $this->dbutil->optimize_table('store_brand');
        $this->dbutil->optimize_table('store_category');
        $this->dbutil->optimize_table('store_move');
        $this->dbutil->optimize_table('store_quantity');
        $this->dbutil->optimize_table('tas_tasks');
        $this->dbutil->optimize_table('tas_tasks_transfer');
        $this->dbutil->optimize_table('tas_tasks_type1_levels');
        $this->dbutil->optimize_table('tic_category');
        $this->dbutil->optimize_table('tic_priority');
        $this->dbutil->optimize_table('tic_status');
        $this->dbutil->optimize_table('tic_ticket');
        $this->dbutil->optimize_table('usr_trace');
        $this->dbutil->optimize_table('usr_users');
        $this->dbutil->optimize_table('usr_usersgroup');
        $this->dbutil->optimize_table('usr_usersprivileges');
        $this->dbutil->optimize_table('web_customers');
        $this->dbutil->optimize_table('web_members');
        $this->dbutil->optimize_table('web_orders');
        $this->dbutil->optimize_table('web_orders_details');
        $this->dbutil->optimize_table('web_pages');
        $this->dbutil->optimize_table('web_search');
        $this->dbutil->optimize_table('web_slider');
        $this->dbutil->optimize_table('web_website');

        $this->dbutil->optimize_database();

        $data['content_page'] = "mydata/optimize";
		$this->load->view('page', $data);
	}
}
