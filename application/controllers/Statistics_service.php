<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistics_service extends CI_Controller {

	var $data = array();
	
	public function index()
	{
		$this->load->model("sales/M_sale_customer");
		$this->load->model("maintenance/M_set_request_status");
		$this->load->model("maintenance/M_set_area");
		$this->load->model("maintenance/M_services");
		$this->load->model("maintenance/M_data_request_items");
		$this->load->model("maintenance/M_set_teamwork_schedule");
		$this->load->model("maintenance/M_data_invoice");
		$this->load->model("maintenance/M_data_invoice_items");
		$this->load->model("maintenance/M_set_teamwork_tasks");
		$this->load->model("maintenance/M_set_teamwork");
        $this->load->model("maintenance/M_data_request");

		$data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
        $data['Area'] = $this->M_set_area->GetMultiRow();
		
		$this->config->load('settings/config_setting');
		$statistics_page = "";
		$Config_File = "";
		$Config_File_Lang = "";
		//$Segment1 = $this->uri->segment(1);
		//$this->config->set_item('Package', $Segment1);
		$Package = $this->config->item('Package');

		if ($this->config->item('MainLanguage') == "ar")
		{
			$Config_File_Lang = "_ar";
		}
		else
		{
			$Config_File_Lang = "_en";
		}
		
		$AppData = $this->M_app_element->GetApp();
		$PackageName = $AppData->PackageName;
		$statistics_page = "statistics_".$PackageName;
		$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
		$this->config->load($Config_File);
		$this->session->set_userdata('PackageName', $PackageName);
		$PackageData = $this->M_app_package->GetByTitle($PackageName);
		$PackageID = $PackageData->id;
		$this->session->set_userdata('PackageID', $PackageID);
		
		
		$this->LoadLanguage();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
			redirect ($PackageName.'login/');
        }
		$this->session->set_userdata('statistics_filter', "Month");
		$this->session->set_userdata('statistics_page', $statistics_page);
		
		$data['content_page'] = "statistics/statistics_service";
		$this->load->view('home', $data);
	}


	public function filter_today()
	{
		$this->LoadLanguage();
		$this->session->set_userdata('statistics_filter', "Today");
		$statistics_page = $this->session->userdata('statistics_page');
		$data['content_page'] = $statistics_page;
		$this->load->view('home', $data);
	}

	public function filter_year()
	{
		$this->LoadLanguage();
		$this->session->set_userdata('statistics_filter', "Year");
		
		$statistics_page = $this->session->userdata('statistics_page');
		$data['content_page'] = $statistics_page;
		$this->load->view('home', $data);
	}

	public function LoadLanguage()
	{
		$this->config->load('settings/config_setting');
		$Package = $this->config->item('Package');

		if ($this->config->item('MainLanguage') == "ar")
		{
			$Config_File_Lang = "_ar";
		}
		else
		{
			$Config_File_Lang = "_en";
		}

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
	}

}
