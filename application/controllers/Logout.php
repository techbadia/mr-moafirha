<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
	}  
	
	public function index()
	{
		$this->session->sess_destroy();
		$this->output->delete_cache();
		$this->cache->clean();

		//$cookie = array(
			//'name'   => 'Package',
			//'value'  => '',
			//'expire' => '0',
			//'domain' => base_url(),
		//);
        //delete_cookie($cookie);

		$PageURL = base_url().$this->session->userdata('PackageName');
		redirect ($PageURL);
	}

}
