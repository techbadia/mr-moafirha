<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_element extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();

		$AdminID = intval($this->session->userdata('AdminID'));
        if ($AdminID == 0)
        {
            //redirect ('login');
        }
				$this->load->model("app/M_app_element");

	}

	public function index()
	{
		$data['MultiRows'] = $this->M_app_element->GetMultiRow();
		$this->load->view('app_element/app_element', $data);
	}

	public function insertform()
	{
		$this->load->view('app_element/insert', $data);
	}

	public function Insert_Data()
	{
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("details","details","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$parent_id = set_value('parent_id');
			$title = set_value("title");
			$details = set_value('details');
			$view_no = set_value('view_no');
			$search_menu = set_value('search_menu');

			$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/";
			$ImageDirectory = $UploadPath;

			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;

			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
			}

			$NewData = array();
			$NewData = array(
				'parent_id' => $parent_id,
				'title' => $title,
				'details' => $details,
				'image' => $NewFileName,
				'view_no' => $view_no,
				'search_menu' => $search_menu,
			);

			$this->M_category->InsertRecord($NewData);

			redirect ('category');
		}
	}

	public function updateform($rid)
	{
		$Datarow = $this->M_category->GetRow($rid);
		$data['id'] = $Datarow->id;
		$data['parent_id'] = $Datarow->parent_id;
		$data['title'] = $Datarow->title;
		$data['details'] = $Datarow->details;
		$data['view_no'] = $Datarow->view_no;
		$data['search_menu'] = $Datarow->search_menu;

		$this->load->view('category_update', $data);
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("details","details","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$parent_id = set_value('parent_id');
			$title = set_value("title");
			$details = set_value('details');
			$view_no = set_value('view_no');
			$search_menu = set_value('search_menu');
			$ChangeImage = set_value('ChangeImage');

			$NewData = array();
			if($ChangeImage == "True")
			{
				$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
				$exts = explode(".", $_FILES['image']['name']);
				$exts = end($exts);
				$exts = strtolower($exts);
				$UploadPath = "./upload/";
				$ImageDirectory = $UploadPath;

				if (!is_dir($ImageDirectory)) {
					mkdir($ImageDirectory, 0777, TRUE);

				}

				$config['upload_path'] = $ImageDirectory;
				$config['allowed_types'] = "*";
				$config['file_name'] = $filename.".".$exts;
				$config['overwrite'] = TRUE;
				$config['file_ext_tolower'] = TRUE;
				$config['remove_spaces'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				$NewFileName = $filename.".".$exts;

				if (! $this->upload->do_upload('image', $NewFileName))
				{
					$error = array('error' => $this->upload->display_errors());
					$NewFileName ="";
				}
				else {
					$this->upload->do_upload('image', $NewFileName);
				}


				$NewData = array(
					'parent_id' => $parent_id,
					'title' => $title,
					'details' => $details,
					'image' => $NewFileName,
					'view_no' => $view_no,
					'search_menu' => $search_menu,
				);
			}
			else
			{
				$NewData = array(
					'parent_id' => $parent_id,
					'title' => $title,
					'details' => $details,
					'view_no' => $view_no,
					'search_menu' => $search_menu,
				);
			}


			$this->M_category->UpdateRecord($id, $NewData);

			redirect ('category');
		}
	}

	public function Delete($rid)
	{
		$this->db->where('id', $id);
        $this->db->delete('category');
        redirect("category");
	}
}
