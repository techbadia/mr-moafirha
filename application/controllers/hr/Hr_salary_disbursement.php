<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hr_salary_disbursement extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
        $this->load->model("hr/M_hr_vacation_types");
         $this->load->model("hr/M_hr_job");
         	$this->load->model("hr/M_hr_employee");
		$this->load->model("hr/M_hr_vacation");
		$this->load->model("hr/M_hr_inouttimedata");
		$this->load->model("hr/M_hr_workgrouptime");
        $this->load->model("hr/M_hr_weekend");
		$this->load->model("hr/M_hr_employee_subsalary");
        $this->load->model("hr/M_hr_salary_disbursement");
        $this->load->model("hr/M_hr_workgroup");
        $this->load->model("account/M_fin_treeaccount");
        $this->load->model("account/M_fin_journal");


	}
	
	public function index()
	{
	    $data['date'] = set_value('fromdate');
	    $data['workgroup_id']=$Workgroup=set_value('workgroup_id');
		$data['DataRows'] = $this->getEmp($Workgroup);
		$data['DataWorkgroups'] = $this->M_hr_workgroup->GetMultiRow();
		$data['content_page'] = "hr/hr_salary_disbursement";
		$this->load->view('page', $data);
	}

	public function insertDisbursement()
	{
	    $emp_id= set_value('user');
	    $month= set_value('month');
	    $year=set_value('year');
	    $chick= $this->M_hr_salary_disbursement->GetCountRow($emp_id,$month,$year);
	    if($chick->cnt == 0){
	        $dt=$year."-".$month;
	        $salary=$this->getSalary($emp_id,$dt);
	        $data=array(
	            'emp_id' => $emp_id,
	            'month' => $month,
	            'year' => $year,
	            'salary' => $salary,
	            'insert_user' => intval($this->session->userdata('StaffID'))
	            );
	            $this->M_hr_salary_disbursement->InsertRecord($data);
	            
	            $LatestRecord=$this->M_hr_salary_disbursement->GetLatestRecord();
	            
	            
	          
	            
	            
	            //حساب الرواتب
	            $account_s =409;
	            //حساب الرواتب المستحقة
	            $account_as =331;
	            //حساب البنك
	            $account_bank =291;
	            //حساب الصندوق
	            $account_box =295;
	            
	            //قيد الاستحقاق
	              $this->fin_journal_main_insert(lang('salary_disbursement'), 0, $LatestRecord->max);
	            //من حساب رواتب
	            $this->fin_journal_insert($account_s, $salary, 0);
	            //الى حساب رواتب مستحقة
	            $this->fin_journal_insert($account_as, 0, $salary);
	            
	            
	            
	            //قيد الصرف
	              $this->fin_journal_main_insert(lang('salary_disbursement'), 0, $LatestRecord->max);
	            //من رواتب مستحقة
	            $this->fin_journal_insert($account_as, $salary, 0);
	            //الى حساب البنك
	            $this->fin_journal_insert($account_bank, 0, $salary);
	            
	            
	          $result=array("status" =>1);  
	            
	    }
	    else{
	         $result=array("status" =>-1);  
	    }
	     header('Content-Type: application/json');
			 echo json_encode($result);

	}
	public function insertMultiDisbursement()
	{
	     header('Content-Type: application/json');
			 echo json_encode($_POST);

	}
	
	public function getEmp($Workgroup)
	{
	    $employees=$this->M_hr_employee->GetMultiRow();
	     if($Workgroup == -1)
	     {
	         $employees=$this->M_hr_employee->GetMultiRow();
	     }else{
	         $employees=$this->M_hr_employee->GetReport(0,0,0,0,$Workgroup);
	     }
	         return $employees;

	}
	
	public function getSalary($emp_id,$date)
	{
	    $a_date = $date."-23";
		$f_date = $date."-1";
		$fromdate =date("Y-m-d", strtotime($f_date));
		$todate =date("Y-m-t", strtotime($a_date));
		$DtRow=$this->M_hr_employee->GetRow($emp_id);
		$basic_salary = $DtRow->basic_salary;
        $salary_days = $DtRow->salary_days;

        $DailySalary = $basic_salary / $salary_days;
	    $TotalAction1 = $this->M_hr_employee_subsalary->GetSumAction1($emp_id, $fromdate, $todate);
		$TotalAction2 = $this->M_hr_employee_subsalary->GetSumAction2($emp_id, $fromdate, $todate);
		$TotalAction3 = $this->M_hr_employee_subsalary->GetSumAction3($emp_id, $fromdate, $todate);
		$WorkDays = intval($this->M_hr_inouttimedata->CheckEmployee($emp_id, $fromdate, $todate));
		$WorkSalary = $DailySalary * $WorkDays;
		$FinalSalary = $WorkSalary + ($TotalAction1->thevalue) - ($TotalAction2->thevalue) - ($TotalAction3->thevalue);
		$FinalSalary2=number_format($FinalSalary, 2, '.', '');
		return $FinalSalary2;

	}

    public function fin_journal_main_insert($details, $bill_id, $bond_no)
    {
        $thedate = date('Y-m-d');
        $table_name="hr_salary_disbursement";
        $image="";
        $company_id = intval($this->session->userdata('company_id'));
        $user_id = intval($this->session->userdata('StaffID'));
        $automatic = 1;
        $deleted = 0;
        $fin_journal_array = array();
        $fin_journal_array = array(
            'company_id' => $company_id,
            'user_id' => $user_id,
            'details' => $details,
            'thedate' => $thedate,
            'bill_id' => $bill_id,
            'table_name' => $table_name,
            'bond_no' => $bond_no,
            'image' => $image,
            'automatic' => $automatic,
            'deleted' => $deleted,
        );
        $this->M_fin_journal_main->InsertRecord($fin_journal_array);
    }
    public function fin_journal_insert($account_id, $debit, $creditor)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $user_id = intval($this->session->userdata('StaffID'));
        $MainData = $this->M_fin_journal_main->GetLatestRecord($company_id, $user_id);
        $main_id = $MainData->id;
        $fin_journal_array = array();
        $fin_journal_array = array(
            'main_id' => $main_id,
            'account_id' => $account_id,
            'debit' => $debit,
            'creditor' => $creditor,
        );
        $this->M_fin_journal->InsertRecord($fin_journal_array);
    }
    
    
    public function dayCount($date, $day = 5) 
    {        
    	$f_date = $date."-1";
    	$t_date = $date."-23";
    	$f_date =date("Y-m-d", strtotime($f_date));
    	$t_day =date("t", strtotime($a_date));
    	$t_date =date("Y-m-d", strtotime($date.'-'.$t_day));
        $from = new DateTime($f_date);
        $to   = new DateTime($t_date);
        $wF = $from->format('w');
        $wT = $to->format('w');
        if ($wF < $wT)       $isExtraDay = $day >= $wF && $day <= $wT;
        else if ($wF == $wT) $isExtraDay = $wF == $day;
        else                 $isExtraDay = $day >= $wF || $day <= $wT;
        
        return floor($from->diff($to)->days / 7) + $isExtraDay;
    }
    
    public function ss()
    {
        echo "<br/> 10=> ".$this->dayCount('2021-10',5);
        echo "<br/> 11=> ".$this->dayCount('2021-11',5);
        echo "<br/> 12=> ".$this->dayCount('2021-12',5);
    }
    

}
