<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hr_inouttimedata extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
        $this->load->model("hr/M_hr_inouttimedata");
        $this->load->model("hr/M_hr_employee");
        $this->load->model("hr/M_hr_workgrouptime");
        $this->load->model("hr/M_hr_weekend");
	}
	
	public function index()
	{
		$data['DataRows'] = $this->M_hr_inouttimedata->GetMultiRow();
		$data['content_page'] = "hr/hr_inouttimedata";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "hr/hr_inouttimedata_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("employee_id","employee_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $employee_id = set_value("employee_id");

            $InvoiceItems  = date("t");
            $StartDate = date('Y-m-01');
            $NewData = array();
            for ($i = 1; $i <= $InvoiceItems; $i++) {
                $LoopDate = date('Y-m-d',strtotime($StartDate) + (24*60*60*($i - 1)));

                $Absence = "Absence".$i;
                $Absence = set_value($Absence);

                if($Absence == 0)
                {
                    $thedate = "thedate".$i;
                    $thedate = set_value($thedate);

                    $in_time = "in_time".$i;
                    $in_time = set_value($in_time);

                    $out_time = "out_time".$i;
                    $out_time = set_value($out_time);

                    $deleted = set_value('deleted');

                    $NewData = array(
                        'company_id' => $company_id,
                        'employee_id' => $employee_id,
                        'in_date' => $thedate,
                        'in_time' => $in_time,
                        'out_date' => $thedate,
                        'out_time' => $out_time,
                        'deleted' => $deleted,
                    );
        
                    $this->M_hr_inouttimedata->InsertRecord($NewData);
                }

            }
			redirect ('hr/hr_inouttimedata');
		}
	}

	public function updateform($rid)
	{
		$DataRow = $this->M_hr_inouttimedata->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
        $data['employee_id'] = $DataRow->employee_id;
		$data['in_date'] = $DataRow->in_date;
		$data['in_time'] = $DataRow->in_time;
		$data['out_time'] = $DataRow->out_time;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "hr/hr_inouttimedata_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("employee_id","employee_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $employee_id = set_value("employee_id");
			$in_date = set_value("thedate");
			$in_time = set_value('in_time');
			$out_date = set_value('thedate');
			$out_time = set_value('out_time');
			$deleted = set_value('deleted');

			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'employee_id' => $employee_id,
				'in_date' => $in_date,
				'in_time' => $in_time,
				'out_date' => $out_date,
				'out_time' => $out_time,
				'deleted' => $deleted,
			);

			$this->M_hr_inouttimedata->UpdateRecord($id, $NewData);
			
			redirect ('hr/hr_inouttimedata');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_hr_inouttimedata->UpdateRecord($rid, $NewData);

        redirect("hr/hr_inouttimedata");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_hr_inouttimedata->UpdateRecord($rid, $NewData);

        redirect("hr/hr_inouttimedata");
	}
}
