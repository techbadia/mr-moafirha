<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hr_employee extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
        $this->load->model("hr/M_hr_country");
        $this->load->model("hr/M_hr_education");
        $this->load->model("hr/M_hr_job");
        $this->load->model("hr/M_hr_job_type");
        $this->load->model("hr/M_hr_workgroup");
        $this->load->model("hr/M_hr_employee");
        $this->load->model("account/M_fin_treeaccount");
        $this->load->model("account/M_fin_journal");
	}
	
	public function index()
	{
		
		$data['DataRows'] = $this->M_hr_employee->GetMultiRow();
		$data['content_page'] = "hr/hr_employee";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "hr/hr_employee_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("country_id","country_id","required");
		$this->form_validation->set_rules("education_id","education_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
			$country_id = set_value('country_id');
            $education_id = set_value('education_id');
            $job_id = set_value('job_id');
            $job_type_id = set_value('job_type_id');
            $workgroup_id = set_value('workgroup_id');
            $ar_name = set_value('ar_name');
            $en_name = set_value('en_name');
            $pasport_no = set_value('pasport_no');
            $phone = set_value('phone');
            $email = set_value('email');
            $basic_salary = set_value('basic_salary');
            $salary_days = set_value('salary_days');
            $salary_day = set_value('salary_day');
            $add_money = set_value('add_money');
            $deviceno = set_value('deviceno');
            $another_address = set_value('another_address');
            $another_phone = set_value('another_phone');
            $another_email = set_value('another_email');
			$finish_date1 = set_value('finish_date1');
			$finish_date2 = set_value('finish_date2');
			$finish_date3 = set_value('finish_date3');
			$finish_date4 = set_value('finish_date4');
			$finish_date5 = set_value('finish_date5');
			$finish_date6 = set_value('finish_date6');
			$finish_date7 = set_value('finish_date7');
			$finish_date8 = set_value('finish_date8');
			$finish_date9 = set_value('finish_date9');
			$deleted = set_value('deleted');


			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
				'country_id' => $country_id,
                'education_id' => $education_id,
                'job_id' => $job_id,
                'job_type_id' => $job_type_id,
                'workgroup_id' => $workgroup_id,
                'ar_name' => $ar_name,
                'en_name' => $en_name,
                'pasport_no' => $pasport_no,
                'phone' => $phone,
                'email' => $email,
                'basic_salary' => $basic_salary,
                'salary_days' => $salary_days,
                'salary_day' => $salary_day,
                'add_money' => $add_money,
                'deviceno' => $deviceno,
                'another_address' => $another_address,
                'another_phone' => $another_phone,
                'another_email' => $another_email,
				'finish_date1' => $finish_date1,
				'finish_date2' => $finish_date2,
				'finish_date3' => $finish_date3,
				'finish_date4' => $finish_date4,
				'finish_date5' => $finish_date5,
				'finish_date6' => $finish_date6,
				'finish_date7' => $finish_date7,
				'finish_date8' => $finish_date8,
				'finish_date9' => $finish_date9,
				'deleted' => $deleted,
			);
			$this->M_hr_employee->InsertRecord($NewData);
			redirect ('hr/hr_employee');
		}
	}

	public function updateform($rid)
	{
		$DataRow = $this->M_hr_employee->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['country_id'] = $DataRow->country_id;
        $data['education_id'] = $DataRow->education_id;
        $data['job_id'] = $DataRow->job_id;
        $data['job_type_id'] = $DataRow->job_type_id;
        $data['workgroup_id'] = $DataRow->workgroup_id;
        $data['ar_name'] = $DataRow->ar_name;
        $data['en_name'] = $DataRow->en_name;
        $data['pasport_no'] = $DataRow->pasport_no;
        $data['phone'] = $DataRow->phone;
        $data['email'] = $DataRow->email;
        $data['basic_salary'] = $DataRow->basic_salary;
        $data['salary_days'] = $DataRow->salary_days;
        $data['salary_day'] = $DataRow->salary_day;
        $data['add_money'] = $DataRow->add_money;
        $data['deviceno'] = $DataRow->deviceno;
        $data['another_address'] = $DataRow->another_address;
        $data['another_phone'] = $DataRow->another_phone;
        $data['another_email'] = $DataRow->another_email;
		$data['finish_date1'] = $DataRow->finish_date1;
		$data['finish_date2'] = $DataRow->finish_date2;
		$data['finish_date3'] = $DataRow->finish_date3;
		$data['finish_date4'] = $DataRow->finish_date4;
		$data['finish_date5'] = $DataRow->finish_date5;
		$data['finish_date6'] = $DataRow->finish_date6;
		$data['finish_date7'] = $DataRow->finish_date7;
		$data['finish_date8'] = $DataRow->finish_date8;
		$data['finish_date9'] = $DataRow->finish_date9;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "hr/hr_employee_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("country_id","country_id","required");
		$this->form_validation->set_rules("education_id","education_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $country_id = set_value('country_id');
            $education_id = set_value('education_id');
            $job_id = set_value('job_id');
            $job_type_id = set_value('job_type_id');
            $workgroup_id = set_value('workgroup_id');
            $ar_name = set_value('ar_name');
            $en_name = set_value('en_name');
            $pasport_no = set_value('pasport_no');
            $phone = set_value('phone');
            $email = set_value('email');
            $basic_salary = set_value('basic_salary');
            $salary_days = set_value('salary_days');
            $salary_day = set_value('salary_day');
            $add_money = set_value('add_money');
            $deviceno = set_value('deviceno');
            $another_address = set_value('another_address');
            $another_phone = set_value('another_phone');
            $another_email = set_value('another_email');
			$finish_date1 = set_value('finish_date1');
			$finish_date2 = set_value('finish_date2');
			$finish_date3 = set_value('finish_date3');
			$finish_date4 = set_value('finish_date4');
			$finish_date5 = set_value('finish_date5');
			$finish_date6 = set_value('finish_date6');
			$finish_date7 = set_value('finish_date7');
			$finish_date8 = set_value('finish_date8');
			$finish_date9 = set_value('finish_date9');
			$deleted = set_value('deleted');

			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
				'country_id' => $country_id,
                'education_id' => $education_id,
                'job_id' => $job_id,
                'job_type_id' => $job_type_id,
                'workgroup_id' => $workgroup_id,
                'ar_name' => $ar_name,
                'en_name' => $en_name,
                'pasport_no' => $pasport_no,
                'phone' => $phone,
                'email' => $email,
                'basic_salary' => $basic_salary,
                'salary_days' => $salary_days,
                'salary_day' => $salary_day,
                'add_money' => $add_money,
                'deviceno' => $deviceno,
                'another_address' => $another_address,
                'another_phone' => $another_phone,
                'another_email' => $another_email,
				'finish_date1' => $finish_date1,
				'finish_date2' => $finish_date2,
				'finish_date3' => $finish_date3,
				'finish_date4' => $finish_date4,
				'finish_date5' => $finish_date5,
				'finish_date6' => $finish_date6,
				'finish_date7' => $finish_date7,
				'finish_date8' => $finish_date8,
				'finish_date9' => $finish_date9,
				'deleted' => $deleted,
			);
			

			$this->M_hr_employee->UpdateRecord($id, $NewData);
			
			redirect ('hr/hr_employee');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_hr_employee->UpdateRecord($rid, $NewData);

        redirect("hr/hr_employee");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_hr_employee->UpdateRecord($rid, $NewData);

        redirect("hr/hr_employee");
	}
}
