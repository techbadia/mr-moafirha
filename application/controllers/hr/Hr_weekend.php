<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hr_weekend extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
        $this->load->model("hr/M_hr_weekend");
        $this->load->model("hr/M_hr_employee");
	}
	
	public function index()
	{
		
		$data['DataRows'] = $this->M_hr_weekend->GetMultiRow();
		$data['content_page'] = "hr/hr_weekend";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "hr/hr_weekend_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("dayoffer","dayoffer","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $title = set_value('title');
            $ar_title = "";
            $en_title = "";
            if($title == 1)
            {
                $ar_title = "السبت";
                $en_title = "Saturday";
            }
            else if($title == 2)
            {
                $ar_title = "الأحد";
                $en_title = "Sunday";
            }
            else if($title == 3)
            {
                $ar_title = "الإثنين";
                $en_title = "Monday";
            }
            else if($title == 4)
            {
                $ar_title = "الثلاثاء";
                $en_title = "Tuesday";
            }
            else if($title == 5)
            {
                $ar_title = "الأربعاء";
                $en_title = "Wednesday";
            }
            else if($title == 6)
            {
                $ar_title = "الخميس";
                $en_title = "Thursday";
            }
            else
            {
                $ar_title = "الجمعة";
                $en_title = "Friday";
            }
			$dayoffer = set_value('dayoffer');
			$deleted = set_value('deleted');


			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
				'ar_title' => $ar_title,
                'en_title' => $en_title,
                'dayoffer' => $dayoffer,
				'deleted' => $deleted,
			);

			$this->M_hr_weekend->InsertRecord($NewData);
			
			redirect ('hr/hr_weekend');
		}
	}

	public function updateform($rid)
	{
		$DataRow = $this->M_hr_weekend->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['ar_title'] = $DataRow->ar_title;
        $data['en_title'] = $DataRow->en_title;
        $data['dayoffer'] = $DataRow->dayoffer;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "hr/hr_weekend_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("dayoffer","dayoffer","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
			$title = set_value('title');
            $dayoffer = set_value('dayoffer');
            $ar_title = "";
            $en_title = "";
            if($title == 1)
            {
                $ar_title = "السبت";
                $en_title = "Saturday";
            }
            else if($title == 2)
            {
                $ar_title = "الأحد";
                $en_title = "Sunday";
            }
            else if($title == 3)
            {
                $ar_title = "الإثنين";
                $en_title = "Monday";
            }
            else if($title == 4)
            {
                $ar_title = "الثلاثاء";
                $en_title = "Tuesday";
            }
            else if($title == 5)
            {
                $ar_title = "الأربعاء";
                $en_title = "Wednesday";
            }
            else if($title == 6)
            {
                $ar_title = "الخميس";
                $en_title = "Thursday";
            }
            else
            {
                $ar_title = "الجمعة";
                $en_title = "Friday";
            }
			$deleted = set_value('deleted');

			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
				'ar_title' => $ar_title,
                'en_title' => $en_title,
                'dayoffer' => $dayoffer,
				'deleted' => $deleted,
			);
			

			$this->M_hr_weekend->UpdateRecord($id, $NewData);
			
			redirect ('hr/hr_weekend');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_hr_weekend->UpdateRecord($rid, $NewData);

        redirect("hr/hr_weekend");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_hr_weekend->UpdateRecord($rid, $NewData);

        redirect("hr/hr_weekend");
	}
}
