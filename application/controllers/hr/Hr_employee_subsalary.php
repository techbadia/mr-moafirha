<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hr_employee_subsalary extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
        $this->load->model("hr/M_hr_employee_subsalary");
        $this->load->model("hr/M_hr_employee");
        $this->load->model("account/M_fin_treeaccount");
	}
	
	public function index()
	{
		
		$data['DataRows'] = $this->M_hr_employee_subsalary->GetMultiRow();
		$data['content_page'] = "hr/hr_employee_subsalary";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "hr/hr_employee_subsalary_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("employee_id","employee_id","required");
		$this->form_validation->set_rules("actionid","actionid","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
			$employee_id = set_value('employee_id');
            $actionid = set_value('actionid');
            $thedate = set_value('thedate');
            $thevalue = set_value('thevalue');
            $notes = set_value('notes');
            $accept = set_value('accept');
			$deleted = set_value('deleted');
            $tree_id = set_value('tree_id');

			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
				'employee_id' => $employee_id,
                'actionid' => $actionid,
                'thedate' => $thedate,
                'thevalue' => $thevalue,
                'notes' => $notes,
                'accept' => $accept,
				'deleted' => $deleted,
			);

			$this->M_hr_employee_subsalary->InsertRecord($NewData);
            
            /////////////////////////
            $acc_hr_loan = intval($this->session->userdata('acc_hr_loan'));
            $FindAccountCategory = $this->M_fin_treeaccount->GetRow($acc_hr_loan);
            $parent_id = $FindAccountCategory->parent_id;

            $EmployeeData = $this->M_hr_employee->GetRow($employee_id);
            $title = $EmployeeData->ar_name;
            $title_en = $EmployeeData->en_name;
            if($actionid == 3)
            {
                if($tree_id > 0)
                {
                    $CheckAccount = $this->M_fin_treeaccount->CheckLoanFind($employee_id);
                    if($CheckAccount == 0)
                    {
                        $NewData = array();
                        $NewData = array(
                            'company_id' => $company_id,
                            'category_id' => 1,
                            'parent_id' => $acc_hr_loan,
                            'account_no' => $employee_id,
                            'title' => $title,
                            'title_en' => $title_en,
                            'startamount' => 0,
                            'basic' => 0,
                            'level_no' => 0,
                            'loan_user_id' => $employee_id,
                            'deleted' => $deleted,
                        );
            
                        $this->M_fin_treeaccount->InsertRecord($NewData);
                    }
                    
                    $CheckAccount = $this->M_fin_treeaccount->CheckLoanFind($employee_id);
                    $LoanAccount = $this->M_fin_treeaccount->GetLoanAccount($employee_id);
                    $LoanAccountID = $LoanAccount->id;

                    $LatestRecord = $this->M_hr_employee_subsalary->GetLatestRecord($company_id, $employee_id);
                    $details = $notes;
                    $bill_id = $LatestRecord->id;
                    $table_name = "hr_employee_subsalary";
                    $bond_no = $LatestRecord->id;
                    $NewFileName = "";
                    $this->fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $NewFileName);
                    //من حساب
                    $account_id = $LoanAccountID;
                    $debit = $thevalue;
                    $creditor = 0;
                    $this->fin_journal_insert($account_id, $debit, $creditor);
                    //إلى حساب
                    $account_id = $tree_id;
                    $debit = 0;
                    $creditor = $thevalue;
                    $this->fin_journal_insert($account_id, $debit, $creditor);
                }
            }
            /////////////////////////

			redirect ('hr/hr_employee_subsalary');
		}
    }
    
    public function fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $image)
	{
		$company_id = intval($this->session->userdata('company_id'));
		$user_id = intval($this->session->userdata('StaffID'));
		$automatic = 1;
		$deleted = 0;

		$fin_journal_array = array();
		$fin_journal_array = array(
			'company_id' => $company_id,
			'user_id' => $user_id,
			'details' => $details,
			'thedate' => $thedate,
			'bill_id' => $bill_id,
			'table_name' => $table_name,
			'bond_no' => $bond_no,
			'image' => $image,
			'automatic' => $automatic,
			'deleted' => $deleted,
		);

		$this->M_fin_journal_main->InsertRecord($fin_journal_array);
	}

	public function fin_journal_insert($account_id, $debit, $creditor)
	{
		$company_id = intval($this->session->userdata('company_id'));
		$user_id = intval($this->session->userdata('StaffID'));

		$MainData = $this->M_fin_journal_main->GetLatestRecord($company_id, $user_id);
		$main_id = $MainData->id;

		$fin_journal_array = array();
		$fin_journal_array = array(
			'main_id' => $main_id,
			'account_id' => $account_id,
			'debit' => $debit,
			'creditor' => $creditor,
		);

		$this->M_fin_journal->InsertRecord($fin_journal_array);
	}

	public function updateform($rid)
	{
		$DataRow = $this->M_hr_employee_subsalary->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['employee_id'] = $DataRow->employee_id;
        $data['actionid'] = $DataRow->actionid;
        $data['thedate'] = $DataRow->thedate;
        $data['thevalue'] = $DataRow->thevalue;
        $data['notes'] = $DataRow->notes;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "hr/hr_employee_subsalary_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("employee_id","employee_id","required");
		$this->form_validation->set_rules("actionid","actionid","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
			$employee_id = set_value('employee_id');
            $actionid = set_value('actionid');
            $thedate = set_value('thedate');
            $thevalue = set_value('thevalue');
            $notes = set_value('notes');
            $accept = set_value('accept');
			$deleted = set_value('deleted');

			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
				'employee_id' => $employee_id,
                'actionid' => $actionid,
                'thedate' => $thedate,
                'thevalue' => $thevalue,
                'notes' => $notes,
                'accept' => $accept,
				'deleted' => $deleted,
			);
			

			$this->M_hr_employee_subsalary->UpdateRecord($id, $NewData);
			
			redirect ('hr/hr_employee_subsalary');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_hr_employee_subsalary->UpdateRecord($rid, $NewData);

        redirect("hr/hr_employee_subsalary");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_hr_employee_subsalary->UpdateRecord($rid, $NewData);

        redirect("hr/hr_employee_subsalary");
	}
}
