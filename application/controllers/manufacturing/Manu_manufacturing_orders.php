<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manu_manufacturing_orders extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		
		$this->load->model("manufacturing/M_manu_manufacturing_orders");
		$this->load->model("manufacturing/M_manu_manufacturing_orders_items");
		$this->load->model("purchase/M_buy_manufacturer");
		$this->load->model("store/M_product");
		$this->load->model("account/M_fin_treeaccount");
		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");
	}
	
	public function index()
	{
		$this->session->set_userdata('Filter', lang('manu_manufacturing_orders_date_all'));

		$data['DataRows'] = $this->M_manu_manufacturing_orders->GetMultiRow();
		$data['content_page'] = "manufacturing/manu_manufacturing_orders";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$this->session->set_userdata('Filter', "");

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "manufacturing/manu_manufacturing_orders_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		$this->session->set_userdata('Filter', "");

		$company_id = intval($this->session->userdata('company_id'));
		$StaffID = intval($this->session->userdata('StaffID'));

		$this->form_validation->set_rules("over_cost","over_cost","required");
		$this->form_validation->set_rules("notes","notes","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = $company_id;
			$user_id = $StaffID;
			$thedate = set_value('thedate');
			$over_cost = set_value('over_cost');
			$notes = set_value('notes');
			$deleted = set_value('deleted');


			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'user_id' => $user_id,
				'thedate' => $thedate,
				'over_cost' => $over_cost,
				'notes' => $notes,
				'deleted' => $deleted,
			);

			$this->M_manu_manufacturing_orders->InsertRecord($NewData);

			$LatestRecord = $this->M_manu_manufacturing_orders->GetLatestRecord($company_id, $user_id);
			$bill_id = $LatestRecord->id;

			$InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
			//M_product
			$barcode = "barcode";
			$location_id = "location_id";
			$quantity = "quantity";
			for ($i = 1; $i <= $InvoiceItems; $i++) {
				$quantity = "quantity".$i;
				$quantity = $this->input->post($quantity);
				if($quantity > 0)
				{
					$barcode = "barcode".$i;
					$barcode = $this->input->post($barcode);
					$ProductData = $this->M_product->GetByBarcode($barcode);
					$product_id = $ProductData->id;
					$location_id = "location_id".$i;
					$location_id = $this->input->post($location_id);
					$unitprice = "unitprice".$i;
					$unitprice = $this->input->post($unitprice);

					//M_manu_manufacturing_orders_items
					$ItemsArray = array();
					$ItemsArray = array(
						'bill_id' => $bill_id,
						'store_type_id' => $product_id,
						'location_id' => $location_id,
						'quantity' => $quantity,
						'unitprice' => $unitprice,
					);
					$this->M_manu_manufacturing_orders_items->InsertRecord($ItemsArray);
				}
				
			}

			redirect ('manufacturing/manu_manufacturing_orders');
		}
	}


	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_manu_manufacturing_orders->UpdateRecord($rid, $NewData);

        redirect("manufacturing/manu_manufacturing_orders");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_manu_manufacturing_orders->UpdateRecord($rid, $NewData);

        redirect("manufacturing/manu_manufacturing_orders");
	}

	public function FillProductsList($barcode){
        $CodeCount = $this->M_product->CheckBarcode($barcode);
        if($CodeCount > 0)
        {
            $TypeName = $this->M_product->GetByBarcode($barcode);
            echo $TypeName->title;
        }
        else
        {
            echo "-";
        }
	}

	public function view_main()
	{
		$id = $this->input->get('id');
		$MainDataRow = $this->M_manu_manufacturing_orders->GetInvoiceHeader($id);
		echo json_encode($MainDataRow); 
    	exit();
	}

	public function view_details()
	{
		$id = $this->input->get('id');
		//$id = intval($this->session->userdata('Fin_journal_View_ID'));
		$DetailsRows = $this->M_manu_manufacturing_orders->GetInvoiceDetails($id);
		echo json_encode($DetailsRows); 
		exit();
	}
}
