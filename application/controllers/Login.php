<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();

		$StaffID = intval($this->session->userdata('StaffID'));
        if (!empty($StaffID))
        {
            redirect ('/');
        }
        if ($StaffID == 0)
        {
            //redirect ('login');
        }
		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}

		$this->load->model("settings/M_settings_acceptance");
		$this->load->model("settings/M_settings_notifications");
	}

	public function index()
	{

		$this->load->helper('cookie');
		$this->config->load('settings/config_setting');
		$Segment2 = $this->uri->segment(1);
		//echo $this->session->userdata('package_title');
		//echo $this->session->userdata('Package');


		if ($this->config->item('MainLanguage') == "ar")
		{
			$Config_File_Lang = "_ar";
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
		{
			$Config_File_Lang = "_en";
			$this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		//$this->session->userdata('Package')
		//$PackageName = $this->session->userdata('PackageName');


		//$this->session->set_userdata('package_title', $PackageName);


		//$this->session->set_userdata('Package', $this->config->item('package_title'));

		$data['app_title'] = $this->config->item('package_title');
		$AppData = $this->M_app_element->GetApp();
		$PackageName = $AppData->PackageName;
		$POS_available = $AppData->pos;
		$project_available = $AppData->project;
		$this->session->set_userdata('POS_available', $POS_available);
		$this->session->set_userdata('project_available', $project_available);
		$this->session->set_userdata('PackageName', $PackageName);
		if($PackageName == "")
		{
			$PackageName = $this->session->userdata('PackageName');
		}
		if($PackageName == "")
		{
			$PackageName = $this->session->userdata('Package');
		}
		//echo $PackageName;

		$statistics_page = "statistics_".$PackageName;
		$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
		$this->config->load($Config_File);

		$data['package_title'] = $this->config->item('package_title');
		$this->session->set_userdata('package_title', $this->config->item('package_title'));



		$data['content_page'] = "statistics_".$PackageName;
		$this->load->view('login', $data);
	}
	public function forgotPassword()
	{
		$this->load->view('forgotPassword', $data);

	}
	public function resetPassword()
	{
		$this->db->where("reset_password_code",$_GET['code']);
		$query = $this->db->get("usr_users");
		$user = $query->row();
		// var_dump($user);die();
		if(empty($user) || empty($_GET['code']))
		{
			$this->session->set_flashdata("error","الرابط غير صحيح");
			redirect("login/forgotPassword");
		}
		$this->load->view('resetPassword', $data);
	}
	public function changePassword()
	{
		$this->load->library('form_validation','arabic');

		$this->form_validation->set_rules("password","password","matches[confirm_password]");
		$this->form_validation->set_rules("confirm_password","confirm_password","matches[password]");

		if ($this->form_validation->run() == FALSE) {
						//$this->load->view('index', $data);
						$this->session->set_flashdata("error","يجب ان تكون كلمة المرور و تأكيد كلمة المرور متطابقتين");
						redirect($_SERVER["HTTP_REFERER"]);
				}
		else
		{
			$data['password'] = md5($_POST['password']);
			$data['reset_password_code'] = NULL;
			$this->db->where('id', $_SESSION['reset_user_id']);
			$this->db->update("usr_users",$data);
			$this->session->set_flashdata("success","تم تعديل كلمة المرور بنجاح");
			redirect("login");

		}
	}
	public function sendCodeToEmail()
	{
		$this->form_validation->set_rules("email","email","required");

		if ($this->form_validation->run() == FALSE) {
						//$this->load->view('index', $data);
						$this->session->set_flashdata("error","من فضلك قم بادخال البريد الالكتروني");
						redirect("login/forgotPassword");
				}
		else
		{
				$this->db->where("email",$_POST['email']);
				$query = $this->db->get("usr_users");
				$user = $query->row();
				// var_dump($user);die();
				if(empty($user))
				{
					$this->session->set_flashdata("error","البريد الالكتروني غير صحيح");
					redirect("login/forgotPassword");
				}
				$data['reset_password_code'] = $this->generateRandomInteger(6);
				$_SESSION['reset_user_id'] = $user->id;
				$this->db->where('id', $user->id);
				$this->db->update("usr_users",$data);
				$this->load->library('email');
				$this->email->from("info@erp.com", "ERP");
				$this->email->to($user->email);

				//$this->email->cc('hamada.abdelazeez@gmail.com');
				$this->email->subject("كلمة المرور الجديدة لحسابك - ERP ");
				// $this->email->set_mailtype('html');
				$message = "";
				$message .= "السلام عليكم ورحمة الله وبركاته<br />";
				$message .= "بناءا على طلبكم بخصوص استرجاع كلمة المرور الخاصة بحسابكم على لوحة التحكم <br />";
				$message .= "نرسل اليكم رابط تغيير كلمة المرور <br />";
				$url = base_url()."login/resetPassword?code=".$data['reset_password_code'];
				$message .= '<a href="'.$url.'">اضغط هنا</a>';
				// var_dump($message);die();
				$this->email->message($message);
				$result = $this->email->send();
				// var_dump($this->email->print_debugger());die();
				$this->session->set_flashdata("success","تم ارسال رابط استرجاع كلمة المرور الى بريدك الالكتروني");
				// redirect("login");
				redirect("login/forgotPassword");

		}
	}
	function generateRandomInteger($length = 5) {
	    $characters = '123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function checklogin()
	{
		$this->config->load('settings/config_setting');

		$this->form_validation->set_rules("username","username","required");
		$this->form_validation->set_rules("password","password","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
        }
		else
		{
			$Package = $this->session->userdata('Package');
			$this->config->set_item('Package', $Package);

			$username = set_value('username');
			$password = md5(set_value("password"));

			$UserRowNum = $this->M_usr_users->GetUserRow($username, $password);
			if ($UserRowNum >0)
			{
				$UserData = $this->M_usr_users->CheckUser($username, $password);
				$StaffID = intval($UserData->id);
				$this->session->set_userdata('StaffID', $StaffID);
				$company_id = $UserData->company_id;
				$this->session->set_userdata('company_id', $company_id);
				$GroupID = $UserData->group_id;
				$this->session->set_userdata('GroupID', $GroupID);
				$branches_lookup = $UserData->branches_lookup;
				$this->session->set_userdata('branches_lookup', $branches_lookup);
				//Financial settings
				$DataRow = $this->M_fin_settings->GetRow();
				$this->session->set_userdata('acc_sale', $DataRow->acc_sale);
				$this->session->set_userdata('acc_website_sale', $DataRow->acc_website_sale);
				$this->session->set_userdata('acc_sale_reurn', $DataRow->acc_sale_reurn);
				$this->session->set_userdata('acc_buy', $DataRow->acc_buy);
				$this->session->set_userdata('acc_buy_reurn', $DataRow->acc_buy_reurn);
				$this->session->set_userdata('acc_treasury', $DataRow->acc_treasury);
				$this->session->set_userdata('acc_bank', $DataRow->acc_bank);
				$this->session->set_userdata('acc_expenses', $DataRow->acc_expenses);
				$this->session->set_userdata('acc_revenues', $DataRow->acc_revenues);
				$this->session->set_userdata('acc_debtors', $DataRow->acc_debtors);
				$this->session->set_userdata('acc_creditors', $DataRow->acc_creditors);
				$this->session->set_userdata('acc_capture_papers', $DataRow->acc_capture_papers);
				$this->session->set_userdata('acc_payment_papers', $DataRow->acc_payment_papers);
				$this->session->set_userdata('acc_inventory', $DataRow->acc_inventory);
				$this->session->set_userdata('acc_supplier', $DataRow->acc_supplier);
				$this->session->set_userdata('acc_customer', $DataRow->acc_customer);
				$this->session->set_userdata('acc_material_store', $DataRow->acc_material_store);
				$this->session->set_userdata('acc_damaged_store', $DataRow->acc_damaged_store);
				$this->session->set_userdata('acc_invoice_items', $DataRow->acc_invoice_items);
				$this->session->set_userdata('acc_custody', $DataRow->acc_custody);
				$this->session->set_userdata('acc_affiliate', $DataRow->acc_affiliate);
				$this->session->set_userdata('acc_delivery', $DataRow->acc_delivery);
				$this->session->set_userdata('acc_factory_cost', $DataRow->acc_factory_cost);
				$this->session->set_userdata('acc_operating_expenses', $DataRow->acc_operating_expenses);
				$this->session->set_userdata('acc_salary', $DataRow->acc_salary);
				$this->session->set_userdata('acc_tax_sale', $DataRow->acc_tax_sale);
				$this->session->set_userdata('acc_tax_sale_percent', $DataRow->acc_tax_sale_percent);
				$this->session->set_userdata('acc_tax_purchase', $DataRow->acc_tax_purchase);
				$this->session->set_userdata('acc_tax_purchase_percent', $DataRow->acc_tax_purchase_percent);
				$this->session->set_userdata('currency_id', $DataRow->currency_id);
				$this->session->set_userdata('acc_sales_commission', $DataRow->acc_sales_commission);
				$this->session->set_userdata('acc_fixed_assets', $DataRow->acc_fixed_assets);
				$this->session->set_userdata('acc_current_assets', $DataRow->acc_current_assets);

				$currency_id = $DataRow->currency_id;
				$CurrencyData = $this->M_currencies->GetRow($currency_id);
				if ($this->session->userdata('lang') == "ar")
				{
					$this->session->set_userdata('currency_title', $CurrencyData->title);
					$this->session->set_userdata('currency_symbol', $CurrencyData->symbol);
				}
				else
				{
					$this->session->set_userdata('currency_title', $CurrencyData->title_en);
					$this->session->set_userdata('currency_symbol', $CurrencyData->symbol_en);
				}

				$this->session->set_userdata('header', $DataRow->header);
				$this->session->set_userdata('footer', $DataRow->footer);

				$this->Load_Acceptance();
				$this->Notifications();
				$this->Load_Printing();

				$PackageName = $this->session->userdata('PackageName');
				redirect ($PackageName);
			}
			else
			{
				redirect('login');
			}
		}
	}

	public function Load_Acceptance()
	{
		$DataRow = $this->M_settings_acceptance->GetRow();
		//$this->session->set_userdata('company_id', $DataRow->company_id);
		$this->session->set_userdata('Acc_arabic_required', $DataRow->arabic_required);
		$this->session->set_userdata('Acc_english_required', $DataRow->english_required);
		$this->session->set_userdata('Acc_upload_required', $DataRow->upload_required);
		$this->session->set_userdata('Acc_fin_option1', $DataRow->fin_option1);
		$this->session->set_userdata('Acc_fin_option2', $DataRow->fin_option2);
		$this->session->set_userdata('Acc_fin_option3', $DataRow->fin_option3);
		$this->session->set_userdata('Acc_purchase_option1', $DataRow->purchase_option1);
		$this->session->set_userdata('Acc_sales_option1', $DataRow->sales_option1);
		$this->session->set_userdata('Acc_sales_option2', $DataRow->sales_option2);
		$this->session->set_userdata('Acc_store_option1', $DataRow->store_option1);
		$this->session->set_userdata('Acc_hr_option1', $DataRow->hr_option1);
		$this->session->set_userdata('Acc_hr_option2', $DataRow->hr_option2);
		$this->session->set_userdata('Acc_hr_option3', $DataRow->hr_option3);
		$this->session->set_userdata('Acc_custody_option1', $DataRow->custody_option1);
		$this->session->set_userdata('Acc_custody_option2', $DataRow->custody_option2);
		$this->session->set_userdata('Acc_service_option1', $DataRow->service_option1);
		$this->session->set_userdata('Acc_maintenance_option1', $DataRow->maintenance_option1);
		$this->session->set_userdata('Acc_manufacturing_option1', $DataRow->manufacturing_option1);
	}

	public function Notifications()
	{
		$DataRow = $this->M_settings_notifications->GetRow();
		//$this->session->set_userdata('company_id', $DataRow->company_id);
		$this->session->set_userdata('Not_fin_option1', $DataRow->fin_option1);
		$this->session->set_userdata('Not_fin_option2', $DataRow->fin_option2);
		$this->session->set_userdata('Not_fin_option3', $DataRow->fin_option3);
		$this->session->set_userdata('Not_fin_option4', $DataRow->fin_option4);
		$this->session->set_userdata('Not_purchase_option1', $DataRow->purchase_option1);
		$this->session->set_userdata('Not_sales_option1', $DataRow->sales_option1);
		$this->session->set_userdata('Not_store_option1', $DataRow->store_option1);
		$this->session->set_userdata('Not_hr_option1', $DataRow->hr_option1);
		$this->session->set_userdata('Not_hr_option2', $DataRow->hr_option2);
		$this->session->set_userdata('Not_hr_option3', $DataRow->hr_option3);
		$this->session->set_userdata('Not_hr_option4', $DataRow->hr_option4);
		$this->session->set_userdata('Not_custody_option1', $DataRow->custody_option1);
		$this->session->set_userdata('Not_custody_option2', $DataRow->custody_option2);
		$this->session->set_userdata('Not_service_option1', $DataRow->service_option1);
		$this->session->set_userdata('Not_maintenance_option1', $DataRow->maintenance_option1);
		$this->session->set_userdata('Not_maintenance_option2', $DataRow->maintenance_option2);
		$this->session->set_userdata('Not_manufacturing_option1', $DataRow->manufacturing_option1);
	}

	public function Load_Printing()
	{
		$DataRow = $this->M_settings_print->GetRow();
		//$this->session->set_userdata('company_id', $DataRow->company_id);
		$this->session->set_userdata('buy_bill_ar_text', $DataRow->buy_bill_ar_text);
		$this->session->set_userdata('buy_bill_en_text', $DataRow->buy_bill_en_text);
		$this->session->set_userdata('buy_bill_orientation', $DataRow->buy_bill_orientation);
		$this->session->set_userdata('buy_bill_size', $DataRow->buy_bill_size);
		$this->session->set_userdata('buy_returns_ar_text', $DataRow->buy_returns_ar_text);
		$this->session->set_userdata('buy_returns_en_text', $DataRow->buy_returns_en_text);
		$this->session->set_userdata('buy_returns_orientation', $DataRow->buy_returns_orientation);
		$this->session->set_userdata('buy_returns_size', $DataRow->buy_returns_size);
		$this->session->set_userdata('sale_bill_ar_text', $DataRow->sale_bill_ar_text);
		$this->session->set_userdata('sale_bill_en_text', $DataRow->sale_bill_en_text);
		$this->session->set_userdata('sale_bill_orientation', $DataRow->sale_bill_orientation);
		$this->session->set_userdata('sale_bill_size', $DataRow->sale_bill_size);
		$this->session->set_userdata('sale_returns_ar_text', $DataRow->sale_returns_ar_text);
		$this->session->set_userdata('sale_returns_en_text', $DataRow->sale_returns_en_text);
		$this->session->set_userdata('sale_returns_orientation', $DataRow->sale_returns_orientation);
		$this->session->set_userdata('sale_returns_size', $DataRow->sale_returns_size);
		$this->session->set_userdata('data_invoice_ar_text', $DataRow->data_invoice_ar_text);
		$this->session->set_userdata('data_invoice_en_text', $DataRow->data_invoice_en_text);
		$this->session->set_userdata('data_invoice_orientation', $DataRow->data_invoice_orientation);
		$this->session->set_userdata('data_invoice_size', $DataRow->data_invoice_size);
	}
}
