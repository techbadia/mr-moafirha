<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usr_users extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }
        $this->load->model('store/M_store');
        $this->load->model('permissions/M_usr_users');
	}

	public function index()
	{
		$this->load->model('permissions/M_usr_users');
        $data['MultiRows'] = $this->M_usr_users->GetMultiRow();
        
        $data['content_page'] = "permissions/usr_users";
		$this->load->view('page', $data);
	}
    
    public function insertform()
    {   
        $this->load->model("M_usr_usersgroup");
        $data['AllGroups'] = $this->M_usr_usersgroup->GetMultiRow();
        
        $data['content_page'] = "permissions/usr_users_insert";
        $this->load->view('page', $data);
    }
    
    public function insert_data()
    {
        $this->load->helper('security');
        $this->load->library("form_validation");

        $company_id = intval($this->session->userdata('company_id'));

        $this->form_validation->set_rules("group_id","group_id","required");
        $this->form_validation->set_rules("fullname","fullname","required");
        $this->form_validation->set_rules("email","email","required");
        $this->form_validation->set_rules("username","username","required");
        $this->form_validation->set_rules("password","password","required");
        $this->form_validation->set_rules("active","active","required");
        
        if ($this->form_validation->run() == FALSE) {
        echo validation_errors();
        }
        else {
            
            $group_id = set_value('group_id');
            $fullname = set_value('fullname');
            $email = set_value('email');
            $username = set_value('username');
            $password = md5(set_value('password'));
            $active = set_value('active');
            $branches_lookup = set_value('branches_lookup');
			$sale_start = set_value('sale_start');
			$branch = set_value('branch');
            $deleted = 0;

            $filename = $password;
            $this->load->helper('text');
            $filename = str_replace(" ", "_", $filename);
            $filename = rtrim($filename);
            $exts = explode(".", $_FILES['image']['name']);
            $exts = end($exts);
            
            $UploadPath = "upload/";
            $ImageDirectory = $UploadPath."usr_users/";
            if (!file_exists($ImageDirectory)) {
                mkdir($ImageDirectory, 0777, true);
            }

            $config['upload_path'] = $ImageDirectory;
            $config['allowed_types'] = "*";
            $config['file_name'] = $filename.".".$exts;
            $config['overwrite'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            $NewFileName = $filename.".".$exts;
            
            if ( ! $this->upload->do_upload('image', $NewFileName))
            {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('upload_form', $error);
            }
            $this->upload->do_upload('image', $NewFileName);
            
            if ($this->M_usr_users->CheckDuplicate($username, $password) ==0)
            {
                $NewData = array();
                $NewData = array(
                    'company_id' => $company_id,
                    'group_id' => $group_id,
                    'fullname' => $fullname,
                    'email' => $email,
                    'username' => $username,
                    'password' => $password,
                    'active' => $active,
                    'image' => $NewFileName,
                    'branches_lookup' => $branches_lookup,
					'sale_start' => $sale_start,
					'branch' => $branch,
                    'deleted' => $deleted,
                );

                $this->M_usr_users->InsertRecord($NewData);
            }

            $StoreData = array();
			$AccountData = array();
            $GetThisUser = $this->M_usr_users->CheckUser($username, $password);
            foreach ($_REQUEST['UserStore'] as $UserStore)
            {
                $user_id = $GetThisUser->id;
                $StoreData = array(
                    'user_id' => $user_id,
                    'store_id' => $UserStore,
                );
                $this->M_usr_users_store->InsertRecord($StoreData);
                
            }
			
			foreach ($_REQUEST['UserAccounts'] as $UserAccounts)
            {
                $user_id = $GetThisUser->id;
                $AccountData = array(
                    'user_id' => $user_id,
                    'account_id' => $UserAccounts,
                );
                $this->M_usr_users_account->InsertRecord($AccountData);
            }
            
            $this->load->model("M_usr_usersgroup");
            $data['AllGroups'] = $this->M_usr_usersgroup->GetMultiRow();
        
            $data['MultiRows'] = $this->M_usr_users->GetMultiRow();

            redirect ('permissions/usr_users');
        }
    }
    
    public function updateform($rid)
    {
    	$MyRecord = $this->M_usr_users->GetRow($rid);

        $data['DataRow_id'] = $MyRecord->id;
        $data['DataRow_group_id'] = $MyRecord->group_id;
        $data['DataRow_fullname'] = $MyRecord->fullname;
        $data['DataRow_email'] = $MyRecord->email;
        $data['DataRow_username'] = $MyRecord->username;
        $data['DataRow_password'] = $MyRecord->password;
        $data['DataRow_active'] = $MyRecord->active;
        $data['DataRow_branches_lookup'] = $MyRecord->branches_lookup;
		$data['DataRow_sale_start'] = $MyRecord->sale_start;
		$data['DataRow_branch'] = $MyRecord->branch;
        
        $this->load->model("M_usr_usersgroup");
        $data['AllGroups'] = $this->M_usr_usersgroup->GetMultiRow();
            
        $data['content_page'] = "permissions/usr_users_update";
        $this->load->view('page', $data);
    }
    
    public function update_data()
    {
    	$this->load->helper('security');
        $this->load->library("form_validation");
        
        $this->form_validation->set_rules("group_id","group_id","required");
        $this->form_validation->set_rules("fullname","fullname","required");
        $this->form_validation->set_rules("email","email","required");
        $this->form_validation->set_rules("username","username","required");
        $this->form_validation->set_rules("password","password","required");
        $this->form_validation->set_rules("active","active","required");
        
        if ($this->form_validation->run() == FALSE) {
            echo validation_errors();
        }
        else {
            $id = set_value('id');
            $group_id = set_value('group_id');
            $fullname = set_value('fullname');
            $email = set_value('email');
            $username = set_value('username');
            $password = md5(set_value('password'));
            $active = set_value('active');
            $branches_lookup = set_value('branches_lookup');
			$branch = set_value('branch');
            
            $ChangePassword = set_value('ChangePassword');
            $ChangeImage = set_value('ChangeImage');
            
            if ($ChangeImage =="True")
            {
                $filename = $password;
                $this->load->helper('text');
                $filename = str_replace(" ", "_", $filename);
                $filename = rtrim($filename);
                $exts = explode(".", $_FILES['image']['name']);
                $exts = end($exts);
                
                $UploadPath = "upload/";
                $ImageDirectory = $UploadPath."usr_users/";
                if (!file_exists($ImageDirectory)) {
                    mkdir($ImageDirectory, 0777, true);
                }

                $config['upload_path'] = $ImageDirectory;
                $config['allowed_types'] = "*";
                $config['file_name'] = $filename.".".$exts;
                $config['overwrite'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                $NewFileName = $filename.".".$exts;
                
                if ( ! $this->upload->do_upload('image', $NewFileName))
                {
                    $error = array('error' => $this->upload->display_errors());
                    $this->load->view('upload_form', $error);
                }
                $this->upload->do_upload('image', $NewFileName);
            }
            else
            {
                $NewFileName = "";
            }
            ///////////////////////////////
            
            if (($ChangePassword =="True") && ($ChangeImage =="True"))
            {
                $data = array(
                    'group_id' => $group_id,
                    'fullname' => $fullname,
                    'email' => $email,
                    'username' => $username,
                    'password' => $password,
                    'active' => $active,
                    'branches_lookup' => $branches_lookup,
                    'branch' => $branch,
                    'image' => $NewFileName,
                );

                $this->M_usr_users->UpdateRecord1($id, $data);
            }
            else if (($ChangePassword =="True") && ($ChangeImage ==""))
            {
                $NewFileName = "";
                $data = array(
                    'group_id' => $group_id,
                    'fullname' => $fullname,
                    'email' => $email,
                    'username' => $username,
                    'password' => $password,
                    'active' => $active,
                    'branches_lookup' => $branches_lookup,
                    'branch' => $branch,
                );

                $this->M_usr_users->UpdateRecord1($id, $data);
            }
            else if (($ChangePassword =="") && ($ChangeImage =="True"))
            {
                $password = "";
                $data = array(
                    'group_id' => $group_id,
                    'fullname' => $fullname,
                    'email' => $email,
                    'username' => $username,
                    'active' => $active,
                    'branches_lookup' => $branches_lookup,
                    'branch' => $branch,
                    'image' => $NewFileName,
                );

                $this->M_usr_users->UpdateRecord1($id, $data);
            }
            else
            {
                $NewFileName = "";
                $password = "";
                $data = array(
                    'group_id' => $group_id,
                    'fullname' => $fullname,
                    'email' => $email,
                    'username' => $username,
                    'active' => $active,
                    'branches_lookup' => $branches_lookup,
                    'branch' => $branch,
                );

                $this->M_usr_users->UpdateRecord1($id, $data);
            }
            
            //$this->session->set_userdata('GroupID', $group_id);
            $StoreData = array();
            $this->db->delete("usr_users_store", array('user_id' => $id)); 
            foreach ($_REQUEST['UserStore'] as $UserStore)
            {
                $StoreData = array(
                    'user_id' => $id,
                    'store_id' => $UserStore,
                );
                $this->M_usr_users_store->InsertRecord($StoreData);
            }
			
			$AccountData = array();
            $this->db->delete("usr_users_account", array('user_id' => $id)); 
            foreach ($_REQUEST['UserAccounts'] as $UserAccounts)
            {
                $AccountData = array(
                    'user_id' => $id,
                    'account_id' => $UserAccounts,
                );
                $this->M_usr_users_account->InsertRecord($AccountData);
            }

            $this->load->model("M_usr_usersgroup");
            $data['AllGroups'] = $this->M_usr_usersgroup->GetMultiRow();
            
            $data['MultiRows'] = $this->M_usr_users->GetMultiRow();
            
            //$this->load->view('usr_users', $data);
            redirect ('permissions/usr_users');
        }
    }

    public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

        $this->M_usr_users->UpdateDelete($rid, $NewData);
        
        $Segment1 = $this->uri->segment(1);
        $ControllerName = $this->router->fetch_class();
        $Path = $Segment1."/".$ControllerName;
        redirect ($Path);
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_usr_users->UpdateDelete($rid, $NewData);

        $Segment1 = $this->uri->segment(1);
        $ControllerName = $this->router->fetch_class();
        $Path = $Segment1."/".$ControllerName;
        redirect ($Path);
	}
    
}
