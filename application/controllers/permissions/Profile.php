<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }
        $this->load->model('permissions/M_usr_users');
        $this->load->model("M_usr_usersgroup");

	}

	public function index()
	{
        $this->session->set_userdata('Filter', "");
        $StaffID = intval($this->session->userdata('StaffID'));
        $data['AllGroups'] = $this->M_usr_usersgroup->GetMultiRow();
        
        $data += $this->LoadProfile();
        
        $data['content_page'] = "permissions/profile";
		$this->load->view('page', $data);
    }
    
    public function LoadProfile()
    {
        $StaffID = intval($this->session->userdata('StaffID'));
        $MyRecord = $this->M_usr_users->GetRow($StaffID);

        $data['Profile_id'] = $MyRecord->id;
        $data['Profile_group_id'] = $MyRecord->group_id;
        $data['Profile_fullname'] = $MyRecord->fullname;
        $data['Profile_email'] = $MyRecord->email;
        $data['Profile_username'] = $MyRecord->username;
        $data['Profile_password'] = $MyRecord->password;
        $data['Profile_phone'] = $MyRecord->phone;
        $data['Profile_active'] = $MyRecord->active;
        $data['branches_lookup'] = $MyRecord->branches_lookup;
        $data['Profile_image'] = $MyRecord->image;

        return $data;
    }
    
    
    public function update_data()
    {
    	$this->load->helper('security');
        $this->load->library("form_validation");
        
        $this->form_validation->set_rules("group_id","group_id","required");
        $this->form_validation->set_rules("fullname","fullname","required");
        $this->form_validation->set_rules("email","email","required");
        $this->form_validation->set_rules("username","username","required");
        $this->form_validation->set_rules("password","password","required");
        $this->form_validation->set_rules("active","active","required");
        
        if ($this->form_validation->run() == FALSE) {
        echo validation_errors();
        }
        else {
            $id = set_value('id');
            $group_id = set_value('group_id');
            $fullname = set_value('fullname');
            $email = set_value('email');
            $username = set_value('username');
            $password = md5(set_value('password'));
            $active = set_value('active');
            $branches_lookup = set_value('branches_lookup');
            
            $ChangePassword = set_value('ChangePassword');
            $ChangeImage = set_value('ChangeImage');
            
            if ($ChangeImage =="True")
            {
                $filename = $password;
                $this->load->helper('text');
                $filename = str_replace(" ", "_", $filename);
                $filename = rtrim($filename);
                $exts = explode(".", $_FILES['image']['name']);
                $exts = end($exts);
                
                $UploadPath = "upload/";
                $ImageDirectory = $UploadPath."usr_users/";
                if (!file_exists($ImageDirectory)) {
                    mkdir($ImageDirectory, 0777, true);
                }

                $config['upload_path'] = $ImageDirectory;
                $config['allowed_types'] = "*";
                $config['file_name'] = $filename.".".$exts;
                $config['overwrite'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                $NewFileName = $filename.".".$exts;
                
                if ( ! $this->upload->do_upload('image', $NewFileName))
                {
                    $error = array('error' => $this->upload->display_errors());
                    $this->load->view('upload_form', $error);
                }
                $this->upload->do_upload('image', $NewFileName);
            }
            ///////////////////////////////
            
            if (($ChangePassword =="True") && ($ChangeImage =="True"))
            {
                $data = array(
                    'group_id' => $group_id,
                    'fullname' => $fullname,
                    'email' => $email,
                    'username' => $username,
                    'password' => $password,
                    'active' => $active,
                    'branches_lookup' => $branches_lookup,
                    'branch' => $branch,
                    'image' => $NewFileName,
                );

                $this->M_usr_users->UpdateRecord1($id, $data);
            }
            else if (($ChangePassword =="True") && ($ChangeImage ==""))
            {
                $NewFileName = "";
                $data = array(
                    'group_id' => $group_id,
                    'fullname' => $fullname,
                    'email' => $email,
                    'username' => $username,
                    'password' => $password,
                    'active' => $active,
                    'branches_lookup' => $branches_lookup,
                    'branch' => $branch,
                );

                $this->M_usr_users->UpdateRecord1($id, $data);
            }
            else if (($ChangePassword =="") && ($ChangeImage =="True"))
            {
                $password = "";
                $data = array(
                    'group_id' => $group_id,
                    'fullname' => $fullname,
                    'email' => $email,
                    'username' => $username,
                    'active' => $active,
                    'branches_lookup' => $branches_lookup,
                    'branch' => $branch,
                    'image' => $NewFileName,
                );

                $this->M_usr_users->UpdateRecord1($id, $data);
            }
            else
            {
                $NewFileName = "";
                $password = "";
                $data = array(
                    'group_id' => $group_id,
                    'fullname' => $fullname,
                    'email' => $email,
                    'username' => $username,
                    'active' => $active,
                    'branches_lookup' => $branches_lookup,
                    'branch' => $branch,
                );

                $this->M_usr_users->UpdateRecord1($id, $data);
            }
            
            $this->session->set_userdata('GroupID', $group_id);
            
            $this->load->model("M_usr_usersgroup");
            $data['AllGroups'] = $this->M_usr_usersgroup->GetMultiRow();
            
            $data['MultiRows'] = $this->M_usr_users->GetMultiRow();
            
            //$this->load->view('usr_users', $data);
            redirect ('permissions/profile');
        }
    }

    public function serv_message()
    {
        $this->load->model("service/M_serv_message");
        $this->session->set_userdata('Filter', $this->lang->line('serv_message_index'));
        $data['DataRows'] = $this->M_serv_message->GetInbox();
        $data += $this->LoadProfile();

		$data['content_page'] = "permissions/profile_message_inbox";
		$this->load->view('page', $data);
    }

    public function serv_message_sent()
    {
        $this->load->model("service/M_serv_message");
        $this->session->set_userdata('Filter', $this->lang->line('serv_message_sent'));
        $data['DataRows'] = $this->M_serv_message->GetOutBox();
        $data += $this->LoadProfile();

		$data['content_page'] = "permissions/profile_message_inbox";
		$this->load->view('page', $data);
    }

    public function serv_message_insert()
    {
        $this->form_validation->set_rules("subject","subject","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $from_user = intval($this->session->userdata('StaffID'));
			$subject = set_value('subject');
			$message = set_value('message');
			$thedate = date("Y-m-d");
            $thetime = date("h:i:s");
            $readed = 0;
            $marked = 0;
            $deleted = 0;
            
            $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/serv_message/";
			$ImageDirectory = $UploadPath;
			
			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
            }
            
            $NewData = array();

            $action = $this->input->post('action');
            //echo $action;
            $draft = 0;
            foreach ($_REQUEST['to_user'] as $to_user)
            {
                $NewData = array(
                    'company_id' => $company_id,
                    'from_user' => $from_user,
                    'to_user' => $to_user,
                    'subject' => $subject,
                    'message' => $message,
                    'image' => $NewFileName,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                    'readed' => $readed,
                    'marked' => $marked,
                    'draft' => $draft,
                    'deleted' => $deleted,
                );
    
                $this->M_serv_message->InsertRecord($NewData);
            }

			$this->load->library('user_agent');
		    redirect($_SERVER['HTTP_REFERER']);
		}
    }

    public function view_message()
    {
        $this->load->model("service/M_serv_message");

        $id = 0;
        $this->session->unset_userdata('Message_ID');
        $id = $this->input->get('id');
        $MainDataRow = $this->M_serv_message->GetJSON($id);
		$data['id'] = $MainDataRow->id;
		$data['fullname'] = $MainDataRow->fullname;
		$data['subject'] = $MainDataRow->subject;
		$data['message'] = $MainDataRow->message;
		$data['thedate'] = $MainDataRow->thedate;
		$data['image'] = $MainDataRow->image;

		//$DataRows = $this->M_fin_journal->GetMultiRow($id);
		//$this->session->unset_userdata('Fin_journal_View_ID');
		
		$this->session->set_userdata('Message_ID', $id);

		echo json_encode($MainDataRow); 
		
    	exit();
    }

    public function serv_action()
    {
        $this->load->model("service/M_serv_action");
        $this->load->model("service/M_serv_action_types");
        $this->load->model("project/M_proj_project");

        $this->session->set_userdata('Filter', lang('Incoming_requests'));
        $this->session->set_userdata('ActionTypeFilter', 0);
        $data['DataRows'] = $this->M_serv_action->GetToUser();
        $data += $this->LoadProfile();
		$data['content_page'] = "permissions/profile_serv_action";
		$this->load->view('page', $data);
    }

    public function serv_action_outbound()
    {
        $this->load->model("service/M_serv_action");
        $this->load->model("service/M_serv_action_types");
        $this->load->model("project/M_proj_project");

        $this->session->set_userdata('Filter', lang('Outbound_requests'));
        $this->session->set_userdata('ActionTypeFilter', 0);
        $data['DataRows'] = $this->M_serv_action->GetFromUser();
        $data += $this->LoadProfile();
		$data['content_page'] = "permissions/profile_serv_action";
		$this->load->view('page', $data);
    }
}
