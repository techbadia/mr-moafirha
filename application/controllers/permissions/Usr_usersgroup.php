<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Usr_usersgroup extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();

		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }
				}

				public function checkForPermission()
				{
					$allPage = $this->M_app_module_page->GetAllPagesForCheck();
					$MultiRows = $this->M_usr_usersgroup->GetMultiRow();
					$pagesIds = array();

					// var_dump($pagesIds);die();

					foreach ($allPage as $page) {
					foreach ($MultiRows as $user) {


						$prev =	$this->M_usr_usersprivileges->GetData($user->id,$page->id);
						if(empty($prev)){
						$company_id = intval($this->session->userdata('company_id'));
						$group_id = $user->id;
						$page_id = $page->id;
						$enabled_add = 0;
						$enabled_edit = 0;
						$enabled_delete = 0;
						$enabled_view = 0;
						$enabled_export = 0;

						$NewData = array();
						$NewData = array(
								'company_id' => $company_id,
								'group_id' => $group_id,
								'page_id' => $page_id,
								'enabled_add' => $enabled_add,
								'enabled_edit' => $enabled_edit,
								'enabled_delete' => $enabled_delete,
								'enabled_view' => $enabled_view,
								'enabled_export' => $enabled_export,
						);

						$this->M_usr_usersprivileges->InsertRecord($NewData);

					}
					}
					$pageData['perm_checked'] = 1;
					$this->M_app_module_page->UpdateRecord($page->id,$pageData);
					}
					// echo "done";
				}
	public function index()
	{
		$this->checkForPermission();

        $data['MultiRows'] = $this->M_usr_usersgroup->GetMultiRow();

        $data['content_page'] = "permissions/usr_usersgroup";
		$this->load->view('page', $data);
	}

    public function insertform()
    {
        $data['PageCount'] = $this->M_app_module_page->GetAllPagesCount();
        $data['AllPages'] = $this->M_app_module_page->GetAllPages();

        $data['content_page'] = "permissions/usr_usersgroup_insert";
		$this->load->view('page', $data);
    }

    public function insert_data()
    {
    	$this->load->helper('security');
        $this->load->library("form_validation");

        $this->form_validation->set_rules("ar_title","ar_title","required");
        $this->form_validation->set_rules("en_title","en_title","required");
        $company_id = intval($this->session->userdata('company_id'));
        if ($this->form_validation->run() == FALSE) {
            echo validation_errors();
        }
        else {

            $ar_title = set_value('ar_title');
            $en_title = set_value('en_title');
            $deleted = 0;

            if ($this->M_usr_usersgroup->CheckDuplicate($ar_title) ==0)
            {
                $NewData = array();
                $NewData = array(
                    'company_id' => $company_id,
                    'ar_title' => $ar_title,
                    'en_title' => $en_title,
                    'deleted' => $deleted,
                );

                $this->M_usr_usersgroup->InsertRecord($NewData);
                //$this->M_usr_usersgroup->InsertRecord($ar_title);

                $LatestGroup = $this->M_usr_usersgroup->WhatsID($ar_title);
                $LatestID = $LatestGroup->id;

                $this->load->model("M_usr_usersprivileges");
                $AllPrivileges = $this->M_usr_usersprivileges->GetGroupRow($LatestID);
                $id = "id";
                $enabled_add = "enabled_add";
                $enabled_edit = "enabled_edit";
                $enabled_delete = "enabled_delete";
                $enabled_view = "enabled_view";
                $enabled_export = "enabled_export";

                $ModulePages = $this->M_app_package_module_page->GetMultiRow();
                foreach($ModulePages as $ModulePages_Row) {
                    $PageID = $ModulePages_Row->page_id;
                    $id = "id".$PageID;
                    $enabled_add = "enabled_add".$PageID;
                    $enabled_edit = "enabled_edit".$PageID;
                    $enabled_delete = "enabled_delete".$PageID;
                    $enabled_view = "enabled_view".$PageID;
                    $enabled_export = "enabled_export".$PageID;

                    $company_id = intval($this->session->userdata('company_id'));
                    $group_id = $LatestID;
                    $page_id = $PageID;
                    $enabled_add = set_value($enabled_add);
                    $enabled_edit = set_value($enabled_edit);
                    $enabled_delete = set_value($enabled_delete);
                    $enabled_view = set_value($enabled_view);
                    $enabled_export = set_value($enabled_export);

                    $NewData = array();
                    $NewData = array(
                        'company_id' => $company_id,
                        'group_id' => $group_id,
                        'page_id' => $page_id,
                        'enabled_add' => $enabled_add,
                        'enabled_edit' => $enabled_edit,
                        'enabled_delete' => $enabled_delete,
                        'enabled_view' => $enabled_view,
                        'enabled_export' => $enabled_export,
                    );

                    $this->M_usr_usersprivileges->InsertRecord($NewData);
                }
            }

            $data['MultiRows'] = $this->M_usr_usersgroup->GetMultiRow();

            //$this->load->view('usr_usersgroup', $data);
            $data['content_page'] = "permissions/usr_usersgroup";
		    redirect ('permissions/usr_usersgroup');
        }
    }

    public function updateform($rid)
    {
    	$data['PageCount'] = $this->M_app_module_page->GetAllPagesCount();
        $data['AllPages'] = $this->M_app_module_page->GetAllPages();

        $data['AllPrivileges'] = $this->M_usr_usersprivileges->GetGroupRow($rid);


        $MyRecord = $this->M_usr_usersgroup->GetRow($rid);

        $data['DataRow_id'] = $MyRecord->id;
        $data['DataRow_ar_title'] = $MyRecord->ar_title;
        $data['DataRow_en_title'] = $MyRecord->en_title;

        $data['content_page'] = "permissions/usr_usersgroup_update";
		$this->load->view('page', $data);
    }

    public function update_data()
    {
    	$this->load->helper('security');
        $this->load->library("form_validation");

        $this->form_validation->set_rules("ar_title","ar_title","required");
        $this->form_validation->set_rules("en_title","en_title","required");

        if ($this->form_validation->run() == FALSE) {
        echo validation_errors();
        }
        else {
            $id = set_value('id');
            $group_id = set_value('id');
            $ar_title = set_value('ar_title');
            $en_title = set_value('en_title');

            $this->M_usr_usersgroup->UpdateRecord($id, $ar_title, $en_title);

            /////////////////////
            $this->load->model("M_usr_usersprivileges");
            $AllPrivileges = $this->M_usr_usersprivileges->GetGroupRow($id);
            //$id = "id";
            $page_id = "page_id";
            $group_id = "group_id";
            $enabled_add = "enabled_add";
            $enabled_edit = "enabled_edit";
            $enabled_delete = "enabled_delete";
            $enabled_view = "enabled_view";
            $enabled_export = "enabled_export";

            foreach($AllPrivileges as $AllPrivileges_Row) {
                $id = "id".$AllPrivileges_Row->id;
                $page_id = "page_id".$AllPrivileges_Row->id;
                $enabled_add = "enabled_add".$AllPrivileges_Row->id;
                $enabled_edit = "enabled_edit".$AllPrivileges_Row->id;
                $enabled_delete = "enabled_delete".$AllPrivileges_Row->id;
                $enabled_view = "enabled_view".$AllPrivileges_Row->id;
                $enabled_export = "enabled_export".$AllPrivileges_Row->id;

                $id = set_value($id);
                $enabled_add = set_value($enabled_add);
                $enabled_edit = set_value($enabled_edit);
                $enabled_delete = set_value($enabled_delete);
                $enabled_view = set_value($enabled_view);
                $enabled_export = set_value($enabled_export);
//$id, $group_id, $page_id, $enabled_add, $enabled_edit, $enabled_delete, $enabled_view, $enabled_export

                $this->M_usr_usersprivileges->UpdateRecord($id, $enabled_add,
                        $enabled_edit, $enabled_delete, $enabled_view, $enabled_export);
            }
            /////////////////////
            $data['MultiRows'] = $this->M_usr_usersgroup->GetMultiRow();

            $data['content_page'] = "permissions/usr_usersgroup";
		    redirect ('permissions/usr_usersgroup');
        }
    }

    public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

        $this->M_usr_usersgroup->UpdateDelete($rid, $NewData);

        $Segment1 = $this->uri->segment(1);
        $ControllerName = $this->router->fetch_class();
        $Path = $Segment1."/".$ControllerName;
        redirect ($Path);
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_usr_usersgroup->UpdateDelete($rid, $NewData);

        $Segment1 = $this->uri->segment(1);
        $ControllerName = $this->router->fetch_class();
        $Path = $Segment1."/".$ControllerName;
        redirect ($Path);
	}

}
