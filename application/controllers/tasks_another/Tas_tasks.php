<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tas_tasks extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
        $this->load->model("tasks/M_tas_tasks");
        $this->load->model("tasks/M_tas_tasks_type1_levels");
	}
	
	public function index()
	{
		
		$data['DataRows'] = $this->M_tas_tasks->GetMultiRow();
		$data['content_page'] = "tasks/tas_tasks";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "tasks/tas_tasks_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("details","details","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $from_user = intval($this->session->userdata('StaffID'));
			$thedate = date("Y-m-d");
	        $thetime = date("h:m");
			$title = set_value('title');
			$details = set_value('details');
            $task_type = intval(set_value('task_type'));
            $status = 0;
            $levels = intval(set_value('levels'));
			$deleted = 0;


			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'from_user' => $from_user,
				'thedate' => $thedate,
				'thetime' => $thetime,
				'title' => $title,
				'details' => $details,
                'task_type' => $task_type,
                'status' => $status,
                'levels' => $levels,
				'deleted' => $deleted,
			);

			$this->M_tas_tasks->InsertRecord($NewData);
            
            
            $FunctionName = "";
            if($task_type == 1)
            {
                $FunctionName = "tasks/AddTaskType1/".$levels;
            }
            else if ($task_type == 2)
            {
                $FunctionName = "tasks/AddTaskType2/".$levels;
            }
            else
            {
                $FunctionName = "tasks/AddTaskType3/".$levels;
            }
			redirect ($FunctionName);
		}
    }
    
    public function AddTaskType1($levels = 1)
    {
        $data['levels'] = $levels;
        $data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "tasks/tas_tasks_type1_insert";
		$this->load->view('page', $data);
    }

    public function insert_type1_data()
    {
        $from_user = intval($this->session->userdata('StaffID'));
        $company_id = intval($this->session->userdata('company_id'));
        $LatestTask = $this->M_tas_tasks->GetLatestRow($from_user);
        $task_id = $LatestTask->id;

        $levels = set_value('levels');

        for ($i = 1; $i <= $levels; $i++) {
            $to_user = "to_user".$i;
            $details = "details".$i;
            $start_date = "start_date".$i;
            $start_time = "start_time".$i;
            $end_date = "end_date".$i;
            $end_time = "end_time".$i;
            $status = 0;
            
            $to_user = set_value($to_user);
            $details = set_value($details);
            $start_date = set_value($start_date);
            $start_time = set_value($start_time);
            $end_date = set_value($end_date);
            $end_time = set_value($end_time);

            $UserData = $this->M_usr_users->GetRow($to_user);
            $User_fullname = $UserData->fullname;
            $User_email = $UserData->email;

            $TaskSender = $this->M_usr_users->GetRow($from_user);
            $TaskSender_fullname = $TaskSender->fullname;
            $TaskSender_email = $TaskSender->email;

            ////////////////////////////
            $subject = "تكليف بمهمة جديدة";
			$message = $details;
            $message .= "<br><br>";
            $message .= "<p><strong>أنت مكلف بتنفيذ مهمة جديدة</strong></p>";
            $message .= "<br><hr><br>";
            $message .= "<p><strong>القائم بتكليفك بالمهمة : </strong>".$TaskSender_fullname."</p>";
            $message .= "<p><strong>تاريخ وتوقيت بدء المهمة : </strong>".$start_date." ".$start_time."</p>";
            $message .= "<p><strong>تاريخ وتوقيت إنتهاء المهمة : </strong>".$end_date." ".$end_time."</p>";
            $message .= "<p><strong>لاتنسى أنه يجب أن تقوم بالدخول وتأكيد بدء المهمة وإلا سيحسب عليك بطئ في الإستجابة للتكليف</strong></p>";

			$this->load->library("email");
			
            $config = Array(		
                'mailtype'  => 'html', 
                'charset'   => 'utf-8'
            );
            $this->email->initialize($config);
            $this->load->library("email", $config);
            $this->email->from("info@safety-level.com");
            
			$this->email->to($User_email);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
            ////////////////////////////
            $NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'task_id' => $task_id,
				'to_user' => $to_user,
				'details' => $details,
				'start_date' => $start_date,
				'start_time' => $start_time,
                'end_date' => $end_date,
                'end_time' => $end_time,
                'status' => $status,
				'deleted' => $deleted,
			);

			$this->M_tas_tasks_type1_levels->InsertRecord($NewData);
        }
        redirect ('tasks/tas_tasks');
    }

    public function AddTaskType2($levels = 1)
    {
        $data['levels'] = $levels;
        $data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "tasks/tas_tasks_type2_insert";
		$this->load->view('page', $data);
    }

    public function insert_type2_data()
    {
        $from_user = intval($this->session->userdata('StaffID'));
        $company_id = intval($this->session->userdata('company_id'));
        $LatestTask = $this->M_tas_tasks->GetLatestRow($from_user);
        $task_id = $LatestTask->id;

        $levels = set_value('levels');
        $days = set_value('days');
	    $doloop = set_value('doloop');
        for ($d = 1; $d <= $doloop; $d++) {

        }
        for ($i = 1; $i <= $levels; $i++) {
            $to_user = "to_user".$i;
            $details = "details".$i;
            $start_date = "start_date".$i;
            $start_time = "start_time".$i;
            $end_date = "end_date".$i;
            $end_time = "end_time".$i;
            $status = 0;
            
            $to_user = set_value($to_user);
            $details = set_value($details);
            //$start_date = set_value($start_date);
            $start_time = set_value($start_time);
            //$end_date = set_value($end_date);
            $end_time = set_value($end_time);

            if($d == 1)
			{
				$start_date = set_value($start_date);
				$end_date = set_value($end_date);
			}
			else
			{
				$start_date = set_value($start_date);
				$end_date = set_value($end_date);
				
				$start_date = strtotime("+".$days." days", strtotime($start_date));
				$end_date = strtotime("+".$days." days", strtotime($end_date));
				
				$start_date = date("Y-m-d", $start_date);
				$end_date = date("Y-m-d", $end_date);
			}

            $UserData = $this->M_usr_users->GetRow($to_user);
            $User_fullname = $UserData->fullname;
            $User_email = $UserData->email;

            $TaskSender = $this->M_usr_users->GetRow($from_user);
            $TaskSender_fullname = $TaskSender->fullname;
            $TaskSender_email = $TaskSender->email;

            ////////////////////////////
            $subject = "تكليف بمهمة جديدة";
			$message = $details;
            $message .= "<br><br>";
            $message .= "<p><strong>أنت مكلف بتنفيذ مهمة جديدة</strong></p>";
            $message .= "<br><hr><br>";
            $message .= "<p><strong>القائم بتكليفك بالمهمة : </strong>".$TaskSender_fullname."</p>";
            $message .= "<p><strong>تاريخ وتوقيت بدء المهمة : </strong>".$start_date." ".$start_time."</p>";
            $message .= "<p><strong>تاريخ وتوقيت إنتهاء المهمة : </strong>".$end_date." ".$end_time."</p>";
            $message .= "<p><strong>لاتنسى أنه يجب أن تقوم بالدخول وتأكيد بدء المهمة وإلا سيحسب عليك بطئ في الإستجابة للتكليف</strong></p>";

			$this->load->library("email");
			
            $config = Array(		
                'mailtype'  => 'html', 
                'charset'   => 'utf-8'
            );
            $this->email->initialize($config);
            $this->load->library("email", $config);
            $this->email->from("info@safety-level.com");
            
			$this->email->to($User_email);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
            ////////////////////////////
            $NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'task_id' => $task_id,
				'to_user' => $to_user,
				'details' => $details,
				'start_date' => $start_date,
				'start_time' => $start_time,
                'end_date' => $end_date,
                'end_time' => $end_time,
                'status' => $status,
				'deleted' => $deleted,
			);

			$this->M_tas_tasks_type1_levels->InsertRecord($NewData);
        }
        redirect ('tasks/tas_tasks');
    }

    public function AddTaskType3($levels = 1)
    {
        $data['levels'] = $levels;
        $data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "tasks/tas_tasks_type3_insert";
		$this->load->view('page', $data);
    }

    public function insert_type3_data()
    {
        $from_user = intval($this->session->userdata('StaffID'));
        $company_id = intval($this->session->userdata('company_id'));
        $LatestTask = $this->M_tas_tasks->GetLatestRow($from_user);
        $task_id = $LatestTask->id;

        $levels = set_value('levels');
        $days = set_value('days');
	    $doloop = set_value('doloop');
        for ($d = 1; $d <= $doloop; $d++) {

        }
        for ($i = 1; $i <= $levels; $i++) {
            $to_user = "to_user".$i;
            $details = "details".$i;
            $start_date = "start_date".$i;
            $start_time = "start_time".$i;
            $end_date = "end_date".$i;
            $end_time = "end_time".$i;
            $status = 0;
            
            $to_user = set_value($to_user);
            $details = set_value($details);
            //$start_date = set_value($start_date);
            $start_time = set_value($start_time);
            //$end_date = set_value($end_date);
            $end_time = set_value($end_time);

            if($d == 1)
			{
				$start_date = set_value($start_date);
				$end_date = set_value($end_date);
			}
			else
			{
				$start_date = set_value($start_date);
				$end_date = set_value($end_date);
				
				$start_date = strtotime("+".$days." days", strtotime($start_date));
				$end_date = strtotime("+".$days." days", strtotime($end_date));
				
				$start_date = date("Y-m-d", $start_date);
				$end_date = date("Y-m-d", $end_date);
			}

            $UserData = $this->M_usr_users->GetRow($to_user);
            $User_fullname = $UserData->fullname;
            $User_email = $UserData->email;

            $TaskSender = $this->M_usr_users->GetRow($from_user);
            $TaskSender_fullname = $TaskSender->fullname;
            $TaskSender_email = $TaskSender->email;

            ////////////////////////////
            $subject = "تكليف بمهمة جديدة";
			$message = $details;
            $message .= "<br><br>";
            $message .= "<p><strong>أنت مكلف بتنفيذ مهمة جديدة</strong></p>";
            $message .= "<br><hr><br>";
            $message .= "<p><strong>القائم بتكليفك بالمهمة : </strong>".$TaskSender_fullname."</p>";
            $message .= "<p><strong>تاريخ وتوقيت بدء المهمة : </strong>".$start_date." ".$start_time."</p>";
            $message .= "<p><strong>تاريخ وتوقيت إنتهاء المهمة : </strong>".$end_date." ".$end_time."</p>";
            $message .= "<p><strong>لاتنسى أنه يجب أن تقوم بالدخول وتأكيد بدء المهمة وإلا سيحسب عليك بطئ في الإستجابة للتكليف</strong></p>";

			$this->load->library("email");
			
            $config = Array(		
                'mailtype'  => 'html', 
                'charset'   => 'utf-8'
            );
            $this->email->initialize($config);
            $this->load->library("email", $config);
            $this->email->from("info@safety-level.com");
            
			$this->email->to($User_email);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
            ////////////////////////////
            $NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'task_id' => $task_id,
				'to_user' => $to_user,
				'details' => $details,
				'start_date' => $start_date,
				'start_time' => $start_time,
                'end_date' => $end_date,
                'end_time' => $end_time,
                'status' => $status,
				'deleted' => $deleted,
			);

			$this->M_tas_tasks_type1_levels->InsertRecord($NewData);
        }
        redirect ('tasks/tas_tasks');
    }

	public function updateform($rid)
	{
		$DataRow = $this->M_tas_tasks->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
        $data['user_id'] = $DataRow->user_id;
		$data['web_store'] = $DataRow->web_store;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['email'] = $DataRow->email;
		$data['phone'] = $DataRow->phone;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "tasks/tas_tasks_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("details","details","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $from_user = set_value("from_user");
			$thedate = set_value("thedate");
			$thetime = set_value('thetime');
			$title = set_value('title');
			$details = set_value('details');
            $task_type = set_value('task_type');
            $status = set_value('status');
            $levels = set_value('levels');
			$deleted = set_value('deleted');

			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'from_user' => $from_user,
				'thedate' => $thedate,
				'thetime' => $thetime,
				'title' => $title,
				'details' => $details,
                'task_type' => $task_type,
                'status' => $status,
                'levels' => $levels,
				'deleted' => $deleted,
			);
			

			$this->M_tas_tasks->UpdateRecord($id, $NewData);
			
			redirect ('tasks/tas_tasks');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_tas_tasks->UpdateRecord($rid, $NewData);

        redirect("tasks/tas_tasks");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_tas_tasks->UpdateRecord($rid, $NewData);

        redirect("tasks/tas_tasks");
    }
    
    public function task_action($level =0, $action=0)
    {
        if($action == 1)
        {
            $user_start_date = date("Y-m-d");
	        $user_start_time = date("H:m");
            $status = 1;

            $id = $level;
            $NewData = array();
			$NewData = array(
                'user_start_date' => $user_start_date,
                'user_start_time' => $user_start_time,
				'status' => $status,
			);
		
			$this->M_tas_tasks_type1_levels->UpdateRecord($id, $NewData);
        }
        else if($action == 2)
        {
            $status = 2;

            $id = $level;
            $NewData = array();
			$NewData = array(
				'status' => $status,
            );
            $this->M_tas_tasks_type1_levels->UpdateRecord($id, $NewData);
            $LatestTask = $this->M_tas_tasks_type1_levels->GetRow($level);
            $user_id = $LatestTask->to_user;
            $UserData = $this->M_usr_users->GetRow($user_id);
            $User_email = $UserData->email;
            ////////////////////////////
            $subject = "لقد فشلت في المهمة";
			$message = "<p><strong>المهمة فشلت</strong></p>";
            $message .= "<br><hr><br>";
            $message .= "<p><strong>تفاصيل المهمة : </strong>".$LatestTask->details."</p>";
            $message .= "<p><strong>تاريخ وتوقيت بدء المهمة : </strong>".$LatestTask->start_date." ".$LatestTask->start_time."</p>";

			$this->load->library("email");
			
            $config = Array(		
                'mailtype'  => 'html', 
                'charset'   => 'utf-8'
            );
            $this->email->initialize($config);
            $this->load->library("email", $config);
            $this->email->from("info@safety-level.com");
            
			$this->email->to($User_email);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
            ////////////////////////////
        }
        else if($action == 3)
        {
            $status = 3;

            $id = $level;
            $NewData = array();
			$NewData = array(
				'status' => $status,
            );
            $this->M_tas_tasks_type1_levels->UpdateRecord($id, $NewData);
            $LatestTask = $this->M_tas_tasks_type1_levels->GetRow($level);
            $user_id = $LatestTask->to_user;
            $UserData = $this->M_usr_users->GetRow($user_id);
            $User_email = $UserData->email;
            ////////////////////////////
            $subject = "تم إلغاء مهمة";
			$message = "<p><strong>تنبيه بالغاء مهمة</strong></p>";
            $message .= "<br><hr><br>";
            $message .= "<p><strong>تفاصيل المهمة : </strong>".$LatestTask->details."</p>";
            $message .= "<p><strong>تاريخ وتوقيت بدء المهمة : </strong>".$LatestTask->start_date." ".$LatestTask->start_time."</p>";

			$this->load->library("email");
			
            $config = Array(		
                'mailtype'  => 'html', 
                'charset'   => 'utf-8'
            );
            $this->email->initialize($config);
            $this->load->library("email", $config);
            $this->email->from("info@safety-level.com");
            
			$this->email->to($User_email);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
            ////////////////////////////
        }
        else
        {
            $user_start_date = date("Y-m-d");
	        $user_start_time = date("H:m");
            $status = 4;

            $id = $level;
            $NewData = array();
			$NewData = array(
                'user_start_date' => $user_start_date,
                'user_start_time' => $user_start_time,
				'status' => $status,
			);
		
			$this->M_tas_tasks_type1_levels->UpdateRecord($id, $NewData);
        }
    }
}
