<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistics_hr extends CI_Controller {

	var $data = array();
	
	public function index()
	{
		$this->load->model("hr/M_hr_country");
        $this->load->model("hr/M_hr_education");
        $this->load->model("hr/M_hr_job");
        $this->load->model("hr/M_hr_job_type");
        $this->load->model("hr/M_hr_workgroup");
        $this->load->model("hr/M_hr_employee");
		
		$this->config->load('settings/config_setting');
		$statistics_page = "";
		$Config_File = "";
		$Config_File_Lang = "";
		//$Segment1 = $this->uri->segment(1);
		//$this->config->set_item('Package', $Segment1);
		$Package = $this->config->item('Package');

		if ($this->config->item('MainLanguage') == "ar")
		{
			$Config_File_Lang = "_ar";
		}
		else
		{
			$Config_File_Lang = "_en";
		}
		
		$AppData = $this->M_app_element->GetApp();
		$PackageName = $AppData->PackageName;
		$statistics_page = "statistics_".$PackageName;
		$Config_File = "settings/".$PackageName.$Config_File_Lang."_setting";
		$this->config->load($Config_File);
		$this->session->set_userdata('PackageName', $PackageName);
		$PackageData = $this->M_app_package->GetByTitle($PackageName);
		$PackageID = $PackageData->id;
		$this->session->set_userdata('PackageID', $PackageID);
		
		
		$this->LoadLanguage();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
			redirect ($PackageName.'login/');
        }
		$this->session->set_userdata('statistics_filter', "Month");
		$this->session->set_userdata('statistics_page', $statistics_page);
		
		$data['content_page'] = "statistics/statistics_hr";
		$this->load->view('home', $data);
	}


	public function filter_today()
	{
		$this->LoadLanguage();
		$this->session->set_userdata('statistics_filter', "Today");
		$statistics_page = $this->session->userdata('statistics_page');
		$data['content_page'] = $statistics_page;
		$this->load->view('home', $data);
	}

	public function filter_year()
	{
		$this->LoadLanguage();
		$this->session->set_userdata('statistics_filter', "Year");
		
		$statistics_page = $this->session->userdata('statistics_page');
		$data['content_page'] = $statistics_page;
		$this->load->view('home', $data);
	}

	public function LoadLanguage()
	{
		$this->config->load('settings/config_setting');
		$Package = $this->config->item('Package');

		if ($this->config->item('MainLanguage') == "ar")
		{
			$Config_File_Lang = "_ar";
		}
		else
		{
			$Config_File_Lang = "_en";
		}

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
	}

}
