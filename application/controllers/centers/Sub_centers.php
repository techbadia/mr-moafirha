<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_centers extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();

		// $StaffID = intval($this->session->userdata('StaffID'));
    //     if ($StaffID == 0)
    //     {
    //         $CookiePackageName = get_cookie('Package');
    //         redirect ($CookiePackageName.'login');
    //     }
    //     else
    //     {
    //         $Segment2 = $this->uri->segment(2);
    //         $Segment3 = $this->uri->segment(3);
    //         $group_id = intval($this->session->userdata('GroupID'));
    //         $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
    //         $page_id = $PageData->id;
    //         $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
    //         if($CheckView == 0)
    //         {
    //             redirect ('home');
    //         }
    //         $InsertData = array();
    //         $thedate = date("Y-m-d");
    //         $thetime = date("H:i:s");
    //         $ControllerName = $this->router->fetch_class();
    //         if(($Segment2 == $ControllerName) && ($Segment3 == ""))
    //         {
    //             $InsertData = array(
    //                 'user_id' => $StaffID,
    //                 'page_id' => $page_id,
    //                 'action_id' => 1,
    //                 'thedate' => $thedate,
    //                 'thetime' => $thetime,
    //             );
    //             $this->M_usr_trace->InsertRecord($InsertData);
    //         }
    //         if($Segment3 == "insertform")
    //         {
    //             $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
    //             if($CheckAdd == 0)
    //             {
    //                 redirect ('home');
    //             }
    //         }
    //         if($Segment3 == "insert_data")
    //         {
    //             $InsertData = array(
    //                 'user_id' => $StaffID,
    //                 'page_id' => $page_id,
    //                 'action_id' => 2,
    //                 'thedate' => $thedate,
    //                 'thetime' => $thetime,
    //             );
    //             $this->M_usr_trace->InsertRecord($InsertData);
    //         }
    //         if($Segment3 == "updateform")
    //         {
    //             $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
    //             if($CheckEdit == 0)
    //             {
    //                 redirect ('home');
    //             }
    //         }
    //         if($Segment3 == "update_data")
    //         {
    //             $InsertData = array(
    //                 'user_id' => $StaffID,
    //                 'page_id' => $page_id,
    //                 'action_id' => 3,
    //                 'thedate' => $thedate,
    //                 'thetime' => $thetime,
    //             );
    //             $this->M_usr_trace->InsertRecord($InsertData);
    //         }
    //         if($Segment3 == "delete")
    //         {
    //             $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
    //             if($CheckDelete == 0)
    //             {
    //                 redirect($_SERVER['HTTP_REFERER']);
    //             }
    //             else
    //             {
    //                 $InsertData = array(
    //                     'user_id' => $StaffID,
    //                     'page_id' => $page_id,
    //                     'action_id' => 4,
    //                     'thedate' => $thedate,
    //                     'thetime' => $thetime,
    //                 );
    //                 $this->M_usr_trace->InsertRecord($InsertData);
    //             }
    //         }
    //         if($Segment3 == "undelete")
    //         {
    //             $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
    //             if($CheckDelete == 0)
    //             {
    //                 redirect($_SERVER['HTTP_REFERER']);
    //             }
    //             else
    //             {
    //                 $InsertData = array(
    //                     'user_id' => $StaffID,
    //                     'page_id' => $page_id,
    //                     'action_id' => 5,
    //                     'thedate' => $thedate,
    //                     'thetime' => $thetime,
    //                 );
    //                 $this->M_usr_trace->InsertRecord($InsertData);
    //             }
    //         }
    //     }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		$this->load->model("centers/M_sub_centers");
		$this->load->model("centers/M_center");
		$this->load->model("account/M_fin_journal");
		$this->load->model("account/M_fin_treeaccount");

	}

	public function index()
	{
    if(empty($_GET["main"]))
    {
      redirect ('centers/Centers');
    }
		$data['DataRows'] = $this->M_sub_centers->GetMultiRow();
		$data['content_page'] = "centers/sub_centers";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$centers = $this->M_center->GetMultiRow();
		$data['centers'] = $centers;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "centers/sub_centers_insert";
		$this->load->view('page', $data);
	}
	private function prepareSales($id,$type)
	{
		$this->db->where("sub_center_id",$id);
		$this->db->where("sub_center_type",$type);
		if(!empty($_GET["fromdate"]))
		{
			$this->db->where('thedate >=', $_GET["fromdate"]);
		}
		if(!empty($_GET["todate"]))
		{
			$this->db->where('thedate <=', $_GET["todate"]);
		}

		if(!empty($_GET["keyword"]))
		{
			$this->db->where("customer_id in (select id from sale_customer where ar_title like '%".$_GET["keyword"]."%' OR en_title like '%".$_GET["keyword"]."%')");
		}
		$sales = $this->db->get('sale_bill');
		$sales = $sales->result();
		foreach ($sales as $sale) {
			$account = $this->M_fin_treeaccount->GetRow($sale->tree_id);
			$company_id = $sale->company_id;
			$CompanyData = $this->M_app_company->GetRow($company_id);
			$Invoice_Start = $CompanyData->invoice_sale_start;
			$sale->Invoice_Start = $Invoice_Start;
			$sale->account_no = $account->account_no;
			$sale->account_title = ($this->session->userdata('lang') == "ar")?$account->title:$account->title_en;
			$sale->details = $sale->sale_note;
			$sale->type = lang('Sales');
			$sale->url = base_url()."sales/sale_bill/view_pdf/".$sale->id;

			$sale->dept = 0;
			$sale->creditor = $sale->paid;
			$sale->net = 0 - $sale->paid;
		}
		return $sales;
	}
	private function prepareBuys($id,$type)
	{
		$this->db->where("sub_center_id",$id);
		$this->db->where("sub_center_type",$type);
		if(!empty($_GET["fromdate"]))
		{
			$this->db->where('thedate >=', $_GET["fromdate"]);
		}
		if(!empty($_GET["todate"]))
		{
			$this->db->where('thedate <=', $_GET["todate"]);
		}
		if(!empty($_GET["keyword"]))
		{
			$this->db->where("supplier_id in (select id from buy_manufacturer where title like '%".$_GET["keyword"]."%' OR title_en like '%".$_GET["keyword"]."%')");
		}
		$buys = $this->db->get('buy_bill');
		$buys = $buys->result();
		foreach ($buys as $buy) {
			$account = $this->M_fin_treeaccount->GetRow($buy->tree_id);
			$company_id = $buy->company_id;
			$CompanyData = $this->M_app_company->GetRow($company_id);
			$Invoice_Start = $CompanyData->invoice_buy_start;
			$buy->Invoice_Start = $Invoice_Start;
			$buy->account_no = $account->account_no;
			$buy->account_title = ($this->session->userdata('lang') == "ar")?$account->title:$account->title_en;
			$buy->details = $buy->notes;
			$buy->type = lang('Purchases');
			$buy->url = base_url()."purchase/buy_bill/view_pdf/".$buy->id;

			$buy->dept = $buy->totalvalue;
			$buy->creditor= 0;
			$buy->net = $buy->totalvalue;
		}
		return $buys;
	}
	private function prepareJournalsCred($id,$type)
	{
		$result = array();
		$this->db->where("(sub_center_cred_id = $id and sub_center_cred_type = '$type')");
		if(!empty($_GET["fromdate"]))
		{
			$this->db->where('main_id in (select id from fin_journal_main where thedate >="'.$_GET["fromdate"].'")');
		}
		if(!empty($_GET["todate"]))
		{
			$this->db->where('main_id in (select id from fin_journal_main where thedate <="'.$_GET["todate"].'")');

		}
		if(!empty($_GET["keyword"]))
		{
			$this->db->where("account_id in (select id from fin_treeaccount where title like '%".$_GET["keyword"]."%' OR title_en like '%".$_GET["keyword"]."%')");
		}

		$jours = $this->db->get('fin_journal');
		$jours = $jours->result();


		// foreach ($journals as $journal) {
			// $this->db->where('main_id', $journal->id);
			// $this->db->where('creditor !=',"0.0000");
			// $this->db->where("(sub_center_id != 'NULL' OR sub_center_cred_id != 'NULL')");



			foreach ($jours as $jour) {
				$this->db->where('id', $jour->main_id);

				$journal = $this->db->get('fin_journal_main');
				$journal = $journal->row();
			$account = $this->M_fin_treeaccount->GetRow($jour->account_id);
			$jour->Invoice_Start = $journal->bill_id;
			$jour->thedate = $journal->thedate;
			$jour->account_no = $account->account_no;
			$jour->account_title = ($this->session->userdata('lang') == "ar")?$account->title:$account->title_en;
			$jour->details = $jour->details;
			$jour->type = lang('Fin_journal');
			$jour->url = base_url()."account/fin_journal/view_pdf/".$journal->id;

			$jour->dept = 0;
			$jour->creditor= $jour->creditor;
			$jour->net = 0 - $jour->creditor;
			$result[] = $jour;
			}
		// }
		return $result;

	}
	private function prepareJournalsDept($id,$type)
	{
		$result = array();
		$this->db->where("(sub_center_id = $id and sub_center_type = '$type')");
		if(!empty($_GET["fromdate"]))
		{
			$this->db->where('main_id in (select id from fin_journal_main where thedate >="'.$_GET["fromdate"].'")');
		}
		if(!empty($_GET["todate"]))
		{
			$this->db->where('main_id in (select id from fin_journal_main where thedate <="'.$_GET["todate"].'")');

		}
		if(!empty($_GET["keyword"]))
		{
			$this->db->where("account_id in (select id from fin_treeaccount where title like '%".$_GET["keyword"]."%' OR title_en like '%".$_GET["keyword"]."%')");
		}

		$jours = $this->db->get('fin_journal');
		$jours = $jours->result();


		// foreach ($journals as $journal) {
			// $this->db->where('main_id', $journal->id);
			// $this->db->where('creditor !=',"0.0000");
			// $this->db->where("(sub_center_id != 'NULL' OR sub_center_cred_id != 'NULL')");



			foreach ($jours as $jour) {
				$this->db->where('id', $jour->main_id);

				$journal = $this->db->get('fin_journal_main');
				$journal = $journal->row();
			$account = $this->M_fin_treeaccount->GetRow($jour->account_id);
			$jour->Invoice_Start = $journal->bill_id;
			$jour->thedate = $journal->thedate;
			$jour->account_no = $account->account_no;
			$jour->account_title = ($this->session->userdata('lang') == "ar")?$account->title:$account->title_en;
			$jour->details = $jour->details;
			$jour->type = lang('Fin_journal');
			$jour->url = base_url()."account/fin_journal/view_pdf/".$journal->id;

			$jour->dept = $jour->debit;
			$jour->creditor= 0;
			$jour->net =  $jour->debit;
			$result[] = $jour;
			}
		// }
		return $result;

	}
	// private function prepareJournalsDept($id,$type)
	// {
	// 	$result = array();
	// 	$this->db->where("((sub_center_id = $id && sub_center_type = '$type') OR (sub_center_cred_id = $id && sub_center_cred_type = '$type'))");
	// 	if(!empty($_GET["fromdate"]))
	// 	{
	// 		$this->db->where('thedate >=', $_GET["fromdate"]);
	// 	}
	// 	if(!empty($_GET["todate"]))
	// 	{
	// 		$this->db->where('thedate <=', $_GET["todate"]);
	// 	}
	// 	$journals = $this->db->get('fin_journal_main');
	// 	$journals = $journals->result();
	// 	foreach ($journals as $journal) {
	// 		$this->db->where('main_id', $journal->id);
	// 		$this->db->where('debit !=',"0.0000");
	// 		$this->db->where("(sub_center_id != 'NULL' OR sub_center_cred_id != 'NULL')");
	// 		$this->db->where("((sub_center_id = $id && sub_center_type = '$type') OR (sub_center_cred_id = $id && sub_center_cred_type = '$type'))");
	//
	// 		if(!empty($_GET["keyword"]))
	// 		{
	// 			$this->db->where("account_id in (select id from fin_treeaccount where title like '%".$_GET["keyword"]."%' OR title_en like '%".$_GET["keyword"]."%')");
	// 		}
	// 		$jours = $this->db->get('fin_journal');
	// 		$jours = $jours->result();
	// 		foreach ($jours as $jour) {
	// 		$account = $this->M_fin_treeaccount->GetRow($jour->account_id);
	// 		$jour->Invoice_Start = $journal->bill_id;
	// 		$jour->thedate = $journal->thedate;
	//
	// 		$jour->account_no = $account->account_no;
	// 		$jour->account_title = ($this->session->userdata('lang') == "ar")?$account->title:$account->title_en;
	// 		$jour->details = $jour->details;
	// 		$jour->type = lang('Fin_journal');
	// 		$jour->url = base_url()."account/fin_journal/view_pdf/".$journal->id;
	// 		$jour->dept = $jour->debit;
	// 		$jour->creditor= $jour->creditor;
	// 		$jour->net = $jour->debit;
	// 		$result[] = $jour;
	// 		}
	// 	}
	// 	return $result;
	//
	// }

	private function prepareSubCenters($id,$type = "main")
	{
		$result = array();
		if($type == "main")
		{
			$this->db->where("parent_id",$id);
		}else {
			$this->db->where("id",$id);
			// code...
		}
		$subs = $this->db->get('sub_centers');
		$subs = $subs->result();
		foreach ($subs as $sub) {
			$result1 = array();
			$obj = new stdClass();
			$obj->id = $sub->id;
			$obj->Invoice_Start = "---";
			$obj->thedate = "---";
			$obj->account_no = "---";
			$obj->account_title = "---";
			$obj->details = ($this->session->userdata('lang') == "ar")?$sub->details_ar:$sub->details_en;
			$obj->type = lang('Opening credit');
			$obj->dept = $sub->dept;
			$obj->creditor  = $sub->credit;
			$obj->url = base_url()."centers/sub_centers/updateform/".$sub->id;

			$obj->net = $sub->dept - $sub->credit;
			$result[] = $obj;
			$mainSales = $this->prepareSales($sub->id,"sub");
			$result= array_merge ($result,$mainSales);

			$mainBuys = $this->prepareBuys($sub->id,"sub");
			$result= array_merge ($result,$mainBuys);

			$mainJournals = $this->prepareJournalsCred($sub->id,"sub");
			$result= array_merge ($result,$mainJournals);
			// print_r($result);die();

			$mainJournals = $this->prepareJournalsDept($sub->id,"sub");
			$result= array_merge ($result,$mainJournals);

		}

		return $result;

	}
	public function reports()
	{
		$centers = $this->M_center->GetMultiRow();
		foreach ($centers as $center) {
			$center->subs = $sub_centers;
		}
		$data['centers'] = $centers;
		if(!empty( $centers)){

		$centers1 = $centers[0];
		$sub_centerss = $this->M_sub_centers->GetMultiRowCenter($centers1->id);
		$data['sub_centerss'] = $sub_centerss;
	}
		if(!empty($_GET["center_id"]))
		{
			$sub_centerss = $this->M_sub_centers->GetMultiRowCenter($_GET["center_id"]);
			$data['sub_centerss'] = $sub_centerss;

			if(!empty($_GET["sub_center_id"]))
			{

			$result = array();
			$subs = $this->prepareSubCenters($_GET["sub_center_id"],"sub");
			$result= array_merge ($result,$subs);
			// $mainSales = $this->prepareSales($_GET["sub_center_id"],"sub");
			// $result= array_merge ($mainSales,$result);
			//
			// $mainBuys = $this->prepareBuys($_GET["sub_center_id"],"sub");
			// $result= array_merge ($mainBuys,$result);
			//
			// $mainJournals = $this->prepareJournalsCred($_GET["sub_center_id"],"sub");
			// $result= array_merge ($mainJournals,$result);
			//
			// $mainJournals = $this->prepareJournalsDept($_GET["sub_center_id"],"sub");
			// $result= array_merge ($mainJournals,$result);
			$data["result"] = $result;

		}else {

			$result = array();
			$subs = $this->prepareSubCenters($_GET["center_id"]);
			$result= array_merge ($result,$subs);
			// print_r($result);die();
			$mainSales = $this->prepareSales($_GET["center_id"],"main");
			$result= array_merge ($mainSales,$result);

			$mainBuys = $this->prepareBuys($_GET["center_id"],"main");
			$result= array_merge ($mainBuys,$result);

			$mainJournals = $this->prepareJournalsCred($_GET["center_id"],"main");
			$result= array_merge ($mainJournals,$result);

			$mainJournals = $this->prepareJournalsDept($_GET["center_id"],"main");
			$result= array_merge ($mainJournals,$result);

			$data["result"] = $result;
		}


		}


		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "centers/reports";
		$this->load->view('page', $data);
	}
	public function getSubCenters()
	{
		$main = $_POST['main'];
		$sub_centers = $this->M_sub_centers->GetMultiRowCenter($_POST["main"]);
		$response = "<option value=''>".lang('Choose')."</option>";
		foreach ($sub_centers as $sub) {
			$subTitle = ($this->session->userdata('lang') == "ar")?$sub->name_ar:$sub->name_en;
			$response .= "<option value='".$sub->id."'>".$subTitle."</option>";
		}
		echo $response;die();
	}
	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("name_ar","name_ar","required");
		$this->form_validation->set_rules("dept","dept","required");
		$this->form_validation->set_rules("credit","credit","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
			$name_ar = set_value('name_ar');
			$name_en = set_value('name_en');
			$dept = set_value('dept');
			$credit = set_value('credit');
			$details_ar = set_value('details_ar');
			$details_en = set_value('details_en');
			$parent_id = set_value('parent_id');


			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'parent_id' => $parent_id,
                'r_id' => $this->generateRandomInteger(9),
                'details_ar' => $details_ar,
                'details_en' => $details_en,
                'credit' => $credit,
                'dept' => $dept,
				'name_ar' => $name_ar,
				'name_en' => $name_en,
			);

			$this->M_sub_centers->InsertRecord($NewData);

			redirect ('centers/sub_centers?main='.$parent_id);
		}
	}
	private function generateRandomInteger($length = 5) {
	    $characters = '123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i <= $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	public function updateform($rid)
	{
		$DataRow = $this->M_sub_centers->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['name_ar'] = $DataRow->name_ar;
		$data['name_en'] = $DataRow->name_en;
		$data['details_ar'] = $DataRow->details_ar;
		$data['dept'] = $DataRow->dept;
		$data['credit'] = $DataRow->credit;
		$data['details_en'] = $DataRow->details_en;

		$data['ControllerName'] = $this->router->fetch_class();
    $data['content_page'] = "centers/sub_centers_update";

		$this->load->view('page', $data);

	}

	public function Update_Data()
	{
    $this->form_validation->set_rules("name_ar","name_ar","required");
    $this->form_validation->set_rules("dept","dept","required");
    $this->form_validation->set_rules("credit","credit","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
      $id = set_value('id');
      $company_id = intval($this->session->userdata('company_id'));
$name_ar = set_value('name_ar');
$name_en = set_value('name_en');
$dept = set_value('dept');
$credit = set_value('credit');
$details_ar = set_value('details_ar');
$details_en = set_value('details_en');
$parent_id = set_value('parent_id');


$NewData = array();
$NewData = array(
          'company_id' => $company_id,
          'parent_id' => $parent_id,
          'details_ar' => $details_ar,
          'details_en' => $details_en,
          'credit' => $credit,
          'dept' => $dept,
  'name_ar' => $name_ar,
  'name_en' => $name_en,
);


			$this->M_sub_centers->UpdateRecord($id, $NewData);

      redirect ('centers/sub_centers?main='.$parent_id);
		}
	}

	public function delete($rid)
	{
    $DataRow = $this->M_sub_centers->GetRow($rid);

		$this->M_sub_centers->delete($rid);
    redirect ('centers/sub_centers?main='.$DataRow->parent_id);

	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_store_brand->UpdateRecord($rid, $NewData);

        redirect("store/store_brand");
	}
}
