<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sale_offer extends CI_Controller
{

    var $data = array();

    public function __construct()
    {
        parent::__construct();

        $StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0) {
            redirect('login');
        } else {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if ($CheckView == 0) {
                redirect('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if (($Segment2 == $ControllerName) && ($Segment3 == "")) {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if ($Segment3 == "insertform") {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if ($CheckAdd == 0) {
                    redirect('home');
                }
            }
            if ($Segment3 == "insert_data") {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if ($Segment3 == "updateform") {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if ($CheckEdit == 0) {
                    redirect('home');
                }
            }
            if ($Segment3 == "update_data") {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if ($Segment3 == "delete") {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if ($CheckDelete == 0) {
                    redirect($_SERVER['HTTP_REFERER']);
                } else {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if ($Segment3 == "undelete") {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if ($CheckDelete == 0) {
                    redirect($_SERVER['HTTP_REFERER']);
                } else {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

        $this->config->load('settings/config_setting');

        if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar")) {
            $this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
            $this->session->set_userdata('Alignment', "right");
            $this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
        } else {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
            $this->session->set_userdata('Alignment', "left");
            $this->session->set_userdata('HTML', "lang='en'");
        }

        $this->load->model("project/M_proj_project");

        $this->load->model("sales/M_sale_customer");
        $this->load->model('sales/M_sale_offer');
        $this->load->model('sales/M_sale_offer_items');

        $this->load->model("account/M_fin_treeaccount");
        $this->load->model("account/M_fin_journal");

        $this->load->model("store/M_store");
        $this->load->model("store/M_product");
        $this->load->model("store/M_store_quantity");
    }

    public function index()
    {
        $this->session->set_userdata('Filter', lang('sale_offer_date_all'));

        $data['TableTitle'] = "";
        $data['DataRows'] = $this->M_sale_offer->GetUnDeleted();
        $data['content_page'] = "sales/sale_offer";
        $this->load->view('page', $data);
    }

    public function deleted()
    {
        $this->session->set_userdata('Filter', lang('Deleted'));

        $data['DataRows'] = $this->M_sale_offer->GetDeleted();
        $data['content_page'] = "sales/sale_offer";
        $this->load->view('page', $data);
    }

    public function date_not_expired()
    {
        $this->session->set_userdata('Filter', lang('sale_offer_date_not_expired'));

        $data['DataRows'] = $this->M_sale_offer->GetNotExpired();
        $data['content_page'] = "sales/sale_offer";
        $this->load->view('page', $data);
    }

    public function date_expired()
    {
        $this->session->set_userdata('Filter', lang('sale_offer_date_expired'));

        $data['DataRows'] = $this->M_sale_offer->GetExpired();
        $data['content_page'] = "sales/sale_offer";
        $this->load->view('page', $data);
    }

    public function forproject($project_id)
    {
        $DataRow = $this->M_proj_project->GetRow($project_id);
        $data['ar_title'] = $DataRow->ar_title;
        $data['en_title'] = $DataRow->en_title;
        if ($this->session->userdata('lang') == "ar") {
            $ProjectName = $DataRow->ar_title;
        } else {
            $ProjectName = $DataRow->en_title;
        }
        $data['TableTitle'] = $ProjectName . " :: " . lang('proj_project_Material');
        $data['DataRows'] = $this->M_sale_offer->GetByProject($project_id);
        $data['content_page'] = "sales/sale_offer";
        $this->load->view('page', $data);
    }

    public function insertform()
    {
        $data['ControllerName'] = $this->router->fetch_class();
        $data['content_page'] = "sales/sale_offer_insert";
        $this->load->view('page', $data);
    }

    public function insert_data()
    {
        $company_id = intval($this->session->userdata('company_id'));
        $MemberID = intval($this->session->userdata('StaffID'));

        $this->form_validation->set_rules("customer_id", "customer_id", "required");
        $this->form_validation->set_rules("tree_id", "tree_id", "required");

        if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
        } else {
            $id = set_value('id');
            $company_id = $company_id;
            $customer_id = set_value("customer_id");
            $project_id = set_value("project_id");
            $user_id = $MemberID;
            $thedate = set_value('thedate');
            $expired_date = set_value('expired_date');
            $totalvalue = set_value('totalvalue');
            $paid = set_value('paid');
            $remaining = set_value('remaining');
            $discount = set_value('discount');
            $over_cost = set_value('over_cost');
            $tax = set_value('tax');
            $notes = set_value('notes');
            $tree_id = set_value('tree_id');


            $filename = date("Y") . "-" . date("m") . "-" . date("d") . "-" . date("h") . "-" . date("i") . "-" . date("s");
            $exts = explode(".", $_FILES['image']['name']);
            $exts = end($exts);
            $exts = strtolower($exts);
            $UploadPath = "./upload/sale_offer/";
            $ImageDirectory = $UploadPath;

            if (!is_dir($ImageDirectory)) {
                mkdir($ImageDirectory, 0777, TRUE);

            }

            $config['upload_path'] = $ImageDirectory;
            $config['allowed_types'] = "*";
            $config['file_name'] = $filename . "." . $exts;
            $config['overwrite'] = TRUE;
            $config['file_ext_tolower'] = TRUE;
            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $NewFileName = $filename . "." . $exts;
            if (!$this->upload->do_upload('image', $NewFileName)) {
                $error = array('error' => $this->upload->display_errors());
                $NewFileName = "";
            } else {
                $this->upload->do_upload('image', $NewFileName);
            }

            $NewData = array();
            $NewData = array(
                'company_id' => $company_id,
                'customer_id' => $customer_id,
                'project_id' => $project_id,
                'user_id' => $user_id,
                'thedate' => $thedate,
                'expired_date' => $expired_date,
                'totalvalue' => $totalvalue,
                'paid' => $paid,
                'remaining' => $remaining,
                'discount' => $discount,
                'over_cost' => $over_cost,
                'tax' => $tax,
                'notes' => $notes,
                'image' => $NewFileName,
                'tree_id' => $tree_id,
            );

            $this->M_sale_offer->InsertRecord($NewData);

            $LatestRecord = $this->M_sale_offer->GetLatestRecord($company_id, $customer_id, $user_id);
            $bill_id = $LatestRecord->id;

            $InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
            //M_product
            $barcode = "barcode";
            $location_id = "location_id";
            $quantity = "quantity";
            $RowsCount = set_value('RowsCount');
            for ($i = 1; $i <= $RowsCount; $i++) {
                $quantity = "quantity" . $i;
                $quantity = $this->input->post($quantity);
                if ($quantity > 0) {
                    $barcode = "barcode" . $i;
                    $barcode = $this->input->post($barcode);
                    $ProductData = $this->M_product->GetByBarcode($barcode);
                    $product_id = $ProductData->id;
                    $location_id = "location_id" . $i;
                    $location_id = $this->input->post($location_id);
                    $unitprice = "unitprice" . $i;
                    $unitprice = $this->input->post($unitprice);


                    //M_sale_offer_items
                    $ItemsArray = array();
                    $ItemsArray = array(
                        'bill_id' => $bill_id,
                        'store_type_id' => $product_id,
                        'location_id' => $location_id,
                        'quantity' => $quantity,
                        'unitprice' => $unitprice,
                    );
                    $this->M_sale_offer_items->InsertRecord($ItemsArray);
                }

            }


            redirect('sales/sale_offer');
        }
    }

    public function updateform($rid)
    {
        $this->session->set_userdata('Filter', "");

        $DataRow = $this->M_sale_offer->GetRow($rid);
        $data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
        $data['customer_id'] = $DataRow->customer_id;
        $data['project_id'] = $DataRow->project_id;
        $data['user_id'] = $DataRow->user_id;
        $data['thedate'] = $DataRow->thedate;
        $data['expired_date'] = $DataRow->expired_date;
        $data['totalvalue'] = $DataRow->totalvalue;
        $data['paid'] = $DataRow->paid;
        $data['remaining'] = $DataRow->remaining;
        $data['discount'] = $DataRow->discount;
        $data['over_cost'] = $DataRow->over_cost;
        $data['tax'] = $DataRow->tax;
        $data['notes'] = $DataRow->notes;
        $data['image'] = $DataRow->image;
        $data['tree_id'] = $DataRow->tree_id;
        $data['deleted'] = $DataRow->deleted;

        $data['DetailsRows'] = $this->M_sale_offer_items->GetMultiRow($rid);

        $data['ControllerName'] = $this->router->fetch_class();
        $data['content_page'] = "sales/sale_offer_update";
        $this->load->view('page', $data);
    }

    public function Update_Data()
    {
        $this->session->set_userdata('Filter', "");

        $company_id = intval($this->session->userdata('company_id'));
        $StaffID = intval($this->session->userdata('StaffID'));

        $this->form_validation->set_rules("customer_id", "customer_id", "required");
        $this->form_validation->set_rules("tree_id", "tree_id", "required");

        if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
        } else {
            $id = set_value('id');
            $company_id = $company_id;
            $customer_id = set_value("customer_id");
            $project_id = set_value("project_id");
            $user_id = $StaffID;
            $thedate = set_value('thedate');
            $expired_date = set_value('expired_date');
            $totalvalue = set_value('totalvalue');
            $paid = set_value('paid');
            $remaining = set_value('remaining');
            $discount = set_value('discount');
            $over_cost = set_value('over_cost');
            $tax = set_value('tax');
            $notes = set_value('notes');
            $tree_id = set_value('tree_id');
            $ChangeImage = set_value('ChangeImage');

            if ($ChangeImage == "True") {
                $filename = date("Y") . "-" . date("m") . "-" . date("d") . "-" . date("h") . "-" . date("i") . "-" . date("s");
                $exts = explode(".", $_FILES['image']['name']);
                $exts = end($exts);
                $exts = strtolower($exts);
                $UploadPath = "./upload/sale_offer/";
                $ImageDirectory = $UploadPath;

                if (!is_dir($ImageDirectory)) {
                    mkdir($ImageDirectory, 0777, TRUE);

                }

                $config['upload_path'] = $ImageDirectory;
                $config['allowed_types'] = "*";
                $config['file_name'] = $filename . "." . $exts;
                $config['overwrite'] = TRUE;
                $config['file_ext_tolower'] = TRUE;
                $config['remove_spaces'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $NewFileName = $filename . "." . $exts;
                if (!$this->upload->do_upload('image', $NewFileName)) {
                    $error = array('error' => $this->upload->display_errors());
                    $NewFileName = "";
                } else {
                    $this->upload->do_upload('image', $NewFileName);
                }

                $NewData = array();
                $NewData = array(
                    'company_id' => $company_id,
                    'customer_id' => $customer_id,
                    'project_id' => $project_id,
                    'user_id' => $user_id,
                    'thedate' => $thedate,
                    'expired_date' => $expired_date,
                    'totalvalue' => $totalvalue,
                    'paid' => $paid,
                    'remaining' => $remaining,
                    'discount' => $discount,
                    'over_cost' => $over_cost,
                    'tax' => $tax,
                    'notes' => $notes,
                    'image' => $NewFileName,
                    'tree_id' => $tree_id,
                );
            } else {
                $NewData = array();
                $NewData = array(
                    'company_id' => $company_id,
                    'customer_id' => $customer_id,
                    'project_id' => $project_id,
                    'user_id' => $user_id,
                    'thedate' => $thedate,
                    'expired_date' => $expired_date,
                    'totalvalue' => $totalvalue,
                    'paid' => $paid,
                    'remaining' => $remaining,
                    'discount' => $discount,
                    'over_cost' => $over_cost,
                    'tax' => $tax,
                    'notes' => $notes,
                    'tree_id' => $tree_id,
                );
            }

            $this->M_sale_offer->UpdateRecord($id, $NewData);
            $bill_id = $id;
            $DetailsRows = $this->M_sale_offer_items->GetMultiRow($id);
            $i = 0;
            $InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
            //M_product
            $barcode = "barcode";
            $location_id = "location_id";
            $quantity = "quantity";
            $item = "item";
            foreach ($DetailsRows as $DetailsRows_Row) {
                $i += 1;
                $quantity = "quantity" . $i;
                $quantity = $this->input->post($quantity);
                $item = "item" . $i;
                $item = $this->input->post($item);
                if ($quantity > 0) {
                    $barcode = "barcode" . $i;
                    $barcode = $this->input->post($barcode);
                    $ProductData = $this->M_product->GetByBarcode($barcode);
                    $product_id = $ProductData->id;
                    $location_id = "location_id" . $i;
                    $location_id = $this->input->post($location_id);
                    $unitprice = "unitprice" . $i;
                    $unitprice = $this->input->post($unitprice);

                    //M_sale_offer_items
                    $ItemsArray = array();
                    $ItemsArray = array(
                        'bill_id' => $bill_id,
                        'store_type_id' => $product_id,
                        'location_id' => $location_id,
                        'quantity' => $quantity,
                        'unitprice' => $unitprice,
                    );
                    $this->M_sale_offer_items->UpdateRecord($item, $ItemsArray);
                }

            }

            redirect('sales/sale_offer');
        }
    }

    public function FillProductsPrice($barcode)
    {
        $CodeCount = $this->M_product->CheckBarcode($barcode);
        if ($CodeCount > 0) {
            $TypeName = $this->M_product->GetByBarcode($barcode);
            echo $TypeName->price;
        } else {
            echo "0";
        }
    }

    public function delete($rid)
    {
        $deleted = 1;
        $NewData = array();
        $NewData = array(
            'deleted' => $deleted,
        );

        $this->M_sale_offer->UpdateRecord($rid, $NewData);

        redirect("sale_offer");
    }

    public function undelete($rid)
    {
        $deleted = 0;
        $NewData = array();
        $NewData = array(
            'deleted' => $deleted,
        );

        $this->M_sale_offer->UpdateRecord($rid, $NewData);

        redirect("sale_offer");
    }

    public function FillProductsList($barcode)
    {
        $CodeCount = $this->M_product->CheckBarcode($barcode);
        if ($CodeCount > 0) {
            $TypeName = $this->M_product->GetByBarcode($barcode);
            echo $TypeName->title;
        } else {
            echo "-";
        }
    }

    public function fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $image)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $user_id = intval($this->session->userdata('StaffID'));
        $automatic = 1;
        $deleted = 0;

        $fin_journal_array = array();
        $fin_journal_array = array(
            'company_id' => $company_id,
            'user_id' => $user_id,
            'details' => $details,
            'thedate' => $thedate,
            'bill_id' => $bill_id,
            'table_name' => $table_name,
            'bond_no' => $bond_no,
            'image' => $image,
            'automatic' => $automatic,
            'deleted' => $deleted,
        );

        $this->M_fin_journal_main->InsertRecord($fin_journal_array);
    }

    public function fin_journal_insert($account_id, $debit, $creditor)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $user_id = intval($this->session->userdata('StaffID'));

        $MainData = $this->M_fin_journal_main->GetLatestRecord($company_id, $user_id);
        $main_id = $MainData->id;

        $fin_journal_array = array();
        $fin_journal_array = array(
            'main_id' => $main_id,
            'account_id' => $account_id,
            'debit' => $debit,
            'creditor' => $creditor,
        );

        $this->M_fin_journal->InsertRecord($fin_journal_array);
    }

    public function view_main()
    {
        $id = $this->input->get('id');
        $MainDataRow = $this->M_sale_offer->GetInvoiceHeader($id);
        echo json_encode($MainDataRow);
        exit();
    }

    public function view_details()
    {
        $id = $this->input->get('id');
        //$id = intval($this->session->userdata('Fin_journal_View_ID'));
        $DetailsRows = $this->M_sale_offer->GetInvoiceDetails($id);
        echo json_encode($DetailsRows);
        exit();
    }

    public function relais($id)
    {
        $this->load->model('sales/M_sale_bill');
        $this->load->model('sales/M_sale_bill_items');
        $MainDataRow = $this->M_sale_offer->GetInvoiceHeader($id);
        $DetailsRows = $this->M_sale_offer->GetInvoiceDetails($id);

        $customer = $this->M_sale_customer->GetRow($MainDataRow->customer_id);
        $totalWthTax=$MainDataRow->totalvalue+($MainDataRow->totalvalue*$MainDataRow->tax /100);
        $NewData = array(
            'company_id' => $MainDataRow->company_id,
            'customer_id' => $MainDataRow->customer_id,
            'customer_name' => $customer->ar_title,
            'customer_phone' => $customer->mobile1,
            'project_id' => 0,
            'user_id' => $MainDataRow->user_id,
            'thedate' => date("Y-m-d"),
            'totalvalue' => $totalWthTax,
            'paid' => 0,
            'remaining' => $totalWthTax,
            'discount' => $MainDataRow->discount,
            'tax' => $MainDataRow->tax,
            'notes' => $MainDataRow->notes,
            'image' => $MainDataRow->image,
            'tree_id' => $MainDataRow->tree_id,//??
            'tree_id1' => 0,//??
            'paid1' => 0,
            'paid2' => 0,
            'paid3' => 0,
        );
        $this->M_sale_bill->InsertRecord($NewData);
        $tax = $MainDataRow->tax;
        $company_id = $MainDataRow->company_id;
        $customer_id = $MainDataRow->customer_id;
        $user_id = $MainDataRow->user_id;
        $LatestRecord = $this->M_sale_bill->GetLatestRecord($company_id, $customer_id, $user_id);
        $bill_id = $LatestRecord->id;
        $details = $MainDataRow->notes;
        $table_name = "sale_bill";
        $bond_no = $bill_id;
        $image = $MainDataRow->image;
        $thedate = date("Y-m-d");
        $NewFileName = $MainDataRow->image;
        $TaxAmount = $MainDataRow->totalvalue * ($MainDataRow->tax / 100);
        $this->fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $NewFileName);
        $totalvalue = $MainDataRow->totalvalue;
        $subtotal1 = 0;
        foreach ($DetailsRows as $item) {
            $subtotal1 += $item->subtotal;
        }
        /***********khaled***********/
        /**********start**********/

        /******* من حساب العميل ********/

        $customer_account_id = $customer_id;
        $debit = $totalWthTax;
        $creditor = 0;
        $this->fin_journal_insert($customer_account_id, $debit, $creditor);

        /******* إلى حساب المبيعات ********/
        $account_id = intval($this->session->userdata('acc_sale'));

       

        $debit = 0;
        $creditor = $totalWthTax -$TaxAmount ;
        $this->fin_journal_insert($account_id, $debit, $creditor);

        /******* إلى قيد ضريبة المبيعات ********/
        $account_id = intval($this->session->userdata('acc_tax_sale'));
        $debit = 0;
        $creditor = $TaxAmount;
        $this->fin_journal_insert($account_id, $debit, $creditor);

        /************end************/


        ///////////////////////////
        //تحديث كميات الأصناف بالمخزن

        foreach ($DetailsRows as $item) {
            $quantity = $item->quantity;
            $received = $item->quantity;

            if ($received > 0) {
                $product_id = $item->id;
                $location_id = "0";
                $unitprice = $item->unitprice;
//                $Old_FromStoreData = $this->M_store_quantity->GetStoreProduct($location_id, $product_id);
//                $Old_FromStore_Quantity = $Old_FromStoreData->quantity;
//                $Old_FromStore_id = $Old_FromStoreData->id;
//                $New_FromStore_Quantity = $Old_FromStore_Quantity - $received;
//                $FromStoreArrayData = array();
//                $FromStoreArrayData = array(
//                    'store_id' => $location_id,
//                    'product_id' => $product_id,
//                    'quantity' => $New_FromStore_Quantity,
//                );
//                $this->M_store_quantity->UpdateRecord($Old_FromStore_id, $FromStoreArrayData);
//                $POS_Quantity_Data = array();
//                $POS_Quantity_Data = array(
//                    'product_id' => $product_id,
//                    'store_id' => $location_id,
//                    'quantity' => $New_FromStore_Quantity,
//                );
//                $this->M_Tec_product_store_qty->UpdateRecord($Old_FromStore_id, $POS_Quantity_Data);
//                //M_sale_bill_items
                $ItemsArray = array();
                $ItemsArray = array(
                    'bill_id' => $bill_id,
                    'store_type_id' => $product_id,
                    'location_id' => $location_id,
                    'quantity' => $quantity,
                    'unitprice' => $unitprice,
                    'received' => $received,
                );
                $this->M_sale_bill_items->InsertRecord($ItemsArray);
            }

        }
         $this->M_sale_offer_items->deleteOffer($id);
        redirect('sales/sale_bill');


    }

    public function view_pdf($invoice_id)
    {

        $Alignment = "";
        $Direction = "";
        if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar")) {
            $this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
            $this->session->set_userdata('Alignment', "right");
            $this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
            $Alignment = "right";
            $Direction = "rtl";
        } else {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
            $this->session->set_userdata('Alignment', "left");
            $this->session->set_userdata('HTML', "lang='en'");
            $Alignment = "left";
            $Direction = "ltr";
        }
        $this->load->model("settings/M_fin_settings");
        $this->load->model("settings/M_tec_settings");
        $this->load->model('sales/M_sale_bill');

        $company_id = intval($this->session->userdata('company_id'));
        $DataRow = $this->M_app_company->GetRow($company_id);
        $tax_date = $DataRow->start_date;
        $HeaderImage = base_url() . "upload/settings/" . $DataRow->header;
        $FooterImage = base_url() . "upload/settings/" . $DataRow->footer;
        $Directionality = $this->session->userdata('Direction');
        $PageTitle = lang('pdf_sale_offer');
        $PageSize = $this->session->userdata('sale_bill_size');
        $PageOrientation = $this->session->userdata('sale_bill_orientation');
        $PageFormat = $PageSize . "-" . $PageOrientation;
        require_once 'vendor/autoload.php';
        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => $PageFormat,
            //'margin_left' => 5,
            //'margin_right' => 5,
            'margin_top' => 25,
            'margin_bottom' => 25,
            'margin_header' => 5,
            'margin_footer' => 5,
        ]);
        $mpdf->allow_charset_conversion = true;
        $mpdf->SetTitle($PageTitle);
        $mpdf->SetDirectionality($Directionality);
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;
        $mpdf->debug = true;
        ob_end_clean();
        header("Content-Encoding: None", true);
        $mpdf->SetHTMLHeader('
 		<div style="text-align: center; font-weight: bold;">
 			<img src="' . $HeaderImage . '">
 		</div>');
        $mpdf->SetHTMLFooter('
 		<table width="100%" style="vertical-align: bottom; font-family: serif; 
 			font-size: 8pt; color: #000000; font-weight: bold;">
 			<tr style="border:none!important;">
 				<td colspan="2" style="border:none!important;"><img src="' . $FooterImage . '"></td>
 			</tr>
 			<tr style="border:none!important;">
 				<td width="50%" style="border:none!important;">{DATE j-m-Y}</td>
 				<td width="50%" style="border:none!important;" align="left" lang="en">{PAGENO}/{nbpg}</td>
 			</tr>
 		</table>');
        $DataRow = $this->M_sale_offer->GetInvoiceHeader($invoice_id);
        $MultiRows = $this->M_sale_offer->GetInvoiceDetails($invoice_id);
        $customer = $this->M_sale_customer->GetRow($DataRow->customer_id);

        $data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
        $company_id = $DataRow->company_id;
        $tec_settings = $this->M_tec_settings->GetRow();
        $customer_name = $customer->ar_title;
        $customer_phone = $customer->phone;
        $CompanyData = $this->M_app_company->GetRow($company_id);
        $tax_number = $tec_settings->tax_number;
        $Invoice_Start = $tec_settings->tax_date;
        $customer_id = $DataRow->customer_id;
        $CustomerData = $this->M_sale_customer->GetRow($customer_id);
        if ($customer_name == "") {
            if ($this->session->userdata('lang') == "ar") {
                $CustomerName = $CustomerData->ar_title;
            } else {
                $CustomerName = $CustomerData->en_title;
            }
        } else {
            $CustomerName = $customer_name;
        }
        if ($customer_phone == "") {
            $CustomerPhone = $CustomerData->phone;
        } else {
            $CustomerPhone = $customer_phone;
        }
        $user_id = $DataRow->user_id;
        $UserData = $this->M_usr_users->GetRow($user_id);
        $InvoiceEditor = $UserData->fullname;
        $sale_start = $UserData->sale_start;
        $branch = $UserData->branch;
        if ($branch != "") {
            $branch = " (" . $branch . ")";
        }
        if ($sale_start != "") {
            $Invoice_Start = $UserData->sale_start;
        }

        $thedate = $DataRow->thedate;
        $html = '<style>td, th{border:solid 1px #dddddd;}</style>';
        $html .= '<div align="center" style="font-size:18px">';
        $html .= '<strong>' . $PageTitle . '</strong><br>';
        //$html .= $PageTitle.'<br>('.$invoice_id.')-'.$DataRow->thedate."<br>";
        $html .= '</div><br>';
        $html .= '<table cellspacing="0" cellpadding="5" border="0" width="98%">';
        $html .= '<tbody>';
        $html .= '<tr style="border:none !important;vertical-align: baseline !important;">';
        $html .= '<td colspan="1" style="padding-right:0 !important;border:none !important;">';
        $html .= '<table cellspacing="0" cellpadding="5" width="100%">';
        $html .= '<thead>';
        $html .= '<tr style="background-color:#EBEBEB; font-weight:bold;border:none !important;">';
        $html .= '<th colspan="3">بيانات</th>';
        $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<tbody>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('Site Name') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tec_settings->site_name . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('phone') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tec_settings->tel . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('address') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tec_settings->address . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('buy_manufacturer_tax_card') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tax_number . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('buy_manufacturer_tax_card_date') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tax_date . '</td>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</td>';
        //Customer
        $html .= '<td colspan="1" style="padding-left:0;border:none !important;">';
        $html .= '<table cellspacing="0" cellpadding="5" width="100%">';
        $html .= '<thead>';
        $html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
        $html .= '<th colspan="4">بيانات</th>';
        $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<tbody>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('sale_fullname') . '</td>';
        $html .= '<td colspan="3" style="border: 1px solid #dddddd;">' . $CustomerName . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('phone') . '</td>';
        $html .= '<td colspan="3" style="border: 1px solid #dddddd;">' . $CustomerData->phone . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('address') . '</td>';
        $html .= '<td colspan="3" style="border: 1px solid #dddddd;">' . $CustomerData->address . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('tax_card') . '</td>';
        $html .= '<td colspan="3" style="border: 1px solid #dddddd;">' . $CustomerData->tax_card . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('thedate') . '</td>';
        $html .= '<td colspan="3" style="border: 1px solid #dddddd;">' . $DataRow->thedate . '</td>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '<table cellspacing="0" cellpadding="5" width="98%">';
        $html .= '<tbody>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('sale_bill_totalvalue') . ' :</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;"><b>' . number_format($DataRow->totalvalue, 2) . '</b></td>';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('pdf_Invoice_Editor') . ' :</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;"><b>' . $InvoiceEditor . '</b></td>';
        $html .= '</tr>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</div><br>';
        $html .= '<table cellspacing="0" cellpadding="5" width="98%" style="border: solid 1px #dddddd;">';
        $html .= '<thead>';
        $html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('Serial') . '</th>';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('product_barcode') . '</th>';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('sale_bill_items_store_type_id') . '</th>';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('sale_bill_items_quantity') . '</th>';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('sale_bill_items_unitprice') . '</th>';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('subtotal') . '</th>';
        $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<tbody>';
        $Serial = 0;
        $TotalValue = 0;
        $AllQuantity = 0;
        $FinalBigQ = 0;
        $FinalSmallQ = 0;
        foreach ($MultiRows as $MultiRows_Row) {
            $Serial += 1;
            $type_id = $MultiRows_Row->store_type_id;
            $ProductData = $this->M_product->GetRow($type_id);
            if ($this->session->userdata('lang') == "ar") {
                $ProductName = $ProductData->title;
            } else {
                $ProductName = $ProductData->title_en;
            }
           // $location_id = $MultiRows_Row->location_id;
//            $StoreData = $this->M_store->GetRow($location_id);
//            if ($this->session->userdata('lang') == "ar") {
//                $StoreName = $StoreData->title;
//            } else {
//                $StoreName = $StoreData->title_en;
//            }
            $StoreName = "";
            $html .= '<tr>';
            $html .= '<td style="background-color:#EBEBEB; font-weight:bold;">' . $Serial . '</td>';
            $html .= '<td>' . $ProductData->barcode . '</td>';
            $html .= '<td>' . $ProductName . '</td>';
            $html .= '<td align="center">' . $MultiRows_Row->quantity . '</td>';
            $AllQuantity += $MultiRows_Row->quantity;
            $html .= '<td align="center">' . number_format($MultiRows_Row->unitprice, 2) . '</td>';
            $SubTotal = doubleval($MultiRows_Row->quantity) * doubleval($MultiRows_Row->unitprice);
            $html .= '<td align="center">' . number_format($MultiRows_Row->quantity * $MultiRows_Row->unitprice, 2) . '</td>';
            $html .= '</tr>';
            $TotalValue += $SubTotal;
        }
        $tax = $DataRow->tax;
        $taxValue = ($TotalValue - $DataRow->discount) * ($tax / 100);
        $totalPrice = doubleval($TotalValue + $taxValue) - doubleval($DataRow->discount);
        $Items = $this->lang->line('invoice_items') . ' : ' . $Serial;
        $html .= '<tr>';
        $html .= '<td colspan="4" style="border:none!important">' . $Items . '</td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_totalitemsvalue') . '</td>';
        $html .= '<td align="center">' . number_format($TotalValue, 2) . '</td>';
        $html .= '</tr>';
        $QuantityText = $this->lang->line('sale_bill_items_quantity') . ' : ' . $AllQuantity;
        $html .= '<tr>';
        $html .= '<td colspan="4" style="border:none!important">' . $QuantityText . '</td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_discount') . '</td>';
        $html .= '<td align="center">' . number_format($DataRow->discount, 2) . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="4" style="border:none!important">';
        $html .= '</td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_total_after_discount') . '</td>';
        $html .= '<td align="center">' . number_format($TotalValue - $DataRow->discount, 2) . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="4" style="border:none!important"></td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_required_price') . '</td>';
        $html .= '<td align="center">' . number_format($taxValue, 2) . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="4" style="border:none!important"></td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_totalvalue') . '</td>';
        $html .= '<td align="center">' . number_format($DataRow->totalvalue +$taxValue, 2) . '</td>';
        $html .= '</tr>';

        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '<br>';
        $html .= '<table cellspacing="0" cellpadding="5" border="0" width="98%" align="center">';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;">' . $this->lang->line('IReceived') . '</td>';
        $html .= '<td style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;">' . $InvoiceEditor . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="center" style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;">' . $this->lang->line('ReceviesSign') . '</td>';
        $html .= '<td align="center" rowspan="6" style="border:none!important;">';
//        $this->load->library('QR');
//        $html .= QR::get_base($tec_settings->site_name,$tax_number,$DataRow->thedate, $DataRow->totalvalue,$taxValue);
        $html .= '</td>';
        $html .= '<td align="left" style="border:none!important;">' . $this->lang->line('SalesmanSign') . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;">..........................................</td>';
        $html .= '<td align="left" style="border:none!important;">..........................................</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        if (isset($_GET['test'])) {
            echo $html;
            exit;
        }
        $html = iconv("utf-8", "UTF-8//IGNORE", $html);
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
    }


}
