<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Sale_customer extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		
		$this->load->model("sales/M_sale_customer");
        $this->load->model("account/M_fin_treeaccount");
        $this->load->model("account/M_fin_journal");
        $this->load->model("store/M_store");
        $this->load->model("store/M_product");


        $this->load->model('purchase/M_buy_bill');
		$this->load->model('sales/M_sale_bill');
		$this->load->model('sales/M_sale_returns');
		$this->load->model('purchase/M_buy_manufacturer');
        $this->load->model('pos_sys/M_Tec_customers');
	}
    
	public function index()
	{
        $data['MultiRows'] = $this->M_sale_customer->GetMultiRow();
        $data['Search'] = "False";
        $data['content_page'] = "sales/sale_customer";
		$this->load->view('page', $data);
	}
    
    public function insertform()
    {
        $data['content_page'] = "sales/sale_customer_insert";
		$this->load->view('page', $data);
    }
    
    public function insert_data()
    {
		$this->load->helper('security');
        $this->load->library("form_validation");
        
        $this->form_validation->set_rules("ar_title","ar_title","required");
        
        if ($this->form_validation->run() == FALSE) {
            echo validation_errors();
        }
        else {
            $company_id = intval($this->session->userdata('company_id'));
			$user_id = intval($this->session->userdata('StaffID'));
            $ar_title = set_value('ar_title');
            $en_title = set_value('en_title');
            $phone = set_value('phone');
            $mobile1 = set_value('mobile1');
            $mobile2 = set_value('mobile2');
            $address = set_value('address');
            $email = set_value('email');
            $commercial_register = set_value('commercial_register');
            $tax_card = set_value('tax_card');
            $person = set_value('person');
            $person_phone = set_value('person_phone');
            $credit_limit = set_value('credit_limit');
            $active = set_value('active');
            $deleted = 0;
            $CustomersOweID = intval($this->session->userdata('acc_customer'));
            
            if ($this->M_sale_customer->CheckDuplicate($ar_title, $en_title, $phone) ==0)
            {
                $category_id = 1; //
                $parent_id = $CustomersOweID; //
                // $account_no = set_value('account_no');
                $max=$this->M_fin_treeaccount->GetMaxAccount($parent_id)[0];
                $my_account= '';
                if(!is_null($max->max)){
                    $my_account=$max->max + 1;
                }else{
                    $ParentAccountData = $this->M_fin_treeaccount->GetRow($ar_title); 
                     $my_account=$ParentAccountData->account_no + 1;
                }
                $account_no = $my_account;
                $ar_title = set_value('ar_title');
                $en_title = set_value('en_title');
                $thevalue = set_value('thevalue');
                
                $TreeAccount_array = array();
                $TreeAccount_array = array(
                    'category_id' => 1,
                    'parent_id' => $parent_id,
                    'account_no' => $account_no,
                    'title' => $ar_title,
                    'title_en' => $en_title,
                    'startamount' => $thevalue,
                    'basic' => 0,
                    'level_no' => 4,
                    'deleted' => 0,
                );
            
                $this->M_fin_treeaccount->InsertRecord($TreeAccount_array);
                $AccountData = $this->M_fin_treeaccount->GetAccountID($ar_title); 
                $AccountID = $AccountData->id;
                
                $this->M_sale_customer->InsertRecord($AccountID, $company_id, $user_id, $ar_title, $en_title, $phone, $mobile1, $mobile2, 
                $address, $email, $commercial_register, $tax_card, $person, $person_phone, $credit_limit, $active, $deleted);

                $NewData = array();
                $NewData = array(
                    'id' => $AccountID,
                    'name' => $ar_title,
                    'phone' => $phone,
                    'email' => $email,
                );
                $this->M_Tec_customers->InsertRecord($NewData);
            }
            
            $data['MultiRows'] = $this->M_sale_customer->GetMultiRow();

            redirect ("sales/sale_customer");
        }
    }
    
    public function Insert_Without_Redirect()
    {
        ini_set("memory_limit","512M");
        $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $thedate = date("Y-m-d");
 

        $this->load->helper('security');
        $this->load->library("form_validation");

        $this->form_validation->set_rules("ar_title","ar_title","required");

        if ($this->form_validation->run() == FALSE) {
            echo validation_errors();
        }
        else {
            $company_id = intval($this->session->userdata('company_id'));
			$user_id = intval($this->session->userdata('StaffID'));
            $ar_title = set_value('ar_title');
            $en_title = set_value('en_title');
            $phone = set_value('phone');
            $mobile1 = set_value('mobile1');
            $mobile2 = set_value('mobile2');
            $address = set_value('address');
            $email = set_value('email');
            $commercial_register = set_value('commercial_register');
            $tax_card = set_value('tax_card');
            $person = set_value('person');
            $person_phone = set_value('person_phone');
            $credit_limit = set_value('credit_limit');
            $active = set_value('active');

            $CustomersOweID = intval($this->session->userdata('appsetting_sale_field1'));

            if ($this->M_sale_customer->CheckDuplicate($ar_title, $en_title, $phone) ==0)
            {
                $category_id = 1; //
                $parent_id = $CustomersOweID; //
                $account_no = set_value('account_no');
                $ar_title = set_value('ar_title');
                $en_title = set_value('en_title');
                $thevalue = set_value('thevalue');

                $TreeAccount_array = array();
                $TreeAccount_array = array(
                    'category_id' => 1,
                    'parent_id' => $parent_id,
                    'account_no' => $account_no,
                    'title' => $ar_title,
                    'title_en' => $en_title,
                    'startamount' => $thevalue,
                    'basic' => 0,
                    'level_no' => 3,
                    'deleted' => 0,
                );
            
                $this->M_fin_treeaccount->InsertRecord($TreeAccount_array);

                $AccountData = $this->M_fin_treeaccount->GetAccountID($ar_title); 
                $AccountID = $AccountData->id;

                $this->M_sale_customer->InsertRecord($AccountID, $company_id, $user_id, $ar_title, $en_title, $phone, $mobile1, $mobile2, 
                $address, $email, $commercial_register, $tax_card, $person, $person_phone, $credit_limit, $active);
            }

            $data['MultiRows'] = $this->M_sale_customer->GetMultiRow();
        }
    }
    
    public function updateform($rid)
    {
		$MyRecord = $this->M_sale_customer->GetRow($rid); 
        $data['DataRow_id'] = $MyRecord->id;
        $data['DataRow_ar_title'] = $MyRecord->ar_title;
        $data['DataRow_en_title'] = $MyRecord->en_title;
        $data['DataRow_phone'] = $MyRecord->phone;
        $data['DataRow_mobile1'] = $MyRecord->mobile1;
        $data['DataRow_mobile2'] = $MyRecord->mobile2;
        $data['DataRow_address'] = $MyRecord->address;
        $data['DataRow_email'] = $MyRecord->email;
        $data['DataRow_commercial_register'] = $MyRecord->commercial_register;
        $data['DataRow_tax_card'] = $MyRecord->tax_card;
        $data['DataRow_person'] = $MyRecord->person;
        $data['DataRow_person_phone'] = $MyRecord->person_phone;
        $data['DataRow_credit_limit'] = $MyRecord->credit_limit;
        $data['DataRow_active'] = $MyRecord->active;

        $data['content_page'] = "sales/sale_customer_update";
		$this->load->view('page', $data);
    }
    
    public function update_data()
    {
		$this->load->helper('security');
        $this->load->library("form_validation");
        
        $this->form_validation->set_rules("ar_title","ar_title","required");
        
        if ($this->form_validation->run() == FALSE) {
            echo validation_errors();
        }
        else {
            $id = set_value('id');
            $ar_title = set_value('ar_title');
            $en_title = set_value('en_title');
            $phone = set_value('phone');
            $mobile1 = set_value('mobile1');
            $mobile2 = set_value('mobile2');
            $address = set_value('address');
            $email = set_value('email');
            $commercial_register = set_value('commercial_register');
            $tax_card = set_value('tax_card');
            $person = set_value('person');
            $person_phone = set_value('person_phone');
            $credit_limit = set_value('credit_limit');
            $active = set_value('active');
            
            if ($this->M_sale_customer->CheckDuplicate($ar_title, $en_title, $phone) <=1)
            {
                $this->M_sale_customer->UpdateRecord($id, $ar_title, $en_title, $phone, 
                $mobile1, $mobile2, $address, $email, $commercial_register, $tax_card, $person, $person_phone, $credit_limit, $active);
            }
        }
        
        
        $data['MultiRows'] = $this->M_sale_customer->GetMultiRow();
        redirect ("sales/sale_customer");
    }

    public function pdf()
    {
		
        
    }
    
	public function indebtedness()
	{
		$data['MultiRows'] = $this->M_sale_customer->GetMultiRow();
        
        $data['fromdate'] = date("Y-m-d");
        $data['todate'] = date("Y-m-d");
        $data['content_page'] = "purchase/sale_customer_indebtedness";
		$this->load->view('page', $data);
	}

	public function customerbills()
	{
		$this->load->helper('security');
        $this->load->library("form_validation");
		
		if (set_value('fromdate') != null)
		{
			$fromdate = set_value('fromdate');
		}
		else
		{
			$fromdate = date("Y-m-d");
		}
		$data['fromdate'] = $fromdate;
		
		if (set_value('todate') != null)
		{
			$todate = set_value('todate');
		}
		else
		{
			$todate = date("Y-m-d");
		}
		$data['todate'] = $todate;
		
		if (set_value('ChkType') != "True")
		{
			$ChkType = set_value('ChkType');
			$TypeID = set_value('TypeID');
		}
		else
		{
			$ChkType = "false";
			$TypeID = 0;
		}
		$data['ChkType'] = $ChkType;
		$data['TypeID'] = $TypeID;
		
		if (set_value('ChkStorage') != "True")
		{
			$ChkStorage = set_value('ChkStorage');
			$StorageId = set_value('StorageId');
		}
		else
		{
			$ChkStorage = "false";
			$StorageId = 0;
		}
		$data['ChkStorage'] = $ChkStorage;
		$data['StorageId'] = $StorageId;
		
		
		if (set_value('ChkCustomer') != "True")
		{
			$ChkCustomer = set_value('ChkCustomer');
			$CustomerID = set_value('CustomerID');
		}
		else
		{
			$ChkCustomer = "false";
			$CustomerID = 0;
		}
		$data['ChkCustomer'] = $ChkCustomer;
		$data['CustomerID'] = $CustomerID;
		
		$data['Storage'] = $this->M_store->GetMultiRow();
		
		$data['Human'] = $this->M_sale_customer->GetMultiRow();
		
		$data['AllTypes'] = $this->M_product->GetMultiRow();
        
        $data['content_page'] = "store/storage_types_action1";
		$this->load->view('page', $data);
	}
    
    public function DeleteRow()
    {
        
        $this->load->helper('security');
        $this->load->library("form_validation");

        $this->form_validation->set_rules("table_name","table_name","required");
        
        $DeleteID = set_value('DeleteID');
        $table_name = "table_name".$DeleteID;
        $field = "field".$DeleteID;
        $field_value = "field_value".$DeleteID;
        
        $table_name = set_value($table_name);
        $field = set_value($field);
        $field_value = set_value($field_value);
        
        if($this->input->post('sbm') == "Delete") { 
            $this->db->delete("sale_customer", array($field => $field_value)); 
        }
        else
        {
            
        }
        $PageURL = "sales/sale_customer/";
        redirect ($PageURL);
    }
    
    public function search_data()
	{
		ini_set("memory_limit","512M");
	    $data['MySidebar'] = $this->M_app_module->GetMultiRow();
        $data['MySidebar_Sub'] = "";
        //////////////////////
        $MemberID = intval($this->session->userdata('StaffID'));
        if ($MemberID >0)
        {
            $thedate = date("Y-m-d");
            
            $ThisUser = $this->M_usr_users->GetRow($MemberID); 
            $this->session->set_userdata('ThisUserGroupID', $ThisUser->group_id);
            $this->session->set_userdata('ThisUserPicture', $ThisUser->image);
            
            $this->load->helper('security');
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("search","search","required");
            
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            }
            else {
                $search = set_value('search');
            }

            $data['MultiRows'] = $this->M_sale_customer->GetSearch($search);
            
            $this->load->view('sales/sale_customer', $data);
        }
        else {
            $this->load->view('login', $data);
        }
        
    }
    
    public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_sale_customer->UpdateDelete($rid, $NewData);

        redirect("sales/sale_customer");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_sale_customer->UpdateDelete($rid, $NewData);

        redirect("sales/sale_customer");
	}

    public function view_data()
    {
        $id = $this->input->get('id');
		$MainDataRow = $this->M_sale_customer->GetRow($id);
		echo json_encode($MainDataRow); 
    	exit();
    }

    public function sale_customer_email()
	{
        $data['MultiRows'] = $this->M_sale_customer_email->GetMultiRow();
        $data['Search'] = "False";
        $data['content_page'] = "sales/sale_customer_email";
		$this->load->view('page', $data);
	}

    public function view_email()
    {
        $id = $this->input->get('id');
        $readed = 1;
        $NewData = array();
        $NewData = array(
            'readed' => $readed,
        );
    
        $this->M_sale_customer_email->UpdateRecord($id, $NewData);

        
		$MainDataRow = $this->M_sale_customer_email->GetRow($id);
		echo json_encode($MainDataRow); 
    	exit();
    }

    public function scheduling()
    {
        $data['MultiRows'] = $this->M_sale_customer_schedule->GetMultiRow();
        $data['Search'] = "False";
        $data['content_page'] = "sales/scheduling";
		$this->load->view('page', $data);
    }

    public function scheduling_insert()
    {
        $this->form_validation->set_rules("amount","amount","required");
		$this->form_validation->set_rules("customer_id","customer_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $customer_id = set_value('customer_id');
			$thedate = "thedate";
			$amount = "amount";
			$paid = 0;
            $remaining = 0;

			$NewData = array();
            for ($i = 1; $i <= 12; $i++) {
                $thedate = "thedate".$i;
                $amount = "amount".$i;

                if($amount > 0)
                {
                    $thedate = set_value($thedate);
                    $amount = set_value($amount);
    
                    $NewData = array(
                        'customer_id' => $customer_id,
                        'thedate' => $thedate,
                        'amount' => $amount,
                        'paid' => $paid,
                        'remaining' => $remaining,
                    );
                    $this->M_sale_customer_schedule->InsertRecord($NewData);
                }
                
            }
			
			
			redirect (base_url());
		}
    }

    public function schedule_data($rid)
    {
        $data['MultiRows'] = $this->M_sale_customer_schedule->GetByCustomer($rid); 
        $data['content_page'] = "sales/schedule_data";
		$this->load->view('page', $data);
    }

    public function schedule_paid($rid)
    {
        $MyRecord = $this->M_sale_customer_schedule->GetRow($rid); 
        $data['id'] = $MyRecord->id;
        $data['customer_id'] = $MyRecord->customer_id;
        $data['thedate'] = $MyRecord->thedate;
        $data['amount'] = $MyRecord->amount;
        $data['paid'] = $MyRecord->paid;
        $data['remaining'] = $MyRecord->remaining;

        $data['content_page'] = "sales/schedule_paid";
		$this->load->view('page', $data);
    }

    public function schedule_paid_insert()
    {
        $this->form_validation->set_rules("amount","amount","required");
		$this->form_validation->set_rules("toaccount","toaccount","required");

        if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
        else
        {
            $id = set_value("id");
            $amount = set_value("amount");
            $fromaccount = set_value("fromaccount");
            $toaccount = set_value("toaccount");
            $details = set_value("details");
            $thedate = set_value("thedate");
            $paid = set_value("paid");
            $remaining = set_value("remaining");

            $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/sale_bill/";
			$ImageDirectory = $UploadPath;
			
			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
			}

            $table_name = "sale_customer_schedule";
            $bond_no = $id;
            $this->fin_journal_main_insert($details, $thedate, $id, $table_name, $bond_no, $NewFileName);
            //من حساب الخزينة أو البنك
            $account_id = $fromaccount;
            $debit = $paid;
            $creditor = 0;
            $this->fin_journal_insert($account_id, $debit, $creditor);
            //إلى حساب العميل
            $account_id = $toaccount;
            $debit = 0;
            $creditor = $paid;
            $this->fin_journal_insert($account_id, $debit, $creditor);

            $NewData = array();
            $remaining = $amount - $paid;
			$NewData = array(
				'paid' => $paid,
				'remaining' => $remaining,
			);
            $this->M_sale_customer_schedule->UpdateRecord($id, $NewData);
        }
    }

    public function fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $image)
	{
		$company_id = intval($this->session->userdata('company_id'));
		$user_id = intval($this->session->userdata('StaffID'));
		$automatic = 1;
		$deleted = 0;

		$fin_journal_array = array();
		$fin_journal_array = array(
			'company_id' => $company_id,
			'user_id' => $user_id,
			'details' => $details,
			'thedate' => $thedate,
			'bill_id' => $bill_id,
			'table_name' => $table_name,
			'bond_no' => $bond_no,
			'image' => $image,
			'automatic' => $automatic,
			'deleted' => $deleted,
		);

		$this->M_fin_journal_main->InsertRecord($fin_journal_array);
	}

	public function fin_journal_insert($account_id, $debit, $creditor)
	{
		$company_id = intval($this->session->userdata('company_id'));
		$user_id = intval($this->session->userdata('StaffID'));

		$MainData = $this->M_fin_journal_main->GetLatestRecord($company_id, $user_id);
		$main_id = $MainData->id;

		$fin_journal_array = array();
		$fin_journal_array = array(
			'main_id' => $main_id,
			'account_id' => $account_id,
			'debit' => $debit,
			'creditor' => $creditor,
		);

		$this->M_fin_journal->InsertRecord($fin_journal_array);
	}
}
