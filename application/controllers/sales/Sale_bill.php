<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Sale_bill extends CI_Controller
{
    var $data = array();
    public function __construct()
    {
        parent::__construct();
        // 		ini_set('display_errors', '1');
        // ini_set('display_startup_errors', '1');
        // error_reporting(E_ALL);
        $StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0) {
            redirect('login');
        } else {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if ($CheckView == 0) {
                redirect('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if (($Segment2 == $ControllerName) && ($Segment3 == "")) {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if ($Segment3 == "insertform") {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if ($CheckAdd == 0) {
                    redirect('home');
                }
            }
            if ($Segment3 == "insert_data") {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if ($Segment3 == "updateform") {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if ($CheckEdit == 0) {
                    redirect('home');
                }
            }
            if ($Segment3 == "update_data") {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if ($Segment3 == "delete") {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if ($CheckDelete == 0) {
                    redirect($_SERVER['HTTP_REFERER']);
                } else {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if ($Segment3 == "undelete") {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if ($CheckDelete == 0) {
                    redirect($_SERVER['HTTP_REFERER']);
                } else {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }
        $this->config->load('settings/config_setting');
        if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar")) {
            $this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
            $this->session->set_userdata('Alignment', "right");
            $this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
        } else {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
            $this->session->set_userdata('Alignment', "left");
            $this->session->set_userdata('HTML', "lang='en'");
        }
        $this->load->model("project/M_proj_project");
        $this->load->model("sales/M_sale_customer");
        $this->load->model('sales/M_sale_bill');
        $this->load->model('sales/M_sale_bill_items');
        $this->load->model("account/M_fin_treeaccount");
        $this->load->model("account/M_fin_journal");
        $this->load->model("store/M_store");
        $this->load->model("store/M_product");
        $this->load->model("store/M_store_quantity");
        $this->load->model("service/M_serv_message");
        $this->load->model("pos_sys/M_Tec_product_store_qty");
        $this->load->model("centers/M_center");
    		$this->load->model("centers/M_sub_centers");
    }
    public function index()
    {
        $data['TableTitle'] = "";
        $data['DataRows'] = $this->M_sale_bill->GetToday();
        $data['content_page'] = "sales/sale_bill";
        $this->load->view('page', $data);
    }
    public function forproject($project_id)
    {
        $DataRow = $this->M_proj_project->GetRow($project_id);
        $data['ar_title'] = $DataRow->ar_title;
        $data['en_title'] = $DataRow->en_title;
        if ($this->session->userdata('lang') == "ar") {
            $ProjectName = $DataRow->ar_title;
        } else {
            $ProjectName = $DataRow->en_title;
        }
        $data['TableTitle'] = $ProjectName . " :: " . lang('proj_project_Material');
        $data['DataRows'] = $this->M_sale_bill->GetByProject($project_id);
        $data['content_page'] = "sales/sale_bill";
        $this->load->view('page', $data);
    }
    public function insertform()
    {
        $data['ControllerName'] = $this->router->fetch_class();
        $data['content_page'] = "sales/sale_bill_insert";
        $centers = $this->M_center->GetMultiRow();
    		foreach ($centers as $center) {
    			$sub_centers = $this->M_sub_centers->GetMultiRowCenter($center->id);
    			$center->subs = $sub_centers;
    		}
    		$data['centers'] = $centers;
        $this->load->view('page', $data);
    }
    public function insert_data()
    {
        $company_id = intval($this->session->userdata('company_id'));
        $MemberID = intval($this->session->userdata('StaffID'));
        $this->form_validation->set_rules("customer_id", "customer_id", "required");
        $this->form_validation->set_rules("tree_id", "tree_id", "required");
        if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
        } else {
            $id = set_value('id');
            $company_id = set_value("company_id");
            $customer_id = set_value("customer_id");
            $customer_name = set_value("customer_name");
            $customer_phone = set_value("customer_phone");
            $sub_center_id = set_value("sub_center_id");
            $project_id = set_value("project_id");
            $user_id = $MemberID;
            $thedate = set_value('thedate');
            $totalvalue = set_value('totalvalue');
            $paid = set_value('paid');
            $remaining = set_value('remaining');
            $discount = set_value('discount');
            $tax = set_value('tax');
            $notes = set_value('notes');
            $tree_id = set_value('tree_id');
            $salesperson_id = set_value('salesperson_id');
            $storekeeper = set_value('storekeeper');
            $tree_id1 = set_value('tree_id1');
            $paid1 = set_value('paid1');
            $paid2 = set_value('paid2');
            $paid3 = set_value('paid3');
            $filename = date("Y") . "-" . date("m") . "-" . date("d") . "-" . date("h") . "-" . date("i") . "-" . date("s");
            $exts = explode(".", $_FILES['image']['name']);
            $exts = end($exts);
            $exts = strtolower($exts);
            $UploadPath = "./upload/sale_bill/";
            $ImageDirectory = $UploadPath;
            if (!is_dir($ImageDirectory)) {
                mkdir($ImageDirectory, 0777, TRUE);
            }
            $config['upload_path'] = $ImageDirectory;
            $config['allowed_types'] = "*";
            $config['file_name'] = $filename . "." . $exts;
            $config['overwrite'] = TRUE;
            $config['file_ext_tolower'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $NewFileName = $filename . "." . $exts;
            if (!$this->upload->do_upload('image', $NewFileName)) {
                $error = array('error' => $this->upload->display_errors());
                $NewFileName = "";
            } else {
                $this->upload->do_upload('image', $NewFileName);
            }
            $NewData = array();
            $sub_center_id = explode("_",$sub_center_id);

            $NewData = array(
                'company_id' => $company_id,
                'customer_id' => $customer_id,
                'customer_name' => $customer_name,
                'customer_phone' => $customer_phone,
                'project_id' => $project_id,
                'user_id' => $user_id,
                'thedate' => $thedate,
                'totalvalue' => $totalvalue,
                'paid' => $paid,
                'remaining' => $remaining,
                'discount' => $discount,
                'sub_center_id' => $sub_center_id[1],
        		'sub_center_type' => $sub_center_id[0],
                'tax' => $tax,
                'notes' => $notes,
                'image' => $NewFileName,
                'tree_id' => $tree_id,
                'tree_id1' => $tree_id1,
                'paid1' => $paid1,
                'paid2' => $paid2,
                'paid3' => $paid3,
            );
            //echo "<pre>";print_r($_POST);die("</pre>");
            $this->M_sale_bill->InsertRecord($NewData);
            // var_dump($NewData);
            // die();
            $LatestRecord = $this->M_sale_bill->GetLatestRecord($company_id, $customer_id, $user_id);
            $bill_id = $LatestRecord->id;
            $details = $notes;
            $table_name = "sale_bill";
            $bond_no = $bill_id;
            $image = $NewFileName;
            $TaxAmount = $totalvalue * ($tax / 100);
            $this->fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $NewFileName);
           
           
            /***********khaled***********/
            /**********start**********/

            /******* من حساب العميل ********/

            if ($project_id > 0) {
                $customer_account_id = $project_id;
            } else {
                $customer_account_id = $customer_id;
            }
            $debit = $totalvalue;
            $creditor = 0;
            $this->fin_journal_insert($customer_account_id, $debit, $creditor);

            /******* إلى حساب المبيعات ********/
            $account_id = intval($this->session->userdata('acc_sale'));

            $subtotal1 = set_value('totalvalue');
            $TaxAmount =($totalvalue/(100+$tax)*100) * ($tax / 100);
            $debit = 0;
            $creditor = ($totalvalue - $TaxAmount);
            $this->fin_journal_insert($account_id, $debit, $creditor);

            /******* إلى قيد ضريبة المبيعات ********/
            $account_id = intval($this->session->userdata('acc_tax_sale'));
            $debit = 0;
            $creditor =($totalvalue/(100+$tax)*100) * ($tax / 100);
            $this->fin_journal_insert($account_id, $debit, $creditor);



            if ($totalvalue == $paid) {
                /******* من حساب الخزينة ********/

                if ($paid1 > 0) {
                    $account_id = $tree_id;
                    $debit = $paid1;
                    $creditor = 0;
                    $this->fin_journal_insert($account_id, $debit, $creditor);

                }

                /******* من حساب البنك ********/
                if ($paid2 > 0) {
                    $account_id = $tree_id1;
                    $debit = $paid2;
                    $creditor = 0;
                    $this->fin_journal_insert($account_id, $debit, $creditor);

                }
                /******* من حساب رصيد العميل ********/
                if ($paid3 > 0) {
                    $account_id = $customer_id;
                    $debit = $paid3;
                    $creditor = 0;
                    $this->fin_journal_insert($customer_account_id, $debit, $creditor);

                }

                /******* الى حساب العميل ********/

                $debit = 0;
                $creditor = $totalvalue;
                $this->fin_journal_insert($customer_account_id, $debit, $creditor);

            } else {
                if ($paid > 0) {
                    /******* من حساب الخزينة ********/
                    $_totalPaid=0;
                    if ($paid1 > 0) {
                        $account_id = $tree_id;
                        $debit = $paid1;
                        $creditor = 0;
                        $_totalPaid+=$paid1;
                        $this->fin_journal_insert($account_id, $debit, $creditor);
                    }
                    /******* من حساب البنك ********/
                    if ($paid2 > 0) {
                        $account_id = $tree_id1;
                        $debit = $paid2;
                        $creditor = 0;
                        $_totalPaid+=$paid2;
                        $this->fin_journal_insert($account_id, $debit, $creditor);
                    }
                    /******* من حساب رصيد العميل ********/
                    if ($paid3 > 0) {
                        $debit = $paid3;
                        $creditor = 0;
                        $_totalPaid+=$paid3;
                        $this->fin_journal_insert($customer_account_id, $debit, $creditor);
                    }
                    /******* الى حساب العميل ********/
                    $debit = 0;
                    $creditor = $_totalPaid;
                    $this->fin_journal_insert($customer_account_id, $debit, $creditor);

                }
            }

            /************end************/

            // حساب عمولة مندوب المبيعات
            if ($salesperson_id > 0) {
                $SalesPersonData = $this->M_fin_treeaccount->GetRow($salesperson_id);
                $Commission = $SalesPersonData->commission;
                $CommissionValue = $totalvalue * $Commission;
                $account_id = $salesperson_id;
                $debit = 0;
                $creditor = $CommissionValue;
                $details = $notes;
                $table_name = "sale_bill";
                $bond_no = $bill_id;
                $image = $NewFileName;
                $this->fin_journal_insert($account_id, $debit, $creditor);


             }



           
            ///////////////////////////
            //تحديث كميات الأصناف بالمخزن
            $InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
            //M_product
            $barcode = "barcode";
            $location_id = "location_id";
            $quantity = "quantity";
            $received = "received";
            $MessageBody = "";
            for ($i = 1; $i <= $this->input->post('RowsCount'); $i++) {
                $quantity = "quantity" . $i;
                $received = "received" . $i;
                $quantity = $this->input->post($quantity);
                $received = $this->input->post($received);
                if ($received > 0) {
                    $barcode = "barcode" . $i;
                    //echo $barcode;
                    $barcode = $this->input->post($barcode);
                    $ProductData = $this->M_product->GetByBarcode($barcode);
                    //var_dump($ProductData);
                    $product_id = $ProductData->id;
                    $location_id = "location_id" . $i;
                    $location_id = $this->input->post($location_id);
                    $unitprice = "unitprice" . $i;
                    $unitprice = $this->input->post($unitprice);
                    $Old_FromStoreData = $this->M_store_quantity->GetStoreProduct($location_id, $product_id);
                    $Old_FromStore_Quantity = $Old_FromStoreData->quantity;
                    $Old_FromStore_id = $Old_FromStoreData->id;
                    $New_FromStore_Quantity = $Old_FromStore_Quantity - $received;
                    $FromStoreArrayData = array();
                    $FromStoreArrayData = array(
                        'store_id' => $location_id,
                        'product_id' => $product_id,
                        'quantity' => $New_FromStore_Quantity,
                    );
                    $this->M_store_quantity->UpdateRecord($Old_FromStore_id, $FromStoreArrayData);
                    $POS_Quantity_Data = array();
                    $POS_Quantity_Data = array(
                        'product_id' => $product_id,
                        'store_id' => $location_id,
                        'quantity' => $New_FromStore_Quantity,
                    );
                    $this->M_Tec_product_store_qty->UpdateRecord($Old_FromStore_id, $POS_Quantity_Data);
                    //M_sale_bill_items
                    $ItemsArray = array();
                    $ItemsArray = array(
                        'bill_id' => $bill_id,
                        'store_type_id' => $product_id,
                        'location_id' => $location_id,
                        'quantity' => $quantity,
                        'unitprice' => $unitprice,
                        'received' => $received,
                    );
                    $this->M_sale_bill_items->InsertRecord($ItemsArray);
                    $MessageBody .= "الباركود: " . $barcode . " - اسم الصنف: " . $ProductData->title . " - الكمية: " . $quantity . "<br><hr>";
                }
                if ($storekeeper > 0) {
                    $readed = 0;
                    $marked = 0;
                    $deleted = 0;
                    $thetime = date("h:i:s");
                    $draft = 0;
                    $NewData = array(
                        'company_id' => $company_id,
                        'from_user' => $user_id,
                        'to_user' => $storekeeper,
                        'subject' => "تجهيز طلبية",
                        'message' => $MessageBody,
                        'image' => "",
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                        'readed' => $readed,
                        'marked' => $marked,
                        'draft' => $draft,
                        'deleted' => $deleted,
                    );
                    $this->M_serv_message->InsertRecord($NewData);
                }
            }
            redirect('sales/sale_bill');
        }
    }
    public function updateform($rid)
    {
        $DataRow = $this->M_sale_bill->GetRow($rid);
        $data['id'] = $DataRow->id;
        $data['country_id'] = $DataRow->country_id;
        $data['area'] = $DataRow->area;
        $data['title'] = $DataRow->title;
        $data['email'] = $DataRow->email;
        $data['phone'] = $DataRow->phone;
        $data['deleted'] = $DataRow->deleted;
        $data['ControllerName'] = $this->router->fetch_class();
        $data['content_page'] = "sales/sale_bill_update";
        $this->load->view('page', $data);
    }
    public function Update_Data()
    {
        $this->form_validation->set_rules("title", "title", "required");
        $this->form_validation->set_rules("country_id", "country_id", "required");
        if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
        } else {
            $id = set_value('id');
            $country_id = set_value("country_id");
            $area = set_value("area");
            $title = set_value('title');
            $email = set_value('email');
            $phone = set_value('phone');
            $deleted = set_value('deleted');
            $ChangeImage = set_value('ChangeImage');
            $NewData = array();
            if ($ChangeImage == "True") {
                $filename = date("Y") . "-" . date("m") . "-" . date("d") . "-" . date("h") . "-" . date("i") . "-" . date("s");
                $exts = explode(".", $_FILES['image']['name']);
                $exts = end($exts);
                $exts = strtolower($exts);
                $UploadPath = "./upload/sale_bill/";
                $ImageDirectory = $UploadPath;
                if (!is_dir($ImageDirectory)) {
                    mkdir($ImageDirectory, 0777, TRUE);
                }
                $config['upload_path'] = $ImageDirectory;
                $config['allowed_types'] = "*";
                $config['file_name'] = $filename . "." . $exts;
                $config['overwrite'] = TRUE;
                $config['file_ext_tolower'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $NewFileName = $filename . "." . $exts;
                if (!$this->upload->do_upload('image', $NewFileName)) {
                    $error = array('error' => $this->upload->display_errors());
                    $NewFileName = "";
                } else {
                    $this->upload->do_upload('image', $NewFileName);
                }
                $NewData = array(
                    'country_id' => $country_id,
                    'area' => $area,
                    'title' => $title,
                    'email' => $email,
                    'phone' => $phone,
                    'image' => $NewFileName,
                    'deleted' => $deleted,
                );
            } else {
                $NewData = array(
                    'country_id' => $country_id,
                    'area' => $area,
                    'title' => $title,
                    'email' => $email,
                    'phone' => $phone,
                    'deleted' => $deleted,
                );
            }
            $this->M_sale_bill->UpdateRecord($id, $NewData);
            redirect('sale_bill');
        }
    }
    public function delete($rid)
    {
        $deleted = 1;
        $NewData = array();
        $NewData = array(
            'deleted' => $deleted,
        );
        $this->M_sale_bill->UpdateRecord($rid, $NewData);
        redirect("sale_bill");
    }
    public function undelete($rid)
    {
        $deleted = 0;
        $NewData = array();
        $NewData = array(
            'deleted' => $deleted,
        );
        $this->M_sale_bill->UpdateRecord($rid, $NewData);
        redirect("sale_bill");
    }
    public function FillProductsList($barcode)
    {
        $CodeCount = $this->M_product->CheckBarcode($barcode);
        if ($CodeCount > 0) {
            $TypeName = $this->M_product->GetByBarcode($barcode);
            echo $TypeName->title;
        } else {
            echo "-";
        }
    }
    public function FillProductsPrice($barcode)
    {
        $CodeCount = $this->M_product->CheckBarcode($barcode);
        if ($CodeCount > 0) {
            $TypeName = $this->M_product->GetByBarcode($barcode);
            echo $TypeName->price;
        } else {
            echo "0";
        }
    }
    public function FillProductsMinPrice($barcode)
    {
        $CodeCount = $this->M_product->CheckBarcode($barcode);
        if ($CodeCount > 0) {
            $TypeName = $this->M_product->GetByBarcode($barcode);
            echo $TypeName->lowest_price;
        } else {
            echo "0";
        }
    }
    public function fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $image)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $user_id = intval($this->session->userdata('StaffID'));
        $automatic = 1;
        $deleted = 0;
        $fin_journal_array = array();
        $fin_journal_array = array(
            'company_id' => $company_id,
            'user_id' => $user_id,
            'details' => $details,
            'thedate' => $thedate,
            'bill_id' => $bill_id,
            'table_name' => $table_name,
            'bond_no' => $bond_no,
            'image' => $image,
            'automatic' => $automatic,
            'deleted' => $deleted,
        );
        $this->M_fin_journal_main->InsertRecord($fin_journal_array);
    }
    public function fin_journal_insert($account_id, $debit, $creditor)
    {
        $company_id = intval($this->session->userdata('company_id'));
        $user_id = intval($this->session->userdata('StaffID'));
        $MainData = $this->M_fin_journal_main->GetLatestRecord($company_id, $user_id);
        $main_id = $MainData->id;
        $fin_journal_array = array();
        $fin_journal_array = array(
            'main_id' => $main_id,
            'account_id' => $account_id,
            'debit' => $debit,
            'creditor' => $creditor,
        );
        $this->M_fin_journal->InsertRecord($fin_journal_array);
    }
    public function view_main()
    {
        $id = $this->input->get('id');
        $MainDataRow = $this->M_sale_bill->GetInvoiceHeader($id);
        $CustomerData = $this->M_sale_customer->GetRow($MainDataRow->customer_id);
        $AccountData = $this->M_fin_treeaccount->GetRow($MainDataRow->customer_id);
        $MainDataRow->customer_title = $CustomerData->ar_title;
        $MainDataRow->customer_title_en = $CustomerData->en_title;
        $MainDataRow->account_title = $AccountData->title;
        $MainDataRow->account_title_en = $AccountData->en_title;
        $MainDataRow->remaining = $MainDataRow->totalvalue - $MainDataRow->paid;
        echo json_encode($MainDataRow);
        exit();
    }
    public function view_details()
    {
        $id = $this->input->get('id');
        //$id = intval($this->session->userdata('Fin_journal_View_ID'));
        $DetailsRows = $this->M_sale_bill->GetInvoiceDetails($id);
        foreach ($DetailsRows as $row) {
          $ProductData = $this->M_product->GetRow($row->store_type_id);
           $row->title = $ProductData->title;
           $row->title_en = $ProductData->title_en;
           $row->unitprice = $ProductData->price;
           $row->subtotal = $ProductData->price * $row->quantity ;
        }
        echo json_encode($DetailsRows);
        exit();
    }
    public function deleted()
    {
        $this->session->set_userdata('Filter', lang('Deleted'));
        $data['DataRows'] = $this->M_sale_bill->GetDeleted();
        $data['content_page'] = "sales/sale_bill";
        $this->load->view('page', $data);
    }
    public function payment($payment = 1)
    {
        if ($payment == 1) {
            $this->session->set_userdata('Filter', lang('buy_bill_full_paid'));
            $data['DataRows'] = $this->M_sale_bill->GetFullPaid();
        } else if ($payment == 2) {
            $this->session->set_userdata('Filter', lang('buy_bill_part_paid'));
            $data['DataRows'] = $this->M_sale_bill->GetPartPaid();
        } else {
            $this->session->set_userdata('Filter', lang('buy_bill_not_paid'));
            $data['DataRows'] = $this->M_sale_bill->GetNotPaid();
        }
        $data['content_page'] = "sales/sale_bill";
        $this->load->view('page', $data);
    }
    public function delivered($payment = 4)
    {
        $this->session->set_userdata('Filter', lang('Quantities_not_fully_delivered'));
        $data['DataRows'] = $this->M_sale_bill->GetNotDelivered();
        $data['content_page'] = "sales/sale_bill_delivered";
        $this->load->view('page', $data);
    }
    public function close_invoice()
    {
        $this->session->set_userdata('Filter', "");
        $company_id = intval($this->session->userdata('company_id'));
        $StaffID = intval($this->session->userdata('StaffID'));
        $this->form_validation->set_rules("id", "id", "required");
        $this->form_validation->set_rules("tree_id", "tree_id", "required");
        if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
        } else {
            $id = set_value('id');
            $paid_value = set_value('paid_value');
            $tree_id = set_value('tree_id');
            $Bill_Record = $this->M_sale_bill->GetRow($id);
            $totalvalue = $Bill_Record->totalvalue;
            $paid = $Bill_Record->paid;
            $remaining = $Bill_Record->remaining;
            $customer_id = $Bill_Record->customer_id;
            $thedate = date("Y-m-d");
            $user_id = $StaffID;
            $New_Paid = $paid_value + $paid;
            $New_Remaining = $totalvalue - $New_Paid;
            $NewData = array();
            $NewData = array(
                'paid' => $New_Paid,
                'remaining' => $New_Remaining,
            );
            $this->M_sale_bill->UpdateRecord($id, $NewData);
            $filename = date("Y") . "-" . date("m") . "-" . date("d") . "-" . date("h") . "-" . date("i") . "-" . date("s");
            $exts = explode(".", $_FILES['image']['name']);
            $exts = end($exts);
            $exts = strtolower($exts);
            $UploadPath = "./upload/sale_bill/";
            $ImageDirectory = $UploadPath;
            if (!is_dir($ImageDirectory)) {
                mkdir($ImageDirectory, 0777, TRUE);
            }
            $config['upload_path'] = $ImageDirectory;
            $config['allowed_types'] = "*";
            $config['file_name'] = $filename . "." . $exts;
            $config['overwrite'] = TRUE;
            $config['file_ext_tolower'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $NewFileName = $filename . "." . $exts;
            if (!$this->upload->do_upload('image', $NewFileName)) {
                $error = array('error' => $this->upload->display_errors());
                $NewFileName = "";
            } else {
                $this->upload->do_upload('image', $NewFileName);
            }
            $bill_id = $id;
            //Insert fin_journal
            $details = $this->lang->line('sale_bill_pay');
            $table_name = "sale_bill";
            $bond_no = $bill_id;
            $this->fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $NewFileName);
            // من حساب الخزينة أو البنك
            $account_id = $tree_id;
            $debit = $paid_value;
            $creditor = 0;
            $this->fin_journal_insert($account_id, $debit, $creditor);
            //إلى حساب العميل
            $account_id = $customer_id;
            $debit = 0;
            $creditor = $paid_value;
            $this->fin_journal_insert($account_id, $debit, $creditor);
            redirect('sales/sale_bill');
        }
    }
    public function view_pdf($invoice_id)
    {
        $Alignment = "";
        $Direction = "";
        if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar")) {
            $this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
            $this->session->set_userdata('Alignment', "right");
            $this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
            $Alignment = "right";
            $Direction = "rtl";
        } else {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
            $this->session->set_userdata('Alignment', "left");
            $this->session->set_userdata('HTML', "lang='en'");
            $Alignment = "left";
            $Direction = "ltr";
        }
        $this->load->model("settings/M_fin_settings");
        $this->load->model("settings/M_tec_settings");
        $company_id = intval($this->session->userdata('company_id'));
        $DataRow = $this->M_app_company->GetRow($company_id);
        $tax_date = $DataRow->start_date;
        $HeaderImage = base_url() . "upload/settings/" . $DataRow->header;
        $FooterImage = base_url() . "upload/settings/" . $DataRow->footer;
        $Directionality = $this->session->userdata('Direction');
        $PageTitle = lang('pdf_sale_bill');
        $PageSize = $this->session->userdata('sale_bill_size');
        $PageOrientation = $this->session->userdata('sale_bill_orientation');
        $PageFormat = $PageSize . "-" . $PageOrientation;
        require_once 'vendor/autoload.php';
        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => $PageFormat,
            //'margin_left' => 5,
            //'margin_right' => 5,
            'margin_top' => 25,
            'margin_bottom' => 25,
            'margin_header' => 5,
            'margin_footer' => 5,
        ]);
        $mpdf->allow_charset_conversion = true;
        $mpdf->SetTitle($PageTitle);
        $mpdf->SetDirectionality($Directionality);
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;
        $mpdf->debug = true;
        ob_end_clean();
        header("Content-Encoding: None", true);
        $mpdf->SetHTMLHeader('
 		<div style="text-align: center; font-weight: bold;">
 			<img src="' . $HeaderImage . '">
 		</div>');
        $mpdf->SetHTMLFooter('
 		<table width="100%" style="vertical-align: bottom; font-family: serif;
 			font-size: 8pt; color: #000000; font-weight: bold;">
 			<tr style="border:none!important;">
 				<td colspan="2" style="border:none!important;"><img src="' . $FooterImage . '"></td>
 			</tr>
 			<tr style="border:none!important;">
 				<td width="50%" style="border:none!important;">{DATE j-m-Y}</td>
 				<td width="50%" style="border:none!important;" align="left" lang="en">{PAGENO}/{nbpg}</td>
 			</tr>
 		</table>');
        $DataRow = $this->M_sale_bill->GetRow($invoice_id);
        $data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
        $company_id = $DataRow->company_id;
        $tec_settings = $this->M_tec_settings->GetRow();
        $customer_name = $DataRow->customer_name;
        $customer_phone = $DataRow->customer_phone;
        $CompanyData = $this->M_app_company->GetRow($company_id);
        $tax_number = $tec_settings->tax_number;
        $Invoice_Start = $tec_settings->tax_date;
        $customer_id = $DataRow->customer_id;
        $CustomerData = $this->M_sale_customer->GetRow($customer_id);
        if ($customer_name == "") {
            if ($this->session->userdata('lang') == "ar") {
                $CustomerName = $CustomerData->ar_title;
            } else {
                $CustomerName = $CustomerData->en_title;
            }
        } else {
            $CustomerName = $customer_name;
        }
        if ($customer_phone == "") {
            $CustomerPhone = $CustomerData->phone;
        } else {
            $CustomerPhone = $customer_phone;
        }
        $user_id = $DataRow->user_id;
        $UserData = $this->M_usr_users->GetRow($user_id);
        $InvoiceEditor = $UserData->fullname;
        $sale_start = $UserData->sale_start;
        $branch = $UserData->branch;
        if ($branch != "") {
            $branch = " (" . $branch . ")";
        }
        if ($sale_start != "") {
            $Invoice_Start = $UserData->sale_start;
        }
        $delivery_id = $DataRow->delivery_id;
        $thedate = $DataRow->thedate;
        $MultiRows = $this->M_sale_bill_items->GetMultiRow($invoice_id);
        $html = '<style>td, th{border:solid 1px #dddddd;}</style>';
        $html .= '<div align="center" style="font-size:18px">';
        $html .= '<strong>' . $PageTitle . $branch . '</strong><br>';
        //$html .= $PageTitle.'<br>('.$invoice_id.')-'.$DataRow->thedate."<br>";
        $html .= '</div><br>';
        $html .= '<table cellspacing="0" cellpadding="5" border="0" width="98%">';
        $html .= '<tbody>';
        $html .= '<tr style="border:none !important;vertical-align: baseline !important;">';
        $html .= '<td colspan="1" style="padding-right:0 !important;border:none !important;">';
        $html .= '<table cellspacing="0" cellpadding="5" width="100%">';
        $html .= '<thead>';
        $html .= '<tr style="background-color:#EBEBEB; font-weight:bold;border:none !important;">';
        $html .= '<th colspan="3">بيانات</th>';
        $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<tbody>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('Site Name') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tec_settings->site_name . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('phone') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tec_settings->tel . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('address') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tec_settings->address . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('buy_manufacturer_tax_card') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tax_number . '</td>';
        $html .= '</tr>';
        // $html .= '<tr style="border:none !important;">';
        // $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('buy_manufacturer_tax_card_date') . '</td>';
        // $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tax_number . '</td>';
        // $html .= '</tr>';
        // $html .= '<tr style="border:none !important;">';
        // $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('buy_manufacturer_tax_card_date') . '</td>';
        // $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tax_date . '</td>';
        // $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</td>';
        //Customer
        $html .= '<td colspan="1" style="padding-left:0;border:none !important;">';
        $html .= '<table cellspacing="0" cellpadding="5" width="100%">';
        $html .= '<thead>';
        $html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
        $html .= '<th colspan="3">بيانات</th>';
        $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<tbody>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('sale_fullname') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $CustomerName . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('phone') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $CustomerPhone . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('address') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $CustomerData->address . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('tax_card') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $CustomerData->tax_card . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('data_account_invoice_id') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $invoice_id . '</td>'; //$Invoice_Start . "  -  " .
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('thedate') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $DataRow->thedate . '</td>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '<table cellspacing="0" cellpadding="5" width="98%">';
        $html .= '<tbody>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('sale_bill_totalvalue') . ' :</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;"><b>' . number_format($DataRow->totalvalue, 2) . '</b></td>';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('pdf_Invoice_Editor') . ' :</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;"><b>' . $InvoiceEditor . '</b></td>';
        $html .= '</tr>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</div><br>';
        $html .= '<table cellspacing="0" cellpadding="5" width="98%" style="border: solid 1px #dddddd;">';
        $html .= '<thead>';
        $html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('Serial') . '</th>';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('product_barcode') . '</th>';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('sale_bill_items_store_type_id') . '</th>';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('sale_bill_items_quantity') . '</th>';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('sale_bill_items_unitprice') . '</th>';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('subtotal') . '</th>';
        $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<tbody>';
        $Serial = 0;
        $TotalValue = 0;
        $AllQuantity = 0;
        $FinalBigQ = 0;
        $FinalSmallQ = 0;
        foreach ($MultiRows as $MultiRows_Row) {
            $Serial += 1;
            $type_id = $MultiRows_Row->store_type_id;
            $ProductData = $this->M_product->GetRow($type_id);
            if ($this->session->userdata('lang') == "ar") {
                $ProductName = $ProductData->title;
            } else {
                $ProductName = $ProductData->title_en;
            }
            $location_id = $MultiRows_Row->location_id;
            $StoreData = $this->M_store->GetRow($location_id);
            if ($this->session->userdata('lang') == "ar") {
                $StoreName = $StoreData->title;
            } else {
                $StoreName = $StoreData->title_en;
            }
            $html .= '<tr>';
            $html .= '<td style="background-color:#EBEBEB; font-weight:bold;">' . $Serial . '</td>';
            $html .= '<td>' . $ProductData->barcode . '</td>';
            $html .= '<td>' . $ProductName . '</td>';
            $html .= '<td align="center">' . $MultiRows_Row->quantity . '</td>';
            $AllQuantity += $MultiRows_Row->quantity;
            $html .= '<td align="center">' . number_format($MultiRows_Row->unitprice, 2) . '</td>';
            $SubTotal = doubleval($MultiRows_Row->quantity) * doubleval($MultiRows_Row->unitprice);
            $html .= '<td align="center">' . number_format($MultiRows_Row->quantity * $MultiRows_Row->unitprice, 2) . '</td>';
            $html .= '</tr>';
            $TotalValue += $SubTotal;
        }
        $tax = $DataRow->tax;
        $taxValue = ($TotalValue - $DataRow->discount) * ($tax / 100);
        $totalPrice = doubleval($TotalValue + $taxValue) - doubleval($DataRow->discount);
        $Items = $this->lang->line('invoice_items') . ' : ' . $Serial;
        $html .= '<tr>';
        $html .= '<td colspan="4" style="border:none!important">' . $Items . '</td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_totalitemsvalue') . '</td>';
        $html .= '<td align="center">' . number_format($TotalValue, 2) . '</td>';
        $html .= '</tr>';
        $QuantityText = $this->lang->line('sale_bill_items_quantity') . ' : ' . $AllQuantity;
        $html .= '<tr>';
        $html .= '<td colspan="4" style="border:none!important">' . $QuantityText . '</td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_discount') . '</td>';
        $html .= '<td align="center">' . number_format($DataRow->discount, 2) . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="4" style="border:none!important">';
        if ($this->session->userdata('lang') == "ar") {
            $invoice_text = $this->session->userdata('sale_bill_ar_text');
        } else {
            $invoice_text = $this->session->userdata('sale_bill_en_text');
        }
        $html .= $invoice_text;
        $html .= '</td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_total_after_discount') . '</td>';
        $html .= '<td align="center">' . number_format($TotalValue - $DataRow->discount, 2) . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="4" style="border:none!important"></td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_required_price') . '</td>';
        $html .= '<td align="center">' . number_format($taxValue, 2) . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="4" style="border:none!important"></td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_totalvalue') . '</td>';
        $html .= '<td align="center">' . number_format($DataRow->totalvalue, 2) . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="4" style="border:none!important"></td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_total_paid') . '</td>';
        $html .= '<td align="center">' . number_format($DataRow->paid, 2) . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="4" style="border:none!important">';
        $ArabicMoney = $this->makeNumber2Text($DataRow->paid);
        $html .= $ArabicMoney . ' ' . $this->lang->line('JustDoNot');
        $html .= '</td>';
        if($DataRow->remaining >0){
        $html .= '<td align="center">' . $this->lang->line('sale_bill_remaining') . '</td>';
        $html .= '<td align="center">' . number_format($DataRow->remaining, 2) . '</td>';

        }
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '<br>';
        $html .= '<table cellspacing="0" cellpadding="5" border="0" width="98%" align="center">';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;">' . $this->lang->line('IReceived') . '</td>';
        $html .= '<td style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;">' . $InvoiceEditor . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="center" style="border:none!important;">QR Code</td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;">' . $this->lang->line('ReceviesSign') . '</td>';
                     $html .= '<td align="center" rowspan="6" style="border:none!important;">';
        $this->load->library('QR');
        $html .= QR::get_base($tec_settings->site_name,$tax_number,$DataRow->thedate, $DataRow->totalvalue,$taxValue);
        $html .= '</td>';
        // $html .= '<td align="center" rowspan="6" style="border:none!important;"><img height="160" src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=' . $this->lang->line('Site Name') . ' : ' . $tec_settings->site_name . '%0A' . $this->lang->line('buy_manufacturer_tax_card') . ' : ' . $tax_number . '%0A' . $this->lang->line('buy_offer_tax') . ' : ' . $taxValue . '%0A' . $this->lang->line('sale_bill_totalvalue') . ' : ' . $DataRow->totalvalue . '&choe=UTF-8" ></td>';
        $html .= '<td align="left" style="border:none!important;">' . $this->lang->line('SalesmanSign') . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;">..........................................</td>';
        $html .= '<td align="left" style="border:none!important;">..........................................</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        if (isset($_GET['test'])) {
            echo $html;
            exit;
        }
        $html = iconv("utf-8", "UTF-8//IGNORE", $html);
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
    }
    public function view_pdf_store_column($invoice_id)
    {
        if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar")) {
            $this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
            $this->session->set_userdata('Alignment', "right");
            $this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
        } else {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
            $this->session->set_userdata('Alignment', "left");
            $this->session->set_userdata('HTML', "lang='en'");
        }
        $this->load->model("settings/M_fin_settings");
        $DataRow = $this->M_fin_settings->GetRow();
        $HeaderImage = base_url() . "upload/settings/" . $DataRow->header;
        $FooterImage = base_url() . "upload/settings/" . $DataRow->footer;
        $Directionality = $this->session->userdata('Direction');
        $PageTitle = lang('pdf_sale_bill');
        require_once 'vendor/autoload.php';
        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            //'format' => 'A4'.($orientation == 'L' ? '-L' : ''),
            //'orientation' => $orientation,
            'margin_left' => 5,
            'margin_right' => 5,
            'margin_top' => 35,
            'margin_bottom' => 45,
            'margin_header' => 5,
            'margin_footer' => 5,
        ]);
        $mpdf->allow_charset_conversion = true;
        $mpdf->SetTitle($PageTitle);
        $mpdf->SetDirectionality($Directionality);
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;
        $mpdf->debug = true;
        ob_end_clean();
        header("Content-Encoding: None", true);
        $mpdf->SetHTMLHeader('
 		<div style="text-align: center; font-weight: bold;">
 			<img src="' . $HeaderImage . '">
 		</div>');
        $mpdf->SetHTMLFooter('
 		<table width="100%" style="vertical-align: bottom; font-family: serif;
 			font-size: 8pt; color: #000000; font-weight: bold;">
 			<tr" style="border:none!important;">
 				<td colspan="2" style="border:none!important;"><img src="' . $FooterImage . '"></td>
 			</tr>
 			<tr "style="border:none!important;">
 				<td width="50%" style="border:none!important;">{DATE j-m-Y}</td>
 				<td width="50%" style="border:none!important;" align="left" lang="en">{PAGENO}/{nbpg}</td>
 			</tr>
 		</table>');
        $DataRow = $this->M_sale_bill->GetRow($invoice_id);
        $data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
        $customer_id = $DataRow->customer_id;
        $CustomerData = $this->M_sale_customer->GetRow($customer_id);
        if ($this->session->userdata('lang') == "ar") {
            $CustomerName = $CustomerData->ar_title;
        } else {
            $CustomerName = $CustomerData->en_title;
        }
        $user_id = $DataRow->user_id;
        $UserData = $this->M_usr_users->GetRow($user_id);
        $InvoiceEditor = $UserData->fullname;
        $delivery_id = $DataRow->delivery_id;
        $thedate = $DataRow->thedate;
        $MultiRows = $this->M_sale_bill_items->GetMultiRow($invoice_id);
        $html = '';
        $html .= '<div align="center" style="font-size:18px"><strong>';
        $html .= $PageTitle . '<br>(' . $invoice_id . ')-' . $DataRow->thedate . "<br>";
        $html .= '</strong></div><br>';
        $html .= '<table cellspacing="0" cellpadding="5" border="0" width="98%">';
        $html .= '<tr>';
        $html .= '<td>' . $this->lang->line('sale_bill_customer_id') . '</td>';
        $html .= '<td>' . $CustomerName . '</td>';
        $html .= '<td>' . $this->lang->line('pdf_Invoice_Editor') . '</td>';
        $html .= '<td>' . $InvoiceEditor . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>' . $this->lang->line('thedate') . '</td>';
        $html .= '<td>' . $DataRow->thedate . '</td>';
        $html .= '<td>' . $this->lang->line('buy_offer_tax') . '</td>';
        $html .= '<td>' . $DataRow->tax . '%</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>' . $this->lang->line('sale_bill_totalvalue') . '</td>';
        $html .= '<td>' . $DataRow->totalvalue . '</td>';
        $html .= '<td>' . $this->lang->line('sale_bill_paid') . '</td>';
        $html .= '<td>' . $DataRow->paid . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>' . $this->lang->line('sale_bill_remaining') . '</td>';
        $html .= '<td>' . $DataRow->remaining . '</td>';
        $html .= '<td>' . $this->lang->line('sale_bill_discount') . '</td>';
        $html .= '<td>' . $DataRow->discount . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="4">' . $DataRow->notes . '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<br>';
        $html .= '<table cellspacing="0" cellpadding="5" border="1" width="98%">';
        $html .= '<thead>';
        $html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
        $html .= '<th>' . $this->lang->line('Serial') . '</th>';
        $html .= '<th>' . $this->lang->line('product_barcode') . '</th>';
        $html .= '<th>' . $this->lang->line('sale_bill_items_store_type_id') . '</th>';
        $html .= '<th>' . $this->lang->line('sale_bill_items_location_id') . '</th>';
        $html .= '<th>' . $this->lang->line('sale_bill_items_quantity') . '</th>';
        $html .= '<th>' . $this->lang->line('sale_bill_items_unitprice') . '</th>';
        $html .= '<th>' . $this->lang->line('subtotal') . '</th>';
        $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<tbody>';
        $Serial = 0;
        $TotalValue = 0;
        $AllQuantity = 0;
        $FinalBigQ = 0;
        $FinalSmallQ = 0;
        foreach ($MultiRows as $MultiRows_Row) {
            $Serial += 1;
            $type_id = $MultiRows_Row->store_type_id;
            $ProductData = $this->M_product->GetRow($type_id);
            if ($this->session->userdata('lang') == "ar") {
                $ProductName = $ProductData->title;
            } else {
                $ProductName = $ProductData->title_en;
            }
            $location_id = $MultiRows_Row->location_id;
            $StoreData = $this->M_store->GetRow($location_id);
            if ($this->session->userdata('lang') == "ar") {
                $StoreName = $StoreData->title;
            } else {
                $StoreName = $StoreData->title_en;
            }
            $html .= '<tr>';
            $html .= '<td style="background-color:#EBEBEB; font-weight:bold;">' . $Serial . '</td>';
            $html .= '<td>' . $ProductData->barcode . '</td>';
            $html .= '<td>' . $ProductName . '</td>';
            $html .= '<td align="center">' . $StoreName . '</td>';
            $html .= '<td align="center">' . $MultiRows_Row->quantity . '</td>';
            $AllQuantity += $MultiRows_Row->quantity;
            $html .= '<td align="center">' . $MultiRows_Row->unitprice . '</td>';
            $SubTotal = doubleval($MultiRows_Row->quantity) * doubleval($MultiRows_Row->unitprice);
            $html .= '<td align="center">' . $SubTotal . '</td>';
            $html .= '</tr>';
            $TotalValue += $SubTotal;
        }
        $html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
        $html .= '<td></td>';
        $html .= '<td colspan="5">' . $this->lang->line('sale_bill_totalvalue') . '</td>';
        $html .= '<td align="center">' . $DataRow->totalvalue . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $Items = $this->lang->line('invoice_items') . ' : ' . $Serial;
        $html .= '<td colspan="3" style="border:none !important;">' . $Items . '</td>';
        $html .= '<td colspan="2" style="border:none !important;"></td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_discount') . '</td>';
        $html .= '<td align="center">' . $DataRow->discount . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $QuantityText = $this->lang->line('sale_bill_items_quantity') . ' : ' . $AllQuantity;
        $html .= '<td colspan="3" style="border:none !important;">' . $QuantityText . '</td>';
        $html .= '<td colspan="2" style="border:none !important;"></td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_totalvalue') . '</td>';
        $html .= '<td align="center">' . $DataRow->totalvalue . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="5" style="border:none !important;">';
        //$html .= $this->session->userdata('invoice_text');
        $html .= '</td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_paid') . '</td>';
        $html .= '<td align="center">' . $DataRow->paid . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="5" style="border:none !important;">';
        $ArabicMoney = $this->makeNumber2Text($DataRow->totalvalue);
        $html .= $ArabicMoney . ' ' . $this->lang->line('JustDoNot');
        $html .= '</td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_remaining') . '</td>';
        $html .= '<td align="center">' . $DataRow->remaining . '</td>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '<br>';
        $html .= '<table cellspacing="0" cellpadding="5" border="0" width="98%" align="center">';
        $html .= '<tr>';
        $html .= '<td align="right">' . $this->lang->line('IReceived') . '</td>';
        $html .= '<td align="center">' . $this->lang->line('driver') . '</td>';
        $html .= '<td align="left">' . $InvoiceEditor . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right">' . $this->lang->line('ReceviesSign') . '</td>';
        $html .= '<td align="center">' . $this->lang->line('DriverSign') . '</td>';
        $html .= '<td align="left">' . $this->lang->line('SalesmanSign') . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right">..........................................</td>';
        $html .= '<td align="center">..........................................</td>';
        $html .= '<td align="left">..........................................</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html = iconv("utf-8", "UTF-8//IGNORE", $html);
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
    }
    public function makeNumber2Text($numberValue)
    {
        //$this->cache->clean();
        ini_set("memory_limit", "512M");
        $textResult = ''; // so i can use .=
        $numberValue = "$numberValue";
        if ($numberValue[0] == '-') {
            $textResult .= 'سالب ';
            $numberValue = substr($numberValue, 1);
        }
        $numberValue = (int)$numberValue;
        $def = array(
            "0" => 'صفر',
            "1" => 'واحد',
            "2" => 'اثنان',
            "3" => 'ثلاث',
            "4" => 'اربع',
            "5" => 'خمس',
            "6" => 'ست',
            "7" => 'سبع',
            "8" => 'ثمان',
            "9" => 'تسع',
            "10" => 'عشر',
            "11" => 'أحد عشر',
            "12" => 'اثنا عشر',
            "100" => 'مائة',
            "200" => 'مئتان',
            "1000" => 'ألف',
            "2000" => 'ألفين',
            "1000000" => 'مليون',
            "2000000" => 'مليونان'
        );
        // check for defind values
        if (isset($def[$numberValue])) {
            // checking for numbers from 2 to 10 :reson = 2 to 10 uses 'ة' at the end
            if ($numberValue < 11 && $numberValue > 2) {
                $textResult .= $def[$numberValue] . 'ة';
            } else {
                // the rest of the defined numbers
                $textResult .= $def[$numberValue];
            }
        } else {
            $tensCheck = $numberValue % 10;
            $numberValue = "$numberValue";
            for ($x = strlen($numberValue); $x > 0; $x--) {
                $places[$x] = $numberValue[strlen($numberValue) - $x];
            }
            switch (count($places)) {
                case 2: // 2 numbers
                case 1: // or 1 number
                    {
                        $textResult .= ($places[1] != 0) ? $def[$places[1]] . (($places[1] > 2 || $places[2] == 1) ? 'ة' : '') . (($places[2] != 1) ? ' و' : ' ') : '';
                        $textResult .= (($places[2] > 2) ? $def[$places[2]] . 'ون' : $def[10] . (($places[2] != 2) ? '' : 'ون'));
                    }
                    break;
                case 3: // 3 numbers
                    {
                        $lastTwo = (int)$places[2] . $places[1];
                        $textResult .= ($places[3] > 2) ? $def[$places[3]] . ' ' . $def[100] : $def[(int)$places[3] . "00"];
                        if ($lastTwo != 0) {
                            $textResult .= ' و' . $this->makeNumber2Text($lastTwo);
                        }
                    }
                    break;
                case 4: // 4 numbrs
                    {
                        $lastThree = (int)$places[3] . $places[2] . $places[1];
                        $textResult .= ($places[4] > 2) ? $def[$places[4]] . 'ة الاف' : $def[(int)$places[4] . "000"];
                        if ($lastThree != 0) {
                            $textResult .= ' و' . $this->makeNumber2Text($lastThree);
                        }
                    }
                    break;
                case 5: // 5 numbers
                    {
                        $lastThree = (int)$places[3] . $places[2] . $places[1];
                        $textResult .= $this->makeNumber2Text((int)$places[5] . $places[4]) . ((((int)$places[5] . $places[4]) != 10) ? ' الفاً' : ' الاف');
                        if ($lastThree != 0) {
                            $textResult .= ' و' . $this->makeNumber2Text($lastThree);
                        }
                    }
                    break;
                case 6: // 6 numbers
                    {
                        $lastThree = (int)$places[3] . $places[2] . $places[1];
                        $textResult .= $this->makeNumber2Text((int)$places[6] . $places[5] . $places[4]) . ((((int)$places[5] . $places[4]) != 10) ? ' الفاً' : ' الاف');
                        if ($lastThree != 0) {
                            $textResult .= ' و' . $this->makeNumber2Text($lastThree);
                        }
                    }
                    break;
                case 7: // 7 numbers 1 mill
                    {
                        $textResult .= ($places[7] > 2) ? $def[$places[7]] . ' ملايين' : $def[(int)$places[7] . "000000"];
                        $textResult .= ' و';
                        $textResult .= $this->makeNumber2Text((int)$places[6] . $places[5] . $places[4] . $places[3] . $places[2] . $places[1]);
                    }
                    break;
                case 8: // 8 numbers 10 mill
                case 9: // 9 numbers 100 mill
                    {
                        $places[9] = (isset($places[9])) ? $places[9] : '';
                        $firstThree = (int)$places[9] . $places[8] . $places[7];
                        $textResult .= $this->makeNumber2Text($firstThree);
                        $textResult .= ($firstThree < 11) ? ' ملايين ' : ' مليونا ';
                        if (((int)$places[6] . $places[5] . $places[4] . $places[3] . $places[2] . $places[1]) != 0) {
                            $textResult .= ' و';
                            $textResult .= $this->makeNumber2Text((int)$places[6] . $places[5] . $places[4] . $places[3] . $places[2] . $places[1]);
                        }
                    }
                    break;
                default: {
                        $textResult = 'هذا رقم كبير .. ';
                    }
            }
        }
        return $textResult;
    }
    public function LoadStores($Store_id, $barcode)
    {
        $CodeCount = $this->M_product->CheckBarcode($barcode);
        if ($CodeCount > 0) {
            $TypeName = $this->M_product->GetByBarcode($barcode);
            $Product_id = $TypeName->id;
            $CheckAvailable = $this->M_store_quantity->CountFillStores($Store_id, $Product_id);
            if ($CheckAvailable > 0) {
                $DetailsRows = $this->M_store_quantity->FillStores($Store_id, $Product_id);
                echo $DetailsRows->quantity;
            } else {
                echo "0";
            }
        } else {
        }
    }
    public function GetCustomerBalance($customer_id)
    {
        $DebitRow = $this->M_fin_journal->GetSum_Debit($customer_id);
        $DebitValue = $DebitRow->debit;
        $CreditorRow = $this->M_fin_journal->GetSum_Creditor($customer_id);
        $CreditorValue = $CreditorRow->creditor;
        $CustomerBalance = $CreditorValue - $DebitValue;
        if ($CustomerBalance > 0) {
            echo $CustomerBalance;
        } else {
            echo "0";
        }
    }
    public function Delivery($rid)
    {
        $data['DataRows'] = $this->M_sale_bill_items->GetMultiRow($rid);
        $data['rid'] = $rid;
        $data['ControllerName'] = $this->router->fetch_class();
        $data['content_page'] = "sales/sale_bill_delivery";
        $this->load->view('page', $data);
    }
    public function Delivery_Update()
    {
        $this->form_validation->set_rules("deleted", "deleted", "required");
        if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
        } else {
            $barcode = "barcode";
            $location_id = "location_id";
            $quantity = "quantity";
            $received = "received";
            $rid = $this->input->post("rid");
            $DataRows = $this->M_sale_bill_items->GetMultiRow($rid);
            foreach ($DataRows as $DataRows_Row) {
                $i = $DataRows_Row->id;
                $row_id = "row_id" . $i;
                $row_id = $this->input->post($row_id);
                $received = "received" . $i;
                $received = $this->input->post($received);
                if ($received > 0) {
                    $barcode = "barcode" . $i;
                    $barcode = $this->input->post($barcode);
                    $ProductData = $this->M_product->GetByBarcode($barcode);
                    $product_id = $ProductData->id;
                    $location_id = "location_id" . $i;
                    $location_id = $this->input->post($location_id);
                    $Old_FromStoreData = $this->M_store_quantity->GetStoreProduct($location_id, $product_id);
                    $Old_FromStore_Quantity = $Old_FromStoreData->quantity;
                    $Old_FromStore_id = $Old_FromStoreData->id;
                    $New_FromStore_Quantity = $Old_FromStore_Quantity - $received;
                    $FromStoreArrayData = array();
                    $FromStoreArrayData = array(
                        'store_id' => $location_id,
                        'product_id' => $product_id,
                        'quantity' => $New_FromStore_Quantity,
                    );
                    $this->M_store_quantity->UpdateRecord($Old_FromStore_id, $FromStoreArrayData);
                    $POS_Quantity_Data = array();
                    $POS_Quantity_Data = array(
                        'product_id' => $product_id,
                        'store_id' => $location_id,
                        'quantity' => $New_FromStore_Quantity,
                    );
                    $this->M_Tec_product_store_qty->UpdateRecord($Old_FromStore_id, $POS_Quantity_Data);
                    $GetOldReceived = $this->M_sale_bill_items->GetRow($i);
                    $Old_received = $GetOldReceived->received;
                    $New_received = $Old_received + $received;
                    $Received_Data = array();
                    $Received_Data = array(
                        'received' => $New_received,
                    );
                    $this->M_sale_bill_items->UpdateRecord($i, $Received_Data);
                }
            }
        }
        $data['DataRows'] = $this->M_sale_bill->GetToday();
        $data['content_page'] = "sales/sale_bill";
        $this->load->view('page', $data);
    }
    public function pay($customer_id)
    {
        $CustomerData = $this->M_sale_customer->GetRow($customer_id);
        if ($this->session->userdata('lang') == "ar") {
            $CustomerTitle = $CustomerData->ar_title;
        } else {
            $CustomerTitle = $CustomerData->en_title;
        }
        $data['TableTitle'] = "";
        $data['customer_id'] = $customer_id;
        $this->session->set_userdata('Filter', $CustomerTitle);
        $data['DataRows'] = $this->M_sale_bill->GetInvoiceByCustomer($customer_id);
        $data['content_page'] = "sales/sale_bill_pay";
        $this->load->view('page', $data);
    }
}
