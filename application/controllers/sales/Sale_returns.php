<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sale_returns extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		
		$this->load->model("sales/M_sale_customer");
		$this->load->model('sales/M_sale_returns');
		$this->load->model('sales/M_sale_returns_items');

        $this->load->model("account/M_fin_treeaccount");
		$this->load->model("account/M_fin_journal");
		
        $this->load->model("store/M_store");
        $this->load->model("store/M_product");
		$this->load->model("store/M_store_quantity");
		$this->load->model("pos_sys/M_Tec_product_store_qty");
	}  
	
	public function index()
	{
		$data['DataRows'] = $this->M_sale_returns->GetMultiRow();
		$data['content_page'] = "sales/sale_returns";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$this->session->set_userdata('Filter', "");

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "sales/sale_returns_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		$company_id = intval($this->session->userdata('company_id'));
		$MemberID = intval($this->session->userdata('StaffID'));

		$this->form_validation->set_rules("customer_id","customer_id","required");
		$this->form_validation->set_rules("tree_id","tree_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = $company_id;
			$customer_id = set_value("customer_id");
			$user_id = $MemberID;
			$thedate = set_value('thedate');
			$totalvalue = set_value('totalvalue');
			$paid = set_value('paid');
			$remaining = set_value('remaining');
			$discount = set_value('discount');
			$notes = set_value('notes');
			$tree_id = set_value('tree_id');

			$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/sale_returns/";
			$ImageDirectory = $UploadPath;
			
			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
			}

			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'customer_id' => $customer_id,
				'user_id' => $user_id,
				'thedate' => $thedate,
				'totalvalue' => $totalvalue,
				'paid' => $paid,
				'remaining' => $remaining,
				'discount' => $discount,
				'notes' => $notes,
				'image' => $NewFileName,
				'tree_id' => $tree_id,
			);

			$this->M_sale_returns->InsertRecord($NewData);

			$LatestRecord = $this->M_sale_returns->GetLatestRecord($company_id, $customer_id, $user_id);
			$bill_id = $LatestRecord->id;
			
			$details = $notes;
			$table_name = "sale_returns";
			$bond_no = $bill_id;
			

			$this->fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $NewFileName);

			$account_id = intval($this->session->userdata('acc_sale_reurn'));
			$debit = $totalvalue;
			$creditor = 0;
			$details = $notes;
			$table_name = "sale_returns";
			$bond_no = $bill_id;
			
			// من حساب مردودات المبيعات
			$this->fin_journal_insert($account_id, $debit, $creditor);
			// من حساب امانة ضريبة المبيعات
			$account_id =intval($this->session->userdata('acc_tax_sale'));
			$debit = $totalvalue *intval($this->session->userdata('acc_tax_sale_percent')) /100;
			$creditor = 0;
			$this->fin_journal_insert($account_id, $debit, $creditor);
			
			
			
	        // الى حساب العميل
	        $account_id =$customer_id;
	        $debit =0;
			$creditor = $totalvalue+( $totalvalue *intval($this->session->userdata('acc_tax_sale_percent')) /100);
		
			$this->fin_journal_insert($account_id, $debit, $creditor);
			
			
			

	        // من حساب العميل
	        $account_id =$customer_id;
			$debit =$totalvalue+( $totalvalue *intval($this->session->userdata('acc_tax_sale_percent')) /100);
			$creditor = 0;
			$this->fin_journal_insert($account_id, $debit, $creditor);
			
			// إلى حساب الخزينة أو البنك
				$account_id = $tree_id;
				$debit = 0;
				$creditor = $totalvalue+( $totalvalue *intval($this->session->userdata('acc_tax_sale_percent')) /100);;
				$this->fin_journal_insert($account_id, $debit, $creditor);
			/*
			if($totalvalue == $paid)
			{
				// إلى حساب الخزينة أو البنك
				$account_id = $tree_id;
				$debit = 0;
				$creditor = $totalvalue;
				$this->fin_journal_insert($account_id, $debit, $creditor);
			}
			else
			{
				if($paid > 0)
				{
					// إلى حساب الخزينة أو البنك
					$account_id = $tree_id;
					$debit = 0;
					$creditor = $paid;
					$this->fin_journal_insert($account_id, $debit, $creditor);

					// إلى حساب العميل
					$account_id = $customer_id;
					$debit = 0;
					$creditor = $remaining;
					$this->fin_journal_insert($account_id, $debit, $creditor);
				}
				else
				{
					// إلى حساب العميل
					$account_id = $customer_id;
					$debit = 0;
					$creditor = $totalvalue;
					$this->fin_journal_insert($account_id, $debit, $creditor);
				}
			}
			
			*/
			
			//تحديث كميات الأصناف بالمخزن
			$InvoiceItems = intval($this->session->userdata('acc_sale_reurn'));
			//M_product
			$barcode = "barcode";
			$location_id = "location_id";
			$quantity = "quantity";
			for ($i = 1; $i <= $InvoiceItems; $i++) {
				$quantity = "quantity".$i;
				$quantity = $this->input->post($quantity);
				if($quantity > 0)
				{
					$barcode = "barcode".$i;
					$barcode = $this->input->post($barcode);
					$ProductData = $this->M_product->GetByBarcode($barcode);
					$product_id = $ProductData->id;
					$location_id = "location_id".$i;
					$location_id = $this->input->post($location_id);
					$unitprice = "unitprice".$i;
					$unitprice = $this->input->post($unitprice);
				
					$Old_FromStoreData = $this->M_store_quantity->GetStoreProduct($location_id, $product_id);
					$Old_FromStore_Quantity = $Old_FromStoreData->quantity;
					$Old_FromStore_id = $Old_FromStoreData->id;
					$New_FromStore_Quantity = $Old_FromStore_Quantity + $quantity;
					$FromStoreArrayData = array();
					$FromStoreArrayData = array(
						'store_id' => $location_id,
						'product_id' => $product_id,
						'quantity' => $New_FromStore_Quantity,
					);
					$this->M_store_quantity->UpdateRecord($Old_FromStore_id, $FromStoreArrayData);

					
					$POS_Quantity_Data = array();
					$POS_Quantity_Data = array(
						'product_id' => $product_id,
						'store_id' => $location_id,
						'quantity' => $New_FromStore_Quantity,
					);
					$this->M_Tec_product_store_qty->UpdateRecord($Old_FromStore_id, $POS_Quantity_Data);

					//M_sale_returns_items
					$ItemsArray = array();
					$ItemsArray = array(
						'bill_id' => $bill_id,
						'store_type_id' => $product_id,
						'location_id' => $location_id,
						'quantity' => $quantity,
						'unitprice' => $unitprice,
					);
					$this->M_sale_returns_items->InsertRecord($ItemsArray);
				}
				
			}


			redirect ('sales/sale_returns');
		}
	}

	public function update_view($rid)
	{
		$DataRow = $this->M_sale_returns->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['country_id'] = $DataRow->country_id;
		$data['area'] = $DataRow->area;
		$data['title'] = $DataRow->title;
		$data['email'] = $DataRow->email;
		$data['phone'] = $DataRow->phone;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		//$data += $this->WebSite();
		$this->load->view('sale_returns/sale_returns_update', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("country_id","country_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$country_id = set_value("country_id");
			$area = set_value("area");
			$title = set_value('title');
			$email = set_value('email');
			$phone = set_value('phone');
			$deleted = set_value('deleted');
			$ChangeImage = set_value('ChangeImage');

			$NewData = array();
			if($ChangeImage == "True")
			{
				$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
				$exts = explode(".", $_FILES['image']['name']);
				$exts = end($exts);
				$exts = strtolower($exts);
				$UploadPath = "./upload/sale_returns/";
				$ImageDirectory = $UploadPath;
				
				if (!is_dir($ImageDirectory)) {
					mkdir($ImageDirectory, 0777, TRUE);

				}

				$config['upload_path'] = $ImageDirectory;
				$config['allowed_types'] = "*";
				$config['file_name'] = $filename.".".$exts;
				$config['overwrite'] = TRUE;
				$config['file_ext_tolower'] = TRUE;
				$config['remove_spaces'] = TRUE;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				$NewFileName = $filename.".".$exts;
				if (! $this->upload->do_upload('image', $NewFileName))
				{
					$error = array('error' => $this->upload->display_errors());
					$NewFileName ="";
				}
				else {
					$this->upload->do_upload('image', $NewFileName);
				}
				
				$NewData = array(
					'country_id' => $country_id,
					'area' => $area,
					'title' => $title,
					'email' => $email,
					'phone' => $phone,
					'image' => $NewFileName,
					'deleted' => $deleted,
				);
			}
			else
			{
				$NewData = array(
					'country_id' => $country_id,
					'area' => $area,
					'title' => $title,
					'email' => $email,
					'phone' => $phone,
					'deleted' => $deleted,
				);
			}
			

			$this->M_sale_returns->UpdateRecord($id, $NewData);
			
			redirect ('sale_returns');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_sale_returns->UpdateRecord($rid, $NewData);

        redirect("sale_returns");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_sale_returns->UpdateRecord($rid, $NewData);

        redirect("sale_returns");
	}

	public function FillProductsList($barcode){
        $CodeCount = $this->M_product->CheckBarcode($barcode);
        if($CodeCount > 0)
        {
            $TypeName = $this->M_product->GetByBarcode($barcode);
            echo $TypeName->title;
        }
        else
        {
            echo "-";
        }
	}

	public function fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $image)
	{
		$company_id = intval($this->session->userdata('company_id'));
		$user_id = intval($this->session->userdata('StaffID'));
		$automatic = 1;
		$deleted = 0;

		$fin_journal_array = array();
		$fin_journal_array = array(
			'company_id' => $company_id,
			'user_id' => $user_id,
			'details' => $details,
			'thedate' => $thedate,
			'bill_id' => $bill_id,
			'table_name' => $table_name,
			'bond_no' => $bond_no,
			'image' => $image,
			'automatic' => $automatic,
			'deleted' => $deleted,
		);

		$this->M_fin_journal_main->InsertRecord($fin_journal_array);
	}

	public function fin_journal_insert($account_id, $debit, $creditor)
	{
		$company_id = intval($this->session->userdata('company_id'));
		$user_id = intval($this->session->userdata('StaffID'));

		$MainData = $this->M_fin_journal_main->GetLatestRecord($company_id, $user_id);
		$main_id = $MainData->id;

		$fin_journal_array = array();
		$fin_journal_array = array(
			'main_id' => $main_id,
			'account_id' => $account_id,
			'debit' => $debit,
			'creditor' => $creditor,
		);

		$this->M_fin_journal->InsertRecord($fin_journal_array);
	}

	public function view_main()
	{
		$id = $this->input->get('id');
		$MainDataRow = $this->M_sale_returns->GetInvoiceHeader($id);
		echo json_encode($MainDataRow); 
    	exit();
	}

	public function view_details()
	{
		$id = $this->input->get('id');
		//$id = intval($this->session->userdata('Fin_journal_View_ID'));
		$DetailsRows = $this->M_sale_returns->GetInvoiceDetails($id);
		echo json_encode($DetailsRows); 
		exit();
	}
  public function view_pdf($id)
    {
        $MainDataRow = $this->M_sale_returns->GetInvoiceHeader($id);
        $DetailsRows = $this->M_sale_returns->GetInvoiceDetails($id);
        $Alignment = "";
        $Direction = "";
        if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar")) {
            $this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
            $this->session->set_userdata('Alignment', "right");
            $this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
            $Alignment = "right";
            $Direction = "rtl";
        } else {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
            $this->session->set_userdata('Alignment', "left");
            $this->session->set_userdata('HTML', "lang='en'");
            $Alignment = "left";
            $Direction = "ltr";
        }
        $this->load->model("settings/M_fin_settings");
        $this->load->model("settings/M_tec_settings");
        $company_id = intval($this->session->userdata('company_id'));
        $DataRow = $this->M_app_company->GetRow($company_id);
        $tax_date = $DataRow->start_date;
        $HeaderImage = base_url() . "upload/settings/" . $DataRow->header;
        $FooterImage = base_url() . "upload/settings/" . $DataRow->footer;
        $Directionality = $this->session->userdata('Direction');
        $PageTitle = lang('pdf_sale_returns');
        $PageSize = $this->session->userdata('sale_bill_size');
        $PageOrientation = $this->session->userdata('sale_bill_orientation');
        $PageFormat = $PageSize . "-" . $PageOrientation;
        require_once 'vendor/autoload.php';
        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => $PageFormat,
            //'margin_left' => 5,
            //'margin_right' => 5,
            'margin_top' => 25,
            'margin_bottom' => 25,
            'margin_header' => 5,
            'margin_footer' => 5,
        ]);
        $mpdf->allow_charset_conversion = true;
        $mpdf->SetTitle($PageTitle);
        $mpdf->SetDirectionality($Directionality);
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;
        $mpdf->debug = true;
        ob_end_clean();
        header("Content-Encoding: None", true);
        $mpdf->SetHTMLHeader('
 		<div style="text-align: center; font-weight: bold;">
 			<img src="' . $HeaderImage . '">
 		</div>');
        $mpdf->SetHTMLFooter('
 		<table width="100%" style="vertical-align: bottom; font-family: serif;
 			font-size: 8pt; color: #000000; font-weight: bold;">
 			<tr style="border:none!important;">
 				<td colspan="2" style="border:none!important;"><img src="' . $FooterImage . '"></td>
 			</tr>
 			<tr style="border:none!important;">
 				<td width="50%" style="border:none!important;">{DATE j-m-Y}</td>
 				<td width="50%" style="border:none!important;" align="left" lang="en">{PAGENO}/{nbpg}</td>
 			</tr>
 		</table>');
 		$html ="";
 		
        $MainDataRow = $this->M_sale_returns->GetInvoiceHeader($id);
        $DetailsRows = $this->M_sale_returns->GetInvoiceDetails($id);
        $data['id'] = $MainDataRow->id;
        $data['company_id'] = $MainDataRow->company_id;
        $company_id = $MainDataRow->company_id;
        $tec_settings = $this->M_tec_settings->GetRow();
        $customer_name = $MainDataRow->customer_title;
        $CompanyData = $this->M_app_company->GetRow($company_id);
        $tax_number = $tec_settings->tax_number;
        $Invoice_Start = $tec_settings->tax_date;
        $customer_id = $MainDataRow->customer_id;
        $CustomerData = $this->M_sale_customer->GetRow($customer_id);
        $CustomerPhone = $CustomerData->phone;
        if ($customer_name == "") {
            if ($this->session->userdata('lang') == "ar") {
                $CustomerName = $CustomerData->ar_title;
            } else {
                $CustomerName = $CustomerData->en_title;
            }
        } else {
            $CustomerName = $customer_name;
        }
        
       $user_id = $MainDataRow->user_id;
        $UserData = $this->M_usr_users->GetRow($user_id);
        $InvoiceEditor = $UserData->fullname;
        $sale_start = $UserData->sale_start;
        $branch = $UserData->branch;
        if ($branch != "") {
            $branch = " (" . $branch . ")";
        }
        if ($sale_start != "") {
            $Invoice_Start = $UserData->sale_start;
        }
       // $delivery_id = $MainDataRow->delivery_id;
        $thedate = $MainDataRow->thedate;
        $html = '<style>td, th{border:solid 1px #dddddd;}</style>';
        $html .= '<div align="center" style="font-size:18px">';
        $html .= '<strong>' . $PageTitle . $branch . '</strong><br>';
        $html .= '</div><br>';
        $html .= '<table cellspacing="0" cellpadding="5" border="0" width="98%">';
        $html .= '<tbody>';
        $html .= '<tr style="border:none !important;vertical-align: baseline !important;">';
        $html .= '<td colspan="1" style="padding-right:0 !important;border:none !important;">';
        $html .= '<table cellspacing="0" cellpadding="5" width="100%">';
        $html .= '<thead>';
        $html .= '<tr style="background-color:#EBEBEB; font-weight:bold;border:none !important;">';
        $html .= '<th colspan="3">بيانات</th>';
        $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<tbody>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('Site Name') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tec_settings->site_name . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('phone') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tec_settings->tel . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('address') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tec_settings->address . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('buy_manufacturer_tax_card') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tax_number . '</td>';
        $html .= '</tr>';
        // $html .= '<tr style="border:none !important;">';
        // $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('buy_manufacturer_tax_card_date') . '</td>';
        // $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $tax_date . '</td>';
        // $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 0px solid #fff;"></td>';
        $html .= '<td colspan="2" style="border: 0px solid #fff;color:#fff">.</td>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</td>';
        //Customer
        $html .= '<td colspan="1" style="padding-left:0;border:none !important;">';
        $html .= '<table cellspacing="0" cellpadding="5" width="100%">';
        $html .= '<thead>';
        $html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
        $html .= '<th colspan="3">بيانات</th>';
        $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<tbody>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('sale_fullname') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $CustomerName . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('phone') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $CustomerPhone . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('address') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $CustomerData->address . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('tax_card') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $CustomerData->tax_card . '</td>';
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('data_account_invoice_id') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $id . '</td>'; //$Invoice_Start . "  -  " .
        $html .= '</tr>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('thedate') . '</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;">' . $MainDataRow->thedate . '</td>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '<table cellspacing="0" cellpadding="5" width="98%">';
        $html .= '<tbody>';
        $html .= '<tr style="border:none !important;">';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('sale_returns_totalvalue') . ' :</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;"><b>' . number_format($MainDataRow->totalvalue, 2) . '</b></td>';
        $html .= '<td colspan="1" style="border: 1px solid #dddddd;">' . $this->lang->line('pdf_Invoice_Editor') . ' :</td>';
        $html .= '<td colspan="2" style="border: 1px solid #dddddd;"><b>' . $InvoiceEditor . '</b></td>';
        $html .= '</tr>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</div><br>';
        $html .= '<table cellspacing="0" cellpadding="5" width="98%" style="border: solid 1px #dddddd;">';
        $html .= '<thead>';
        $html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('Serial') . '</th>';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('sale_bill_items_store_type_id') . '</th>';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('sale_bill_items_quantity') . '</th>';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('sale_bill_items_unitprice') . '</th>';
        $html .= '<th style="border: 1px solid #dddddd;">' . $this->lang->line('subtotal') . '</th>';
        $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<tbody>';
       $Serial = 0;
        $TotalValue = 0;
        $AllQuantity = 0;
        $FinalBigQ = 0;
        $FinalSmallQ = 0;
        foreach ($DetailsRows as $MultiRows_Row) {
            $Serial += 1;
            $type_id = $MultiRows_Row->store_type_id;
            $ProductData = $this->M_product->GetRow($type_id);
            if ($this->session->userdata('lang') == "ar") {
                $ProductName = $ProductData->title;
            } else {
                $ProductName = $ProductData->title_en;
            }
            $location_id = $MultiRows_Row->store_type_id;
            $StoreData = $this->M_store->GetRow($location_id);
            if ($this->session->userdata('lang') == "ar") {
                $StoreName = $StoreData->title;
            } else {
                $StoreName = $StoreData->title_en;
            }
            $html .= '<tr>';
            $html .= '<td style="background-color:#EBEBEB; font-weight:bold;">' . $Serial . '</td>';
            $html .= '<td>' . $ProductName . '</td>';
            $html .= '<td align="center">' . $MultiRows_Row->quantity . '</td>';
            $AllQuantity += $MultiRows_Row->quantity;
            $html .= '<td align="center">' . number_format($MultiRows_Row->unitprice, 2) . '</td>';
            $SubTotal = doubleval($MultiRows_Row->quantity) * doubleval($MultiRows_Row->unitprice);
            $html .= '<td align="center">' . number_format($MultiRows_Row->quantity * $MultiRows_Row->unitprice, 2) . '</td>';
            $html .= '</tr>';
            $TotalValue += $SubTotal;
        }
      /*   $tax = $DataRow->tax;
        $taxValue = ($TotalValue - $DataRow->discount) * ($tax / 100);
        $totalPrice = doubleval($TotalValue + $taxValue) - doubleval($DataRow->discount);
        $Items = $this->lang->line('invoice_items') . ' : ' . $Serial;
       */ $html .= '<tr>';
        $html .= '<td colspan="3" style="border:none!important">' . $Items . '</td>';
        $html .= '<td align="center">' . $this->lang->line('sale_returns_totalvalue') . '</td>';
        $html .= '<td align="center">' . number_format($TotalValue, 2) . '</td>';
        $html .= '</tr>';
        $QuantityText = $this->lang->line('sale_bill_items_quantity') . ' : ' . $AllQuantity;
        $html .= '<tr>';
        $html .= '<td colspan="3" style="border:none!important">' . $QuantityText . '</td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_discount') . '</td>';
        $html .= '<td align="center">' . number_format($MainDataRow->discount, 2) . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="3" style="border:none!important">';
        if ($this->session->userdata('lang') == "ar") {
            $invoice_text = $this->session->userdata('sale_bill_ar_text');
        } else {
            $invoice_text = $this->session->userdata('sale_bill_en_text');
        }
        $html .= $invoice_text;
        $html .= '</td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_total_after_discount') . '</td>';
        $html .= '<td align="center">' . number_format( $TotalValue - $MainDataRow->discount, 2) . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="3" style="border:none!important"></td>';
        $html .= '<td align="center">' . $this->lang->line('buy_offer_tax') . '</td>';
        $tax_ammount=( $TotalValue - $MainDataRow->discount)*$this->session->userdata('acc_tax_sale_percent')/100;
        $html .= '<td align="center">' . number_format($tax_ammount, 2) . '</td>';
        $html .= '</tr>';
        // print_r($_SESSION);die();
        // $html .= '<tr>';
        // $html .= '<td colspan="3" style="border:none!important"></td>';
        // $html .= '<td align="center">' . $this->lang->line('sale_bill_totalvalue') . '</td>';
        // $html .= '<td align="center">' . number_format($MainDataRow->totalvalue, 2) . '</td>';
        // $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="3" style="border:none!important"></td>';
        $html .= '<td align="center">' . $this->lang->line('sale_bill_totalvalue') . '</td>';
        $html .= '<td align="center">' . number_format($MainDataRow->paid +$tax_ammount, 2) . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="3" style="border:none!important">';
        // $ArabicMoney = $this->makeNumber2Text($MainDataRow->paid);
        // $html .= $ArabicMoney . ' ' . $this->lang->line('JustDoNot');
        $html .= '</td>';
        if($MainDataRow->remaining >0){
        $html .= '<td align="center">' . $this->lang->line('sale_bill_remaining') . '</td>';
        $html .= '<td align="center">' . number_format($MainDataRow->remaining, 2) . '</td>';

         }
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '<br>';
        $html .= '<table cellspacing="0" cellpadding="5" border="0" width="98%" align="center">';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;">' . $this->lang->line('IReceived') . '</td>';
        $html .= '<td style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;">' . $InvoiceEditor . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="center" style="border:none!important;">QR Code</td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;">' . $this->lang->line('ReceviesSign') . '</td>';
                     $html .= '<td align="center" rowspan="6" style="border:none!important;">';
        $this->load->library('QR');
        $html .= QR::get_base($tec_settings->site_name,$tax_number,$MainDataRow->thedate, $MainDataRow->totalvalue,$taxValue);
        $html .= '</td>';
        // $html .= '<td align="center" rowspan="6" style="border:none!important;"><img height="160" src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=' . $this->lang->line('Site Name') . ' : ' . $tec_settings->site_name . '%0A' . $this->lang->line('buy_manufacturer_tax_card') . ' : ' . $tax_number . '%0A' . $this->lang->line('buy_offer_tax') . ' : ' . $taxValue . '%0A' . $this->lang->line('sale_bill_totalvalue') . ' : ' . $MainDataRow->totalvalue . '&choe=UTF-8" ></td>';
        $html .= '<td align="left" style="border:none!important;">' . $this->lang->line('SalesmanSign') . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;">..........................................</td>';
        $html .= '<td align="left" style="border:none!important;">..........................................</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td align="right" style="border:none!important;"></td>';
        $html .= '<td align="left" style="border:none!important;"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        if (isset($_GET['test'])) {
            echo $html;
            exit;
        }
        $html = iconv("utf-8", "UTF-8//IGNORE", $html);
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
    }
	public function deleted()
	{
		$this->session->set_userdata('Filter', lang('Deleted'));

		$data['DataRows'] = $this->M_sale_returns->GetDeleted();
		$data['content_page'] = "sales/sale_returns";
		$this->load->view('page', $data);
	}

	public function payment($payment=1)
	{
		if($payment == 1)
		{
			$this->session->set_userdata('Filter', lang('buy_bill_full_paid'));
			$data['DataRows'] = $this->M_sale_returns->GetFullPaid();
		}
		else if($payment == 2)
		{
			$this->session->set_userdata('Filter', lang('buy_bill_part_paid'));
			$data['DataRows'] = $this->M_sale_returns->GetPartPaid();
		}
		else
		{
			$this->session->set_userdata('Filter', lang('buy_bill_not_paid'));
			$data['DataRows'] = $this->M_sale_returns->GetNotPaid();
		}
		
		$data['content_page'] = "sales/sale_returns";
		$this->load->view('page', $data);
	}

	public function close_invoice()
	{
		$this->session->set_userdata('Filter', "");

		$company_id = intval($this->session->userdata('company_id'));
		$StaffID = intval($this->session->userdata('StaffID'));

		$this->form_validation->set_rules("id","id","required");
		$this->form_validation->set_rules("tree_id","tree_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$paid_value = set_value('paid_value');
			$tree_id = set_value('tree_id');
			$from_customer_account = set_value('from_customer_account');
			$Bill_Record = $this->M_sale_returns->GetRow($id);
			$totalvalue = $Bill_Record->totalvalue;
			$paid = $Bill_Record->paid;
			$remaining = $Bill_Record->remaining;
			$customer_id = $Bill_Record->customer_id;
			$thedate = date("Y-m-d");
			$user_id = $StaffID;

			$New_Paid = $paid_value + $paid;
			$New_Remaining = $totalvalue - $New_Paid;


			$NewData = array();
			$NewData = array(
				'paid' => $New_Paid,
				'remaining' => $New_Remaining,
			);

			$this->M_sale_returns->UpdateRecord($id, $NewData);


			$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/sale_returns/";
			$ImageDirectory = $UploadPath;
			
			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
			}

			$bill_id = $id;
			
			//Insert fin_journal
			$acc_sale_reurn = intval($this->session->userdata('acc_sale_reurn'));
			$details = $this->lang->line('sale_returns_pay');
			$table_name = "sale_returns";
			$bond_no = $bill_id;
			
			$this->fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $NewFileName);
			// من حساب مردودات المبيعات 
			$account_id = $acc_sale_reurn;
			$debit = $paid_value;
			$creditor = 0;
			$this->fin_journal_insert($account_id, $debit, $creditor);
			//إلى حساب العميل أو إلى حساب الخزينة والبنك 
			if($from_customer_account == 1)
			{
				$account_id = $customer_id;
			}
			else
			{
				$account_id = $tree_id;
			}
			$account_id = $customer_id;
			$debit = 0;
			$creditor = $paid_value;
			$this->fin_journal_insert($account_id, $debit, $creditor);
			

			redirect ('sales/sale_returns');
		}
	}
}
