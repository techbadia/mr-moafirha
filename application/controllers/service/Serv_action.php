<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Serv_action extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		$this->load->helper('url'); 
        $this->load->helper('form'); 
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
        $this->load->model("service/M_serv_action");
        $this->load->model("service/M_serv_action_types");
        $this->load->model("project/M_proj_project");
	}
	
	public function index()
	{
        $this->session->set_userdata('Filter', lang('serv_action_all'));
        $this->session->set_userdata('ActionTypeFilter', 0);
		$data['DataRows'] = $this->M_serv_action->GetMultiRow();
		$data['content_page'] = "service/serv_action";
		$this->load->view('page', $data);
    }

    public function done()
	{
        $this->session->set_userdata('Filter', lang('serv_action_done'));
        $this->session->set_userdata('ActionTypeFilter', 0);
		$data['DataRows'] = $this->M_serv_action->Get_done();
		$data['content_page'] = "service/serv_action";
		$this->load->view('page', $data);
    }

    public function onhold()
	{
        $this->session->set_userdata('Filter', lang('serv_action_onhold'));
        $this->session->set_userdata('ActionTypeFilter', 0);
		$data['DataRows'] = $this->M_serv_action->Get_onhold();
		$data['content_page'] = "service/serv_action";
		$this->load->view('page', $data);
    }

    public function expired_onhold()
	{
        $this->session->set_userdata('Filter', lang('serv_action_expired_onhold'));
        $this->session->set_userdata('ActionTypeFilter', 0);
		$data['DataRows'] = $this->M_serv_action->Get_expired_onhold();
		$data['content_page'] = "service/serv_action";
		$this->load->view('page', $data);
    }

    public function expired_done()
	{
        $this->session->set_userdata('Filter', lang('serv_action_expired_done'));
        $this->session->set_userdata('ActionTypeFilter', 0);
		$data['DataRows'] = $this->M_serv_action->Get_expired_done();
		$data['content_page'] = "service/serv_action";
		$this->load->view('page', $data);
    }

    public function coming()
	{
        $this->session->set_userdata('Filter', lang('serv_action_coming'));
        $this->session->set_userdata('ActionTypeFilter', 0);
		$data['DataRows'] = $this->M_serv_action->Get_coming();
		$data['content_page'] = "service/serv_action";
		$this->load->view('page', $data);
    }

    public function type($type_id)
	{
        $TypeData = $this->M_serv_action_types->GetRow($type_id);
        if ($this->session->userdata('lang') == "ar")
        {
            $TypeTitle = $TypeData->ar_title;
        }
        else
        {
            $TypeTitle = $TypeData->en_title;
        }

        $this->session->set_userdata('Filter', $TypeTitle);
        $this->session->set_userdata('ActionTypeFilter', $type_id);
		$data['DataRows'] = $this->M_serv_action->GetByType($type_id);
		$data['content_page'] = "service/serv_action";
		$this->load->view('page', $data);
    }
    
	public function insertform()
	{
        $this->session->set_userdata('Filter', "");
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "service/serv_action_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("start_date","start_date","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $from_user = intval($this->session->userdata('StaffID'));
            $to_user = set_value('to_user');
            $type_id = set_value('type_id');
            $project_id = set_value('project_id');
            $add_date = set_value('add_date');
            $add_time = set_value('add_time');
            $start_date = set_value('start_date');
            $end_date = set_value('end_date');
			$details = set_value('details');
			$done = set_value('done');
            $deleted = 0;
            
            $EventData = array();
            $EventData = array(
                'company_id' => $company_id,
                'from_user' => $from_user,
                'to_user' => $to_user,
                'type_id' => $type_id,
                'project_id' => $project_id,
                'add_date' => $add_date,
                'add_time' => $add_time,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'details' => $details,
                'done' => $done,
                'deleted' => $deleted,
            );
            $this->M_serv_action->InsertRecord($EventData);

			redirect ('service/serv_action');
		}
	}

	
	public function delete($rid)
	{
        $this->session->set_userdata('Filter', "");

		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_serv_action->UpdateRecord($rid, $NewData);

        redirect("service/serv_action");
	}

	public function undelete($rid)
	{
        $this->session->set_userdata('Filter', "");

		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_serv_action->UpdateRecord($rid, $NewData);

        redirect("service/serv_action");
    }

    public function do_done($rid)
	{
        $this->session->set_userdata('Filter', "");

		$done = 1;
		$NewData = array();
		$NewData = array(
			'done' => $done,
		);

		$this->M_serv_action->UpdateRecord($rid, $NewData);

        redirect("service/serv_action");
    }

    public function deleted()
    {
        $this->session->set_userdata('Filter', lang('Deleted'));
		$data['DataRows'] = $this->M_serv_action->GetDeleted();
		$data['content_page'] = "service/serv_action";
		$this->load->view('page', $data);
    }

    public function view($rid)
    {
        $this->session->set_userdata('Filter', "");

        $DataRow = $this->M_serv_action->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
        $data['from_user'] = $DataRow->from_user;
        $data['to_user'] = $DataRow->to_user;
        $data['type_id'] = $DataRow->type_id;
        $data['project_id'] = $DataRow->project_id;
        $data['add_date'] = $DataRow->add_date;
        $data['add_time'] = $DataRow->add_time;
		$data['start_date'] = $DataRow->start_date;
		$data['end_date'] = $DataRow->end_date;
		$data['details'] = $DataRow->details;
        $data['done'] = $DataRow->done;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "service/serv_action_view";
		$this->load->view('page', $data);
    }

    public function updateform($rid)
    {
        $this->session->set_userdata('Filter', "");

        $DataRow = $this->M_serv_action->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
        $data['from_user'] = $DataRow->from_user;
        $data['to_user'] = $DataRow->to_user;
        $data['type_id'] = $DataRow->type_id;
        $data['project_id'] = $DataRow->project_id;
        $data['add_date'] = $DataRow->add_date;
        $data['add_time'] = $DataRow->add_time;
		$data['start_date'] = date("Y-m-d", strtotime($DataRow->start_date));
		$data['end_date'] = date("Y-m-d", strtotime($DataRow->end_date));
		$data['details'] = $DataRow->details;
        $data['done'] = $DataRow->done;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "service/serv_action_update";
		$this->load->view('page', $data);
    }

    public function Update_Data()
    {
        $this->form_validation->set_rules("start_date","start_date","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $from_user = intval($this->session->userdata('StaffID'));
            $to_user = set_value('to_user');
            $type_id = set_value('type_id');
            $project_id = set_value('project_id');
            $add_date = set_value('add_date');
            $add_time = set_value('add_time');
            $start_date = set_value('start_date');
            $end_date = set_value('end_date');
			$details = set_value('details');
			$done = set_value('done');
            $deleted = 0;
            
            $ActionData = array();
            $ActionData = array(
                'company_id' => $company_id,
                'from_user' => $from_user,
                'to_user' => $to_user,
                'type_id' => $type_id,
                'project_id' => $project_id,
                'add_date' => $add_date,
                'add_time' => $add_time,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'details' => $details,
                'done' => $done,
                'deleted' => $deleted,
            );
            $this->M_serv_action->UpdateRecord($id, $ActionData);

			redirect ('service/serv_action');
		}
    }
    
}
