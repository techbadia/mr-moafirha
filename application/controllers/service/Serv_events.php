<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Serv_events extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		$this->load->helper('url'); 
        $this->load->helper('form'); 
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
        $this->load->model("store/M_serv_events");
        $this->load->model("store/M_serv_events_users");
	}
	
	public function index()
	{
		$this->session->set_userdata('Filter', lang('serv_events_all'));
		$data['DataRows'] = $this->M_serv_events->GetMultiRow();
		$data['content_page'] = "service/serv_events";
		$this->load->view('page', $data);
    }
    
    public function events_old()
    {
        $this->session->set_userdata('Filter', lang('serv_events_old'));
		$data['DataRows'] = $this->M_serv_events->Get_old();
		$data['content_page'] = "service/serv_events";
		$this->load->view('page', $data);
    }

    public function events_current()
    {
        $this->session->set_userdata('Filter', lang('serv_events_current'));
		$data['DataRows'] = $this->M_serv_events->Get_current();
		$data['content_page'] = "service/serv_events";
		$this->load->view('page', $data);
    }

    public function events_upcoming()
    {
        $this->session->set_userdata('Filter', lang('serv_events_upcoming'));
		$data['DataRows'] = $this->M_serv_events->Get_coming();
		$data['content_page'] = "service/serv_events";
		$this->load->view('page', $data);
    }

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "service/serv_events_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("start_date","start_date","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $start_date = set_value('start_date');
            $end_date = set_value('end_date');
			$details = html_entity_decode(set_value('kt-tinymce-4'));
			$scheduled = set_value('scheduled');
            $scheduled_month = set_value('scheduled_month');
            $scheduled_year = set_value('scheduled_year');
            $deleted = 0;
            
            $EventData = array();
            $EventData = array(
                'company_id' => $company_id,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'details' => $details,
                'scheduled' => $scheduled,
                'scheduled_month' => $scheduled_month,
                'scheduled_year' => $scheduled_year,
                'deleted' => $deleted,
            );
            $this->M_serv_events->InsertRecord($EventData);

            $LatestRecord = $this->M_serv_events->GetLatest($start_date, $end_date);
            $EventID = $LatestRecord->id;

            $NewData = array();
            foreach ($_REQUEST['to_user'] as $to_user)
            {
                $NewData = array(
                    'company_id' => $company_id,
                    'event_id' => $EventID,
                    'user_id' => $to_user,
                    'deleted' => $deleted,
                );
    
                $this->M_serv_events_users->InsertRecord($NewData);
            }

			redirect ('service/serv_events');
		}
	}

	
	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_serv_events->UpdateRecord($rid, $NewData);

        redirect("service/serv_events");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_serv_events->UpdateRecord($rid, $NewData);

        redirect("service/serv_events");
    }

    public function deleted()
    {
        $this->session->set_userdata('Filter', lang('serv_message_trash'));
		$data['DataRows'] = $this->M_serv_events->GetDeleted();
		$data['content_page'] = "service/serv_events";
		$this->load->view('page', $data);
    }

    public function view($rid)
    {
        $this->session->set_userdata('Filter', "");

        $DataRow = $this->M_serv_events->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['start_date'] = $DataRow->start_date;
		$data['end_date'] = $DataRow->end_date;
		$data['details'] = $DataRow->details;
        $data['scheduled'] = $DataRow->scheduled;
        $data['scheduled_month'] = $DataRow->scheduled_month;
        $data['scheduled_year'] = $DataRow->scheduled_year;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "service/serv_events_view";
		$this->load->view('page', $data);
    }

    public function updateform($rid)
    {
        $this->session->set_userdata('Filter', "");

        $DataRow = $this->M_serv_events->GetRow($rid);
        $data['id'] = $DataRow->id;
        $data['event_id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['start_date'] = $DataRow->start_date;
		$data['end_date'] = $DataRow->end_date;
		$data['details'] = $DataRow->details;
        $data['scheduled'] = $DataRow->scheduled;
        $data['scheduled_month'] = $DataRow->scheduled_month;
        $data['scheduled_year'] = $DataRow->scheduled_year;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "service/serv_events_update";
		$this->load->view('page', $data);
    }

    public function Update_Data()
    {
        $this->form_validation->set_rules("start_date","start_date","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $start_date = set_value('start_date');
            $end_date = set_value('end_date');
			$details = html_entity_decode(set_value('kt-tinymce-4'));
			$scheduled = set_value('scheduled');
            $scheduled_month = set_value('scheduled_month');
            $scheduled_year = set_value('scheduled_year');
            $deleted = set_value('deleted');
            
            $EventData = array();
            $EventData = array(
                'company_id' => $company_id,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'details' => $details,
                'scheduled' => $scheduled,
                'scheduled_month' => $scheduled_month,
                'scheduled_year' => $scheduled_year,
                'deleted' => $deleted,
            );
            $this->M_serv_events->UpdateRecord($id, $EventData);

            $this->db->delete("serv_events_users", array('event_id' => $id));

            $NewData = array();
            foreach ($_REQUEST['to_user'] as $to_user)
            {
                $NewData = array(
                    'company_id' => $company_id,
                    'event_id' => $id,
                    'user_id' => $to_user,
                    'deleted' => $deleted,
                );
    
                $this->M_serv_events_users->InsertRecord($NewData);
            }

			redirect ('service/serv_events');
		}
    }
    
}
