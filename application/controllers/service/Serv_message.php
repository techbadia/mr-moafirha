<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Serv_message extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		$this->load->helper('url'); 
        $this->load->helper('form'); 
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
        $this->load->model("service/M_serv_message");
        
	}
	
	public function index()
	{
		$this->session->set_userdata('Filter', "");
		$data['DataRows'] = $this->M_serv_message->GetInbox();
		$data['content_page'] = "service/serv_message_inbox";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "service/serv_message_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("subject","subject","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $from_user = intval($this->session->userdata('StaffID'));
			$subject = set_value('subject');
			$message = html_entity_decode(set_value('kt-tinymce-4'));
			$thedate = date("Y-m-d");
            $thetime = date("h:i:s");
            $readed = 0;
            $marked = 0;
            $deleted = 0;
            
            $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/serv_message/";
			$ImageDirectory = $UploadPath;
			
			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
            }
            
            $NewData = array();

            $action = $this->input->post('action');
            //echo $action;
            if($action == 'SaveDraft')
            {
                $draft = 1;
                $to_user = 0;
                $NewData = array(
                    'company_id' => $company_id,
                    'from_user' => $from_user,
                    'to_user' => $to_user,
                    'subject' => $subject,
                    'message' => $message,
                    'image' => $NewFileName,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                    'readed' => $readed,
                    'marked' => $marked,
                    'draft' => $draft,
                    'deleted' => $deleted,
                );
    
                $this->M_serv_message->InsertRecord($NewData);
            }
            if($action == 'Send')
            {
                $draft = 0;
                foreach ($_REQUEST['to_user'] as $to_user)
                {
                    $NewData = array(
                        'company_id' => $company_id,
                        'from_user' => $from_user,
                        'to_user' => $to_user,
                        'subject' => $subject,
                        'message' => $message,
                        'image' => $NewFileName,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                        'readed' => $readed,
                        'marked' => $marked,
                        'draft' => $draft,
                        'deleted' => $deleted,
                    );
        
                    $this->M_serv_message->InsertRecord($NewData);
                }
                
            }

			redirect ('service/serv_message');
		}
	}

	
	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_serv_message->UpdateRecord($rid, $NewData);

        redirect("service/serv_message");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_serv_message->UpdateRecord($rid, $NewData);

        redirect("service/serv_message");
    }
    
    public function marked()
    {
        $this->session->set_userdata('Filter', lang('serv_message_marked'));
		$data['DataRows'] = $this->M_serv_message->GetMarked();
		$data['content_page'] = "service/serv_message_marked";
		$this->load->view('page', $data);
    }

    public function deleted()
    {
        $this->session->set_userdata('Filter', lang('serv_message_trash'));
		$data['DataRows'] = $this->M_serv_message->GetDeleted();
		$data['content_page'] = "service/serv_message_deleted";
		$this->load->view('page', $data);
    }

    public function remove_mark($rid)
	{
		$marked = 0;
		$NewData = array();
		$NewData = array(
			'marked' => $marked,
		);

		$this->M_serv_message->UpdateRecord($rid, $NewData);

        $this->load->library('user_agent');
		redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function add_mark($rid)
	{
		$marked = 1;
		$NewData = array();
		$NewData = array(
			'marked' => $marked,
		);

		$this->M_serv_message->UpdateRecord($rid, $NewData);

        $this->load->library('user_agent');
		redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function draft()
    {
        $this->session->set_userdata('Filter', lang('serv_message_draft'));
		$data['DataRows'] = $this->M_serv_message->GetDraft();
		$data['content_page'] = "service/serv_message_draft";
		$this->load->view('page', $data);
    }

    public function sent()
    {
        $this->session->set_userdata('Filter', lang('serv_message_sent'));
		$data['DataRows'] = $this->M_serv_message->GetOutBox();
		$data['content_page'] = "service/serv_message_sent";
		$this->load->view('page', $data);
    }

    public function view($rid)
    {
        $this->session->set_userdata('Filter', "");

        $readed = 1;
		$NewData = array();
		$NewData = array(
			'readed' => $readed,
		);
        $this->M_serv_message->UpdateRecord($rid, $NewData);
        
        $DataRow = $this->M_serv_message->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['from_user'] = $DataRow->from_user;
		$data['to_user'] = $DataRow->to_user;
		$data['subject'] = $DataRow->subject;
        $data['message'] = $DataRow->message;
        $data['image'] = $DataRow->image;
        $data['thedate'] = $DataRow->thedate;
        $data['thetime'] = $DataRow->thetime;
        $data['readed'] = $DataRow->readed;
        $data['marked'] = $DataRow->marked;
        $data['draft'] = $DataRow->draft;
        $data['deleted'] = $DataRow->deleted;
        
        $from_user = $DataRow->from_user;
        $UserData = $this->M_usr_users->GetRow($from_user);
        $data['SenderID'] = $DataRow->from_user;
        $data['SenderFullName'] = $UserData->fullname;
        $data['SenderImage'] = $UserData->image;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "service/serv_message_view";
		$this->load->view('page', $data);
    }

    public function read($rid)
    {
        $this->session->set_userdata('Filter', "");
        
        $DataRow = $this->M_serv_message->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['from_user'] = $DataRow->from_user;
		$data['to_user'] = $DataRow->to_user;
		$data['subject'] = $DataRow->subject;
        $data['message'] = $DataRow->message;
        $data['image'] = $DataRow->image;
        $data['thedate'] = $DataRow->thedate;
        $data['thetime'] = $DataRow->thetime;
        $data['readed'] = $DataRow->readed;
        $data['marked'] = $DataRow->marked;
        $data['draft'] = $DataRow->draft;
        $data['deleted'] = $DataRow->deleted;
        
        $from_user = $DataRow->from_user;
        $UserData = $this->M_usr_users->GetRow($from_user);
        $data['SenderID'] = $DataRow->from_user;
        $data['SenderFullName'] = $UserData->fullname;
        $data['SenderImage'] = $UserData->image;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "service/serv_message_view";
		$this->load->view('page', $data);
    }

    public function updateform($rid)
    {
        $this->session->set_userdata('Filter', "");

        $DataRow = $this->M_serv_message->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['from_user'] = $DataRow->from_user;
		$data['to_user'] = $DataRow->to_user;
		$data['subject'] = $DataRow->subject;
        $data['message'] = $DataRow->message;
        $data['image'] = $DataRow->image;
        $data['thedate'] = $DataRow->thedate;
        $data['thetime'] = $DataRow->thetime;
        $data['readed'] = $DataRow->readed;
        $data['marked'] = $DataRow->marked;
        $data['draft'] = $DataRow->draft;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "service/serv_message_update";
		$this->load->view('page', $data);
    }

    public function Update_Data()
    {
        $this->form_validation->set_rules("subject","subject","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $from_user = intval($this->session->userdata('StaffID'));
			$subject = set_value('subject');
			$message = html_entity_decode(set_value('kt-tinymce-4'));
			$thedate = date("Y-m-d");
            $thetime = date("h:i:s");
            $readed = 0;
            $marked = 0;
            $deleted = 0;
            
            $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/serv_message/";
			$ImageDirectory = $UploadPath;
			
			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
            }
            
            $NewData = array();

            $draft = 0;
            foreach ($_REQUEST['to_user'] as $to_user)
            {
                $NewData = array(
                    'company_id' => $company_id,
                    'from_user' => $from_user,
                    'to_user' => $to_user,
                    'subject' => $subject,
                    'message' => $message,
                    'image' => $NewFileName,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                    'readed' => $readed,
                    'marked' => $marked,
                    'draft' => $draft,
                    'deleted' => $deleted,
                );
    
                $this->M_serv_message->UpdateRecord($id, $NewData);
            }
            

			redirect ('service/serv_message');
		}
    }

    public function reply($rid)
    {
        $this->session->set_userdata('Filter', "");
        
        $DataRow = $this->M_serv_message->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['from_user'] = $DataRow->from_user;
		$data['to_user'] = $DataRow->to_user;
		$data['subject'] = $DataRow->subject;
        $data['message'] = $DataRow->message;
        $data['image'] = $DataRow->image;
        $data['thedate'] = $DataRow->thedate;
        $data['thetime'] = $DataRow->thetime;
        $data['readed'] = $DataRow->readed;
        $data['marked'] = $DataRow->marked;
        $data['draft'] = $DataRow->draft;
        $data['deleted'] = $DataRow->deleted;
        
        $from_user = $DataRow->from_user;
        $UserData = $this->M_usr_users->GetRow($from_user);
        $data['SenderID'] = $DataRow->from_user;
        $data['SenderFullName'] = $UserData->fullname;
        $data['SenderImage'] = $UserData->image;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "service/serv_message_reply";
		$this->load->view('page', $data);
    }
}
