<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branche extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);

        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }

        $this->load->model('app/M_app_company');
	}

	public function index()
	{
        $data['MultiRows'] = $this->M_app_company->GetMultiRow();
        
        $data['content_page'] = "app/company";
		$this->load->view('page', $data);
	}
    
    public function insertform()
    {       
        $data['content_page'] = "app/company_insert";
        $this->load->view('page', $data);
    }
    
    public function insert_data()
    {
        $this->load->helper('security');
        $this->load->library("form_validation");

        $company_id = intval($this->session->userdata('company_id'));

        $this->form_validation->set_rules("ar_title","ar_title","required");
        $this->form_validation->set_rules("en_title","en_title","required");
        
        if ($this->form_validation->run() == FALSE) {
        echo validation_errors();
        }
        else {
            
            $ar_title = set_value('ar_title');
            $en_title = set_value('en_title');
            $person = set_value('person');
            $email = set_value('email');
            $phone = set_value('phone');
            $tax_number = set_value('tax_number');
            $amount = 0;
            $invoice_sale_start = set_value('invoice_sale_start');
            $invoice_sale_return_start = set_value('invoice_sale_return_start');
            $invoice_buy_start = set_value('invoice_buy_start');
            $invoice_buy_return_start = set_value('invoice_buy_return_start');
            $invoice_service_start = set_value('invoice_service_start');
            
            $NewData = array();
            $NewData = array(
                'ar_title' => $ar_title,
                'en_title' => $en_title,
                'person' => $person,
                'email' => $email,
                'email' => $email,
                'phone' => $phone,
                'tax_number' => $tax_number,
                'amount' => $amount,
                'invoice_sale_start' => $invoice_sale_start,
                'invoice_sale_return_start' => $invoice_sale_return_start,
                'invoice_buy_start' => $invoice_buy_start,
                'invoice_buy_return_start' => $invoice_buy_return_start,
                'invoice_service_start' => $invoice_service_start,
            );

            $this->M_app_company->InsertRecord($NewData);

            $GetLatestRow = $this->M_app_company->GetLatest($ar_title, $en_title);
            $LatestID = $GetLatestRow->id;
            //// Insert M_fin_settings
            $fin_settings = $this->M_fin_settings->GetRow();
            $fin_settings_Data = array();
            $fin_settings_Data = array(
                'company_id' => $LatestID,
                'acc_sale' => $fin_settings->acc_sale,
                'acc_website_sale' => $fin_settings->acc_website_sale,
                'acc_sale_reurn' => $fin_settings->acc_sale_reurn,
                'acc_buy' => $fin_settings->acc_buy,
                'acc_buy_reurn' => $fin_settings->acc_buy_reurn,
                'acc_treasury' => $fin_settings->acc_treasury,
                'acc_bank' => $fin_settings->acc_bank,
                'acc_expenses' => $fin_settings->acc_expenses,
                'acc_revenues' => $fin_settings->acc_revenues,
                'acc_debtors' => $fin_settings->acc_debtors,
                'acc_creditors' => $fin_settings->acc_creditors,
                'acc_capture_papers' => $fin_settings->acc_capture_papers,
                'acc_payment_papers' => $fin_settings->acc_payment_papers,
                'acc_inventory' => $fin_settings->acc_inventory,
                'acc_supplier' => $fin_settings->acc_supplier,
                'acc_customer' => $fin_settings->acc_customer,
                'acc_material_store' => $fin_settings->acc_material_store,
                'acc_damaged_store' => $fin_settings->acc_damaged_store,
                'acc_invoice_items' => $fin_settings->acc_invoice_items,
                'acc_custody' => $fin_settings->acc_custody,
                'acc_hr_loan' => $fin_settings->acc_hr_loan,
                'acc_affiliate' => $fin_settings->acc_affiliate,
                'acc_delivery' => $fin_settings->acc_delivery,
                'acc_factory_cost' => $fin_settings->acc_factory_cost,
                'acc_operating_expenses' => $fin_settings->acc_operating_expenses,
                'acc_salary' => $fin_settings->acc_salary,
                'acc_tax_sale' => $fin_settings->acc_tax_sale,
                'acc_tax_sale_percent' => $fin_settings->acc_tax_sale_percent,
                'acc_tax_purchase' => $fin_settings->acc_tax_purchase,
                'acc_tax_purchase_percent' => $fin_settings->acc_tax_purchase_percent,
                'currency_id' => $fin_settings->currency_id,
                'acc_sales_commission' => $fin_settings->acc_sales_commission,
            );
            $this->M_fin_settings->InsertRecord($fin_settings_Data);
            //// Insert M_settings_acceptance
            $this->load->model('settings/M_settings_acceptance');
            $settings_acceptance = $this->M_settings_acceptance->GetRow();
            $settings_acceptance_Data = array();
            $settings_acceptance_Data = array(
                'company_id' => $LatestID,
                'arabic_required' => $settings_acceptance->arabic_required,
                'english_required' => $settings_acceptance->english_required,
                'upload_required' => $settings_acceptance->upload_required,
                'fin_option1' => $settings_acceptance->fin_option1,
                'fin_option2' => $settings_acceptance->fin_option2,
                'fin_option3' => $settings_acceptance->fin_option3,
                'purchase_option1' => $settings_acceptance->purchase_option1,
                'sales_option1' => $settings_acceptance->sales_option1,
                'sales_option2' => $settings_acceptance->sales_option2,
                'store_option1' => $settings_acceptance->store_option1,
                'hr_option1' => $settings_acceptance->hr_option1,
                'hr_option2' => $settings_acceptance->hr_option2,
                'hr_option3' => $settings_acceptance->hr_option3,
                'custody_option1' => $settings_acceptance->custody_option1,
                'custody_option2' => $settings_acceptance->custody_option2,
                'service_option1' => $settings_acceptance->service_option1,
                'maintenance_option1' => $settings_acceptance->maintenance_option1,
                'manufacturing_option1' => $settings_acceptance->manufacturing_option1,
            );
            $this->M_settings_acceptance->InsertRecord($settings_acceptance_Data);
            //// Insert M_settings_notifications
            $this->load->model('settings/M_settings_notifications');
            $settings_notifications = $this->M_settings_notifications->GetRow();
            $settings_notifications_Data = array();
            $settings_notifications_Data = array(
                'company_id' => $LatestID,
                'fin_option1' => $settings_notifications->fin_option1,
                'fin_option2' => $settings_notifications->fin_option2,
                'fin_option3' => $settings_notifications->fin_option3,
                'fin_option4' => $settings_notifications->fin_option4,
                'purchase_option1' => $settings_notifications->purchase_option1,
                'sales_option1' => $settings_notifications->sales_option1,
                'store_option1' => $settings_notifications->store_option1,
                'hr_option1' => $settings_notifications->hr_option1,
                'hr_option2' => $settings_notifications->hr_option2,
                'hr_option3' => $settings_notifications->hr_option3,
                'hr_option4' => $settings_notifications->hr_option4,
                'custody_option1' => $settings_notifications->custody_option1,
                'custody_option2' => $settings_notifications->custody_option2,
                'service_option1' => $settings_notifications->service_option1,
                'maintenance_option1' => $settings_notifications->maintenance_option1,
                'maintenance_option2' => $settings_notifications->maintenance_option2,
                'manufacturing_option1' => $settings_notifications->manufacturing_option1,
            );
            $this->M_settings_notifications->InsertRecord($settings_notifications_Data);
            //// Insert M_fin_treeaccount
            $this->load->model('app/M_fin_treeaccount');
            $DataRows = $this->M_fin_treeaccount->GetBasicAccounts();
            $Accounts_Data = array();
            foreach($DataRows as $DataRows_Row) {
                $Accounts_Data = array(
                    'company_id' => $LatestID,
                    'category_id' => $DataRows_Row->category_id,
                    'parent_id' => $DataRows_Row->parent_id,
                    'account_no' => $DataRows_Row->account_no,
                    'title' => $DataRows_Row->title,
                    'title_en' => $DataRows_Row->title_en,
                    'startamount' => $DataRows_Row->startamount,
                    'basic' => $DataRows_Row->basic,
                    'level_no' => $DataRows_Row->level_no,
                    'custody_user_id' => $DataRows_Row->custody_user_id,
                    'loan_user_id' => $DataRows_Row->loan_user_id,
                    'commission' => $DataRows_Row->commission,
                    'deleted' => $DataRows_Row->deleted,
                );
                $this->M_fin_treeaccount->InsertRecord($Accounts_Data);
            }
            //// Insert M_settings_print
            $this->load->model('settings/M_settings_print');
            $settings_print = $this->M_settings_print->GetRow();
            $settings_print_Data = array();
            $settings_print_Data = array(
                'company_id' => $LatestID,
                'buy_bill_ar_text' => $settings_print->buy_bill_ar_text,
                'buy_bill_en_text' => $settings_print->buy_bill_en_text,
                'buy_bill_orientation' => $settings_print->buy_bill_orientation,
                'buy_bill_size' => $settings_print->buy_bill_size,
                'buy_returns_ar_text' => $settings_print->buy_returns_ar_text,
                'buy_returns_en_text' => $settings_print->buy_returns_en_text,
                'buy_returns_orientation' => $settings_print->buy_returns_orientation,
                'buy_returns_size' => $settings_print->buy_returns_size,
                'sale_bill_ar_text' => $settings_print->sale_bill_ar_text,
                'sale_bill_en_text' => $settings_print->sale_bill_en_text,
                'sale_bill_orientation' => $settings_print->sale_bill_orientation,
                'sale_bill_size' => $settings_print->sale_bill_size,
                'sale_returns_ar_text' => $settings_print->sale_returns_ar_text,
                'sale_returns_en_text' => $settings_print->sale_returns_en_text,
                'sale_returns_orientation' => $settings_print->sale_returns_orientation,
                'sale_returns_size' => $settings_print->sale_returns_size,
                'data_invoice_ar_text' => $settings_print->data_invoice_ar_text,
                'data_invoice_en_text' => $settings_print->data_invoice_en_text,
                'data_invoice_orientation' => $settings_print->data_invoice_orientation,
                'data_invoice_size' => $settings_print->data_invoice_size,
            );
            $this->M_settings_print->InsertRecord($settings_print_Data);
            ////////////////////////////////
            $CompanyFolder = "branche/updateform/".$LatestID;
            redirect ($CompanyFolder);
        }
    }
    
    public function updateform($rid)
    {
    	$MyRecord = $this->M_app_company->GetRow($rid);

        $data['id'] = $MyRecord->id;
        $data['ar_title'] = $MyRecord->ar_title;
        $data['en_title'] = $MyRecord->en_title;
        $data['person'] = $MyRecord->person;
        $data['email'] = $MyRecord->email;
        $data['phone'] = $MyRecord->phone;
        $data['tax_number'] = $MyRecord->tax_number;
        $data['amount'] = $MyRecord->amount;
        $data['invoice_sale_start'] = $MyRecord->invoice_sale_start;
        $data['invoice_sale_return_start'] = $MyRecord->invoice_sale_return_start;
        $data['invoice_buy_start'] = $MyRecord->invoice_buy_start;
        $data['invoice_buy_return_start'] = $MyRecord->invoice_buy_return_start;
        $data['invoice_service_start'] = $MyRecord->invoice_service_start;
            
        $data['content_page'] = "app/company_update";
        $this->load->view('page', $data);
    }
    
    public function update_data()
    {
    	$this->load->helper('security');
        $this->load->library("form_validation");
        
        $this->form_validation->set_rules("ar_title","ar_title","required");
        $this->form_validation->set_rules("en_title","en_title","required");
        
        if ($this->form_validation->run() == FALSE) {
        echo validation_errors();
        }
        else {
            $id = set_value('id');
            $ar_title = set_value('ar_title');
            $en_title = set_value('en_title');
            $person = set_value('person');
            $email = set_value('email');
            $phone = set_value('phone');
            $tax_number = set_value('tax_number');
            $amount = 0;
            $invoice_sale_start = set_value('invoice_sale_start');
            $invoice_sale_return_start = set_value('invoice_sale_return_start');
            $invoice_buy_start = set_value('invoice_buy_start');
            $invoice_buy_return_start = set_value('invoice_buy_return_start');
            $invoice_service_start = set_value('invoice_service_start');
            
            $NewData = array();
			$NewData = array(
                'ar_title' => $ar_title,
				'en_title' => $en_title,
				'person' => $person,
				'email' => $email,
				'phone' => $phone,
                'tax_number' => $tax_number,
				'amount' => $amount,
                'invoice_sale_start' => $invoice_sale_start,
                'invoice_sale_return_start' => $invoice_sale_return_start,
                'invoice_buy_start' => $invoice_buy_start,
                'invoice_buy_return_start' => $invoice_buy_return_start,
                'invoice_service_start' => $invoice_service_start,
			);
			

			$this->M_app_company->UpdateRecord($id, $NewData);

            $CompanyFolder = "branche/updateform/".$id;
            redirect ($CompanyFolder);
        }
    }

    public function change_header()
    {
        $this->load->helper('security');
        $this->load->library("form_validation");
        
        $this->form_validation->set_rules("header_id","header_id","required");
        
        if ($this->form_validation->run() == FALSE) {
        echo validation_errors();
        }
        else {
            $header_id = set_value('header_id');
            
            $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/settings/";
			$ImageDirectory = $UploadPath;
			
			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
			}
            
            $NewData = array();
			$NewData = array(
                'header' => $NewFileName,
			);
			
			$this->M_app_company->UpdateRecord($header_id, $NewData);

            $CompanyFolder = "branche/updateform/".$header_id;
            redirect ($CompanyFolder);
        }
    }
    
    public function change_footer()
    {
        $this->load->helper('security');
        $this->load->library("form_validation");
        
        $this->form_validation->set_rules("footer_id","footer_id","required");
        
        if ($this->form_validation->run() == FALSE) {
        echo validation_errors();
        }
        else {
            $footer_id = set_value('footer_id');
            
            $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/settings/";
			$ImageDirectory = $UploadPath;
			
			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
			}
            
            $NewData = array();
			$NewData = array(
                'footer' => $NewFileName,
			);
			
			$this->M_app_company->UpdateRecord($footer_id, $NewData);

            $CompanyFolder = "branche/updateform/".$footer_id;
            redirect ($CompanyFolder);
        }
    }

}
