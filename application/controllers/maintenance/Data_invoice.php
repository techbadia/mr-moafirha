<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_invoice extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }
        $this->load->model("sales/M_sale_customer");
		$this->load->model("maintenance/M_set_request_status");
		$this->load->model("maintenance/M_set_area");
		$this->load->model("maintenance/M_services");
		$this->load->model("maintenance/M_data_request_items");
		$this->load->model("maintenance/M_set_teamwork_schedule");
		$this->load->model("maintenance/M_data_invoice");
		$this->load->model("maintenance/M_data_invoice_items");
		$this->load->model("maintenance/M_set_teamwork_tasks");
		$this->load->model("maintenance/M_set_teamwork");
		$this->load->model("maintenance/M_data_request");
		$this->load->model("maintenance/M_data_invoice_expenses");
	}
	
	public function index()
	{
		$fromdate = date("Y-m-d");
		$todate = date("Y-m-d");
		$area_id = 0;
		$customer_id = 0;
		$teamwork_id = 0;
		$status_id = 0;

		$data['fromdate'] = $fromdate;
		$data['todate'] = $todate;
		$data['area_id'] = $area_id;
		$data['customer_id'] = $customer_id;
		$data['teamwork_id'] = $teamwork_id;
		$data['status_id'] = $status_id;
        
        $data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
		$data['Area'] = $this->M_set_area->GetMultiRow();
		$data['TeamWork'] = $this->M_set_teamwork->GetMultiRow();
        
		$data['DataRows'] = $this->M_data_invoice->GetMultiRow($customer_id, $teamwork_id, $status_id, $area_id, $fromdate, $todate);
		$data['content_page'] = "maintenance/data_invoice";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
		$data['Area'] = $this->M_set_area->GetMultiRow();
		$data['TeamWork'] = $this->M_set_teamwork->GetMultiRow();

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "maintenance/data_invoice_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		$this->load->helper('security');
		$this->load->library("form_validation");
		
		$this->form_validation->set_rules("ar_title","ar_title","required");
		$this->form_validation->set_rules("en_title","en_title","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
			$company_id = intval($this->session->userdata('company_id'));
			$user_id = intval($this->session->userdata('StaffID'));
			$affiliate_id = 0;
			$ar_title = set_value('ar_title');
			$en_title = set_value('en_title');
			$phone = set_value('phone');

			$GetCustomerData = $this->M_sale_customer->GetCustomerData($phone);
			$customer_id = $GetCustomerData->id;
            $status_id = set_value('status_id');
            $area_id = set_value('area_id');
            $source = 1;
            $details = set_value('details');
            $thedate = set_value('thedate');
            $thetime = set_value('thetime');
            $insert_date = date("Y-m-d");
            $insert_time = date("h:m");
			$deleted = 0;

			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'user_id' => $user_id,
				'affiliate_id' => $affiliate_id,
				'customer_id' => $customer_id,
				'status_id' => $status_id,
				'area_id' => $area_id,
				'source' => $source,
				'details' => $details,
                'thedate' => $thedate,
				'thetime' => $thetime,
				'insert_date' => $insert_date,
				'insert_time' => $insert_time,
				'deleted' => $deleted,
			);

			$this->M_data_invoice->InsertRecord($NewData);
			
			redirect ('maintenance/data_invoice');
		}
	}

	public function updateform($rid)
	{
		$data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
		$data['Area'] = $this->M_set_area->GetMultiRow();
		$data['TeamWork'] = $this->M_set_teamwork->GetMultiRow();

		$DataRow = $this->M_data_invoice->GetRow($rid);
        $data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['user_id'] = $DataRow->user_id;
		$data['affiliate_id'] = $DataRow->affiliate_id;
		$data['customer_id'] = $DataRow->customer_id;
		$data['status_id'] = $DataRow->status_id;
		$data['area_id'] = $DataRow->area_id;
		$data['source'] = $DataRow->source;
		$data['details'] = $DataRow->details;
		$data['thedate'] = $DataRow->thedate;
		$data['thetime'] = $DataRow->thetime;
		$data['insert_date'] = $DataRow->insert_date;
		$data['insert_time'] = $DataRow->insert_time;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "maintenance/data_invoice_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("status_id","status_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');

			$DataRow = $this->M_data_invoice->GetRow($id);


			$status_id = set_value('status_id');
			$closed_date = date("Y-m-d");
            $closed_time = date("h:m");
			$tree_id = set_value('tree_id');

			$NewData = array();

			if($status_id == 5)
			{
				$NewData = array(
					'status_id' => $status_id,
					'closed_date' => $closed_date,
					'closed_time' => $closed_time,
				);
			}
			else
			{
				$NewData = array(
					'status_id' => $status_id,
				);
			}

			$this->M_data_invoice->UpdateRecord($id, $NewData);

			/////////////////////////////////
			if($status_id == 5)
			{
				//M_data_invoice_items
				$qunatity = 0;
				$unit_price = 0;
				$totalvalue = 0;
				$TotalItems = $this->M_data_invoice_items->GetByRequest($id);
				foreach($TotalItems as $TotalItems_Row) {
					$qunatity = $TotalItems_Row->qunatity;
					$unit_price = $TotalItems_Row->unit_price;
					$totalvalue += $qunatity * $unit_price;
				}
				$details = $DataRow->details;
				$table_name = "data_invoice";
	
				$this->fin_journal_main_insert($details, $closed_date, $id, $table_name, $id, "");
				//من حساب الخزينة
				$account_id = $tree_id;
				$debit = $totalvalue;
				$creditor = 0;
				$this->fin_journal_insert($account_id, $debit, $creditor);
				//إلى حساب الخدمات
				$account_id = intval($this->session->userdata('acc_revenues'));
				$debit = 0;
				$creditor = $totalvalue;
				$this->fin_journal_insert($account_id, $debit, $creditor);
			}
			
			/////////////////////////////////
			
			redirect ('maintenance/data_invoice');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_data_invoice->UpdateRecord($rid, $NewData);

        redirect("maintenance/data_invoice");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_data_invoice->UpdateRecord($rid, $NewData);

        redirect("maintenance/data_invoice");
    }
    
    public function filter()
	{
        $this->form_validation->set_rules("fromdate","fromdate","required");
        if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$fromdate = set_value("fromdate");
			$todate = set_value("todate");
			$area_id = set_value("area_id");
			$customer_id = set_value("customer_id");
			$teamwork_id = 0;
			$status_id = set_value("status_id");
	
			$data['fromdate'] = $fromdate;
			$data['todate'] = $todate;
			$data['area_id'] = $area_id;
			$data['customer_id'] = $customer_id;
			$data['teamwork_id'] = $teamwork_id;
			$data['status_id'] = $status_id;
	
			$data['Customer'] = $this->M_sale_customer->GetMultiRow();
			$data['Status'] = $this->M_set_request_status->GetMultiRow();
        	$data['Area'] = $this->M_set_area->GetMultiRow();
			$data['TeamWork'] = $this->M_set_teamwork->GetMultiRow();

			$data['DataRows'] = $this->M_data_invoice->GetMultiRow($customer_id, $teamwork_id, $status_id, $area_id, $fromdate, $todate);
			$data['content_page'] = "maintenance/data_invoice";
			$this->load->view('page', $data);
		}
    }
    

	public function view_main()
	{
		$id = $this->input->get('id');
		$MainDataRow = $this->M_data_invoice->GetHead($id);
		echo json_encode($MainDataRow); 
    	exit();
	}

	public function view_details()
	{
		$id = $this->input->get('id');
		//$id = intval($this->session->userdata('Fin_journal_View_ID'));
		$DetailsRows = $this->M_data_invoice->GetDetails($id);
		echo json_encode($DetailsRows); 
		exit();
	}

	public function expenses($invoice_id)
	{
		$data['invoice_id'] = $invoice_id;
		$data['status_name'] = $this->lang->line('data_invoice');
		$data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
		$data['Area'] = $this->M_set_area->GetMultiRow();
		$data['Service'] = $this->M_services->GetMultiRow();
		
		$data['content_page'] = "maintenance/data_invoice_expenses_insert";
		$this->load->view('page', $data);
	}


	public function expenses_insert()
	{
		
	}
	
	public function fin_journal_main_insert($details, $thedate, $bill_id, $table_name, $bond_no, $image)
	{
		$company_id = intval($this->session->userdata('company_id'));
		$user_id = intval($this->session->userdata('StaffID'));
		$automatic = 1;
		$deleted = 0;

		$fin_journal_array = array();
		$fin_journal_array = array(
			'company_id' => $company_id,
			'user_id' => $user_id,
			'details' => $details,
			'thedate' => $thedate,
			'bill_id' => $bill_id,
			'table_name' => $table_name,
			'bond_no' => $bond_no,
			'image' => $image,
			'automatic' => $automatic,
			'deleted' => $deleted,
		);

		$this->M_fin_journal_main->InsertRecord($fin_journal_array);
	}

	public function fin_journal_insert($account_id, $debit, $creditor)
	{
		$company_id = intval($this->session->userdata('company_id'));
		$user_id = intval($this->session->userdata('StaffID'));

		$MainData = $this->M_fin_journal_main->GetLatestRecord($company_id, $user_id);
		$main_id = $MainData->id;

		$fin_journal_array = array();
		$fin_journal_array = array(
			'main_id' => $main_id,
			'account_id' => $account_id,
			'debit' => $debit,
			'creditor' => $creditor,
		);

		$this->M_fin_journal->InsertRecord($fin_journal_array);
	}

	
}
