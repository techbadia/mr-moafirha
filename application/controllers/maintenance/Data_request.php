<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_request extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
        }
        $this->load->model("sales/M_sale_customer");
		$this->load->model("maintenance/M_set_request_status");
		$this->load->model("maintenance/M_set_area");
		$this->load->model("maintenance/M_services");
		$this->load->model("maintenance/M_data_request_items");
		$this->load->model("maintenance/M_set_teamwork_schedule");
		$this->load->model("maintenance/M_data_invoice");
		$this->load->model("maintenance/M_data_invoice_items");
		$this->load->model("maintenance/M_set_teamwork_tasks");
		$this->load->model("maintenance/M_set_teamwork");
        $this->load->model("maintenance/M_data_request");
	}
	
	public function index()
	{
		$fromdate = date("Y-m-d");
		$todate = date("Y-m-d");
		$area_id = 0;
		$customer_id = 0;
		$status_id = 0;

		$data['fromdate'] = $fromdate;
		$data['todate'] = $todate;
		$data['area_id'] = $area_id;
		$data['customer_id'] = $customer_id;
		$data['status_id'] = $status_id;
        
        $data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
        $data['Area'] = $this->M_set_area->GetMultiRow();
        
		$data['DataRows'] = $this->M_data_request->GetMultiRow($customer_id, $status_id, $area_id, $fromdate, $todate);
		$data['content_page'] = "maintenance/data_request";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
		$data['Area'] = $this->M_set_area->GetMultiRow();
		
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "maintenance/data_request_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		$this->load->helper('security');
		$this->load->library("form_validation");
		
		$this->form_validation->set_rules("ar_title","ar_title","required");
		$this->form_validation->set_rules("en_title","en_title","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
			$company_id = intval($this->session->userdata('company_id'));
			$user_id = intval($this->session->userdata('StaffID'));
			$affiliate_id = 0;
			$ar_title = set_value('ar_title');
			$en_title = set_value('en_title');
			$phone = set_value('phone');

			$GetCustomerData = $this->M_sale_customer->GetCustomerData($phone);
			$customer_id = $GetCustomerData->id;
            $status_id = set_value('status_id');
            $area_id = set_value('area_id');
            $source = 1;
            $details = set_value('details');
            $thedate = set_value('thedate');
            $thetime = set_value('thetime');
            $insert_date = date("Y-m-d");
            $insert_time = date("h:m");
			$deleted = 0;

			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'user_id' => $user_id,
				'affiliate_id' => $affiliate_id,
				'customer_id' => $customer_id,
				'status_id' => $status_id,
				'area_id' => $area_id,
				'source' => $source,
				'details' => $details,
                'thedate' => $thedate,
				'thetime' => $thetime,
				'insert_date' => $insert_date,
				'insert_time' => $insert_time,
				'deleted' => $deleted,
			);

			$this->M_data_request->InsertRecord($NewData);

			$LatestRequest = $this->M_data_request->GetLatest($customer_id, $area_id, $thedate, $thetime);
			$request_id = $LatestRequest->id;

			foreach ($_REQUEST['ServiceList'] as $ServiceList)
            {
				$ServiceData = $this->M_services->GetRow($ServiceList);
				$unit_price = $ServiceData->price;
                $NewData = array();
				$NewData = array(
					'request_id' => $request_id,
					'service_id' => $ServiceList,
					'qunatity' => 1,
					'unit_price' => $unit_price,
				);
				$this->M_data_request_items->InsertRecord($NewData);
            }
			
			redirect ('maintenance/data_request');
		}
	}

	public function updateform($rid)
	{
		$data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
		$data['Area'] = $this->M_set_area->GetMultiRow();

		$DataRow = $this->M_data_request->GetRow($rid);
        $data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['user_id'] = $DataRow->user_id;
		$data['affiliate_id'] = $DataRow->affiliate_id;
		$data['customer_id'] = $DataRow->customer_id;
		$data['status_id'] = $DataRow->status_id;
		$data['area_id'] = $DataRow->area_id;
		$data['source'] = $DataRow->source;
		$data['details'] = $DataRow->details;
		$data['thedate'] = $DataRow->thedate;
		$data['thetime'] = $DataRow->thetime;
		$data['insert_date'] = $DataRow->insert_date;
		$data['insert_time'] = $DataRow->insert_time;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "maintenance/data_request_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("customer_id","customer_id","required");
		$this->form_validation->set_rules("status_id","status_id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
			$user_id = set_value('user_id');
            $affiliate_id = set_value('affiliate_id');
			$customer_id = set_value('customer_id');
			$status_id = set_value('status_id');
			$area_id = set_value('area_id');
			$source = set_value('source');
			$details = set_value('details');
			$thedate = set_value('thedate');
			$thetime = set_value('thetime');

			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
				'user_id' => $user_id,
                'affiliate_id' => $affiliate_id,
                'customer_id' => $customer_id,
				'status_id' => $status_id,
				'area_id' => $area_id,
				'source' => $source,
				'details' => $details,
				'thedate' => $thedate,
				'thetime' => $thetime,
			);
			
			$this->M_data_request->UpdateRecord($id, $NewData);

			$this->db->delete('data_request_items', array('request_id' => $id));
			$LatestRequest = $this->M_data_request->GetLatest($customer_id, $area_id, $thedate, $thetime);
			$request_id = $LatestRequest->id;

			foreach ($_REQUEST['ServiceList'] as $ServiceList)
            {
				$ServiceData = $this->M_services->GetRow($ServiceList);
				$unit_price = $ServiceData->price;
                $NewData = array();
				$NewData = array(
					'request_id' => $request_id,
					'service_id' => $ServiceList,
					'qunatity' => 1,
					'unit_price' => $unit_price,
				);
				$this->M_data_request_items->InsertRecord($NewData);
            }
			
			redirect ('maintenance/data_request');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_data_request->UpdateRecord($rid, $NewData);

        redirect("maintenance/data_request");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_data_request->UpdateRecord($rid, $NewData);

        redirect("maintenance/data_request");
    }
    
    public function filter()
	{
		$data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
        $data['Area'] = $this->M_set_area->GetMultiRow();
		
        $this->form_validation->set_rules("fromdate","fromdate","required");
        if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$fromdate = set_value("fromdate");
			$todate = set_value("todate");
			$area_id = set_value("area_id");
			$customer_id = set_value("customer_id");
			$status_id = set_value("status_id");
	
			$data['fromdate'] = $fromdate;
			$data['todate'] = $todate;
			$data['area_id'] = $area_id;
			$data['customer_id'] = $customer_id;
			$data['status_id'] = $status_id;
	
			$data['Customer'] = $this->M_sale_customer->GetMultiRow();
			$data['Status'] = $this->M_set_request_status->GetMultiRow();
        	$data['Area'] = $this->M_set_area->GetMultiRow();
        
			$data['DataRows'] = $this->M_data_request->GetMultiRow($customer_id, $status_id, $area_id, $fromdate, $todate);
			$data['content_page'] = "maintenance/data_request";
			$this->load->view('page', $data);
		}
    }
    
    public function convert($rid)
	{
		$this->session->set_userdata('Filter', $this->lang->line('data_request_convert'));

		$DataRow = $this->M_data_request->GetRow($rid);
        $data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
		$data['user_id'] = $DataRow->user_id;
		$data['customer_id'] = $DataRow->customer_id;
		$data['status_id'] = $DataRow->status_id;
		$data['area_id'] = $DataRow->area_id;
		$data['source'] = $DataRow->source;
		$data['details'] = $DataRow->details;
		$data['thedate'] = $DataRow->thedate;
		$data['thetime'] = $DataRow->thetime;
		$data['insert_date'] = $DataRow->insert_date;
		$data['insert_time'] = $DataRow->insert_time;
		$data['deleted'] = $DataRow->deleted;
		
				
		$data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
		$data['Area'] = $this->M_set_area->GetMultiRow();
		$data['Service'] = $this->M_services->GetMultiRow();
		$data['ControllerName'] = $this->router->fetch_class();
        $data['Teamwork'] = $this->M_set_teamwork->GetMultiRow();
        
        $data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "maintenance/data_request_convert";
		$this->load->view('page', $data);
    }
    
    public function convert_update()
	{
		$this->form_validation->set_rules("details","details","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = set_value("company_id");
			$user_id = intval($this->session->userdata('StaffID'));
			$customer_id = set_value("customer_id");
			$teamwork_id = set_value('teamwork_id');
			$status_id = set_value("status_id");
			$area_id = set_value("area_id");
			$source = set_value("source");
			$details = set_value("details");
			$thedate = set_value("thedate");
			$thetime = set_value("thetime");
			$insert_date = set_value("insert_date");
			$insert_time = set_value("insert_time");
			$deleted = set_value('deleted');
			$schedule = set_value('schedule');

			$Request_status_id = 2;
			$this->M_data_request->UpdateStatus($id, $Request_status_id);

			if($schedule > 0)
			{
				$this->load->model('M_set_teamwork_schedule');
				$CustomerData = $this->M_sale_customer->GetRow($customer_id);
				$fullname = $CustomerData->fullname;
				$address = $CustomerData->address;
				$phone = $CustomerData->phone;
				
				$this->M_set_teamwork_schedule->InsertRecord($teamwork_id, $fullname, $phone, $address, 
				$thedate, $schedule);
			}

			$InvoiceData = array();
			$InvoiceData = array(
				'company_id' => $company_id,
				'user_id' => $user_id,
				'customer_id' => $customer_id,
				'teamwork_id' => $teamwork_id,
				'status_id' => $status_id,
				'area_id' => $area_id,
				'source' => $source,
				'details' => $details,
				'thedate' => $thedate,
				'thetime' => $thetime,
				'insert_date' => $insert_date,
				'insert_time' => $insert_time,
				'schedule' => $schedule,
				'deleted' => $deleted,
			);

			$this->M_data_invoice->InsertRecord($InvoiceData);
			
			$LatestInvoice = $this->M_data_invoice->GetLatest($user_id, $customer_id, $teamwork_id);
			$LatestInvoiceID = $LatestInvoice->id;
			
			$this->load->model("M_data_request_items");
			$FindService = $this->M_data_request_items->GetByRequest($id);
			foreach($FindService as $FindService_Row) {
				//service_id, qunatity, unit_price
				$service_id = $FindService_Row->service_id;
				$qunatity = $FindService_Row->qunatity;
				$unit_price = $FindService_Row->unit_price;
				$this->M_data_invoice_items->InsertRecord($LatestInvoiceID, $service_id, $qunatity, $unit_price);
			}

			if(($status_id == 4) || ($status_id == 6))
			{
				$this->load->model("M_set_teamwork");
				$TeamWorkData = $this->M_set_teamwork->GetRow($teamwork_id);
				$TeamWork_Email = $TeamWorkData->email;

				$CustomerData = $this->M_sale_customer->GetRow($customer_id);
				$Customer_fullname = $CustomerData->fullname;
				$Customer_address = $CustomerData->address;
				$Customer_phone = $CustomerData->phone;

				$this->load->model("M_set_teamwork_tasks");
				$closed = 0;
				$this->M_set_teamwork_tasks->InsertRecord($teamwork_id, $LatestInvoiceID, $thedate, $thetime, $closed);

				$StaffID = intval($this->session->userdata('StaffID'));
				$UserData = $this->M_usr_users->GetRow($StaffID); 
				$company_id = $UserData->company_id;
				$CompanyData = $this->M_app_company->GetRow($company_id);
				$CompanyEmail = $CompanyData->email;

				$this->load->library("email");
				$config = Array(		
					'mailtype'  => 'html', 
					'charset'   => 'utf-8'
				);
				$this->email->initialize($config);
				$this->load->library("email", $config);
				$this->email->from($CompanyEmail);
				$this->email->to($TeamWork_Email);
				//$this->email->cc('lbi.egypt.tutorial@gmail.com');
				if($status_id == 4)
				{
					$this->email->subject("عملية صيانة جديدة");
				}
				else
				{
					$this->email->subject("تأجيل عملية صيانة");
				}
				
				$FullMessage = "بيانات العميل : ".$Customer_fullname."<br>";
				$FullMessage .= "عنوان العميل : ".$Customer_address."<br>";
				$FullMessage .= "تليفون العميل : ".$Customer_phone."<br>";
				$FullMessage .= "<br><hr><br>";
				$FullMessage .= "<strong>العمل المطلوب تنفيذه : </strong><br>".$details."<br>";
				$FullMessage .= "تاريخ التنفيذ المطلوب : ".$thedate."<br>";
				$FullMessage .= "وقت التنفيذ المطلوب : ".$thetime."<br>";
				$this->email->message($FullMessage);
				$this->email->send();
			}
                
            redirect("maintenance/data_invoice");
		}
	}

	public function GetArabicName($phone)
	{
		$RowsCount = $this->M_sale_customer->CheckPhone($phone);
		if($RowsCount > 0)
		{
			$CustomerFullName = $this->M_sale_customer->GetCustomerData($phone);
            echo $CustomerFullName->ar_title;
		}
		else
		{
			echo "";
		}
	}

	public function GetEnglishName($phone)
	{
		$RowsCount = $this->M_sale_customer->CheckPhone($phone);
		if($RowsCount > 0)
		{
			$CustomerFullName = $this->M_sale_customer->GetCustomerData($phone);
            echo $CustomerFullName->en_title;
		}
		else
		{
			echo "";
		}
	}

	public function GetMobile($phone)
	{
		$RowsCount = $this->M_sale_customer->CheckPhone($phone);
		if($RowsCount > 0)
		{
			$CustomerFullName = $this->M_sale_customer->GetCustomerData($phone);
            echo $CustomerFullName->mobile1;
		}
		else
		{
			echo "";
		}
	}

	public function GetWhatsApp($phone)
	{
		$RowsCount = $this->M_sale_customer->CheckPhone($phone);
		if($RowsCount > 0)
		{
			$CustomerFullName = $this->M_sale_customer->GetCustomerData($phone);
            echo $CustomerFullName->mobile2;
		}
		else
		{
			echo "";
		}
	}



	public function GetAddress($phone)
	{
		$RowsCount = $this->M_sale_customer->CheckPhone($phone);
		if($RowsCount > 0)
		{
			$CustomerFullName = $this->M_sale_customer->GetCustomerData($phone);
            echo $CustomerFullName->address;
		}
		else
		{
			echo "";
		}
	}

	public function GetEmail($phone)
	{
		$RowsCount = $this->M_sale_customer->CheckPhone($phone);
		if($RowsCount > 0)
		{
			$CustomerFullName = $this->M_sale_customer->GetCustomerData($phone);
            echo $CustomerFullName->email;
		}
		else
		{
			echo "";
		}
	}

	public function GetArea($phone)
	{
		$RowsCount = $this->M_sale_customer->CheckPhone($phone);
		if($RowsCount > 0)
		{
			$CustomerFullName = $this->M_sale_customer->GetCustomerData($phone);
            echo $CustomerFullName->area_id;
		}
		else
		{
			echo "";
		}
	}

	public function view($rid)
	{
		$DataRow = $this->M_data_request->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['user_id'] = $DataRow->user_id;
		$data['customer_id'] = $DataRow->customer_id;
		$data['status_id'] = $DataRow->status_id;
		$data['area_id'] = $DataRow->area_id;
		$data['source'] = $DataRow->source;
		$data['details'] = $DataRow->details;
		$data['thedate'] = $DataRow->thedate;
		$data['thetime'] = $DataRow->thetime;
		$data['insert_date'] = $DataRow->insert_date;
		$data['insert_time'] = $DataRow->insert_time;
		$data['deleted'] = $DataRow->deleted;

		$data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
		$data['Area'] = $this->M_set_area->GetMultiRow();
		$data['Service'] = $this->M_services->GetMultiRow();
		$data['ControllerName'] = $this->router->fetch_class();
		$data['FindService'] = $this->M_data_request_items->GetByRequest($rid);
		
		$this->load->view('data_request/data_request_view', $data);
	}

	public function view_main()
	{
		$id = $this->input->get('id');
		$MainDataRow = $this->M_data_request->GetHead($id);
		echo json_encode($MainDataRow); 
    	exit();
	}

	public function view_details()
	{
		$id = $this->input->get('id');
		//$id = intval($this->session->userdata('Fin_journal_View_ID'));
		$DetailsRows = $this->M_data_request->GetDetails($id);
		echo json_encode($DetailsRows); 
		exit();
	}

	public function area($area_id)
	{
		$fromdate = date("2021-01-01");
		$todate = date("Y-m-d");
		//$area_id = 0;
		$customer_id = 0;
		$status_id = 0;

		$RowData = $this->M_set_area->GetRow($area_id);
		if ($this->session->userdata('lang') == "ar")
		{
			$FilterName = $RowData->ar_title;
		}
		else
		{
			$FilterName = $RowData->en_title;
		}
		$this->session->set_userdata('Filter', $FilterName);
		
		$data['fromdate'] = $fromdate;
		$data['todate'] = $todate;
		$data['area_id'] = $area_id;
		$data['customer_id'] = $customer_id;
		$data['status_id'] = $status_id;
        
        $data['Customer'] = $this->M_sale_customer->GetMultiRow();
		$data['Status'] = $this->M_set_request_status->GetMultiRow();
        $data['Area'] = $this->M_set_area->GetMultiRow();
        
		$data['DataRows'] = $this->M_data_request->GetMultiRow($customer_id, $status_id, $area_id, $fromdate, $todate);
		$data['content_page'] = "maintenance/data_request";
		$this->load->view('page', $data);
	}
}
