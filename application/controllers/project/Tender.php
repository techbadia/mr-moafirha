<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tender extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
        $this->load->model("project/M_proj_status");
        $this->load->model("project/M_proj_project");
        $this->load->model("project/M_proj_project_files");
        $this->load->model("sales/M_sale_customer");
	}
	
	public function index()
	{
		$data['DataRows'] = $this->M_proj_project->GetMultiRow();
		$data['content_page'] = "project/tender";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "project/tender_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("ar_title","ar_title","required");
		$this->form_validation->set_rules("en_title","en_title","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $customer_id = set_value('customer_id');
            $status_id = set_value('status_id');
            $manager_id = set_value('manager_id');
			$ar_title = set_value('ar_title');
            $en_title = set_value('en_title');
            $ar_details = set_value('ar_details');
            $en_details = set_value('en_details');
            $start_date = set_value('start_date');
            $end_date = set_value('end_date');
            $labor_cost = set_value('labor_cost');
            $material_cost = set_value('material_cost');
            $total_cost = set_value('total_cost');
            $amount = set_value('amount');
			$deleted = set_value('deleted');

            $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/project/";
			$ImageDirectory = $UploadPath;
			
			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
			}

			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'customer_id' => $customer_id,
                'status_id' => $status_id,
                'manager_id' => $manager_id,
                'ar_title' => $ar_title,
				'en_title' => $en_title,
                'ar_details' => $ar_details,
                'en_details' => $en_details,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'labor_cost' => $labor_cost,
                'material_cost' => $material_cost,
                'total_cost' => $total_cost,
                'amount' => $amount,
                'image' => $NewFileName,
				'deleted' => $deleted,
			);
            $this->M_proj_project->InsertRecord($NewData);
            
            $LatestProject = $this->M_proj_project->GetLatest($customer_id, $status_id, $manager_id);
            $ProjectID = $LatestProject->id;
            $level_id = 0;
            $task_id = 0;

            $count = count($_FILES['files']['name']);
            for($i=0;$i<$count;$i++){
                if(!empty($_FILES['files']['name'][$i])){
                    $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                    $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                    $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                    $_FILES['file']['size'] = $_FILES['files']['size'][$i];
          
                    $config['upload_path'] = 'upload/project/'; 
                    $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx|xls|xlsx|csv|txt|rtf|html';
                    $config['max_size'] = '50000'; // max_size in kb
                    //$config['file_name'] = $_FILES['files']['name'][$i];
                    $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
                    $exts = explode(".", $_FILES['files']['name'][$i]);
                    $exts = end($exts);
                    $exts = strtolower($exts);
                    $config['file_name'] = $filename.".".$exts;
                    $config['file_ext_tolower'] = TRUE;
                    $config['remove_spaces'] = TRUE;

                    $this->load->library('upload',$config); 
                    $this->upload->initialize($config);
                    if($this->upload->do_upload('file')){
                        $uploadData = $this->upload->data();
                        $filename = $uploadData['file_name'];
            
                        $FileData = array();
                        $FileData = array(
                            'company_id' => $company_id,
                            'project_id' => $ProjectID,
                            'level_id' => $level_id,
                            'task_id' => $task_id,
                            'ar_title' => $ar_title,
                            'en_title' => $en_title,
                            'image' => $filename,
                        );
                        $this->M_proj_project_files->InsertRecord($FileData);
                    }
                }
            }


			redirect ('project/tender');
		}
    }
    
	public function updateform($rid)
	{
		$DataRow = $this->M_proj_project->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
        $data['customer_id'] = $DataRow->customer_id;
        $data['status_id'] = $DataRow->status_id;
        $data['manager_id'] = $DataRow->manager_id;
        $data['ar_title'] = $DataRow->ar_title;
		$data['en_title'] = $DataRow->en_title;
        $data['ar_details'] = $DataRow->ar_details;
        $data['en_details'] = $DataRow->en_details;
        $data['start_date'] = $DataRow->start_date;
        $data['end_date'] = $DataRow->end_date;
        $data['labor_cost'] = $DataRow->labor_cost;
        $data['material_cost'] = $DataRow->material_cost;
        $data['total_cost'] = $DataRow->total_cost;
        $data['amount'] = $DataRow->amount;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "project/tender_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("ar_title","ar_title","required");
		$this->form_validation->set_rules("en_title","en_title","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $customer_id = set_value('customer_id');
            $status_id = set_value('status_id');
            $manager_id = set_value('manager_id');
			$ar_title = set_value('ar_title');
            $en_title = set_value('en_title');
            $ar_details = set_value('ar_details');
            $en_details = set_value('en_details');
            $start_date = set_value('start_date');
            $end_date = set_value('end_date');
            $labor_cost = set_value('labor_cost');
            $material_cost = set_value('material_cost');
            $total_cost = set_value('total_cost');
            $amount = set_value('amount');
			$deleted = set_value('deleted');

			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'customer_id' => $customer_id,
                'status_id' => $status_id,
                'manager_id' => $manager_id,
                'ar_title' => $ar_title,
				'en_title' => $en_title,
                'ar_details' => $ar_details,
                'en_details' => $en_details,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'labor_cost' => $labor_cost,
                'material_cost' => $material_cost,
                'total_cost' => $total_cost,
                'amount' => $amount,
				'deleted' => $deleted,
			);
			

			$this->M_proj_project->UpdateRecord($id, $NewData);
			
			redirect ('project/tender');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_proj_project->UpdateRecord($rid, $NewData);

        redirect("project/tender");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_proj_project->UpdateRecord($rid, $NewData);

        redirect("project/tender");
    }
    
    public function convert($rid)
	{
		$DataRow = $this->M_proj_project->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
        $data['customer_id'] = $DataRow->customer_id;
        $data['status_id'] = $DataRow->status_id;
        $data['manager_id'] = $DataRow->manager_id;
        $data['ar_title'] = $DataRow->ar_title;
		$data['en_title'] = $DataRow->en_title;
        $data['ar_details'] = $DataRow->ar_details;
        $data['en_details'] = $DataRow->en_details;
        $data['start_date'] = $DataRow->start_date;
        $data['end_date'] = $DataRow->end_date;
        $data['labor_cost'] = $DataRow->labor_cost;
        $data['material_cost'] = $DataRow->material_cost;
        $data['total_cost'] = $DataRow->total_cost;
        $data['amount'] = $DataRow->amount;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "project/tender_convert";
		$this->load->view('page', $data);
		
    }
    
    public function convert_Data()
	{
		$this->form_validation->set_rules("ar_title","ar_title","required");
		$this->form_validation->set_rules("en_title","en_title","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $customer_id = set_value('customer_id');
            $status_id = set_value('status_id');
            $manager_id = set_value('manager_id');
			$ar_title = set_value('ar_title');
            $en_title = set_value('en_title');
            $ar_details = set_value('ar_details');
            $en_details = set_value('en_details');
            $start_date = set_value('start_date');
            $end_date = set_value('end_date');
            $labor_cost = set_value('labor_cost');
            $material_cost = set_value('material_cost');
            $total_cost = set_value('total_cost');
            $amount = set_value('amount');
			$deleted = set_value('deleted');

			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'customer_id' => $customer_id,
                'status_id' => $status_id,
                'manager_id' => $manager_id,
                'ar_title' => $ar_title,
				'en_title' => $en_title,
                'ar_details' => $ar_details,
                'en_details' => $en_details,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'labor_cost' => $labor_cost,
                'material_cost' => $material_cost,
                'total_cost' => $total_cost,
                'amount' => $amount,
				'deleted' => $deleted,
			);
			

			$this->M_proj_project->UpdateRecord($id, $NewData);
			
			redirect ('project/proj_project');
		}
	}
}
