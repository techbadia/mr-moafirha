<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proj_project_level_task extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
        
        $this->load->model("project/M_proj_project");
        $this->load->model("project/M_proj_project_level");
        $this->load->model("project/M_proj_project_level_task");
        $this->load->model("project/M_proj_status");
        
	}
	
	public function index()
	{
		$data['DataRows'] = $this->M_proj_project_level_task->GetMultiRow();
		$data['content_page'] = "project/proj_project_level_task";
		$this->load->view('page', $data);
	}

    public function project($project_id)
    {
        $data['DataRows'] = $this->M_proj_project_level_task->GetForProject($project_id);
		$data['content_page'] = "project/proj_project_level_task";
		$this->load->view('page', $data);
    }

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "project/proj_project_level_task_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("ar_details","ar_details","required");
		$this->form_validation->set_rules("en_details","en_details","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $project_id = set_value('project_id');
            $level_id = set_value('level_id');
            $user_id = set_value('user_id');
            $status_id = set_value('status_id');
			$ar_details = set_value('ar_details');
            $en_details = set_value('en_details');
            $start_date = set_value('start_date');
            $end_date = set_value('end_date');

            $deleted = 0;
			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'project_id' => $project_id,
                'level_id' => $level_id,
                'user_id' => $user_id,
                'status_id' => $status_id,
                'ar_details' => $ar_details,
				'en_details' => $en_details,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'deleted' => $deleted,
			);
            $this->M_proj_project_level_task->InsertRecord($NewData);

			redirect ('project/proj_project_level_task');
		}
    }
    
	public function updateform($rid)
	{
		$DataRow = $this->M_proj_project_level_task->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
        $data['project_id'] = $DataRow->project_id;
        $data['level_id'] = $DataRow->level_id;
        $data['user_id'] = $DataRow->user_id;
        $data['status_id'] = $DataRow->status_id;
        $data['ar_details'] = $DataRow->ar_details;
        $data['en_details'] = $DataRow->en_details;
        $data['start_date'] = $DataRow->start_date;
        $data['end_date'] = $DataRow->end_date;
        $data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "project/proj_project_level_task_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("ar_details","ar_details","required");
		$this->form_validation->set_rules("en_details","en_details","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $project_id = set_value('project_id');
            $level_id = set_value('level_id');
            $user_id = set_value('user_id');
            $status_id = set_value('status_id');
			$ar_details = set_value('ar_details');
            $en_details = set_value('en_details');
            $start_date = set_value('start_date');
            $end_date = set_value('end_date');

			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'project_id' => $project_id,
                'level_id' => $level_id,
                'user_id' => $user_id,
                'status_id' => $status_id,
                'ar_details' => $ar_details,
				'en_details' => $en_details,
                'start_date' => $start_date,
                'end_date' => $end_date,
			);
			

			$this->M_proj_project_level_task->UpdateRecord($id, $NewData);
			
			redirect ('project/proj_project_level_task');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_proj_project_level_task->UpdateRecord($rid, $NewData);

        redirect("project/proj_project_level_task");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_proj_project_level_task->UpdateRecord($rid, $NewData);

        redirect("project/proj_project_level_task");
    }
    
}
