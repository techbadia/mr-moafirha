<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expense_items extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();

		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}

        $this->load->model("project/M_proj_project");
        $this->load->model("project/M_proj_project_level");
        $this->load->model("project/M_proj_project_level_task");
        $this->load->model("project/M_proj_status");
        $this->load->model("project/M_expense_items");

	}

	public function index()
	{

				$data['DataRows'] = $this->M_expense_items->GetAll();
		$data['content_page'] = "project/expense_items";
		$this->load->view('page', $data);
	}

    public function project($project_id)
    {
        $data['DataRows'] = $this->M_proj_project_level->GetForProject($project_id);
		$data['content_page'] = "project/proj_project_level";
		$this->load->view('page', $data);
    }

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "project/expense_items_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		$this->form_validation->set_rules("item_name","item_name","required");
		$this->form_validation->set_rules("item_name_en","item_name_en","required");
	if ($this->form_validation->run() == FALSE) {
					//$this->load->view('index', $data);
					echo validation_errors();
	}
	else
	{
					$item_name_en = set_value('item_name_en');
					$item_name = set_value('item_name');




		$NewData = array();
		$NewData = array(
							'item_name' => $item_name,
							'item_name_en' => $item_name_en,

		);
					$sub_id = $this->M_expense_items->InsertRecord($NewData);
					// var_dump($_POST);die();
					redirect ("/project/expense_items");
	}
    }

	public function updateform($rid)
	{
		$DataRow = $this->M_expense_items->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['item_name'] = $DataRow->item_name;
        $data['item_name_en'] = $DataRow->item_name_en;


		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "project/updateformexpense_items";
		$this->load->view('page', $data);

	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("item_name","item_name","required");
		$this->form_validation->set_rules("item_name_en","item_name_en","required");
	if ($this->form_validation->run() == FALSE) {
					//$this->load->view('index', $data);
					echo validation_errors();
	}
	else
	{
					$item_name_en = set_value('item_name_en');
					$item_name = set_value('item_name');


					$id = set_value('id');


		$NewData = array();
		$NewData = array(
							'item_name' => $item_name,
							'item_name_en' => $item_name_en,

		);
					$sub_id = $this->M_expense_items->UpdateRecord($id, $NewData);
					// var_dump($_POST);die();
					redirect ("/project/expense_items");
	}
	}

	public function delete($rid)
	{	$this->M_expense_items->delete($rid);

        redirect("project/expense_items");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_proj_project_level->UpdateRecord($rid, $NewData);

        redirect("project/proj_project_level");
    }

}
