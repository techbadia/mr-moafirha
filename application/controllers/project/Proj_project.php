<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proj_project extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();

		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
        $this->load->model("project/M_proj_status");
        $this->load->model("project/M_proj_project");
        $this->load->model("project/M_proj_project_level_task");
        $this->load->model("project/M_proj_project_devices");
        $this->load->model("project/M_proj_sub_projects");
        $this->load->model("project/M_expense_items");

        $this->load->model("sales/M_sale_customer");
        $this->load->model("account/M_fin_treeaccount");
        $this->load->model("account/M_fin_journal_main");
        $this->load->model("account/M_fin_journal");
        $this->load->model("service/M_serv_action");


	}

	public function index()
	{
		$data['DataRows'] = $this->M_proj_project->GetMultiRow();
		$data['content_page'] = "project/proj_project";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['items'] = 			$this->M_expense_items->GetAll();

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "project/proj_project_insert";
		$this->load->view('page', $data);
	}
	public function expense_items_insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "project/expense_items_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("ar_title","ar_title","required");
		$this->form_validation->set_rules("en_title","en_title","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $customer_id = set_value('customer_id');
            $status_id = set_value('status_id');
            $manager_id = set_value('manager_id');
			$ar_title = set_value('ar_title');
            $en_title = set_value('en_title');
            $ar_details = set_value('ar_details');
            $en_details = set_value('en_details');
            $start_date = set_value('start_date');
            $end_date = set_value('end_date');
            $labor_cost = set_value('labor_cost');
            $material_cost = set_value('material_cost');
            $total_cost = set_value('total_cost');
            $amount = set_value('amount');
			$deleted = set_value('deleted');

            $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/project/";
			$ImageDirectory = $UploadPath;

			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
			}
			$text = "";
			// var_dump($_POST["items"]);die();
			if(!empty($_POST["items"]))
			{
				for ($i=0; $i < count($_POST["items"]); $i++) {
					$text .= $_POST["items"][$i]."_".$_POST["amounts"][$i]."|";
				}
			}
			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'customer_id' => $customer_id,
                'status_id' => $status_id,
                'manager_id' => $manager_id,
                'ar_title' => $ar_title,
				'en_title' => $en_title,
                'ar_details' => $ar_details,
                'en_details' => $en_details,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'labor_cost' => $labor_cost,
                'material_cost' => $material_cost,
                'total_cost' => $total_cost,
                'amount' => $amount,
                'image' => $NewFileName,
                'item_amount' => $text,
				'deleted' => $deleted,
			);
            $this->M_proj_project->InsertRecord($NewData);


            $LatestProject = $this->M_proj_project->GetLatest($customer_id, $status_id, $manager_id);
            $ProjectID = $LatestProject->id;
            ////////////////////// Add Project in Account Tree
            $category_id = 1; //
            $parent_id = $customer_id; //
            $account_no = $customer_id.$ProjectID;
            $thevalue = 0;

            $TreeAccount_array = array();
            $TreeAccount_array = array(
                'category_id' => 1,
                'parent_id' => $parent_id,
                'account_no' => $account_no,
                'title' => $ar_title,
                'title_en' => $en_title,
                'startamount' => $thevalue,
                'basic' => 0,
                'level_no' => 4,
                'deleted' => 0,
            );

            $this->M_fin_treeaccount->InsertRecord($TreeAccount_array);
            $FindAccountData = $this->M_fin_treeaccount->FindLatestRow($ar_title, $en_title);
            $AccountID = $FindAccountData->id;

            $ProjectAccountData = array();
			$ProjectAccountData = array(
                'fin_treeaccount_id' => $AccountID,
			);
            $this->M_proj_project->UpdateRecord($ProjectID, $ProjectAccountData);
            //////////////////////
            $level_id = 0;
            $task_id = 0;

            $count = count($_FILES['files']['name']);
            for($i=0;$i<$count;$i++){
                if(!empty($_FILES['files']['name'][$i])){
                    $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                    $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                    $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                    $_FILES['file']['size'] = $_FILES['files']['size'][$i];

                    $config['upload_path'] = 'upload/project/';
                    $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx|xls|xlsx|csv|txt|rtf|html';
                    $config['max_size'] = '50000'; // max_size in kb
                    //$config['file_name'] = $_FILES['files']['name'][$i];
                    $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
                    $exts = explode(".", $_FILES['files']['name'][$i]);
                    $exts = end($exts);
                    $exts = strtolower($exts);
                    $config['file_name'] = $filename.".".$exts;
                    $config['file_ext_tolower'] = TRUE;
                    $config['remove_spaces'] = TRUE;

                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if($this->upload->do_upload('file')){
                        $uploadData = $this->upload->data();
                        $filename = $uploadData['file_name'];

                        $FileData = array();
                        $FileData = array(
                            'company_id' => $company_id,
                            'project_id' => $ProjectID,
                            'level_id' => $level_id,
                            'task_id' => $task_id,
                            'ar_title' => $ar_title,
                            'en_title' => $en_title,
                            'image' => $filename,
                        );
                        $this->M_proj_project_files->InsertRecord($FileData);
                    }
                }
            }

			redirect ('project/proj_project');
		}
    }

	public function updateform($rid)
	{
		$DataRow = $this->M_proj_project->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
        $data['customer_id'] = $DataRow->customer_id;
        $data['status_id'] = $DataRow->status_id;
        $data['manager_id'] = $DataRow->manager_id;
        $data['ar_title'] = $DataRow->ar_title;
		$data['en_title'] = $DataRow->en_title;
        $data['ar_details'] = $DataRow->ar_details;
        $data['en_details'] = $DataRow->en_details;
        $data['start_date'] = $DataRow->start_date;
        $data['end_date'] = $DataRow->end_date;
        $data['labor_cost'] = $DataRow->labor_cost;
        $data['material_cost'] = $DataRow->material_cost;
        $data['total_cost'] = $DataRow->total_cost;
        $data['amount'] = $DataRow->amount;
		$data['deleted'] = $DataRow->deleted;
		$data['items'] = 			$this->M_expense_items->GetAll();
		$data['item_amount'] = $DataRow->item_amount;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "project/proj_project_update";
		$this->load->view('page', $data);

	}
	public function updateformSupProj($rid)
	{
		$DataRow = $this->M_proj_sub_projects->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['sup_proj_name'] = $DataRow->sup_proj_name;
        $data['project_id'] = $DataRow->project_id;
        $data['details'] = $DataRow->details;
        $data['date_start'] = $DataRow->date_start;
        $data['date_end'] = $DataRow->date_end;
		$data['total'] = $DataRow->total;
        $data['paid'] = $DataRow->paid;
        $data['remain'] = $DataRow->remain;
        $data['customer_type'] = $DataRow->customer_type;
        $data['customer_name'] = $DataRow->customer_name;
        $data['customer_id_number'] = $DataRow->customer_id_number;
        $data['customer_national'] = $DataRow->customer_national;
        $data['customer_address'] = $DataRow->customer_address;
        $data['customer_mobile'] = $DataRow->customer_mobile;
        $data['closed'] = $DataRow->closed;
        $data['item_amount'] = $DataRow->item_amount;
        $data['files'] = 			$this->M_proj_sub_projects->GetForSubProject($DataRow->id);
        $data['items'] = 			$this->M_expense_items->GetAll();

		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "project/proj_sub_project_update";
		$this->load->view('page', $data);

	}
	public function updateformexpense_items($rid)
	{
		$DataRow = $this->M_expense_items->GetRow($rid);
		$data['id'] = $DataRow->id;
        $data['item_name'] = $DataRow->item_name;
        $data['item_name_en'] = $DataRow->item_name_en;


		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "project/updateformexpense_items";
		$this->load->view('page', $data);

	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("ar_title","ar_title","required");
		$this->form_validation->set_rules("en_title","en_title","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $company_id = intval($this->session->userdata('company_id'));
            $customer_id = set_value('customer_id');
            $status_id = set_value('status_id');
            $manager_id = set_value('manager_id');
			$ar_title = set_value('ar_title');
            $en_title = set_value('en_title');
            $ar_details = set_value('ar_details');
            $en_details = set_value('en_details');
            $start_date = set_value('start_date');
            $end_date = set_value('end_date');
            $labor_cost = set_value('labor_cost');
            $material_cost = set_value('material_cost');
            $total_cost = set_value('total_cost');
            $amount = set_value('amount');
			$deleted = set_value('deleted');
			$text = "";
			// var_dump($_POST["items"]);die();
			if(!empty($_POST["items"]))
			{
				for ($i=0; $i < count($_POST["items"]); $i++) {
					$text .= $_POST["items"][$i]."_".$_POST["amounts"][$i]."|";
				}
			}
			$NewData = array();
			$NewData = array(
                'company_id' => $company_id,
                'customer_id' => $customer_id,
                'status_id' => $status_id,
                'manager_id' => $manager_id,
                'ar_title' => $ar_title,
				'en_title' => $en_title,
                'ar_details' => $ar_details,
                'en_details' => $en_details,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'labor_cost' => $labor_cost,
                'material_cost' => $material_cost,
                'total_cost' => $total_cost,
                'amount' => $amount,
				'deleted' => $deleted,
				'item_amount' => "|".$text,
			);


			$this->M_proj_project->UpdateRecord($id, $NewData);

			redirect ('project/proj_project');
		}
	}
	public function Update_Data_sup()
	{
		$this->form_validation->set_rules("project_id","project_id","required");
$this->form_validation->set_rules("sup_proj_name","sup_proj_name","required");

if ($this->form_validation->run() == FALSE) {
				//$this->load->view('index', $data);
				echo validation_errors();
}
else
{
				$id = set_value('id');
				$project_id = set_value('project_id');
				$sup_proj_name = set_value('sup_proj_name');
				$date_start = set_value('date_start');
				$date_end = set_value('date_end');
	$details = set_value('details');
				$total = set_value('total');
				$paid = set_value('paid');
				$remain = set_value('remain');
				$customer_id_number = set_value('customer_id_number');
				$customer_name = set_value('customer_name');
				$customer_type = set_value('customer_type');
				$customer_national = set_value('customer_national');
				$customer_address = set_value('customer_address');
				$customer_mobile = set_value('customer_mobile');
				$closed = set_value('closed');



				$ProjectData = $this->M_proj_project->GetRow($project_id);
				$customer_id = $ProjectData->customer_id;
				$text = "";
				// var_dump($_POST["items"]);die();
				if(!empty($_POST["items"]))
				{
					for ($i=0; $i < count($_POST["items"]); $i++) {
						$text .= $_POST["items"][$i]."_".$_POST["amount"][$i]."|";
					}
				}
	$NewData = array();
	$NewData = array(
						'sup_proj_name' => $sup_proj_name,
						'customer_name' => $customer_name,
						'customer_type' => $customer_type,
						'customer_id_number' => $customer_id_number,
						'customer_national' => $customer_national,
						'customer_address' => $customer_address,
						'customer_mobile' => $customer_mobile,
						'project_id' => $project_id,
						'date_start' => $date_start,
						'details' => $details,
						'date_end' => $date_end,
						'total' => $total,
						'paid' => $paid,
						'closed' => $closed,
						'item_amount' => "|".$text,
						'remain' => $remain,
	);
				// $sub_id = $this->M_proj_sub_projects->UpdateRecord($NewData);
				// var_dump($NewData);die();/
			$this->M_proj_sub_projects->UpdateRecord($id, $NewData);
			$count = count($_FILES['files']['name']);
			for($i=0;$i<$count;$i++){
					if(!empty($_FILES['files']['name'][$i])){
							$_FILES['file']['name'] = $_FILES['files']['name'][$i];
							$_FILES['file']['type'] = $_FILES['files']['type'][$i];
							$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
							$_FILES['file']['error'] = $_FILES['files']['error'][$i];
							$_FILES['file']['size'] = $_FILES['files']['size'][$i];

							$config['upload_path'] = 'upload/project/';
							$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx|xls|xlsx|csv|txt|rtf|html';
							$config['max_size'] = '50000'; // max_size in kb
							//$config['file_name'] = $_FILES['files']['name'][$i];
							$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
							$exts = explode(".", $_FILES['files']['name'][$i]);
							$exts = end($exts);
							$exts = strtolower($exts);
							$config['file_name'] = $filename.".".$exts;
							$config['file_ext_tolower'] = TRUE;
							$config['remove_spaces'] = TRUE;

							$this->load->library('upload',$config);
							$this->upload->initialize($config);
							if($this->upload->do_upload('file')){
									$uploadData = $this->upload->data();
									$filename = $uploadData['file_name'];
									$company_id = intval($this->session->userdata('company_id'));

									$FileData = array();
									$FileData = array(
											'company_id' => $company_id,
											'project_id' => $project_id,
											'sub_project_id' => $id,
											// 'level_id' => $level_id,
											// 'task_id' => $task_id,
											// 'ar_title' => $ar_title,
											// 'en_title' => $en_title,
											'image' => $filename,
									);
									$this->M_proj_project_files->InsertRecord($FileData);
							}
					}
			}
			redirect ("/project/proj_project/proj_sub_projects/".$project_id);
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_proj_project->UpdateRecord($rid, $NewData);

        redirect("project/proj_project");
	}
	public function hardDelete($rid)
	{


		$this->M_proj_project->delete($rid);

        redirect("project/proj_project");
	}
	public function deleteexpense_items($rid)
	{

		$this->M_expense_items->delete($rid);

        redirect("project/proj_project/expense_items");
	}
	public function deleteSupProj($rid)
	{
		$DataRow = $this->M_proj_sub_projects->GetRow($rid);
		// var_dump($DataRow);die();
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_proj_sub_projects->UpdateRecord($rid, $NewData);
		redirect ("/project/proj_project/proj_sub_projects/".$DataRow->project_id);

        // redirect($_SERVER["HTTP_REFERER"]);
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_proj_project->UpdateRecord($rid, $NewData);

        redirect("project/proj_project");
    }

    public function view_project($rid)
	{
		$DataRow = $this->M_proj_project->GetRow($rid);
		$total_sup_expenses = 0;
		$total_expenses = $DataRow->total_cost;
		$amount_item = explode("|",trim($DataRow->item_amount,"|"));
		foreach ($amount_item as $item) {
			$item = explode("_",$item);
			$total_expenses += $item[1];
		}
		$data['project_id'] = $DataRow->id;
        $data['company_id'] = $DataRow->company_id;
        $data['customer_id'] = $DataRow->customer_id;
        $data['status_id'] = $DataRow->status_id;
        $data['manager_id'] = $DataRow->manager_id;
        $data['ar_title'] = $DataRow->ar_title;
		$data['en_title'] = $DataRow->en_title;
        $data['ar_details'] = $DataRow->ar_details;
        $data['en_details'] = $DataRow->en_details;
        $data['start_date'] = $DataRow->start_date;
        $data['end_date'] = $DataRow->end_date;
        $data['labor_cost'] = $DataRow->labor_cost;
        $data['material_cost'] = $DataRow->material_cost;
        $data['total_cost'] = $DataRow->total_cost;
        $data['amount'] = $DataRow->amount;
        $data['image'] = $DataRow->image;
        $data['deleted'] = $DataRow->deleted;
        $data['fin_treeaccount_id'] = $DataRow->fin_treeaccount_id;

        $customer_id = $DataRow->customer_id;
        $CustomerData = $this->M_sale_customer->GetRow($customer_id);
        $data['Customer_ar_title'] = $CustomerData->ar_title;
        $data['Customer_en_title'] = $CustomerData->en_title;
        $data['Customer_phone'] = $CustomerData->phone;
        $data['Customer_mobile1'] = $CustomerData->mobile1;
        $data['Customer_email'] = $CustomerData->email;

        $manager_id = $DataRow->manager_id;
        $ManagerData = $this->M_usr_users->GetRow($manager_id);
        $data['Manager'] = $ManagerData->fullname;

        $status_id = $DataRow->status_id;
        $StatusData = $this->M_proj_status->GetRow($status_id);
        $data['Status_ar_title'] = $StatusData->ar_title;
        $data['Status_en_title'] = $StatusData->en_title;
				$subs = $this->M_proj_sub_projects->GetByProject($rid);
				// var_dump($subs);die();
				$paid = 0;
				$total_sup_expenses = 0;
				foreach ($subs as $sub) {
					$paid += $sub->paid;
					$amount_item = explode("|",trim($sub->item_amount,"|"));
					foreach ($amount_item as $item) {
						$item = explode("_",$item);
						$total_sup_expenses += $item[1];
					}
					 $sub->total_sup_expenses += $item[1];
				}
				$data['subs'] = $subs;
				$data['total_paid'] = $paid;
				$data['total_sup_expenses'] = $total_sup_expenses;
				$data['total_expenses'] = $total_expenses;
				$data['amount'] = $DataRow->amount;
				$data['ControllerName'] = $this->router->fetch_class();
				$data['content_page'] = "project/proj_project_view";
				$this->load->view('page', $data);
    }

    public function earning($rid)
    {
        $project_id = $rid;
        $DataRow = $this->M_proj_project->GetRow($rid);
        $data['ar_title'] = $DataRow->ar_title;
        $data['en_title'] = $DataRow->en_title;

        $treasury_id = intval($this->session->userdata('acc_treasury')); //حساب الخزينة
        $bank_id = intval($this->session->userdata('acc_bank')); // حساب البنك

        $data['DataRows'] = $this->M_fin_journal_main->GetAccountAction($project_id, $treasury_id, $bank_id);
		$data['content_page'] = "project/earning";
		$this->load->view('page', $data);
    }

    public function expenses($rid)
    {
        $project_id = $rid;
        $DataRow = $this->M_proj_project->GetRow($rid);
        $data['ar_title'] = $DataRow->ar_title;
		$data['en_title'] = $DataRow->en_title;
        $fin_treeaccount_id = intval($this->session->userdata('acc_operating_expenses'));

        $data['DataRows'] = $this->M_fin_journal_main->GetProject_expenses($project_id, $fin_treeaccount_id);
		$data['content_page'] = "project/expenses";
		$this->load->view('page', $data);
    }

    public function labor($rid)
    {
        $project_id = $rid;
        $DataRow = $this->M_proj_project->GetRow($rid);
        $data['ar_title'] = $DataRow->ar_title;
		$data['en_title'] = $DataRow->en_title;
        $fin_treeaccount_id = intval($this->session->userdata('acc_salary'));

        $data['DataRows'] = $this->M_fin_journal_main->GetProject_expenses($project_id, $fin_treeaccount_id);
		$data['content_page'] = "project/labor";
		$this->load->view('page', $data);
    }
    //material
    public function material($rid)
    {
        $project_id = $rid;
        redirect("sales/sale_bill/forproject/".$project_id);
    }

    public function proj_project_devices($rid)
    {
        $data['project_id'] = $rid;
        $data['DataRows'] = $this->M_proj_project_devices->GetByProject($rid);
		$data['content_page'] = "project/proj_project_devices";
		$this->load->view('page', $data);
    }
    public function proj_sub_projects($rid)
    {
        $data['project_id'] = $rid;
        $data['DataRows'] = $this->M_proj_sub_projects->GetByProject($rid);
		$data['content_page'] = "project/proj_sub_projects";
		$this->load->view('page', $data);
    }
    public function expense_items()
    {
        $data['DataRows'] = $this->M_expense_items->GetAll();
		$data['content_page'] = "project/expense_items";
		$this->load->view('page', $data);
    }

    public function proj_project_devices_tree($rid)
    {
        $data['project_id'] = $rid;
        $data['DataRows'] = $this->M_proj_project_devices->GetByProject($rid);
		$data['content_page'] = "project/proj_project_devices_tree";
		$this->load->view('page', $data);
    }
    public function proj_sub_projects_tree($rid)
    {
        $data['project_id'] = $rid;
        $data['DataRows'] = $this->M_proj_sub_projects->GetByProject($rid);
		$data['content_page'] = "project/proj_sub_projects_tree";
		$this->load->view('page', $data);
    }

    public function devices_insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "project/proj_project_devices_insert";
		$this->load->view('page', $data);
	}
    public function sup_project_insertform()
	{
		$data['items'] = 			$this->M_expense_items->GetAll();

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "project/proj_sup_projects_insert";
		$this->load->view('page', $data);
	}

    public function devices_insert_data()
    {
        $this->form_validation->set_rules("project_id","project_id","required");
		$this->form_validation->set_rules("barcode","barcode","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $project_id = set_value('project_id');
            $barcode = set_value('barcode');
            $plugin_date = set_value('plugin_date');
			$details = set_value('details');
            $building_no = set_value('building_no');
            $floor_no = set_value('floor_no');
            $room_no = set_value('room_no');

            $filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/project_devices/";
			$ImageDirectory = $UploadPath;

			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
			}

            $ProjectData = $this->M_proj_project->GetRow($project_id);
            $customer_id = $ProjectData->customer_id;

			$NewData = array();
			$NewData = array(
                'customer_id' => $customer_id,
                'project_id' => $project_id,
                'barcode' => $barcode,
                'details' => $details,
                'plugin_date' => $plugin_date,
				'building_no' => $building_no,
                'floor_no' => $floor_no,
                'room_no' => $room_no,
                'image' => $NewFileName,
			);
            $this->M_proj_project_devices->InsertRecord($NewData);

			redirect ('project/proj_project');
		}
    }
    public function sub_project_insert_data()
    {
        $this->form_validation->set_rules("project_id","project_id","required");
		$this->form_validation->set_rules("sup_proj_name","sup_proj_name","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $id = set_value('id');
            $project_id = set_value('project_id');
            $closed = set_value('closed');
            $sup_proj_name = set_value('sup_proj_name');
            $date_start = set_value('date_start');
            $date_end = set_value('date_end');
			$details = set_value('details');
            $total = set_value('total');
            $paid = set_value('paid');
            $remain = set_value('remain');
            $customer_id_number = set_value('customer_id_number');
            $customer_name = set_value('customer_name');
            $customer_type = set_value('customer_type');
            $customer_national = set_value('customer_national');
            $customer_address = set_value('customer_address');
            $customer_mobile = set_value('customer_mobile');



            $ProjectData = $this->M_proj_project->GetRow($project_id);
            $customer_id = $ProjectData->customer_id;
						// var_dump($_POST["items"]);die();
						if(!empty($_POST["items"]))
						{
							for ($i=0; $i < count($_POST["items"]); $i++) {
								$text .= $_POST["items"][$i]."_".$_POST["amount"][$i]."|";
							}
						}
			$NewData = array();
			$NewData = array(
                'sup_proj_name' => $sup_proj_name,
                'closed' => $closed,
                'customer_name' => $customer_name,
                'customer_type' => $customer_type,
                'customer_id_number' => $customer_id_number,
                'customer_national' => $customer_national,
                'customer_address' => $customer_address,
                'customer_mobile' => $customer_mobile,
                'project_id' => $project_id,
                'date_start' => $date_start,
                'details' => $details,
                'date_end' => $date_end,
								'total' => $total,
                'paid' => $paid,
                'item_amount' => $text,
                'remain' => $remain,
			);
            $sub_id = $this->M_proj_sub_projects->InsertRecord($NewData);
						$count = count($_FILES['files']['name']);
						for($i=0;$i<$count;$i++){
								if(!empty($_FILES['files']['name'][$i])){
										$_FILES['file']['name'] = $_FILES['files']['name'][$i];
										$_FILES['file']['type'] = $_FILES['files']['type'][$i];
										$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
										$_FILES['file']['error'] = $_FILES['files']['error'][$i];
										$_FILES['file']['size'] = $_FILES['files']['size'][$i];

										$config['upload_path'] = 'upload/project/';
										$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx|xls|xlsx|csv|txt|rtf|html';
										$config['max_size'] = '50000'; // max_size in kb
										//$config['file_name'] = $_FILES['files']['name'][$i];
										$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
										$exts = explode(".", $_FILES['files']['name'][$i]);
										$exts = end($exts);
										$exts = strtolower($exts);
										$config['file_name'] = $filename.".".$exts;
										$config['file_ext_tolower'] = TRUE;
										$config['remove_spaces'] = TRUE;

										$this->load->library('upload',$config);
										$this->upload->initialize($config);
										if($this->upload->do_upload('file')){
												$uploadData = $this->upload->data();
												$filename = $uploadData['file_name'];
												$company_id = intval($this->session->userdata('company_id'));

												$FileData = array();
												$FileData = array(
														'company_id' => $company_id,
														'project_id' => $project_id,
														'sub_project_id' => $sub_id,
														// 'level_id' => $level_id,
														// 'task_id' => $task_id,
														// 'ar_title' => $ar_title,
														// 'en_title' => $en_title,
														'image' => $filename,
												);
												$this->M_proj_project_files->InsertRecord($FileData);
										}
								}
						}

						redirect ("/project/proj_project/proj_sub_projects/".$project_id);
		}
    }
    public function expense_items_insert_data()
    {
			$this->form_validation->set_rules("item_name","item_name","required");
			$this->form_validation->set_rules("item_name_en","item_name_en","required");
		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $item_name_en = set_value('item_name_en');
            $item_name = set_value('item_name');




			$NewData = array();
			$NewData = array(
                'item_name' => $item_name,
                'item_name_en' => $item_name_en,

			);
            $sub_id = $this->M_expense_items->InsertRecord($NewData);
						// var_dump($_POST);die();
						redirect ("/project/proj_project/expense_items");
		}
    }
    public function Update_Data_item()
    {
			$this->form_validation->set_rules("item_name","item_name","required");
			$this->form_validation->set_rules("item_name_en","item_name_en","required");
		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
            $item_name_en = set_value('item_name_en');
            $item_name = set_value('item_name');


						$id = set_value('id');


			$NewData = array();
			$NewData = array(
                'item_name' => $item_name,
                'item_name_en' => $item_name_en,

			);
            $sub_id = $this->M_expense_items->UpdateRecord($id, $NewData);
						// var_dump($_POST);die();
						redirect ("/project/proj_project/expense_items");
		}
    }
}
