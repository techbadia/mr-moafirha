<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_notifications extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		$this->load->model("settings/M_settings_notifications");
		$this->load->model("account/M_fin_treeaccount");
	} 
	
	public function index()
	{
		$DataRow = $this->M_settings_notifications->GetRow();
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['fin_option1'] = $DataRow->fin_option1;
		$data['fin_option2'] = $DataRow->fin_option2;
		$data['fin_option3'] = $DataRow->fin_option3;
		$data['fin_option4'] = $DataRow->fin_option4;
		$data['purchase_option1'] = $DataRow->purchase_option1;
		$data['sales_option1'] = $DataRow->sales_option1;
		$data['store_option1'] = $DataRow->store_option1;
		$data['hr_option1'] = $DataRow->hr_option1;
		$data['hr_option2'] = $DataRow->hr_option2;
		$data['hr_option3'] = $DataRow->hr_option3;
		$data['hr_option4'] = $DataRow->hr_option4;
		$data['custody_option1'] = $DataRow->custody_option1;
		$data['custody_option2'] = $DataRow->custody_option2;
		$data['service_option1'] = $DataRow->service_option1;
		$data['maintenance_option1'] = $DataRow->maintenance_option1;
		$data['maintenance_option2'] = $DataRow->maintenance_option2;
		$data['manufacturing_option1'] = $DataRow->manufacturing_option1;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "settings/settings_notifications";
		$this->load->view('page', $data);
	}

	
    public function Update_module3()
	{
		$this->form_validation->set_rules("id","id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$fin_option1 = set_value("fin_option1");
			$fin_option2 = set_value("fin_option2");
            $fin_option3 = set_value("fin_option3");
            $fin_option4 = set_value("fin_option4");

			$NewData = array();
			$NewData = array(
				'fin_option1' => $fin_option1,
				'fin_option2' => $fin_option2,
                'fin_option3' => $fin_option3,
                'fin_option4' => $fin_option4,
			);

			$this->M_settings_notifications->UpdateRecord($id, $NewData);
			
			$DataRow = $this->M_settings_notifications->GetRow();
			$this->session->set_userdata('Not_fin_option1', $DataRow->fin_option1);
			$this->session->set_userdata('Not_fin_option2', $DataRow->fin_option2);
			$this->session->set_userdata('Not_fin_option3', $DataRow->fin_option3);
			$this->session->set_userdata('Not_fin_option4', $DataRow->fin_option4);
			
			redirect ('settings/settings_acceptance');
		}
    }
    
    public function Update_module4()
	{
		$this->form_validation->set_rules("id","id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$purchase_option1 = set_value("purchase_option1");

			$NewData = array();
			$NewData = array(
				'purchase_option1' => $purchase_option1,
			);

			$this->M_settings_notifications->UpdateRecord($id, $NewData);
			
			$DataRow = $this->M_settings_notifications->GetRow();
			$this->session->set_userdata('Not_purchase_option1', $DataRow->purchase_option1);

			redirect ('settings/settings_acceptance');
		}
    }
    
    public function Update_module5()
	{
		$this->form_validation->set_rules("id","id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$sales_option1 = set_value("sales_option1");

			$NewData = array();
			$NewData = array(
				'sales_option1' => $sales_option1,
			);

			$this->M_settings_notifications->UpdateRecord($id, $NewData);
			
			$DataRow = $this->M_settings_notifications->GetRow();
			$this->session->set_userdata('Not_sales_option1', $DataRow->sales_option1);

			redirect ('settings/settings_acceptance');
		}
    }
    
    public function Update_module6()
	{
		$this->form_validation->set_rules("id","id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$store_option1 = set_value("store_option1");

			$NewData = array();
			$NewData = array(
				'store_option1' => $store_option1,
			);

			$this->M_settings_notifications->UpdateRecord($id, $NewData);
			
			$DataRow = $this->M_settings_notifications->GetRow();
			$this->session->set_userdata('Not_store_option1', $DataRow->store_option1);

			redirect ('settings/settings_acceptance');
		}
    }
    
    public function Update_module7()
	{
		$this->form_validation->set_rules("id","id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$hr_option1 = set_value("hr_option1");
			$hr_option2 = set_value("hr_option2");
            $hr_option3 = set_value("hr_option3");
            $hr_option4 = set_value("hr_option4");

			$NewData = array();
			$NewData = array(
				'hr_option1' => $hr_option1,
				'hr_option2' => $hr_option2,
                'hr_option3' => $hr_option3,
                'hr_option4' => $hr_option4,
			);

			$this->M_settings_notifications->UpdateRecord($id, $NewData);
			
			$DataRow = $this->M_settings_notifications->GetRow();
			$this->session->set_userdata('Not_hr_option1', $DataRow->hr_option1);
			$this->session->set_userdata('Not_hr_option2', $DataRow->hr_option2);
			$this->session->set_userdata('Not_hr_option3', $DataRow->hr_option3);
			$this->session->set_userdata('Not_hr_option4', $DataRow->hr_option4);

			redirect ('settings/settings_acceptance');
		}
    }

    public function Update_module15()
	{
		$this->form_validation->set_rules("id","id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$custody_option1 = set_value("custody_option1");
			$custody_option2 = set_value("custody_option2");

			$NewData = array();
			$NewData = array(
				'custody_option1' => $custody_option1,
				'custody_option2' => $custody_option2,
			);

			$this->M_settings_notifications->UpdateRecord($id, $NewData);
			
			$DataRow = $this->M_settings_notifications->GetRow();
			$this->session->set_userdata('Not_custody_option1', $DataRow->custody_option1);
			$this->session->set_userdata('Not_custody_option2', $DataRow->custody_option2);

			redirect ('settings/settings_acceptance');
		}
    }

    public function Update_module16()
	{
		$this->form_validation->set_rules("id","id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$service_option1 = set_value("service_option1");

			$NewData = array();
			$NewData = array(
				'service_option1' => $service_option1,
			);

			$this->M_settings_notifications->UpdateRecord($id, $NewData);
			
			$DataRow = $this->M_settings_notifications->GetRow();
			$this->session->set_userdata('Not_service_option1', $DataRow->service_option1);

			redirect ('settings/settings_acceptance');
		}
    }

    public function Update_module17()
	{
		$this->form_validation->set_rules("id","id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $maintenance_option1 = set_value("maintenance_option1");
            $maintenance_option2 = set_value("maintenance_option2");

			$NewData = array();
			$NewData = array(
                'maintenance_option1' => $maintenance_option1,
                'maintenance_option2' => $maintenance_option2,
			);

			$this->M_settings_notifications->UpdateRecord($id, $NewData);
			
			$DataRow = $this->M_settings_notifications->GetRow();
			$this->session->set_userdata('Not_maintenance_option1', $DataRow->maintenance_option1);
			$this->session->set_userdata('Not_maintenance_option2', $DataRow->maintenance_option2);

			redirect ('settings/settings_acceptance');
		}
    }

    public function Update_module21()
	{
		$this->form_validation->set_rules("id","id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
            $manufacturing_option1 = set_value("manufacturing_option1");

			$NewData = array();
			$NewData = array(
                'manufacturing_option1' => $manufacturing_option1,
			);

			$this->M_settings_notifications->UpdateRecord($id, $NewData);
			
			$DataRow = $this->M_settings_notifications->GetRow();
			$this->session->set_userdata('Not_manufacturing_option1', $DataRow->manufacturing_option1);
			
			redirect ('settings/settings_acceptance');
		}
    }
	
}
