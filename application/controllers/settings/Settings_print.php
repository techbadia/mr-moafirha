<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_print extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		$this->load->model("settings/M_settings_print");
	} 
	
	public function index()
	{
		$DataRow = $this->M_settings_print->GetRow();
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['buy_bill_ar_text'] = $DataRow->buy_bill_ar_text;
		$data['buy_bill_en_text'] = $DataRow->buy_bill_en_text;
		$data['buy_bill_orientation'] = $DataRow->buy_bill_orientation;
		$data['buy_bill_size'] = $DataRow->buy_bill_size;
		$data['buy_returns_ar_text'] = $DataRow->buy_returns_ar_text;
		$data['buy_returns_en_text'] = $DataRow->buy_returns_en_text;
		$data['buy_returns_orientation'] = $DataRow->buy_returns_orientation;
		$data['buy_returns_size'] = $DataRow->buy_returns_size;
		$data['sale_bill_ar_text'] = $DataRow->sale_bill_ar_text;
		$data['sale_bill_en_text'] = $DataRow->sale_bill_en_text;
		$data['sale_bill_orientation'] = $DataRow->sale_bill_orientation;
		$data['sale_bill_size'] = $DataRow->sale_bill_size;
		$data['sale_returns_ar_text'] = $DataRow->sale_returns_ar_text;
		$data['sale_returns_en_text'] = $DataRow->sale_returns_en_text;
		$data['sale_returns_orientation'] = $DataRow->sale_returns_orientation;
		$data['sale_returns_size'] = $DataRow->sale_returns_size;
		$data['data_invoice_ar_text'] = $DataRow->data_invoice_ar_text;
		$data['data_invoice_en_text'] = $DataRow->data_invoice_en_text;
		$data['data_invoice_orientation'] = $DataRow->data_invoice_orientation;
		$data['data_invoice_size'] = $DataRow->data_invoice_size;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "settings/settings_print_update";
		$this->load->view('page', $data);
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("company_id","company_id","required");
		$this->form_validation->set_rules("buy_bill_ar_text","buy_bill_ar_text","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = set_value("company_id");
			$buy_bill_ar_text = set_value("buy_bill_ar_text");
			$buy_bill_en_text = set_value("buy_bill_en_text");
			$buy_bill_orientation = set_value('buy_bill_orientation');
			$buy_bill_size = set_value('buy_bill_size');
			$buy_returns_ar_text = set_value('buy_returns_ar_text');
			$buy_returns_en_text = set_value('buy_returns_en_text');
			$buy_returns_orientation = set_value('buy_returns_orientation');
			$buy_returns_size = set_value('buy_returns_size');
			$sale_bill_ar_text = set_value('sale_bill_ar_text');
			$sale_bill_en_text = set_value('sale_bill_en_text');
			$sale_bill_orientation = set_value('sale_bill_orientation');
			$sale_bill_size = set_value('sale_bill_size');
			$sale_returns_ar_text = set_value('sale_returns_ar_text');
			$sale_returns_en_text = set_value('sale_returns_en_text');
			$sale_returns_orientation = set_value('sale_returns_orientation');
			$sale_returns_size = set_value('sale_returns_size');
			$data_invoice_ar_text = set_value('data_invoice_ar_text');
			$data_invoice_en_text = set_value('data_invoice_en_text');
			$data_invoice_orientation = set_value('data_invoice_orientation');
			$data_invoice_size = set_value('data_invoice_size');

			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'buy_bill_ar_text' => $buy_bill_ar_text,
				'buy_bill_en_text' => $buy_bill_en_text,
				'buy_bill_orientation' => $buy_bill_orientation,
				'buy_bill_size' => $buy_bill_size,
				'buy_returns_ar_text' => $buy_returns_ar_text,
				'buy_returns_en_text' => $buy_returns_en_text,
				'buy_returns_orientation' => $buy_returns_orientation,
				'buy_returns_size' => $buy_returns_size,
				'sale_bill_ar_text' => $sale_bill_ar_text,
				'sale_bill_en_text' => $sale_bill_en_text,
				'sale_bill_orientation' => $sale_bill_orientation,
				'sale_bill_size' => $sale_bill_size,
				'sale_returns_ar_text' => $sale_returns_ar_text,
				'sale_returns_en_text' => $sale_returns_en_text,
				'sale_returns_orientation' => $sale_returns_orientation,
				'sale_returns_size' => $sale_returns_size,
				'data_invoice_ar_text' => $data_invoice_ar_text,
				'data_invoice_en_text' => $data_invoice_en_text,
				'data_invoice_orientation' => $data_invoice_orientation,
				'data_invoice_size' => $data_invoice_size,
			);
			

			$this->M_settings_print->UpdateRecord($id, $NewData);

			$DataRow = $this->M_settings_print->GetRow();
			$this->session->set_userdata('buy_bill_ar_text', $DataRow->buy_bill_ar_text);
			$this->session->set_userdata('buy_bill_en_text', $DataRow->buy_bill_en_text);
			$this->session->set_userdata('buy_bill_orientation', $DataRow->buy_bill_orientation);
			$this->session->set_userdata('buy_bill_size', $DataRow->buy_bill_size);
			$this->session->set_userdata('buy_returns_ar_text', $DataRow->buy_returns_ar_text);
			$this->session->set_userdata('buy_returns_en_text', $DataRow->buy_returns_en_text);
			$this->session->set_userdata('buy_returns_orientation', $DataRow->buy_returns_orientation);
			$this->session->set_userdata('buy_returns_size', $DataRow->buy_returns_size);
			$this->session->set_userdata('sale_bill_ar_text', $DataRow->sale_bill_ar_text);
			$this->session->set_userdata('sale_bill_en_text', $DataRow->sale_bill_en_text);
			$this->session->set_userdata('sale_bill_orientation', $DataRow->sale_bill_orientation);
			$this->session->set_userdata('sale_bill_size', $DataRow->sale_bill_size);
			$this->session->set_userdata('sale_returns_ar_text', $DataRow->sale_returns_ar_text);
			$this->session->set_userdata('sale_returns_en_text', $DataRow->sale_returns_en_text);
			$this->session->set_userdata('sale_returns_orientation', $DataRow->sale_returns_orientation);
			$this->session->set_userdata('sale_returns_size', $DataRow->sale_returns_size);
			$this->session->set_userdata('data_invoice_ar_text', $DataRow->data_invoice_ar_text);
			$this->session->set_userdata('data_invoice_en_text', $DataRow->data_invoice_en_text);
			$this->session->set_userdata('data_invoice_orientation', $DataRow->data_invoice_orientation);
			$this->session->set_userdata('data_invoice_size', $DataRow->data_invoice_size);
			
			redirect ('settings/settings_print');
		}
	}
	
}
