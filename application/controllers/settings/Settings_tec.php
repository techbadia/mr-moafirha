<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_tec extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();

		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();

        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		$this->load->model("settings/M_tec_settings");
	}

	public function index()
	{
		$DataRow = $this->M_tec_settings->GetRow();
		$data['id'] = $DataRow->id;
		$data['site_name'] = $DataRow->site_name;
		$data['address'] = $DataRow->address;
		$data['tel'] = $DataRow->tel;
		$data['tax_number'] = $DataRow->tax_number;
		$data['tax_date'] = $DataRow->tax_date;
		$data['default_email'] = $DataRow->default_email;
		$data['logo'] = $DataRow->logo;
		$data['crm'] = $DataRow->crm;
		$data['DB_pass'] = $DataRow->DB_pass;
		$data['DB_uname'] = $DataRow->DB_uname;
		$data['DB_name'] = $DataRow->DB_name;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "settings/settings_tec_update";
		$this->load->view('page', $data);
	}

	public function Update_Data()
	{

			$id = set_value('id');
			$site_name = set_value("site_name");
			$address = set_value("address");
			$tel = set_value("tel");
			$tax_number = set_value("tax_number");
			$tax_date = set_value("tax_date");
			$crm = set_value("crm");
			$DB_name = set_value("DB_name");
			$DB_uname = set_value("DB_uname");
			$DB_pass = set_value("DB_pass");

			$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");

            if(!isset($_FILES['image']) || $_FILES['image']['error'] == UPLOAD_ERR_NO_FILE) {
                    $logo = set_value("old_logo");;

            } else {
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/settings/";
			$ImageDirectory = $UploadPath;

			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
            $this->upload->do_upload('image', $NewFileName);
            $logo = $NewFileName;

            }


			$NewData = array();
			$NewData = array(
				'logo' => $logo,
				'site_name' => $site_name,
				'address' => $address,
				'tel' => $tel,
				'tax_number' => $tax_number,
				'tax_date' => $tax_date,
				'logo' => $logo,
				'crm' => $crm,
				'DB_name' => $DB_name,
				'DB_uname' => $DB_uname,
				'DB_pass' => $DB_pass,
			);


			$this->M_tec_settings->UpdateRecord($id, $NewData);


			redirect ('settings/settings_tec');

	}

}
