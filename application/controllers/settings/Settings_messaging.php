<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_messaging extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            redirect ('login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		$this->load->model("settings/M_fin_settings");
		$this->load->model("account/M_fin_treeaccount");
	} 
	
	public function index()
	{
		$DataRow = $this->M_fin_settings->GetRow();
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['acc_sale'] = $DataRow->acc_sale;
		$data['acc_website_sale'] = $DataRow->acc_website_sale;
		$data['acc_sale_reurn'] = $DataRow->acc_sale_reurn;
		$data['acc_buy'] = $DataRow->acc_buy;
		$data['acc_buy_reurn'] = $DataRow->acc_buy_reurn;
		$data['acc_treasury'] = $DataRow->acc_treasury;
		$data['acc_bank'] = $DataRow->acc_bank;
		$data['acc_expenses'] = $DataRow->acc_expenses;
		$data['acc_debtors'] = $DataRow->acc_debtors;
		$data['acc_creditors'] = $DataRow->acc_creditors;
		$data['acc_capture_papers'] = $DataRow->acc_capture_papers;
		$data['acc_payment_papers'] = $DataRow->acc_payment_papers;
		$data['acc_inventory'] = $DataRow->acc_inventory;
		$data['acc_supplier'] = $DataRow->acc_supplier;
		$data['acc_customer'] = $DataRow->acc_customer;
		$data['acc_material_store'] = $DataRow->acc_material_store;
		$data['acc_damaged_store'] = $DataRow->acc_damaged_store;
		$data['acc_invoice_items'] = $DataRow->acc_invoice_items;
		$data['acc_custody'] = $DataRow->acc_custody;
		$data['acc_affiliate'] = $DataRow->acc_affiliate;
		$data['acc_delivery'] = $DataRow->acc_delivery;
		$data['acc_factory_cost'] = $DataRow->acc_factory_cost;
		$data['acc_operating_expenses'] = $DataRow->acc_operating_expenses;
		$data['acc_salary'] = $DataRow->acc_salary;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "settings/fin_settings_update";
		$this->load->view('page', $data);
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("company_id","company_id","required");
		$this->form_validation->set_rules("acc_sale","acc_sale","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = set_value("company_id");
			$acc_sale = set_value("acc_sale");
			$acc_website_sale = set_value("acc_website_sale");
			$acc_sale_reurn = set_value('acc_sale_reurn');
			$acc_buy = set_value('acc_buy');
			$acc_buy_reurn = set_value('acc_buy_reurn');
			$acc_treasury = set_value('acc_treasury');
			$acc_bank = set_value('acc_bank');
			$acc_expenses = set_value('acc_expenses');
			$acc_debtors = set_value('acc_debtors');
			$acc_creditors = set_value('acc_creditors');
			$acc_capture_papers = set_value('acc_capture_papers');
			$acc_payment_papers = set_value('acc_payment_papers');
			$acc_inventory = set_value('acc_inventory');
			$acc_supplier = set_value('acc_supplier');
			$acc_customer = set_value('acc_customer');
			$acc_material_store = set_value('acc_material_store');
			$acc_damaged_store = set_value('acc_damaged_store');
			$acc_invoice_items = set_value('acc_invoice_items');
			$acc_custody = set_value('acc_custody');
			$acc_affiliate = set_value('acc_affiliate');
			$acc_delivery = set_value('acc_delivery');
			$acc_factory_cost = set_value('acc_factory_cost');
			$acc_operating_expenses = set_value('acc_operating_expenses');
			$acc_salary = set_value('acc_salary');

			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'acc_sale' => $acc_sale,
				'acc_website_sale' => $acc_website_sale,
				'acc_sale_reurn' => $acc_sale_reurn,
				'acc_buy' => $acc_buy,
				'acc_buy_reurn' => $acc_buy_reurn,
				'acc_treasury' => $acc_treasury,
				'acc_bank' => $acc_bank,
				'acc_expenses' => $acc_expenses,
				'acc_debtors' => $acc_debtors,
				'acc_creditors' => $acc_creditors,
				'acc_capture_papers' => $acc_capture_papers,
				'acc_payment_papers' => $acc_payment_papers,
				'acc_inventory' => $acc_inventory,
				'acc_supplier' => $acc_supplier,
				'acc_customer' => $acc_customer,
				'acc_material_store' => $acc_material_store,
				'acc_damaged_store' => $acc_damaged_store,
				'acc_invoice_items' => $acc_invoice_items,
				'acc_custody' => $acc_custody,
				'acc_affiliate' => $acc_affiliate,
				'acc_delivery' => $acc_delivery,
				'acc_factory_cost' => $acc_factory_cost,
				'acc_operating_expenses' => $acc_operating_expenses,
				'acc_salary' => $acc_salary,
			);
			

			$this->M_fin_settings->UpdateRecord($id, $NewData);
			
			redirect ('settings/fin_settings');
		}
	}

	public function Update_header()
	{
		$this->form_validation->set_rules("id","id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
		}

		$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
		$exts = explode(".", $_FILES['image']['name']);
		$exts = end($exts);
		$exts = strtolower($exts);
		$UploadPath = "./upload/settings/";
		$ImageDirectory = $UploadPath;
		
		if (!is_dir($ImageDirectory)) {
			mkdir($ImageDirectory, 0777, TRUE);

		}

		$config['upload_path'] = $ImageDirectory;
		$config['allowed_types'] = "*";
		$config['file_name'] = $filename.".".$exts;
		$config['overwrite'] = TRUE;
		$config['file_ext_tolower'] = TRUE;
		$config['remove_spaces'] = TRUE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$NewFileName = $filename.".".$exts;
		if (! $this->upload->do_upload('image', $NewFileName))
		{
			$error = array('error' => $this->upload->display_errors());
			$NewFileName ="";
		}
		else {
			$this->upload->do_upload('image', $NewFileName);
		}

		$NewData = array();
		$NewData = array(
			'header' => $NewFileName,
		);
		

		$this->M_fin_settings->UpdateRecord($id, $NewData);

		redirect ('settings/fin_settings');
	}

	public function Update_footer()
	{
		$this->form_validation->set_rules("id","id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
		}
		
		$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
		$exts = explode(".", $_FILES['image']['name']);
		$exts = end($exts);
		$exts = strtolower($exts);
		$UploadPath = "./upload/settings/";
		$ImageDirectory = $UploadPath;
		
		if (!is_dir($ImageDirectory)) {
			mkdir($ImageDirectory, 0777, TRUE);

		}

		$config['upload_path'] = $ImageDirectory;
		$config['allowed_types'] = "*";
		$config['file_name'] = $filename.".".$exts;
		$config['overwrite'] = TRUE;
		$config['file_ext_tolower'] = TRUE;
		$config['remove_spaces'] = TRUE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$NewFileName = $filename.".".$exts;
		if (! $this->upload->do_upload('image', $NewFileName))
		{
			$error = array('error' => $this->upload->display_errors());
			$NewFileName ="";
		}
		else {
			$this->upload->do_upload('image', $NewFileName);
		}

		$NewData = array();
		$NewData = array(
			'footer' => $NewFileName,
		);
		

		$this->M_fin_settings->UpdateRecord($id, $NewData);

		redirect ('settings/fin_settings');
	}
	
}
