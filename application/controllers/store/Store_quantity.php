<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store_quantity extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();
		
		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            $CookiePackageName = get_cookie('Package');
            redirect ($CookiePackageName.'login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}
		$this->load->model("store/M_store");
		$this->load->model("store/M_product");
		$this->load->model("store/M_store_move");
		$this->load->model("store/M_store_quantity");
	}

	public function index()
	{

		$data['DataRows'] = $this->M_store_quantity->GetMultiRow();
		$data['content_page'] = "store/store_quantity";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "store/store_quantity_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("store_id","store_id","required");
		$this->form_validation->set_rules("quantity","quantity","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = intval($this->session->userdata('company_id'));
			$store_id = set_value("store_id");
			$product_id = set_value('product_id');
			$quantity = set_value('quantity');

			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'store_id' => $store_id,
				'product_id' => $product_id,
				'quantity' => $quantity,
			);

			$this->M_store_quantity->InsertRecord($NewData);
			
			redirect ('store/store_quantity');
		}
	}

	public function updateform($rid)
	{
		$DataRow = $this->M_store_quantity->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['store_id'] = $DataRow->store_id;
		$data['product_id'] = $DataRow->product_id;
		$data['quantity'] = $DataRow->quantity;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "store/store_quantity_update";
		$this->load->view('page', $data);
		
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("store_id","store_id","required");
		$this->form_validation->set_rules("quantity","quantity","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = set_value("company_id");
			$store_id = set_value("store_id");
			$product_id = set_value('product_id');
			$quantity = set_value('quantity');

			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'store_id' => $store_id,
				'product_id' => $product_id,
				'quantity' => $quantity,
			);
			

			$this->M_store_quantity->UpdateRecord($id, $NewData);
			
			redirect ('store/store_quantity');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_store_quantity->UpdateRecord($rid, $NewData);

        redirect("store/store_quantity");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_store_quantity->UpdateRecord($rid, $NewData);

        redirect("store/store_quantity");
	}

	public function update_move($rid)
	{
		$DataRow = $this->M_store_quantity->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['store_id'] = $DataRow->store_id;
		$data['product_id'] = $DataRow->product_id;
		$data['quantity'] = $DataRow->quantity;

		$product_id = $DataRow->product_id;
		$ProductData = $this->M_product->GetRow($product_id);
		$data['barcode'] = $ProductData->barcode;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "store/store_quantity_move";
		$this->load->view('page', $data);
	}

	public function insert_move()
	{
		$this->form_validation->set_rules("from_store","from_store","required");
		$this->form_validation->set_rules("to_store","to_store","required");

		$this->load->model("M_store_quantity");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$company_id = intval($this->session->userdata('ShopID'));
			$user_id = intval($this->session->userdata('MemberID'));
			$from_store = set_value("from_store");
			$to_store = set_value('to_store');
			$thedate = date("Y-m-d");
			$thetime = date("h:i:s");
			$product_id = set_value('product_id');
			$quantity = set_value('quantity');

			if($quantity > 0)
			{
				
				$CountStoreProduct = $this->M_store_quantity->CountStoreProduct($from_store, $product_id);
				if($CountStoreProduct > 0)
				{
					$Old_FromStoreData = $this->M_store_quantity->GetStoreProduct($from_store, $product_id);
					$Old_FromStore_Quantity = $Old_FromStoreData->quantity;
					$Old_FromStore_id = $Old_FromStoreData->id;
					$New_FromStore_Quantity = $Old_FromStore_Quantity - $quantity;
					$FromStoreArrayData = array();
					$FromStoreArrayData = array(
						'store_id' => $from_store,
						'product_id' => $product_id,
						'quantity' => $New_FromStore_Quantity,
					);
					$this->M_store_quantity->UpdateRecord($Old_FromStore_id, $FromStoreArrayData);

					$CountStoreProduct = $this->M_store_quantity->CountStoreProduct($to_store, $product_id);
					if($CountStoreProduct == 0)
					{
						$ToStoreArrayData = array();
						$ToStoreArrayData = array(
							'company_id' => $company_id,
							'store_id' => $to_store,
							'product_id' => $product_id,
							'quantity' => $quantity,
						);
						$this->M_store_quantity->InsertRecord($ToStoreArrayData);
					}
					else
					{
						$Old_ToStoreData = $this->M_store_quantity->GetStoreProduct($to_store, $product_id);
						$Old_ToStore_Quantity = $Old_ToStoreData->quantity;
						$Old_ToStore_id = $Old_ToStoreData->id;
						$New_ToStore_Quantity = $Old_ToStore_Quantity + $quantity;
						$ToStoreArrayData = array();
						$ToStoreArrayData = array(
							'store_id' => $to_store,
							'product_id' => $product_id,
							'quantity' => $New_ToStore_Quantity,
						);
						$this->M_store_quantity->UpdateRecord($Old_ToStore_id, $ToStoreArrayData);
					}
				}

				$NewData = array();
				$NewData = array(
					'company_id' => $company_id,
					'user_id' => $user_id,
					'from_store' => $from_store,
					'to_store' => $to_store,
					'product_id' => $product_id,
					'quantity' => $quantity,
					'thedate' => $thedate,
					'thetime' => $thetime,
				);

				$this->M_store_move->InsertRecord($NewData);
			}
			redirect ('store/store_quantity');
		}
	}
}
