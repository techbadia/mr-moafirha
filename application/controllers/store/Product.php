<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	var $data = array();
    public function __construct() {
		parent::__construct();

		$StaffID = intval($this->session->userdata('StaffID'));
        if ($StaffID == 0)
        {
            $CookiePackageName = get_cookie('Package');
            redirect ($CookiePackageName.'login');
        }
        else
        {
            $Segment2 = $this->uri->segment(2);
            $Segment3 = $this->uri->segment(3);
            $group_id = intval($this->session->userdata('GroupID'));
            $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
            $page_id = $PageData->id;
            $CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
            if($CheckView == 0)
            {
                redirect ('home');
            }
            $InsertData = array();
            $thedate = date("Y-m-d");
            $thetime = date("H:i:s");
            $ControllerName = $this->router->fetch_class();
            if(($Segment2 == $ControllerName) && ($Segment3 == ""))
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 1,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "insertform")
            {
                $CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
                if($CheckAdd == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "insert_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 2,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "updateform")
            {
                $CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
                if($CheckEdit == 0)
                {
                    redirect ('home');
                }
            }
            if($Segment3 == "update_data")
            {
                $InsertData = array(
                    'user_id' => $StaffID,
                    'page_id' => $page_id,
                    'action_id' => 3,
                    'thedate' => $thedate,
                    'thetime' => $thetime,
                );
                $this->M_usr_trace->InsertRecord($InsertData);
            }
            if($Segment3 == "delete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 4,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
            if($Segment3 == "undelete")
            {
                $CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
                if($CheckDelete == 0)
                {
                    redirect($_SERVER['HTTP_REFERER']);
                }
                else
                {
                    $InsertData = array(
                        'user_id' => $StaffID,
                        'page_id' => $page_id,
                        'action_id' => 5,
                        'thedate' => $thedate,
                        'thetime' => $thetime,
                    );
                    $this->M_usr_trace->InsertRecord($InsertData);
                }
            }
        }

		$this->config->load('settings/config_setting');

		if ((($this->session->userdata('lang')) == "") || (($this->session->userdata('lang')) == "ar"))
        {
			$this->session->set_userdata('lang', $this->config->item('MainLanguage'));
            $this->lang->load('mywords', 'arabic');
            $this->session->set_userdata('CSSFile', ".rtl");
            $this->session->set_userdata('Direction', "rtl");
			$this->session->set_userdata('Alignment', "right");
			$this->session->set_userdata('HTML', "direction='rtl' dir='rtl' style='direction: rtl'");
		}
		else
        {
            $this->lang->load('mywords', 'english');
            $this->session->set_userdata('CSSFile', "");
            $this->session->set_userdata('Direction', "ltr");
			$this->session->set_userdata('Alignment', "left");
			$this->session->set_userdata('HTML', "lang='en'");
		}

		$this->load->model("store/M_store");
		$this->load->model("store/M_store_quantity");
		$this->load->model("store/M_store_category");
		$this->load->model("store/M_store_brand");
		$this->load->model("store/M_product");
		$this->load->model("pos_sys/M_Tec_products");
		$this->load->model("pos_sys/M_Tec_stores");
		$this->load->model("pos_sys/M_Tec_product_store_qty");
	}

	public function index()
	{
		$data['DataRows'] = $this->M_product->GetMultiRow();
		$data['content_page'] = "store/product";
		$this->load->view('page', $data);
	}

	public function insertform()
	{
		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "store/product_insert";
		$this->load->view('page', $data);
	}

	public function insert_data()
	{
		//brand
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("details","details","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = intval($this->session->userdata('company_id'));
			$category_id = set_value('category_id');
			$brand_id = set_value('brand_id');
			$barcode = set_value('barcode');
			$batch_number = set_value('batch_number');
			$title = set_value('title');
			$title_en = set_value('title_en');
			$details = set_value("details");
			$details_en = set_value("details_en");
			$cost_price = set_value('cost_price');
			$lowest_price = set_value('lowest_price');
			$price = set_value('price');
			$finished = set_value('finished');
			$demand = set_value('demand');

			$model_name = set_value('model_name');
			$chassis_no = set_value('chassis_no');
			$plate_No = set_value('plate_No');
			$meter_reading = set_value('meter_reading');
			$motor_number = set_value('motor_number');
			$color = set_value('color');

			$deleted = set_value('deleted');

			// Upload Image
			$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/product/";
			$ImageDirectory = $UploadPath;

			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);
			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
			}
			/// Upload catalogue
			$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['catalogue']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/product/";
			$ImageDirectory = $UploadPath;

			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);
			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$catalogue = $filename.".".$exts;
			if (! $this->upload->do_upload('catalogue', $catalogue))
			{
				$error = array('error' => $this->upload->display_errors());
				$catalogue ="";
			}
			else {
				$this->upload->do_upload('catalogue', $catalogue);
			}

			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'category_id' => $category_id,
				'brand_id' => $brand_id,
				'barcode' => $barcode,
				'batch_number' => $batch_number,
				'title' => $title,
				'title_en' => $title_en,
				'details' => $details,
				'details_en' => $details_en,
				'cost_price' => $cost_price,
				'lowest_price' => $lowest_price,
				'price' => $price,
				'image' => $NewFileName,
				'catalogue' => $catalogue,
				'finished' => $finished,
				'demand' => $demand,
				'model_name' => $model_name,
				'chassis_no' => $chassis_no,
				'plate_No' => $plate_No,
				'meter_reading' => $meter_reading,
				'motor_number' => $motor_number,
				'color' => $color,
				'deleted' => $deleted,
			);

			$this->M_product->InsertRecord($NewData);

			$tax = intval($this->session->userdata('acc_tax_sale_percent'));
			$barcode_symbology = "code128";
			$LatestProduct = $this->M_product->GetLatestProduct($category_id, $barcode, $title);
			$product_id = $LatestProduct->id;
			// insert into tec_products
			$Tec_products_Data = array();
			$Tec_products_Data = array(
				'id' => $product_id,
				'code' => $product_id,
				'name' => $title,
				'category_id' => $category_id,
				'price' => $price,
				'image' => $NewFileName,
				'tax' => $tax,
				'cost' => $cost_price,
				'tax_method' => 0,
				'quantity' => 0,
				'barcode_symbology' => $barcode_symbology,
				'type' => "standard",
				'details' => $details,
				'alert_quantity' => $demand,
			);

			$this->M_Tec_products->InsertRecord($Tec_products_Data);
			// end insert into tec_products & store_quantity
			$company_id = intval($this->session->userdata('company_id'));
            $user_id = intval($this->session->userdata('StaffID'));
			$StoreList = $this->M_store->GetMultiRow();
			$Tec_product_store_qty_Data = array();
			$store_quantity_Data = array();
			foreach($StoreList as $StoreList_Row) {
				$id = $StoreList_Row->id;
				$Store = $StoreList_Row->title;
				$StoreID = "StoreID".$id;
				$qunatity = "qunatity".$id;

				$StoreID = set_value($StoreID);
				$qunatity = set_value($qunatity);

				$Find_POS_Store = $this->M_Tec_stores->CheckByTitle($Store);
				if($Find_POS_Store > 0)
				{
					$Find_POS_Store_data = $this->M_Tec_stores->FindByName($Store);
					$shop_id = $Find_POS_Store_data->id;
					$Tec_product_store_qty_Data = array(
						'product_id' => $product_id,
						'store_id' => $shop_id,
						'quantity' => $qunatity,
						'price' => $price,
					);
					$this->M_Tec_product_store_qty->InsertRecord($Tec_product_store_qty_Data);
				}

				$store_quantity_Data = array(
					'company_id' => $company_id,
					'store_id' => $StoreID,
					'product_id' => $product_id,
					'quantity' => $qunatity,
					'deleted' => 0,
				);
				$this->M_store_quantity->InsertRecord($store_quantity_Data);

			}
			//

			//
			redirect ('store/product');
		}
	}


	public function updateform($rid)
	{
		$DataRow = $this->M_product->GetRow($rid);
		$data['id'] = $DataRow->id;
		$data['company_id'] = $DataRow->company_id;
		$data['category_id'] = $DataRow->category_id;
		$data['brand_id'] = $DataRow->brand_id;
		$data['barcode'] = $DataRow->barcode;
		$data['batch_number'] = $DataRow->batch_number;
		$data['title'] = $DataRow->title;
		$data['title_en'] = $DataRow->title_en;
		$data['details'] = $DataRow->details;
		$data['details_en'] = $DataRow->details_en;
		$data['cost_price'] = $DataRow->cost_price;
		$data['lowest_price'] = $DataRow->lowest_price;
		$data['price'] = $DataRow->price;
		$data['finished'] = $DataRow->finished;
		$data['demand'] = $DataRow->demand;
		$data['model_name'] = $DataRow->model_name;
		$data['chassis_no'] = $DataRow->chassis_no;
		$data['plate_No'] = $DataRow->plate_No;
		$data['meter_reading'] = $DataRow->meter_reading;
		$data['motor_number'] = $DataRow->motor_number;
		$data['color'] = $DataRow->color;
		$data['deleted'] = $DataRow->deleted;

		$data['ControllerName'] = $this->router->fetch_class();
		$data['content_page'] = "store/product_update";
		$this->load->view('page', $data);
	}

	public function Update_Data()
	{
		$this->form_validation->set_rules("title","title","required");
		$this->form_validation->set_rules("details","details","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');
			$company_id = intval($this->session->userdata('company_id'));
			$category_id = set_value('category_id');
			$brand_id = set_value('brand_id');
			$barcode = set_value('barcode');
			$batch_number = set_value('batch_number');
			$title = set_value('title');
			$title_en = set_value('title_en');
			$details = set_value("details");
			$details_en = set_value("details_en");
			$cost_price = set_value('cost_price');
			$lowest_price = set_value('lowest_price');
			$price = set_value('price');
			$finished = set_value('finished');
			$demand = set_value('demand');

			$model_name = set_value('model_name');
			$chassis_no = set_value('chassis_no');
			$plate_No = set_value('plate_No');
			$meter_reading = set_value('meter_reading');
			$motor_number = set_value('motor_number');
			$color = set_value('color');

			$deleted = set_value('deleted');

			$NewData = array();
			$NewData = array(
				'company_id' => $company_id,
				'category_id' => $category_id,
				'brand_id' => $brand_id,
				'barcode' => $barcode,
				'batch_number' => $batch_number,
				'title' => $title,
				'title_en' => $title_en,
				'details' => $details,
				'details_en' => $details_en,
				'cost_price' => $cost_price,
				'lowest_price' => $lowest_price,
				'price' => $price,
				'finished' => $finished,
				'demand' => $demand,
				'model_name' => $model_name,
				'chassis_no' => $chassis_no,
				'plate_No' => $plate_No,
				'meter_reading' => $meter_reading,
				'motor_number' => $motor_number,
				'color' => $color,
				'deleted' => $deleted,
			);

			$this->M_product->UpdateRecord($id, $NewData);

			redirect ('store/product');
		}
	}

	public function delete($rid)
	{
		$deleted = 1;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_product->UpdateRecord($rid, $NewData);

        redirect("store/product");
	}

	public function undelete($rid)
	{
		$deleted = 0;
		$NewData = array();
		$NewData = array(
			'deleted' => $deleted,
		);

		$this->M_product->UpdateRecord($rid, $NewData);

        redirect("store/product");
	}

	public function ChangeImage()
	{
		$this->form_validation->set_rules("id","id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');

			// Upload Image
			$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['image']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/product/";
			$ImageDirectory = $UploadPath;

			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('image', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('image', $NewFileName);
			}

			$NewData = array();
			$NewData = array(
				'image' => $NewFileName,
			);

			$this->M_product->UpdateRecord($id, $NewData);

			redirect ('store/product');
		}
	}

	public function ChangeCatalogue()
	{
		$this->form_validation->set_rules("id","id","required");

		if ($this->form_validation->run() == FALSE) {
            //$this->load->view('index', $data);
            echo validation_errors();
		}
		else
		{
			$id = set_value('id');

			// Upload Image
			$filename = date("Y")."-".date("m")."-".date("d")."-".date("h")."-".date("i")."-".date("s");
			$exts = explode(".", $_FILES['catalogue']['name']);
			$exts = end($exts);
			$exts = strtolower($exts);
			$UploadPath = "./upload/product/";
			$ImageDirectory = $UploadPath;

			if (!is_dir($ImageDirectory)) {
				mkdir($ImageDirectory, 0777, TRUE);

			}

			$config['upload_path'] = $ImageDirectory;
			$config['allowed_types'] = "*";
			$config['file_name'] = $filename.".".$exts;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$NewFileName = $filename.".".$exts;
			if (! $this->upload->do_upload('catalogue', $NewFileName))
			{
				$error = array('error' => $this->upload->display_errors());
				$NewFileName ="";
			}
			else {
				$this->upload->do_upload('catalogue', $NewFileName);
			}

			$NewData = array();
			$NewData = array(
				'catalogue' => $NewFileName,
			);

			$this->M_product->UpdateRecord($id, $NewData);

			redirect ('store/product');
		}
	}
}
