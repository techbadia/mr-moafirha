<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| AUTO-LOADER
| -------------------------------------------------------------------
| This file specifies which systems should be loaded by default.
|
| In order to keep the framework as light-weight as possible only the
| absolute minimal resources are loaded by default. For example,
| the database is not connected to automatically since no assumption
| is made regarding whether you intend to use it.  This file lets
| you globally define which systems you would like loaded with every
| request.
|
| -------------------------------------------------------------------
| Instructions
| -------------------------------------------------------------------
|
| These are the things you can load automatically:
|
| 1. Packages
| 2. Libraries
| 3. Drivers
| 4. Helper files
| 5. Custom config files
| 6. Language files
| 7. Models
|
*/

/*
| -------------------------------------------------------------------
|  Auto-load Packages
| -------------------------------------------------------------------
| Prototype:
|
|  $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
|
*/
$autoload['packages'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Libraries
| -------------------------------------------------------------------
| These are the classes located in system/libraries/ or your
| application/libraries/ directory, with the addition of the
| 'database' library, which is somewhat of a special case.
|
| Prototype:
|
|	$autoload['libraries'] = array('database', 'email', 'session');
|
| You can also supply an alternative library name to be assigned
| in the controller:
|
|	$autoload['libraries'] = array('user_agent' => 'ua');
*/
$autoload['libraries'] = array('database', 'session', 'pagination','upload', 'cart', 
'form_validation', 'user_agent');

/*
| -------------------------------------------------------------------
|  Auto-load Drivers
| -------------------------------------------------------------------
| These classes are located in system/libraries/ or in your
| application/libraries/ directory, but are also placed inside their
| own subdirectory and they extend the CI_Driver_Library class. They
| offer multiple interchangeable driver options.
|
| Prototype:
|
|	$autoload['drivers'] = array('cache');
|
| You can also supply an alternative property name to be assigned in
| the controller:
|
|	$autoload['drivers'] = array('cache' => 'cch');
|
*/
$autoload['drivers'] = array('cache');

/*
| -------------------------------------------------------------------
|  Auto-load Helper Files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['helper'] = array('url', 'file');
*/
$autoload['helper'] = array('html', 'url', 'directory', 'file', 'form', 'cookie', 'language', 'security','jwt_helper');

/*
| -------------------------------------------------------------------
|  Auto-load Config files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['config'] = array('config1', 'config2');
|
| NOTE: This item is intended for use ONLY if you have created custom
| config files.  Otherwise, leave it blank.
|
*/
$autoload['config'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Language files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['language'] = array('lang1', 'lang2');
|
| NOTE: Do not include the "_lang" part of your file.  For example
| "codeigniter_lang.php" would be referenced as array('codeigniter');
|
*/
$autoload['language'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Models
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['model'] = array('first_model', 'second_model');
|
| You can also supply an alternative model name to be assigned
| in the controller:
|
|	$autoload['model'] = array('first_model' => 'first');
*/
$autoload['model'] = array('app/M_app_company', 'app/M_app_package', 'app/M_app_package_module', 'app/M_app_package_module_page', 
'app/M_app_module', 'app/M_app_module_page', 'permissions/M_app_module_page', 
'permissions/M_usr_usersgroup', 'permissions/M_usr_usersprivileges', 'service/M_serv_message', 'service/M_serv_notifications', 
'service/M_serv_events', 'service/M_serv_events_users', 'service/M_serv_action', 'service/M_serv_action_types', 
'M_app_element', 'permissions/M_usr_users', 'tasks/M_tas_tasks', 'tasks/M_tas_tasks_transfer', 
'tasks/M_tas_tasks_type1_levels', 'project/M_proj_project', 'project/M_proj_project_files', 
'project/M_proj_project_level', 'project/M_proj_project_level_task', 'account/M_fin_journal', 
'account/M_fin_journal_main', 'settings/M_fin_settings', 'account/M_fin_treeaccount', 
'account/M_fin_treecategory', 'permissions/M_usr_trace', 'M_statistics', 'support/M_tic_status', 
'M_currencies', 'settings/M_settings_print', 'permissions/M_usr_users_store', 
'permissions/M_usr_users_account', 'Astatistics', 'maintenance/M_set_teamwork_tasks', 
'maintenance/M_set_teamwork_schedule', 'maintenance/M_set_teamwork', 
'communication/M_push_messages', 'communication/M_push_notifications', 
'communication/M_sale_customer_email', 'communication/M_sale_customer_schedule', 
'sales/M_sale_customer', 'maintenance/M_data_request_web');
