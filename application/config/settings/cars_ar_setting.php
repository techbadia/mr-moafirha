<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config['You_Welcome'] = "مرحباً بك في";
$config['package_title'] = 'نظام تخطيط موارد المنشأة المالية';
$config['package_details'] = 'يمكنك الإعتماد على كفاءة النظام في تحليل البيانات والعمليات المحاسبية والإدارية المتعددة المستويات.';
