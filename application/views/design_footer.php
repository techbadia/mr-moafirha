<!--begin::Global Config(global config for global JS scripts)-->
<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
<!--end::Global Config-->
<!--begin::Global Theme Bundle(used by all pages)-->
<script src="<?php echo base_url();?>/assets/plugins/global/plugins.bundle.js"></script>
<script src="<?php echo base_url();?>/assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
<script src="<?php echo base_url();?>/assets/js/scripts.bundle.js"></script>
<!--end::Global Theme Bundle-->
<!--begin::Page Vendors(used by this page)-->
<script src="<?php echo base_url();?>/assets/plugins/custom/jstree/jstree.bundle.js"></script>
<script src="<?php echo base_url();?>/assets/js/pages/features/miscellaneous/treeview.js"></script>
<script src="<?php echo base_url();?>/assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
<!--end::Page Vendors-->
<!--begin::Page Scripts(used by this page)-->
<script src="<?php echo base_url();?>/assets/js/pages/widgets.js"></script>
<!--end::Page Scripts-->


<?php
// Chart types
// line, spline, area, areaspline, column, bar, pie, scatter
if(($Segment1 == "account") && ($Segment2 == "fin_liquidity_risk") || ($Segment1 == "") || ($Segment1 == "erp") ||
($Segment1 == "cars") || ($Segment1 == "car_rental"))
{
?>
<script type="text/javascript">

$(function () {
    var lastColor;
    function getRandColor(same, darkness) {
        //6 levels of brightness from 0 to 5, 0 being the darkest
        var rgb = [];
        if(same && lastColor) {
            rgb = lastColor;
        } else {
            rgb = [Math.random() * 256, Math.random() * 256, Math.random() * 256];
        }
        var mix = [darkness * 51, darkness * 51, darkness * 51]; //51 => 255/5
        var mixedrgb = [rgb[0] + mix[0], rgb[1] + mix[1], rgb[2] + mix[2]].map(function (x) {
            return Math.round(x / 2.0)
        })
        lastColor = rgb;
        return "rgb(" + mixedrgb.join(",") + ")";
    }
});

Highcharts.chart('container', {
    data: {
        table: 'SalesChart'
    },
    chart: {
        type: 'line',
    },
    title: {
        text: '<?php echo lang('ShortCut_Statistics_Sales');?>'
    },
    yAxis: {
        allowDecimals: true,
        title: {
            text: '<?php echo $this->session->userdata('currency_title');?>'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('Purchase_container', {
    data: {
        table: 'PurchaseChart'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '<?php echo lang('ShortCut_Statistics_Purchase');?>'
    },
    yAxis: {
        allowDecimals: true,
        title: {
            text: '<?php echo $this->session->userdata('currency_title');?>'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('Service_container', {
    data: {
        table: 'ServiceChart'
    },
    chart: {
        type: 'pie'
    },
    title: {
        text: '<?php echo lang('ShortCut_Statistics_Service');?>'
    },
    yAxis: {
        allowDecimals: true,
        title: {
            text: '<?php echo $this->session->userdata('currency_title');?>'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('Treasury_container', {
    data: {
        table: 'TreasuryChart'
    },
    chart: {
        type: 'bar',
    },
    title: {
        text: '<?php echo lang('ShortCut_Reports_Account_1');?>'
    },
    yAxis: {
        allowDecimals: true,
        title: {
            text: '<?php echo $this->session->userdata('currency_title');?>'
        },
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('Bank_container', {
    data: {
        table: 'BankChart'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '<?php echo lang('ShortCut_Reports_Account_2');?>'
    },
    yAxis: {
        allowDecimals: true,
        title: {
            text: '<?php echo $this->session->userdata('currency_title');?>'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('Custody_container', {
    data: {
        table: 'CustodyChart'
    },
    chart: {
        type: 'spline'
    },
    title: {
        text: '<?php echo lang('ShortCut_Reports_Account_8');?>'
    },
    yAxis: {
        allowDecimals: true,
        title: {
            text: '<?php echo $this->session->userdata('currency_title');?>'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});
</script>
<?php
}
?>
