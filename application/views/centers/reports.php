<?php $base_url = base_url(); ?>
                <!--begin: Datatable -->
                <div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
                    <form class="kt-form kt-form--label-right" method="get" action="<?php echo base_url().$Segment1."/".$Segment2."/".$Segment3;?>" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-md-2">
                                <label><?php echo $this->lang->line('Account name / client / supplier');?></label>
                                <input type="text" name="keyword" class="form-control" value="<?php echo $_GET["keyword"];?>" >
                            </div>
                            <div class="form-group col-md-2">
                                <label><?php echo $this->lang->line('hr_holidays_fromdate');?></label>
                                <input type="date" name="fromdate" class="form-control" value="<?php echo $_GET["fromdate"];?>" >
                            </div>
                            <div class="form-group col-md-2">
                                <label><?php echo $this->lang->line('hr_holidays_todate');?></label>
                                <input type="date" name="todate" class="form-control" value="<?php echo $_GET["todate"];?>" >
                            </div>

                            <div class="form-group col-md-2">
                              <label  label=""><?php echo lang('sub_center_parent_id'); ?></label >

                                <select name="center_id" class="form-control select2 center_id" onchange="getSubCenters()">

                                  <?php
                                  foreach($centers as $center) {
                                    if ($this->session->userdata('lang') == "ar")
                                    {
                                      $ToTitle = $center->name_ar;
                                    }
                                    else
                                    {
                                      $ToTitle = $center->name_en;
                                    }
                                    ?>

                                    <option <?php echo ($_GET['center_id'] == $center->id)?"selected":"" ?> value="<?php echo $center->id;?>"><?php echo $ToTitle;?></option>
                                  <?php } ?>
                                </select>
                              </div>

                            <div class="form-group col-md-2">
                              <label  label=""><?php echo lang('sub_center'); ?></label >

                      				<select name="sub_center_id" class="form-control select2 sub_center_id">
                                <option  value=""><?php echo lang('Choose');?></option>

                      					<?php
                      					foreach($sub_centerss as $sub) {
                                  if ($this->session->userdata('lang') == "ar")
                                  {
                                    $subTitle = $sub->name_ar;
                                  }
                                  else
                                  {
                                    $subTitle = $sub->name_en;
                                  }
                      					?>

                      					<option <?php echo ($_GET['sub_center_id'] == $sub->id)?"selected":"" ?> value="<?php echo $sub->id;?>"><?php echo $subTitle;?></option>
                      				<?php } ?>
                      				</select>
                      			</div>

                            <div class="form-group col-md-2">
                                <button type="submit" class="btn btn-success btn-custom" id="kt_sweetalert_demo_3_3" style="margin-top:25px;"><?php echo lang('Search');?></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="">

                <?php if(!empty($_GET['center_id'])){
                  $center = $this->M_center->GetRow($_GET['center_id']);

                  ?>
                  <h6><?php echo lang('sub_center_parent_id'); ?> :
                    <?php

                    if ($this->session->userdata('lang') == "ar")
                    {
                      echo $center->name_ar;
                    }
                    else
                    {
                      echo $center->name_en;
                    }
                    ?>
                  </h6>
                <?php } ?>
                <?php if(!empty($_GET['sub_center_id'])){
                  $center = $this->M_sub_centers->GetRow($_GET['sub_center_id']);

                  ?>
                  <h6><?php echo lang('sub_center'); ?> :
                    <?php

                    if ($this->session->userdata('lang') == "ar")
                    {
                      echo $center->name_ar;
                    }
                    else
                    {
                      echo $center->name_en;
                    }
                    ?>
                  </h6>
                <?php } ?>
                <?php if(!empty($_GET['fromdate'])){

                  ?>
                  <h6><?php echo lang('hr_holidays_fromdate'); ?> :
                    <?php

                    if ($this->session->userdata('lang') == "ar")
                    {
                      echo $_GET['fromdate'];
                    }
                    else
                    {
                      echo $_GET['fromdate'];
                    }
                    ?>
                  </h6>
                <?php } ?>
                <?php if(!empty($_GET['todate'])){

                  ?>
                  <h6><?php echo lang('hr_holidays_todate'); ?> :
                    <?php

                    if ($this->session->userdata('lang') == "ar")
                    {
                      echo $_GET['todate'];
                    }
                    else
                    {
                      echo $_GET['todate'];
                    }
                    ?>
                  </h6>
                <?php } ?>
                <?php if(!empty($_GET['keyword'])){

                  ?>
                  <h6><?php echo lang('Account name / client / supplier'); ?> :
                    <?php

                    if ($this->session->userdata('lang') == "ar")
                    {
                      echo $_GET['keyword'];
                    }
                    else
                    {
                      echo $_GET['keyword'];
                    }
                    ?>
                  </h6>
                <?php } ?>
              </div>
              <style media="screen">
              div.dt-buttons {
  position: relative;
  float: left;
}
              </style>
                <table class="table table-separate table-head-custom table-checkable" id="examples">
                  <thead>
                    <tr>
                      <th><?php echo lang('Number');?></th>
                      <th><?php echo lang('Date');?></th>
                      <th><?php echo lang('fin_treeaccount_account_no');?></th>
                      <th><?php echo lang('fin_treeaccount_account_name');?></th>
                      <th><?php echo lang('Description');?></th>
                      <th><?php echo lang('Type');?></th>
                      <th><?php echo lang('cost_center_dept');?></th>
                      <th><?php echo lang('cost_center_creditor');?></th>
                      <th><?php echo lang('cost_center_net');?></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $counter = 1;
                    foreach($result as $row) {
                      if($counter == 1)
                      {
                        $credit = $row->dept - $row->creditor;
                      }else {
                        $credit = ($credit + $row->dept) - $row->creditor;
                      }
                      $creditTotal += $credit;
                      $deptTotal += $row->dept;
                      $creTotal += $row->creditor;
                    ?>
                    <tr>
                      <td><?php echo $row->Invoice_Start;?></td>
                      <td><?php echo $row->thedate;?></td>
                      <td><?php echo $row->account_no;?></td>
                      <td><?php echo $row->account_title;?></td>
                      <td><?php echo $row->details;?></td>
                      <td> <a href="<?php echo $row->url ?>" class="btn btn-info" target="_blank"> <?php echo $row->type;?></a></td>
                      <td> <?php echo $row->dept ?> <?php echo lang('SAR');?> </td>
                      <td> <?php echo $row->creditor ?> <?php echo lang('SAR');?> </td>
                      <td> <?php echo $credit ?> <?php echo lang('SAR');?> </td>
                    </tr>



                    <?php
              $counter++;      }
              if(!empty($_GET['center_id'] )){
                    ?>
                    <tr style="background-color:#94e8a7">
                      <td><?php echo lang('Total');?></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td> <?php echo $deptTotal ?> <?php echo lang('SAR');?> </td>
                      <td> <?php echo $creTotal ?> <?php echo lang('SAR');?> </td>
                      <td> <?php echo $creditTotal ?> <?php echo lang('SAR');?> </td>
                    </tr>
                  <?php } ?>
                  </tbody>

                </table>
