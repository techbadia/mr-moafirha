<div class="card card-custom gutter-b example example-compact">
                    <?php
                    $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
                    echo form_open_multipart($FormPath);
                    ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('cost_center_name');?></label>
												<input type="text" name="name_ar" class="form-control" value="<?php echo $name_ar;?>" required>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('cost_center_name_en');?></label>
												<input type="text" name="name_en" class="form-control" value="<?php echo $name_en;?>" >
											</div>
										</div>
									</div>
                  <input type="hidden" name="parent_id" value="<?php echo $_GET['main'] ?>">

                  <div class="form-body">
                    <div class="form-group row">
                      <div class="col-lg-6 col-md-6 col-sm-12">
                        <label class="col-form-label"><?php echo lang('sub_center_dept');?></label>
                        <input type="text" name="dept" class="form-control" value="<?php echo $dept;?>" required>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12">
                        <label class="col-form-label"><?php echo lang('sub_center_creditor');?></label>
                        <input type="text" name="credit" class="form-control" value="<?php echo $credit;?>" required>
                      </div>
                    </div>
                  </div>
                  <div class="form-body">
                    <div class="form-group row">
                      <div class="col-lg-6 col-md-6 col-sm-12">
                        <label class="col-form-label"><?php echo lang('sub_center_details');?></label>
                        <textarea name="details_ar" class="form-control" rows="8" cols="80"><?php echo $details_ar;?></textarea>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12">
                        <label class="col-form-label"><?php echo lang('sub_center_details_en');?></label>
                        <textarea name="details_en" class="form-control" rows="8" cols="80"><?php echo $details_en;?></textarea>
                      </div>
                    </div>
                  </div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php echo $id;?>">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."?main=".$_GET['main'];?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>
