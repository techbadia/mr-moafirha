
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
                        echo form_open_multipart($FormPath);
                    ?>
                    <?php if(empty($_GET['main'])){ ?>
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <label class="col-form-label"><?php echo lang('sub_center_parent_id');?></label>
                        <select name="parent_id" class="form-control select2">
                          <option  value=""><?php echo lang('Choose');?></option>
                          <?php
                          foreach($centers as $center) {
                            if ($this->session->userdata('lang') == "ar")
                            {
                              $ToTitle = $center->name_ar;
                            }
                            else
                            {
                              $ToTitle = $center->name_en;
                            }
                          ?>
                          <option  value="<?php echo $center->id;?>"><?php echo $ToTitle;?></option >

                        <?php } ?>
                        </select>
                      </div>
                    <?php }else {
                      ?>
                      <input type="hidden" name="parent_id" value="<?php echo $_GET['main'] ?>">
                      <?php
                      } ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('sub_center_name');?></label>
												<input type="text" name="name_ar" class="form-control" value="" required>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('sub_center_name_en');?></label>
												<input type="text" name="name_en" class="form-control" value="" >
											</div>
										</div>
									</div>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('sub_center_dept');?></label>
												<input type="text" name="dept" class="form-control" value="" required>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('sub_center_creditor');?></label>
												<input type="text" name="credit" class="form-control" value="" required>
											</div>
										</div>
									</div>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('sub_center_details');?></label>
                        <textarea name="details_ar" class="form-control" rows="8" cols="80"></textarea>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('sub_center_details_en');?></label>
                        <textarea name="details_en" class="form-control" rows="8" cols="80"></textarea>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<?php
													$company_id = intval($this->session->userdata('company_id'));
													?>
													<input type="hidden" name="company_id" value="<?php echo $company_id;?>">

													<a href="<?php echo base_url().$Segment1."/".$Segment2."?main=".$_GET['main'];?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>
