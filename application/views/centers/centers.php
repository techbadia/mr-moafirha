  <?php $base_url = base_url(); ?>
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('cost_center_name');?></th>
												<th><?php echo lang('cost_center_subs');?></th>
												<th><?php echo lang('cost_center_dept');?></th>
												<th><?php echo lang('cost_center_creditor');?></th>
												<th><?php echo lang('cost_center_net');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$store_id = $DataRows_Row->id;
                        $query = $this->db->query("select * from sub_centers where parent_id = ".$store_id." order by id asc");
                        $rows = $query->result();
                        $dept = 0;
                        $credit = 0;
                        foreach ($rows as $row) {
                          $dept += $row->dept;
                          $credit += $row->credit;
                        }
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>

												<td><?php echo $DataRows_Row->name_ar;?></td>
												<td><a href="<?php echo $base_url.'/centers/sub_centers?main='.$DataRows_Row->id ?>" class="btn btn-info"> <?php echo count(  $rows) ?> <?php echo lang('sub_center');?></a> </td>
												<td> <?php echo $dept ?> <?php echo lang('SAR');?> </td>
												<td> <?php echo $credit ?> <?php echo lang('SAR');?> </td>
												<td> <?php echo $dept - $credit ?> <?php echo lang('SAR');?> </td>


												<td style="text-align:center">
													<a href="<?php echo $base_url.$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i>
													</a>
												</td>
												<td style="text-align:center">


                          <a href="<?php echo $base_url.$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>" onclick="return confirm('<?php echo lang("If you delete main cost center its subs will be deleted too .Are you sure ?") ?>')">
                            <i class="flaticon-delete text-danger icon-lg"></i>
                          </a>												</td>

											</tr>
											<?php
											}
											?>
										</tbody>

									</table>
