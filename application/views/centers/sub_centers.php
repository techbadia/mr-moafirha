<?php $base_url = base_url(); ?>
                <!--begin: Datatable -->
                <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th><?php echo lang('sub_center_code');?></th>
                      <th><?php echo lang('sub_center_name');?></th>
                      <th><?php echo lang('sub_center_dept');?></th>
                      <th><?php echo lang('sub_center_creditor');?></th>
                      <th><?php echo lang('sub_center_net');?></th>
                      <th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
                      <th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach($DataRows as $DataRows_Row) {
                      $store_id = $DataRows_Row->id;
                    ?>
                    <tr>
                      <td><?php echo $DataRows_Row->id;?></td>
                      <td><?php echo $DataRows_Row->r_id;?></td>

                      <td><?php echo $DataRows_Row->name_ar;?></td>
                      <td><?php echo $DataRows_Row->dept;?> <?php echo lang('SAR');?> </td>
                      <td><?php echo $DataRows_Row->credit;?> <?php echo lang('SAR');?> </td>
                      <td> <?php echo $DataRows_Row->dept - $DataRows_Row->credit;?> <?php echo lang('SAR');?> </td>


                      <td style="text-align:center">
                        <a href="<?php echo $base_url.$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id."?main=".$_GET['main'];?>">
                          <i class="flaticon-edit-1 text-primary icon-lg"></i>
                        </a>
                      </td>
                      <td style="text-align:center">


                        <a href="<?php echo $base_url.$Segment1."/".$Segment2."/delete/".$DataRows_Row->id."?main=".$_GET['main'];?>" onclick="return confirm('<?php echo lang("Are you sure?") ?>')">
                          <i class="flaticon-delete text-danger icon-lg"></i>
                        </a>												</td>

                    </tr>
                    <?php
                    }
                    ?>
                  </tbody>

                </table>
