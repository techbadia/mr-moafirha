<div class="col-12">
    <h3><?php echo $this->lang->line('tic_category');?></h3>
</div>
<?php
foreach($Category as $Category_Row) {
    if ($this->session->userdata('lang') == "ar")
    {
        $CategoryTitle = $Category_Row->ar_title;
    }
    else
    {
        $CategoryTitle = $Category_Row->en_title;
    }
    $CategoryID = $Category_Row->id;
?>
<div class="col-xl-3">
    <!--begin::Stats Widget 16-->
    <a href="<?php echo base_url();?>support/tic_ticket/category/<?php echo $CategoryID;?>" class="card card-custom card-stretch gutter-b" style="background-color: #f44336">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <polygon fill="#000000" opacity="0.3" points="5 15 3 21.5 9.5 19.5"/>
                        <path d="M13.5,21 C8.25329488,21 4,16.7467051 4,11.5 C4,6.25329488 8.25329488,2 13.5,2 C18.7467051,2 23,6.25329488 23,11.5 C23,16.7467051 18.7467051,21 13.5,21 Z M8.5,13 C9.32842712,13 10,12.3284271 10,11.5 C10,10.6715729 9.32842712,10 8.5,10 C7.67157288,10 7,10.6715729 7,11.5 C7,12.3284271 7.67157288,13 8.5,13 Z M13.5,13 C14.3284271,13 15,12.3284271 15,11.5 C15,10.6715729 14.3284271,10 13.5,10 C12.6715729,10 12,10.6715729 12,11.5 C12,12.3284271 12.6715729,13 13.5,13 Z M18.5,13 C19.3284271,13 20,12.3284271 20,11.5 C20,10.6715729 19.3284271,10 18.5,10 C17.6715729,10 17,10.6715729 17,11.5 C17,12.3284271 17.6715729,13 18.5,13 Z" fill="#000000"/>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
            <?php echo $this->lang->line('tickets_count');?> : 
            <?php
            $TicketsCount = $this->M_tic_category->tic_ticket_Count($CategoryID);
            echo $TicketsCount;
            ?>
            </div>
            <div class="font-weight-bold text-white font-size-sm">
            <?php
            echo $CategoryTitle;
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>
<?php
}
?>

<div class="col-12">
    <h3><?php echo $this->lang->line('tic_priority');?></h3>
</div>
<?php
foreach($Priority as $Priority_Row) {
    if ($this->session->userdata('lang') == "ar")
    {
        $PriorityTitle = $Priority_Row->ar_title;
    }
    else
    {
        $PriorityTitle = $Priority_Row->en_title;
    }
    $PriorityID = $Priority_Row->id;
?>
<div class="col-xl-3">
    <!--begin::Stats Widget 16-->
    <a href="<?php echo base_url();?>support/tic_ticket/priority/<?php echo $PriorityID;?>" class="card card-custom card-stretch gutter-b" style="background-color: #B71C1C">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <polygon fill="#000000" opacity="0.3" points="5 15 3 21.5 9.5 19.5"/>
                        <path d="M13.5,21 C8.25329488,21 4,16.7467051 4,11.5 C4,6.25329488 8.25329488,2 13.5,2 C18.7467051,2 23,6.25329488 23,11.5 C23,16.7467051 18.7467051,21 13.5,21 Z M9,8 C8.44771525,8 8,8.44771525 8,9 C8,9.55228475 8.44771525,10 9,10 L18,10 C18.5522847,10 19,9.55228475 19,9 C19,8.44771525 18.5522847,8 18,8 L9,8 Z M9,12 C8.44771525,12 8,12.4477153 8,13 C8,13.5522847 8.44771525,14 9,14 L14,14 C14.5522847,14 15,13.5522847 15,13 C15,12.4477153 14.5522847,12 14,12 L9,12 Z" fill="#000000"/>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
            <?php echo $this->lang->line('tickets_count');?> : 
            <?php
            $TicketsCount = $this->M_tic_priority->tic_ticket_Count($PriorityID);
            echo $TicketsCount;
            ?>
            </div>
            <div class="font-weight-bold text-white font-size-sm">
            <?php
            echo $PriorityTitle;
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>
<?php
}
?>

<div class="col-12">
    <h3><?php echo $this->lang->line('tic_status');?></h3>
</div>
<?php
foreach($Status as $Status_Row) {
    if ($this->session->userdata('lang') == "ar")
    {
        $StatusTitle = $Status_Row->ar_title;
    }
    else
    {
        $StatusTitle = $Status_Row->en_title;
    }
    $StatusID = $Status_Row->id;
?>
<div class="col-xl-3">
    <!--begin::Stats Widget 16-->
    <a href="<?php echo base_url();?>support/tic_ticket/status/<?php echo $StatusID;?>" class="card card-custom card-stretch gutter-b" style="background-color: #F44336">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <path d="M21.9999843,15.009808 L22.0249378,15 L22.0249378,19.5857864 C22.0249378,20.1380712 21.5772226,20.5857864 21.0249378,20.5857864 C20.7597213,20.5857864 20.5053674,20.4804296 20.317831,20.2928932 L18.0249378,18 L5,18 C3.34314575,18 2,16.6568542 2,15 L2,6 C2,4.34314575 3.34314575,3 5,3 L19,3 C20.6568542,3 22,4.34314575 22,6 L22,15 C22,15.0032706 21.9999948,15.0065399 21.9999843,15.009808 Z M6.16794971,10.5547002 C7.67758127,12.8191475 9.64566871,14 12,14 C14.3543313,14 16.3224187,12.8191475 17.8320503,10.5547002 C18.1384028,10.0951715 18.0142289,9.47430216 17.5547002,9.16794971 C17.0951715,8.86159725 16.4743022,8.98577112 16.1679497,9.4452998 C15.0109146,11.1808525 13.6456687,12 12,12 C10.3543313,12 8.9890854,11.1808525 7.83205029,9.4452998 C7.52569784,8.98577112 6.90482849,8.86159725 6.4452998,9.16794971 C5.98577112,9.47430216 5.86159725,10.0951715 6.16794971,10.5547002 Z" fill="#000000"/>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
            <?php echo $this->lang->line('tickets_count');?> : 
            <?php
            $TicketsCount = $this->M_tic_status->tic_ticket_Count($StatusID);
            echo $TicketsCount;
            ?>
            </div>
            <div class="font-weight-bold text-white font-size-sm">
            <?php
            echo $StatusTitle;
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>
<?php
}
?>

<div class="col-12">
    <h3><?php echo $this->lang->line('tic_ticket_assign_to');?></h3>
</div>
<?php
$CollectAssignTo = $this->M_tic_ticket->CollectAssignTo();
foreach($CollectAssignTo as $CollectAssignTo_Row) {
    $UserID = $CollectAssignTo_Row->assign_to;
    $UserData = $this->M_usr_users->GetRow($UserID);
    $UserFullName = $UserData->fullname;
?>
<div class="col-xl-3">
    <!--begin::Stats Widget 16-->
    <a href="<?php echo base_url();?>support/tic_ticket/assign_to/<?php echo $UserID;?>" class="card card-custom card-stretch gutter-b" style="background-color: #E57373">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <path d="M4.875,20.75 C4.63541667,20.75 4.39583333,20.6541667 4.20416667,20.4625 L2.2875,18.5458333 C1.90416667,18.1625 1.90416667,17.5875 2.2875,17.2041667 C2.67083333,16.8208333 3.29375,16.8208333 3.62916667,17.2041667 L4.875,18.45 L8.0375,15.2875 C8.42083333,14.9041667 8.99583333,14.9041667 9.37916667,15.2875 C9.7625,15.6708333 9.7625,16.2458333 9.37916667,16.6291667 L5.54583333,20.4625 C5.35416667,20.6541667 5.11458333,20.75 4.875,20.75 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                        <path d="M2,11.8650466 L2,6 C2,4.34314575 3.34314575,3 5,3 L19,3 C20.6568542,3 22,4.34314575 22,6 L22,15 C22,15.0032706 21.9999948,15.0065399 21.9999843,15.009808 L22.0249378,15 L22.0249378,19.5857864 C22.0249378,20.1380712 21.5772226,20.5857864 21.0249378,20.5857864 C20.7597213,20.5857864 20.5053674,20.4804296 20.317831,20.2928932 L18.0249378,18 L12.9835977,18 C12.7263047,14.0909841 9.47412135,11 5.5,11 C4.23590829,11 3.04485894,11.3127315 2,11.8650466 Z M6,7 C5.44771525,7 5,7.44771525 5,8 C5,8.55228475 5.44771525,9 6,9 L15,9 C15.5522847,9 16,8.55228475 16,8 C16,7.44771525 15.5522847,7 15,7 L6,7 Z" fill="#000000"/>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-dark font-weight-bolder font-size-h5 mb-2 mt-5">
            <?php echo $this->lang->line('tickets_count');?> : 
            <?php
            $TicketsCount = $this->M_tic_ticket->CollectAssignTo_Count($UserID);
            echo $TicketsCount;
            ?>
            </div>
            <div class="font-weight-bold text-dark font-size-sm">
            <?php
            echo $UserFullName;
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>
<?php
}
?>