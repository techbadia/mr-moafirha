<div class="col-12">
    <h3><?php echo $this->lang->line('ShortCut_Statistics_HR1');?></h3>
</div>
<?php
$Countries = $this->M_hr_country->GetMultiRow();
foreach($Countries as $Countries_Row) {
    if ($this->session->userdata('lang') == "ar")
    {
        $CountryName = $Countries_Row->ar_title;
    }
    else
    {
        $CountryName = $Countries_Row->en_title;
    }
    $CountryID = $Countries_Row->id;
?>
<div class="col-xl-4">
    <!--begin::Stats Widget 16-->
    <a href="#" class="card card-custom card-stretch gutter-b" style="background-color: #4a235a">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <path d="M3.5,3 L5,3 L5,19.5 C5,20.3284271 4.32842712,21 3.5,21 L3.5,21 C2.67157288,21 2,20.3284271 2,19.5 L2,4.5 C2,3.67157288 2.67157288,3 3.5,3 Z" fill="#000000"/>
                        <path d="M6.99987583,2.99995344 L19.754647,2.99999303 C20.3069317,2.99999474 20.7546456,3.44771138 20.7546439,3.99999613 C20.7546431,4.24703684 20.6631995,4.48533385 20.497938,4.66895776 L17.5,8 L20.4979317,11.3310353 C20.8673908,11.7415453 20.8341123,12.3738351 20.4236023,12.7432941 C20.2399776,12.9085564 20.0016794,13 19.7546376,13 L6.99987583,13 L6.99987583,2.99995344 Z" fill="#000000" opacity="0.3"/>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
            <?php echo $CountryName;?> : 
            <?php
            echo $this->M_hr_employee->CountryCount($CountryID);
            ?>
            </div>
            <div class="font-weight-bold text-white font-size-sm">
            <?php
            echo $this->lang->line('employee_count');
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>
<?php
}
?>

<div class="col-12">
    <h3><?php echo $this->lang->line('ShortCut_Statistics_HR2');?></h3>
</div>
<?php
$Education = $this->M_hr_education->GetMultiRow();
foreach($Education as $Education_Row) {
    if ($this->session->userdata('lang') == "ar")
    {
        $EducationName = $Education_Row->ar_title;
    }
    else
    {
        $EducationName = $Education_Row->en_title;
    }
    $EducationID = $Education_Row->id;
?>
<div class="col-xl-4">
    <!--begin::Stats Widget 16-->
    <a href="#" class="card card-custom card-stretch gutter-b" style="background-color: #943126">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <path d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z" fill="#000000"/>
                        <rect fill="#000000" opacity="0.3" transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519) " x="16.3255682" y="2.94551858" width="3" height="18" rx="1"/>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
            <?php echo $EducationName;?> : 
            <?php
            echo $this->M_hr_employee->EducationCount($EducationID);
            ?>
            </div>
            <div class="font-weight-bold text-white font-size-sm">
            <?php
            echo $this->lang->line('employee_count');
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>
<?php
}
?>

<div class="col-12">
    <h3><?php echo $this->lang->line('ShortCut_Statistics_HR3');?></h3>
</div>
<?php
$Job = $this->M_hr_job->GetMultiRow();
foreach($Job as $Job_Row) {
    if ($this->session->userdata('lang') == "ar")
    {
        $JobName = $Job_Row->ar_title;
    }
    else
    {
        $JobName = $Job_Row->en_title;
    }
    $JobID = $Job_Row->id;
?>
<div class="col-xl-4">
    <!--begin::Stats Widget 16-->
    <a href="#" class="card card-custom card-stretch gutter-b" style="background-color: #9c640c">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <path d="M8.40093151,17 L5,17 C3.34314575,17 2,15.1568542 2,13.5 C2,11.8431458 3.34314575,10 5,10 L6,10 L6,8 L21,8 L21,11.5 C21,15.6421356 17.6421356,19 13.5,19 C11.5309185,19 9.73907026,18.2411745 8.40093151,17 Z M6.86504659,15 C6.38614142,14.0940164 6.08736465,13.0781211 6.01640228,12 L5,12 C4.44771525,12 4,12.9477153 4,13.5 C4,14.0522847 4.44771525,15 5,15 L6.86504659,15 Z" fill="#000000"/>
                        <rect fill="#000000" opacity="0.3" x="6" y="21" width="15" height="2" rx="1"/>
                        <path d="M8.11576273,0 L9.27322553,1.15267194 C8.41777098,2.01168201 8.42065331,3.40153019 9.27966338,4.25698473 C9.35322262,4.3302395 9.4318859,4.39818368 9.51506091,4.46030566 L10,4.82249831 L9.02250371,6.13126634 L8.53756462,5.76907368 C8.39249331,5.66072242 8.25529121,5.54221626 8.12699144,5.41444753 C6.62873232,3.92238985 6.62370505,1.49825912 8.11576273,0 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                        <path d="M13.1157627,1 L14.2732255,2.15267194 C13.417771,3.01168201 13.4206533,4.40153019 14.2796634,5.25698473 C14.3532226,5.3302395 14.4318859,5.39818368 14.5150609,5.46030566 L15,5.82249831 L14.0225037,7.13126634 L13.5375646,6.76907368 C13.3924933,6.66072242 13.2552912,6.54221626 13.1269914,6.41444753 C11.6287323,4.92238985 11.6237051,2.49825912 13.1157627,1 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                        <path d="M18.1157627,0 L19.2732255,1.15267194 C18.417771,2.01168201 18.4206533,3.40153019 19.2796634,4.25698473 C19.3532226,4.3302395 19.4318859,4.39818368 19.5150609,4.46030566 L20,4.82249831 L19.0225037,6.13126634 L18.5375646,5.76907368 C18.3924933,5.66072242 18.2552912,5.54221626 18.1269914,5.41444753 C16.6287323,3.92238985 16.6237051,1.49825912 18.1157627,0 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
            <?php echo $JobName;?> : 
            <?php
            echo $this->M_hr_employee->JobCount($JobID);
            ?>
            </div>
            <div class="font-weight-bold text-white font-size-sm">
            <?php
            echo $this->lang->line('employee_count');
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>
<?php
}
?>

<div class="col-12">
    <h3><?php echo $this->lang->line('ShortCut_Statistics_HR4');?></h3>
</div>
<?php
$JobType = $this->M_hr_job_type->GetMultiRow();
foreach($JobType as $JobType_Row) {
    if ($this->session->userdata('lang') == "ar")
    {
        $JobTypeName = $JobType_Row->ar_title;
    }
    else
    {
        $JobTypeName = $JobType_Row->en_title;
    }
    $JobTypeID = $JobType_Row->id;
?>
<div class="col-xl-4">
    <!--begin::Stats Widget 16-->
    <a href="#" class="card card-custom card-stretch gutter-b" style="background-color: #0e6655">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24"/>
                        <path d="M4.85714286,1 L11.7364114,1 C12.0910962,1 12.4343066,1.12568431 12.7051108,1.35473959 L17.4686994,5.3839416 C17.8056532,5.66894833 18,6.08787823 18,6.52920201 L18,19.0833333 C18,20.8738751 17.9795521,21 16.1428571,21 L4.85714286,21 C3.02044787,21 3,20.8738751 3,19.0833333 L3,2.91666667 C3,1.12612489 3.02044787,1 4.85714286,1 Z M8,12 C7.44771525,12 7,12.4477153 7,13 C7,13.5522847 7.44771525,14 8,14 L15,14 C15.5522847,14 16,13.5522847 16,13 C16,12.4477153 15.5522847,12 15,12 L8,12 Z M8,16 C7.44771525,16 7,16.4477153 7,17 C7,17.5522847 7.44771525,18 8,18 L11,18 C11.5522847,18 12,17.5522847 12,17 C12,16.4477153 11.5522847,16 11,16 L8,16 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                        <path d="M6.85714286,3 L14.7364114,3 C15.0910962,3 15.4343066,3.12568431 15.7051108,3.35473959 L20.4686994,7.3839416 C20.8056532,7.66894833 21,8.08787823 21,8.52920201 L21,21.0833333 C21,22.8738751 20.9795521,23 19.1428571,23 L6.85714286,23 C5.02044787,23 5,22.8738751 5,21.0833333 L5,4.91666667 C5,3.12612489 5.02044787,3 6.85714286,3 Z M8,12 C7.44771525,12 7,12.4477153 7,13 C7,13.5522847 7.44771525,14 8,14 L15,14 C15.5522847,14 16,13.5522847 16,13 C16,12.4477153 15.5522847,12 15,12 L8,12 Z M8,16 C7.44771525,16 7,16.4477153 7,17 C7,17.5522847 7.44771525,18 8,18 L11,18 C11.5522847,18 12,17.5522847 12,17 C12,16.4477153 11.5522847,16 11,16 L8,16 Z" fill="#000000" fill-rule="nonzero"/>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
            <?php echo $JobTypeName;?> : 
            <?php
            echo $this->M_hr_employee->JobTypeCount($JobTypeID);
            ?>
            </div>
            <div class="font-weight-bold text-white font-size-sm">
            <?php
            echo $this->lang->line('employee_count');
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>
<?php
}
?>