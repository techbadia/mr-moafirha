<div class="col-12">
    <h3><?php echo $this->lang->line('ShortCut_Statistics_Account1');?></h3>
</div>
<?php
$acc_treasury = intval($this->session->userdata('acc_treasury'));
$AllAccounts = $this->M_fin_treeaccount->GetSubAccounts($acc_treasury);
foreach($AllAccounts as $AllAccounts_Row) {
    if ($this->session->userdata('lang') == "ar")
    {
        $AccountName = $AllAccounts_Row->title;
    }
    else
    {
        $AccountName = $AllAccounts_Row->title_en;
    }
    $AccountID = $AllAccounts_Row->id;
    $AccountCategoryID = $AllAccounts_Row->category_id;
?>
<div class="col-xl-4">
    <!--begin::Stats Widget 13-->
    <a href="" class="card card-custom card-stretch gutter-b" style="background-color: #17202a">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <rect fill="#000000" opacity="0.3" x="11.5" y="2" width="2" height="4" rx="1"/>
                        <rect fill="#000000" opacity="0.3" x="11.5" y="16" width="2" height="5" rx="1"/>
                        <path d="M15.493,8.044 C15.2143319,7.68933156 14.8501689,7.40750104 14.4005,7.1985 C13.9508311,6.98949895 13.5170021,6.885 13.099,6.885 C12.8836656,6.885 12.6651678,6.90399981 12.4435,6.942 C12.2218322,6.98000019 12.0223342,7.05283279 11.845,7.1605 C11.6676658,7.2681672 11.5188339,7.40749914 11.3985,7.5785 C11.2781661,7.74950085 11.218,7.96799867 11.218,8.234 C11.218,8.46200114 11.2654995,8.65199924 11.3605,8.804 C11.4555005,8.95600076 11.5948324,9.08899943 11.7785,9.203 C11.9621676,9.31700057 12.1806654,9.42149952 12.434,9.5165 C12.6873346,9.61150047 12.9723317,9.70966616 13.289,9.811 C13.7450023,9.96300076 14.2199975,10.1308324 14.714,10.3145 C15.2080025,10.4981676 15.6576646,10.7419985 16.063,11.046 C16.4683354,11.3500015 16.8039987,11.7268311 17.07,12.1765 C17.3360013,12.6261689 17.469,13.1866633 17.469,13.858 C17.469,14.6306705 17.3265014,15.2988305 17.0415,15.8625 C16.7564986,16.4261695 16.3733357,16.8916648 15.892,17.259 C15.4106643,17.6263352 14.8596698,17.8986658 14.239,18.076 C13.6183302,18.2533342 12.97867,18.342 12.32,18.342 C11.3573285,18.342 10.4263378,18.1741683 9.527,17.8385 C8.62766217,17.5028317 7.88033631,17.0246698 7.285,16.404 L9.413,14.238 C9.74233498,14.6433354 10.176164,14.9821653 10.7145,15.2545 C11.252836,15.5268347 11.7879973,15.663 12.32,15.663 C12.5606679,15.663 12.7949989,15.6376669 13.023,15.587 C13.2510011,15.5363331 13.4504991,15.4540006 13.6215,15.34 C13.7925009,15.2259994 13.9286662,15.0740009 14.03,14.884 C14.1313338,14.693999 14.182,14.4660013 14.182,14.2 C14.182,13.9466654 14.1186673,13.7313342 13.992,13.554 C13.8653327,13.3766658 13.6848345,13.2151674 13.4505,13.0695 C13.2161655,12.9238326 12.9248351,12.7908339 12.5765,12.6705 C12.2281649,12.5501661 11.8323355,12.420334 11.389,12.281 C10.9583312,12.141666 10.5371687,11.9770009 10.1255,11.787 C9.71383127,11.596999 9.34650161,11.3531682 9.0235,11.0555 C8.70049838,10.7578318 8.44083431,10.3968355 8.2445,9.9725 C8.04816568,9.54816454 7.95,9.03200304 7.95,8.424 C7.95,7.67666293 8.10199848,7.03700266 8.406,6.505 C8.71000152,5.97299734 9.10899753,5.53600171 9.603,5.194 C10.0970025,4.85199829 10.6543302,4.60183412 11.275,4.4435 C11.8956698,4.28516587 12.5226635,4.206 13.156,4.206 C13.9160038,4.206 14.6918294,4.34533194 15.4835,4.624 C16.2751706,4.90266806 16.9686637,5.31433061 17.564,5.859 L15.493,8.044 Z" fill="#000000"/>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-inverse-danger font-weight-bolder font-size-h5 mb-2 mt-5">
                <?php
                if ($this->session->userdata('lang') == "ar")
                {
                    echo $this->lang->line('OneBalance')." ".$AccountName;
                }
                else
                {
                    echo $AccountName." ".$this->lang->line('OneBalance');
                }
                ?> : 
                <?php
                $Today = date("Y-m-d");
                if($this->session->userdata('statistics_filter') == "Today")
                {
                    $StartData = $Today;
                    $EndDate = $Today;
                }
                else if($this->session->userdata('statistics_filter') == "Month")
                {
                    $StartData = date('Y-m-01', strtotime($Today));
                    $EndDate = date('Y-m-t', strtotime($Today));
                }
                else
                {
                    $StartData = date('Y-01-01', strtotime($Today));
                    $EndDate = date('Y-12-31', strtotime($Today));
                }
                $fromdate = $StartData;
                $StartAmount = 0;
                $BeforDay = date('Y-m-d', strtotime('-1 days', strtotime($fromdate)));
                $AmountBeforeJournal = $StartAmount;
                $AmountBeforeJournalData = $this->M_fin_journal->GetRealtedJournalStartDate($AccountID, $fromdate);
                $AmountBeforeJournalValue = 0;
                $RealtedJournalStartDateCount = $this->M_fin_journal->GetRealtedJournalStartDateCount($AccountID, $fromdate);
                $AccountCategoryID = 0;
                if ($RealtedJournalStartDateCount >0)
                {
                    foreach($AmountBeforeJournalData as $AmountBeforeJournalData_Row) {
                        //$AmountBeforeJournalValue = doubleval($AmountBeforeJournalData_Row->thevalue);
                        $AccountData = $this->M_fin_treeaccount->GetRow($AccountID);
                        $AccountCategoryID = $AccountData->category_id;
                        if (($AccountCategoryID ==1) || ($AccountCategoryID ==4))
                        {
                            
                        }
                        else
                            {
                                $AmountBeforeJournal -= doubleval($AmountBeforeJournalData_Row->debit);
                                $AmountBeforeJournal += doubleval($AmountBeforeJournalData_Row->creditor);
                            }
                    }
                }
                $Balance = $AmountBeforeJournal;
                $TotalValue = 0;
                $Serial = 0;
                $Debit = 0;
                $DebitTotal = 0;
                $CreditorTotal = 0;
                $Creditor = 0;
                $CategoryID = 0;
                $TheValue = 0;
                $OtherAccountName = "";
                $MultiRows = $this->M_fin_journal->GetDetailsByAccount($AccountID);
                foreach($MultiRows as $MultiRows_Row) {
                    $Serial += 1;
                    $AccountName = $this->M_fin_treeaccount->GetRow($AccountID);
                    $category_id = $AccountName->category_id;

                    $main_id = $MultiRows_Row->main_id;
                    $JournalMainData = $this->M_fin_journal_main->GetRow($main_id);
                    $details = $JournalMainData->details;
                    $thedate = $JournalMainData->thedate;

                    $Debit += doubleval($MultiRows_Row->debit);
                    $DebitTotal += $Debit;
                    
                    $Creditor += doubleval($MultiRows_Row->creditor);
                    $CreditorTotal += $Creditor;
                    
                    $TheValue = 0;
                    $Balance = $Balance + ($Debit - $Creditor);

                    $Final = 0;
					if (($AccountCategoryID ==1) || ($AccountCategoryID ==4))
					{
						$Final = $StartAmount + $Debit - $Creditor;
						//echo $Debit;
                    }
                    
                    $Final = 0;
					if (($AccountCategoryID ==2) || ($AccountCategoryID ==3) || ($AccountCategoryID ==5))
					{
						$Final = $StartAmount - $Debit + $Creditor;
						//echo $Creditor;
                    }
                    
                }
                echo doubleval($Balance);
                ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 13-->
</div>
<?php
}
?>


<div class="col-12">
    <h3><?php echo $this->lang->line('ShortCut_Statistics_Account2');?></h3>
</div>
<?php
$acc_bank = intval($this->session->userdata('acc_bank'));
$AllAccounts = $this->M_fin_treeaccount->GetSubAccounts($acc_bank);
foreach($AllAccounts as $AllAccounts_Row) {
    if ($this->session->userdata('lang') == "ar")
    {
        $AccountName = $AllAccounts_Row->title;
    }
    else
    {
        $AccountName = $AllAccounts_Row->title_en;
    }
    $AccountID = $AllAccounts_Row->id;
    $AccountCategoryID = $AllAccounts_Row->category_id;
?>
<div class="col-xl-4">
    <!--begin::Stats Widget 13-->
    <a href="" class="card card-custom card-stretch gutter-b" style="background-color: #6e2c00">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <rect fill="#000000" opacity="0.3" x="11.5" y="2" width="2" height="4" rx="1"/>
                        <rect fill="#000000" opacity="0.3" x="11.5" y="16" width="2" height="5" rx="1"/>
                        <path d="M15.493,8.044 C15.2143319,7.68933156 14.8501689,7.40750104 14.4005,7.1985 C13.9508311,6.98949895 13.5170021,6.885 13.099,6.885 C12.8836656,6.885 12.6651678,6.90399981 12.4435,6.942 C12.2218322,6.98000019 12.0223342,7.05283279 11.845,7.1605 C11.6676658,7.2681672 11.5188339,7.40749914 11.3985,7.5785 C11.2781661,7.74950085 11.218,7.96799867 11.218,8.234 C11.218,8.46200114 11.2654995,8.65199924 11.3605,8.804 C11.4555005,8.95600076 11.5948324,9.08899943 11.7785,9.203 C11.9621676,9.31700057 12.1806654,9.42149952 12.434,9.5165 C12.6873346,9.61150047 12.9723317,9.70966616 13.289,9.811 C13.7450023,9.96300076 14.2199975,10.1308324 14.714,10.3145 C15.2080025,10.4981676 15.6576646,10.7419985 16.063,11.046 C16.4683354,11.3500015 16.8039987,11.7268311 17.07,12.1765 C17.3360013,12.6261689 17.469,13.1866633 17.469,13.858 C17.469,14.6306705 17.3265014,15.2988305 17.0415,15.8625 C16.7564986,16.4261695 16.3733357,16.8916648 15.892,17.259 C15.4106643,17.6263352 14.8596698,17.8986658 14.239,18.076 C13.6183302,18.2533342 12.97867,18.342 12.32,18.342 C11.3573285,18.342 10.4263378,18.1741683 9.527,17.8385 C8.62766217,17.5028317 7.88033631,17.0246698 7.285,16.404 L9.413,14.238 C9.74233498,14.6433354 10.176164,14.9821653 10.7145,15.2545 C11.252836,15.5268347 11.7879973,15.663 12.32,15.663 C12.5606679,15.663 12.7949989,15.6376669 13.023,15.587 C13.2510011,15.5363331 13.4504991,15.4540006 13.6215,15.34 C13.7925009,15.2259994 13.9286662,15.0740009 14.03,14.884 C14.1313338,14.693999 14.182,14.4660013 14.182,14.2 C14.182,13.9466654 14.1186673,13.7313342 13.992,13.554 C13.8653327,13.3766658 13.6848345,13.2151674 13.4505,13.0695 C13.2161655,12.9238326 12.9248351,12.7908339 12.5765,12.6705 C12.2281649,12.5501661 11.8323355,12.420334 11.389,12.281 C10.9583312,12.141666 10.5371687,11.9770009 10.1255,11.787 C9.71383127,11.596999 9.34650161,11.3531682 9.0235,11.0555 C8.70049838,10.7578318 8.44083431,10.3968355 8.2445,9.9725 C8.04816568,9.54816454 7.95,9.03200304 7.95,8.424 C7.95,7.67666293 8.10199848,7.03700266 8.406,6.505 C8.71000152,5.97299734 9.10899753,5.53600171 9.603,5.194 C10.0970025,4.85199829 10.6543302,4.60183412 11.275,4.4435 C11.8956698,4.28516587 12.5226635,4.206 13.156,4.206 C13.9160038,4.206 14.6918294,4.34533194 15.4835,4.624 C16.2751706,4.90266806 16.9686637,5.31433061 17.564,5.859 L15.493,8.044 Z" fill="#000000"/>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-inverse-danger font-weight-bolder font-size-h5 mb-2 mt-5">
                <?php
                if ($this->session->userdata('lang') == "ar")
                {
                    echo $this->lang->line('OneBalance')." ".$AccountName;
                }
                else
                {
                    echo $AccountName." ".$this->lang->line('OneBalance');
                }
                ?> : 
                <?php
                $Today = date("Y-m-d");
                if($this->session->userdata('statistics_filter') == "Today")
                {
                    $StartData = $Today;
                    $EndDate = $Today;
                }
                else if($this->session->userdata('statistics_filter') == "Month")
                {
                    $StartData = date('Y-m-01', strtotime($Today));
                    $EndDate = date('Y-m-t', strtotime($Today));
                }
                else
                {
                    $StartData = date('Y-01-01', strtotime($Today));
                    $EndDate = date('Y-12-31', strtotime($Today));
                }
                $fromdate = $StartData;
                $StartAmount = 0;
                $BeforDay = date('Y-m-d', strtotime('-1 days', strtotime($fromdate)));
                $AmountBeforeJournal = $StartAmount;
                $AmountBeforeJournalData = $this->M_fin_journal->GetRealtedJournalStartDate($AccountID, $fromdate);
                $AmountBeforeJournalValue = 0;
                $RealtedJournalStartDateCount = $this->M_fin_journal->GetRealtedJournalStartDateCount($AccountID, $fromdate);
                $AccountCategoryID = 0;
                if ($RealtedJournalStartDateCount >0)
                {
                    foreach($AmountBeforeJournalData as $AmountBeforeJournalData_Row) {
                        //$AmountBeforeJournalValue = doubleval($AmountBeforeJournalData_Row->thevalue);
                        $AccountData = $this->M_fin_treeaccount->GetRow($AccountID);
                        $AccountCategoryID = $AccountData->category_id;
                        if (($AccountCategoryID ==1) || ($AccountCategoryID ==4))
                        {
                            
                        }
                        else
                            {
                                $AmountBeforeJournal -= doubleval($AmountBeforeJournalData_Row->debit);
                                $AmountBeforeJournal += doubleval($AmountBeforeJournalData_Row->creditor);
                            }
                    }
                }
                $Balance = $AmountBeforeJournal;
                $TotalValue = 0;
                $Serial = 0;
                $Debit = 0;
                $DebitTotal = 0;
                $CreditorTotal = 0;
                $Creditor = 0;
                $CategoryID = 0;
                $TheValue = 0;
                $OtherAccountName = "";
                $MultiRows = $this->M_fin_journal->GetDetailsByAccount($AccountID);
                foreach($MultiRows as $MultiRows_Row) {
                    $Serial += 1;
                    $AccountName = $this->M_fin_treeaccount->GetRow($AccountID);
                    $category_id = $AccountName->category_id;

                    $main_id = $MultiRows_Row->main_id;
                    $JournalMainData = $this->M_fin_journal_main->GetRow($main_id);
                    $details = $JournalMainData->details;
                    $thedate = $JournalMainData->thedate;

                    $Debit += doubleval($MultiRows_Row->debit);
                    $DebitTotal += $Debit;
                    
                    $Creditor += doubleval($MultiRows_Row->creditor);
                    $CreditorTotal += $Creditor;
                    
                    $TheValue = 0;
                    $Balance = $Balance + ($Debit - $Creditor);

                    $Final = 0;
					if (($AccountCategoryID ==1) || ($AccountCategoryID ==4))
					{
						$Final = $StartAmount + $Debit - $Creditor;
						//echo $Debit;
                    }
                    
                    $Final = 0;
					if (($AccountCategoryID ==2) || ($AccountCategoryID ==3) || ($AccountCategoryID ==5))
					{
						$Final = $StartAmount - $Debit + $Creditor;
						//echo $Creditor;
                    }
                    
                }
                echo doubleval($Balance);
                ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 13-->
</div>
<?php
}
?>