<?php
$Products = $this->M_product->GetMultiRow();
foreach($Products as $Products_Row) {
    if ($this->session->userdata('lang') == "ar")
    {
        $ProductName = $Products_Row->title;
    }
    else
    {
        $ProductName = $Products_Row->title_en;
    }
    $ProductID = $Products_Row->id;
    $Quantity = $this->M_manu_manufacturing_orders_items->GetCount($ProductID)->quantity;
    if($Quantity > 0) {
?>
<div class="col-xl-4">
    <!--begin::Stats Widget 16-->
    <a href="#" class="card card-custom card-stretch gutter-b" style="background-color: #21618c">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <path d="M3.5,3 L5,3 L5,19.5 C5,20.3284271 4.32842712,21 3.5,21 L3.5,21 C2.67157288,21 2,20.3284271 2,19.5 L2,4.5 C2,3.67157288 2.67157288,3 3.5,3 Z" fill="#000000"/>
                        <path d="M6.99987583,2.99995344 L19.754647,2.99999303 C20.3069317,2.99999474 20.7546456,3.44771138 20.7546439,3.99999613 C20.7546431,4.24703684 20.6631995,4.48533385 20.497938,4.66895776 L17.5,8 L20.4979317,11.3310353 C20.8673908,11.7415453 20.8341123,12.3738351 20.4236023,12.7432941 C20.2399776,12.9085564 20.0016794,13 19.7546376,13 L6.99987583,13 L6.99987583,2.99995344 Z" fill="#000000" opacity="0.3"/>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
            <?php echo $ProductName;?> : 
            <?php
            echo $Quantity;
            ?>
            </div>
            <div class="font-weight-bold text-white font-size-sm">
            <?php
            echo $this->lang->line('manu_manufacturing_orders_count');
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>
<?php
    }
}
?>