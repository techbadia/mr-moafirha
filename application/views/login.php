<!DOCTYPE html>
<html lang="ar">
<?php $base_url = base_url(); ?>
	<!--begin::Head-->
	<head>
		<base href="">
		<meta charset="utf-8" />
		<title><?php echo $this->session->userdata('package_title');?></title>
		<meta name="description" content="Login page example" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<link rel="canonical" href="https://keenthemes.com/metronic" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Custom Styles(used by this page)-->
		<link href="<?php echo $base_url;?>assets/1/assets/css/pace.min.css" rel="stylesheet" type="text/css" />
		<!--end::Page Custom Styles-->
		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="<?php echo $base_url;?>assets/1/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />


		<link href="<?php echo $base_url;?>assets/1/assets/css/app.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $base_url;?>assets/1/assets/css/icons.css" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles-->

		<link rel="shortcut icon" href="<?php echo $base_url;?>assets/media/logos/favicon.ico" />
		<style>


		    @import url(https://fonts.googleapis.com/earlyaccess/amiri.css);
@import url(https://fonts.googleapis.com/earlyaccess/droidarabickufi.css);
@import url(https://fonts.googleapis.com/earlyaccess/droidarabicnaskh.css);
@import url(https://fonts.googleapis.com/earlyaccess/lateef.css);
@import url(https://fonts.googleapis.com/earlyaccess/scheherazade.css);
@import url(https://fonts.googleapis.com/earlyaccess/thabit.css);


.amiri{font-family: 'Amiri', serif;}
.droid-arabic-kufi{font-family: 'Droid Arabic Kufi', serif;}
.droid-arabic-naskh{font-family: 'Droid Arabic Naskh', serif;}
.lateef{font-family: 'Lateef', serif;}
.scheherazade{font-family: 'Scheherazade', serif;}
.thabit{font-family: 'Thabit', serif;}

body{
	direction: rtl;
}
.my-image{
	text-align: right !important;
}
.text-start {
    text-align: right!important;
}
		</style>
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body  style="font-family: 'Droid Arabic Kufi', serif;">
	<body>
		<div class="authentication-reset-password d-flex align-items-center justify-content-center">
	<!-- wrapper -->
	<div class="wrapper">
			<div class="row">
				<div class="col-12 col-lg-10 mx-auto">
					<div class="card">
						<div class="row g-0">
							<div class="col-lg-5 border-end">
								<div class="card-body">
									<div class="p-5">
										<div class="text-start">
											<?php if(!empty($_SESSION['success'])){ ?>
												<label class="alert alert-success alert-block text-left" dir="rtl"><?php echo $_SESSION['success'] ?></label>
											<?php } ?>
											<img src="<?php echo $base_url;?>assets/package_logo/login.png" width="180" alt="">
								         	<!--<img src="<?php echo $base_url;?>assets/package_logo/<?php echo $this->session->userdata('PackageName');?>.png" width="180" alt="">-->

										</div>
										<form action="<?php echo $base_url;?>login/checklogin" method="post" novalidate="novalidate" id="kt_login_signin_form">
										<h4 class="mt-5 font-weight-bold ">تسجيل الدخول</h4>
										<p class="text-muted">من فضلك قم بإدخال اسم المستخدم وكلمة المرور</p>

										<div class="mb-3">
											<label class="form-label">اسم المستخدم</label>

											<input class="form-control form-control-solid h-auto" type="text" placeholder="اسم المستخدم" name="username" autocomplete="off" />
										</div>
										<div class="mb-3 mt-5">
											<label class="form-label">كلمة المرور</label>
											<input class="form-control form-control-solid h-auto" type="password" placeholder="كلمة المرور" name="password" autocomplete="off" />
										</div>
										<div class="d-grid gap-2">



										<button type="submit" id="kt_login_signin_submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3">تسجيل الدخول</button>

										<a href="<?php echo $base_url."login/forgotPassword" ?>" class="btn btn-light"> نسيت كلمة السر <i class='bx bx-arrow-back mr-1'></i></a>


									</div>

									</form>
									</div>
								</div>
							</div>
							<div class="col-lg-7">
															<img src="<?php echo $base_url;?>upload/login-logo.jpg" class="card-img login-img h-100 my-image" alt="...">

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end wrapper -->



	<script src="<?php echo $base_url;?>assets/1/assets/js/pace.min.js"></script>
</body>

</html>
