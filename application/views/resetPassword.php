<!DOCTYPE html>
<html lang="en">
<?php $base_url = base_url(); ?>
	<!--begin::Head-->
	<head>
		<base href="">
		<meta charset="utf-8" />
		<title><?php echo $this->session->userdata('package_title');?></title>
		<meta name="description" content="Login page example" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<link rel="canonical" href="https://keenthemes.com/metronic" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Custom Styles(used by this page)-->
		<link href="<?php echo $base_url;?>assets/1/assets/css/pace.min.css" rel="stylesheet" type="text/css" />
		<!--end::Page Custom Styles-->
		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="<?php echo $base_url;?>assets/1/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />


		<link href="<?php echo $base_url;?>assets/1/assets/css/app.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $base_url;?>assets/1/assets/css/icons.css" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles-->

		<link rel="shortcut icon" href="<?php echo $base_url;?>assets/media/logos/favicon.ico" />
		<style>


		    @import url(https://fonts.googleapis.com/earlyaccess/amiri.css);
@import url(https://fonts.googleapis.com/earlyaccess/droidarabickufi.css);
@import url(https://fonts.googleapis.com/earlyaccess/droidarabicnaskh.css);
@import url(https://fonts.googleapis.com/earlyaccess/lateef.css);
@import url(https://fonts.googleapis.com/earlyaccess/scheherazade.css);
@import url(https://fonts.googleapis.com/earlyaccess/thabit.css);

body{
	direction: rtl;
}
.amiri{font-family: 'Amiri', serif;}
.droid-arabic-kufi{font-family: 'Droid Arabic Kufi', serif;}
.droid-arabic-naskh{font-family: 'Droid Arabic Naskh', serif;}
.lateef{font-family: 'Lateef', serif;}
.scheherazade{font-family: 'Scheherazade', serif;}
.thabit{font-family: 'Thabit', serif;}

.text-start {
    text-align: right!important;
}


		</style>
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body  style="font-family: 'Droid Arabic Kufi', serif;">
	<body>
		<div class="authentication-reset-password d-flex align-items-center justify-content-center">
	<!-- wrapper -->
	<div class="wrapper">
			<div class="row">
				<div class="col-12 col-lg-10 mx-auto">
					<div class="card">
						<div class="row g-0">
							<div class="col-lg-5 border-end">
								<div class="card-body">
									<?php if(!empty($_SESSION['error'])){ ?>
										<label class="alert alert-danger alert-block text-left" dir="rtl"><?php echo $_SESSION['error'] ?></label>
									<?php } ?>

									<div class="p-5">
										<div class="text-start">
											<img src="<?php echo $base_url;?>assets/package_logo/login.png" width="180" alt="">
								         	<!--<img src="<?php echo $base_url;?>assets/package_logo/<?php echo $this->session->userdata('PackageName');?>.png" width="180" alt="">-->

										</div>
										<form action="<?php echo $base_url;?>login/changePassword" method="post" novalidate="novalidate" id="kt_login_signin_form">
										<h4 class="mt-5 font-weight-bold ">تغيير كلمة المرور</h4>

										<div class="mb-3">
											<label class="form-label">كلمة المرور الجديدة</label>
											<input class="form-control form-control-solid h-auto" required type="password"  name="password" autocomplete="off" />
										</div>
										<div class="mb-3">
											<label class="form-label">تأكيد كلمة المرور </label>
											<input class="form-control form-control-solid h-auto" required type="password"  name="confirm_password" autocomplete="off" />
										</div>

										<div class="d-grid gap-2">



										<button type="submit" id="kt_login_signin_submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3">تغيير</button>



									</div>

									</form>
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<img src="<?php echo $base_url;?>upload/login-logo.jpg" class="card-img login-img h-100 my-image" alt="...">

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end wrapper -->



	<script src="<?php echo $base_url;?>assets/1/assets/js/pace.min.js"></script>
</body>

</html>
