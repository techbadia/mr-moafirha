<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<!--begin::Info-->
		<?php
		$Segment1 = $this->uri->segment(1);
		$Segment2 = $this->uri->segment(2);
		$Segment3 = $this->uri->segment(3);
		$Segment4 = $this->uri->segment(4);
		if(($Segment1 == "") || ($Segment1 == "home") || ($Segment1 == "erp")) {
		?>
		<div class="d-flex align-items-center flex-wrap mr-2">
			<!--begin::Page Title-->
			<h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"><?php echo $this->lang->line('Dashboard');?></h5>
			<!--end::Page Title-->
		</div>
		<?php
		}
		else
		{
			$ModuleName = "";
			$GetByFolderCount = $this->M_app_module->GetByFolderCount($Segment1);
			if($GetByFolderCount > 0)
			{
				$ModuleData = $this->M_app_module->GetByFolder($Segment1);
				if ($this->session->userdata('lang') == "ar")
				{
					$ModuleName = $ModuleData->ar_title;
				}
				else
				{
					$ModuleName = $ModuleData->en_title;
				}
			}
			else
			{
				switch ($Segment1) {
					case "statistics_account":
						$ModuleName = $this->lang->line('ShortCut_Statistics_Account');
						break;
					case "statistics_purchase":
						$ModuleName = $this->lang->line('ShortCut_Statistics_Purchase');
						break;
					case "statistics_sales":
						$ModuleName = $this->lang->line('ShortCut_Statistics_Sales');
						break;
					case "statistics_store":
						$ModuleName = $this->lang->line('ShortCut_Statistics_Store');
						break;
					case "statistics_hr":
						$ModuleName = $this->lang->line('ShortCut_Statistics_HR');
						break;
					case "statistics_manufacturing":
						$ModuleName = $this->lang->line('ShortCut_Statistics_Manufacturing');
						break;
					case "statistics_service":
						$ModuleName = $this->lang->line('ShortCut_Statistics_Service');
						break;
					default:
						$ModuleName = $this->lang->line('ShortCut_Statistics_Technical');
				}
			}
			?>
		<div class="d-flex align-items-center flex-wrap mr-2">
			<!--begin::Page Title-->
			<h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"><?php echo $ModuleName;?></h5>
			<!--end::Page Title-->
			<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<?php
				$CheckPageData = $this->M_app_module_page->GetCount($Segment2);
				if($CheckPageData > 0) {
				$PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
				$PageName = "";
				if ($this->session->userdata('lang') == "ar")
				{
					$PageName = $PageData->ar_title;
				}
				else
				{
					$PageName = $PageData->en_title;
				}
				?>
				<li class="breadcrumb-item">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="text-muted"><?php echo $PageName;?></a>
				</li>
				<?php
				}
				if($Segment3 == "insertform")
				{
				?>
				<li class="breadcrumb-item">
				<a href="#" class="text-muted"><?php echo $PageName;?></a>
				</li>
				<?php
				}
				?>
				
			</ul>
		</div>
		<?php
		}
		?>
		<!--end::Info-->
		<?php
		if(($Segment1 == "") || ($Segment1 == "home") || ($Segment1 == $this->session->userdata('PackageName')) || 
		($Segment1 == "statistics_account") || ($Segment1 == "statistics_purchase") || ($Segment1 == "statistics_sales") || 
		($Segment1 == "statistics_store") || ($Segment1 == "statistics_hr") || ($Segment1 == "statistics_manufacturing") || 
		($Segment1 == "statistics_service") || ($Segment1 == "statistics_technical")) {
		?>
		<!--begin::Toolbar-->
		<div class="d-flex align-items-center">
			<!--begin::Actions-->
			<a href="<?php echo base_url()."home/filter_today";?>" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
			<?php echo $this->lang->line('Today');?>
			</a>
			<a href="<?php echo base_url();?>home/filter_month" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
			<?php echo $this->lang->line('Month');?>
			</a>
			<a href="<?php echo base_url()."home/filter_year";?>" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
			<?php echo $this->lang->line('Year');?>
			</a>
			<!--end::Actions-->
			<!--begin::Daterange-->
			<!--
			<a href="#" class="btn btn-sm btn-light font-weight-bold mr-2" id="kt_dashboard_daterangepicker" data-toggle="tooltip" title="Select dashboard daterange" data-placement="left">
				<span class="text-muted font-size-base font-weight-bold mr-2" id="kt_dashboard_daterangepicker_title"><?php echo $this->lang->line('Today');?></span>
				<span class="text-primary font-size-base font-weight-bolder" id="kt_dashboard_daterangepicker_date">Aug 16</span>
			</a>
			-->
			<!--end::Daterange-->
		</div>
		<!--end::Toolbar-->
		<?php } ?>
	</div>
</div>