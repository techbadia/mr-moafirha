<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
	<!--begin::Subheader-->
	<div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
		<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
			<!--begin::Info-->
			<div class="d-flex align-items-center flex-wrap mr-1">
				<!--begin::Page Heading-->
				<div class="d-flex align-items-baseline flex-wrap mr-5">
					<?php
					$Segment1 = $this->uri->segment(1);
					$Segment2 = $this->uri->segment(2);
					$Segment3 = $this->uri->segment(3);
					$Segment4 = $this->uri->segment(4);

					$ModuleName = "";
					if($Segment1 == "branche")
					{
					$ModuleName = $this->lang->line('Branches');
					}
					else if(($Segment1 == "mydata") && ($Segment2 == "repair"))
					{
					$ModuleName = $this->lang->line('Databse_Repair');
					$PageDetails = $this->lang->line('Databse_Repair');
					}
					else if(($Segment1 == "mydata") && ($Segment2 == "deleterows"))
					{
					$ModuleName = $this->lang->line('DataDelete');
					$PageDetails = $this->lang->line('DataDelete');
					}
					else if(($Segment1 == "mydata") && ($Segment2 == "optimize"))
					{
					$ModuleName = $this->lang->line('Databse_Optimize');
					$PageDetails = $this->lang->line('Databse_Optimize');
					}
					else if(($Segment1 == "mydata") && ($Segment2 == "backup"))
					{
					$ModuleName = $this->lang->line('DatabaseBackeup');
					$PageDetails = $this->lang->line('DatabaseBackeup');
					}
					else if(($Segment1 == "pos_sys") && ($Segment2 == "tec_stores"))
					{
					$ModuleName = $this->lang->line('POS_Management');
					$PageDetails = $this->lang->line('POS_Management_Details');
					}
					else if(($Segment1 == "home") && ($Segment2 == "push_messages"))
					{
					$ModuleName = $this->lang->line('Send_Message_To_Customers');
					$PageDetails = $this->lang->line('Send_Message_To_Customers');
					}
					else if(($Segment1 == "home") && ($Segment2 == "push_notifications"))
					{
					$ModuleName = $this->lang->line('Send_Notification_To_Customers');
					$PageDetails = $this->lang->line('Send_Notification_To_Customers');
					}
					else if(($Segment1 == "project") && ($Segment2 == "proj_project") && ($Segment3 == "proj_sub_projects"))
					{
					$ModuleName = $this->lang->line('ShortCut_sub_project');
					$PageDetails = $this->lang->line('ShortCut_sub_project');
					}
					else if(($Segment1 == "project") && ($Segment2 == "proj_project") && ($Segment3 == "expense_items"))
					{
					$ModuleName = $this->lang->line('expense_items');
					$PageDetails = $this->lang->line('expense_items');
					}
					else if(($Segment1 == "project") && ($Segment2 == "proj_project") && ($Segment3 == "expense_items_insertform"))
					{
					$ModuleName = $this->lang->line('expense_items');
					$PageDetails = $this->lang->line('expense_items');
					}
					else if(($Segment1 == "project") && ($Segment2 == "proj_project") && ($Segment3 == "updateformexpense_items"))
					{
					$ModuleName = $this->lang->line('expense_items');
					$PageDetails = $this->lang->line('expense_items');
					}
					else
					{
						$ModuleData = $this->M_app_module->M_app_module->GetByFolder($Segment1);
						if ($this->session->userdata('lang') == "ar")
						{
							$ModuleName = $ModuleData->ar_title;
						}
						else
						{
							$ModuleName = $ModuleData->en_title;
						}
					}

					$icon_name = $ModuleData->icon_name;
					?>
					<!--begin::Page Title-->
					<h5 class="text-dark font-weight-bold my-1 mr-5"><?php echo $ModuleName;?></h5>
					<!--end::Page Title-->
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
						<?php
						include("page_name.php");
						if($Segment3 != "expense_items_insertform"  && $Segment3 != "updateformexpense_items" && $Segment3 != "expense_items")
						{
						?>
						<li class="breadcrumb-item">
							<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="text-muted"><?php echo $PageName;?></a>
						</li>
						<?php
					}
						if($Segment3 == "insertform")
						{
							echo "<li class='breadcrumb-item'>".$this->lang->line('AddRecord')."</li>";
						}
						if($Segment3 == "proj_sub_projects")
						{
							?>
							<li class="breadcrumb-item">
								<a href="<?php echo base_url().$Segment1."/".$Segment2."/".$Segment3."/".$Segment4;?>" class="text-muted"><?php echo lang("ShortCut_sub_project");?></a>
							</li>
							<?php
						}
						if($Segment3 == "expense_items")
						{
							?>
							<li class="breadcrumb-item">
								<a href="<?php echo base_url().$Segment1."/".$Segment2."/".$Segment3;?>" class="text-muted"><?php echo lang("expense_items");?></a>
							</li>
							<?php
						}
						if($Segment3 == "expense_items_insertform")
						{
							?>
							<li class="breadcrumb-item">
								<a href="<?php echo base_url().$Segment1."/".$Segment2."/".$Segment3;?>" class="text-muted"><?php echo lang("expense_items");?></a>
							</li>
							<?php
						}
						if($Segment3 == "updateformexpense_items")
						{
							?>
							<li class="breadcrumb-item">
								<a href="<?php echo base_url().$Segment1."/".$Segment2."/".$Segment3;?>" class="text-muted"><?php echo lang("expense_items");?></a>
							</li>
							<?php
						}
						if($Segment3 == "updateform")
						{
							echo "<li class='breadcrumb-item'>".$this->lang->line('Edit')."</li>";
						}
						?>
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Page Heading-->
			</div>
			<!--end::Info-->
		</div>
	</div>
	<!--end::Subheader-->
	<!--begin::Entry-->
	<div class="d-flex flex-column-fluid">
		<!--begin::Container-->
		<div class="container">
			<!--begin::Notice-->
			<div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert">
				<div class="alert-icon">
					<span class="svg-icon svg-icon-primary svg-icon-xl">
						<!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect x="0" y="0" width="24" height="24" />
								<path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3" />
								<path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero" />
							</g>
						</svg>
						<!--end::Svg Icon-->
					</span>
				</div>
				<div class="alert-text"><?php echo $PageDetails;
				if($Segment2 == "sub_centers" && !empty($_GET["main"]))
				{
					$center = $this->M_center->GetRow($_GET["main"]);
					if ($this->session->userdata('lang') == "ar")
					{
						echo " لمركز التكلفة الرئيسي  : ".$center->name_ar;
					}
					else
					{
						echo " for main cost center: ".$center->name_en;
					}
				}

				?></div>
			</div>
			<!--end::Notice-->
			<!--begin::Card-->
			<div class="card card-custom">
				<div class="card-header py-3">
					<div class="card-title">
						<?php
						if($Segment3 == "proj_sub_projects")
						{
							echo lang('ShortCut_sub_project');
						}else {
							// code...
							echo $icon_name;
						}
						?>
						<h3 class="card-label">
						<?php
						if($Segment3 != "")
						{
							if($Segment3 == "insertform")
							{
								echo $this->lang->line('AddNew')." ";
							}
							if($Segment3 == "updateform")
							{
								echo $this->lang->line('Edit')." ";
							}
						}
						?>
						<?php
						$Filter = $this->session->userdata('Filter');
						if($Filter == "")
						{
							if(($Segment1 == "account") && ($Segment2 == "fin_treeaccount"))
							{
								echo "<a href='".base_url().$Segment1."/".$Segment2."'>".$this->lang->line('fin_treeaccount_table')."</a> | ";
								echo "<a href='".base_url().$Segment1."/".$Segment2."/tree'>".$this->lang->line('fin_treeaccount_treeview')."</a>";
							}
							else
							{
								if($Segment3 != "proj_sub_projects" && $Segment3 != "expense_items" && $Segment3 !="expense_items_insertform" && $Segment3 !="updateformexpense_items")
								{
								echo $PageName;
							}
								if($Segment3 == "expense_items" ||$Segment3 =="expense_items_insertform" ||$Segment3 =="updateformexpense_items")
								{
								echo $this->lang->line('expense_items');
							}
							}
						}
						else
						{
							echo $PageName." | <span class='form-text text-muted' style='display: inline; font-size: 12px;'>".$Filter."</span>";
						}
						?>
						</h3>
					</div>
					<?php
					if(($Segment1 != "settings") && ($Segment2 != "profile") && ($Segment2 != "fin_liquidity_risk") && ($Segment2 != "trial_balance_dt") &&
					($Segment2 != "data_invoice") && ($Segment2 != "whatsapp_chat") && ($Segment2 != "deleterows") &&
					($Segment2 != "repair") && ($Segment2 != "optimize") && ($Segment2 != "backup") && ($Segment2 != "cars_invoice")  && ($Segment3 != "reports")) {
					?>
					<div class="card-toolbar">
						<!--begin::Button-->
						<?php
						$AddLinkOption = "";
						if(($Segment1 == "tasks_me") || ($Segment1 == "tasks_another"))
						{
							$AddLinkOption = base_url()."tasks_another/tas_tasks/insertform";
						}
						else if(($Segment1 == "branche") && ($Segment2 == "updateform"))
						{
							$AddLinkOption = base_url()."branche/insertform";
						}
						else if(($Segment1 == "pos_sys") && ($Segment2 == "tec_stores") && ($Segment3 == "tec_users"))
						{
							$AddLinkOption = base_url().$Segment1."/".$Segment2."/tec_users_insertform";
						}
						else if(($Segment1 == "project") && ($Segment2 == "proj_project") && ($Segment3 == "proj_project_devices"))
						{
							$AddLinkOption = base_url().$Segment1."/".$Segment2."/devices_insertform";
						}
						else if(($Segment1 == "project") && ($Segment2 == "proj_project") && ($Segment3 == "proj_sub_projects"))
						{
							$AddLinkOption = base_url().$Segment1."/".$Segment2."/sup_project_insertform?pro_id=".$Segment4;
						}
						else if(($Segment1 == "centers") && ($Segment2 == "sub_centers"))
						{
							$AddLinkOption = base_url().$Segment1."/".$Segment2."/insertform?main=".$_GET['main'];
						}
						else if(($Segment1 == "project") && ($Segment2 == "proj_project") && ($Segment3 == "expense_items"))
						{
							$AddLinkOption = base_url().$Segment1."/".$Segment2."/expense_items_insertform";
						}

						else
						{
							$AddLinkOption = base_url().$Segment1."/".$Segment2."/insertform";
						}
						if(($Segment1 == "project") && ($Segment2 == "proj_project") && ($Segment3 == "")  && (empty($_GET) || $_GET['showInCards'] == 2))
						{?>
							<a href="?showInCards=1" class="btn btn-primary font-weight-bolder" style="margin-left:5px">
								<?php echo lang('show_in_card');?>
							</a>
							<?php
						}
						if(($Segment1 == "sales") && ($Segment2 == "sale_customer") && ($Segment3 == ""))
						{?>
							<button
	                   class="font-weight-bolder btn btn-danger" style="margin-left:5px;  font-size: 16px;
" onclick="synchronous()"><i class="fas fa-sync"></i>Sync </button>
							<?php
						}
						if(($Segment1 == "hr") && ($Segment2 == "hr_employee") && ($Segment3 == ""))
						{?>
							<button
	                   class="font-weight-bolder btn btn-danger" style="margin-left:5px;  font-size: 16px;
" onclick="synchronous()"><i class="fas fa-sync"></i>Sync </button>
							<?php
						}
						if(($Segment1 == "project") && ($Segment2 == "proj_project") && ($Segment3 == ""))
						{?>
							<button
	                   class="font-weight-bolder btn btn-danger" style="margin-left:5px;  font-size: 16px;
" onclick="synchronous()"><i class="fas fa-sync"></i>Sync </button>
							<?php
						}
						if(($Segment1 == "hr") && ($Segment2 == "hr_job") && ($Segment3 == ""))
						{?>
							<button
	                   class="font-weight-bolder btn btn-danger" style="margin-left:5px;  font-size: 16px;
" onclick="synchronous()"><i class="fas fa-sync"></i>Sync </button>
							<?php
						}
						if(($Segment1 == "store") && ($Segment2 == "product") && ($Segment3 == ""))
						{?>
							<button
	                   class="font-weight-bolder btn btn-danger" style="margin-left:5px;  font-size: 16px;
" onclick="synchronous()"><i class="fas fa-sync"></i>Sync </button>
							<?php
						}
						if(($Segment1 == "project") && ($Segment2 == "proj_project") && ($Segment3 == "")  && (!empty($_GET) && $_GET['showInCards'] == 1))
						{ ?>
							<a href="?showInCards=2" class="btn btn-primary font-weight-bolder" style="margin-left:5px">
								<?php echo lang('show_in_table');?>
							</a>
							<?php
						}
						if($Segment1 != "reports")
						{
						if($Segment2 == "fin_treeaccount")
						{
							$Segment2 = $this->uri->segment(2);
							$group_id = intval($this->session->userdata('GroupID'));
							$PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
							$page_id = $PageData->id;
							$CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
							if($CheckAdd != 0 )
							{
						?>
						<a href="<?php echo $AddLinkOption;?>" class="btn btn-primary font-weight-bolder" >
						<span class="svg-icon svg-icon-md">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<rect x="0" y="0" width="24" height="24"/>
									<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
									<path d="M11,11 L11,7 C11,6.44771525 11.4477153,6 12,6 C12.5522847,6 13,6.44771525 13,7 L13,11 L17,11 C17.5522847,11 18,11.4477153 18,12 C18,12.5522847 17.5522847,13 17,13 L13,13 L13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 L11,13 L7,13 C6.44771525,13 6,12.5522847 6,12 C6,11.4477153 6.44771525,11 7,11 L11,11 Z" fill="#000000"/>
								</g>
							</svg>
						</span>
						<?php echo $this->lang->line('AddRecord');?></a>


					<?php } ?>
						<!--end::Button-->
					<?php
				}else {
					?>
					<a href="<?php echo $AddLinkOption;?>" class="btn btn-primary font-weight-bolder" >
					<span class="svg-icon svg-icon-md">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect x="0" y="0" width="24" height="24"/>
								<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
								<path d="M11,11 L11,7 C11,6.44771525 11.4477153,6 12,6 C12.5522847,6 13,6.44771525 13,7 L13,11 L17,11 C17.5522847,11 18,11.4477153 18,12 C18,12.5522847 17.5522847,13 17,13 L13,13 L13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 L11,13 L7,13 C6.44771525,13 6,12.5522847 6,12 C6,11.4477153 6.44771525,11 7,11 L11,11 Z" fill="#000000"/>
							</g>
						</svg>
					</span>
					<?php echo $this->lang->line('AddRecord');?></a>


					<?
				}
				?>
				<?php
					}
					?>
					<?php
					}
					?>
				</div>
				</div>
				<div class="card-body" style="padding-right: 15px; padding-left: 15px; padding-top: 5px; padding-bottom: 5px;">
					<!--begin: Datatable-->
