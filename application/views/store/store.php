
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('store_parent_id');?></th>
												<th><?php echo lang('store_title');?></th>
												<th><?php echo lang('store_title_en');?></th>
												<th><?php echo lang('store_address');?></th>
												<th><?php echo lang('store_email');?></th>
												<th><?php echo lang('store_phone');?></th>
												<th><?php echo lang('store_active');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$store_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
													<?php
												$parent_id = $DataRows_Row->parent_id;
												if($parent_id > 0)
												{
													$StoreData = $this->M_store->GetRow($parent_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $StoreData->title;
													}
													else
													{
														echo $StoreData->title_en;
													}
												}
												?>
												</td>
												<td><?php echo $DataRows_Row->title;?></td>
												<td><?php echo $DataRows_Row->title_en;?></td>
												<td><?php echo $DataRows_Row->address;?></td>
												<td><?php echo $DataRows_Row->email;?></td>
												<td><?php echo $DataRows_Row->phone;?></td>
												<td>
												<?php
												if($DataRows_Row->deleted == 0)
												{
													echo lang('Yes');
												}
												else
												{
													echo lang('No');
												}
												?>
												</td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

								