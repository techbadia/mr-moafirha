<style media="screen">
#overlay{
  position: fixed;
  top: 0;
  z-index: 100;
  width: 100%;
  height:100%;
  display: none;
  background: rgba(0,0,0,0.6);
}
.cv-spinner {
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
}
.spinner {
  width: 40px;
  height: 40px;
  border: 4px #ddd solid;
  border-top: 4px #2e93e6 solid;
  border-radius: 50%;
  animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
  100% {
    transform: rotate(360deg);
  }
}
.is-hide{
  display:none;
}
</style>
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('product_barcode');?></th>
												<th><?php echo lang('product_category_id');?></th>
												<th><?php echo lang('product_brand_id');?></th>
												<th><?php echo lang('product_thetitle');?></th>
												<th><?php echo lang('product_cost_price');?></th>
												<th><?php echo lang('product_Price');?></th>
												<th><?php echo lang('product_lowest_price');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
											// 	if($DataRows_Row->id == 25){
											//
											// 	// var_dump($DataRows_Row->title);die();
											// }
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td><?php echo $DataRows_Row->barcode;?></td>
												<td>
													<?php
													$category_id = $DataRows_Row->category_id;
													$CategoryData = $this->M_store_category->GetRow($category_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $CategoryData->title;
													}
													else
													{
														echo $CategoryData->title_en;
													}
													?>
												</td>
												<td>
													<?php
													$brand_id = $DataRows_Row->brand_id;
													$BrandData = $this->M_store_brand->GetRow($brand_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $BrandData->title;
													}
													else
													{
														echo $BrandData->title_en;
													}
													?>
												</td>
												<td>
												<?php
												if ($this->session->userdata('lang') == "ar")
												{
													echo $DataRows_Row->title;
												}
												else
												{
													echo $DataRows_Row->title_en;
												}
												?>
												</td>
												<td><?php echo $DataRows_Row->cost_price;?></td>
												<td><?php echo $DataRows_Row->price;?></td>
												<td><?php echo $DataRows_Row->lowest_price;?></td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i>
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i>
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i>
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
									</table>
