
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('store_move_user_id');?></th>
												<th><?php echo lang('store_move_from_store');?></th>
												<th><?php echo lang('store_move_to_store');?></th>
												<th><?php echo lang('store_move_product_id');?></th>
												<th><?php echo lang('store_move_quantity');?></th>
												<th><?php echo lang('store_move_thedate');?></th>
												<th><?php echo lang('store_move_thetime');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												
												$store_move_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
													<?php
													$user_id = $DataRows_Row->user_id;
													$UserData = $this->M_usr_users->GetRow($user_id);
													echo $UserData->fullname;
													?>
												</td>
												<td>
													<?php
													$from_store = $DataRows_Row->from_store;
													$FromStoreData = $this->M_store->GetRow($from_store);
													$parent_id = $FromStoreData->parent_id;
													if($parent_id > 0)
													{
														$StoreData = $this->M_store->GetRow($parent_id);
														if ($this->session->userdata('lang') == "ar")
														{
															$MainStore = $StoreData->title." : ";
														}
														else
														{
															$MainStore = $StoreData->title_en." : ";
														}
													}
													else
													{
														$MainStore = "";
													}
													if ($this->session->userdata('lang') == "ar")
													{
														echo $MainStore.$FromStoreData->title;
													}
													else
													{
														echo $MainStore.$FromStoreData->title_en;
													}
													?>
												</td>
												<td>
													<?php
													$to_store = $DataRows_Row->to_store;
													$ToStoreData = $this->M_store->GetRow($to_store);
													$parent_id = $ToStoreData->parent_id;
													if($parent_id > 0)
													{
														$StoreData = $this->M_store->GetRow($parent_id);
														if ($this->session->userdata('lang') == "ar")
														{
															$MainStore = $StoreData->title." : ";
														}
														else
														{
															$MainStore = $StoreData->title_en." : ";
														}
													}
													else
													{
														$MainStore = "";
													}
													if ($this->session->userdata('lang') == "ar")
													{
														echo $MainStore.$ToStoreData->title;
													}
													else
													{
														echo $MainStore.$ToStoreData->title_en;
													}
													?>
												</td>
												<td>
													<?php
													$product_id = $DataRows_Row->product_id;
													$ProductData = $this->M_product->GetRow($product_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $ProductData->title;
													}
													else
													{
														echo $ProductData->title_en;
													}
													?>
												</td>
												<td><?php echo $DataRows_Row->quantity;?></td>
												<td><?php echo $DataRows_Row->thedate;?></td>
												<td><?php echo $DataRows_Row->thetime;?></td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

									