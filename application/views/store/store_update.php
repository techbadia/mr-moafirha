<div class="card card-custom gutter-b example example-compact">
                    <?php
                    $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
                    echo form_open_multipart($FormPath);
                    ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_parent_id');?></label>
												<select name="parent_id" class="form-control">
													<option value="0" <?php if($parent_id == 0) {?>selected<?php }?>>-</option>
													<?php
													$StoreList = $this->M_store->GetMultiRow();
													foreach($StoreList as $StoreList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$StoreTitle = $StoreList_Row->title;
														}
														else
														{
															$StoreTitle = $StoreList_Row->title_en;
														}
													?>
													<option value="<?php echo $StoreList_Row->id;?>" <?php if($parent_id == $StoreList_Row->id) {?>selected<?php }?>><?php echo $StoreTitle;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_title');?></label>
												<input type="text" name="title" class="form-control" value="<?php echo $title;?>" required>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_title_en');?></label>
												<input type="text" name="title_en" class="form-control" value="<?php echo $title_en;?>" required>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_address');?></label>
												<input type="text" name="address" class="form-control" value="<?php echo $address;?>" required>
											</div>
											
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_email');?></label>
												<input type="email" name="email" class="form-control" value="<?php echo $email;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_phone');?></label>
												<input type="text" name="phone" class="form-control" value="<?php echo $phone;?>" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php echo $id;?>">
													<input type="hidden" name="web_store" value="<?php echo $web_store;?>">
													<input type="hidden" name="deleted" value="<?php echo $deleted;?>">
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>