<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
                        echo form_open_multipart($FormPath);
                    ?>

								
<div class="form-body">
<?php
//echo "company_id = ".intval($this->session->userdata('company_id'));
//echo "<br>user_id = ".intval($this->session->userdata('StaffID'));
?>
		<div class="form-group row" style="background-color: #5d78ff !important; padding-bottom: 20px;">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label" style="color: #FFFFFF"><?php echo lang('store_move_from_store');?></label>
				<select name="from_store" id="from_store" class="form-control">
					<?php
					$StoreList = $this->M_store->GetMultiRow();
					foreach($StoreList as $StoreList_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$FromStore = $StoreList_Row->title;
						}
						else
						{
							$FromStore = $StoreList_Row->title_en;
						}
						$parent_id = $StoreList_Row->parent_id;
						if($parent_id > 0)
						{
							$StoreData = $this->M_store->GetRow($parent_id);
							if ($this->session->userdata('lang') == "ar")
							{
								$MainStore = $StoreData->title." : ";
							}
							else
							{
								$MainStore = $StoreData->title_en." : ";
							}
						}
						else
						{
							$MainStore = "";
						}
					?>
					<option value="<?php echo $StoreList_Row->id;?>"><?php echo $MainStore.$FromStore;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label" style="color: #FFFFFF"><?php echo lang('store_move_to_store');?></label>
				<select name="to_store" id="to_store" class="form-control">
					<?php
					$StoreList = $this->M_store->GetMultiRow();
					foreach($StoreList as $StoreList_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$ToStore = $StoreList_Row->title;
						}
						else
						{
							$ToStore = $StoreList_Row->title_en;
						}
						$parent_id = $StoreList_Row->parent_id;
						if($parent_id > 0)
						{
							$StoreData = $this->M_store->GetRow($parent_id);
							if ($this->session->userdata('lang') == "ar")
							{
								$MainStore = $StoreData->title." : ";
							}
							else
							{
								$MainStore = $StoreData->title_en." : ";
							}
						}
						else
						{
							$MainStore = "";
						}
					?>
					<option value="<?php echo $StoreList_Row->id;?>"><?php echo $MainStore.$ToStore;?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group row">
		<?php
		for ($i = 1; $i <= 10; $i++) {
		?>
			<div class="col-lg-3 col-md-3 col-sm-12">
				<label class="col-form-label"><?php echo lang('store_move_product_code');?></label>
				<input type="text" name="product_id<?php echo $i;?>" id="barcode<?php echo $i;?>" onfocus="GetName(<?php echo $i;?>)" onblur="GetName(<?php echo $i;?>)" class="form-control" value="">
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12">
				<label class="col-form-label"><?php echo lang('store_move_product_name');?></label>
				<input type="text"  disabled id="title<?php echo $i;?>" class="form-control" value="">
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12">
				<label class="col-form-label"><?php echo lang('store_move_quantity');?><?php echo lang('store_move_from_store');?></label>
				<input type="text"id="available_from_store<?php echo $i;?>"  disabled  class="form-control" value="">
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12">
				<label class="col-form-label"><?php echo lang('store_move_quantity');?><?php echo lang('store_move_to_store');?></label>
				<input type="text" id="available_to_store<?php echo $i;?>"  disabled  class="form-control" value="">
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12">
				<label class="col-form-label"><?php echo lang('store_move_quantity');?></label>
				<input type="number" min="0" step="1" name="quantity<?php echo $i;?>" class="form-control" value="0">
			</div>
		<?php
		}
		?>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php //echo $id;?>">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>
</div>

								