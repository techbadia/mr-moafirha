
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
	$FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
	echo form_open_multipart($FormPath);
?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_barcode');?></label>
				<input type="text" name="barcode" class="form-control" value="<?php echo $barcode;?>" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_batch_number');?></label>
				<input type="text" name="batch_number" class="form-control" value="<?php echo $batch_number;?>">
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_category_id');?></label>
				<select name="category_id" class="form-control">
					<?php
					$CategoryList = $this->M_store_category->GetMultiRow();
					foreach($CategoryList as $CategoryList_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Category = $CategoryList_Row->title;
						}
						else
						{
							$Category = $CategoryList_Row->title_en;
						}
					?>
					<option <?php if($category_id == $CategoryList_Row->id) {?>selected<?php }?> value="<?php echo $CategoryList_Row->id;?>"><?php echo $Category;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_brand_id');?></label>
				<select name="brand_id" class="form-control">
					<?php
					$BrandList = $this->M_store_brand->GetMultiRow();
					foreach($BrandList as $BrandList_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Brand = $BrandList_Row->title;
						}
						else
						{
							$Brand = $BrandList_Row->title_en;
						}
					?>
					<option <?php if($brand_id == $BrandList_Row->id) {?>selected<?php }?> value="<?php echo $BrandList_Row->id;?>"><?php echo $Brand;?></option>
					<?php } ?>
				</select>
			</div>
			<?php
			if($this->session->userdata('PackageName') == "car_rental")
			{
			?>
				<div class="col-lg-2 col-md-4 col-sm-12">
					<label class="col-form-label"><?php echo lang('cars_cars_model_name');?></label>
					<input type="text" name="model_name" class="form-control" value="<?php echo $model_name;?>" required>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-12">
					<label class="col-form-label"><?php echo lang('cars_cars_chassis_no');?></label>
					<input type="text" name="chassis_no" class="form-control" value="<?php echo $chassis_no;?>" required>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-12">
					<label class="col-form-label"><?php echo lang('cars_cars_plate_No');?></label>
					<input type="text" name="plate_No" class="form-control" value="<?php echo $plate_No;?>">
				</div>
				<div class="col-lg-2 col-md-4 col-sm-12">
					<label class="col-form-label"><?php echo lang('cars_cars_meter_reading');?></label>
					<input type="number" min="0" step="0.001" name="meter_reading" class="form-control" value="<?php echo $meter_reading;?>" required>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-12">
					<label class="col-form-label"><?php echo lang('cars_cars_motor_number');?></label>
					<input type="text" name="motor_number" class="form-control" value="<?php echo $motor_number;?>" required>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-12">
					<label class="col-form-label"><?php echo lang('cars_cars_color');?></label>
					<input type="text" name="color" class="form-control" value="<?php echo $color;?>" required>
				</div>
			<?php } ?>
			<div class="col-lg-2 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_cost_price');?></label>
				<input type="number" min="0" step="0.01" name="cost_price" class="form-control" value="<?php echo $cost_price;?>" required>
			</div>
			<div class="col-lg-2 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_Price');?></label>
				<input type="number" min="0" step="0.01" name="price" class="form-control" value="<?php echo $price;?>" required>
			</div>
			<div class="col-lg-2 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_lowest_price');?></label>
				<input type="number" min="0" step="0.01" name="lowest_price" class="form-control" value="<?php echo $lowest_price;?>" required>
			</div>
			<?php
			if($this->session->userdata('PackageName') == "erp")
			{
			?>
			<div class="col-lg-2 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_finished');?></label>
				<select name="finished" class="form-control">
					<option <?php if($finished == 1) {?>selected<?php }?> value="1"><?php echo lang('Yes');?></option>
					<option <?php if($finished == 0) {?>selected<?php }?> value="0"><?php echo lang('No');?></option>
				</select>
			</div>
			<?php } ?>
			<div class="col-lg-2 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('active');?></label>
				<select name="deleted" class="form-control">
					<option <?php if($deleted == 0) {?>selected<?php }?> value="0"><?php echo lang('Yes');?></option>
					<option <?php if($deleted == 1) {?>selected<?php }?> value="1"><?php echo lang('No');?></option>
				</select>
			</div>
			<div class="col-lg-2 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_demand');?></label>
				<input type="number" min="0" step="0.01" name="demand" class="form-control" value="<?php echo $demand;?>" required>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_title');?></label>
				<input type="text" name="title" class="form-control" value="<?php echo $title;?>" required>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_title_en');?></label>
				<input type="text" name="title_en" class="form-control" value="<?php echo $title_en;?>" required dir="ltr">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_details');?></label>
				<textarea name="details" class="form-control" rows="3" required><?php echo $details;?></textarea>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_details_en');?></label>
				<textarea name="details_en" class="form-control" rows="3" required dir="ltr"><?php echo $details_en;?></textarea>
			</div>



		</div>

	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php echo $id;?>">
					<?php
					if($this->session->userdata('PackageName') != "erp")
					{
					?>
					<input type="hidden" name="finished" value="<?php echo $finished;?>">
					<?php } ?>
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>
</div>

<br>

<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-40">
	<?php
		$FormPath = base_url().$Segment1."/".$Segment2."/ChangeImage";
		echo form_open_multipart($FormPath);
	?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_image');?></label>
				<input type="file" name="image" class="form-control" dir="ltr">
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php echo $id;?>">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('ChangeImage');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>
</div>
<br>

<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-40">
	<?php
		$FormPath = base_url().$Segment1."/".$Segment2."/ChangeCatalogue";
		echo form_open_multipart($FormPath);
	?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_catalogue');?></label>
				<input type="file" name="catalogue" class="form-control" dir="ltr">
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php echo $id;?>">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('ChangeCatalogue');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>
</div>
