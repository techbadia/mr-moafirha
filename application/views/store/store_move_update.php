
<div class="card card-custom gutter-b example example-compact">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
                        echo form_open_multipart($FormPath);
                    ?>
								<!--begin::Form-->
								<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_move_country_id');?></label>
												<select name="country_id" class="form-control">
													<?php
													$CountryList = $this->M_country->GetMultiRow();
													foreach($CountryList as $CountryList_Row) {
													?>
													<option <?php if($country_id == $CountryList_Row->id) {?>selected<?php }?> value="<?php echo $CountryList_Row->id;?>"><?php echo $CountryList_Row->title;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_move_area');?></label>
												<select name="area" class="form-control">
													<option <?php if($area == 1) {?>selected<?php }?> value="1"><?php echo lang('store_move_local');?></option>
													<option <?php if($area == 2) {?>selected<?php }?> value="2"><?php echo lang('store_move_world');?></option>
													<option <?php if($area == 3) {?>selected<?php }?> value="3"><?php echo lang('store_move_poth');?></option>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_move_title');?></label>
												<input type="text" name="title" class="form-control" value="<?php echo $title;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_move_email');?></label>
												<input type="email" name="email" class="form-control" value="<?php echo $email;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_move_phone');?></label>
												<input type="text" name="phone" class="form-control" value="<?php echo $phone;?>" required>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<input type="checkbox" id="ChangeImage" name="ChangeImage" value="True" />&nbsp;
												<?php echo lang('ChangeImage');?></label>
												<input type="file" name="image" class="form-control" dir="ltr">
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php echo $id;?>">
													<input type="hidden" name="deleted" value="<?php echo $deleted;?>">
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>