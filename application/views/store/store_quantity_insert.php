
<div class="card card-custom gutter-b example example-compact">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
                        echo form_open_multipart($FormPath);
                    ?>
								<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_quantity_store_id');?></label>
												<select name="store_id" class="form-control">
													<?php
													$StoreList = $this->M_store->GetMultiRow();
													foreach($StoreList as $StoreList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$StoreTitle = $StoreList_Row->title;
														}
														else
														{
															$StoreTitle = $StoreList_Row->title_en;
														}
														$parent_id = $StoreList_Row->parent_id;
														if($parent_id > 0)
														{
															$StoreData = $this->M_store->GetRow($parent_id);
															if ($this->session->userdata('lang') == "ar")
															{
																$MainStore = $StoreData->title." : ";
															}
															else
															{
																$MainStore = $StoreData->title_en." : ";
															}
														}
														else
														{
															$MainStore = "";
														}
													?>
													<option value="<?php echo $StoreList_Row->id;?>"><?php echo $MainStore.$StoreTitle;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_quantity_product_id');?></label>
												<select name="product_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true">
													<?php
													$ProductList = $this->M_product->GetMultiRow();
													foreach($ProductList as $ProductList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$ProductTitle = $ProductList_Row->title;
														}
														else
														{
															$ProductTitle = $ProductList_Row->title_en;
														}
													?>
													<option value="<?php echo $ProductList_Row->id;?>"><?php echo $ProductList_Row->barcode." : ".$ProductTitle;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_quantity_quantity');?></label>
												<input type="number" min="1" step="1" name="quantity" class="form-control" value="" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<?php $company_id = intval($this->session->userdata('company_id'));?>
													<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
													
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>