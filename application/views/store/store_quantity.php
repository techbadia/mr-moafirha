

									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('store_quantity_store_id');?></th>
												<th><?php echo lang('store_quantity_product_id');?></th>
												<th><?php echo lang('store_quantity_quantity');?></th>
												<th><?php echo lang('product_cost_price');?></th>
												<th><?php echo lang('sale_bill_items_total');?></th>
												<th><?php echo lang('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$store_quantity_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
													<?php
													$store_id = $DataRows_Row->store_id;
													$StoreData = $this->M_store->GetRow($store_id);
													if ($this->session->userdata('lang') == "ar")
													{
														$StoreTitle = $StoreData->title;
													}
													else
													{
														$StoreTitle = $StoreData->title_en;
													}
													$parent_id = $StoreData->parent_id;
													if($parent_id > 0)
													{
														$StoreData = $this->M_store->GetRow($parent_id);
														if ($this->session->userdata('lang') == "ar")
														{
															$MainStore = $StoreData->title." : ";
														}
														else
														{
															$MainStore = $StoreData->title_en." : ";
														}
													}
													else
													{
														$MainStore = "";
													}
													echo $MainStore.$StoreTitle;
													?>
												</td>
												<td>
													<?php
													$product_id = $DataRows_Row->product_id;
													$ProductData = $this->M_product->GetRow($product_id);
													if ($this->session->userdata('lang') == "ar")
													{
														$ProductTitle = $ProductData->title;
													}
													else
													{
														$ProductTitle = $ProductData->title_en;
													}
													echo $ProductData->barcode." : ".$ProductTitle;
													?>
												</td>
												<td><?php echo $DataRows_Row->quantity;?></td>
												<td style="text-align:center"><?php echo $ProductData->cost_price;?></td>
												<td style="text-align:center">
												<?php
												$ItemQuantity = $DataRows_Row->quantity;
												$ItemCost = $ProductData->cost_price;
												$ItemValue = doubleval($ItemQuantity * $ItemCost);
												echo $ItemValue;
												?>
												</td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

									