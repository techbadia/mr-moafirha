
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
	$FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
	echo form_open_multipart($FormPath);
?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_barcode');?></label>
				<input type="text" name="barcode" class="form-control" value="" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_batch_number');?></label>
				<input type="text" name="batch_number" class="form-control" value="">
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_category_id');?></label>
				<select name="category_id" class="form-control">
					<?php
					$CategoryList = $this->M_store_category->GetMultiRow();
					foreach($CategoryList as $CategoryList_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Category = $CategoryList_Row->title;
						}
						else
						{
							$Category = $CategoryList_Row->title_en;
						}
					?>
					<option value="<?php echo $CategoryList_Row->id;?>"><?php echo $Category;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_brand_id');?></label>
				<select name="brand_id" class="form-control">
					<?php
					$BrandList = $this->M_store_brand->GetMultiRow();
					foreach($BrandList as $BrandList_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Brand = $BrandList_Row->title;
						}
						else
						{
							$Brand = $BrandList_Row->title_en;
						}
					?>
					<option value="<?php echo $BrandList_Row->id;?>"><?php echo $Brand;?></option>
					<?php } ?>
				</select>
			</div>
			<?php
			if($this->session->userdata('PackageName') == "car_rental")
			{
			?>
				<div class="col-lg-2 col-md-4 col-sm-12">
					<label class="col-form-label"><?php echo lang('cars_cars_model_name');?></label>
					<input type="text" name="model_name" class="form-control" value="" required>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-12">
					<label class="col-form-label"><?php echo lang('cars_cars_chassis_no');?></label>
					<input type="text" name="chassis_no" class="form-control" value="" required>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-12">
					<label class="col-form-label"><?php echo lang('cars_cars_plate_No');?></label>
					<input type="text" name="plate_No" class="form-control" value="">
				</div>
				<div class="col-lg-2 col-md-4 col-sm-12">
					<label class="col-form-label"><?php echo lang('cars_cars_meter_reading');?></label>
					<input type="number" min="0" step="0.001" name="meter_reading" class="form-control" value="" required>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-12">
					<label class="col-form-label"><?php echo lang('cars_cars_motor_number');?></label>
					<input type="text" name="motor_number" class="form-control" value="" required>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-12">
					<label class="col-form-label"><?php echo lang('cars_cars_color');?></label>
					<input type="text" name="color" class="form-control" value="" required>
				</div>
			<?php } ?>
			<div class="col-lg-2 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_cost_price');?></label>
				<input type="number" min="0" step="0.01" name="cost_price" class="form-control" value="" required>
			</div>
			<div class="col-lg-2 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_Price');?></label>
				<input type="number" min="0" step="0.01" name="price" class="form-control" value="" required>
			</div>
			<div class="col-lg-2 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_lowest_price');?></label>
				<input type="number" min="0" step="0.01" name="lowest_price" class="form-control" value="" required>
			</div>
			<?php
			if($this->session->userdata('PackageName') == "erp")
			{
			?>
			<div class="col-lg-2 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_finished');?></label>
				<select name="finished" class="form-control">
					<option value="1"><?php echo lang('Yes');?></option>
					<option value="0"><?php echo lang('No');?></option>
				</select>
			</div>
			<?php } ?>
			<div class="col-lg-2 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('active');?></label>
				<select name="deleted" class="form-control">
					<option value="0"><?php echo lang('Yes');?></option>
					<option value="1"><?php echo lang('No');?></option>
				</select>
			</div>
			<div class="col-lg-2 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_demand');?></label>
				<input type="number" min="0" step="0.01" name="demand" class="form-control" value="1" required>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_image');?></label>
				<input type="file" name="image" class="form-control" dir="ltr">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_catalogue');?></label>
				<input type="file" name="catalogue" class="form-control" dir="ltr">
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_title');?></label>
				<input type="text" name="title" class="form-control" value="" required>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_title_en');?></label>
				<input type="text" name="title_en" class="form-control" value="" required dir="ltr">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_details');?></label>
				<textarea name="details" class="form-control" rows="3" required></textarea>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('product_details_en');?></label>
				<textarea name="details_en" class="form-control" rows="3" required dir="ltr"></textarea>
			</div>
			<hr>
		</div>
		<div class="form-group row">
			<div class="col-lg-3 col-md-3 col-sm-12 p-3 mb-2 bg-primary text-white">
				<label class="col-form-label text-white"><?php echo lang('store');?></label>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-12 p-3 mb-2 bg-primary text-white">
				<label class="col-form-label text-white"><?php echo lang('qunatity');?></label>
			</div>
		</div>
		<?php
		$StoreList = $this->M_store->GetMultiRow();
		$StoreID = 0;
		foreach($StoreList as $StoreList_Row) {
			$StoreID = $StoreList_Row->id;
			if ($this->session->userdata('lang') == "ar")
			{
				$Store = $StoreList_Row->title;
			}
			else
			{
				$Store = $StoreList_Row->title_en;
			}
		?>
		<div class="form-group row">
			<div class="col-lg-3 col-md-3 col-sm-12">
				<label class="col-form-label"><?php echo $Store;?></label>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-12">
				<input type="number" min="0" step="1" name="qunatity<?php echo $StoreID;?>" class="form-control" value="0" required>
				<input type="hidden" name="StoreID<?php echo $StoreID;?>" value="<?php echo $StoreID;?>">
			</div>
		</div>
		<?php
		}
		?>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php //echo $id;?>">
					<?php
					if($this->session->userdata('PackageName') != "erp")
					{
					?>
					<input type="hidden" name="finished" value="1">
					<?php } ?>
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
<?php
	echo form_close();
?>
</div>