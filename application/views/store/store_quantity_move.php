<div class="card card-custom gutter-b example example-compact">
<?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_move";
                        echo form_open_multipart($FormPath);
                    ?>
								
									<div class="kt-portlet__body">
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_move_from_store');?></label>
												<?php
												$StoreData = $this->M_store->GetRow($store_id);
												if ($this->session->userdata('lang') == "ar")
												{
													$FromStore = $StoreData->title;
												}
												else
												{
													$FromStore = $StoreData->title_en;
												}
												?>
												<input type="text" name="storename" class="form-control" value="<?php echo $FromStore;?>" required readonly>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_move_to_store');?></label>
												<select name="to_store" class="form-control" required>
													<?php
													$StoreList = $this->M_store->GetAnotheStores($store_id);
													foreach($StoreList as $StoreList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$ToStore = $StoreList_Row->title;
														}
														else
														{
															$ToStore = $StoreList_Row->title_en;
														}
													?>
													<option value="<?php echo $StoreList_Row->id;?>"><?php echo $ToStore;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_move_product_code');?></label>
												<input type="text" name="product_id" class="form-control" value="<?php echo $barcode;?>" required readonly>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('store_move_quantity');?></label>
												<input type="number" min="0" step="1" name="quantity" class="form-control" value="0" required>
											</div>
										</div>
										
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<?php $company_id = intval($this->session->userdata('company_id'));?>
													<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
													<input type="hidden" name="from_store" value="<?php echo $store_id;?>">
													<input type="hidden" name="product_id" value="<?php echo $product_id;?>">
													
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>