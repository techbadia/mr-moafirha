<div class="dropdown">
    <!--begin::Toggle-->
    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
        <div class="btn btn-icon btn-clean btn-dropdown btn-lg mr-1">
            <span class="svg-icon svg-icon-xl svg-icon-primary">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Media/Equalizer.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5" />
                        <rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5" />
                        <rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5" />
                        <rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
        </div>
    </div>
    <!--end::Toggle-->
    <!--begin::Dropdown-->
    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
        <!--begin:Header-->
        <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url(<?php echo base_url();?>assets/media/misc/bg-1.jpg)">
            <h4 class="text-white font-weight-bold"><?php echo $this->lang->line('QuickActions');?></h4>
            <span class="btn btn-success btn-sm font-weight-bold font-size-sm mt-2">
                <?php
                $PendingActions = $this->M_serv_action->PendingActions();
                echo $PendingActions;
                ?> <?php echo $this->lang->line('PendingAction');?>
            </span>
        </div>
        <!--end:Header-->
        <!--begin:Nav-->
        <div class="row row-paddingless">
            <?php
            $ActionTypes = $this->M_serv_action_types->Get_all();
            foreach($ActionTypes as $ActionTypes_Row) {
                $ActionTypeID = $ActionTypes_Row->id;
                if ($this->session->userdata('lang') == "ar")
                {
                    $ActionTypeTitle = $ActionTypes_Row->ar_title;
                }
                else
                {
                    $ActionTypeTitle = $ActionTypes_Row->en_title;
                }
            ?>
            <!--begin:Item-->
            <div class="col-6">
                <a href="<?php echo base_url();?>service/serv_action/type/<?php echo $ActionTypeID;?>" class="d-block py-10 px-5 text-center bg-hover-light border-right border-bottom">
                    <?php echo $ActionTypes_Row->svg_icon;?>
                    <span class="d-block text-dark-75 font-weight-bold font-size-h6 mt-2 mb-1"><?php echo $ActionTypeTitle;?></span>
                </a>
            </div>
            <!--end:Item-->
            <?php
            }
            ?>
            
        </div>
        <!--end:Nav-->
    </div>
    <!--end::Dropdown-->
</div>