
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
	$FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
	echo form_open_multipart($FormPath);
?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('sale_fullname');?></label>
				<select name="customer_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
					<?php
					$CustomersList = $this->M_sale_customer->GetMultiRow();
					foreach($CustomersList as $CustomersList_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$CustomerTitle = $CustomersList_Row->ar_title;
						}
						else
						{
							$CustomerTitle = $CustomersList_Row->en_title;
						}
						$Phone = $CustomersList_Row->phone;
					?>
					<option value="<?php echo $CustomersList_Row->id;?>"><?php echo $CustomerTitle." : ".$Phone;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('cars_cars_model_id');?></label>
				<select name="model_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
					<?php
					$ModelList = $this->M_cars_model->GetMultiRow();
					foreach($ModelList as $ModelList_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Brand = $ModelList_Row->title;
						}
						else
						{
							$Brand = $ModelList_Row->title_en;
						}
					?>
					<option value="<?php echo $ModelList_Row->id;?>"><?php echo $Brand;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('cars_cars_model_name');?></label>
				<input type="text" name="model_name" class="form-control" value="" required>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('cars_cars_chassis_no');?></label>
				<input type="text" name="chassis_no" class="form-control" value="" required>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('cars_cars_plate_No');?></label>
				<input type="text" name="plate_No" class="form-control" value="">
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('cars_cars_meter_reading');?></label>
				<input type="number" min="0" step="0.001" name="meter_reading" class="form-control" value="" required>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('cars_cars_motor_number');?></label>
				<input type="text" name="motor_number" class="form-control" value="" required>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('cars_cars_color');?></label>
				<input type="text" name="color" class="form-control" value="" required>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('cars_cars_oil_change_date');?></label>
				<input type="date" name="oil_change_date" class="form-control" value="<?php echo date("Y-m-d");?>" required>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('cars_cars_oil_change_every');?></label>
				<input type="number" min="30" step="1" name="oil_change_every" class="form-control" value="" required>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<label class="col-form-label"><?php echo lang('cars_cars_notes');?></label>
				<textarea name="notes" class="form-control" rows="3" required></textarea>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<label><?php echo lang('cars_cars_images');?> : <small><?php echo lang('multiple_files');?></small></label>
				<div></div>
				<div class="custom-file">
					<input type='file' name='files[]' multiple="" class="custom-file-input" id="customFile" />
					<label class="custom-file-label" for="customFile"><?php echo lang('Choose_files');?></label>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php //echo $id;?>">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
<?php
	echo form_close();
?>
</div>