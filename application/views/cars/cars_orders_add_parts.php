<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <?php
        $FormPath = base_url().$Segment1."/".$Segment2."/insert_parts";
        echo form_open_multipart($FormPath);
    ?>
    <div class="kt-portlet__body">
        <div class="form-group row">
            <div class="col-lg-12">
                <h3>
                <?php
                $CarData = $this->M_cars_cars->GetRow($car_id);
                echo lang('cars_cars_plate_No')." : ".$CarData->plate_No;
                ?>
                </h3>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-5 col-md-5 col-sm-12 p-3 mb-2 bg-primary text-white">
                <label class="col-form-label" style="color:#FFFFFF"><?php echo lang('cars_orders_part_name');?></label>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
                <label class="col-form-label" style="color:#FFFFFF"><?php echo lang('qunatity');?></label>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
                <label class="col-form-label" style="color:#FFFFFF"><?php echo lang('buy_bill_items_unitprice');?></label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 p-3 mb-2 bg-primary text-white">
                <label class="col-form-label" style="color:#FFFFFF"><?php echo lang('buy_bill_image');?></label>
            </div>
        
            <?php
            $InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
            $BackgroundColor = "p-3 mb-2 bg-success-o-40";
            for ($i = 1; $i <= $InvoiceItems; $i++) {
                if($BackgroundColor == "p-3 mb-2 bg-success-o-40")
                {
                    $BackgroundColor = "p-3 mb-2 bg-success-o-20";
                }
                else
                {
                    $BackgroundColor = "p-3 mb-2 bg-success-o-40";
                }
            ?>
        
            <div class="col-lg-5 col-md-5 col-sm-12 <?php echo $BackgroundColor;?>">
                <input type="text" name="part_name<?php echo $i;?>" id="part_name<?php echo $i;?>" class="form-control" value="">
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
                <input type="number" min="0" name="qunatity<?php echo $i;?>" id="qunatity<?php echo $i;?>" class="form-control" value="0">
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
                <input type="number" min="0" step="0.001" name="unit_price<?php echo $i;?>" id="unit_price<?php echo $i;?>" class="form-control" value="0">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 <?php echo $BackgroundColor;?>">
                <input type="file" name="image<?php echo $i;?>" class="form-control" dir="ltr">
            </div>
        
            <?php
            }
            ?>
        </div>
    </div>
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            <div class="row">
                <div class="col-lg-12 ml-lg-auto">
                    <input type="hidden" name="id" value="<?php echo $id;?>">
                    <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                    <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                </div>
            </div>
        </div>
    </div>
    <?php
        echo form_close();
    ?>
</div>