<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <?php
        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
        echo form_open_multipart($FormPath);
    ?>
    <div class="kt-portlet__body">
        <div class="form-group row">
            <div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('cars_cars');?></label>
				<select name="car_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
					<?php
					$CarList = $this->M_cars_cars->GetMultiRow();
					foreach($CarList as $CarList_Row) {
                        $customer_id = $CarList_Row->customer_id;
                        $CustomerData = $this->M_sale_customer->GetRow($customer_id);
						if ($this->session->userdata('lang') == "ar")
						{
							$CustomerTitle = $CustomerData->ar_title;
						}
						else
						{
							$CustomerTitle = $CustomerData->en_title;
						}
						$Phone = $CustomerData->phone;
					?>
					<option value="<?php echo $CarList_Row->id;?>"><?php echo $CustomerTitle." : ".$Phone." : ".lang('cars_cars_plate_No')." = ".$CarList_Row->plate_No;?></option>
					<?php } ?>
				</select>
			</div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <label class="col-form-label"><?php echo $this->lang->line('data_request_status_id');?></label>
                <select id="status_id" name="status_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                    <?php
                    foreach($Status as $Status_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $Status_Row->ar_title;
                        }
                        else
                        {
                            $Title = $Status_Row->en_title;
                        }
                    ?>
                    <option value="<?php echo $Status_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <label class="col-form-label"><?php echo $this->lang->line('closed_date');?></label>
                <input type="date" id="closed_date" name="closed_date" class="form-control" value="<?php echo date("Y-m-d"); ?>" required>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <label class="col-form-label"><?php echo $this->lang->line('closed_time');?></label>
                <input type="time" id="closed_time" name="closed_time" class="form-control" value="<?php echo date("h:m"); ?>" required>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <label class="col-form-label"><?php echo lang('data_invoice_teamwork_id');?></label>
                <select name="teamwork_id" class="form-control" class="form-control" required>
                    <?php
                    foreach($TeamWork as $TeamWork_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $TeamWork_Row->ar_title;
                        }
                        else
                        {
                            $Title = $TeamWork_Row->en_title;
                        }
                    ?>
                    <option value="<?php echo $TeamWork_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <label class="col-form-label"><?php echo $this->lang->line('data_request_area_id');?></label>
                <select id="area_id" name="area_id" class="form-control" required>
                    <?php
                    foreach($Area as $Area_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $Area_Row->ar_title;
                        }
                        else
                        {
                            $Title = $Area_Row->en_title;
                        }
                    ?>
                    <option value="<?php echo $Area_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-md-12">
                <label class="col-form-label"><?php echo $this->lang->line('data_request_details');?></label>
                <input type="text" id="details" name="details" class="form-control" required>
            </div>
            <div class="form-group col-md-12">
                <label class="col-form-label"><?php echo $this->lang->line('set_service');?></label>
                <select id="ServiceList[]" name="ServiceList[]" class="form-control kt-selectpicker" data-live-search="true" data-size="8" multiple="multiple" size="8" required>
                    <?php
                    $ServiceList = $this->M_cars_service->GetMultiRow();
                    foreach($ServiceList as $ServiceList_Row) {
                        $Title = "";
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $ServiceList_Row->ar_title;
                        }
                        else
                        {
                            $Title = $ServiceList_Row->en_title;
                        }
                    ?>
                    <option value="<?php echo $ServiceList_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-5 col-md-5 col-sm-12 p-3 mb-2 bg-primary text-white">
                <label class="col-form-label" style="color:#FFFFFF"><?php echo lang('cars_orders_part_name');?></label>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
                <label class="col-form-label" style="color:#FFFFFF"><?php echo lang('qunatity');?></label>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
                <label class="col-form-label" style="color:#FFFFFF"><?php echo lang('buy_bill_items_unitprice');?></label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 p-3 mb-2 bg-primary text-white">
                <label class="col-form-label" style="color:#FFFFFF"><?php echo lang('buy_bill_image');?></label>
            </div>
        
            <?php
            $InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
            $BackgroundColor = "p-3 mb-2 bg-success-o-40";
            for ($i = 1; $i <= $InvoiceItems; $i++) {
                if($BackgroundColor == "p-3 mb-2 bg-success-o-40")
                {
                    $BackgroundColor = "p-3 mb-2 bg-success-o-20";
                }
                else
                {
                    $BackgroundColor = "p-3 mb-2 bg-success-o-40";
                }
            ?>
        
            <div class="col-lg-5 col-md-5 col-sm-12 <?php echo $BackgroundColor;?>">
                <input type="text" name="part_name<?php echo $i;?>" id="part_name<?php echo $i;?>" class="form-control" value="">
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
                <input type="number" min="0" name="qunatity<?php echo $i;?>" id="qunatity<?php echo $i;?>" class="form-control" value="0">
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
                <input type="number" min="0" step="0.001" name="unit_price<?php echo $i;?>" id="unit_price<?php echo $i;?>" class="form-control" value="0">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 <?php echo $BackgroundColor;?>">
                <input type="file" name="image<?php echo $i;?>" class="form-control" dir="ltr">
            </div>
        
            <?php
            }
            ?>
        </div>
    </div>
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            <div class="row">
                <div class="col-lg-12 ml-lg-auto">
                    <input type="hidden" name="id" value="<?php //echo $id;?>">
                    <input type="hidden" name="source" value="<?php //echo $source;?>">
                    <input type="hidden" name="insert_date" value="<?php //echo $insert_date;?>">
                    <input type="hidden" name="insert_time" value="<?php //echo $insert_time;?>">
                    <input type="hidden" name="deleted" value="0<?php //echo $deleted;?>">
                    <?php
                    $company_id = intval($this->session->userdata('company_id'));
                    ?>
                    <input type="hidden" name="company_id" value="<?php echo $company_id;?>">

                    <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                    <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                </div>
            </div>
        </div>
    </div>
    <?php
        echo form_close();
    ?>
</div>