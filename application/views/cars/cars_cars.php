
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('sale_fullname');?></th>
												<th><?php echo lang('cars_cars_chassis_no');?></th>
												<th><?php echo lang('cars_cars_model_id');?></th>
												<th><?php echo lang('cars_cars_plate_No');?></th>
												<th><?php echo lang('cars_cars_meter_reading');?></th>
												<th><?php echo lang('cars_cars_motor_number');?></th>
												<th><?php echo lang('cars_cars_color');?></th>
												<th><?php echo lang('cars_cars_images');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
												<?php
												$customer_id = $DataRows_Row->customer_id;
												$CustomerData = $this->M_sale_customer->GetRow($customer_id);
												if ($this->session->userdata('lang') == "ar")
												{
													$CustomerTitle = $CustomerData->ar_title;
												}
												else
												{
													$CustomerTitle = $CustomerData->en_title;
												}
												$Phone = $CustomerData->phone;
												echo $CustomerTitle." : ".$Phone;
												?>
												</td>
												<td><?php echo $DataRows_Row->chassis_no;?></td>
												<td>
													<?php
													$model_id = $DataRows_Row->model_id;
													$ModelData = $this->M_cars_model->GetRow($model_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $ModelData->title." : ".$DataRows_Row->model_name;
													}
													else
													{
														echo $ModelData->title_en." : ".$DataRows_Row->model_name;
													}
													?>
												</td>
												<td><?php echo $DataRows_Row->plate_No;?></td>
												<td><?php echo $DataRows_Row->meter_reading;?></td>
												<td><?php echo $DataRows_Row->motor_number;?></td>
												<td><?php echo $DataRows_Row->color;?></td>
												<td>
												<?php
												$CarImages = $this->M_cars_cars_images->GetImages($id);
												foreach($CarImages as $CarImages_Row) {
													echo "<a target='_blank' href='".base_url()."upload/cars/".$CarImages_Row->image."'>".$CarImages_Row->id."</a><br>";
												}
												?>
												</td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
									</table>

								