
<div class="card card-custom gutter-b example example-compact">
<?php
$attributes = array('id' => 'kt_form_2', 'name' => 'kt_form_2');
										$FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
										echo form_open_multipart($FormPath, $attributes);
									?>

									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-3 col-md-3 col-sm-12 bg-primary text-white">
												<label class="col-form-label"><?php echo lang('fin_journal_debit');?></label>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12 bg-primary text-white">
												<label class="col-form-label"><?php echo lang('fin_journal_account_id');?></label>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12 bg-primary text-white">
												<label class="col-form-label"><?php echo lang('fin_journal_creditor');?></label>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12 bg-primary text-white">
												<label class="col-form-label"><?php echo lang('fin_journal_account_id');?></label>
											</div>
										</div>
										<div class="form-group row">
										<?php
										$BackgroundColor = "p-3 mb-2 bg-light text-dark";
										for ($i = 1; $i <= 1; $i++) {
											if($BackgroundColor == "p-3 mb-2 bg-light text-dark")
											{
												$BackgroundColor = "p-3 mb-2 bg-white text-dark";
											}
											else
											{
												$BackgroundColor = "p-3 mb-2 bg-light text-dark";
											}
										?>
											<div class="col-lg-3 col-md-3 col-sm-12 <?php echo $BackgroundColor;?>">
												
												<input type="number" step="0.001" min="0" name="debit<?php echo $i?>" id="debit<?php echo $i?>" class="form-control" onfocus="TotalDebitValue()" onblur="TotalDebitValue()" value="0">
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12 <?php echo $BackgroundColor;?>">
												<select name="fromaccount<?php echo $i?>" class="form-control kt-selectpicker" data-size="5" data-live-search="true">
													<?php
													$acc_custody = intval($this->session->userdata('acc_custody'));
													$AccountList = $this->M_fin_treeaccount->GetMultiRow();
													foreach($AccountList as $AccountList_Row) {
														$parent_id = intval($AccountList_Row->parent_id);
														if($acc_custody == $parent_id) {
														if ($this->session->userdata('lang') == "ar")
														{
															$AccountTitle = $AccountList_Row->title;
														}
														else
														{
															$AccountTitle = $AccountList_Row->title_en;
														}
													?>
													<option value="<?php echo $AccountList_Row->id;?>"><?php echo $AccountTitle;?></option>
													<?php
														}
												}
												?>
												</select>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12 <?php echo $BackgroundColor;?>">
												<input type="number" step="0.001" min="0" name="creditor<?php echo $i?>" id="creditor<?php echo $i?>" class="form-control" onfocus="TotalCreditorValue()" onblur="TotalCreditorValue()" value="0">
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12 <?php echo $BackgroundColor;?>">
												<select name="toaccount<?php echo $i?>" class="form-control kt-selectpicker" data-size="5" data-live-search="true">
													<?php
													$acc_treasury = intval($this->session->userdata('acc_treasury'));
													$acc_bank = intval($this->session->userdata('acc_bank'));
													$AccountList = $this->M_fin_treeaccount->GetMultiRow();
													foreach($AccountList as $AccountList_Row) {
														$account_id = $AccountList_Row->id;
														if(($account_id == $acc_treasury) || ($account_id == $acc_bank)) {
														if ($this->session->userdata('lang') == "ar")
														{
															$AccountTitle = $AccountList_Row->title;
														}
														else
														{
															$AccountTitle = $AccountList_Row->title_en;
														}
													?>
													<option value="<?php echo $AccountList_Row->id;?>"><?php echo $AccountTitle;?></option>
													<?php
														}
													}
													?>
												</select>
											</div>
										<?php } ?>
										</div>
										<div class="form-group row">
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_journal_debit_total');?></label>
												<input type="number" step="0.001" min="0" name="debit_total" id="debit_total" class="form-control" value="0">
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12"></div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_journal_creditor_total');?></label>
												<input type="number" step="0.001" min="0" name="creditor_total" id="creditor_total" class="form-control" value="0">
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12"></div>
										</div>
										<div class="form-group row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_journal_details');?></label>
												<input type="text" name="details" id="details" class="form-control" value="" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_journal_thedate');?></label>
												<input type="date" name="thedate" class="form-control" value="<?php echo date('Y-m-d');?>" required>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_journal_image');?></label>
												<input type="file" name="image" id="image" class="form-control" dir="ltr" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<input type="hidden" name="deleted" value="0">
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
										echo form_close();
									?>

								</div>