
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('fin_treeaccount_category_id');?></th>
												<th><?php echo lang('fin_treeaccount_parent_id');?></th>
												<th><?php echo lang('fin_treeaccount_account_no');?></th>
												<th><?php echo lang('fin_treeaccount_title');?></th>
												<th><?php echo lang('fin_treeaccount_title_en');?></th>
												<th><?php echo lang('fin_treeaccount_startamount');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('View');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$fin_treeaccount_id = $DataRows_Row->id;
												$acc_custody = intval($this->session->userdata('acc_custody'));
												$category_id = intval($DataRows_Row->category_id);
												$parent_id = intval($DataRows_Row->parent_id);
												if($acc_custody == $parent_id) {
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
													<?php
													$CategoryData = $this->M_fin_treecategory->GetRow($category_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $CategoryData->title;
													}
													else
													{
														echo $CategoryData->title_en;
													}
													?>
												</td>
												<td>
													<?php
													if($parent_id > 0)
													{
														$ParentData = $this->M_fin_treeaccount->GetRow($parent_id);
														if ($this->session->userdata('lang') == "ar")
														{
															echo $ParentData->title;
														}
														else
														{
															echo $ParentData->title_en;
														}
													}
													?>
												</td>
												<td><?php echo $DataRows_Row->account_no;?></td>
												<td><?php echo $DataRows_Row->title;?></td>
												<td><?php echo $DataRows_Row->title_en;?></td>
												<td><?php echo $DataRows_Row->startamount;?></td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<a href="<?php echo base_url()."/account/fin_treeaccount/view_account/".$DataRows_Row->id;?>" target="_lank">
														<i class="flaticon-search text-success icon-lg"></i> 
													</a>
												</td>
											</tr>
											<?php
												}
											}
											?>
										</tbody>
										
									</table>