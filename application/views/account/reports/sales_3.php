
<div id="DivForPrint" <?php echo $this->session->userdata('HTML');?>>
	<br><br>
	<div style="margin-left: auto; margin-right: auto; width: 80%; text-align: center; font-weight: bold">
		<?php echo $title;?>
		<br>
		<?php echo $fromdate." : ".$todate;?>
	</div>
	<br><br>
	<!--begin: Datatable -->
	<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
		<thead>
			<tr>
				<th><?php echo lang('Serial');?></th>
				<th>ID</th>
				<th><?php echo lang('sale_fullname');?></th>
				<th><?php echo lang('buy_manufacturer_phone');?></th>
				<th><?php echo lang('buy_manufacturer_mobile');?></th>
				<th><?php echo lang('buy_manufacturer_email');?></th>
				<th><?php echo lang('buy_manufacturer_person');?></th>
				<th><?php echo lang('Balance');?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$Serial = 0;
			foreach($DataRows as $DataRows_Row) {
				$buy_manufacturer_id = $DataRows_Row->id;
				$accountid = $DataRows_Row->id;
				
				$StartAmount = 0;
				$BeforDay = date('Y-m-d', strtotime('-1 days', strtotime($fromdate)));
				$AmountBeforeJournal = $StartAmount;
				$AmountBeforeJournalData = $this->M_fin_journal->GetRealtedJournalStartDate($accountid, $fromdate);
				$AmountBeforeJournalValue = 0;
				$RealtedJournalStartDateCount = $this->M_fin_journal->GetRealtedJournalStartDateCount($accountid, $fromdate);
				$AccountCategoryID = 0;
				if ($RealtedJournalStartDateCount >0)
				{
					foreach($AmountBeforeJournalData as $AmountBeforeJournalData_Row) {
						//$AmountBeforeJournalValue = doubleval($AmountBeforeJournalData_Row->thevalue);
						$AccountData = $this->M_fin_treeaccount->GetRow($accountid);
						$AccountCategoryID = $AccountData->category_id;
						if (($AccountCategoryID ==1) || ($AccountCategoryID ==5))
						{
							$AmountBeforeJournal += doubleval($AmountBeforeJournalData_Row->debit);
							$AmountBeforeJournal -= doubleval($AmountBeforeJournalData_Row->creditor);
						}
						else
							{
								
							}
					}
				}
				//echo $AmountBeforeJournal;
				$Balance = $AmountBeforeJournal;

				$TotalValue = 0;
				
				$Debit = 0;
				$DebitTotal = 0;
				$CreditorTotal = 0;
				$Creditor = 0;
				$CategoryID = 0;
				$TheValue = 0;
				$OtherAccountName = "";
				$Serial += 1;
				$MultiRows = $this->M_fin_journal_main->GetAccountAction_RangDate($accountid, $fromdate, $todate);
				foreach($MultiRows as $MultiRows_Row) {
				
				if ($this->session->userdata('lang') == "ar")
				{
					$AccountTitle = $MultiRows_Row->title;
				}
				else
				{
					$AccountTitle = $MultiRows_Row->title_en;
				}

				$AccountName = $this->M_fin_treeaccount->GetRow($accountid);
				$category_id = $AccountName->category_id;

				$Debit += doubleval($MultiRows_Row->debit);
				$DebitTotal += $Debit;
				
				$Creditor += doubleval($MultiRows_Row->creditor);
				$CreditorTotal += $Creditor;
				$CategoryID = 0;
				$TheValue = 0;
				if(($category_id == 1) || ($category_id == 5))
				{

					$Balance = $Balance + $MultiRows_Row->debit - $MultiRows_Row->creditor;
				}
				else
				{
					$Balance = $Balance + $MultiRows_Row->creditor - $MultiRows_Row->debit;
				}
				//echo $this->Converter->ConvertToArabic($Balance);
				}
			?>
			<tr>
				<td><?php echo $Serial;?></td>
				<td>
				<a href="<?php echo base_url();?>account/fin_treeaccount/view_account/<?php echo $DataRows_Row->id;?>" target="_blank"><?php echo $DataRows_Row->id;?></a>
				</td>
				<td><?php echo $DataRows_Row->ar_title;?></td>
				<td><?php echo $DataRows_Row->phone;?></td>
				<td><?php echo $DataRows_Row->mobile1;?></td>
				<td><?php echo $DataRows_Row->email;?></td>
				<td><?php echo $DataRows_Row->person;?></td>
				<td>
				<?php
				echo doubleval($Balance);
				?>
				</td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>
</div>

<div class="d-flex justify-content-between">
	<button type="button" class="btn btn-primary font-weight-bold" onclick="printDiv();"><?php echo $this->lang->line('Print');?></button>
</div>	