<br>
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
$Segment1 = $this->uri->segment(1);
$Segment2 = $this->uri->segment(2);
$FormPath = base_url().$Segment1."/".$Segment2."/"."store_3_search";
echo form_open_multipart($FormPath);
?>
<div class="row">
	<div class="col-md-11">
		<label class="col-form-label"><?php echo lang('store_quantity_store_id');?></label>
		<select name="store_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
			<?php
			$StoreList = $this->M_store->GetMultiRow();
			foreach($StoreList as $StoreList_Row) {
				if ($this->session->userdata('lang') == "ar")
				{
					$StoreTitle = $StoreList_Row->title;
				}
				else
				{
					$StoreTitle = $StoreList_Row->title_en;
				}
			?>
			<option <?php if($store_id == $StoreList_Row->id) {?>selected<?php }?> value="<?php echo $StoreList_Row->id;?>"><?php echo $StoreTitle;?></option>
			<?php } ?>
		</select>
	</div>
	<div class="col-md-1">
		<button type="submit" class="btn btn-primary mr-2" style="margin-top:35px;"><?php echo $this->lang->line('Search');?></button>
	</div>
</div>
<?php
echo form_close();
?>
</div>

<div id="DivForPrint" <?php echo $this->session->userdata('HTML');?>>
	<br><br>
	<div style="margin-left: auto; margin-right: auto; width: 80%; text-align: center; font-weight: bold">
		<?php
		echo $title;
		if($store_id > 0)
		{
			echo " : ";
			$StoreName = $this->M_store->GetRow($store_id);
			if ($this->session->userdata('lang') == "ar")
			{
				echo $StoreName->title;
			}
			else
			{
				echo $StoreName->title_en;
			}
		}
		?>
	</div>
	<br><br>
	<!--begin: Datatable -->
	<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
		<thead>
			<tr>
				<th>ID</th>
				<th><?php echo lang('product_barcode');?></th>
				<th><?php echo lang('product_category_id');?></th>
				<th><?php echo lang('product_brand_id');?></th>
				<th><?php echo lang('product_title');?></th>
				<th><?php echo lang('store_quantity_store_id');?></th>
				<th><?php echo lang('product_Price');?></th>
				<th><?php echo lang('product_lowest_price');?></th>
				<th><?php echo lang('store_quantity_quantity');?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			if($store_id > 0) {
			foreach($DataRows as $DataRows_Row) {
				$product_id = $DataRows_Row->id;
			?>
			<tr>
				<td><?php echo $DataRows_Row->id;?></td>
				<td><?php echo $DataRows_Row->barcode;?></td>
				<td>
					<?php
					$category_id = $DataRows_Row->category_id;
					$CategoryData = $this->M_store_category->GetRow($category_id);
					if ($this->session->userdata('lang') == "ar")
					{
						echo $CategoryData->title;
					}
					else
					{
						echo $CategoryData->title_en;
					}
					?>
				</td>
				<td>
					<?php
					$brand_id = $DataRows_Row->brand_id;
					$BrandData = $this->M_store_brand->GetRow($brand_id);
					if ($this->session->userdata('lang') == "ar")
					{
						echo $BrandData->title;
					}
					else
					{
						echo $BrandData->title_en;
					}
					?>
				</td>
				<td><?php echo $DataRows_Row->title;?></td>
				<td>
				<?php
				$StoreName = $this->M_store->GetRow($store_id);
				if ($this->session->userdata('lang') == "ar")
					{
						echo $StoreName->title;
					}
					else
					{
						echo $StoreName->title_en;
					}
				?>
				</td>
				<td><?php echo $DataRows_Row->price;?></td>
				<td><?php echo $DataRows_Row->lowest_price;?></td>
				<td>
				<?php
				$CheckCount = $this->M_store_quantity->CountStoreProduct($store_id, $product_id);
				if($CheckCount > 0)
				{
					$Qunatity = $this->M_store_quantity->GetTotalQuantityByStore($store_id, $product_id);
					echo $Qunatity->quantity;
				}
				else
				{
					echo "(0)";
				}
				?>
				</td>
			</tr>
			<?php
			}
		}
			?>
		</tbody>
	</table>

</div>


<div class="d-flex justify-content-between">
	<button type="button" class="btn btn-primary font-weight-bold" onclick="printDiv();"><?php echo $this->lang->line('Print');?></button>
</div>	