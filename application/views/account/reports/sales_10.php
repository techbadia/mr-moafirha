<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
	<thead>
		<tr>
			<th>ID</th>
			<th><?php echo lang('product_barcode');?></th>
			<th><?php echo lang('product_title');?></th>
			<th><?php echo lang('product_title_en');?></th>
			<th><?php echo lang('qunatity');?></th>
			<th><?php echo lang('product_Price');?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach($DataRows as $DataRows_Row) {
		?>
		<tr>
			<td><?php echo $DataRows_Row->id;?></td>
			<td><?php echo $DataRows_Row->barcode;?></td>
			<td><?php echo $DataRows_Row->title;?></td>
			<td><?php echo $DataRows_Row->title_en;?></td>
			<td><?php echo $DataRows_Row->quantity;?></td>
			<td><?php echo $DataRows_Row->unitprice;?></td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>