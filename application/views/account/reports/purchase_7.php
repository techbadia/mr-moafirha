<br>
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
$Segment1 = $this->uri->segment(1);
$Segment2 = $this->uri->segment(2);
$FormPath = base_url().$Segment1."/".$Segment2."/"."purchase_7_search";
echo form_open_multipart($FormPath);
?>
<div class="row">
	<div class="col-md-2">
		<label class="col-form-label"><?php echo lang('from_date');?></label>
		<input type="date" name="fromdate" class="form-control" value="<?php echo $fromdate;?>" required>
	</div>
	<div class="col-md-2">
		<label class="col-form-label"><?php echo lang('to_date');?></label>
		<input type="date" name="todate" class="form-control" value="<?php echo $todate;?>" required>
	</div>
	<div class="col-md-4">
		<label class="col-form-label"><?php echo lang('store_quantity_product_id');?></label>
		<select name="product_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
		<option <?php if($product_id == 0) {?>selected<?php }?> value="0">-</option>
		<?php
		$ProductList = $this->M_product->GetMultiRow();
		foreach($ProductList as $ProductList_Row) {
			if ($this->session->userdata('lang') == "ar")
			{
				$ProductTitle = $ProductList_Row->title;
			}
			else
			{
				$ProductTitle = $ProductList_Row->title_en;
			}
		?>
		<option <?php if($product_id == $ProductList_Row->id) {?>selected<?php }?> value="<?php echo $ProductList_Row->id;?>"><?php echo $ProductTitle;?></option>
		<?php } ?>
		</select>
	</div>
	<div class="col-md-3">
		<label class="col-form-label"><?php echo lang('buy_bill_supplier_id');?></label>
		<select name="supplier_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
		<option <?php if($supplier_id == 0) {?>selected<?php }?> value="0">-</option>
			<?php
			$SuppliersList = $this->M_buy_manufacturer->GetMultiRow();
			foreach($SuppliersList as $SuppliersList_Row) {
				if ($this->session->userdata('lang') == "ar")
				{
					$SupplierTitle = $SuppliersList_Row->title;
				}
				else
				{
					$SupplierTitle = $SuppliersList_Row->title_en;
				}
			?>
			<option <?php if($supplier_id == $SuppliersList_Row->id) {?>selected<?php }?> value="<?php echo $SuppliersList_Row->id;?>"><?php echo $SupplierTitle;?></option>
			<?php } ?>
		</select>
	</div>
	<div class="col-md-1">
		<button type="submit" class="btn btn-primary mr-2" style="margin-top:35px;"><?php echo $this->lang->line('Search');?></button>
	</div>
</div>
<?php
echo form_close();
?>
</div>

<div id="DivForPrint" <?php echo $this->session->userdata('HTML');?>>
	<br><br>
	<div style="margin-left: auto; margin-right: auto; width: 80%; text-align: center; font-weight: bold">
		<?php echo $title;?>
		<br>
		<?php echo $fromdate." : ".$todate;?>
	</div>
	<br><br>
	<!--begin: Datatable -->
	<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
		<thead>
			<tr>
				<th><?php echo lang('Serial');?></th>
				<th><?php echo lang('buy_bill_supplier_id');?></th>
				<th><?php echo lang('data_customer_phone');?></th>
				<th><?php echo lang('buy_bill_thedate');?></th>
				<th><?php echo lang('store_quantity_product_id');?></th>
				<th><?php echo lang('buy_bill_items_unitprice');?></th>
				<th><?php echo lang('data_account_invoice_id');?></th>
				<th><?php echo lang('Total');?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$Serial = 0;
			foreach($DataRows as $DataRows_Row) {
				$Serial += 1;
			?>
			<tr>
				<td><?php echo $Serial;?></td>
				<td>
					<?php
					if ($this->session->userdata('lang') == "ar")
					{
						$SupplierTitle = $DataRows_Row->supplier_ar_title;
					}
					else
					{
						$SupplierTitle = $DataRows_Row->supplier_en_title;
					}
					echo $SupplierTitle;
					?>
				</td>
				<td><?php echo $DataRows_Row->phone;?></td>
				<td><?php echo $DataRows_Row->thedate;?></td>
				<td>
					<?php
					if ($this->session->userdata('lang') == "ar")
					{
						$ProductTitle = $DataRows_Row->product_ar_title;
					}
					else
					{
						$ProductTitle = $DataRows_Row->product_en_title;
					}
					echo $ProductTitle;
					?>
				</td>
				<td><?php echo $DataRows_Row->unitprice;?></td>
				<td><?php echo "#".$DataRows_Row->bill_id;?></td>
				<td><?php echo $DataRows_Row->totalvalue;?></td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>

</div>


<div class="d-flex justify-content-between">
	<button type="button" class="btn btn-primary font-weight-bold" onclick="printDiv();"><?php echo $this->lang->line('Print');?></button>
</div>									
		
		<div id="show_modal" class="modal fade" role="dialog" style="background: #000;">
			<div class="modal-dialog" style="min-width: 600px;">
				<div class="modal-content">
				<div class="modal-header">
					<h3 style="font-size: 24px; color: #17919e; text-shadow: 1px 1px #ccc;">
					<i class="fa fa-folder"></i><?php echo lang('buy_bill');?>
					</h3>
				</div>
				<div class="modal-body">
				<div class="row">
					<div class="col-3"><?php echo lang('expenses_bill_id');?></div>
					<div class="col-3" id="id"></div>
					<div class="col-3"><?php echo lang('expenses_thedate');?></div>
					<div class="col-3" id="thedate"></div>
				</div>
				<div class="row">
					<div class="col-3"><?php echo lang('buy_bill_supplier_id');?></div>
					<div class="col-9" id="supplier"></div>
				</div>
				<div class="row">
					<div class="col-3"><?php echo lang('buy_bill_totalvalue');?></div>
					<div class="col-3" id="totalvalue"></div>
					<div class="col-3"><?php echo lang('buy_bill_paid');?></div>
					<div class="col-3" id="paid"></div>
				</div>
				<div class="row">
					<div class="col-3"><?php echo lang('buy_bill_remaining');?></div>
					<div class="col-3" id="remaining"></div>
					<div class="col-3"><?php echo lang('buy_bill_discount');?></div>
					<div class="col-3" id="discount"></div>
				</div>
				<div class="row">
					<div class="col-3"><?php echo lang('buy_bill_tree_id');?></div>
					<div class="col-9" id="account_title"></div>
				</div>
				<div class="row">
					<div class="col-12" id="notes"></div>
				</div>
				<table id="ModalTable" class="table table-bordered table-striped">
					<thead class="btn-primary">
						<tr>
							<th align='right'><?php echo lang('buy_bill_items_store_type_id');?></th>
							<th style="text-align:center"><?php echo lang('buy_bill_items_quantity');?></th>
							<th style="text-align:center"><?php echo lang('buy_bill_items_unitprice');?></th>
							<th style="text-align:center"><?php echo lang('fin_journal_Total');?></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>

					

				</div>
				<div class="modal-footer d-print-none">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo lang('Close');?></button>
					<button type="button" class="btn btn-info" onclick="printSelection(document.getElementById('show_modal'));return false;"><i class="fa fa-print"></i> <?php echo $this->lang->line('Print');?></button>
				</div>
				</div>
			</div>
		</div>




<div id="modalDiv">
	<div class="modal fade" id="buy_bill_pay" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: #c0edf1; text-align:<?php echo $this->session->userdata('Alignment');?> !important">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title" style="direction:<?php echo $this->session->userdata('Direction');?> !important">
						<?php echo $this->lang->line('buy_bill_Pay');?>
					</h4>
				</div>
				<?php
				$FormName = "buy_bill_pay";
				$Target = base_url().$Segment1."/".$Segment2."/close_invoice";
					echo form_open_multipart($Target, "id=$FormName name=$FormName");
				?>
				<div id="modalDiv">
					<div class="modal-body">
						<div class="row">               
							<div class="col-lg-4 col-md-4 col-sm-4">
								<label><?php echo $this->lang->line('buy_bill_totalvalue');?></label>
								<input type="number" id="totalvalue" name="totalvalue" value="" readonly class="form-control" required>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<label><?php echo $this->lang->line('buy_bill_remaining');?></label>
								<input type="number" min="0" id="remaining" name="remaining" readonly value="" class="form-control" required>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<label><?php echo $this->lang->line('buy_bill_paid');?></label>
								<input type="number" min="0" step="0.001" id="paid" name="paid" readonly value="" class="form-control" required>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 15px; margin-bottom: 15px;">
								
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4 col-md-6 col-sm-6">
								<label><?php echo $this->lang->line('To_be_paid');?></label>
								<input type="number" min="0" step="0.001" id="paid_value" name="paid_value" class="form-control" value="" required autofocus>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-6">
								<label><?php echo $this->lang->line('Debit_from_supplier_account');?></label>
								<select id="from_supplier_account" name="from_supplier_account" class="form-control" required>
									<option value="1"><?php echo $this->lang->line('Yes');?></option>
									<option value="0"><?php echo $this->lang->line('No');?></option>
								</select>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-6">
								<label><?php echo $this->lang->line('sale_bill_tree_id');?></label>
								<select id="tree_id" name="tree_id" class="form-control" required>
									<?php
									$acc_treasury = intval($this->session->userdata('acc_treasury'));
									$toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_treasury);
									foreach($toaccount as $toaccount_Row) {
										if ($this->session->userdata('lang') == "ar")
										{
											$ToTitle = $toaccount_Row->title;
										}
										else
										{
											$ToTitle = $toaccount_Row->title_en;
										}
									?>
									<option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
									<?php } ?>
									<?php
									$acc_bank = intval($this->session->userdata('acc_bank'));
									$toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_bank);
									foreach($toaccount as $toaccount_Row) {
										if ($this->session->userdata('lang') == "ar")
										{
											$ToTitle = $toaccount_Row->title;
										}
										else
										{
											$ToTitle = $toaccount_Row->title_en;
										}
									?>
									<option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<label class="col-form-label"><?php echo lang('buy_bill_image');?></label>
								<input type="file" name="image" class="form-control" dir="ltr" required>
							</div>
						</div>
						<div class="form-actions right noPrint" id="SaveButtons" style="text-align: right; font-weight: bold !important; font-size: 18px; margin-top: 10px;">
							<input type="hidden" name="id" id="id" value="">
							<button type="submit" class="btn btn-success" accesskey="s"><?php echo $this->lang->line('Pay');?></button>
						</div>
					</div>
				</div>
				<div class="modal-footer" id="FooterButtons">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $this->lang->line('Close');?></button>
					<button type="button" class="btn btn-info" onclick="printSelection(document.getElementById('buy_bill_pay'));return false;"><i class="fa fa-print"></i> <?php echo $this->lang->line('Print');?></button>
				</div>
				<?php
					echo form_close();
				?>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>