<br>
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
$Segment1 = $this->uri->segment(1);
$Segment2 = $this->uri->segment(2);
$FormPath = base_url().$Segment1."/".$Segment2."/"."hr_1_search";
echo form_open_multipart($FormPath);
?>
<div class="row">
	<div class="col-md-2">
		<label class="col-form-label"><?php echo lang('employee_country_id');?></label>
		<select name="country_id" class="form-control" data-size="5" data-live-search="true" required>
			<option <?php if($country_id == 0) {?>selected <?php }?> value="0">-</option>
			<?php
			$CountryData = $this->M_hr_country->GetMultiRow();
			foreach($CountryData as $CountryData_Row) {
				if ($this->session->userdata('lang') == "ar")
				{
					$Country = $CountryData_Row->ar_title;
				}
				else
				{
					$Country = $CountryData_Row->en_title;
				}
			?>
			<option <?php if($country_id == $CountryData_Row->id) {?>selected <?php }?> value="<?php echo $CountryData_Row->id;?>"><?php echo $Country;?></option>
			<?php
			}
			?>
		</select>
	</div>
	<div class="col-md-2">
		<label class="col-form-label"><?php echo lang('employee_education_id');?></label>
		<select name="education_id" class="form-control" data-size="5" data-live-search="true" required>
			<option <?php if($education_id == 0) {?>selected <?php }?> value="0">-</option>
			<?php
			$EducationData = $this->M_hr_education->GetMultiRow();
			foreach($EducationData as $EducationData_Row) {
				if ($this->session->userdata('lang') == "ar")
				{
					$Education = $EducationData_Row->ar_title;
				}
				else
				{
					$Education = $EducationData_Row->en_title;
				}
			?>
			<option <?php if($education_id == $EducationData_Row->id) {?>selected <?php }?> value="<?php echo $EducationData_Row->id;?>"><?php echo $Education;?></option>
			<?php
			}
			?>
		</select>
	</div>
	<div class="col-md-2">
		<label class="col-form-label"><?php echo lang('employee_job_id');?></label>
		<select name="job_id" class="form-control" data-size="5" data-live-search="true" required>
			<option <?php if($job_id == 0) {?>selected <?php }?> value="0">-</option>
			<?php
			$JobData = $this->M_hr_job->GetMultiRow();
			foreach($JobData as $JobData_Row) {
				if ($this->session->userdata('lang') == "ar")
				{
					$Job = $JobData_Row->ar_title;
				}
				else
				{
					$Job = $JobData_Row->en_title;
				}
			?>
			<option <?php if($job_id == $JobData_Row->id) {?>selected <?php }?> value="<?php echo $JobData_Row->id;?>"><?php echo $Job;?></option>
			<?php
			}
			?>
		</select>
	</div>
	<div class="col-md-2">
		<label class="col-form-label"><?php echo lang('employee_job_type_id');?></label>
		<select name="job_type_id" class="form-control" required>
			<option <?php if($job_type_id == 0) {?>selected <?php }?> value="0">-</option>
			<?php
			$JobTypeData = $this->M_hr_job_type->GetMultiRow();
			foreach($JobTypeData as $JobTypeData_Row) {
				if ($this->session->userdata('lang') == "ar")
				{
					$JobType = $JobTypeData_Row->ar_title;
				}
				else
				{
					$JobType = $JobTypeData_Row->en_title;
				}
			?>
			<option <?php if($job_type_id == $JobTypeData_Row->id) {?>selected <?php }?> value="<?php echo $JobTypeData_Row->id;?>"><?php echo $JobType;?></option>
			<?php
			}
			?>
		</select>
	</div>
	<div class="col-md-2">
		<label class="col-form-label"><?php echo lang('employee_workgroup_id');?></label>
		<select name="workgroup_id" class="form-control" required>
			<option <?php if($workgroup_id == 0) {?>selected <?php }?> value="0">-</option>
			<?php
			$WorkGroupData = $this->M_hr_workgroup->GetMultiRow();
			foreach($WorkGroupData as $WorkGroupData_Row) {
				if ($this->session->userdata('lang') == "ar")
				{
					$WorkGroup = $WorkGroupData_Row->ar_title;
				}
				else
				{
					$WorkGroup = $WorkGroupData_Row->en_title;
				}
			?>
			<option <?php if($workgroup_id == $WorkGroupData_Row->id) {?>selected <?php }?> value="<?php echo $WorkGroupData_Row->id;?>"><?php echo $WorkGroup;?></option>
			<?php
			}
			?>
		</select>
	</div>
	<div class="col-md-2">
		<button type="submit" class="btn btn-primary mr-2" style="margin-top:35px;"><?php echo $this->lang->line('Search');?></button>
	</div>
</div>
<?php
echo form_close();
?>
</div>

<div id="DivForPrint" <?php echo $this->session->userdata('HTML');?>>
	<br><br>
	<div style="margin-left: auto; margin-right: auto; width: 80%; text-align: center; font-weight: bold">
		<?php
		echo $title;
		?>
	</div>
	<br><br>
	<!--begin: Datatable -->
	<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
		<thead>
			<tr>
				<th>ID</th>
				<th><?php echo lang('employee_country_id');?></th>
				<th><?php echo lang('employee_education_id');?></th>
				<th><?php echo lang('employee_job_id');?></th>
				<th><?php echo lang('employee_job_type_id');?></th>
				<th><?php echo lang('employee_workgroup_id');?></th>
				<th><?php echo lang('hr_employee_id');?></th>
				<th><?php echo lang('employee_phone');?></th>
				<th><?php echo lang('employee_loan');?></th>
				<th><?php echo lang('employee_custody');?></th>
				<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
				<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
				<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach($DataRows as $DataRows_Row) {
				$employee_id = $DataRows_Row->id;
			?>
			<tr>
				<td><?php echo $DataRows_Row->id;?></td>
				<td>
					<?php
					$country_id = $DataRows_Row->country_id;
					$CountryData = $this->M_hr_country->GetRow($country_id);
					if ($this->session->userdata('lang') == "ar")
					{
						echo $CountryData->ar_title;
					}
					else
					{
						echo $CountryData->en_title;
					}
					?>
				</td>
				<td>
					<?php
					$education_id = $DataRows_Row->education_id;
					$EducationData = $this->M_hr_education->GetRow($education_id);
					if ($this->session->userdata('lang') == "ar")
					{
						echo $EducationData->ar_title;
					}
					else
					{
						echo $EducationData->en_title;
					}
					?>
				</td>
				<td>
					<?php
					$job_id = $DataRows_Row->job_id;
					$JobData = $this->M_hr_job->GetRow($job_id);
					if ($this->session->userdata('lang') == "ar")
					{
						echo $JobData->ar_title;
					}
					else
					{
						echo $JobData->en_title;
					}
					?>
				</td>
				<td>
					<?php
					$job_type_id = $DataRows_Row->job_type_id;
					$JobTypeData = $this->M_hr_job_type->GetRow($job_type_id);
					if ($this->session->userdata('lang') == "ar")
					{
						echo $JobTypeData->ar_title;
					}
					else
					{
						echo $JobTypeData->en_title;
					}
					?>
				</td>
				<td>
					<?php
					$workgroup_id = $DataRows_Row->workgroup_id;
					$WorkGroupData = $this->M_hr_workgroup->GetRow($workgroup_id);
					if ($this->session->userdata('lang') == "ar")
					{
						echo $WorkGroupData->ar_title;
					}
					else
					{
						echo $WorkGroupData->en_title;
					}
					?>
				</td>
				<td>
				<?php
				if ($this->session->userdata('lang') == "ar")
				{
					echo $DataRows_Row->ar_name;
				}
				else
				{
					echo $DataRows_Row->en_name;
				}
				?>
				</td>
				<td><?php echo $DataRows_Row->phone;?></td>
				<td>
				<?php
				$CheckAccount = $this->M_fin_treeaccount->CheckLoanFind($employee_id);
				if($CheckAccount > 0)
				{
					$Debit = 0;
					$Creditor = 0;
					
					$LoanAccount = $this->M_fin_treeaccount->GetLoanAccount($employee_id);
					$LoanAccountID = $LoanAccount->id;

					$MultiRows = $this->M_fin_journal->GetDetailsByAccount($LoanAccountID);
					foreach($MultiRows as $MultiRows_Row) {
						$Debit += doubleval($MultiRows_Row->debit);
						$Creditor += doubleval($MultiRows_Row->creditor);
					}
					$LoanValue = $Debit - $Creditor;
					$Link = base_url()."account/fin_treeaccount/view_account/".$LoanAccountID;
					echo "<a target='_blank' href='".$Link."'>".$LoanValue."</a>";
				}
				?>
				</td>
				<td>
				<?php
				$CheckAccount = $this->M_fin_treeaccount->CheckCustodyFind($employee_id);
				if($CheckAccount > 0)
				{
					$Debit = 0;
					$Creditor = 0;
					
					$CustodyAccount = $this->M_fin_treeaccount->GetCustodyAccount($employee_id);
					$CustodyAccountID = $CustodyAccount->id;

					$MultiRows = $this->M_fin_journal->GetDetailsByAccount($CustodyAccountID);
					foreach($MultiRows as $MultiRows_Row) {
						$Debit += doubleval($MultiRows_Row->debit);
						$Creditor += doubleval($MultiRows_Row->creditor);
					}
					$CustodyValue = $Debit - $Creditor;
					$Link = base_url()."account/fin_treeaccount/view_account/".$CustodyAccountID;
					echo "<a target='_blank' href='".$Link."'>".$CustodyValue."</a>";
				}
				?>
				</td>
				<td style="text-align:center">
					<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
						<i class="flaticon-edit-1 text-primary icon-lg"></i> 
					</a>
				</td>
				<td style="text-align:center">
					<?php
						if($DataRows_Row->deleted == 0) {
					?>
					<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
						<i class="flaticon-delete text-danger icon-lg"></i> 
					</a>
					<?php } ?>
				</td>
				<td style="text-align:center">
					<?php
						if($DataRows_Row->deleted == 1) {
					?>
						<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
							<i class="flaticon-refresh text-success icon-lg"></i> 
						</a>
					<?php } ?>
				</td>
			</tr>
			<?php
			}
			?>
		</tbody>
		
	</table>

</div>


<div class="d-flex justify-content-between">
	<button type="button" class="btn btn-primary font-weight-bold" onclick="printDiv();"><?php echo $this->lang->line('Print');?></button>
</div>	