<br>
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
$Segment1 = $this->uri->segment(1);
$Segment2 = $this->uri->segment(2);
$FormPath = base_url().$Segment1."/".$Segment2."/"."hr_3_search";
echo form_open_multipart($FormPath);
?>
<div class="row">
	<div class="col-md-5">
		<label class="col-form-label"><?php echo lang('from_date');?></label>
		<input type="date" name="fromdate" class="form-control" value="<?php echo $fromdate;?>" required>
	</div>
	<div class="col-md-5">
		<label class="col-form-label"><?php echo lang('to_date');?></label>
		<input type="date" name="todate" class="form-control" value="<?php echo $todate;?>" required>
	</div>
	<div class="col-md-2">
		<button type="submit" class="btn btn-primary mr-2" style="margin-top:35px;"><?php echo $this->lang->line('Search');?></button>
	</div>
</div>
<?php
echo form_close();
?>
</div>

<div id="DivForPrint" <?php echo $this->session->userdata('HTML');?>>
	<br><br>
	<div style="margin-left: auto; margin-right: auto; width: 80%; text-align: center; font-weight: bold">
		<?php
		echo $title;
		?>
	</div>
	<br><br>
	<!--begin: Datatable -->
	<?php
	if($fromdate <> $todate) {
	?>
	<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
		<thead>
			<tr>
				<th>ID</th>
				<th><?php echo lang('hr_employee_id');?></th>
				<th><?php echo lang('hr_employee_workdays');?></th>
				<th><?php echo lang('hr_employee_day_salary');?></th>
				<th><?php echo lang('hr_employee_workdays_salary');?></th>
				<th><?php echo lang('hr_employee_subsalary_action1');?></th>
				<th><?php echo lang('hr_employee_subsalary_action2');?></th>
				<th><?php echo lang('hr_employee_subsalary_action3');?></th>
				<th><?php echo lang('employee_basic_salary');?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$DailySalary = 0;
			$basic_salary = 0;
			$salary_days = 0;
			foreach($DataRows as $DataRows_Row) {
				$employee_id = $DataRows_Row->id;
				$basic_salary = $DataRows_Row->basic_salary;
				$salary_days = $DataRows_Row->salary_days;
				$DailySalary = $basic_salary / $salary_days;
			?>
			<tr>
				<td><?php echo $DataRows_Row->id;?></td>
				<td>
				<?php
				if ($this->session->userdata('lang') == "ar")
				{
					echo $DataRows_Row->ar_name;
				}
				else
				{
					echo $DataRows_Row->en_name;
				}
				?>
				</td>
				<td>
					<?php
					$WorkDays = intval($this->M_hr_inouttimedata->CheckEmployee($employee_id, $fromdate, $todate));
					echo $WorkDays;
					?>
				</td>
				<td>
					<?php
					echo number_format($DailySalary, 2, '.', '');
					?>
				</td>
				<td>
					<?php
					$WorkSalary = $DailySalary * $WorkDays;
					echo number_format($WorkSalary, 2, '.', '');
					?>
				</td>
				<td>
					<?php
					$TotalAction1 = $this->M_hr_employee_subsalary->GetSumAction1($employee_id, $fromdate, $todate);
					echo number_format($TotalAction1->thevalue, 2, '.', '');
					?>
				</td>
				<td>
					<?php
					$TotalAction2 = $this->M_hr_employee_subsalary->GetSumAction2($employee_id, $fromdate, $todate);
					echo number_format($TotalAction2->thevalue, 2, '.', '');
					?>
				</td>
				<td>
					<?php
					$TotalAction3 = $this->M_hr_employee_subsalary->GetSumAction3($employee_id, $fromdate, $todate);
					echo number_format($TotalAction3->thevalue, 2, '.', '');
					?>
				</td>
				<td>
					<?php
					$FinalSalary = $WorkSalary + ($TotalAction1->thevalue) - ($TotalAction2->thevalue) - ($TotalAction3->thevalue);
					echo number_format($FinalSalary, 2, '.', '');
					?>
				</td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>
	<?php } ?>

</div>


<div class="d-flex justify-content-between">
	<button type="button" class="btn btn-primary font-weight-bold" onclick="printDiv();"><?php echo $this->lang->line('Print');?></button>
</div>									