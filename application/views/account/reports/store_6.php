<div id="DivForPrint" <?php echo $this->session->userdata('HTML');?>>
	<br><br>
	<div style="margin-left: auto; margin-right: auto; width: 80%; text-align: center; font-weight: bold">
		<?php
		echo $title;
		?>
	</div>
	<br><br>
	<!--begin: Datatable -->
	<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
		<thead>
			<tr>
				<th><?php echo lang('product_barcode');?></th>
				<th><?php echo lang('product_title');?></th>
				<th style="text-align:center"><?php echo lang('product_Price');?></th>
				<th style="text-align:center"><?php echo lang('product_lowest_price');?></th>
				<th style="text-align:center"><?php echo lang('product_cost_price');?></th>
				<?php
				$StoreList = $this->M_store->GetMultiRow();
				foreach($StoreList as $StoreList_Row) {
					$StoreName = "";
					if ($this->session->userdata('lang') == "ar")
					{
						$StoreName = $StoreList_Row->title;
					}
					else
					{
						$StoreName = $StoreList_Row->title_en;
					}
				?>
				<th style="text-align:center"><?php echo $StoreName;?></th>
				<?php
				}
				?>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach($DataRows as $DataRows_Row) {
				$product_id = $DataRows_Row->id;
			?>
			<tr>
				<td><?php echo $DataRows_Row->barcode;?></td>
				<td>
				<?php
				echo $DataRows_Row->title;
				if ($this->session->userdata('lang') == "ar")
				{
					echo $DataRows_Row->title;
				}
				else
				{
					echo $DataRows_Row->title_en;
				}
				?>
				</td>
				<td style="text-align:center"><?php echo $DataRows_Row->price;?></td>
				<td style="text-align:center"><?php echo $DataRows_Row->lowest_price;?></td>
				<td style="text-align:center"><?php echo $DataRows_Row->cost_price;?></td>
				<?php
				$StoreList = $this->M_store->GetMultiRow();
				foreach($StoreList as $StoreList_Row) {
					$store_id = $StoreList_Row->id;
					$Qunatity = 0;
					$StoreQunatityCheck = $this->M_store_quantity->CountStoreProduct($store_id, $product_id);
					if($StoreQunatityCheck > 0)
					{
						$StoreQunatity = $this->M_store_quantity->GetStoreProduct($store_id, $product_id);
						$Qunatity = $StoreQunatity->quantity;
					}
					else
					{
						$Qunatity = 0;
					}
				?>
				<td style="text-align:center"><?php echo "(".$Qunatity.")";?></td>
				<?php
				}
				?>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>

</div>


<div class="d-flex justify-content-between">
	<button type="button" class="btn btn-primary font-weight-bold" onclick="printDiv();"><?php echo $this->lang->line('Print');?></button>
</div>	