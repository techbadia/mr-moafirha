<br>
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
$Segment1 = $this->uri->segment(1);
$Segment2 = $this->uri->segment(2);
$FormPath = base_url().$Segment1."/".$Segment2."/"."hr_4_search";
echo form_open_multipart($FormPath);
?>
<div class="row">
	<div class="col-md-4">
		<label class="col-form-label"><?php echo lang('hr_vacation_employee_id');?></label>
		<select name="employee_id" class="form-control" required>
			<option <?php if($employee_id == 0) {?>selected <?php }?> value="0">-</option>
			<?php
			$EmployeeData = $this->M_hr_employee->GetMultiRow();
			foreach($EmployeeData as $EmployeeData_Row) {
				if ($this->session->userdata('lang') == "ar")
				{
					$Employee = $EmployeeData_Row->ar_name;
				}
				else
				{
					$Employee = $EmployeeData_Row->en_name;
				}
			?>
			<option <?php if($employee_id == $EmployeeData_Row->id) {?>selected <?php }?> value="<?php echo $EmployeeData_Row->id;?>"><?php echo $Employee;?></option>
			<?php
			}
			?>
		</select>
	</div>
	<div class="col-md-3">
		<label class="col-form-label"><?php echo lang('from_date');?></label>
		<input type="date" name="fromdate" class="form-control" value="<?php echo $fromdate;?>" required>
	</div>
	<div class="col-md-3">
		<label class="col-form-label"><?php echo lang('to_date');?></label>
		<input type="date" name="todate" class="form-control" value="<?php echo $todate;?>" required>
	</div>
	<div class="col-md-2">
		<button type="submit" class="btn btn-primary mr-2" style="margin-top:35px;"><?php echo $this->lang->line('Search');?></button>
	</div>
</div>
<?php
echo form_close();
?>
</div>

<div id="DivForPrint" <?php echo $this->session->userdata('HTML');?>>
	<br><br>
	<div style="margin-left: auto; margin-right: auto; width: 80%; text-align: center; font-weight: bold">
		<?php
		echo $title;
		?>
	</div>
	<br><br>
	<!--begin: Datatable -->
	<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
		<thead>
			<tr>
				<th>ID</th>
				<th><?php echo lang('hr_vacation_employee_id');?></th>
				<th><?php echo lang('hr_vacation_type_id');?></th>
				<th><?php echo lang('hr_vacation_from_date');?></th>
				<th><?php echo lang('hr_vacation_to_date');?></th>
				<th><?php echo lang('hr_vacation_accept');?></th>
				<th><?php echo lang('active');?></th>
				<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
				<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
				<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach($DataRows as $DataRows_Row) {
				$hr_vacation_id = $DataRows_Row->id;
			?>
			<tr>
				<td><?php echo $DataRows_Row->id;?></td>
				<td>
					<?php
					$employee_id = $DataRows_Row->employee_id;
					$EmployeeData = $this->M_hr_employee->GetRow($employee_id);
					echo $EmployeeData->ar_name;
					?>
				</td>
				<td>
					<?php
					$type_id = $DataRows_Row->type_id;
					$VacationData = $this->M_hr_vacation_types->GetRow($type_id);
					echo $VacationData->ar_title;
					?>
				</td>
				<td><?php echo $DataRows_Row->from_date;?></td>
				<td><?php echo $DataRows_Row->to_date;?></td>
				<td>
					<?php
					$accept = $DataRows_Row->accept;
					if($accept == 1)
					{
						echo $this->lang->line('Yes');
					}
					else
					{
						echo $this->lang->line('No');
					}
					?>
				</td>
				<td>
				<?php
				if($DataRows_Row->deleted == 0)
				{
					echo lang('Yes');
				}
				else
				{
					echo lang('No');
				}
				?>
				</td>
				<td style="text-align:center">
					<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
						<i class="flaticon-edit-1 text-primary icon-lg"></i> 
					</a>
				</td>
				<td style="text-align:center">
					<?php
						if($DataRows_Row->deleted == 0) {
					?>
					<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
						<i class="flaticon-delete text-danger icon-lg"></i> 
					</a>
					<?php } ?>
				</td>
				<td style="text-align:center">
					<?php
						if($DataRows_Row->deleted == 1) {
					?>
						<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
							<i class="flaticon-refresh text-success icon-lg"></i> 
						</a>
					<?php } ?>
				</td>
			</tr>
			<?php
			}
			?>
		</tbody>
		
	</table>

</div>


<div class="d-flex justify-content-between">
	<button type="button" class="btn btn-primary font-weight-bold" onclick="printDiv();"><?php echo $this->lang->line('Print');?></button>
</div>									