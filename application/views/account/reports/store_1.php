<br>
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
$Segment1 = $this->uri->segment(1);
$Segment2 = $this->uri->segment(2);
$FormPath = base_url().$Segment1."/".$Segment2."/"."store_1_search";
echo form_open_multipart($FormPath);
?>
<div class="row">
	<div class="col-md-5">
		<label class="col-form-label"><?php echo lang('from_date');?></label>
		<input type="date" name="fromdate" class="form-control" value="<?php echo $fromdate;?>" required>
	</div>
	<div class="col-md-5">
		<label class="col-form-label"><?php echo lang('to_date');?></label>
		<input type="date" name="todate" class="form-control" value="<?php echo $todate;?>" required>
	</div>
	<div class="col-md-2">
		<button type="submit" class="btn btn-primary mr-2" style="margin-top:35px;"><?php echo $this->lang->line('Search');?></button>
	</div>
</div>
<?php
echo form_close();
?>
</div>

<div id="DivForPrint" <?php echo $this->session->userdata('HTML');?>>
	<br><br>
	<div style="margin-left: auto; margin-right: auto; width: 80%; text-align: center; font-weight: bold">
		<?php echo $title;?>
		<br>
		<?php echo $fromdate." : ".$todate;?>
	</div>
	<br><br>
	<!--begin: Datatable -->
	<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
		<thead>
			<tr>
				<th>ID</th>
				<th><?php echo lang('product_barcode');?></th>
				<th><?php echo lang('product_thetitle');?></th>
				<th style="text-align:center"><?php echo lang('Purchases_quantity');?></th>
				<th style="text-align:center"><?php echo lang('Quantity_purchase_returns');?></th>
				<th style="text-align:center"><?php echo lang('sales_quantity');?></th>
				<th style="text-align:center"><?php echo lang('Quantity_sales_returns');?></th>
				<th style="text-align:center"><?php echo lang('Current_Balance');?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach($DataRows as $DataRows_Row) {
				$product_id = $DataRows_Row->id;
			?>
			<tr>
				<td><?php echo $DataRows_Row->id;?></td>
				<td><?php echo $DataRows_Row->barcode;?></td>
				<td>
				<?php
				if ($this->session->userdata('lang') == "ar")
				{
					echo $DataRows_Row->title;
				}
				else
				{
					echo $DataRows_Row->title_en;
				}
				?>
				</td>
				<td style="text-align:center">
				<?php
				$TotalBuy = $this->M_buy_bill_items->GetTotalQuantity($product_id, $fromdate, $todate);
				echo doubleval($TotalBuy->quantity);
				?>
				</td>
				<td style="text-align:center">
				<?php
				$TotalBuyReturn = $this->M_buy_returns_items->GetTotalQuantity($product_id, $fromdate, $todate);
				echo doubleval($TotalBuyReturn->quantity);
				?>
				</td>
				<td style="text-align:center">
				<?php
				$TotalSale = $this->M_sale_bill_items->GetTotalQuantity($product_id, $fromdate, $todate);
				echo doubleval($TotalSale->quantity);
				?>
				</td>
				<td style="text-align:center">
				<?php
				$TotalSaleReturn = $this->M_sale_returns_items->GetTotalQuantity($product_id, $fromdate, $todate);
				echo doubleval($TotalSaleReturn->quantity);
				?>
				</td>
				<td style="text-align:center">
				<?php
				$TotalBuy_quantity = doubleval($TotalBuy->quantity);
				$TotalReturn_quantity = doubleval($TotalBuyReturn->quantity);
				$TotalSale_quantity = doubleval($TotalSale->quantity);
				$TotalSaleReturn_quantity = doubleval($TotalSaleReturn->quantity);
				$Net = ($TotalBuy_quantity) - ($TotalReturn_quantity) - ($TotalSale_quantity) + ($TotalSaleReturn_quantity);
				echo doubleval($Net);
				?>
				</td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>

</div>


<div class="d-flex justify-content-between">
	<button type="button" class="btn btn-primary font-weight-bold" onclick="printDiv();"><?php echo $this->lang->line('Print');?></button>
</div>	