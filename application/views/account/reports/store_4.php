<div id="DivForPrint" <?php echo $this->session->userdata('HTML');?>>
	<br><br>
	<div style="margin-left: auto; margin-right: auto; width: 80%; text-align: center; font-weight: bold">
		<?php echo $title;?>
	</div>
	<br><br>
	<!--begin: Datatable -->
	<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
		<thead>
			<tr>
				<th>ID</th>
				<th><?php echo lang('product_barcode');?></th>
				<th><?php echo lang('product_category_id');?></th>
				<th><?php echo lang('product_brand_id');?></th>
				<th><?php echo lang('product_title');?></th>
				<th><?php echo lang('product_title_en');?></th>
				<th><?php echo lang('product_Price');?></th>
				<th><?php echo lang('product_lowest_price');?></th>
				<th><?php echo lang('store_quantity_quantity');?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach($DataRows as $DataRows_Row) {
				$demand = intval($DataRows_Row->demand);
				$product_id = intval($DataRows_Row->id);
				$TotalQuantity = $this->M_store_quantity->GetTotalQuantity($product_id);
				if($TotalQuantity->quantity <= $demand) {
			?>
			<tr>
				<td><?php echo $DataRows_Row->id;?></td>
				<td><?php echo $DataRows_Row->barcode;?></td>
				<td>
					<?php
					$category_id = $DataRows_Row->category_id;
					$CategoryData = $this->M_store_category->GetRow($category_id);
					if ($this->session->userdata('lang') == "ar")
					{
						echo $CategoryData->title;
					}
					else
					{
						echo $CategoryData->title_en;
					}
					?>
				</td>
				<td>
					<?php
					$brand_id = $DataRows_Row->brand_id;
					$BrandData = $this->M_store_brand->GetRow($brand_id);
					if ($this->session->userdata('lang') == "ar")
					{
						echo $BrandData->title;
					}
					else
					{
						echo $BrandData->title_en;
					}
					?>
				</td>
				<td><?php echo $DataRows_Row->title;?></td>
				<td><?php echo $DataRows_Row->title_en;?></td>
				<td><?php echo $DataRows_Row->price;?></td>
				<td><?php echo $DataRows_Row->lowest_price;?></td>
				<td><?php echo $TotalQuantity->quantity; ?></td>
			</tr>
			<?php
				}
			}
			?>
		</tbody>
	</table>

</div>


<div class="d-flex justify-content-between">
	<button type="button" class="btn btn-primary font-weight-bold" onclick="printDiv();"><?php echo $this->lang->line('Print');?></button>
</div>	