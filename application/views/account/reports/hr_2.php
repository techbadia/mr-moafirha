<br>
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
$Segment1 = $this->uri->segment(1);
$Segment2 = $this->uri->segment(2);
$FormPath = base_url().$Segment1."/".$Segment2."/"."hr_2_search";
echo form_open_multipart($FormPath);
?>
<div class="row">
	<div class="col-md-4">
		<label class="col-form-label"><?php echo lang('hr_vacation_employee_id');?></label>
		<select name="employee_id" class="form-control" required>
			<option <?php if($employee_id == 0) {?>selected <?php }?> value="0">-</option>
			<?php
			$EmployeeData = $this->M_hr_employee->GetMultiRow();
			foreach($EmployeeData as $EmployeeData_Row) {
				if ($this->session->userdata('lang') == "ar")
				{
					$Employee = $EmployeeData_Row->ar_name;
				}
				else
				{
					$Employee = $EmployeeData_Row->en_name;
				}
			?>
			<option <?php if($employee_id == $EmployeeData_Row->id) {?>selected <?php }?> value="<?php echo $EmployeeData_Row->id;?>"><?php echo $Employee;?></option>
			<?php
			}
			?>
		</select>
	</div>
	<div class="col-md-3">
		<label class="col-form-label"><?php echo lang('from_date');?></label>
		<input type="date" name="fromdate" class="form-control" value="<?php echo $fromdate;?>" required>
	</div>
	<div class="col-md-3">
		<label class="col-form-label"><?php echo lang('to_date');?></label>
		<input type="date" name="todate" class="form-control" value="<?php echo $todate;?>" required>
	</div>
	<div class="col-md-2">
		<button type="submit" class="btn btn-primary mr-2" style="margin-top:35px;"><?php echo $this->lang->line('Search');?></button>
	</div>
</div>
<?php
echo form_close();
?>
</div>

<div id="DivForPrint" <?php echo $this->session->userdata('HTML');?>>
	<br><br>
	<div style="margin-left: auto; margin-right: auto; width: 80%; text-align: center; font-weight: bold">
		<?php
		echo $title;
		?>
	</div>
	<br><br>
	<!--begin: Datatable -->
	<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
		<thead>
			<tr>
				<th>ID</th>
				<th><?php echo lang('hr_employee_id');?></th>
				<th><?php echo lang('hr_in_date');?></th>
				<th><?php echo lang('hr_in_time');?></th>
				<th><?php echo lang('hr_out_date');?></th>
				<th><?php echo lang('hr_out_time');?></th>
				<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
				<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
				<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach($DataRows as $DataRows_Row) {
				$store_id = $DataRows_Row->id;
			?>
			<tr>
				<td><?php echo $DataRows_Row->id;?></td>
				<td>
				<?php
				$user_id = $DataRows_Row->employee_id;
				$UserData = $this->M_usr_users->GetRow($user_id);
				echo $UserData->fullname;
				?>
				</td>
				<td><?php echo $DataRows_Row->in_date;?></td>
				<td><?php echo $DataRows_Row->in_time;?></td>
				<td><?php echo $DataRows_Row->out_date;?></td>
				<td><?php echo $DataRows_Row->out_time;?></td>
				<td style="text-align:center">
					<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
						<i class="flaticon-edit-1 text-primary icon-lg"></i> 
					</a>
				</td>
				<td style="text-align:center">
					<?php
						if($DataRows_Row->deleted == 0) {
					?>
					<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
						<i class="flaticon-delete text-danger icon-lg"></i> 
					</a>
					<?php } ?>
				</td>
				<td style="text-align:center">
					<?php
						if($DataRows_Row->deleted == 1) {
					?>
						<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
							<i class="flaticon-refresh text-success icon-lg"></i> 
						</a>
					<?php } ?>
				</td>
			</tr>
			<?php
			}
			?>
		</tbody>
		
	</table>

</div>


<div class="d-flex justify-content-between">
	<button type="button" class="btn btn-primary font-weight-bold" onclick="printDiv();"><?php echo $this->lang->line('Print');?></button>
</div>									