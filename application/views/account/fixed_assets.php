<!--begin: Datatable -->
<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
	<thead>
		<tr>
			<th>ID</th>
			<th><?php echo lang('fin_treeaccount_account_no');?></th>
			<th><?php echo lang('fin_treeaccount_title');?></th>
			<th><?php echo lang('fin_treeaccount_title_en');?></th>
			<th><?php echo lang('fin_treeaccount_startamount');?></th>
			<th><?php echo lang('fin_treeaccount_depreciation');?></th>
			<th><?php echo lang('fin_treeaccount_thedate');?></th>
			<th><?php echo lang('fin_treeaccount_image');?></th>
			<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
			<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
			<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
			<th style="text-align:center"><?php echo $this->lang->line('View');?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach($DataRows as $DataRows_Row) {
			$fin_treeaccount_id = $DataRows_Row->id;
		?>
		<tr>
			<td><?php echo $DataRows_Row->id;?></td>
			<td><?php echo $DataRows_Row->account_no;?></td>
			<td><?php echo $DataRows_Row->title;?></td>
			<td><?php echo $DataRows_Row->title_en;?></td>
			<td><?php echo $DataRows_Row->startamount;?></td>
			<td><?php echo $DataRows_Row->depreciation;?></td>
			<td><?php echo $DataRows_Row->thedate;?></td>
			<td>
			<?php
			if($DataRows_Row->image != "") {
			echo "<a target='_blank' href='".base_url()."upload/fixed_assets/".$DataRows_Row->image."'>".$DataRows_Row->id."</a>";
			}
			?>
			</td>
			<td style="text-align:center">
				<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
					<i class="flaticon-edit-1 text-primary icon-lg"></i> 
				</a>
			</td>
			<td style="text-align:center">
				<?php
					if($DataRows_Row->deleted == 0) {
				?>
				<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
					<i class="flaticon-delete text-danger icon-lg"></i> 
				</a>
				<?php } ?>
			</td>
			<td style="text-align:center">
				<?php
					if($DataRows_Row->deleted == 1) {
				?>
					<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
						<i class="flaticon-refresh text-success icon-lg"></i> 
					</a>
				<?php } ?>
			</td>
			<td style="text-align:center">
				<a href="<?php echo base_url()."/account/fin_treeaccount/view_account/".$DataRows_Row->id;?>" target="_lank">
					<i class="flaticon-search text-success icon-lg"></i> 
				</a>
			</td>
		</tr>
		<?php
			$SubLevel = $this->M_fin_treeaccount->GetSubAccounts($fin_treeaccount_id);
			foreach($SubLevel as $SubLevel_Row) {
			?>
			<tr>
				<td><?php echo $SubLevel_Row->id;?></td>
				<td><?php echo $SubLevel_Row->account_no;?></td>
				<td><?php echo $SubLevel_Row->title;?></td>
				<td><?php echo $SubLevel_Row->title_en;?></td>
				<td><?php echo $SubLevel_Row->startamount;?></td>
				<td><?php echo $DataRows_Row->depreciation;?></td>
				<td><?php echo $DataRows_Row->thedate;?></td>
				<td>
				<?php
				if($DataRows_Row->image != "") {
				echo "<a target='_blank' href='".base_url()."upload/fixed_assets/".$DataRows_Row->image."'>".$DataRows_Row->id."</a>";
				}
				?>
				</td>
				<td style="text-align:center">
					<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$SubLevel_Row->id;?>">
						<i class="flaticon-edit-1 text-primary icon-lg"></i> 
					</a>
				</td>
				<td style="text-align:center">
					<?php
						if($SubLevel_Row->deleted == 0) {
					?>
					<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$SubLevel_Row->id;?>">
						<i class="flaticon-delete text-danger icon-lg"></i> 
					</a>
					<?php } ?>
				</td>
				<td style="text-align:center">
					<?php
						if($SubLevel_Row->deleted == 1) {
					?>
						<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$SubLevel_Row->id;?>">
							<i class="flaticon-refresh text-success icon-lg"></i> 
						</a>
					<?php } ?>
				</td>
				<td style="text-align:center">
					<a href="<?php echo base_url()."/account/fin_treeaccount/view_account/".$SubLevel_Row->id;?>" target="_lank">
						<i class="flaticon-search text-success icon-lg"></i> 
					</a>
				</td>
			</tr>
			<?php
			}
		}
		?>
	</tbody>
	
</table>