<br>
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
$Segment1 = $this->uri->segment(1);
$Segment2 = $this->uri->segment(2);
$FormPath = base_url().$Segment1."/".$Segment2."/search_account";
echo form_open_multipart($FormPath);
?>
<div class="row">
	<div class="col-md-5">
		<label class="col-form-label"><?php echo lang('from_date');?></label>
		<input type="date" name="fromdate" class="form-control" value="<?php echo $fromdate;?>" required>
	</div>
	<div class="col-md-5">
		<label class="col-form-label"><?php echo lang('to_date');?></label>
		<input type="date" name="todate" class="form-control" value="<?php echo $todate;?>" required>
	</div>
	<div class="col-md-2">
		<input type="hidden" id="accountid" name="accountid" value="<?php echo $accountid;?>">
		<input type="hidden" id="Search" name="Search" value="<?php echo $Search;?>" />
		<button type="submit" class="btn btn-primary mr-2" style="margin-top:35px;"><?php echo $this->lang->line('Search');?></button>
	</div>
</div>
<?php
echo form_close();
?>
</div>

<div id="DivForPrint" <?php echo $this->session->userdata('HTML');?>>
	<br><br>
	<div style="margin-left: auto; margin-right: auto; width: 80%; text-align: center; font-weight: bold">
		<?php echo lang('Account_statement')." ".$title;?>
		<br>
		<?php echo $fromdate." : ".$todate;?>
	</div>
	<br><br>

	<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1" style="width:98%">
		<thead>
			<tr>
				<th style="text-align:center"><?php echo $this->lang->line('Serial');?></th>
				<th style="text-align:center"><?php echo $this->lang->line('fin_journal_debit');?></th>
				<th style="text-align:center"><?php echo $this->lang->line('fin_journal_creditor');?></th>
				<th style="text-align:center"><?php echo $this->lang->line('Balance');?></th>
				<th><?php echo $this->lang->line('fin_journal_account_id');?></th>
				<th><?php echo $this->lang->line('fin_journal_details');?></th>
				<th><?php echo $this->lang->line('fin_journal_thedate');?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td></td>
				<td style="text-align:center">
					<?php
					if (($category_id ==1) || ($category_id ==5))
					{
						echo $startamount;
					}
					?>
				</td>
				<td style="text-align:center">
					<?php
					if (($category_id ==2) || ($category_id ==3) || ($category_id ==4))
					{
						echo $startamount;
					}
					?>
				</td>
				<td style="text-align:center"><strong>
					<?php
					$StartAmount = 0;
					$BeforDay = date('Y-m-d', strtotime('-1 days', strtotime($fromdate)));
					$AmountBeforeJournal = $StartAmount;
					$AmountBeforeJournalData = $this->M_fin_journal->GetRealtedJournalStartDate($accountid, $fromdate);
					$AmountBeforeJournalValue = 0;
					$RealtedJournalStartDateCount = $this->M_fin_journal->GetRealtedJournalStartDateCount($accountid, $fromdate);
					$AccountCategoryID = 0;
					if ($RealtedJournalStartDateCount >0)
					{
						foreach($AmountBeforeJournalData as $AmountBeforeJournalData_Row) {
							//$AmountBeforeJournalValue = doubleval($AmountBeforeJournalData_Row->thevalue);
							$AccountData = $this->M_fin_treeaccount->GetRow($accountid);
							$AccountCategoryID = $AccountData->category_id;
							if (($AccountCategoryID ==1) || ($AccountCategoryID ==4))
							{
								
							}
							else
								{
									$AmountBeforeJournal -= doubleval($AmountBeforeJournalData_Row->debit);
									$AmountBeforeJournal += doubleval($AmountBeforeJournalData_Row->creditor);
								}
						}
					}
					echo $AmountBeforeJournal;
					$Balance = $AmountBeforeJournal;
					?></strong>
				</td>
				<td></td>
				<td><?php echo "الرصيد السابق";?></td>
				<td></td>
			</tr>
			<?php
			$TotalValue = 0;
			$Serial = 0;
			$Debit = 0;
			$DebitTotal = 0;
			$CreditorTotal = 0;
			$Creditor = 0;
			$CategoryID = 0;
			$TheValue = 0;
			$OtherAccountName = "";
			$Balance = $startamount;
			//$AccountCategoryID = 0;
			$MultiRows = $this->M_fin_journal->GetDetailsByAccount($accountid);
			foreach($MultiRows as $MultiRows_Row) {
				$Serial += 1;
				$AccountName = $this->M_fin_treeaccount->GetRow($accountid);
				$category_id = $AccountName->category_id;

				$main_id = $MultiRows_Row->main_id;
				$JournalMainData = $this->M_fin_journal_main->GetRow($main_id);
				$details = $JournalMainData->details;
				$thedate = $JournalMainData->thedate;
			?>
			<tr>
			<td style="text-align:center"><?php echo $Serial;?></td>
			<td style="text-align:center">
				<?php
					echo doubleval($MultiRows_Row->debit);
					$Debit += doubleval($MultiRows_Row->debit);
					$DebitTotal += $Debit;
				?>
			</td>
			<td style="text-align:center">
				<?php
					echo doubleval($MultiRows_Row->creditor);
					$Creditor += doubleval($MultiRows_Row->creditor);
					$CreditorTotal += $Creditor;
				?>
			</td>
			<td style="text-align:center">
			</td>
			<td>
				<?php
				
				echo $AccountName->title;
				?>
			</td>
			<td><?php echo $details;?></td>
			<td><?php echo $thedate;?></td>
			</tr>
			<?php
			$CategoryID = 0;
			$TheValue = 0;
			if(($category_id == 1) || ($category_id == 3))
			{

				$Balance = $Balance + $MultiRows_Row->debit - $MultiRows_Row->creditor;
			}
			else
			{
				$Balance = $Balance + $MultiRows_Row->creditor - $MultiRows_Row->debit;
			}
			//echo $this->Converter->ConvertToArabic($Balance);
			}
			?>
		</tbody>
		<tfoot>
			<tr>
				<th></th>
				<th style="text-align:center">
					<?php
					$Final = 0;
					if (($AccountCategoryID ==1) || ($AccountCategoryID ==4))
					{
						$Final = $startamount + $Debit - $Creditor;
						//echo $Debit;
					}
					echo doubleval($Debit);
					?>
				</th>
				<th style="text-align:center">
					<?php
					$Final = 0;
					if (($AccountCategoryID ==2) || ($AccountCategoryID ==3) || ($AccountCategoryID ==5))
					{
						$Final = $StartAmount - $Debit + $Creditor;
						//echo $Creditor;
					}
					echo doubleval($Creditor);
					?>
				</th>
				<th style="text-align:center">
					<?php
					echo doubleval($Balance);
					?>
				</th>
				<th></th>
				<th><?php echo "الرصيد الختامي";?></th>
				<th></th>
			</tr>
		</tfoot>
	</table>
</div>


<div class="d-flex justify-content-between">
	<button type="button" class="btn btn-primary font-weight-bold" onclick="printDiv();"><?php echo $this->lang->line('Print');?></button>
</div>