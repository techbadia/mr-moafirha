<?php

$this->load->model('settings/M_fin_settings');
$this->load->model('M_fin_treeaccount');
$this->load->model('app/M_app_company');
//$Assets = $this->M_fin_treeaccount->GetByCategory(1);
//$Liabilities = $this->M_fin_treeaccount->GetByCategory(2);
//$StockholdersEquity = $this->M_fin_treeaccount->GetByCategory(3);

$company_id = intval($this->session->userdata('company_id'));
$DataRow = $this->M_app_company->GetRow($company_id);
$this->session->set_userdata('header', $DataRow->header);
$this->session->set_userdata('footer', $DataRow->footer);

$HeaderImage = base_url() . "upload/settings/" . $this->session->userdata('header');
$FooterImage = base_url() . "upload/settings/" . $this->session->userdata('footer');
$Directionality = $this->session->userdata('Direction');
$PageTitle = $this->lang->line('Trial_Balance');


require_once 'vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf([
    'mode' => 'utf-8',
    //'format' => 'A4'.($orientation == 'L' ? '-L' : ''),
    'orientation' => 'L',
    'margin_left' => 5,
    'margin_right' => 5,
    'margin_top' => 15,
    'margin_bottom' => 15,
    'margin_header' => 5,
    'margin_footer' => 5,
]);
$mpdf->allow_charset_conversion = true;
//$mpdf->charset_in='cp1252';
$mpdf->SetTitle($PageTitle);
$mpdf->SetDirectionality($Directionality);
$mpdf->autoScriptToLang = true;
$mpdf->autoLangToFont = true;
$mpdf->debug = true;
//$mpdf->defaultPageNumStyle = 'arabic-indic';
 $fromDT=$this->input->post("fromDT");
   $toDT=$this->input->post("toDT"); 
ob_end_clean();
header("Content-Encoding: None", true);

$mpdf->SetHTMLHeader('
		<div style="text-align: center; font-weight: bold;">
			<img src="' . $HeaderImage . '"style="height: 10%;width:100%;">
		</div>');

$mpdf->SetHTMLFooter('<img src="' . $FooterImage . '"style="height: 3cm;width:100%;">
		<table width="100%" style="vertical-align: bottom; font-family: serif; 
			font-size: 8pt; color: #000000; font-weight: bold;">
			<tr>
				<td colspan="2"><img sssrc="' . $FooterImage . '"style="height: 10%;width:100%;"></td>
			</tr>
			<tr>
				<td width="50%">{DATE j-m-Y}</td>
				<td width="50%" align="left" lang="en">{PAGENO}/{nbpg}</td>
			</tr>
		</table>');
$i = 1;
$html = '';
$html .= '<div align="center" style="font-size:18px; margin-top: 30px;"><strong>' . $PageTitle . '</strong><br>' ." ". $fromDT   ." - ". $toDT . '</div><br>';
$html .= '<div style="float:right; width:100%">';

if ($this->session->userdata('lang') == "ar") {
    $dir = "rtl";
} else {
    $dir = "ltr";
}
$html .= '<table cellspacing="0" cellpadding="5" border="1" width="98%" dir="' . $dir . '" style="text-align: center;">';
$html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
$html .= '<td width="28%" colspan="3"></td>';
$html .= '<td width="18%" colspan="2">' . $this->lang->line('Initial_Balance') . '</td>';
$html .= '<td width="18%" colspan="2">' . $this->lang->line('period_movement') . '</td>';
$html .= '<td width="18%" colspan="2">' . $this->lang->line('fin_journal_Total') . '</td>';
$html .= '<td width="18%" colspan="2">' . $this->lang->line('Balance') . '</td>';
$html .= '</tr>';
$html .= '<tr style="background-color:#EBEBEB; font-weight:bold;">';
$html .= '<td width="4%">#</td>';
$html .= '<td width="6%">' . $this->lang->line('fin_treeaccount_account_no') . '</td>';
$html .= '<td width="18%">' . $this->lang->line('fin_journal_account_id') . '</td>';
$html .= '<td width="9%">' . $this->lang->line('fin_journal_debit') . '</td>';
$html .= '<td width="9%">' . $this->lang->line('fin_journal_creditor') . '</td>';
$html .= '<td width="9%">' . $this->lang->line('fin_journal_debit') . '</td>';
$html .= '<td width="9%">' . $this->lang->line('fin_journal_creditor') . '</td>';
$html .= '<td width="9%">' . $this->lang->line('fin_journal_debit') . '</td>';
$html .= '<td width="9%">' . $this->lang->line('fin_journal_creditor') . '</td>';
$html .= '<td width="9%">' . $this->lang->line('fin_journal_debit') . '</td>';
$html .= '<td width="9%">' . $this->lang->line('fin_journal_creditor') . '</td>';
$html .= '</tr>';
$html .= '<tbody>';
$fixed_assets_total = 0;
$fixed_assets_debit = 0;
$fixed_assets_creditor = 0;
$sum1 = $sum2 = $sum3 = $sum4 = $sum5 = $sum5 = $sum6 = $sum7 = $sum8 = 0;
$acc_fixed_assets = intval($this->session->userdata('acc_fixed_assets'));
$FixedAssetsAccounts = $this->M_fin_treeaccount->GetMultiRow2();

foreach ($FixedAssetsAccounts as $CurrentAssetsAccounts_Row) {
    $query = $this->db->query("select IFNULL(sum(debit), 0) as sum_debit from fin_journal JOIN fin_journal_main ON fin_journal_main.id=fin_journal.main_id where account_id=$CurrentAssetsAccounts_Row->id AND  thedate BETWEEN '$fromDT' AND  '$toDT' ");
    $res = $query->result()['0'];
    $query2 = $this->db->query("select IFNULL(sum(creditor), 0) as sum_creditor from fin_journal  JOIN fin_journal_main ON fin_journal_main.id=fin_journal.main_id where account_id=$CurrentAssetsAccounts_Row->id AND  thedate BETWEEN '$fromDT' AND  '$toDT' ");
    $res2 = $query2->result()['0'];
   
    //if (($CurrentAssetsAccounts_Row->startamount != 0) || ($res->sum_debit != 0) || ($res2->sum_creditor != 0)) {

        $html .= '<tr>';
        $html .= '<td >' . $i++ . '</td>';
        $html .= '<td >' . $CurrentAssetsAccounts_Row->account_no . '</td>';

        if ($this->session->userdata('lang') == "ar") {
            $acc_current_assets_name = $CurrentAssetsAccounts_Row->title;
        } else {
            $acc_current_assets_name = $CurrentAssetsAccounts_Row->title_en;
        }
        $sum1 += $CurrentAssetsAccounts_Row->startamount;
        $html .= '<td >' . $acc_current_assets_name . '</td>';
        $html .= '<td >' . $CurrentAssetsAccounts_Row->startamount . '</td>';
        $sum2 += 0;
        $html .= '<td >0</td>';
        $sum3 += $res->sum_debit;
        $html .= '<td >' . $res->sum_debit . '</td>';
        $sum4 += $res2->sum_creditor;
        $html .= '<td >' . $res2->sum_creditor . '</td>';
        $sum5 += ($CurrentAssetsAccounts_Row->startamount + $res->sum_debit);
        $html .= '<td >' . ($CurrentAssetsAccounts_Row->startamount + $res->sum_debit) . '</td>';

        $sum6 += $res2->sum_creditor;
        $html .= '<td >' . $res2->sum_creditor . '</td>';

        $ss = $CurrentAssetsAccounts_Row->startamount + $res->sum_debit;
        if (($ss - $res2->sum_creditor) > 0) {
            $sum7 += ($ss - $res2->sum_creditor);
            $html .= '<td >' . ($ss - $res2->sum_creditor) . '</td>';
            $sum8 += 0;
            $html .= '<td >0</td>';
        } else {
            $sum7 += 0;
            $html .= '<td >0</td>';
            $sum8 += ($res2->sum_creditor - $ss);
            $html .= '<td >' . ($res2->sum_creditor - $ss) . '</td>';
        }

        $html .= '</tr>';
   // }
}
$html .= '<tr>';
$html .= '<td colspan="3"><b>' . $this->lang->line('fin_journal_Total') . '</b></td>';
$html .= '<td>' . $sum1 . '</td>';
$html .= '<td>' . $sum2 . '</td>';
$html .= '<td>' . $sum3 . '</td>';
$html .= '<td>' . $sum4 . '</td>';
$html .= '<td>' . $sum5 . '</td>';
$html .= '<td>' . $sum6 . '</td>';
$html .= '<td>' . $sum7 . '</td>';
$html .= '<td>' . $sum8 . '</td>';
$html .= '</tr>';
$html .= '</tbody>';
$html .= '</table>';
$html .= '</div>';
$html .= '<div style="float:left; width:50%">';
$html .= '</div>';

$html .= '<br><br><br>';

$html = iconv("utf-8", "UTF-8//IGNORE", $html);
$mpdf->WriteHTML($html);


//echo $html;

$mpdf->Output();
exit;