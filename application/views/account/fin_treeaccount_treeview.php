<div class="card-body">
	<div id="kt_tree_1" class="tree-demo">
		<ul>
			<?php
			$cat_id=0;
			$level_no = 1;
			$GetTreeLevel1 = $this->M_fin_treeaccount->GetByLevel($level_no);
			foreach($GetTreeLevel1 as $GetTreeLevel1_Row) {
				$AccountID = $GetTreeLevel1_Row->id;
				$cat_id=$GetTreeLevel1_Row->id;
				if ($this->session->userdata('lang') == "ar")
				{
					$AccountTitle = $GetTreeLevel1_Row->title;
				}
				else
				{
					$AccountTitle = $GetTreeLevel1_Row->title_en;
				}
				$max=$this->M_fin_treeaccount->GetMaxAccount( $AccountID)[0];
			?>
			<li id="account_<?=$GetTreeLevel1_Row->id?>">
				<?php echo " (".$GetTreeLevel1_Row->level_no.") ". $AccountTitle ." (".$GetTreeLevel1_Row->account_no.") ";?>
				<?php
				$Segment2 = $this->uri->segment(2);
				$group_id = intval($this->session->userdata('GroupID'));
				$PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
				$page_id = $PageData->id;
				$CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
				if($CheckAdd != 0)
				{
			?>
				<button class="btn btn-sm px-1" onclick="addTree('<?=$cat_id?>','<?=$GetTreeLevel1_Row->id?>','2','<?php
				if(!is_null($max->max)){
						$mun=$max->max + 1000000;
				}else{
						 $mun=$GetTreeLevel1_Row->account_no + 1000000;
				}
				echo $mun;
				?>')"><i class="fa fa-plus"></i></button>
			<?php } ?>
				<!-- <button class="btn btn-sm px-1 <?= !$GetTreeLevel1_Row->deleted ? 'delete_btn_tree' : 'undelete_btn_tree' ?>"  onclick="return deleteTreeAccount('<?=$GetTreeLevel1_Row->id?>', this);"><i class="<?= !$GetTreeLevel1_Row->deleted ? 'flaticon-delete text-danger' : 'flaticon-refresh text-success' ?> icon-lg"></i></button> -->
				<ul>
					<?php
					$level_no = 2;
					$GetTreeLevel2 = $this->M_fin_treeaccount->GetSubByLevel($level_no, $AccountID);
					foreach($GetTreeLevel2 as $GetTreeLevel2_Row) {
						$AccountID = $GetTreeLevel2_Row->id;
						if ($this->session->userdata('lang') == "ar")
						{
							$AccountTitle = $GetTreeLevel2_Row->title;
						}
						else
						{
							$AccountTitle = $GetTreeLevel2_Row->title_en;
						}
						$max=$this->M_fin_treeaccount->GetMaxAccount( $AccountID)[0];
					?>
					<li id="account_<?=$GetTreeLevel2_Row->id?>">
						<a href="<?php echo base_url();?>account/fin_treeaccount/view_account/<?php echo $AccountID;?>"><?php echo " (".$GetTreeLevel2_Row->level_no.") ".$AccountTitle." (".$GetTreeLevel2_Row->account_no.")";?>
							<?php
							$Segment2 = $this->uri->segment(2);
							$group_id = intval($this->session->userdata('GroupID'));
							$PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
							$page_id = $PageData->id;
							$CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
							if($CheckAdd != 0)
							{
						?>
						<button class="btn btn-sm px-1"  onclick="addTree('<?=$cat_id?>','<?=$GetTreeLevel2_Row->id?>','3','<?php
				if(!is_null($max->max)){
						$mun=$max->max + 10000;
				}else{
						 $mun=$GetTreeLevel2_Row->account_no + 10000;
				}
				echo $mun;
				?>')"><i class="fa fa-plus"></i></button>
			<?php } ?>
				<!-- <button class="btn btn-sm px-1 <?= !$GetTreeLevel2_Row->deleted ? 'delete_btn_tree' : 'undelete_btn_tree' ?>"  onclick="return deleteTreeAccount('<?=$GetTreeLevel2_Row->id?>', this)"><i class="<?= !$GetTreeLevel2_Row->deleted ? 'flaticon-delete text-danger' : 'flaticon-refresh text-success' ?> icon-lg"></i></button> -->
				</a>

						<ul>
							<?php
							$level_no = 3;
							$GetTreeLevel3 = $this->M_fin_treeaccount->GetSubByLevel($level_no, $AccountID);
							foreach($GetTreeLevel3 as $GetTreeLevel3_Row) {
								$AccountID = $GetTreeLevel3_Row->id;
								if ($this->session->userdata('lang') == "ar")
								{
									$AccountTitle = $GetTreeLevel3_Row->title;
								}
								else
								{
									$AccountTitle = $GetTreeLevel3_Row->title_en;
								}
								$max=$this->M_fin_treeaccount->GetMaxAccount( $AccountID)[0];
							?>
							<li id="account_<?=$GetTreeLevel3_Row->id?>">
								<a href="<?php echo base_url();?>account/fin_treeaccount/view_account/<?php echo $AccountID;?>"><?php echo " (".$GetTreeLevel3_Row->level_no.") ".$AccountTitle." (".$GetTreeLevel3_Row->account_no.")";?>
									<?php
									$Segment2 = $this->uri->segment(2);
									$group_id = intval($this->session->userdata('GroupID'));
									$PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
									$page_id = $PageData->id;
									$CheckAdd = $this->M_usr_usersprivileges->CheckAdd($group_id, $page_id);
									if($CheckAdd != 0)
									{
								?>
													<button class="btn btn-sm px-1"  onclick="addTree('<?=$cat_id?>','<?=$GetTreeLevel3_Row->id?>','4','<?php
				if(!is_null($max->max)){
						$mun=$max->max + 1;
				}else{
						 $mun=$GetTreeLevel3_Row->account_no + 1;
				}
				echo $mun;
				?>')"><i class="fa fa-plus"></i></button>
<?php } ?>
<?php
$Segment2 = $this->uri->segment(2);
$group_id = intval($this->session->userdata('GroupID'));
$PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
$page_id = $PageData->id;
$CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
if($CheckDelete != 0)
{
?>
				<button class="btn btn-sm px-1 delete_btn_tree"  onclick="return deleteTreeAccount('<?=$GetTreeLevel3_Row->id?>', this)"><i class="flaticon-delete text-danger icon-lg"></i></button>
			<?php } ?>
			</a>
								<ul>
									<?php
									$level_no = 4;
									$GetTreeLevel4 = $this->M_fin_treeaccount->GetSubByLevel($level_no, $AccountID);
									foreach($GetTreeLevel4 as $GetTreeLevel4_Row) {
										$AccountID = $GetTreeLevel4_Row->id;
										if ($this->session->userdata('lang') == "ar")
										{
											$AccountTitle = $GetTreeLevel4_Row->title;
										}
										else
										{
											$AccountTitle = $GetTreeLevel4_Row->title_en;
										}
									?>
									<li data-jstree='{ "type" : "file" }'  id="account_<?=$GetTreeLevel4_Row->id?>">
										<a href="<?php echo base_url();?>account/fin_treeaccount/view_account/<?php echo $AccountID;?>"><?php echo " (".$GetTreeLevel4_Row->level_no.") ".$AccountTitle." (".$GetTreeLevel4_Row->account_no.")";?>
										</button>
										<?php
										$Segment2 = $this->uri->segment(2);
										$group_id = intval($this->session->userdata('GroupID'));
										$PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
										$page_id = $PageData->id;
										$CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
										if($CheckDelete != 0)
										{
										?>
										<button class="btn btn-sm px-1 delete_btn_tree"  onclick="return deleteTreeAccount('<?=$GetTreeLevel4_Row->id?>', this)"><i class="flaticon-delete text-danger icon-lg"></i></button>
									<?php } ?>
								</a>
										<ul>
											<?php
											$level_no = 5;
											$GetTreeLevel5 = $this->M_fin_treeaccount->GetSubByLevel($level_no, $AccountID);
											foreach($GetTreeLevel5 as $GetTreeLevel5_Row) {
												$AccountID = $GetTreeLevel5_Row->id;
												if ($this->session->userdata('lang') == "ar")
												{
													$AccountTitle = $GetTreeLevel5_Row->title;
												}
												else
												{
													$AccountTitle = $GetTreeLevel5_Row->title_en;
												}
											?>
											<li data-jstree='{ "type" : "file" }' id="account_<?=$GetTreeLevel5_Row->id?>">
												<a href="<?php echo base_url();?>account/fin_treeaccount/view_account/<?php echo $AccountID;?>"><?php echo " (".$GetTreeLevel5_Row->level_no.") ".$AccountTitle." (".$GetTreeLevel5_Row->account_no.")";?></button><button class="btn btn-sm px-1 delete_btn_tree"  onclick="return deleteTreeAccount('<?=$GetTreeLevel5_Row->id?>', this)"><i class="flaticon-delete text-danger icon-lg"></i></button>
																	</a>

												<ul>
													<?php
													$level_no = 6;
													$GetTreeLevel6 = $this->M_fin_treeaccount->GetSubByLevel($level_no, $AccountID);
													foreach($GetTreeLevel6 as $GetTreeLevel6_Row) {
														$AccountID = $GetTreeLevel6_Row->id;
														if ($this->session->userdata('lang') == "ar")
														{
															$AccountTitle = $GetTreeLevel6_Row->title;
														}
														else
														{
															$AccountTitle = $GetTreeLevel6_Row->title_en;
														}
													?>
													<li data-jstree='{ "type" : "file" }' id="account_<?=$GetTreeLevel6_Row->id?>">
														<a href="<?php echo base_url();?>account/fin_treeaccount/view_account/<?php echo $AccountID;?>"><?php echo " (".$GetTreeLevel6_Row->level_no.") ".$AccountTitle." (".$GetTreeLevel6_Row->account_no.")";?></button><button class="btn btn-sm px-1 delete_btn_tree"  onclick="return deleteTreeAccount('<?=$GetTreeLevel6_Row->id?>', this)"><i class="flaticon-delete text-danger icon-lg"></i></button>
																			</a>

													</li>
													<?php
													}
													?>
												</ul>
											</li>
											<?php
											}
											?>
										</ul>
									</li>
									<?php
									}
									?>
								</ul>
							</li>
							<?php
							}
							?>
						</ul>
					</li>
					<?php
					}
					?>
				</ul>

			</li>
			<?php
			}
			?>
		</ul>
	</div>
</div>



<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog modal-sm modal-notify" role="document">
			<!--Content-->
			<div class="modal-content text-center">
					<!--Header-->
					<div class="modal-header d-flex justify-content-center">
							<p class="heading" id="HeadTitle">اضافة سجل </p>
					</div>

					<!--Body-->
					<div class="modal-body">
							<form action="<?=base_url('account/fin_treeaccount/insert_data')?>"
										enctype="multipart/form-data" method="post" accept-charset="utf-8">
									<input type="hidden" id="startamount" name="startamount" value="0">
									<input type="hidden" id="basic" name="basic" value="0">
									<input type="hidden" id="deleted" name="deleted" value="0">
									<input type="hidden" name="id" value="">
									<input type="hidden" name="adQu" value="1">
									<input type="hidden" id="company_id" name="company_id" value="1">
									<input type="hidden" id="category_id" name="category_id" value="">
									<input type="hidden" id="parent_id" name="parent_id" value="">
									<input type="hidden" id="level_no" name="level_no" value="">
									<input type="hidden" id="account_no" name="account_no" value="">


									<div class="form-body">
											<div class="form-group row">
													<div class="col-sm-12">
															<label class="col-form-label">الاسم العربي</label>
															<input type="text" name="title" class="form-control" value="" required="">
													</div>
													<div class="col-sm-12">
															<label class="col-form-label">الاسم الانجليزي</label>
															<input type="text" name="title_en" class="form-control" value="" required="">
													</div>
											</div>
									</div>
									<div class="kt-portlet__foot">
											<div class="kt-form__actions">
													<div class="row">
															<div class="col-lg-12 ml-lg-auto">
																	<a type="button" class="btn  btn-primary waves-effect" data-dismiss="modal">اغلاق</a>
																	<button type="submit" class="btn btn-primary mr-2" accesskey="s">حفظ</button>
															</div>
													</div>
											</div>
									</div>
							</form>


					</div>
			</div>
	</div>
</div>

<script>
deleteTreeAccount = (account_id, elem)=>{
	var action = ($(elem).hasClass('delete_btn_tree')) ? 'delete' : 'undelete';

	if(action == 'delete'){
		if(!confirm('هل أنت متأكد من عملية الحذف ؟')) return false;
		// $(elem).removeClass('delete_btn_tree').addClass('undelete_btn_tree')
		// $(elem).find('i').attr('class', 'flaticon-refresh text-success icon-lg');
		var node = $("#kt_tree_1").jstree().get_node("account_"+account_id);
				$("#kt_tree_1").jstree("delete_node", node);
	}else{
		$(elem).addClass('delete_btn_tree').removeClass('undelete_btn_tree')
		$(elem).find('i').attr('class', 'flaticon-delete text-danger icon-lg');
	}
	$.get('<?php echo base_url();?>account/fin_treeaccount/'+action+'/'+account_id+'/forever');
	return false;
}
</script>
