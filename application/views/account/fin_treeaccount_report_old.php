<!DOCTYPE html>
<html direction="<?php echo $this->session->userdata('Direction');?>" dir="<?php echo $this->session->userdata('Direction');?>" style="direction: <?php echo $this->session->userdata('Direction');?>">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta charset="utf-8"/>
<title><?php echo $this->lang->line('AppTitle');?></title>

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<link rel="canonical" href="https://www.lbi-egypt.com/" />
<!--begin::Fonts-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
<!--end::Fonts-->
<!--begin::Page Vendors Styles(used by this page)-->
<link href="<?php echo base_url();?>/assets/plugins/custom/fullcalendar/fullcalendar.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles-->
<!--begin::Global Theme Styles(used by all pages)-->
<link href="<?php echo base_url();?>/assets/plugins/global/plugins.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>/assets/plugins/custom/prismjs/prismjs.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>/assets/css/style.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?php echo base_url();?>/assets/media/logos/favicon.ico" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/arabic_font.css" />
<style type="text/css">
        body{font-family: 'Droid Arabic Kufi', serif; padding-bottom: 0px !important; letter-spacing: 0px;}
</style>
</head>
<body>
<br><br>
<?php
    echo form_open("account/fin_treeaccount/search_account");
?>
    <table cellspacing="0" cellpadding="5" border="1" width="98%" dir="rtl" align="center">
        <tr>
            <td align="right"><strong>من تاريخ :</strong></td>
            <td align="right"><input type="date" id="fromdate" name="fromdate" value="<?php echo $fromdate;?>" required></td>
            <td align="right"><strong>إلى تاريخ :</strong></td>
            <td align="right"><input type="date" id="todate" name="todate" value="<?php echo $todate;?>" required></td>
            <input type="hidden" id="accountid" name="accountid" value="<?php echo $accountid;?>">
            <input type="hidden" id="Search" name="Search" value="<?php echo $Search;?>" />
            <td align="right"><button type="submit"><?php echo $this->lang->line('Search');?></button></td>
        </tr>
    </table>
<?php
    echo form_close();
?>
<br><br>
<div style="margin-left: auto; margin-right: auto; width: 80%; text-align: center; font-weight: bold">
    <?php echo $title;?>
    <br>
    <?php echo $fromdate." : ".$todate;?>
</div>
<br><br>
<div class="kt-portlet__body">
<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
<thead class="flip-content">
<tr>
    <th></th>
    <th><?php echo $this->lang->line('fin_journal_debit');?></th>
    <th><?php echo $this->lang->line('fin_journal_creditor');?></th>
    <th><?php echo $this->lang->line('Balance');?></th>
    <th><?php echo $this->lang->line('fin_journal_account_id');?></th>
    <th><?php echo $this->lang->line('fin_journal_details');?></th>
    <th><?php echo $this->lang->line('fin_journal_thedate');?></th>
</tr>
</thead>
<tbody>
    <tr>
        <td></td>
        <td>
            <?php
            if (($category_id ==1) || ($category_id ==4))
			{
				//echo $StartAmount;
			}
            ?>
        </td>
        <td>
            <?php
            if (($category_id ==2) || ($category_id ==3) || ($category_id ==5))
			{
				//echo $StartAmount;
			}
            ?>
        </td>
        <td><strong>
        	<?php
            $StartAmount = 0;
            $BeforDay = date('Y-m-d', strtotime('-1 days', strtotime($fromdate)));
        	$AmountBeforeJournal = $startamount;
			$AmountBeforeJournalData = $this->M_fin_journal->GetRealtedJournalStartDate($accountid, $fromdate);
			$AmountBeforeJournalValue = 0;
			$RealtedJournalStartDateCount = $this->M_fin_journal->GetRealtedJournalStartDateCount($accountid, $fromdate);
			$AccountCategoryID = 0;
			if ($RealtedJournalStartDateCount >0)
			{
				foreach($AmountBeforeJournalData as $AmountBeforeJournalData_Row) {
					//$AmountBeforeJournalValue = doubleval($AmountBeforeJournalData_Row->thevalue);
					$AccountData = $this->M_fin_treeaccount->GetRow($accountid);
					$AccountCategoryID = $AccountData->category_id;
					if (($AccountCategoryID ==1) || ($AccountCategoryID ==4))
					{
                        
					}
					else
						{
							$AmountBeforeJournal -= doubleval($AmountBeforeJournalData_Row->debit);
                            $AmountBeforeJournal += doubleval($AmountBeforeJournalData_Row->creditor);
						}
				}
			}
			echo $AmountBeforeJournal;
			$Balance = $AmountBeforeJournal;
        	?></strong>
        </td>
        <td></td>
        <td><?php echo "الرصيد السابق";?></td>
        <td></td>
    </tr>
    <?php
    $TotalValue = 0;
    $Serial = 0;
    $Debit = 0;
	$DebitTotal = 0;
	$CreditorTotal = 0;
    $Creditor = 0;
    $CategoryID = 0;
    $TheValue = 0;
    $OtherAccountName = "";
	//$Balance = $StartAmount;
	//$AccountCategoryID = 0;
    $MultiRows = $this->M_fin_journal->GetDetailsByAccount($accountid);
    foreach($MultiRows as $MultiRows_Row) {
        $Serial += 1;
        $AccountName = $this->M_fin_treeaccount->GetRow($accountid);
        $category_id = $AccountName->category_id;

        $main_id = $MultiRows_Row->main_id;
        $JournalMainData = $this->M_fin_journal_main->GetRow($main_id);
        $details = $JournalMainData->details;
        $thedate = $JournalMainData->thedate;
    ?>
    <tr>
    <td><?php echo $Serial;?></td>
    <td>
        <?php
            echo doubleval($MultiRows_Row->debit);
            $Debit += doubleval($MultiRows_Row->debit);
            $DebitTotal += $Debit;
        ?>
    </td>
    <td>
        <?php
            echo doubleval($MultiRows_Row->creditor);
            $Creditor += doubleval($MultiRows_Row->creditor);
			$CreditorTotal += $Creditor;
        ?>
    </td>
    <td>
    </td>
    <td>
        <?php
        
        echo $AccountName->title;
        ?>
    </td>
    <td><?php echo $details;?></td>
    <td><?php echo $thedate;?></td>
    </tr>
    <?php
    $CategoryID = 0;
    $TheValue = 0;
    if(($category_id == 1) || ($category_id == 3))
    {

        $Balance = $Balance + $MultiRows_Row->debit - $MultiRows_Row->creditor;
    }
    else
    {
        $Balance = $Balance + $MultiRows_Row->creditor - $MultiRows_Row->debit;
    }
    //echo $this->Converter->ConvertToArabic($Balance);
    }
    ?>

<tr style="background-color:#EBEBEB; font-weight:bold;">
<td><?php echo ($Serial + 1);?></td>
<td>
    <?php
    $Final = 0;
    if (($AccountCategoryID ==1) || ($AccountCategoryID ==4))
	{
		$Final = $startamount + $Debit - $Creditor;
        //echo $Debit;
	}
	echo doubleval($Debit);
    ?>
</td>
<td>
    <?php
    $Final = 0;
    if (($AccountCategoryID ==2) || ($AccountCategoryID ==3) || ($AccountCategoryID ==5))
	{
		$Final = $StartAmount - $Debit + $Creditor;
        //echo $Creditor;
	}
	echo doubleval($Creditor);
    ?>
</td>
<td>
	<?php
    echo doubleval($Balance);
    ?>
</td>
<td></td>
<td><?php echo "الرصيد الختامي";?></td>
<td></td>
</tr>
        
</tbody>
</table>
</div>
                            
<!--begin::Global Config(global config for global JS scripts)-->
<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
<!--end::Global Config-->
<!--begin::Global Theme Bundle(used by all pages)-->
<script src="<?php echo base_url();?>assets/plugins/global/plugins.bundle.js"></script>
<script src="<?php echo base_url();?>assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
<script src="<?php echo base_url();?>assets/js/scripts.bundle.js"></script>
<script src="<?php echo base_url();?>assets/plugins/custom/datatables/datatables.bundle.js"></script>
<!--end::Page Vendors-->
<!--begin::Page Scripts(used by this page)-->
<script src="<?php echo base_url();?>assets/js/pages/crud/datatables/extensions/buttons.js"></script>
<!--begin::Page Scripts(used by this page)-->
<script src="<?php echo base_url();?>assets/js/pages/crud/forms/widgets/select2.js"></script>
<script src="<?php echo base_url();?>assets/js/pages/crud/forms/widgets/bootstrap-select.js"></script>
</body>

<!-- END BODY -->

<!-- Mirrored from www.keenthemes.com/preview/metronic/theme_rtl/templates/admin/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Jun 2015 00:11:15 GMT -->
</html>