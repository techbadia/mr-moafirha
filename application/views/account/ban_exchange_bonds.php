<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
    <thead>
        <tr>
            <th>ID</th>
            <th><?php echo lang('fin_journal_details');?></th>
            <th><?php echo lang('fin_journal_thedate');?></th>
            <th><?php echo lang('fin_journal_bill_id');?></th>
            <th><?php echo lang('fin_journal_table_name');?></th>
            <th><?php echo lang('fin_journal_automatic');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('View');?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($DataRows as $DataRows_Row) {
            $fin_journal_id = $DataRows_Row->id;
        ?>
        <tr>
            <td>
            <?php echo $DataRows_Row->id;?>
            </td>
            <td><?php echo $DataRows_Row->details;?></td>
            <td><?php echo $DataRows_Row->thedate;?></td>
            <td><?php echo $DataRows_Row->bill_id;?></td>
            <td>
            <?php
            $table_name = $DataRows_Row->table_name;
            if($table_name == "buy_bill")
            {
                echo lang('buy_bill');
            }
            else if($table_name == "buy_returns")
            {
                echo lang('buy_returns');
            }
            else if($table_name == "expenses")
            {
                echo lang('expenses');
            }
            else if($table_name == "sale_bill")
            {
                echo lang('sale_bill');
            }
            else if($table_name == "sale_returns")
            {
                echo lang('sale_returns');
            }
            else
            {
                echo lang('fin_journal_manual');
            }
            ?>
            </td>
            <td>
                <?php
                $automatic = $DataRows_Row->automatic;
                if($automatic == 1)
                {
                    echo lang('Yes');
                }
                else
                {
                    echo lang('No');
                }
                ?>
            </td>
            <td style="text-align:center">
                <?php
                    if($DataRows_Row->deleted == 0) {
                ?>
                <a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
                    <i class="flaticon-delete text-danger icon-lg"></i> 
                </a>
                <?php } ?>
            </td>
            <td style="text-align:center">
                <?php
                    if($DataRows_Row->deleted == 1) {
                ?>
                    <a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
                        <i class="flaticon-refresh text-success icon-lg"></i> 
                    </a>
                <?php } ?>
            </td>
            <td style="text-align:center">
            <a class="btn btn-danger font-weight-bold mr-2 btn-sm" target="_blank" href="<?php echo base_url()."account/fin_journal/view_pdf/".$DataRows_Row->id;?>/2">PDF</a>
            <button class="btn btn-primary view_detail" relid="<?php echo $DataRows_Row->id;?>"><i class="fa fa-eye"></i></button>
            </td>
        </tr>
        <?php
        }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <th>ID</th>
            <th><?php echo lang('fin_journal_details');?></th>
            <th><?php echo lang('fin_journal_thedate');?></th>
            <th><?php echo lang('fin_journal_bill_id');?></th>
            <th><?php echo lang('fin_journal_table_name');?></th>
            <th><?php echo lang('fin_journal_automatic');?></th>
            <th><?php echo lang('Delete');?></th>
            <th><?php echo lang('UnDelete');?></th>
            <th><?php echo lang('View');?></th>
        </tr>
    </tfoot>
</table>

<div id="show_modal" class="modal fade" role="dialog" style="background: #000;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 style="font-size: 24px; color: #17919e; text-shadow: 1px 1px #ccc;">
                    <i class="fa fa-folder"></i><?php echo lang('mon_exchange_bonds');?>
                </h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3"><?php echo lang('fin_journal_details');?></div>
                    <div class="col-md-9" id="details"></div>
                </div>
                <div class="row">
                    <div class="col-md-3"><?php echo lang('fin_journal_thedate');?></div>
                    <div class="col-md-9" id="thedate"></div>
                </div>
                <table id="ModalTable" class="table table-bordered table-striped">
                    <thead class="btn-primary">
                        <tr>
                        <th align='right'><?php echo lang('fin_journal_account_id');?></th>
                        <th style="text-align:center"><?php echo lang('fin_journal_debit');?></th>
                        <th style="text-align:center"><?php echo lang('fin_journal_creditor');?></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo lang('Close');?></button>
                <button type="button" class="btn btn-info" onclick="printSelection(document.getElementById('show_modal'));return false;"><i class="fa fa-print"></i> <?php echo $this->lang->line('Print');?></button>
            </div>
        </div>
    </div>
</div>