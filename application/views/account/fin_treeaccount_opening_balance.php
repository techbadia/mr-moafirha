
								<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
									<?php
										$FormPath = base_url().$Segment1."/".$Segment2."/opening_balance_insert";
										echo form_open_multipart($FormPath);
									?>
									<div class="form-body">
										<div class="form-group row">

											<div class="col-sm-12">
												<label class="col-form-label"><?php echo lang('details');?></label>
												<input type="text"  name="details" class="form-control" value="" >
											</div>
                                            <?php
                                            foreach ($DataRows as $DataRow)
                                            {
                                            ?>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_account_no');?></label>
                                                <input type="hidden"  name="accountID[]" value="<?=$DataRow->id?>" required>
                                                <span class="form-control"><?=$DataRow->account_no?></span>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_title');?></label>
                                                <span class="form-control"><?php
                                                    if ($this->session->userdata('lang') == "ar")
                                                   $name= $DataRow->title;
                                                    else
                                                        $name= $DataRow->title_en;
                                                    echo $name;
                                                    ?></span>
                                            </div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_journal_debit');?></label>
												<input type="number" name="debit[]" value="0" class="form-control" >
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_journal_creditor');?></label>
												<input type="number" name="creditor[]" value="0"  class="form-control" >
											</div>
                                                <hr>
                                            <?php
                                            }
                                            ?>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
										echo form_close();
									?>
							</div>
								