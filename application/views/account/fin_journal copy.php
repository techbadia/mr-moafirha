<?php
include(__DIR__."/../design/page_details.php");
$CurrentPage = $this->uri->segment(1);
?>
<!DOCTYPE html>
<html direction="<?php echo $this->session->userdata('Direction');?>" dir="<?php echo $this->session->userdata('Direction');?>" style="direction: <?php echo $this->session->userdata('Direction');?>">

	<!-- begin::Head -->
	<head>
		<base href="../../../">
		<meta charset="utf-8" />
		<title><?php echo $PageTitle;?></title>
		<meta name="description" content="Buttons examples">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

		<!--end::Fonts -->

		<!--begin::Page Vendors Styles(used by this page) -->
		<link href="<?php echo base_url();?>assets/plugins/custom/datatables/datatables.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />

		<!--end::Page Vendors Styles -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="<?php echo base_url();?>assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>assets/css/style.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->
		<link href="<?php echo base_url();?>assets/css/skins/header/base/light.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>assets/css/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>assets/css/skins/brand/dark.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>assets/css/skins/aside/dark.css" rel="stylesheet" type="text/css" />

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="<?php echo base_url();?>assets/media/logos/favicon.ico" />
		<link href="<?php echo base_url();?>assets/css/arabic_font.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
				body{font-family: 'Droid Arabic Kufi', serif; padding-bottom: 0px !important; letter-spacing: 0px;}
		</style>
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

		<!-- begin:: Page -->

		<!-- begin:: Header Mobile -->
		<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
			<div class="kt-header-mobile__logo">
				<a href="<?php echo base_url();?>">
					<img alt="Logo" src="<?php echo base_url();?>assets/media/logos/logo-light.png" />
				</a>
			</div>
			<div class="kt-header-mobile__toolbar">
				<button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
			</div>
		</div>

		<!-- end:: Header Mobile -->
		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

				<!-- begin:: Aside -->

				<!-- Uncomment this to display the close button of the panel
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
-->
				<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

					<!-- begin:: Aside -->
					<div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
						<div class="kt-aside__brand-logo">
							<a href="<?php echo base_url();?>">
								<img alt="Logo" src="<?php echo base_url();?>assets/media/logos/logo-light.png" />
							</a>
						</div>
						<div class="kt-aside__brand-tools">
							<button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
								<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<polygon points="0 0 24 0 24 24 0 24" />
											<path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) " />
											<path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) " />
										</g>
									</svg></span>
								<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<polygon points="0 0 24 0 24 24 0 24" />
											<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
											<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
										</g>
									</svg></span>
							</button>

							<!--
			<button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
			-->
						</div>
					</div>

					<!-- end:: Aside -->

					<!-- begin:: Aside Menu -->
					<?php include(__DIR__."/../design/design_sidebar.php");?>
					<!-- end:: Aside Menu -->
				</div>

				<!-- end:: Aside -->
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

					<!-- begin:: Header -->
					<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

						<!-- begin:: Header Menu -->

						<!-- Uncomment this to display the close button of the panel
<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
-->
						<?php include(__DIR__."/../design/design_header.php");?>

						<!-- end:: Header Menu -->

						<!-- begin:: Header Topbar -->
						<?php include(__DIR__."/../design/design_topbar.php");?>
						<!-- end:: Header Topbar -->
					</div>

					<!-- end:: Header -->
					<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

						<!-- begin:: Subheader -->
						<?php include(__DIR__."/../design/design_titlebar.php");?>
						<!-- end:: Subheader -->

						<!-- begin:: Content -->
						<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<div class="alert alert-light alert-elevate" role="alert">
								<div class="alert-icon"><i class="flaticon-information"></i></div>
								<div class="alert-text"><?php echo $PageDetails;?></div>
							</div>
							<div class="kt-portlet">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
										<?php echo $Path2;?>
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="dropdown dropdown-inline">
												<a href="<?php echo base_url();?>fin_journal/insert_view" class="btn btn-brand btn-icon-sm"><i class="flaticon2-plus"></i> <?php echo lang('Add');?></a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('fin_journal_bill_id');?></th>
												<th><?php echo lang('fin_journal_table_name');?></th>
												<th><?php echo lang('fin_journal_automatic');?></th>
												<th><?php echo lang('fin_journal_details');?></th>
												<th><?php echo lang('fin_journal_thedate');?></th>
												<th><?php echo lang('Edit');?></th>
												<th><?php echo lang('Delete');?></th>
												<th><?php echo lang('UnDelete');?></th>
												<th><?php echo lang('View');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												
												$fin_journal_id = $DataRows_Row->id;
											?>
											<tr>
												<td>
												<?php echo $DataRows_Row->id;?>
												</td>
												<td><?php echo $DataRows_Row->bill_id;?></td>
												<td>
												<?php
												$table_name = $DataRows_Row->table_name;
												if($table_name == "buy_bill")
												{
													echo lang('buy_bill');
												}
												else if($table_name == "buy_returns")
												{
													echo lang('buy_returns');
												}
												else if($table_name == "expenses")
												{
													echo lang('expenses');
												}
												else if($table_name == "sale_bill")
												{
													echo lang('sale_bill');
												}
												else if($table_name == "sale_returns")
												{
													echo lang('sale_returns');
												}
												else
												{
													echo lang('fin_journal_manual');
												}
												?>
												</td>
												<td>
													<?php
													$automatic = $DataRows_Row->automatic;
													if($automatic == 1)
													{
														echo lang('Yes');
													}
													else
													{
														echo lang('No');
													}
													?>
												</td>
												<td><?php echo $DataRows_Row->details;?></td>
												<td><?php echo $DataRows_Row->thedate;?></td>
												<td>
												<a href="<?php echo base_url();?>fin_journal/update_view/<?php echo $DataRows_Row->id;?>" class="btn btn-secondary btn-sm"><i class="flaticon-edit"></i></a>
												</td>
												<td>
												<?php
												$deleted = $DataRows_Row->deleted;
												if($deleted == 0)
												{
												?>
												<a href="<?php echo base_url();?>fin_journal/delete/<?php echo $DataRows_Row->id;?>" class="btn btn-danger btn-sm"><i class="flaticon2-delete"></i></a>
												<?php
												}
												?>
												</td>
												<td>
												<?php
												if($deleted == 1)
												{
												?>
												<a href="<?php echo base_url();?>fin_journal/undelete/<?php echo $DataRows_Row->id;?>" class="btn btn-success btn-sm"><i class="flaticon2-reply"></i></a>
												<?php
												}
												?>
												</td>
												<td>
												<button class="btn btn-primary view_detail" relid="<?php echo $DataRows_Row->id;?>"><?php echo lang('View');?></button>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										<tfoot>
											<tr>
												<th>ID</th>
												<th><?php echo lang('fin_journal_bill_id');?></th>
												<th><?php echo lang('fin_journal_table_name');?></th>
												<th><?php echo lang('fin_journal_automatic');?></th>
												<th><?php echo lang('fin_journal_details');?></th>
												<th><?php echo lang('fin_journal_thedate');?></th>
												<th><?php echo lang('Edit');?></th>
												<th><?php echo lang('Delete');?></th>
												<th><?php echo lang('UnDelete');?></th>
												<th><?php echo lang('View');?></th>
											</tr>
										</tfoot>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
							
						</div>

						<!-- end:: Content -->
					</div>

					<!-- begin:: Footer -->
					<?php include(__DIR__."/../design/design_footer.php");?>

					<!-- end:: Footer -->
				</div>
			</div>
		</div>

		<!-- end:: Page -->

		

		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>

		<!-- end::Scrolltop -->

		<div id="show_modal" class="modal fade" role="dialog" style="background: #000;">
			<div class="modal-dialog">
				<div class="modal-content">
				<div class="modal-header">
					<h3 style="font-size: 24px; color: #17919e; text-shadow: 1px 1px #ccc;">
					<i class="fa fa-folder"></i><?php echo lang('fin_journal');?>
					</h3>
				</div>
				<div class="modal-body">
				<div class="row">
					<div class="col-md-3"><?php echo lang('fin_journal_details');?></div>
					<div class="col-md-9" id="details"></div>
				</div>
				<div class="row">
					<div class="col-md-3"><?php echo lang('fin_journal_thedate');?></div>
					<div class="col-md-9" id="thedate"></div>
				</div>
					<table class="table table-bordered table-striped" id="ModalTable">
						<thead class="btn-primary">
							<tr>
							<th><?php echo lang('fin_journal_account_id');?></th>
							<th><?php echo lang('fin_journal_debit');?></th>
							<th><?php echo lang('fin_journal_creditor');?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$TotalDebit = 0;
							$TotalCreditor = 0;
							$CurrentJournalID = 0;
							$CurrentJournalID = intval($this->session->userdata('Fin_journal_View_ID'));
							if($CurrentJournalID > 0) {
							$DetailsRows = "";
							$DetailsRows = $this->M_fin_journal->GetModalDetails();
							foreach($DetailsRows as $DetailsRows_Row) {
								$TotalDebit += doubleval($DetailsRows_Row->debit);
								$TotalCreditor += doubleval($DetailsRows_Row->creditor);
							?>
							<tr>
							<td>
							<?php
							$account_id = $DetailsRows_Row->account_id;
							$AccountData = $this->M_fin_treeaccount->GetRow($account_id);
							if ($this->session->userdata('lang') == "ar")
							{
								echo $AccountData->title;
							}
							else
							{
								echo $AccountData->title_en;
							}
							?>
							</td>
							<td><?php echo $DetailsRows_Row->debit;?></td>
							<td><?php echo $DetailsRows_Row->creditor;?></td>
							</tr>
							<?php
							}
							}
							?>
						</tbody>
						<tfoot class="btn-primary">
							<tr>
							<th><?php echo lang('fin_journal_Total');?></th>
							<th><?php echo $TotalDebit;?></th>
							<th><?php echo $TotalCreditor;?></th>
							</tr>
						</tfoot>
					</table>

					

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo lang('Close');?></button>
				</div>
				</div>
			</div>
		</div>

		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": [
							"#c5cbe3",
							"#a1a8c3",
							"#3d4465",
							"#3e4466"
						],
						"shape": [
							"#f0f3ff",
							"#d9dffa",
							"#afb4d4",
							"#646c9a"
						]
					}
				}
			};
		</script>

		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="<?php echo base_url();?>assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
		<script src="<?php echo base_url();?>assets/js/scripts.bundle.js" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors(used by this page) -->
		<script src="<?php echo base_url();?>assets/plugins/custom/datatables/datatables.bundle.js" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts(used by this page) -->
		<script src="<?php echo base_url();?>assets/js/pages/crud/datatables/extensions/buttons.js" type="text/javascript"></script>

		<!--end::Page Scripts -->

		<script type="text/javascript">
			$(document).ready(function() {

				$('.view_detail').click(function(){
					
					var id = $(this).attr('relid'); //get the attribute value
					
					$.ajax({
						url : "<?php echo base_url(); ?>fin_journal/view_main",
						data:{id : id},
						method:'GET',
						dataType:'json',
						success:function(response) {
							$('#details').html(response.details); //hold the response in id and show on popup
							$('#thedate').html(response.thedate);
							$('#bill_id').html(response.bill_id);
							$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
							records = JSON.parse(data);
						}
					});


					$.ajax({
						url: '<?php echo base_url(); ?>fin_journal/view_details',
						data:{id : id},
						type: 'get',
						dataType: 'JSON',
						success: function(response){
							var len = response.length;
							$("#ModalTable").find("tbody").empty();
							for(var i=0; i<len; i++){
								var id = response[i].id;
								var account_id = response[i].account_id;
								var debit = response[i].debit;
								var creditor = response[i].creditor;
								
								var tr_str = "<tr>" +
									"<td align='center'>" + (i+1) + "</td>" +
									"<td align='center'>" + account_id + "</td>" +
									"<td align='center'>" + debit + "</td>" +
									"<td align='center'>" + creditor + "</td>" +
									"</tr>";

								$("#ModalTable tbody").append(tr_str);
							}

						}
					});

				});

				
			});

		</script>
	</body>

	<!-- end::Body -->
</html>