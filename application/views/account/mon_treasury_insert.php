
								<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
									<?php
										$FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
										echo form_open_multipart($FormPath);
									?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_category_id');?></label>
												<select class="form-control kt-selectpicker" name="category_id" data-size="5" data-live-search="true" required>
													<?php
													$CategoryList = $this->M_fin_treecategory->GetMultiRow();
													foreach($CategoryList as $CategoryList_Row) {
														if($CategoryList_Row->id == 1) {
														if ($this->session->userdata('lang') == "ar")
														{
															$CategoryTitle = $CategoryList_Row->title;
														}
														else
														{
															$CategoryTitle = $CategoryList_Row->title_en;
														}
													?>
													<option value="<?php echo $CategoryList_Row->id;?>"><?php echo $CategoryTitle;?></option>
													<?php } } ?>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_parent_id');?></label>
												<select name="parent_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
													<?php
													$acc_treasury = intval($this->session->userdata('acc_treasury'));
													$AccountList = $this->M_fin_treeaccount->GetMultiRow();
													foreach($AccountList as $AccountList_Row) {
														$AccountID = $AccountList_Row->id;
														if($acc_treasury == $AccountID) {
														if ($this->session->userdata('lang') == "ar")
														{
															$AccountTitle = $AccountList_Row->title;
														}
														else
														{
															$AccountTitle = $AccountList_Row->title_en;
														}
													?>
													<option value="<?php echo $AccountList_Row->id;?>"><?php echo $AccountTitle;?></option>
													<?php
														}
													}
													?>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_account_no');?></label>
												<input type="text" name="account_no" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_title');?></label>
												<input type="text" name="title" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_title_en');?></label>
												<input type="text" name="title_en" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_startamount');?></label>
												<input type="number" min="0" step="0.001" name="startamount" class="form-control" dir="ltr" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<?php
													$company_id = intval($this->session->userdata('company_id'));
													?>
													<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
													<input type="hidden" name="basic" value="0">
													<input type="hidden" name="level_no" value="0">
													<input type="hidden" name="deleted" value="0">
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
										echo form_close();
									?>
							</div>
								