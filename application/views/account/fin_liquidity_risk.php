<figure class="highcharts-figure">
    <div id="container"></div>
<?php
$TotalRevenue = 0;
$TotalExpenses = 0;

foreach($Revenue as $Revenue_Row) {
	$TotalRevenue += doubleval($Revenue_Row->amount);
}

foreach($Expenses as $Expenses_Row) {
	$TotalExpenses += doubleval($Expenses_Row->amount);
}
?>
    <table id="datatable">
        <thead>
            <tr>
                <th></th>
                <th><?php echo lang('fin_expected_revenue');?></th>
                <th><?php echo lang('fin_expected_expenses');?></th>
            </tr>
        </thead>
        <tbody>
			<?php

			?>
            <tr>
                <th><?php echo lang('Pointer');?></th>
                <td><?php echo $TotalRevenue;?></td>
                <td><?php echo $TotalExpenses;?></td>
            </tr>
            <?php

			?>
        </tbody>
    </table>
</figure>