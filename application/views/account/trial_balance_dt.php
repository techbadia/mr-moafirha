
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <div class="form-body">
        <!-- <button name="add" id="submitbtn" class="btn btn-success btn-xs add pull-right" style="float:right"> Add More</button> -->
 <form method="post" action="" id="formPost">
        <div class="form-group row">

            <div class="col-lg-3 col-md-3 col-sm-12">
                <label class="col-form-label"><?php echo lang('from_date'); ?></label>
                <input type="date" name="fromDT" id="fromDT" class="form-control" value="<?php echo date('Y-m-d'); ?>" required>
            </div>
            
            
            <div class="col-lg-3 col-md-3 col-sm-12">
                <label class="col-form-label"><?php echo lang('to_date'); ?></label>
                <input type="date"  name="toDT" id="toDT" class="form-control" value="<?php echo date('Y-m-d'); ?>" required>
            </div>
            
           
      
        </div>
         </form>
    </div>
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            <div class="row">
                <div class="col-lg-12 ml-lg-auto">

                   
                    <button type="button" id="btnPdf" class="btn btn-primary mr-2" accesskey="s"
                            >PDF <i class="fas fa-file-pdf"></i></button>
                            
                    <button type="button" id="btnExcel" class="btn btn-primary mr-2" accesskey="s"
                            >Excel <i class="fas fa-file-excel"></i></button>
                            
                            
                </div>
            </div>
        </div>
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
    $(document).on('click','#btnPdf',function(data ,callbak){

        var start= new Date($('#fromDT').val());
        var end= new Date($('#toDT').val());
            if (start < end) {
            var frm=$('#formPost');
            frm.attr('action', '<?=base_url("account/Balance_pdf")?>');
            frm.submit();

            }else{
            alert("تحقق من التاريخ");
            
            }
    

    });
$(document).on('click','#btnExcel',function(data ,callbak){

        var start= new Date($('#fromDT').val());
        var end= new Date($('#toDT').val());
            if (start < end) {
            var frm=$('#formPost');
            frm.attr('action', '<?=base_url("account/Balance_excel")?>');
            frm.submit();

            }else{
            alert("تحقق من التاريخ");
            
            }
    

    });

</script>        

</div>