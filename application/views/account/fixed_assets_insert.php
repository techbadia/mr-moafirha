
								<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
									<?php
										$FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
										echo form_open_multipart($FormPath);
									?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('Branches');?></label>
												<select name="company_id" class="form-control">
													<?php
													$CompaniesList = $this->M_app_company->GetMultiRow();
													foreach($CompaniesList as $CompaniesList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$Title = $CompaniesList_Row->ar_title;
														}
														else
														{
															$Title = $CompaniesList_Row->en_title;
														}
													?>
													<option <?php if($company_id == $CompaniesList_Row->id) {?>selected<?php } ?> value="<?php echo $CompaniesList_Row->id;?>"><?php echo $Title;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_account_no');?></label>
												<input type="text" name="account_no" class="form-control" value="" required>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_title');?></label>
												<input type="text" name="title" class="form-control" value="" required>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_title_en');?></label>
												<input type="text" name="title_en" class="form-control" value="" required>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_purchasing_price');?></label>
												<input type="number" min="0" step="0.001" name="startamount" class="form-control" dir="ltr" required>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_thedate');?></label>
												<input type="date" name="thedate" class="form-control" value="<?php echo date("Y-m-d");?>" required>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_depreciation');?></label>
												<input type="number" min="0" step="0.001" name="depreciation" class="form-control" required>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_image');?></label>
												<input type="file" name="image" class="form-control" dir="ltr">
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<input type="hidden" name="basic" value="0">
													<input type="hidden" name="level_no" value="3">
													<input type="hidden" name="deleted" value="0">
													<input type="hidden" name="category_id" value="1">
													<?php
													$acc_fixed_assets = intval($this->session->userdata('acc_fixed_assets'));
													?>
													<input type="hidden" name="parent_id" value="<?php echo $acc_fixed_assets;?>">
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
										echo form_close();
									?>
							</div>
								