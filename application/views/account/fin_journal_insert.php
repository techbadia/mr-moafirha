<style media="screen">
.optionGroup {
  font-weight: bold;
  font-style: italic;
}

.optionChild {
  padding-left: 15px;
}
</style>
<?php
// echo "helo"; die;

$AccountList = $this->M_fin_treeaccount->GetMultiRow2();
$output = '';

foreach ($AccountList as $AccountList_Row) {

    if ($this->session->userdata('lang') == "ar") {
        $AccountTitle = $AccountList_Row->title;
    } else {
        $AccountTitle = $AccountList_Row->title_en;
    }
    $output .= "<option value='.$AccountList_Row->id.'>" . $AccountList_Row->account_no . ' - ' .$AccountTitle . "</option>";
}

?>
<?php
foreach($centers as $center) {
  if ($this->session->userdata('lang') == "ar")
  {
    $ToTitle = $center->name_ar;
  }
  else
  {
    $ToTitle = $center->name_en;
  }
  $centersOut .= "<option value='main_.$center->id.'>" . $ToTitle . "</option>";

?>
  <?php foreach ($center->subs as $sub) {
    if ($this->session->userdata('lang') == "ar")
    {
      $subTitle = $sub->name_ar;
    }
    else
    {
      $subTitle = $sub->name_en;
    }
    $centersOut .= "<option value='sub_.$sub->id.'>  --" . $subTitle . "</option>";

   ?>
<?php } ?>
<?php } ?>
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <?php
    $attributes = array('id' => 'kt_form_2', 'name' => 'kt_form_2');
    $FormPath = base_url() . $Segment1 . "/" . $Segment2 . "/insert_data";
    echo form_open_multipart($FormPath, $attributes);
    ?>
    <div class="form-body">
        <div class="form-group row ">
            <div class="col-lg-3 col-md-3 col-sm-12 bg-primary text-white">
                <label class="col-form-label"><?php echo lang('fin_journal_debit'); ?></label>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 bg-primary text-white">
                <label class="col-form-label"><?php echo lang('fin_journal_account_id'); ?></label>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 bg-primary text-white">
                <label class="col-form-label"><?php echo lang('sub_center_id'); ?></label>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 bg-primary text-white">
              <span class="svg-icon svg-icon-md add" style="float:right;background-color:white;margin-top:8px" id="submitbtndept">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                       width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24"/>
                          <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
                          <path d="M11,11 L11,7 C11,6.44771525 11.4477153,6 12,6 C12.5522847,6 13,6.44771525 13,7 L13,11 L17,11 C17.5522847,11 18,11.4477153 18,12 C18,12.5522847 17.5522847,13 17,13 L13,13 L13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 L11,13 L7,13 C6.44771525,13 6,12.5522847 6,12 C6,11.4477153 6.44771525,11 7,11 L11,11 Z"
                                fill="#000000"/>
                      </g>
                  </svg>
              </span>
            </div>

        </div>


        <!-- <button name="add" id="submitbtn" class="btn btn-success btn-xs add pull-right" style="float:right"> Add More</button> -->

        <div class="form-group row append-row-dept">
            <?php
            $BackgroundColor = "p-3 mb-2 bg-success-o-40";
            $InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
            for ($i = 1; $i <= $InvoiceItems; $i++) {
                if ($BackgroundColor == "p-3 mb-2 bg-success-o-40") {
                    $BackgroundColor = "p-3 mb-2 bg-success-o-20";
                } else {
                    $BackgroundColor = "p-3 mb-2 bg-success-o-40";
                }
                ?>
                <div class="col-lg-4 col-md-4 col-sm-12 <?php echo $BackgroundColor; ?>" lang="en">

                    <input type="number" step="0.001" min="0" name="debit<?php echo $i ?>" id="debit<?php echo $i ?>"
                           class="form-control" onfocus="TotalDebitValue()" onblur="TotalDebitValue()" value="0">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 position-relative <?php echo $BackgroundColor; ?>" lang="ar">
                    <select name="fromaccount<?php echo $i ?>" class="form-control kt-selectpicker" data-size="5"
                            data-live-search="true" id="fromaccount<?php echo $i ?>" required>
                            <option value="">-- إختر --</option>
                        <?php
                        $AccountList = $this->M_fin_treeaccount->GetMultiRow2();
                        foreach ($AccountList as $AccountList_Row) {
                            if ($this->session->userdata('lang') == "ar") {
                                $AccountTitle = $AccountList_Row->title;
                            } else {
                                $AccountTitle = $AccountList_Row->title_en;
                            }
                            ?>
                            <option value="<?php echo $AccountList_Row->id; ?>"><?php echo $AccountList_Row->account_no . ' - ' .$AccountTitle; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">


          				<select name="sub_center_id<?php echo $i ?>" class="form-control select2">
                    <option  value=""><?php echo lang('Choose');?></option>

          					<?php
          					foreach($centers as $center) {
          						if ($this->session->userdata('lang') == "ar")
          						{
          							$ToTitle = $center->name_ar;
          						}
          						else
          						{
          							$ToTitle = $center->name_en;
          						}
          					?>
                      <option class="optionGroup" value="main_<?php echo $center->id;?>"><?php echo $ToTitle;?></option>
          						<?php foreach ($center->subs as $sub) {
          							if ($this->session->userdata('lang') == "ar")
          							{
          								$subTitle = $sub->name_ar;
          							}
          							else
          							{
          								$subTitle = $sub->name_en;
          							}
          						 ?>
          					<option class="optionChild" value="sub_<?php echo $sub->id;?>">-- <?php echo $subTitle;?></option>
          					<?php } ?>
          				<?php } ?>
          				</select>
          			</div>


                <div class="col-lg-12 col-md-12 col-sm-12">
                    <hr>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 <?php echo $BackgroundColor; ?>"  lang="en">
                    <input type="text" name="details<?php echo $i ?>" id="details<?php echo $i ?>" class="select_ form-control"
                           value="" placeholder="<?php echo lang('fin_journal_details'); ?>">
                </div>
            <?php } ?>
        </div>
        <div class="form-group row ">
            <div class="col-lg-3 col-md-3 col-sm-12 bg-primary text-white">
                <label class="col-form-label"><?php echo lang('fin_journal_creditor'); ?></label>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 bg-primary text-white">
                <label class="col-form-label"><?php echo lang('fin_journal_account_id'); ?></label>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 bg-primary text-white">
                <label class="col-form-label"><?php echo lang('sub_center_id'); ?></label>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 bg-primary text-white">
              <span class="svg-icon svg-icon-md add" style="float:right;background-color:white;margin-top:8px" id="submitbtncred">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                       width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24"/>
                          <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
                          <path d="M11,11 L11,7 C11,6.44771525 11.4477153,6 12,6 C12.5522847,6 13,6.44771525 13,7 L13,11 L17,11 C17.5522847,11 18,11.4477153 18,12 C18,12.5522847 17.5522847,13 17,13 L13,13 L13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 L11,13 L7,13 C6.44771525,13 6,12.5522847 6,12 C6,11.4477153 6.44771525,11 7,11 L11,11 Z"
                                fill="#000000"/>
                      </g>
                  </svg>
              </span>
            </div>
        </div>
        <div class="form-group row append-row-cred">
            <?php
            $BackgroundColor = "p-3 mb-2 bg-success-o-40";
            $InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
            for ($i = 1; $i <= $InvoiceItems; $i++) {
                if ($BackgroundColor == "p-3 mb-2 bg-success-o-40") {
                    $BackgroundColor = "p-3 mb-2 bg-success-o-20";
                } else {
                    $BackgroundColor = "p-3 mb-2 bg-success-o-40";
                }
                ?>
                <div class="col-lg-4 col-md-4 col-sm-12 <?php echo $BackgroundColor; ?>" lang="ar">
                    <input type="number" step="0.001" min="0" name="creditor<?php echo $i ?>"
                           id="creditor<?php echo $i ?>" class="form-control" onfocus="TotalCreditorValue()"
                           onblur="TotalCreditorValue()" value="0">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 <?php echo $BackgroundColor; ?>" lang="en">
                    <select name="toaccount<?php echo $i ?>" class="form-control kt-selectpicker" data-size="5"
                            data-live-search="true" required>
                            <option value="">-- إختر --</option>
                        <?php
                        $AccountList = $this->M_fin_treeaccount->GetMultiRow2();
                        foreach ($AccountList as $AccountList_Row) {
                            if ($this->session->userdata('lang') == "ar") {
                                $AccountTitle = $AccountList_Row->title;
                            } else {
                                $AccountTitle = $AccountList_Row->title_en;
                            }
                            ?>
                            <option value="<?php echo $AccountList_Row->id; ?>"><?php echo $AccountList_Row->account_no . ' - ' .$AccountTitle; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">


          				<select name="sub_center_cred_id<?php echo $i ?>" class="form-control select2">
                    <option  value=""><?php echo lang('Choose');?></option>

          					<?php
          					foreach($centers as $center) {
          						if ($this->session->userdata('lang') == "ar")
          						{
          							$ToTitle = $center->name_ar;
          						}
          						else
          						{
          							$ToTitle = $center->name_en;
          						}
          					?>
                    <option class="optionGroup" value="main_<?php echo $center->id;?>"><?php echo $ToTitle;?></option>
          						<?php foreach ($center->subs as $sub) {
          							if ($this->session->userdata('lang') == "ar")
          							{
          								$subTitle = $sub->name_ar;
          							}
          							else
          							{
          								$subTitle = $sub->name_en;
          							}
          						 ?>
          					<option class="optionChild" value="sub_<?php echo $sub->id;?>">-- <?php echo $subTitle;?></option>
          					<?php } ?>
          				<?php } ?>
          				</select>
          			</div>


                <div class="col-lg-12 col-md-12 col-sm-12">
                    <hr>
                </div>
            <?php } ?>
        </div>




        <div class="form-group row">

            <div class="col-lg-3 col-md-3 col-sm-12">
                <label class="col-form-label"><?php echo lang('fin_journal_debit_total'); ?></label>
                <input type="number" step="0.001" min="0" name="debit_total" id="debit_total" class="form-control"
                       value="0">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12"></div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <label class="col-form-label"><?php echo lang('fin_journal_creditor_total'); ?></label>
                <input type="number" step="0.001" min="0" name="creditor_total" id="creditor_total" class="form-control"
                       value="0">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12"></div>
        </div>
        <div class="form-group row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <label class="col-form-label"><?php echo lang('fin_journal_details'); ?></label>
                <input type="text" name="details" id="details" class="select_ form-control" value="">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <label class="col-form-label"><?php echo lang('fin_journal_thedate'); ?></label>
                <input type="date" name="thedate" class="form-control" value="<?php echo date('Y-m-d'); ?>" required>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12">
                <label class="col-form-label"><?php echo lang('fin_journal_image'); ?></label>
                <input type="file" name="image" id="image" class="form-control" dir="ltr">
            </div>
        </div>
    </div>
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            <div class="row">
                <div class="col-lg-12 ml-lg-auto">
                    <input type="hidden" name="id" value="<?php //echo $id;?>">
                    <?php
                    $InvoiceItemsd = (!empty($this->session->userdata('acc_invoice_items_de')))?intval($this->session->userdata('acc_invoice_items_de')):1;
                    $InvoiceItemsc = (!empty($this->session->userdata('acc_invoice_items_cr')))?intval($this->session->userdata('acc_invoice_items_cr')):1;
                    ?>
                    <input type="hidden" name="acc_invoice_items_de" id="acc_invoice_items_de"
                           value="<?php echo $InvoiceItemsd; ?>">
                    <input type="hidden" name="acc_invoice_items_cr" id="acc_invoice_items_cr"
                           value="<?php echo $InvoiceItemsc; ?>">
                    <input type="hidden" name="deleted" value="0">
                    <a href="<?php echo base_url() . $Segment1 . "/" . $Segment2; ?>" class="btn btn-secondary"
                       accesskey="b"><?php echo $this->lang->line('Back'); ?></a>
                    <button type="submit" class="btn btn-primary mr-2" accesskey="s"
                            onclick="return Check_Debit_Creditor()"><?php echo $this->lang->line('Save'); ?></button>
                </div>
            </div>
        </div>
    </div>
    <?php


    echo form_close();
    ?>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


    <script>

        $(document).ready(function () {
            var count = <?php echo $InvoiceItems?>;
            $(document).on("keydown", ".select_", function(e){
                if(e.which == 13){
                    $("#submitbtndept").trigger('click');
                    return false;
                }
            });
            $(document).on('focus', '.kt-selectpicker', function(){
                $(this).next().trigger('click');
            })
            $(document).on('click', '.deletebtn', function(){
                let btn_id = $(this).data('id');
                console.log(btn_id)
                $(btn_id).remove();
                count = $('.appended_row_dept').length+1;
            })
            $(document).on('click', '#submitbtndept', function () {

                var BackgroundColor = "p-3 mb-2 bg-success-o-40";

                var SelectBox = "<?= $output; ?>";
                var SelectBoxCenter = "<?= $centersOut; ?>";
                var BackgroundColor = "p-3 mb-2 bg-success-o-40";

                if (BackgroundColor == "p-3 mb-2 bg-success-o-40") {
                    BackgroundColor = "p-3 mb-2 bg-success-o-20";
                } else {
                    BackgroundColor = "p-3 mb-2 bg-success-o-40";
                }

                count++;
                var html = '';
                html += `<div class="col-12 row appended_row_dept" id="___${count}"><div class="col-12 p-0 m-2 mx-3"><span class="svg-icon svg-icon-md add deletebtn" style="float:right; margin-top:-5px;" data-id="#___${count}">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
  <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
</svg>
        </span> `;
                html += '</div><div class="col-lg-4 col-md-4 col-sm-12 ' + BackgroundColor + '">';
                html += '<input type="number" step="0.001" min="0" name="debit' + count + '" id="debit' + count + '" class="form-control" onfocus="TotalDebitValue()" onblur="TotalDebitValue()" value="0">';
                html += '</div>';

                html += '<div class="col-lg-4 col-md-4 col-sm-12 ' + BackgroundColor + '" >';
                html += '<select name="fromaccount' + count + '" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required><option value="">-- إختر --</option>' + SelectBox + '"</select>';
                html += '</div>';

                html += '<div class="col-lg-4 col-md-4 col-sm-12 ' + BackgroundColor + '">';
                html += '<select name="sub_center_id' + count + '" class="form-control kt-selectpicker" data-size="5" data-live-search="true"><option value="">-- إختر --</option>' + SelectBoxCenter + '</select>';
                html += '</div>';

                html += '<tr><div class="col-lg-12 col-md-12 col-sm-12 ' + BackgroundColor + '" style="top:10px">';
                html += '<input type="text" step="0.001" min="0" name="details' + count + '" id="details' + count + '" class="form-control select_" onfocus="TotalDebitValue()" onblur="TotalDebitValue()" value="" placeholder="<?php echo lang('fin_journal_details');?>">';
                html += '</div></tr>';

                html += '<div class="col-lg-12 col-md-12 col-sm-12">';
                html += '<hr>';
                html += '</div>';
                html += `</div>`;


                // html += '<td><input type="text" name="item_name[]" class="form-control item_name" /></td>';
                // html += '<td><select name="item_category[]" class="form-control item_category" data-sub_category_id="'+count+'"><option value="">Select Category</option></select></td>';
                // html += '<td><select name="item_sub_category[]" class="form-control item_sub_category" id="item_sub_category'+count+'"><option value="">Select Sub Category</option></select></td>';
                // html += '<td><button type="button" name="remove" class="btn btn-danger btn-xs remove"><span class="glyphicon glyphicon-minus"></span></button></td>';
                $('#acc_invoice_items_de').val(count);
                $('.append-row-dept').append(html);
                $('#debit' + count).trigger('focus')
                $('.kt-selectpicker').selectpicker();
            });

            $(document).on('click', '.remove', function () {
                $(this).closest('tr').remove();
            });

        });
    </script>
    <script>

        $(document).ready(function () {
            var count = <?php echo $InvoiceItems?>;
            $(document).on("keydown", ".select_", function(e){
                if(e.which == 13){
                    $("#submitbtncred").trigger('click');
                    return false;
                }
            });
            $(document).on('focus', '.kt-selectpicker', function(){
                $(this).next().trigger('click');
            })
            $(document).on('click', '.deletebtn', function(){
                let btn_id = $(this).data('id');
                console.log(btn_id)
                $(btn_id).remove();
                count = $('.appended_row_cred').length+1;
            })
            $(document).on('click', '#submitbtncred', function () {

                var BackgroundColor = "p-3 mb-2 bg-success-o-40";

                var SelectBox = "<?= $output; ?>";
                var SelectBoxCenter = "<?= $centersOut; ?>";

                var BackgroundColor = "p-3 mb-2 bg-success-o-40";

                if (BackgroundColor == "p-3 mb-2 bg-success-o-40") {
                    BackgroundColor = "p-3 mb-2 bg-success-o-20";
                } else {
                    BackgroundColor = "p-3 mb-2 bg-success-o-40";
                }

                count++;
                var html = '';
                html += `<div class="col-12 row appended_row_cred" id="___${count}"><div class="col-12 p-0 m-2 mx-3"><span class="svg-icon svg-icon-md add deletebtn" style="float:right; margin-top:-5px;" data-id="#___${count}">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
  <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
</svg>
        </span> `;


                html += '</div><div class="col-lg-4 col-md-4 col-sm-12 ' + BackgroundColor + '">';
                html += '<input type="number" step="0.001" min="0" name="creditor' + count + '" id="creditor' + count + '" class="form-control" onfocus="TotalCreditorValue()" onblur="TotalCreditorValue()" value="0">';
                html += '</div>';


                html += '<div class="col-lg-4 col-md-4 col-sm-12 ' + BackgroundColor + '" >';
                html += '<select name="toaccount' + count + '" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required><option value="">-- إختر --</option>' + SelectBox + '</select>';
                html += '</div>';

                html += '<div class="col-lg-4 col-md-4 col-sm-12 ' + BackgroundColor + '">';
                html += '<select name="sub_center_cred_id' + count + '" class="form-control kt-selectpicker" data-size="5" data-live-search="true"><option value="0">-- إختر --</option>' + SelectBoxCenter + '"</select>';
                html += '</div>';

                html += '<div class="col-lg-12 col-md-12 col-sm-12">';
                html += '<hr>';
                html += '</div>';
                html += `</div>`;


                // html += '<td><input type="text" name="item_name[]" class="form-control item_name" /></td>';
                // html += '<td><select name="item_category[]" class="form-control item_category" data-sub_category_id="'+count+'"><option value="">Select Category</option></select></td>';
                // html += '<td><select name="item_sub_category[]" class="form-control item_sub_category" id="item_sub_category'+count+'"><option value="">Select Sub Category</option></select></td>';
                // html += '<td><button type="button" name="remove" class="btn btn-danger btn-xs remove"><span class="glyphicon glyphicon-minus"></span></button></td>';
                $('#acc_invoice_items_cr').val(count);
                $('.append-row-cred').append(html);
                $('#debit' + count).trigger('focus')
                $('.kt-selectpicker').selectpicker();
            });

            $(document).on('click', '.remove', function () {
                $(this).closest('tr').remove();
            });

        });
    </script>


</div>
