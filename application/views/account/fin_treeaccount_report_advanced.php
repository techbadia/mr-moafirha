<br>
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
	<?php
	$Segment1 = $this->uri->segment(1);
	$Segment2 = $this->uri->segment(2);
	$FormPath = base_url() . $Segment1 . "/" . $Segment2 . "/search_account";
	echo form_open_multipart($FormPath);
	?>
	<div class="row">
		<div class="col-md-3">
			<label class="col-form-label"><?php echo lang('from_date'); ?></label>
			<input type="date" name="fromdate" class="form-control" value="<?php echo $fromdate; ?>" required>
		</div>
		<div class="col-md-3">
			<label class="col-form-label"><?php echo lang('to_date'); ?></label>
			<input type="date" name="todate" class="form-control" value="<?php echo $todate; ?>" required>
		</div>
		<div class="col-md-5">
			<label class="col-form-label"><?php echo lang('fin_journal_account_id'); ?></label>
			<select name="accountid" class="form-control kt-selectpicker" data-size="5" data-live-search="true"
					required>
				<?php

				$AccountList = $this->M_fin_treeaccount->GetMultiRow(false); //GetMain_Sub($main_accountid); //
				foreach ($AccountList as $AccountList_Row) {
					if ($this->session->userdata('lang') == "ar") {
						$FromTitle = $AccountList_Row->title;
					} else {
						$FromTitle = $AccountList_Row->title_en;
					}
					?>
					<option <?php if ($accountid == $AccountList_Row->id) { ?>selected<?php } ?>
							value="<?php echo $AccountList_Row->id; ?>"><?php echo $AccountList_Row->account_no . ' - ' . $FromTitle; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-1">
			<input type="hidden" id="main_accountid" name="main_accountid" value="<?php echo $main_accountid; ?>"/>
			<input type="hidden" id="Search" name="Search" value="<?php echo $Search; ?>"/>
			<button type="submit" class="btn btn-primary mr-2"
					style="margin-top:35px;"><?php echo $this->lang->line('Search'); ?></button>
		</div>
	</div>
	<?php
	echo form_close();
	?>
</div>

<div id="DivForPrint" <?php echo $this->session->userdata('HTML'); ?>>

	<div>
		<table class="table table-separate table-head-custom table-checkable" id="datatable_Print" style="width:98%">
			<thead>
			<tr>
				<th style="text-align:center"><?php echo $this->lang->line('Serial'); ?></th>
				<th style="text-align:center"><?php echo $this->lang->line('fin_journal_debit'); ?></th>
				<th style="text-align:center"><?php echo $this->lang->line('fin_journal_creditor'); ?></th>
				<th style="text-align:center"><?php echo $this->lang->line('Balance'); ?></th>
				<th><?php echo $this->lang->line('fin_journal_account_id'); ?></th>
				<th><?php echo $this->lang->line('fin_journal_details'); ?></th>
				<th><?php echo $this->lang->line('fin_journal_thedate'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php
			if (isset($accountid) && !empty($accountid)) {
				$AccountList2 = $this->M_fin_treeaccount->GetMain_Sub($accountid);
				foreach ($AccountList2 as $accountRow) {
					if ($this->session->userdata('lang') == "ar") {
						$title = $accountRow->title;
					} else {
						$title = $accountRow->title_en;
					}
					?>
					<tr>
						<td colspan="7" style="text-align: center;">
							<h4 style="margin-bottom:0;"><?php echo lang('Account_statement') . " " . $title; ?></h4>
							<span><?php echo $fromdate . " : " . $todate; ?></span>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<?php
							if (($category_id == 1) || ($category_id == 5)) {
								echo $startamount;
							}
							?>
						</td>
						<td>
							<?php
							if (($category_id == 2) || ($category_id == 3) || ($category_id == 4)) {
								echo $startamount;
							}
							?>
						</td>
						<td style="text-align:center"><strong>
								<?php
								//$StartAmount = 0;
								$BeforDay = date('Y-m-d', strtotime('-1 days', strtotime($fromdate)));
								$AmountBeforeJournal = $startamount;
								$AmountBeforeJournalData = $this->M_fin_journal->GetRealtedJournalStartDate($accountRow->id, $fromdate);
								$AmountBeforeJournalValue = 0;
								$RealtedJournalStartDateCount = $this->M_fin_journal->GetRealtedJournalStartDateCount($accountRow->id, $fromdate);
								$AccountCategoryID = 0;
								if ($RealtedJournalStartDateCount > 0) {
									foreach ($AmountBeforeJournalData as $AmountBeforeJournalData_Row) {
										//$AmountBeforeJournalValue = doubleval($AmountBeforeJournalData_Row->thevalue);
										$AccountData = $this->M_fin_treeaccount->GetRow($accountRow->id);
										$AccountCategoryID = $AccountData->category_id;
										if (($AccountCategoryID == 1) || ($AccountCategoryID == 4)) {
										} else {
											$AmountBeforeJournal -= doubleval($AmountBeforeJournalData_Row->debit);
											$AmountBeforeJournal += doubleval($AmountBeforeJournalData_Row->creditor);
										}
									}
								}
								echo $AmountBeforeJournal;
								$Balance = $AmountBeforeJournal;
								?></strong>
						</td>
						<td></td>
						<td><?php echo "الرصيد السابق"; ?></td>
						<td></td>
					</tr>
					<?php
					$TotalValue = 0;
					$Serial = 0;
					$Debit = 0;
					$DebitTotal = 0;
					$CreditorTotal = 0;
					$Creditor = 0;
					$CategoryID = 0;
					$TheValue = 0;
					$OtherAccountName = "";
					//$Balance = $StartAmount;
					//$AccountCategoryID = 0;
					$MultiRows = $this->M_fin_journal_main->GetAccountAction_RangDate($accountRow->id, $fromdate, $todate);
					foreach ($MultiRows as $MultiRows_Row) {
						$Serial += 1;
						if ($this->session->userdata('lang') == "ar") {
							$AccountTitle = $MultiRows_Row->title;
						} else {
							$AccountTitle = $MultiRows_Row->title_en;
						}

						$AccountName = $this->M_fin_treeaccount->GetRow($accountRow->id);
						$category_id = $AccountName->category_id;
						?>

						<tr>
							<td style="text-align:center"><?php echo $Serial; ?></td>
							<td style="text-align:center">
								<?php
								echo doubleval($MultiRows_Row->debit);
								$Debit += doubleval($MultiRows_Row->debit);
								$DebitTotal += $Debit;
								?>
							</td>
							<td style="text-align:center">
								<?php
								echo doubleval($MultiRows_Row->creditor);
								$Creditor += doubleval($MultiRows_Row->creditor);
								$CreditorTotal += $Creditor;
								?>
							</td>
								<td style="text-align:center"><?=$Balance+$MultiRows_Row->debit-$MultiRows_Row->creditor?>
							</td>
							<td>
								<?php

								echo $AccountTitle;
								?>
							</td>
							<td><?php echo $MultiRows_Row->details; ?></td>
							<td><a  target="_blank"  href="<?=base_url('account/fin_journal/view_pdf/'.$MultiRows_Row->main_id.'/1')?>"><i class="fa fa-print fa-2x"></i></a></td>
							<td><?php echo $MultiRows_Row->thedate; ?></td>
						</tr>
						<?php
						$CategoryID = 0;
						$TheValue = 0;
						if (($category_id == 1) || ($category_id == 5)) {

							$Balance = $Balance + $MultiRows_Row->debit - $MultiRows_Row->creditor;
						} else {
							$Balance = $Balance + $MultiRows_Row->creditor - $MultiRows_Row->debit;
						}
						//echo $this->Converter->ConvertToArabic($Balance);
					}
					if ($accountid == 291 && $accountid == $accountRow->id) {
					    
					    $Creditor_m =0;
					    $Debit_m=0;
					    $Balance_m=0;
					    foreach ($AccountList2 as $accountRow_m) {
					        	$MultiRows_m = $this->M_fin_journal_main->GetAccountAction_RangDate($accountRow_m->id, $fromdate, $todate);
            					foreach ($MultiRows_m as $MultiRows_Row_m) {
            					        $Creditor_m +=$MultiRows_Row_m->creditor;
                					    $Debit_m +=$MultiRows_Row_m->debit;
                					    
            					    }
					    
					    }
					     $Balance_m= $Debit_m - $Creditor_m;
						?>
						<tr>

							<th></th>
							<th style="text-align:center">
								<?php
								$Final = 0;
								if (($AccountCategoryID == 1) || ($AccountCategoryID == 4)) {
									$Final = $startamount + $Debit - $Creditor;
									//echo $Debit;
								}
								echo doubleval($Debit_m);
								?>
							</th>
							<th style="text-align:center">
								<?php
								$Final = 0;
								if (($AccountCategoryID == 2) || ($AccountCategoryID == 3) || ($AccountCategoryID == 5)) {
									$Final = $startamount - $Debit + $Creditor;
									//echo $Creditor;
								}
								echo doubleval($Creditor_m);
								?>
							</th>
							<th style="text-align:center">
								<?php
								echo doubleval($Balance_m);
								?>
							</th>
							<th></th>
							<th><?php echo "الرصيد الختامي"; ?></th>
							<th></th>
						</tr>
						<!-- </div> -->
						<?php
					}else{
						?>
						<tr>

							<th></th>
							<th style="text-align:center">
								<?php
								$Final = 0;
								if (($AccountCategoryID == 1) || ($AccountCategoryID == 4)) {
									$Final = $startamount + $Debit - $Creditor;
									//echo $Debit;
								}
								echo doubleval($Debit);
								?>
							</th>
							<th style="text-align:center">
								<?php
								$Final = 0;
								if (($AccountCategoryID == 2) || ($AccountCategoryID == 3) || ($AccountCategoryID == 5)) {
									$Final = $startamount - $Debit + $Creditor;
									//echo $Creditor;
								}
								echo doubleval($Creditor);
								?>
							</th>
							<th style="text-align:center">
								<?php
								echo doubleval($Balance);
								?>
							</th>
							<th></th>
							<th><?php echo "الرصيد الختامي"; ?></th>
							<th></th>
						</tr>
						<!-- </div> -->
						<?php
					}
				}
			}
			?>
			</tbody>
		</table>
	</div>
</div>
<div class="d-flex justify-content-between">
	<button type="button" class="btn btn-primary font-weight-bold"
			onclick="printDiv();"><?php echo $this->lang->line('Print'); ?></button>
</div>
