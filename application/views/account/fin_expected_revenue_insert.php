
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
                        echo form_open_multipart($FormPath);
                    ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_expected_revenue_account_id');?></label>
												<select name="account_id" class="form-control" required>
													<?php
													$AccountsList = $this->M_fin_treeaccount->GetByCategory(4);
													foreach($AccountsList as $AccountsList_Row) {
														if($this->session->userdata('lang') == "ar")
														{
															$AccountTitle = $AccountsList_Row->title;
														}
														else
														{
															$AccountTitle = $AccountsList_Row->title_en;
														}
														$parent_id = $AccountsList_Row->parent_id;
														if($parent_id > 0) {
													?>
													<option value="<?php echo $AccountsList_Row->id;?>"><?php echo $AccountTitle;?></option>
													<?php
														}
													}
													?>
												</select>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_expected_revenue_amount');?></label>
												<input type="number" name="amount" class="form-control" value="0" min="1" step="1" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<?php
													$company_id = intval($this->session->userdata('company_id'));
													?>
													<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
													<input type="hidden" name="deleted" value="0">

													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>