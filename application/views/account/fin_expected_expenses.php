
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('fin_expected_expenses_account_id');?></th>
												<th><?php echo lang('fin_expected_expenses_amount');?></th>
												<th><?php echo lang('fin_expected_expenses_actual');?></th>
												<th><?php echo lang('store_active');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$store_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
												<?php
												$account_id = $DataRows_Row->account_id;
												$AccountData = $this->M_fin_treeaccount->GetRow($account_id);
												if($this->session->userdata('lang') == "ar")
												{
													echo $AccountData->title;
												}
												else
												{
													echo $AccountData->title_en;
												}
												?>
												</td>
												<td><?php echo $DataRows_Row->amount;?></td>
												<td>
													<?php //echo $DataRows_Row->actual;?>
												</td>
												<td>
												<?php
												if($DataRows_Row->deleted == 0)
												{
													echo lang('Yes');
												}
												else
												{
													echo lang('No');
												}
												?>
												</td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

								