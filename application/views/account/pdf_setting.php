<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
            <?php echo lang('Edit')." ".lang('PageHeader');?>
            </h3>
        </div>
    </div>
    <form class="kt-form kt-form--label-right" method="post" action="<?php echo base_url();?>fin_settings/Update_header" enctype="multipart/form-data">
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <label class="col-form-label"><?php echo lang('ChangeImage');?></label>
                    <input type="file" name="image" class="form-control" dir="ltr">
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="company_id" value="<?php echo $company_id;?>">
                        <button type="submit" class="btn btn-success btn-custom" id="kt_sweetalert_demo_3_3"><?php echo lang('Save');?></button>
                        <button type="reset" class="btn btn-dark btn-custom"><?php echo lang('Reset');?></button>
                        <a href="<?php echo $this->uri->segment(1);?>" class="btn btn-warning btn-custom"><?php echo lang('Back');?></a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
            <?php echo lang('Edit')." ".lang('PageFooter');?>
            </h3>
        </div>
    </div>
    <form class="kt-form kt-form--label-right" method="post" action="<?php echo base_url();?>fin_settings/Update_footer" enctype="multipart/form-data">
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <label class="col-form-label"><?php echo lang('ChangeImage');?></label>
                    <input type="file" name="image" class="form-control" dir="ltr">
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="company_id" value="<?php echo $company_id;?>">
                        <button type="submit" class="btn btn-success btn-custom" id="kt_sweetalert_demo_3_3"><?php echo lang('Save');?></button>
                        <button type="reset" class="btn btn-dark btn-custom"><?php echo lang('Reset');?></button>
                        <a href="<?php echo $this->uri->segment(1);?>" class="btn btn-warning btn-custom"><?php echo lang('Back');?></a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>