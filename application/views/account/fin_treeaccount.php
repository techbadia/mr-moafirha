<?php
$Segment2 = $this->uri->segment(2);
$Segment3 = $this->uri->segment(3);
$group_id = intval($this->session->userdata('GroupID'));
$PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
$page_id = $PageData->id;
 ?>
<br>
	<div class="d-flex align-items-center p-4 bg-dark" style="text-align:center !important">
		<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-transparent-success font-weight-bold mr-2">
			<?php echo lang('fin_treecategory_all');?>
		</a>
		<a href="<?php echo base_url().$Segment1."/".$Segment2."/category/1";?>" class="btn btn-transparent-success font-weight-bold mr-2">
			<?php echo lang('fin_treecategory_1');?>
		</a>
		<a href="<?php echo base_url().$Segment1."/".$Segment2."/category/2";?>" class="btn btn-transparent-success font-weight-bold mr-2">
			<?php echo lang('fin_treecategory_2');?>
		</a>
		<a href="<?php echo base_url().$Segment1."/".$Segment2."/category/3";?>" class="btn btn-transparent-success font-weight-bold mr-2">
			<?php echo lang('fin_treecategory_3');?>
		</a>
		<a href="<?php echo base_url().$Segment1."/".$Segment2."/category/4";?>" class="btn btn-transparent-success font-weight-bold mr-2">
			<?php echo lang('fin_treecategory_4');?>
		</a>
		<a href="<?php echo base_url().$Segment1."/".$Segment2."/category/5";?>" class="btn btn-transparent-success font-weight-bold mr-2">
			<?php echo lang('fin_treecategory_5');?>
		</a>
		<!--
		<a href="#" class="btn btn-transparent-warning font-weight-bold mr-2">Warning</a>
		<a href="#" class="btn btn-transparent-white font-weight-bold">White</a>
		-->
		<?php
		$CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
		if($CheckDelete != 0)
		{
		 ?>
		<a href="<?php echo base_url().$Segment1."/".$Segment2."/deleted/";?>" class="btn btn-transparent-white font-weight-bold" style="position: absolute !important; <?php echo $AnotherAlign;?>: 25px !important;">
			<i class="flaticon-delete"></i> <?php echo lang('Deleted');?>
		</a>
	<?php } ?>
	</div>
<br>
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('fin_treeaccount_category_id');?></th>
												<th><?php echo lang('fin_treeaccount_parent_id');?></th>
												<th><?php echo lang('fin_treeaccount_account_no');?></th>
												<th><?php echo lang('fin_treeaccount_level_no');?></th>
												<th><?php echo lang('fin_treeaccount_title');?></th>
												<th><?php echo lang('fin_treeaccount_title_en');?></th>
												<th><?php echo lang('fin_treeaccount_startamount');?></th>
												<?php

												$CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
												// var_dump($CheckEdit);die();

												if($CheckEdit != 0)
												{
												 ?>

												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
											<?php }
											$CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
											if($CheckDelete != 0)
											{
											?>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											<?php } ?>
										<?php
										$CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
										if($CheckView != 0)
										{
										?>
												<th style="text-align:center"><?php echo $this->lang->line('View');?></th>
											<?php } ?>

											</tr>
										</thead>
										<tbody>
											<?php
											$ids = [];
											foreach($DataRows as $DataRows_Row) {
												$fin_treeaccount_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
													<?php
													$category_id = $DataRows_Row->category_id;
													$CategoryData = $this->M_fin_treecategory->GetRow($category_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $CategoryData->title;
													}
													else
													{
														echo $CategoryData->title_en;
													}
													?>
												</td>
												<td>
													<?php
													$parent_id = $DataRows_Row->parent_id;
													if($parent_id > 0)
													{
														$ParentData = $this->M_fin_treeaccount->GetRow($parent_id);
														if ($this->session->userdata('lang') == "ar")
														{
															echo $ParentData->title;
														}
														else
														{
															echo $ParentData->title_en;
														}
													}
													?>
												</td>
												<td><?php echo $DataRows_Row->account_no;?></td>
												<td><?php echo $DataRows_Row->level_no;?></td>
												<td><?php echo $DataRows_Row->title;?></td>
												<td><?php echo $DataRows_Row->title_en;?></td>
												<td><?php echo $DataRows_Row->startamount;?></td>
												<?php
												$CheckEdit = $this->M_usr_usersprivileges->CheckEdit($group_id, $page_id);
												// var_dump($CheckEdit);die();

												if($CheckEdit != 0)
												{
													?>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i>
													</a>
												</td>
											<?php }
?>

<?php											$CheckDelete = $this->M_usr_usersprivileges->CheckDelete($group_id, $page_id);
if($CheckDelete != 0)
{
	?>
<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i>
													</a>
												<?php } ?>
												</td>

											<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted != 0) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i>
														</a>
													<?php } ?>
												</td>
											<?php } ?>
											<?php
											$CheckView = $this->M_usr_usersprivileges->CheckView($group_id, $page_id);
											if($CheckView != 0)
											{
												?>
											<td style="text-align:center">

													<a href="<?php echo base_url()."/account/fin_treeaccount/view_account/".$DataRows_Row->id;?>" target="_lank">
														<i class="flaticon-search text-success icon-lg"></i>
													</a>
										</td>
									<?php } ?>

											</tr>
											<?php
											}
											?>
										</tbody>

									</table>
