<?php
global $PageName;
global $PageDetails;

if(($Segment1 == "account") && ($Segment2 == "reports"))
{
  $PageName = $this->session->userdata('ReportName');
  $PageDetails = $this->lang->line('ShortCut_Reports');
}
else if($Segment1 == "branche")
{
  $PageName = $this->lang->line('Branches');
  $PageDetails = $this->lang->line('Branches');
}
else if(($Segment1 == "mydata") && ($Segment2 == "repair"))
{
  $PageName = $this->lang->line('Databse_Repair');
  $PageDetails = $this->lang->line('Databse_Repair');
}
else if(($Segment1 == "mydata") && ($Segment2 == "deleterows"))
{
  $PageName = $this->lang->line('DataDelete');
  $PageDetails = $this->lang->line('DataDelete');
}
else if(($Segment1 == "mydata") && ($Segment2 == "optimize"))
{
  $PageName = $this->lang->line('Databse_Optimize');
  $PageDetails = $this->lang->line('Databse_Optimize');
}
else if(($Segment1 == "mydata") && ($Segment2 == "backup"))
{
  $PageName = $this->lang->line('DatabaseBackeup');
  $PageDetails = $this->lang->line('DatabaseBackeup');
}
else if(($Segment1 == "pos_sys") && ($Segment2 == "tec_stores"))
{
  $PageName = $this->lang->line('POS_Management');
  $PageDetails = $this->lang->line('POS_Management_Details');
}
else if(($Segment1 == "home") && ($Segment2 == "push_messages"))
{
  $PageName = $this->lang->line('Send_Message_To_Customers');
  $PageDetails = $this->lang->line('Send_Message_To_Customers');
}
else if(($Segment1 == "home") && ($Segment2 == "push_notifications"))
{
  $PageName = $this->lang->line('Send_Notification_To_Customers');
  $PageDetails = $this->lang->line('Send_Notification_To_Customers');
}
else
{
  $PageData = $this->M_app_module_page->GetPageDataByClass($Segment2);
  $PageName = "";
  if ($this->session->userdata('lang') == "ar")
  {
      $PageName = $PageData->ar_title;
      $PageDetails = $PageData->ar_details;
  }
  else
  {
      $PageName = $PageData->en_title;
      $PageDetails = $PageData->en_details;
  }
}
?>