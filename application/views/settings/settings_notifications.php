<div class="card card-custom gutter-b example example-compact">

<div class="col-lg-12">
    <ul class="nav nav-tabs nav-tabs-line">
        <?php
        $FirstTab = "";
        $FirstTabNo = 0;
        $PackageModules = $this->M_app_package_module->GetMainModules($PackageID);
        foreach($PackageModules as $PackageModules_Row) {
            $FirstTabNo += 1;
            if($FirstTabNo == 3)
            {
                $FirstTab = "nav-link active";
            }
            else
            {
                $FirstTab = "nav-link";
            }
            $app_package_module_id = $PackageModules_Row->id;
            $module_id = $PackageModules_Row->module_id;
            $ModuleData = $this->M_app_module->GetRow($module_id);
            $ModuleTitle = "";
            if ($this->session->userdata('lang') == "ar")
            {
                $ModuleTitle = $PackageModules_Row->ar_title;
            }
            else
            {
                $ModuleTitle = $PackageModules_Row->en_title;
            }
            if($module_id > 2) {
        ?>
        <li class="nav-item">
            <a class="<?php echo $FirstTab;?>" data-toggle="tab" href="#kt_tab_pane_<?php echo $app_package_module_id;?>"><?php echo $ModuleTitle;?></a>
        </li>
        <?php
            }
        }
        ?>
    </ul>
    <div class="tab-content mt-5" id="myTabContent">
        <?php
        $FirstTab = "";
        $FirstTabNo = 0;
        $PackageModules = $this->M_app_package_module->GetMainModules($PackageID);
        foreach($PackageModules as $PackageModules_Row) {
            $FirstTabNo += 1;
            if($FirstTabNo == 3)
            {
                $FirstTab = "tab-pane fade show active";
            }
            else
            {
                $FirstTab = "tab-pane fade";
            }
            $app_package_module_id = $PackageModules_Row->id;
            $module_id = $PackageModules_Row->module_id;
            $ModuleData = $this->M_app_module->GetRow($module_id);
            $ModuleTitle = "";
            if ($this->session->userdata('lang') == "ar")
            {
                $ModuleTitle = $ModuleData->ar_title;
            }
            else
            {
                $ModuleTitle = $ModuleData->en_title;
            }
            if($module_id > 2) {
        ?>
        <div class="<?php echo $FirstTab;?>" id="kt_tab_pane_<?php echo $app_package_module_id;?>" role="tabpanel" aria-labelledby="kt_tab_pane_2">
        <?php
        //echo $ModuleTitle;
        //إدارة الحسابات
        if($module_id == 3) {
        ?>
        <?php
            $FormPath = base_url().$Segment1."/".$Segment2."/Update_module3";
            echo form_open_multipart($FormPath);
        ?>
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_fin_option1');?></label>
                    <input type="number" name="fin_option1" class="form-control" min="0.01" step="0.01" value="<?php echo $fin_option1;?>" required>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_fin_option2');?></label>
                    <input type="number" name="fin_option2" class="form-control" min="0.01" step="0.01" value="<?php echo $fin_option2;?>" required>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_fin_option3');?></label>
                    <input type="number" name="fin_option3" class="form-control" min="0.01" step="0.01" value="<?php echo $fin_option3;?>" required>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_fin_option4');?></label>
                    <input type="number" name="fin_option4" class="form-control" min="0.01" step="0.01" value="<?php echo $fin_option4;?>" required>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="company_id" value="<?php echo $company_id;?>">
                        <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
            echo form_close();
        ?>
        <?php } ?>

        <?php
        //إدارة المشتريات
        if($module_id == 4) {
        ?>
        <?php
            $FormPath = base_url().$Segment1."/".$Segment2."/Update_module4";
            echo form_open_multipart($FormPath);
        ?>
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_purchase_option1');?></label>
                    <input type="number" name="purchase_option1" class="form-control" min="0.01" step="0.01" value="<?php echo $purchase_option1;?>" required>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="company_id" value="<?php echo $company_id;?>">
                        <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
            echo form_close();
        ?>
        <?php } ?>


        <?php
        //إدارة المبيعات
        if($module_id == 5) {
        ?>
        <?php
            $FormPath = base_url().$Segment1."/".$Segment2."/Update_module5";
            echo form_open_multipart($FormPath);
        ?>
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_sales_option1');?></label>
                    <input type="number" name="sales_option1" class="form-control" min="0.01" step="0.01" value="<?php echo $sales_option1;?>" required>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="company_id" value="<?php echo $company_id;?>">
                        <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
            echo form_close();
        ?>
        <?php } ?>

        <?php
        //إدارة المخازن
        if($module_id == 6) {
        ?>
        <?php
            $FormPath = base_url().$Segment1."/".$Segment2."/Update_module6";
            echo form_open_multipart($FormPath);
        ?>
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_store_option1');?></label>
                    <input type="number" name="store_option1" class="form-control" min="0.01" step="0.01" value="<?php echo $store_option1;?>" required>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="company_id" value="<?php echo $company_id;?>">
                        <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
            echo form_close();
        ?>
        <?php } ?>

        <?php
        //إدارة الموارد البشرية
        if($module_id == 7) {
        ?>
        <?php
            $FormPath = base_url().$Segment1."/".$Segment2."/Update_module7";
            echo form_open_multipart($FormPath);
        ?>
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_hr_option1');?></label>
                    <input type="number" name="hr_option1" class="form-control" min="1" step="1" value="<?php echo $hr_option1;?>" required>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_hr_option2');?></label>
                    <input type="number" name="hr_option2" class="form-control" min="1" step="1" value="<?php echo $hr_option2;?>" required>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_hr_option3');?></label>
                    <select name="hr_option3" class="form-control" required>
                        <option <?php if($hr_option3 == 1) {?>selected<?php }?> value="1"><?php echo lang('Yes');?></option>
                        <option <?php if($hr_option3 == 0) {?>selected<?php }?> value="0"><?php echo lang('No');?></option>
                    </select>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_hr_option4');?></label>
                    <select name="hr_option4" class="form-control" required>
                        <option <?php if($hr_option4 == 1) {?>selected<?php }?> value="1"><?php echo lang('Yes');?></option>
                        <option <?php if($hr_option4 == 0) {?>selected<?php }?> value="0"><?php echo lang('No');?></option>
                    </select>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="company_id" value="<?php echo $company_id;?>">
                        <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
            echo form_close();
        ?>
        <?php } ?>

        <?php
        //إدارة العهدة
        if($module_id == 15) {
        ?>
        <?php
            $FormPath = base_url().$Segment1."/".$Segment2."/Update_module15";
            echo form_open_multipart($FormPath);
        ?>
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_custody_option1');?></label>
                    <input type="number" name="custody_option1" class="form-control" min="1" step="1" value="<?php echo $custody_option1;?>" required>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_custody_option2');?></label>
                    <input type="number" name="custody_option2" class="form-control" min="1" step="1" value="<?php echo $custody_option2;?>" required>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="company_id" value="<?php echo $company_id;?>">
                        <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
            echo form_close();
        ?>
        <?php } ?>

        <?php
        //إدارة التواصل
        if($module_id == 16) {
        ?>
        <?php
            $FormPath = base_url().$Segment1."/".$Segment2."/Update_module16";
            echo form_open_multipart($FormPath);
        ?>
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_service_option1');?></label>
                    <select name="service_option1" class="form-control" required>
                        <option <?php if($service_option1 == 1) {?>selected<?php }?> value="1"><?php echo lang('Yes');?></option>
                        <option <?php if($service_option1 == 0) {?>selected<?php }?> value="0"><?php echo lang('No');?></option>
                    </select>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="company_id" value="<?php echo $company_id;?>">
                        <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
            echo form_close();
        ?>
        <?php } ?>

        <?php
        //إدارة الخدمات
        if($module_id == 17) {
        ?>
        <?php
            $FormPath = base_url().$Segment1."/".$Segment2."/Update_module17";
            echo form_open_multipart($FormPath);
        ?>
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_maintenance_option1');?></label>
                    <select name="maintenance_option1" class="form-control" required>
                        <option <?php if($maintenance_option1 == 1) {?>selected<?php }?> value="1"><?php echo lang('Yes');?></option>
                        <option <?php if($maintenance_option1 == 0) {?>selected<?php }?> value="0"><?php echo lang('No');?></option>
                    </select>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_maintenance_option2');?></label>
                    <select name="maintenance_option2" class="form-control" required>
                        <option <?php if($maintenance_option2 == 1) {?>selected<?php }?> value="1"><?php echo lang('Yes');?></option>
                        <option <?php if($maintenance_option2 == 0) {?>selected<?php }?> value="0"><?php echo lang('No');?></option>
                    </select>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="company_id" value="<?php echo $company_id;?>">
                        <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
            echo form_close();
        ?>
        <?php } ?>

        <?php
        //إدارة التصنيع
        if($module_id == 21) {
        ?>
        <?php
            $FormPath = base_url().$Segment1."/".$Segment2."/Update_module21";
            echo form_open_multipart($FormPath);
        ?>
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <label class="col-form-label"><?php echo lang('settings_notifications_manufacturing_option1');?></label>
                    <select name="manufacturing_option1" class="form-control" required>
                        <option <?php if($manufacturing_option1 == 1) {?>selected<?php }?> value="1"><?php echo lang('Yes');?></option>
                        <option <?php if($manufacturing_option1 == 0) {?>selected<?php }?> value="0"><?php echo lang('No');?></option>
                    </select>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="company_id" value="<?php echo $company_id;?>">
                        <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
            echo form_close();
        ?>
        <?php } ?>

        </div>
        <?php
            }
        }
        ?>
    </div>
</div>
<br>

        
    </div>