<div class="card card-custom gutter-b example example-compact">
        <?php
            $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
            echo form_open_multipart($FormPath);
        ?>
        <div class="kt-portlet__body">
            <div class="form-group row p-3 mb-2 bg-success-o-20">
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('buy_bill_ar_text');?></label>
                    <textarea name="buy_bill_ar_text" class="form-control" rows="5" required><?php echo $buy_bill_ar_text;?></textarea>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('buy_bill_en_text');?></label>
                    <textarea name="buy_bill_en_text" class="form-control" rows="5" dir="ltr" required><?php echo $buy_bill_en_text;?></textarea>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('buy_bill_orientation');?></label>
                    <select name="buy_bill_orientation" class="form-control" data-size="5" data-live-search="true" required>
                        <option <?php if($buy_bill_orientation == "L") {?>selected<?php }?> value="L"><?php echo lang('Landscape');?></option>
                        <option <?php if($buy_bill_orientation == "P") {?>selected<?php }?> value="P"><?php echo lang('Portrait');?></option>
                    </select>
                    <label class="col-form-label"><?php echo lang('buy_bill_size');?></label>
                    <select name="buy_bill_size" class="form-control" data-size="5" data-live-search="true" required>
                        <option <?php if($buy_bill_size == "A0") {?>selected<?php }?> value="A0">A0</option>
                        <option <?php if($buy_bill_size == "A1") {?>selected<?php }?> value="A1">A1</option>
                        <option <?php if($buy_bill_size == "A2") {?>selected<?php }?> value="A2">A2</option>
                        <option <?php if($buy_bill_size == "A3") {?>selected<?php }?> value="A3">A3</option>
                        <option <?php if($buy_bill_size == "A4") {?>selected<?php }?> value="A4">A4</option>
                        <option <?php if($buy_bill_size == "A5") {?>selected<?php }?> value="A5">A5</option>
                        <option <?php if($buy_bill_size == "A6") {?>selected<?php }?> value="A6">A6</option>
                        <option <?php if($buy_bill_size == "A7") {?>selected<?php }?> value="A7">A7</option>
                        <option <?php if($buy_bill_size == "A8") {?>selected<?php }?> value="A8">A8</option>
                    </select>
                </div>
            </div>

            <div class="form-group row p-3 mb-2 bg-success-o-20">
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('buy_returns_ar_text');?></label>
                    <textarea name="buy_returns_ar_text" class="form-control" rows="5" required><?php echo $buy_returns_ar_text;?></textarea>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('buy_returns_en_text');?></label>
                    <textarea name="buy_returns_en_text" class="form-control" rows="5" dir="ltr" required><?php echo $buy_returns_en_text;?></textarea>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('buy_returns_orientation');?></label>
                    <select name="buy_returns_orientation" class="form-control" data-size="5" data-live-search="true" required>
                        <option <?php if($buy_returns_orientation == "L") {?>selected<?php }?> value="L"><?php echo lang('Landscape');?></option>
                        <option <?php if($buy_returns_orientation == "P") {?>selected<?php }?> value="P"><?php echo lang('Portrait');?></option>
                    </select>
                    <label class="col-form-label"><?php echo lang('buy_returns_size');?></label>
                    <select name="buy_returns_size" class="form-control" data-size="5" data-live-search="true" required>
                        <option <?php if($buy_returns_size == "A0") {?>selected<?php }?> value="A0">A0</option>
                        <option <?php if($buy_returns_size == "A1") {?>selected<?php }?> value="A1">A1</option>
                        <option <?php if($buy_returns_size == "A2") {?>selected<?php }?> value="A2">A2</option>
                        <option <?php if($buy_returns_size == "A3") {?>selected<?php }?> value="A3">A3</option>
                        <option <?php if($buy_returns_size == "A4") {?>selected<?php }?> value="A4">A4</option>
                        <option <?php if($buy_returns_size == "A5") {?>selected<?php }?> value="A5">A5</option>
                        <option <?php if($buy_returns_size == "A6") {?>selected<?php }?> value="A6">A6</option>
                        <option <?php if($buy_returns_size == "A7") {?>selected<?php }?> value="A7">A7</option>
                        <option <?php if($buy_returns_size == "A8") {?>selected<?php }?> value="A8">A8</option>
                    </select>
                </div>
            </div>

            <div class="form-group row p-3 mb-2 bg-success-o-20">
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('sale_bill_ar_text');?></label>
                    <textarea name="sale_bill_ar_text" class="form-control" rows="5" required><?php echo $sale_bill_ar_text;?></textarea>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('sale_bill_en_text');?></label>
                    <textarea name="sale_bill_en_text" class="form-control" rows="5" dir="ltr" required><?php echo $sale_bill_en_text;?></textarea>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('sale_bill_orientation');?></label>
                    <select name="sale_bill_orientation" class="form-control" data-size="5" data-live-search="true" required>
                        <option <?php if($sale_bill_orientation == "L") {?>selected<?php }?> value="L"><?php echo lang('Landscape');?></option>
                        <option <?php if($sale_bill_orientation == "P") {?>selected<?php }?> value="P"><?php echo lang('Portrait');?></option>
                    </select>
                    <label class="col-form-label"><?php echo lang('sale_bill_size');?></label>
                    <select name="sale_bill_size" class="form-control" data-size="5" data-live-search="true" required>
                        <option <?php if($sale_bill_size == "A0") {?>selected<?php }?> value="A0">A0</option>
                        <option <?php if($sale_bill_size == "A1") {?>selected<?php }?> value="A1">A1</option>
                        <option <?php if($sale_bill_size == "A2") {?>selected<?php }?> value="A2">A2</option>
                        <option <?php if($sale_bill_size == "A3") {?>selected<?php }?> value="A3">A3</option>
                        <option <?php if($sale_bill_size == "A4") {?>selected<?php }?> value="A4">A4</option>
                        <option <?php if($sale_bill_size == "A5") {?>selected<?php }?> value="A5">A5</option>
                        <option <?php if($sale_bill_size == "A6") {?>selected<?php }?> value="A6">A6</option>
                        <option <?php if($sale_bill_size == "A7") {?>selected<?php }?> value="A7">A7</option>
                        <option <?php if($sale_bill_size == "A8") {?>selected<?php }?> value="A8">A8</option>
                    </select>
                </div>
            </div>

            <div class="form-group row p-3 mb-2 bg-success-o-20">
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('sale_returns_ar_text');?></label>
                    <textarea name="sale_returns_ar_text" class="form-control" rows="5" required><?php echo $sale_returns_ar_text;?></textarea>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('sale_returns_en_text');?></label>
                    <textarea name="sale_returns_en_text" class="form-control" rows="5" dir="ltr" required><?php echo $sale_returns_en_text;?></textarea>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('sale_returns_orientation');?></label>
                    <select name="sale_returns_orientation" class="form-control" data-size="5" data-live-search="true" required>
                        <option <?php if($sale_returns_orientation == "L") {?>selected<?php }?> value="L"><?php echo lang('Landscape');?></option>
                        <option <?php if($sale_returns_orientation == "P") {?>selected<?php }?> value="P"><?php echo lang('Portrait');?></option>
                    </select>
                    <label class="col-form-label"><?php echo lang('sale_returns_size');?></label>
                    <select name="sale_returns_size" class="form-control" data-size="5" data-live-search="true" required>
                        <option <?php if($sale_returns_size == "A0") {?>selected<?php }?> value="A0">A0</option>
                        <option <?php if($sale_returns_size == "A1") {?>selected<?php }?> value="A1">A1</option>
                        <option <?php if($sale_returns_size == "A2") {?>selected<?php }?> value="A2">A2</option>
                        <option <?php if($sale_returns_size == "A3") {?>selected<?php }?> value="A3">A3</option>
                        <option <?php if($sale_returns_size == "A4") {?>selected<?php }?> value="A4">A4</option>
                        <option <?php if($sale_returns_size == "A5") {?>selected<?php }?> value="A5">A5</option>
                        <option <?php if($sale_returns_size == "A6") {?>selected<?php }?> value="A6">A6</option>
                        <option <?php if($sale_returns_size == "A7") {?>selected<?php }?> value="A7">A7</option>
                        <option <?php if($sale_returns_size == "A8") {?>selected<?php }?> value="A8">A8</option>
                    </select>
                </div>
            </div>
            
            <div class="form-group row p-3 mb-2 bg-success-o-20">
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('data_invoice_ar_text');?></label>
                    <textarea name="data_invoice_ar_text" class="form-control" rows="5" required><?php echo $data_invoice_ar_text;?></textarea>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('data_invoice_en_text');?></label>
                    <textarea name="data_invoice_en_text" class="form-control" rows="5" dir="ltr" required><?php echo $data_invoice_en_text;?></textarea>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('data_invoice_orientation');?></label>
                    <select name="data_invoice_orientation" class="form-control" data-size="5" data-live-search="true" required>
                        <option <?php if($data_invoice_orientation == "L") {?>selected<?php }?> value="L"><?php echo lang('Landscape');?></option>
                        <option <?php if($data_invoice_orientation == "P") {?>selected<?php }?> value="P"><?php echo lang('Portrait');?></option>
                    </select>
                    <label class="col-form-label"><?php echo lang('data_invoice_size');?></label>
                    <select name="data_invoice_size" class="form-control" data-size="5" data-live-search="true" required>
                        <option <?php if($data_invoice_size == "A0") {?>selected<?php }?> value="A0">A0</option>
                        <option <?php if($data_invoice_size == "A1") {?>selected<?php }?> value="A1">A1</option>
                        <option <?php if($data_invoice_size == "A2") {?>selected<?php }?> value="A2">A2</option>
                        <option <?php if($data_invoice_size == "A3") {?>selected<?php }?> value="A3">A3</option>
                        <option <?php if($data_invoice_size == "A4") {?>selected<?php }?> value="A4">A4</option>
                        <option <?php if($data_invoice_size == "A5") {?>selected<?php }?> value="A5">A5</option>
                        <option <?php if($data_invoice_size == "A6") {?>selected<?php }?> value="A6">A6</option>
                        <option <?php if($data_invoice_size == "A7") {?>selected<?php }?> value="A7">A7</option>
                        <option <?php if($data_invoice_size == "A8") {?>selected<?php }?> value="A8">A8</option>
                    </select>
                </div>
            </div>

        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="company_id" value="<?php echo $company_id;?>">

                        <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
            echo form_close();
        ?>
    </div>