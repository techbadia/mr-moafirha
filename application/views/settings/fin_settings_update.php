<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
        <?php
            $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
            echo form_open_multipart($FormPath);
        ?>
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="col-lg-12">
                    <div class="card card-custom">
                        <div class="card-header ribbon ribbon-clip ribbon-right bg-success-o-90">
                            <div class="ribbon-target" style="top: 12px;">
                            <span class="ribbon-inner bg-primary"></span><?php echo lang('fin_settings_purchase');?></div>
                            <h3 class="card-title"><?php echo lang('fin_settings_purchase_details');?></h3>
                        </div>
                        <div class="card-body bg-success-o-60">
                            <div class="form-group row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_buy');?></label>
                                    <select name="acc_buy" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_buy == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_buy_reurn');?></label>
                                    <select name="acc_buy_reurn" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_buy_reurn == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_supplier');?></label>
                                    <select name="acc_supplier" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_supplier == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="card card-custom">
                        <div class="card-header ribbon ribbon-clip ribbon-right bg-primary-o-90">
                            <div class="ribbon-target" style="top: 12px;">
                            <span class="ribbon-inner bg-primary"></span><?php echo lang('fin_settings_sale');?></div>
                            <h3 class="card-title"><?php echo lang('fin_settings_sale_details');?></h3>
                        </div>
                        <div class="card-body bg-primary-o-60">
                            <div class="form-group row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_sale');?></label>
                                    <select name="acc_sale" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_sale == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_sale_reurn');?></label>
                                    <select name="acc_sale_reurn" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_sale_reurn == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_customer');?></label>
                                    <select name="acc_customer" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_customer == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>

                <div class="col-lg-6">
                    <div class="card card-custom">
                        <div class="card-header ribbon ribbon-clip ribbon-right bg-warning-o-90">
                            <div class="ribbon-target" style="top: 12px;">
                            <span class="ribbon-inner bg-warning"></span><?php echo lang('fin_settings_hr_loan');?></div>
                            <h3 class="card-title"><?php echo lang('fin_settings_hr_loan_details');?></h3>
                        </div>
                        <div class="card-body bg-warning-o-60">
                            <div class="form-group row">
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_custody');?></label>
                                    <select name="acc_custody" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_custody == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_hr_loan');?></label>
                                    <select name="acc_hr_loan" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_hr_loan == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6">
                    <div class="card card-custom">
                        <div class="card-header ribbon ribbon-clip ribbon-right bg-danger-o-90">
                            <div class="ribbon-target" style="top: 12px;">
                            <span class="ribbon-inner bg-danger"></span><?php echo lang('fin_settings_money');?></div>
                            <h3 class="card-title"><?php echo lang('fin_settings_money_details');?></h3>
                        </div>
                        <div class="card-body bg-danger-o-60">
                            <div class="form-group row">
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_treasury');?></label>
                                    <select name="acc_treasury" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_treasury == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_bank');?></label>
                                    <select name="acc_bank" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_bank == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-12" style="margin-top:20px;">
                    <div class="card card-custom">
                        <div class="card-header ribbon ribbon-clip ribbon-right bg-success">
                            <div class="ribbon-target" style="top: 12px;">
                            <span class="ribbon-inner bg-success"></span><?php echo lang('fin_settings_tax');?></div>
                            <h3 class="card-title"><?php echo lang('fin_settings_tax_details');?></h3>
                        </div>  
                        <div class="card-body bg-success-o-60">
                            <div class="form-group row">
                                <div class="col-lg-3 col-md-6 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_tax_sale');?></label>
                                    <select name="acc_tax_sale" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_tax_sale == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_tax_sale_percent');?></label>
                                    <div class="input-group">
                                        <input type="number" step="1" min="0" name="acc_tax_sale_percent" class="form-control" value="<?php echo $acc_tax_sale_percent;?>">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_tax_purchase');?></label>
                                    <select name="acc_tax_purchase" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_tax_purchase == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_tax_purchase_percent');?></label>
                                    <div class="input-group">
                                        <input type="number" step="1" min="0" name="acc_tax_purchase_percent" class="form-control" value="<?php echo $acc_tax_purchase_percent;?>">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6" style="margin-top:20px;">
                    <div class="card card-custom">
                        <div class="card-header ribbon ribbon-clip ribbon-right bg-warning">
                            <div class="ribbon-target" style="top: 12px;">
                            <span class="ribbon-inner bg-warning"></span><?php echo lang('fin_settings_debit_balances');?></div>
                            <h3 class="card-title"><?php echo lang('fin_settings_debit_balances_details');?></h3>
                        </div>  
                        <div class="card-body bg-warning-o-60">
                            <div class="form-group row">
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_expenses');?></label>
                                    <select name="acc_expenses" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_expenses == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_debtors');?></label>
                                    <select name="acc_debtors" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_debtors == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_capture_papers');?></label>
                                    <select name="acc_capture_papers" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_capture_papers == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_sales_commission');?></label>
                                    <select name="acc_sales_commission" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_sales_commission == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_current_assets');?></label>
                                    <select name="acc_current_assets" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_current_assets == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6" style="margin-top:20px;">
                    <div class="card card-custom">
                        <div class="card-header ribbon ribbon-clip ribbon-right bg-primary">
                            <div class="ribbon-target" style="top: 12px;">
                            <span class="ribbon-inner bg-primary"></span><?php echo lang('fin_settings_Credit_balances');?></div>
                            <h3 class="card-title"><?php echo lang('fin_settings_Credit_balances_details');?></h3>
                        </div>  
                        <div class="card-body bg-primary-o-60">
                            <div class="form-group row">
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_revenues');?></label>
                                    <select name="acc_revenues" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_revenues == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_creditors');?></label>
                                    <select name="acc_creditors" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_creditors == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_payment_papers');?></label>
                                    <select name="acc_payment_papers" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_payment_papers == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_fixed_assets');?></label>
                                    <select name="acc_fixed_assets" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_fixed_assets == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <label class="col-form-label"><?php echo lang('acc_invoice_items');?></label>
                            <input type="number" name="acc_invoice_items" class="form-control" min="1" step="1" value="<?php echo $acc_invoice_items;?>">
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <label class="col-form-label"><?php echo lang('Currency');?></label>
                            <select name="currency_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                <?php
                                $CurrenciesList = $this->M_currencies->GetMultiRow();
                                foreach($CurrenciesList as $CurrenciesList_Row) {
                                    if ($this->session->userdata('lang') == "ar")
                                    {
                                        $CurrencyTitle = $CurrenciesList_Row->title;
                                    }
                                    else
                                    {
                                        $CurrencyTitle = $CurrenciesList_Row->title_en;
                                    }
                                ?>
                                <option <?php if($currency_id == $CurrenciesList_Row->id) {?>selected<?php }?> value="<?php echo $CurrenciesList_Row->id;?>">
                                <?php echo $CurrencyTitle;?>
                                </option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                </div>

                <div class="col-lg-12" style="margin-top:20px;">
                    <div class="card card-custom">
                        <div class="card-header ribbon ribbon-clip ribbon-right bg-danger">
                            <div class="ribbon-target" style="top: 12px;">
                            <span class="ribbon-inner bg-danger"></span><?php echo lang('fin_settings_store');?></div>
                            <h3 class="card-title"><?php echo lang('fin_settings_store_details');?></h3>
                        </div>  
                        <div class="card-body bg-danger-o-60">
                            <div class="form-group row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_inventory');?></label>
                                    <select name="acc_inventory" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_inventory == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_material_store');?></label>
                                    <select name="acc_material_store" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_material_store == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <label class="col-form-label"><?php echo lang('acc_damaged_store');?></label>
                                    <select name="acc_damaged_store" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
                                        <?php
                                        $AccountsList = $this->M_fin_treeaccount->GetMultiRow(false);
                                        foreach($AccountsList as $AccountsList_Row) {
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $AccountTitle = $AccountsList_Row->title;
                                            }
                                            else
                                            {
                                                $AccountTitle = $AccountsList_Row->title_en;
                                            }
                                        ?>
                                        <option <?php if($acc_damaged_store == $AccountsList_Row->id) {?>selected<?php }?> value="<?php echo $AccountsList_Row->id;?>">
                                        <?php echo $AccountTitle;?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
            </div>
            
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="company_id" value="<?php echo $company_id;?>">

                        <input type="hidden" name="acc_website_sale" value="<?php echo $acc_website_sale;?>">
                        <input type="hidden" name="acc_affiliate" value="<?php echo $acc_affiliate;?>">
                        <input type="hidden" name="acc_delivery" value="<?php echo $acc_delivery;?>">
                        <input type="hidden" name="acc_factory_cost" value="<?php echo $acc_factory_cost;?>">

                        <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
            echo form_close();
        ?>
    </div>