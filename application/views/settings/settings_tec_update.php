<div class="card card-custom gutter-b example example-compact">
        <?php
            $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
            echo form_open_multipart($FormPath);
        ?>
        <div class="kt-portlet__body">
            <div class="form-group row p-3 mb-2 bg-success-o-20">
                <div class="col-md-12">
                    <?php if(!empty($logo)){ ?>
                    <img src="<?php echo base_url("upload/settings/".$logo)?>" width="100" class="mb-3">
                    <?php } ?>
                    <label><?php echo lang('Logo');?></label>
                    <input type="file" name="image" class="form-control" accept="image/*"  >
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('Site Name');?></label>
                    <input type="text" value="<?php echo $site_name;?>" name="site_name" class="form-control" required>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('address');?></label>
                    <input type="text" value="<?php echo $address;?>" name="address" class="form-control" required>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('phone');?></label>
                    <input type="tel" value="<?php echo $tel;?>" name="tel" class="form-control" required>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('buy_manufacturer_tax_card');?></label>
                    <input type="number" value="<?php echo $tax_number;?>" name="tax_number" class="form-control" required>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('buy_manufacturer_tax_card_date');?></label>
                    <input type="date" value="<?php echo $tax_date;?>" name="tax_date" class="form-control" required>
                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                    <label class="col-form-label"><?php echo lang('crm_link');?></label>
                    <input type="url" value="<?php echo $crm;?>" name="crm" class="form-control" >
                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('Data_base_name');?></label>
                    <input type="text" value="<?php echo $DB_name;?>" name="DB_name" class="form-control" >
                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('Data_user_name');?></label>
                    <input type="text" value="<?php echo $DB_uname;?>" name="DB_uname" class="form-control" >
                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                    <label class="col-form-label"><?php echo lang('Data_password');?></label>
                    <input type="text" value="<?php echo $DB_pass;?>" name="DB_pass" class="form-control" >
                </div>

            </div>


        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="1">
                        <input type="hidden" name="old_logo" value="<?= $logo; ?>">

                        <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
            echo form_close();
        ?>
    </div>
