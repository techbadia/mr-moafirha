<?php
$branches_lookup = intval($this->session->userdata('branches_lookup'));
if ($branches_lookup == 1) {
    $FormPath = base_url() . "home/ChangeCompany";
    echo form_open_multipart($FormPath);
?>
    <div class="row" style="width:80%; margin-right:5px; margin-left:5px">
        <div class="col-10">
            <select id="company_id" name="company_id" class="form-control" required>
                <?php
                $company_id = intval($this->session->userdata('company_id'));
                $CompaniesList = $this->M_app_company->GetMultiRow();
                foreach ($CompaniesList as $CompaniesList_Row) {
                    if ($this->session->userdata('lang') == "ar") {
                        $Title = $CompaniesList_Row->ar_title;
                    } else {
                        $Title = $CompaniesList_Row->en_title;
                    }
                ?>
                    <option <?php if ($company_id == $CompaniesList_Row->id) { ?>selected<?php } ?> value="<?php echo $CompaniesList_Row->id; ?>"><?php echo $Title; ?></option>
                <?php
                }
                ?>
            </select>
        </div>
        <div class="col-2">
            <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Open'); ?></button>
        </div>
    </div>
<?php
    echo form_close();
}
?>

<!-- Start Data Management -->
<?php
//$StaffID = intval($this->session->userdata('StaffID'));
//if ($StaffID == 1) {
    $ItemOpen = "";
    $folder = "mydata";
    $Segment1 = $this->uri->segment(1);
    if ($Segment1 == $folder) {
        $ItemOpen = "menu-item menu-item-submenu menu-item-open";
    } else {
        $ItemOpen = "menu-item menu-item-submenu";
    }
?>
    <?php if (!empty($crm)) { ?>
        <li class="menu-item ">
            <a href="<?= $crm; ?>crm-login" class="menu-link menu-toggle">
                <span class="svg-icon menu-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"></rect>
                            <path d="M2,13 L22,13 L22,18 C22,19.1045695 21.1045695,20 20,20 L4,20 C2.8954305,20 2,19.1045695 2,18 L2,13 Z M18.5,18 C19.3284271,18 20,17.3284271 20,16.5 C20,15.6715729 19.3284271,15 18.5,15 C17.6715729,15 17,15.6715729 17,16.5 C17,17.3284271 17.6715729,18 18.5,18 Z M13.5,18 C14.3284271,18 15,17.3284271 15,16.5 C15,15.6715729 14.3284271,15 13.5,15 C12.6715729,15 12,15.6715729 12,16.5 C12,17.3284271 12.6715729,18 13.5,18 Z" fill="#000000"></path>
                            <path d="M5.79268604,8 L18.207314,8 C18.5457897,8 18.8612922,8.17121884 19.0457576,8.45501165 L22,13 L2,13 L4.95424243,8.45501165 C5.13870775,8.17121884 5.45421032,8 5.79268604,8 Z" fill="#000000" opacity="0.3"></path>
                        </g>
                    </svg>
                </span>
                <span class="menu-text">CRM</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
    <?php } ?>
    <li class="<?php echo $ItemOpen; ?>" aria-haspopup="true" data-menu-toggle="hover">
        <a href="javascript:;" class="menu-link menu-toggle">
            <span class="svg-icon menu-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path d="M2,13 L22,13 L22,18 C22,19.1045695 21.1045695,20 20,20 L4,20 C2.8954305,20 2,19.1045695 2,18 L2,13 Z M18.5,18 C19.3284271,18 20,17.3284271 20,16.5 C20,15.6715729 19.3284271,15 18.5,15 C17.6715729,15 17,15.6715729 17,16.5 C17,17.3284271 17.6715729,18 18.5,18 Z M13.5,18 C14.3284271,18 15,17.3284271 15,16.5 C15,15.6715729 14.3284271,15 13.5,15 C12.6715729,15 12,15.6715729 12,16.5 C12,17.3284271 12.6715729,18 13.5,18 Z" fill="#000000" />
                        <path d="M5.79268604,8 L18.207314,8 C18.5457897,8 18.8612922,8.17121884 19.0457576,8.45501165 L22,13 L2,13 L4.95424243,8.45501165 C5.13870775,8.17121884 5.45421032,8 5.79268604,8 Z" fill="#000000" opacity="0.3" />
                    </g>
                </svg>
            </span>
            <span class="menu-text"><?php echo lang('DataManagement'); ?></span>
            <i class="menu-arrow"></i>
        </a>
        <div class="menu-submenu" kt-hidden-height="80">
            <i class="menu-arrow"></i>
            <ul class="menu-subnav">
                <li class="menu-item menu-item-parent" aria-haspopup="true">
                    <span class="menu-link">
                        <span class="menu-text"><?php echo lang('DataManagement'); ?></span>
                    </span>
                </li>

                <li class="menu-item" aria-haspopup="true">
                    <a href="<?php echo base_url() . "mydata/deleterows/"; ?>" class="menu-link">
                        <i class="menu-bullet menu-bullet-dot">
                            <span></span>
                        </i>
                        <span class="menu-text"><?php echo lang('DataDelete'); ?></span>
                    </a>
                </li>
                <li class="menu-item" aria-haspopup="true">
                    <a href="<?php echo base_url() . "mydata/repair/"; ?>" class="menu-link">
                        <i class="menu-bullet menu-bullet-dot">
                            <span></span>
                        </i>
                        <span class="menu-text"><?php echo lang('Databse_Repair'); ?></span>
                    </a>
                </li>
                <li class="menu-item" aria-haspopup="true">
                    <a href="<?php echo base_url() . "mydata/optimize/"; ?>" class="menu-link">
                        <i class="menu-bullet menu-bullet-dot">
                            <span></span>
                        </i>
                        <span class="menu-text"><?php echo lang('Databse_Optimize'); ?></span>
                    </a>
                </li>
                <li class="menu-item" aria-haspopup="true">
                    <a href="<?php echo base_url() . "mydata/backup/"; ?>" class="menu-link">
                        <i class="menu-bullet menu-bullet-dot">
                            <span></span>
                        </i>
                        <span class="menu-text"><?php echo lang('DatabaseBackeup'); ?></span>
                    </a>
                </li>
                <?php
                $POS_available = intval($this->session->userdata('POS_available'));
                if ($POS_available == 1) {
                ?>
                    <li class="menu-item" aria-haspopup="true">
                        <a href="<?php echo base_url() . "pos_sys/tec_stores/"; ?>" class="menu-link">
                            <i class="menu-bullet menu-bullet-dot">
                                <span></span>
                            </i>
                            <span class="menu-text"><?php echo lang('POS_Management'); ?></span>
                        </a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </li>
<?php
//}
?>
<!-- End Data Management -->

<?php
$ItemOpen = "";
$folder = "company";
$Segment1 = $this->uri->segment(1);
if ($Segment1 == $folder) {
    $ItemOpen = "menu-item menu-item-submenu menu-item-open";
} else {
    $ItemOpen = "menu-item menu-item-submenu";
}
if ($branches_lookup == 1) {
?>
    <li class="<?php echo $ItemOpen; ?>" aria-haspopup="true" data-menu-toggle="hover">
        <a href="javascript:;" class="menu-link menu-toggle">
            <span class="svg-icon menu-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path d="M6,7 C7.1045695,7 8,6.1045695 8,5 C8,3.8954305 7.1045695,3 6,3 C4.8954305,3 4,3.8954305 4,5 C4,6.1045695 4.8954305,7 6,7 Z M6,9 C3.790861,9 2,7.209139 2,5 C2,2.790861 3.790861,1 6,1 C8.209139,1 10,2.790861 10,5 C10,7.209139 8.209139,9 6,9 Z" fill="#000000" fill-rule="nonzero" />
                        <path d="M7,11.4648712 L7,17 C7,18.1045695 7.8954305,19 9,19 L15,19 L15,21 L9,21 C6.790861,21 5,19.209139 5,17 L5,8 L5,7 L7,7 L7,8 C7,9.1045695 7.8954305,10 9,10 L15,10 L15,12 L9,12 C8.27142571,12 7.58834673,11.8052114 7,11.4648712 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                        <path d="M18,22 C19.1045695,22 20,21.1045695 20,20 C20,18.8954305 19.1045695,18 18,18 C16.8954305,18 16,18.8954305 16,20 C16,21.1045695 16.8954305,22 18,22 Z M18,24 C15.790861,24 14,22.209139 14,20 C14,17.790861 15.790861,16 18,16 C20.209139,16 22,17.790861 22,20 C22,22.209139 20.209139,24 18,24 Z" fill="#000000" fill-rule="nonzero" />
                        <path d="M18,13 C19.1045695,13 20,12.1045695 20,11 C20,9.8954305 19.1045695,9 18,9 C16.8954305,9 16,9.8954305 16,11 C16,12.1045695 16.8954305,13 18,13 Z M18,15 C15.790861,15 14,13.209139 14,11 C14,8.790861 15.790861,7 18,7 C20.209139,7 22,8.790861 22,11 C22,13.209139 20.209139,15 18,15 Z" fill="#000000" fill-rule="nonzero" />
                    </g>
                </svg>
            </span>
            <span class="menu-text"><?php echo lang('Branches'); ?></span>
            <i class="menu-arrow"></i>
        </a>
        <div class="menu-submenu" kt-hidden-height="80">
            <i class="menu-arrow"></i>
            <ul class="menu-subnav">
                <li class="menu-item menu-item-parent" aria-haspopup="true">
                    <span class="menu-link">
                        <span class="menu-text"><?php echo lang('Branches'); ?></span>
                    </span>
                </li>
                <?php
                $company_id = intval($this->session->userdata('company_id'));
                $CompaniesList = $this->M_app_company->GetMultiRow();
                foreach ($CompaniesList as $CompaniesList_Row) {
                    if ($this->session->userdata('lang') == "ar") {
                        $Title = $CompaniesList_Row->ar_title;
                    } else {
                        $Title = $CompaniesList_Row->en_title;
                    }
                    $BrancheID = $CompaniesList_Row->id;
                ?>
                    <li class="menu-item" aria-haspopup="true">
                        <a href="<?php echo base_url() . "branche/updateform/" . $BrancheID; ?>" class="menu-link">
                            <i class="menu-bullet menu-bullet-dot">
                                <span></span>
                            </i>
                            <span class="menu-text"><?php echo $Title; ?></span>
                        </a>
                    </li>
                <?php
                }
                ?>
                <!--
          <li class="menu-item" aria-haspopup="true">
                <a href="https://erp.2030saudivision.com/apps/car_maintenace/branche/insertform" class="menu-link">
                    <i class="menu-bullet menu-bullet-dot">
                        <span></span>
                    </i>
                    <span class="menu-text">اضافة فرع</span>
                </a>
            </li>-->


            </ul>
        </div>
    </li>
<?php } ?>


<?php
$GroupID = intval($this->session->userdata('GroupID'));
$PackageID = intval($this->session->userdata('PackageID'));

$PackageModules = $this->M_app_package_module->GetMainModules($PackageID);
foreach ($PackageModules as $PackageModules_Row) {
    $app_package_module_id = $PackageModules_Row->id;
    $module_id = $PackageModules_Row->module_id;
    $ModuleData = $this->M_app_module->GetRow($module_id);
    $folder = $ModuleData->folder;
    $icon_name = $ModuleData->icon_name;
    $ModuleTitle = "";
    if ($this->session->userdata('lang') == "ar") {
        $ModuleTitle = $ModuleData->ar_title;
    } else {
        $ModuleTitle = $ModuleData->en_title;
    }

    $Segment1 = $this->uri->segment(1);
    $ItemOpen = "";
    if ($Segment1 == $folder) {
        $ItemOpen = "menu-item menu-item-submenu menu-item-open";
    } else {
        $ItemOpen = "menu-item menu-item-submenu";
    }
    $CheckPages = $this->M_app_package_module_page->CheckPagesCount($app_package_module_id);
    $PageCountByModule = $this->M_app_module_page->GetPageByModule($module_id);
    $PageView = 0;
    foreach ($PageCountByModule as $PageCountByModule_Row) {
        $page_id = $PageCountByModule_Row->id;
        $CheckView = $this->M_usr_usersprivileges->CheckView($GroupID, $page_id);

        if ($CheckView > 0) {
            $PageView += 1;
        }
    }
    if (($CheckPages > 0) && ($PageView > 0)) {
?>
        <li class="<?php echo $ItemOpen; ?>" aria-haspopup="true" data-menu-toggle="hover">
            <a href="javascript:;" class="menu-link menu-toggle">
                <span class="svg-icon menu-icon">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                    <?php echo $icon_name; ?>
                    <!--end::Svg Icon-->
                </span>
                <span class="menu-text"><?php echo $ModuleTitle; ?></span>
                <i class="menu-arrow"></i>
            </a>
            <div class="menu-submenu">
                <i class="menu-arrow"></i>
                <ul class="menu-subnav">
                    <li class="menu-item menu-item-parent" aria-haspopup="true">
                        <span class="menu-link">
                            <span class="menu-text"><?php echo $ModuleTitle; ?></span>
                        </span>
                    </li>
                    <?php

                    $ModulePages = $this->M_app_package_module_page->GetByModule($app_package_module_id);
                    // print_r($ModulePages);
                    foreach ($ModulePages as $ModulePages_Row) {
                        $PageID = $ModulePages_Row->page_id;
                        if ($this->session->userdata('lang') == "ar") {
                            $PageTitle = $ModulePages_Row->ar_title;
                        } else {
                            $PageTitle = $ModulePages_Row->en_title;
                        }
                        //echo "P ID = ".$PageID;
                        $PageData = $this->M_app_module_page->GetRow($PageID);
                        // print_r($PageData);

                        $ModuleID = $PageData->module_id;
                        $class_name = $PageData->class_name;
                        $target_blank = $PageData->target_blank;
                        $target = "";
                        if ($target_blank == 1) {
                            $target = " target='_blank'";
                        }
                        $CheckView = intval($this->M_usr_usersprivileges->CheckView($GroupID, $PageID));
                        if ($CheckView > 0) {
                            $Segment2 = $this->uri->segment(2);
                            $ItemActive = "";
                            if ($Segment2 == $class_name) {
                                $ItemActive = "menu-item menu-item-active";
                            } else {
                                $ItemActive = "menu-item";
                            }
                            if($ModulePages_Row->ar_title != "مهام المشاريع"){
                        ?>
                            <li class="<?php echo $ItemActive; ?>" aria-haspopup="true">
                                <a href="<?php echo base_url() . $folder . "/" . $class_name; ?>" <?php echo $target; ?> class="menu-link">
                                    <i class="menu-bullet menu-bullet-dot">
                                        <span></span>
                                    </i>
                                    <span class="menu-text"><?php echo $PageTitle; ?></span>
                                </a>
                            </li>
                    <?php
                        }
                        }
                    }
                    $CheckSubModules = $this->M_app_package_module->GetSubModulesCount($PackageID, $module_id);
                    if ($CheckSubModules > 0) {
                        $SubModules = $this->M_app_package_module->GetSubModules($PackageID, $module_id);
                        // var_dump($SubModules);die();
                        // if($module_id == 8)
                        // {
                        // }
                        foreach ($SubModules as $SubModules_Row) {
                            $app_package_Sub_module_id = $SubModules_Row->id;
                            $Sub_module_id = $SubModules_Row->module_id;
                            $ModuleData = $this->M_app_module->GetRow($Sub_module_id);
                            $folder = $ModuleData->folder;
                            $icon_name = $ModuleData->icon_name;
                            $ModuleTitle = "";
                            if ($this->session->userdata('lang') == "ar") {
                                $ModuleTitle = $ModuleData->ar_title;
                            } else {
                                $ModuleTitle = $ModuleData->en_title;
                            }                            $Segment1 = $this->uri->segment(1);
                            $ItemOpen = "";
                            if ($Segment1 == $folder) {
                                $ItemOpen = "menu-item menu-item-submenu menu-item-open";
                            } else {
                                $ItemOpen = "menu-item menu-item-submenu";
                            }
                    ?>
                            <li class="<?php echo $ItemOpen; ?>" aria-haspopup="true" data-menu-toggle="hover">
                                <a href="javascript:;" class="menu-link menu-toggle">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text"><?php echo $ModuleTitle; ?></span>
                                    <i class="menu-arrow"></i>
                                </a>
                                <div class="menu-submenu">
                                    <i class="menu-arrow"></i>
                                    <ul class="menu-subnav">
                                        <?php
                                        $SubModulePages = $this->M_app_package_module_page->GetByModule($app_package_Sub_module_id);
                                        foreach ($SubModulePages as $SubModulePages_Row) {
                                            $PageID = $SubModulePages_Row->page_id;
                                            if ($this->session->userdata('lang') == "ar") {
                                                $PageTitle = $SubModulePages_Row->ar_title;
                                            } else {
                                                $PageTitle = $SubModulePages_Row->en_title;
                                            }
                                            $PageData = $this->M_app_module_page->GetRow($PageID);
                                            $class_name = $PageData->class_name;
                                            $target_blank = $PageData->target_blank;
                                            $target = "";
                                            if ($target_blank == 1) {
                                                $target = " target='_blank'";
                                            }
                                            $CheckView = intval($this->M_usr_usersprivileges->CheckView($GroupID, $PageID));
                                            if ($CheckView > 0) {
                                                $Segment2 = $this->uri->segment(2);
                                                $ItemActive = "";
                                                if ($Segment2 == $class_name) {
                                                    $ItemActive = "menu-item menu-item-active";
                                                } else {
                                                    $ItemActive = "menu-item";
                                                }
                                        ?>
                                                <li class="<?php echo $ItemActive; ?>" aria-haspopup="true">
                                                    <a href="<?php echo base_url() . $folder . "/" . $class_name; ?>" <?php echo $target; ?> class="menu-link">
                                                        <i class="menu-bullet menu-bullet-dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="menu-text"><?php echo $PageTitle; ?></span>
                                                    </a>
                                                </li>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </li>
                        <?php
                        }
                    }
                    ?>
                </ul>
            </div>
        </li>
<?php
    }
}
?>
