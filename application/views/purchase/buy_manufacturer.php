

									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('buy_manufacturer_title');?></th>
												<th><?php echo lang('buy_manufacturer_title_en');?></th>
												<th><?php echo lang('buy_manufacturer_phone');?></th>
												<th><?php echo lang('buy_manufacturer_mobile');?></th>
												<th><?php echo lang('buy_manufacturer_email');?></th>
												<th><?php echo lang('buy_manufacturer_person');?></th>
												<th><?php echo lang('buy_manufacturer_person_phone');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$buy_manufacturer_id = $DataRows_Row->id;
											?>
											<tr>
												<td>
												<a href="<?php echo base_url();?>account/fin_treeaccount/view_account/<?php echo $DataRows_Row->id;?>" target="_blank"><?php echo $DataRows_Row->id;?></a>
												</td>
												<td><?php echo $DataRows_Row->title;?></td>
												<td><?php echo $DataRows_Row->title_en;?></td>
												<td><?php echo $DataRows_Row->phone;?></td>
												<td><?php echo $DataRows_Row->mobile;?></td>
												<td><?php echo $DataRows_Row->email;?></td>
												<td><?php echo $DataRows_Row->person;?></td>
												<td><?php echo $DataRows_Row->person_phone;?></td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
									</table>

									