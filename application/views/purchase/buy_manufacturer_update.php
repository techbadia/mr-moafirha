
<div class="card card-custom gutter-b example example-compact">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
                        echo form_open_multipart($FormPath);
                    ?>
								
								<div class="form-body">
										<div class="form-group row">
										<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_title');?></label>
												<input type="text" name="title" class="form-control" value="<?php echo $title;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_title_en');?></label>
												<input type="text" name="title_en" class="form-control" value="<?php echo $title_en;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_email');?></label>
												<input type="email" name="email" class="form-control" value="<?php echo $email;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_phone');?></label>
												<input type="text" name="phone" class="form-control" value="<?php echo $phone;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_mobile');?></label>
												<input type="text" name="mobile" class="form-control" value="<?php echo $mobile;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_address');?></label>
												<input type="text" name="address" class="form-control" value="<?php echo $address;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_commercial_register');?></label>
												<input type="text" name="commercial_register" class="form-control" value="<?php echo $commercial_register;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_tax_card');?></label>
												<input type="text" name="tax_card" class="form-control" value="<?php echo $tax_card;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_person');?></label>
												<input type="text" name="person" class="form-control" value="<?php echo $person;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_person_phone');?></label>
												<input type="text" name="person_phone" class="form-control" value="<?php echo $person_phone;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_credit_limit');?></label>
												<input type="text" name="credit_limit" class="form-control" value="<?php echo $credit_limit;?>" required>
											</div>
										</div>
										
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php echo $id;?>">
													<input type="hidden" name="deleted" value="<?php echo $deleted;?>">
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>