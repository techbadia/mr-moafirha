
<div class="card card-custom gutter-b example example-compact">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
                        echo form_open_multipart($FormPath);
                    ?>
								
								<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_title');?></label>
												<input type="text" name="title" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_title_en');?></label>
												<input type="text" name="title_en" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<!--<label class="col-form-label"><?php echo lang('fin_treeaccount_account_no');?></label>-->
												<!--<input type="text" name="account_no" class="form-control" value="" required>-->
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_startamount');?></label>
												<input type="number" min="0" step="0.001" name="startamount" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_email');?></label>
												<input type="email" name="email" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_phone');?></label>
												<input type="text" name="phone" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_mobile');?></label>
												<input type="text" name="mobile" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_address');?></label>
												<input type="text" name="address" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_commercial_register');?></label>
												<input type="text" name="commercial_register" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_tax_card');?></label>
												<input type="text" name="tax_card" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_person');?></label>
												<input type="text" name="person" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_person_phone');?></label>
												<input type="text" name="person_phone" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_manufacturer_credit_limit');?></label>
												<input type="text" name="credit_limit" class="form-control" value="" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<input type="hidden" name="deleted" value="0">
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>

								