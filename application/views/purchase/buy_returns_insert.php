<div class="card card-custom gutter-b example example-compact">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
                        $attributes = array('id' => 'SendData', 'name' => 'SendData');
                        echo form_open_multipart($FormPath, $attributes);
                    ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_returns_supplier_id');?></label>
												<select name="supplier_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
													<?php
													$SuppliersList = $this->M_buy_manufacturer->GetMultiRow();
													foreach($SuppliersList as $SuppliersList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$SupplierTitle = $SuppliersList_Row->title;
														}
														else
														{
															$SupplierTitle = $SuppliersList_Row->title_en;
														}
													?>
													<option value="<?php echo $SuppliersList_Row->id;?>"><?php echo $SupplierTitle;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_returns_thedate');?></label>
												<input type="date" name="thedate" class="form-control" value="<?php echo date("Y-m-d");?>" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_returns_image');?></label>
												<input type="file" name="image" class="form-control" dir="ltr" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_returns_tree_id');?></label>
												<select name="tree_id" class="form-control">
													<?php
													$acc_treasury = intval($this->session->userdata('acc_treasury'));
													$toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_treasury);
													foreach($toaccount as $toaccount_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$ToTitle = $toaccount_Row->title;
														}
														else
														{
															$ToTitle = $toaccount_Row->title_en;
														}
													?>
													<option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
													<?php } ?>
													<?php
													$acc_bank = intval($this->session->userdata('acc_bank'));
													$toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_bank);
													foreach($toaccount as $toaccount_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$ToTitle = $toaccount_Row->title;
														}
														else
														{
															$ToTitle = $toaccount_Row->title_en;
														}
													?>
													<option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
													<?php } ?>
												</select>
											</div>
										</div>

										<div class="form-group row">
											<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
												<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('product_barcode');?></label>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12 p-3 mb-2 bg-primary text-white">
												<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('buy_returns_items_store_type_id');?></label>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
												<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('buy_returns_items_location_id');?></label>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
												<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('buy_returns_items_quantity');?></label>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
												<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('buy_returns_items_unitprice');?></label>
											</div>
											<?php
											$InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
											$BackgroundColor = "p-3 mb-2 bg-light text-dark";
											for ($i = 1; $i <= $InvoiceItems; $i++) {
												if($BackgroundColor == "p-3 mb-2 bg-light text-dark")
												{
													$BackgroundColor = "p-3 mb-2 bg-white text-dark";
												}
												else
												{
													$BackgroundColor = "p-3 mb-2 bg-light text-dark";
												}
											?>
												<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
													<input type="text" name="barcode<?php echo $i;?>" id="barcode<?php echo $i;?>" onfocus="GetName(<?php echo $i;?>)" onblur="GetName(<?php echo $i;?>)" class="form-control" value="">
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12 <?php echo $BackgroundColor;?>">
													<input type="text" name="title<?php echo $i;?>" id="title<?php echo $i;?>" class="form-control" value="">
												</div>
												<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
													<select name="location_id<?php echo $i;?>" class="form-control">
														<?php
														$StoreList = $this->M_store->GetMultiRow();
														foreach($StoreList as $StoreList_Row) {
															if ($this->session->userdata('lang') == "ar")
															{
																$FromStore = $StoreList_Row->title;
															}
															else
															{
																$FromStore = $StoreList_Row->title_en;
															}
														?>
														<option value="<?php echo $StoreList_Row->id;?>"><?php echo $FromStore;?></option>
														<?php } ?>
													</select>
												</div>
												<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
													<input type="number" min="0" step="1" name="quantity<?php echo $i;?>" id="quantity<?php echo $i;?>" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();" class="form-control" value="0">
												</div>
												<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
													<input type="number" min="0" step="1" name="unitprice<?php echo $i;?>" id="unitprice<?php echo $i;?>" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();" class="form-control" value="0">
													<input type="hidden" name="subtotal<?php echo $i;?>" id="subtotal<?php echo $i;?>">
												</div>
											<?php
											}
											?>
										</div>
										
                                         <div class="append-row">
                                        
                                         </div>
                                           
                                           <span class="svg-icon svg-icon-md add" style="float:right; margin-top:-5px;" id="eee">
                                        
                                        							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        
                                                                         width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        
                                        								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        
                                        									<rect x="0" y="0" width="24" height="24"/>
                                        
                                        									<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
                                        
                                        									<path d="M11,11 L11,7 C11,6.44771525 11.4477153,6 12,6 C12.5522847,6 13,6.44771525 13,7 L13,11 L17,11 C17.5522847,11 18,11.4477153 18,12 C18,12.5522847 17.5522847,13 17,13 L13,13 L13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 L11,13 L7,13 C6.44771525,13 6,12.5522847 6,12 C6,11.4477153 6.44771525,11 7,11 L11,11 Z"
                                        
                                                                                  fill="#000000"/>
                                        
                                        								</g>
                                        
                                        							</svg>
                                        		</span>
     
										<div class="form-group row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_returns_notes');?></label>
												<input type="text" name="notes" class="form-control" value="" required>
											</div>

											

											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_returns_totalvalue');?></label>
												<input type="number" min="0" step="0.001" name="totalvalue" id="totalvalue" class="form-control" value="" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_returns_paid');?></label>
												<input type="number" min="0" step="0.001" name="paid" id="paid" onchange="Remaing();" onblur="Remaing();" onkeyup="Remaing();" class="form-control" value="0" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_returns_remaining');?></label>
												<input type="number" min="0" step="0.001" name="remaining" id="remaining" class="form-control" value="" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_returns_discount');?></label>
												<input type="number" min="0" step="0.001"name="discount" id="discount" class="form-control" value="0" required>
											</div>


										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<input type="hidden" name="deleted" value="0">
													<input type="hidden" name="RowsCount" id="RowsCount" value="<?php echo $InvoiceItems;?>">
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>
                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {

            var count = <?php echo $InvoiceItems?>;

            $(document).on('click', '#eee', function () {

                var BackgroundColor = "p-3 mb-2 bg-success-o-40";



                if (BackgroundColor == "p-3 mb-2 bg-success-o-40") {

                    BackgroundColor = "p-3 mb-2 bg-success-o-20";

                } else {

                    BackgroundColor = "p-3 mb-2 bg-success-o-40";

                }



                count++;

                var html = '';

                html += '<div class="form-group row">';

                html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';

                html += '<input type="text" name="barcode' + count + '" id="barcode' + count + '" class="form-control" value="" onfocus="GetName(' + count + '); GetSalePrice(' + count + ');  GetSaleMinPrice(' + count + ');  GetStores(' + count + ')" onblur="GetName(' + count + '); GetSalePrice(' + count + ');  GetSaleMinPrice(' + count + ');  GetStores(' + count + ')" >';

                html += '</div>';



                html += '<div class="col-lg-4 col-md-4 col-sm-12 ' + BackgroundColor + '">';

                html += '<input type="text" name="title' + count + '" id="title' + count + '" class="form-control" value="">';

                html += '</div>';



                html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';

                html += '<select name="location_id' + count + '" id="location_id' + count + '" class="form-control" onchange="GetStores(' + count + ');">"' ; 
                
                
				html += '	<option  value=""><?php echo lang('Choose');?></option>';

						<?php
						$StoreList = $this->M_store->GetMultiRow();
						foreach($StoreList as $StoreList_Row) {
							if ($this->session->userdata('lang') == "ar")
							{
								$FromStore = $StoreList_Row->title;
							}
							else
							{
								$FromStore = $StoreList_Row->title_en;
							}
						?>
					html += '<option value="<?php echo $StoreList_Row->id;?>"><?php echo $FromStore;?></option>';
						<?php } ?>
				
                html +='"</select>';
                html += '</div>';




                html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';

                html += '<input type="number" step="1" min="0" name="quantity' + count + '" id="quantity' + count + '" class="form-control" value="0" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();">';

                html += '</div>';





                html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';

                html += '<input type="number" step="1" min="0" name="unitprice' + count + '" id="unitprice' + count + '" class="form-control" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();" value="0">';

                html += '<input type="hidden" name="subtotal' + count + '" id="subtotal' + count + '">';

                html += '</div>';

                html += '</div>';



                $('#RowsCount').val(count);

                $('.append-row').append(html);

            });



            $(document).on('click', '.remove', function () {

                $(this).closest('tr').remove();

            });



        });

    </script>