<div id="kt_quick_panel" class="offcanvas offcanvas-right pt-5 pb-10">
    <!--begin::Header-->
    <div class="offcanvas-header offcanvas-header-navs d-flex align-items-center justify-content-between mb-5">
        <ul class="nav nav-bold nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-primary flex-grow-1 px-10" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#kt_quick_panel_logs"><?php echo $this->lang->line('serv_events');?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_notifications"><?php echo $this->lang->line('serv_message');?></a>
            </li>
        </ul>
        <div class="offcanvas-close mt-n1 pr-5">
            <a href="#" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_panel_close">
                <i class="ki ki-close icon-xs text-muted"></i>
            </a>
        </div>
    </div>
    <!--end::Header-->
    <!--begin::Content-->
    <div class="offcanvas-content px-10">
        <div class="tab-content">
            <!--begin::Tabpane-->
            <div class="tab-pane fade show pt-3 pr-5 mr-n5 active" id="kt_quick_panel_logs" role="tabpanel">
                <!--begin::Section-->
                <div class="mb-15">
                    <h5 class="font-weight-bold mb-5"><?php echo $this->lang->line('UpComingEvents');?></h5>
                    <?php
                    $UpComingEvents = $this->M_serv_events->Get_coming();
                    foreach($UpComingEvents as $UpComingEvents_Row) {
                        $event_id = $UpComingEvents_Row->id;
                        $user_id = intval($this->session->userdata('StaffID'));
                        $CheckMyEvents = $this->M_serv_events_users->CountMyEvents($event_id, $user_id);
                        if($CheckMyEvents > 0) {
                    ?>
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-wrap mb-5">
                        <div class="symbol symbol-50 symbol-light mr-5">
                            <span class="symbol-label">
                                <img src="<?php echo base_url();?>/assets/media/svg/misc/006-plurk.svg" class="h-50 align-self-center" alt="" />
                            </span>
                        </div>
                        <div class="d-flex flex-column flex-grow-1 mr-2">
                            <a href="<?php echo base_url();?>service/serv_events/view/<?php echo $event_id;?>" class="font-weight-bolder text-dark-75 text-hover-primary font-size-lg mb-1">
                                <?php echo $UpComingEvents_Row->details;?>
                            </a>
                            <span class="text-muted font-weight-bold"><?php echo $UpComingEvents_Row->start_date;?> : <?php echo $UpComingEvents_Row->end_date;?></span>
                        </div>
                    </div>
                    <!--end: Item-->
                    <?php
                        }
                    }
                    ?>
                </div>
                <!--end::Section-->
            </div>
            <!--end::Tabpane-->
            <!--begin::Tabpane-->
            <div class="tab-pane fade pt-2 pr-5 mr-n5" id="kt_quick_panel_notifications" role="tabpanel">
                <!--begin::Nav-->
                <div class="navi navi-icon-circle navi-spacer-x-0">
                    <?php
                    $NewMessage = $this->M_serv_message->GetInboxUnReaded();
                    foreach($NewMessage as $NewMessage_Row) {
                        $MessageID = $NewMessage_Row->id;
                    ?>
                    <!--begin::Item-->
                    <a href="<?php echo base_url();?>service/serv_message/view/<?php echo $MessageID;?>" class="navi-item">
                        <div class="navi-link rounded">
                            <div class="symbol symbol-50 mr-3">
                                <div class="symbol-label">
                                    <i class="flaticon-email text-success icon-lg"></i>
                                </div>
                            </div>
                            <div class="navi-text">
                                <div class="font-weight-bold font-size-lg"><?php echo $NewMessage_Row->subject;?></div>
                                <div class="text-muted"><?php echo $NewMessage_Row->thedate." ".$NewMessage_Row->thetime;?></div>
                            </div>
                        </div>
                    </a>
                    <!--end::Item-->
                    <?php
                    }
                    ?>
                </div>
                <!--end::Nav-->
            </div>
            <!--end::Tabpane-->
        </div>
    </div>
    <!--end::Content-->
</div>