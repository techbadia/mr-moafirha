<br>
    <div class="d-flex align-items-center p-4 bg-dark" style="text-align:center !important">
        <?php
        $BS = "padding-right: 5px !important; padding-left:5px !important;";
        ?>
        <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-transparent-success font-weight-bold mr-2" style="<?php echo $BS?>">
        <i class="flaticon-doc"></i> <?php echo lang('serv_action_all');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/done/";?>" class="btn btn-transparent-primary font-weight-bold mr-2" style="<?php echo $BS?>">
        <i class="flaticon2-check-mark"></i> <?php echo lang('serv_action_done');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/onhold/";?>" class="btn btn-transparent-warning font-weight-bold mr-2" style="<?php echo $BS?>">
        <i class="flaticon-alert-2"></i> <?php echo lang('serv_action_onhold');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/expired_onhold/";?>" class="btn btn-transparent-danger font-weight-bold mr-2" style="<?php echo $BS?>">
        <i class="flaticon-danger"></i> <?php echo lang('serv_action_expired_onhold');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/expired_done/";?>" class="btn btn-transparent-primary font-weight-bold mr-2" style="<?php echo $BS?>">
        <i class="flaticon-interface-10"></i> <?php echo lang('serv_action_expired_done');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/coming/";?>" class="btn btn-transparent-success font-weight-bold mr-2" style="<?php echo $BS?>">
        <i class="flaticon-clock-1"></i> <?php echo lang('serv_action_coming');?>
        </a>
        <!--
        <a href="#" class="btn btn-transparent-warning font-weight-bold mr-2">Warning</a>
        <a href="#" class="btn btn-transparent-white font-weight-bold">White</a>
        -->
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/deleted/";?>" class="btn btn-transparent-white font-weight-bold" style="position: absolute !important; <?php echo $AnotherAlign;?>: 25px !important;">
            <i class="flaticon-delete"></i> <?php echo lang('Deleted');?>
        </a>
    </div>
<br>
<!--begin: Datatable -->
<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
    <thead>
        <tr>
            <th>ID</th>
            <th><?php echo lang('serv_action_from_user');?></th>
            <th><?php echo lang('serv_action_to_user');?></th>
            <th><?php echo lang('serv_action_start_date')." ".lang('serv_action_end_date');?></th>
            <th><?php echo lang('serv_action_details');?></th>
            <th><?php echo lang('serv_action_done');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($DataRows as $DataRows_Row) {
            $from_user = $DataRows_Row->from_user;
            $to_user = $DataRows_Row->to_user;
            $type_id = $DataRows_Row->type_id;
            $project_id = $DataRows_Row->project_id;

            $TypeData = $this->M_serv_action_types->GetRow($type_id);
            if ($this->session->userdata('lang') == "ar")
            {
                $TypeTitle = $TypeData->ar_title;
            }
            else
            {
                $TypeTitle = $TypeData->en_title;
            }

            $ProjectData = $this->M_proj_project->GetRow($project_id);
            if ($this->session->userdata('lang') == "ar")
            {
                $ProjectTitle = $ProjectData->ar_title;
            }
            else
            {
                $ProjectTitle = $ProjectData->en_title;
            }
        ?>
        <tr>
            <td><?php echo $DataRows_Row->id;?></td>
            <td>
            <?php
            $from_user = $DataRows_Row->from_user;
            $UserData = $this->M_usr_users->GetRow($from_user);
            echo $UserData->fullname;
            ?>
            </td>
            <td>
            <?php
            $to_user = $DataRows_Row->to_user;
            $UserData = $this->M_usr_users->GetRow($to_user);
            echo $UserData->fullname;
            ?>
            </td>
            <td><?php echo $DataRows_Row->start_date." ".$DataRows_Row->end_date;?></td>
            <td>
            <a href="<?php echo base_url().$Segment1."/".$Segment2."/view/".$DataRows_Row->id;?>"><?php echo $DataRows_Row->details;?></a><br>
            <?php echo $TypeTitle." : ".$ProjectTitle;?>
            </td>
            <td style="text-align:center">
                <?php
                $done = $DataRows_Row->done;
                if($done == 1)
                {
                    echo $this->lang->line('Yes');
                }
                else
                {
                    echo $this->lang->line('No');
                    echo "<br>";
                    echo "<a href='".base_url().$Segment1."/".$Segment2."/do_done/".$DataRows_Row->id."'>";
                    echo "<i class='flaticon-like text-primary icon-lg'></i>";
                    echo "</a>";
                }
                ?>
            </td>
            <td style="text-align:center">
            <?php
            if($done == 0) {
            ?>
                <a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
                    <i class="flaticon-edit-1 text-primary icon-lg"></i> 
                </a>
            <?php } ?>
            </td>
            <td style="text-align:center">
                <?php
                    if($DataRows_Row->deleted == 0) {
                ?>
                <a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
                    <i class="flaticon-delete text-danger icon-lg"></i> 
                </a>
                <?php } ?>
            </td>
            <td style="text-align:center">
                <?php
                    if($DataRows_Row->deleted == 1) {
                ?>
                    <a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
                        <i class="flaticon-refresh text-success icon-lg"></i> 
                    </a>
                <?php } ?>
            </td>
        </tr>
        <?php
        }
        ?>
    </tbody>
    
</table>