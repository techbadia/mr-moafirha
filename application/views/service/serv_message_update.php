<!--begin::Header-->
<div class="d-flex align-items-center justify-content-between flex-wrap card-spacer-x py-5">
    <!--begin::Title-->
    <div class="d-flex align-items-center mr-2 py-2">
        <div class="font-weight-bold font-size-h3 mr-3"><?php echo lang('serv_message_update');?></div>
    </div>
    <!--end::Title-->
</div>
<!--end::Header-->
<!--begin::Reply-->
<div class="card-spacer mb-3" id="kt_inbox_reply" style="padding-right:5px !important; padding-left:5px !important;">
    <div class="card card-custom shadow-sm">
        <div class="card-body p-0">
            <!--begin::Form-->
            <form id="kt_inbox_reply_form" action="<?php echo base_url().$Segment1."/".$Segment2."/Update_Data";?>" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                <!--begin::Body-->
                <div class="form-body" style="padding-right:15px !important; padding-left:15px !important;">
                    <!--begin::To-->
                    <div class="form-group row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <label class="col-form-label"><?php echo lang('serv_message_to_user');?> : </label>
                            <select name="to_user[]" class="form-control kt-selectpicker" data-live-search="true" multiple="multiple" size="8" required>
                                <?php
                                $UsersList = $this->M_usr_users->GetMultiRow();
                                foreach($UsersList as $UsersList_Row) {
                                    $UserFullName = $UsersList_Row->fullname;
                                ?>
                                <option value="<?php echo $UsersList_Row->id;?>"><?php echo $UserFullName;?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <label class="col-form-label"><?php echo lang('serv_message_subject');?> : </label>
                            <input type="text" name="subject" class="form-control" value="<?php echo $subject;?>" required>
                        </div>
                    </div>
                    <!--end::To-->
                    <div class="form-group row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <label class="col-form-label"><?php echo lang('serv_message_message');?> : </label>
                            <textarea id="kt-tinymce-4" name="kt-tinymce-4" class="tox-target"><?php echo $message;?></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line('serv_message_image');?> : 
                            <?php
                            $Attache = $image;
                            if($Attache != "") {
                            ?>
                            <a target="_blank" href="<?php echo base_url()."upload/serv_message/".$image;?>">
                            <i class="icon-2x text-dark-50 flaticon-attachment"></i>
                            </a>
                            <?php
                            }
                            ?>
                            </label>
                            <input type="file" name="image" id="image" class="form-control" dir="ltr">
                        </div>
                    </div>
                </div>
                <!--end::Body-->
                <div class="kt-portlet__foot" style="padding-right:15px !important; padding-left:15px !important;">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-lg-12 ml-lg-auto">
                                <input type="hidden" name="id" value="<?php echo $id;?>">
                                <input type="hidden" name="deleted" value="<?php echo $deleted;?>">
                                <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                                <button type="submit" class="btn btn-primary mr-2" name="action" value="Send"><?php echo $this->lang->line('serv_message_submit');?></button>
                            </div>
                        </div>
                    </div>
                </div><br><br>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>
<!--end::Reply-->