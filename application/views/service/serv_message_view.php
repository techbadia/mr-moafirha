<br>
    <div class="d-flex align-items-center p-4 bg-dark" style="text-align:center !important">
        <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-transparent-success font-weight-bold mr-2">
        <i class="flaticon-email"></i> <?php echo lang('serv_message_index');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/sent/";?>" class="btn btn-transparent-warning font-weight-bold mr-2">
        <i class="flaticon2-send-1"></i> <?php echo lang('serv_message_sent');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/marked/";?>" class="btn btn-transparent-primary font-weight-bold mr-2">
        <i class="flaticon-star"></i> <?php echo lang('serv_message_marked');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/draft/";?>" class="btn btn-transparent-danger font-weight-bold mr-2">
        <i class="flaticon-edit"></i> <?php echo lang('serv_message_draft');?>
        </a>
        
        <!--
        <a href="#" class="btn btn-transparent-warning font-weight-bold mr-2">Warning</a>
        <a href="#" class="btn btn-transparent-white font-weight-bold">White</a>
        -->
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/deleted/";?>" class="btn btn-transparent-white font-weight-bold" style="position: absolute !important; <?php echo $AnotherAlign;?>: 25px !important;">
            <i class="flaticon-delete"></i> <?php echo lang('serv_message_trash');?>
        </a>
    </div>
<br>


<!--begin::Body-->
<div class="card-body p-0">
    <!--begin::Header-->
    <div class="d-flex align-items-center justify-content-between flex-wrap card-spacer-x py-5">
        <!--begin::Title-->
        <div class="d-flex align-items-center mr-2 py-2">
            <div class="font-weight-bold font-size-h3 mr-3"><?php echo $subject;?></div>
        </div>
        <!--end::Title-->
    </div>
    <!--end::Header-->
    <!--begin::Messages-->
    <div class="mb-3">
        <div class="cursor-pointer shadow-xs toggle-on" data-inbox="message">
            <!--begin::Message Heading-->
            <div class="d-flex card-spacer-x py-6 flex-column flex-md-row flex-lg-column flex-xxl-row justify-content-between">
                <div class="d-flex align-items-center">
                    <span class="symbol symbol-50 mr-4">
                        <span class="symbol-label" style="background-image: url('<?php echo base_url()."upload/usr_users/".$SenderImage;?>')"></span>
                    </span>
                    <div class="d-flex flex-column flex-grow-1 flex-wrap mr-2">
                        <div class="d-flex">
                            <a href="#" class="font-size-lg font-weight-bolder text-dark-75 text-hover-primary mr-2">
                            <?php echo $SenderFullName;?>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="d-flex my-2 my-xxl-0 align-items-md-center align-items-lg-start align-items-xxl-center flex-column flex-md-row flex-lg-column flex-xxl-row">
                    <div class="font-weight-bold text-muted mx-2">
                    <?php echo $thedate." ".$thetime;?>
                    <?php
                    if($marked == 1)
                    {
                        echo " <a href='".base_url().$Segment1."/".$Segment2."/remove_mark/".$id."'><i class='icon-md flaticon-star text-danger'></i></a>";
                    }
                    else
                    {
                        echo " <a href='".base_url().$Segment1."/".$Segment2."/add_mark/".$id."'><i class='icon-md flaticon-star text-light'></i></a>";
                    }
                    ?> 
                    </div>
                </div>
            </div>
            <!--end::Message Heading-->
            <div class="card-spacer-x py-3 toggle-off-item">
                <?php echo $message;?><br><br>
            </div>
        </div>
    </div>
    <!--end::Messages-->
    <?php
    $MyID = intval($this->session->userdata('StaffID'));
    if($from_user <> $MyID) {
    ?>
    <!--begin::Header-->
    <div class="d-flex align-items-center justify-content-between flex-wrap card-spacer-x py-5">
        <!--begin::Title-->
        <div class="d-flex align-items-center mr-2 py-2">
            <div class="font-weight-bold font-size-h3 mr-3"><?php echo lang('serv_message_send_replay');?></div>
        </div>
        <!--end::Title-->
    </div>
    <!--end::Header-->
    <!--begin::Reply-->
    <div class="card-spacer mb-3" id="kt_inbox_reply" style="padding-right:5px !important; padding-left:5px !important;">
        <div class="card card-custom shadow-sm">
            <div class="card-body p-0">
                <!--begin::Form-->
                <form id="kt_inbox_reply_form" action="<?php echo base_url().$Segment1."/".$Segment2."/insert_data";?>" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                    <!--begin::Body-->
                    <div class="form-body" style="padding-right:15px !important; padding-left:15px !important;">
                        <!--begin::To-->
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_message_to_user');?> : </label>
                                <select name="to_user[]" class="form-control kt-selectpicker" data-live-search="true" multiple="multiple" size="8" required>
                                    <?php
                                    $UsersList = $this->M_usr_users->GetMultiRow();
                                    foreach($UsersList as $UsersList_Row) {
                                        $UserFullName = $UsersList_Row->fullname;
                                    ?>
                                    <option <?php if($from_user == $UsersList_Row->id) {?>selected<?php }?> value="<?php echo $UsersList_Row->id;?>"><?php echo $UserFullName;?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_message_subject');?> : </label>
                                <input type="text" name="subject" class="form-control" value="" required>
                            </div>
                        </div>
                        <!--end::To-->
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_message_message');?> : </label>
                                <textarea id="kt-tinymce-4" name="kt-tinymce-4" class="tox-target"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-md-12">
                                <label><?php echo $this->lang->line('serv_message_image');?>:</label>
                                <input type="file" name="image" id="image" class="form-control" dir="ltr">
                            </div>
                        </div>
                    </div>
                    <!--end::Body-->
                    <div class="kt-portlet__foot" style="padding-right:15px !important; padding-left:15px !important;">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-12 ml-lg-auto">
                                    <input type="hidden" name="id" value="<?php echo $id;?>">
                                    <input type="hidden" name="deleted" value="0">
                                    <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                                    <button type="submit" class="btn btn-info mr-2" name="action" value="SaveDraft"><?php echo $this->lang->line('serv_message_save_draft');?></button>
                                    <button type="submit" class="btn btn-primary mr-2" name="action" value="Send"><?php echo $this->lang->line('serv_message_submit');?></button>
                                </div>
                            </div>
                        </div>
                    </div><br><br>
                </form>
                <!--end::Form-->
            </div>
        </div>
    </div>
    <!--end::Reply-->
    <?php
    }
    ?>
</div>
<!--end::Body-->