<br>
    <div class="d-flex align-items-center p-4 bg-dark" style="text-align:center !important">
    <?php
        $BS = "padding-right: 5px !important; padding-left:5px !important;";
        ?>
        <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-transparent-success font-weight-bold mr-2" style="<?php echo $BS?>">
        <i class="flaticon-doc"></i> <?php echo lang('serv_action_all');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/done/";?>" class="btn btn-transparent-primary font-weight-bold mr-2" style="<?php echo $BS?>">
        <i class="flaticon2-check-mark"></i> <?php echo lang('serv_action_done');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/onhold/";?>" class="btn btn-transparent-warning font-weight-bold mr-2" style="<?php echo $BS?>">
        <i class="flaticon-alert-2"></i> <?php echo lang('serv_action_onhold');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/expired_onhold/";?>" class="btn btn-transparent-danger font-weight-bold mr-2" style="<?php echo $BS?>">
        <i class="flaticon-danger"></i> <?php echo lang('serv_action_expired_onhold');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/expired_done/";?>" class="btn btn-transparent-primary font-weight-bold mr-2" style="<?php echo $BS?>">
        <i class="flaticon-interface-10"></i> <?php echo lang('serv_action_expired_done');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/coming/";?>" class="btn btn-transparent-success font-weight-bold mr-2" style="<?php echo $BS?>">
        <i class="flaticon-clock-1"></i> <?php echo lang('serv_action_coming');?>
        </a>
        
        <!--
        <a href="#" class="btn btn-transparent-warning font-weight-bold mr-2">Warning</a>
        <a href="#" class="btn btn-transparent-white font-weight-bold">White</a>
        -->
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/deleted/";?>" class="btn btn-transparent-white font-weight-bold" style="position: absolute !important; <?php echo $AnotherAlign;?>: 25px !important;">
            <i class="flaticon-delete"></i> <?php echo lang('Deleted');?>
        </a>
    </div>
<br>

<?php
$TypeData = $this->M_serv_action_types->GetRow($type_id);
if ($this->session->userdata('lang') == "ar")
{
    $TypeTitle = $TypeData->ar_title;
}
else
{
    $TypeTitle = $TypeData->en_title;
}

$ProjectData = $this->M_proj_project->GetRow($project_id);
if ($this->session->userdata('lang') == "ar")
{
    $ProjectTitle = $ProjectData->ar_title;
}
else
{
    $ProjectTitle = $ProjectData->en_title;
}

$UserData = $this->M_usr_users->GetRow($from_user);
$FromUserName = $UserData->fullname;

$UserData = $this->M_usr_users->GetRow($to_user);
$ToUserName = $UserData->fullname;
?>
<!--begin::Body-->
<div class="card-body p-0">
    <!--begin::Header-->
    <div class="d-flex align-items-center justify-content-between flex-wrap card-spacer-x py-5">
        <!--begin::Title-->
        <div class="d-flex align-items-center mr-2 py-2">
            <div class="font-weight-bold font-size-h3 mr-3"><?php echo $TypeTitle." : ".$ProjectTitle;?></div>
        </div>
        <!--end::Title-->
    </div>
    <!--end::Header-->
    <!--begin::Messages-->
    <div class="mb-3">
        <div class="cursor-pointer shadow-xs toggle-on" data-inbox="message">
            <!--begin::Message Heading-->
            <div class="d-flex card-spacer-x py-6 flex-column flex-md-row flex-lg-column flex-xxl-row justify-content-between">
                <div class="d-flex my-2 my-xxl-0 align-items-md-center align-items-lg-start align-items-xxl-center flex-column flex-md-row flex-lg-column flex-xxl-row">
                    <div class="font-weight-bold text-muted mx-2">
                    <?php echo lang('serv_action_from_user')." : ".$FromUserName." ".lang('serv_action_to_user')." : ".$ToUserName;?><br>
                    <?php echo lang('serv_action_start_date')." : ".$start_date." ".lang('serv_action_end_date')." : ".$end_date;?><br>
                    <?php echo lang('serv_action_add_date')." : ".$add_date." ".lang('serv_action_add_time')." : ".$add_time;?><br>
                    </div>
                </div>
            </div>
            <!--end::Message Heading-->
            <div class="card-spacer-x py-3 toggle-off-item">
                <h3><?php echo lang('serv_action_details');?></h3>
                <?php echo $details;?><br><br>
                <?php echo lang('serv_action_done');?> : <?php
                if($done == 1)
                {
                    echo $this->lang->line('Yes');
                }
                else
                {
                    echo $this->lang->line('No');
                    if($to_user == intval($this->session->userdata('StaffID')))
                    {
                        echo " ";
                        echo "<a href='".base_url().$Segment1."/".$Segment2."/do_done/".$DataRows_Row->id."'>";
                        echo "<i class='flaticon-like text-primary icon-lg'></i>";
                        echo "</a>";
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <!--end::Messages-->
</div>
<!--end::Body-->