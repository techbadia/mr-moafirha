<br>
    <div class="d-flex align-items-center p-4 bg-dark" style="text-align:center !important">
        <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-transparent-success font-weight-bold mr-2">
        <i class="flaticon2-bell-4"></i> <?php echo lang('serv_events_all');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/events_old/";?>" class="btn btn-transparent-primary font-weight-bold mr-2">
        <i class="flaticon-bell-1"></i> <?php echo lang('serv_events_old');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/events_current/";?>" class="btn btn-transparent-warning font-weight-bold mr-2">
        <i class="flaticon-alert-2"></i> <?php echo lang('serv_events_current');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/events_upcoming/";?>" class="btn btn-transparent-danger font-weight-bold mr-2">
        <i class="flaticon-alarm-1"></i> <?php echo lang('serv_events_upcoming');?>
        </a>
        
        <!--
        <a href="#" class="btn btn-transparent-warning font-weight-bold mr-2">Warning</a>
        <a href="#" class="btn btn-transparent-white font-weight-bold">White</a>
        -->
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/deleted/";?>" class="btn btn-transparent-white font-weight-bold" style="position: absolute !important; <?php echo $AnotherAlign;?>: 25px !important;">
            <i class="flaticon-delete"></i> <?php echo lang('Deleted');?>
        </a>
    </div>
<br>
<!--begin: Datatable -->
<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
    <thead>
        <tr>
            <th>ID</th>
            <th><?php echo lang('serv_events_start_date');?></th>
            <th><?php echo lang('serv_events_end_date');?></th>
            <th><?php echo lang('serv_events_details');?></th>
            <th><?php echo lang('serv_events_count');?></th>
            <th><?php echo lang('serv_events_scheduled');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($DataRows as $DataRows_Row) {
            $scheduled = $DataRows_Row->scheduled;
            $scheduled_month = $DataRows_Row->scheduled_month;
            $scheduled_year = $DataRows_Row->scheduled_year;
            $event_id = $DataRows_Row->id;
        ?>
        <tr>
            <td><?php echo $DataRows_Row->id;?></td>
            <td><?php echo $DataRows_Row->start_date;?></td>
            <td><?php echo $DataRows_Row->end_date;?></td>
            <td><?php echo $DataRows_Row->details;?></td>
            <td>
            <?php
            $Invitees = $this->M_serv_events_users->Get_Count($event_id);
            echo $Invitees;
            ?>
            </td>
            <td>
            <?php
            if($scheduled == 1)
            {
                if($scheduled_month > 0)
                {
                    echo $this->lang->line('serv_events_scheduled_month');
                }
                if($scheduled_year > 0)
                {
                    echo " ".$this->lang->line('serv_events_scheduled_year');
                }
            }
            else
            {
                echo $this->lang->line('No');
            }
            ?>
            </td>
            <td style="text-align:center">
                <a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
                    <i class="flaticon-edit-1 text-primary icon-lg"></i> 
                </a>
            </td>
            <td style="text-align:center">
                <?php
                    if($DataRows_Row->deleted == 0) {
                ?>
                <a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
                    <i class="flaticon-delete text-danger icon-lg"></i> 
                </a>
                <?php } ?>
            </td>
            <td style="text-align:center">
                <?php
                    if($DataRows_Row->deleted == 1) {
                ?>
                    <a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
                        <i class="flaticon-refresh text-success icon-lg"></i> 
                    </a>
                <?php } ?>
            </td>
        </tr>
        <?php
        }
        ?>
    </tbody>
    
</table>