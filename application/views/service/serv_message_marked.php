<br>
    <div class="d-flex align-items-center p-4 bg-dark" style="text-align:center !important">
        <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-transparent-success font-weight-bold mr-2">
        <i class="flaticon-email"></i> <?php echo lang('serv_message_index');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/sent/";?>" class="btn btn-transparent-warning font-weight-bold mr-2">
        <i class="flaticon2-send-1"></i> <?php echo lang('serv_message_sent');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/marked/";?>" class="btn btn-transparent-primary font-weight-bold mr-2">
        <i class="flaticon-star"></i> <?php echo lang('serv_message_marked');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/draft/";?>" class="btn btn-transparent-danger font-weight-bold mr-2">
        <i class="flaticon-edit"></i> <?php echo lang('serv_message_draft');?>
        </a>
        
        <!--
        <a href="#" class="btn btn-transparent-warning font-weight-bold mr-2">Warning</a>
        <a href="#" class="btn btn-transparent-white font-weight-bold">White</a>
        -->
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/deleted/";?>" class="btn btn-transparent-white font-weight-bold" style="position: absolute !important; <?php echo $AnotherAlign;?>: 25px !important;">
            <i class="flaticon-delete"></i> <?php echo lang('serv_message_trash');?>
        </a>
    </div>
<br>
<!--begin: Datatable -->
<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
    <thead>
        <tr>
            <th>ID</th>
            <th><?php echo lang('serv_message_subject');?></th>
            <th><?php echo lang('serv_message_from_user');?></th>
            <th><?php echo lang('serv_message_thedate');?></th>
            <th><?php echo lang('serv_message_image');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('serv_message_replay');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($DataRows as $DataRows_Row) {
            $store_id = $DataRows_Row->id;
            $readed = $DataRows_Row->readed;
            $marked = $DataRows_Row->marked;
        ?>
        <tr>
            <td><?php echo $DataRows_Row->id;?></td>
            <td>
            <?php
            if($marked == 1)
            {
                echo "<a href='".base_url().$Segment1."/".$Segment2."/remove_mark/".$DataRows_Row->id."'><i class='icon-md flaticon-star text-danger'></i></a>";
            }
            else
            {
                echo "<a href='".base_url().$Segment1."/".$Segment2."/add_mark/".$DataRows_Row->id."'><i class='icon-md flaticon-star text-light'></i></a>";
            }
            ?>
            <?php
            if($readed == 1)
            {
                echo "<a href='".base_url().$Segment1."/".$Segment2."/view/".$DataRows_Row->id."'>".$DataRows_Row->subject."</a>";
            }
            else
            {
                echo "<strong><a href='".base_url().$Segment1."/".$Segment2."/view/".$DataRows_Row->id."'>".$DataRows_Row->subject."</a></strong>";
            }
            ?>
            </td>
            <td>
            <?php
            $from_user = $DataRows_Row->from_user;
            $UserData = $this->M_usr_users->GetRow($from_user);
            if($readed == 1)
            {
                echo $UserData->fullname;
            }
            else
            {
                echo "<strong>".$UserData->fullname."</strong>";
            }
            ?>
            </td>
            <td>
            <?php
            if($readed == 1)
            {
                echo $DataRows_Row->thedate." ".$DataRows_Row->thetime;
            }
            else
            {
                echo "<strong>".$DataRows_Row->thedate." ".$DataRows_Row->thetime."</strong>";
            }
            ?>
            </td>
            <td>
            <?php
            $Attache = $DataRows_Row->image;
            if($Attache != "") {
            ?>
            <a target="_blank" href="<?php echo base_url()."upload/serv_message/".$DataRows_Row->image;?>">
            <i class="icon-lg text-dark-50 flaticon-attachment"></i>
            </a>
            <?php
            }
            ?>
            </td>
            <td style="text-align:center">
                <a href="<?php echo base_url().$Segment1."/".$Segment2."/reply/".$DataRows_Row->id;?>">
                    <i class="flaticon-reply text-primary icon-lg"></i> 
                </a>
            </td>
            <td style="text-align:center">
                <?php
                    if($DataRows_Row->deleted == 0) {
                ?>
                <a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
                    <i class="flaticon-delete text-danger icon-lg"></i> 
                </a>
                <?php } ?>
            </td>
            <td style="text-align:center">
                <?php
                    if($DataRows_Row->deleted == 1) {
                ?>
                    <a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
                        <i class="flaticon-refresh text-success icon-lg"></i> 
                    </a>
                <?php } ?>
            </td>
        </tr>
        <?php
        }
        ?>
    </tbody>
    
</table>