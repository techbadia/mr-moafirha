<br>
    <div class="d-flex align-items-center p-4 bg-dark" style="text-align:center !important">
        <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-transparent-success font-weight-bold mr-2">
        <i class="flaticon2-bell-4"></i> <?php echo lang('serv_events_all');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/events_old/";?>" class="btn btn-transparent-primary font-weight-bold mr-2">
        <i class="flaticon-bell-1"></i> <?php echo lang('serv_events_old');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/events_current/";?>" class="btn btn-transparent-warning font-weight-bold mr-2">
        <i class="flaticon-alert-2"></i> <?php echo lang('serv_events_current');?>
        </a>
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/events_upcoming/";?>" class="btn btn-transparent-danger font-weight-bold mr-2">
        <i class="flaticon-alarm-1"></i> <?php echo lang('serv_events_upcoming');?>
        </a>
        
        <!--
        <a href="#" class="btn btn-transparent-warning font-weight-bold mr-2">Warning</a>
        <a href="#" class="btn btn-transparent-white font-weight-bold">White</a>
        -->
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/deleted/";?>" class="btn btn-transparent-white font-weight-bold" style="position: absolute !important; <?php echo $AnotherAlign;?>: 25px !important;">
            <i class="flaticon-delete"></i> <?php echo lang('Deleted');?>
        </a>
    </div>
<br>


<!--begin::Body-->
<div class="card-body p-0">
    <!--begin::Header-->
    <div class="d-flex align-items-center justify-content-between flex-wrap card-spacer-x py-5">
        <!--begin::Title-->
        <div class="d-flex align-items-center mr-2 py-2">
            <div class="font-weight-bold font-size-h3 mr-3"><?php echo lang('serv_events_details');?></div>
        </div>
        <!--end::Title-->
    </div>
    <!--end::Header-->
    <!--begin::Messages-->
    <div class="mb-3">
        <div class="cursor-pointer shadow-xs toggle-on" data-inbox="message">
            <!--begin::Message Heading-->
            <div class="d-flex card-spacer-x py-6 flex-column flex-md-row flex-lg-column flex-xxl-row justify-content-between">
                <div class="d-flex my-2 my-xxl-0 align-items-md-center align-items-lg-start align-items-xxl-center flex-column flex-md-row flex-lg-column flex-xxl-row">
                    <div class="font-weight-bold text-muted mx-2">
                    <?php echo $start_date." ".$end_date;?>
                    </div>
                </div>
            </div>
            <!--end::Message Heading-->
            <div class="card-spacer-x py-3 toggle-off-item">
                <?php echo $details;?><br><br>
            </div>
        </div>
    </div>
    <!--end::Messages-->
</div>
<!--end::Body-->