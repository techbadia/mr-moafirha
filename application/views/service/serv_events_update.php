<div class="card card-custom gutter-b example example-compact">
    <?php
        $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
        echo form_open_multipart($FormPath);
    ?>
                    <div class="form-body">
                        <div class="form-group row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_events_start_date');?></label>
                                <input type="date" name="start_date" class="form-control" value="<?php echo $start_date;?>" required>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_events_end_date');?></label>
                                <input type="date" name="end_date" class="form-control" value="<?php echo $end_date;?>" required>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_events_invitees');?> : </label>
                                <select name="to_user[]" class="form-control kt-selectpicker" data-live-search="true" multiple="multiple" size="8" required>
                                    <?php
                                    $UsersList = $this->M_usr_users->GetMultiRow();
                                    foreach($UsersList as $UsersList_Row) {
                                        $UserFullName = $UsersList_Row->fullname;
                                        $user_id = $UsersList_Row->id;
                                        $CheckEvent_User = $this->M_serv_events_users->CountMyEvents($event_id, $user_id);
                                    ?>
                                    <option <?php if($CheckEvent_User > 0) {?>selected<?php }?> value="<?php echo $UsersList_Row->id;?>"><?php echo $UserFullName;?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_events_details');?> : </label>
                                <textarea id="kt-tinymce-4" name="kt-tinymce-4" class="tox-target"><?php echo $details;?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_events_is_scheduled');?></label>
                                <select name="scheduled" class="form-control" required>
                                    <option <?php if($scheduled == 1) {?>selected<?php }?> value="1"><?php echo lang('Yes');?></option>
                                    <option <?php if($scheduled == 0) {?>selected<?php }?> value="0"><?php echo lang('No');?></option>
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_events_scheduled_month');?></label>
                                <select name="scheduled_month" class="form-control" required>
                                    <option <?php if($scheduled_month == 1) {?>selected<?php }?> value="1"><?php echo lang('Yes');?></option>
                                    <option <?php if($scheduled_month == 0) {?>selected<?php }?> value="0"><?php echo lang('No');?></option>
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_events_scheduled_year');?></label>
                                <select name="scheduled_year" class="form-control" required>
                                    <option <?php if($scheduled_year == 1) {?>selected<?php }?> value="1"><?php echo lang('Yes');?></option>
                                    <option <?php if($scheduled_year == 0) {?>selected<?php }?> value="0"><?php echo lang('No');?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-12 ml-lg-auto">
                                    <input type="hidden" name="id" value="<?php echo $id;?>">
                                    <input type="hidden" name="company_id" value="<?php echo $company_id;?>">
                                    <input type="hidden" name="deleted" value="<?php echo $deleted;?>">

                                    <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                                    <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
        echo form_close();
    ?>
</div>