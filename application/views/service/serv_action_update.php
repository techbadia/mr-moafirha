<div class="card card-custom gutter-b example example-compact">
    <?php
        $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
        echo form_open_multipart($FormPath);
    ?>
                    <div class="form-body">
                        <div class="form-group row">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_action_to_user');?></label>
                                <select name="to_user" class="form-control kt-selectpicker" data-live-search="true" size="8" required>
                                    <?php
                                    $from_user = intval($this->session->userdata('StaffID'));
                                    $UsersList = $this->M_usr_users->GetMultiRow();
                                    foreach($UsersList as $UsersList_Row) {
                                        $UserFullName = $UsersList_Row->fullname;
                                        if($UsersList_Row->id <> $from_user) {
                                    ?>
                                    <option <?php if($to_user == $UsersList_Row->id) {?>selected<?php }?> value="<?php echo $UsersList_Row->id;?>"><?php echo $UserFullName;?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_action_type_id');?></label>
                                <select name="type_id" class="form-control" required>
                                    <?php
                                    $TypeList = $this->M_serv_action_types->GetMultiRow();
                                    foreach($TypeList as $TypeList_Row) {
                                        if ($this->session->userdata('lang') == "ar")
                                        {
                                            $TypeTitle = $TypeList_Row->ar_title;
                                        }
                                        else
                                        {
                                            $TypeTitle = $TypeList_Row->en_title;
                                        }
                                    ?>
                                    <option <?php if($type_id == $TypeList_Row->id) {?>selected<?php }?> value="<?php echo $TypeList_Row->id;?>"><?php echo $TypeTitle;?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_action_project_id');?> : </label>
                                <select name="project_id" class="form-control kt-selectpicker" data-live-search="true" size="8" required>
                                    <?php
                                    $ProjectList = $this->M_proj_project->GetMultiRow();
                                    foreach($ProjectList as $ProjectList_Row) {
                                        if ($this->session->userdata('lang') == "ar")
                                        {
                                            $ProjectTitle = $ProjectList_Row->ar_title;
                                        }
                                        else
                                        {
                                            $ProjectTitle = $ProjectList_Row->en_title;
                                        }
                                    ?>
                                    <option <?php if($project_id == $ProjectList_Row->id) {?>selected<?php }?> value="<?php echo $ProjectList_Row->id;?>"><?php echo $ProjectTitle;?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_action_start_date');?></label>
                                <input type="date" name="start_date" class="form-control" value="<?php echo $start_date;?>" min="<?php echo date("Y-m-d");?>" required>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_action_end_date');?></label>
                                <input type="date" name="end_date" class="form-control" value="<?php echo $end_date;?>" min="<?php echo date("Y-m-d");?>" required>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <label class="col-form-label"><?php echo lang('serv_action_details');?> : </label>
                                <textarea id="details" name="details" class="form-control" required><?php echo $details;?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-12 ml-lg-auto">
                                    <input type="hidden" name="id" value="<?php echo $id;?>">
                                    <input type="hidden" name="company_id" value="<?php echo $company_id;?>">
                                    <input type="hidden" name="deleted" value="<?php echo $deleted;?>">
                                    <input type="hidden" name="done" value="<?php echo $done;?>">
                                    <input type="hidden" name="add_date" value="<?php echo $add_date;?>">
                                    <input type="hidden" name="add_time" value="<?php echo $add_time;?>">
                                    <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                                    <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
        echo form_close();
    ?>
</div>