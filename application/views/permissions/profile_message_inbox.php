<!--begin::Container-->
<div class="container">
    <!--begin::Profile Overview-->
    <div class="d-flex flex-row">
        <!--begin::Aside-->
        <div class="flex-row-auto offcanvas-mobile w-200px w-xxl-275px" id="kt_inbox_aside">
            <!--begin::Profile Card-->
            <div class="card card-custom card-stretch">
                <!--begin::Body-->
                <div class="card-body pt-4">
                    <!--begin::Toolbar-->
                    <div class="d-flex justify-content-end">
                        <div class="dropdown dropdown-inline">
                            <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ki ki-bold-more-hor"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                <!--begin::Navigation-->
                                <?php include("profile_dropdown_menu.php");?>
                                <!--end::Navigation-->
                            </div>
                        </div>
                    </div>
                    <!--end::Toolbar-->
                    <!--begin::User-->
                    <div class="d-flex align-items-center">
                        <div class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
                            <div class="symbol-label" style="background-image:url('<?php echo base_url()."upload/usr_users/".$Profile_image;?>')"></div>
                            <i class="symbol-badge bg-success"></i>
                        </div>
                    </div>
                    <div class="d-flex align-items-center">
                        <div>
                            <a href="#" class="font-weight-bolder font-size-h5 text-dark-75 text-hover-primary">
                            <?php echo $Profile_fullname;?>
                            </a>
                        </div>
                    </div>
                    <!--end::User-->
                    <!--begin::Contact-->
                    <div class="py-9">
                        <div class="d-flex align-items-center justify-content-between mb-2">
                            <span class="font-weight-bold mr-2"><?php echo $this->lang->line('Email');?>:</span>
                        </div>
                        <div class="d-flex align-items-center justify-content-between mb-2">
                            <a href="#" class="text-muted text-hover-primary"><?php echo $Profile_email;?></a>
                        </div>
                        <div class="d-flex align-items-center justify-content-between mb-2">
                            <span class="font-weight-bold mr-2"><?php echo $this->lang->line('Phone');?>:</span>
                            <span class="text-muted"><?php echo $Profile_phone;?></span>
                        </div>
                    </div>
                    <!--end::Contact-->
                    <!--begin::Nav-->
                    <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                        <div class="navi-item mb-2">
                            <a href="<?php echo base_url()."permissions/profile/";?>" class="navi-link py-4" style="padding-right:5px; padding-left:5px;">
                                <span class="navi-icon mr-2">
                                    <span class="svg-icon">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Layers.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24" />
                                                <path d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z" fill="#000000" />
                                                <circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5" />
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>
                                </span>
                                <span class="navi-text font-size-lg"><?php echo $this->lang->line('profile_UserProfile');?></span>
                            </a>
                        </div>
                        <div class="navi-item mb-2">
                            <a href="<?php echo base_url()."permissions/profile/serv_message";?>" class="navi-link py-4 active" style="padding-right:5px; padding-left:5px;">
                                <span class="navi-icon mr-2">
                                    <span class="svg-icon">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <path d="M5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000"/>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>
                                </span>
                                <span class="navi-text font-size-lg"><?php echo $this->lang->line('serv_message');?></span>
                            </a>
                        </div>
                        <!--
                        <div class="navi-item mb-2">
                            <a href="<?php //echo base_url()."permissions/profile/serv_action";?>" class="navi-link py-4" style="padding-right:5px; padding-left:5px;">
                                <span class="navi-icon mr-2">
                                    <span class="svg-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <path d="M12.5936684,18.0752333 L12.5936684,14.4063316 L18.5936684,14.4063316 L18.5936684,8.40633161 L12.5936684,8.40633161 L12.5936684,4.73208782 L5.92209543,11.3984411 L12.5936684,18.0752333 Z M14.5936684,6.40633161 L19.5936684,6.40633161 C20.1459531,6.40633161 20.5936684,6.85404686 20.5936684,7.40633161 L20.5936684,15.4063316 C20.5936684,15.9586164 20.1459531,16.4063316 19.5936684,16.4063316 L14.5936684,16.4063316 L14.5936684,21.0946697 C14.5936684,21.2936837 14.5145702,21.484538 14.3737911,21.6252071 C14.0807833,21.9179858 13.6059096,21.9178001 13.313131,21.6247924 L3.62379074,11.9278722 C3.33101216,11.6348644 3.33119799,11.1599907 3.6242058,10.8672121 L13.3135459,1.18545264 C13.4541812,1.04492729 13.6448577,0.965990217 13.8436684,0.965990217 C14.257882,0.965990217 14.5936684,1.30177665 14.5936684,1.71599022 L14.5936684,6.40633161 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.998998, 11.405330) scale(-1, 1) rotate(-270.000000) translate(-11.998998, -11.405330) "/>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                                <span class="navi-text font-size-lg"><?php //echo $this->lang->line('serv_action');?></span>
                            </a>
                        </div>
                        <div class="navi-item mb-2">
                            <a href="<?php //echo base_url()."permissions/profile/tas_tasks";?>" class="navi-link py-4" style="padding-right:5px; padding-left:5px;">
                                <span class="navi-icon mr-2">
                                    <span class="svg-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <path d="M8.46446609,11.2928932 L7.40380592,10.232233 C7.20854378,10.0369709 7.20854378,9.72038841 7.40380592,9.52512627 L8.1109127,8.81801948 C8.30617485,8.62275734 8.62275734,8.62275734 8.81801948,8.81801948 L15.1819805,15.1819805 C15.3772427,15.3772427 15.3772427,15.6938252 15.1819805,15.8890873 L14.4748737,16.5961941 C14.2796116,16.7914562 13.9630291,16.7914562 13.767767,16.5961941 L12.7071068,15.5355339 L7.05025253,21.1923882 C6.26920395,21.9734367 5.00287399,21.9734367 4.22182541,21.1923882 L2.80761184,19.7781746 C2.02656326,18.997126 2.02656326,17.7307961 2.80761184,16.9497475 L8.46446609,11.2928932 Z M4.5753788,18.0104076 C4.38011665,18.2056698 4.38011665,18.5222523 4.5753788,18.7175144 C4.77064094,18.9127766 5.08722343,18.9127766 5.28248558,18.7175144 L9.52512627,14.4748737 C9.72038841,14.2796116 9.72038841,13.9630291 9.52512627,13.767767 C9.32986412,13.5725048 9.01328163,13.5725048 8.81801948,13.767767 L4.5753788,18.0104076 Z" fill="#000000" opacity="0.3"/>
                                                <path d="M16.9497475,5.63603897 L16.7788182,5.4651097 C16.5835561,5.26984755 16.5835561,4.95326506 16.7788182,4.75800292 C16.8266988,4.71012232 16.8838059,4.67246608 16.9466763,4.64731796 L19.4720576,3.63716542 C19.657766,3.56288206 19.869875,3.60641908 20.0113063,3.74785037 L20.2521496,3.98869366 C20.3935809,4.13012495 20.4371179,4.342234 20.3628346,4.52794239 L19.352682,7.05332375 C19.2501253,7.30971551 18.9591401,7.43442346 18.7027484,7.33186676 C18.6398781,7.30671864 18.5827709,7.2690624 18.5348903,7.2211818 L18.363961,7.05025253 L12.7071068,12.7071068 L11.2928932,11.2928932 L16.9497475,5.63603897 Z" fill="#000000"/>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                                <span class="navi-text font-size-lg"><?php //echo $this->lang->line('tas_tasks');?></span>
                            </a>
                        </div>
                        <div class="navi-item mb-2">
                            <a href="<?php //echo base_url()."permissions/profile/proj_project_level_task";?>" class="navi-link py-4" style="padding-right:5px; padding-left:5px;">
                                <span class="navi-icon mr-2">
                                    <span class="svg-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <path d="M18.4246212,12.6464466 L21.2530483,9.81801948 C21.4483105,9.62275734 21.764893,9.62275734 21.9601551,9.81801948 L22.6672619,10.5251263 C22.862524,10.7203884 22.862524,11.0369709 22.6672619,11.232233 L19.8388348,14.0606602 C19.6435726,14.2559223 19.3269901,14.2559223 19.131728,14.0606602 L18.4246212,13.3535534 C18.2293591,13.1582912 18.2293591,12.8417088 18.4246212,12.6464466 Z M3.22182541,17.9497475 L13.1213203,8.05025253 C13.5118446,7.65972824 14.1450096,7.65972824 14.5355339,8.05025253 L15.9497475,9.46446609 C16.3402718,9.85499039 16.3402718,10.4881554 15.9497475,10.8786797 L6.05025253,20.7781746 C5.65972824,21.1686989 5.02656326,21.1686989 4.63603897,20.7781746 L3.22182541,19.363961 C2.83130112,18.9734367 2.83130112,18.3402718 3.22182541,17.9497475 Z" fill="#000000" opacity="0.3"/>
                                                <path d="M12.3890873,1.28248558 L12.3890873,1.28248558 C15.150511,1.28248558 17.3890873,3.52106183 17.3890873,6.28248558 L17.3890873,10.7824856 C17.3890873,11.058628 17.1652297,11.2824856 16.8890873,11.2824856 L12.8890873,11.2824856 C12.6129449,11.2824856 12.3890873,11.058628 12.3890873,10.7824856 L12.3890873,1.28248558 Z" fill="#000000" transform="translate(14.889087, 6.282486) rotate(-45.000000) translate(-14.889087, -6.282486) "/>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                                <span class="navi-text font-size-lg"><?php //echo $this->lang->line('proj_project_level_task');?></span>
                            </a>
                        </div>
                        <div class="navi-item mb-2">
                            <a href="<?php //echo base_url()."permissions/profile/schedule_of_work";?>" class="navi-link py-4" style="padding-right:5px; padding-left:5px;">
                                <span class="navi-icon mr-2">
                                    <span class="svg-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <path d="M7.14319965,19.3575259 C7.67122143,19.7615175 8.25104409,20.1012165 8.87097532,20.3649307 L7.89205065,22.0604779 C7.61590828,22.5387706 7.00431787,22.7026457 6.52602525,22.4265033 C6.04773263,22.150361 5.88385747,21.5387706 6.15999985,21.0604779 L7.14319965,19.3575259 Z M15.1367085,20.3616573 C15.756345,20.0972995 16.3358198,19.7569961 16.8634386,19.3524415 L17.8320512,21.0301278 C18.1081936,21.5084204 17.9443184,22.1200108 17.4660258,22.3961532 C16.9877332,22.6722956 16.3761428,22.5084204 16.1000004,22.0301278 L15.1367085,20.3616573 Z" fill="#000000"/>
                                                <path d="M12,21 C7.581722,21 4,17.418278 4,13 C4,8.581722 7.581722,5 12,5 C16.418278,5 20,8.581722 20,13 C20,17.418278 16.418278,21 12,21 Z M19.068812,3.25407593 L20.8181344,5.00339833 C21.4039208,5.58918477 21.4039208,6.53893224 20.8181344,7.12471868 C20.2323479,7.71050512 19.2826005,7.71050512 18.696814,7.12471868 L16.9474916,5.37539627 C16.3617052,4.78960984 16.3617052,3.83986237 16.9474916,3.25407593 C17.5332781,2.66828949 18.4830255,2.66828949 19.068812,3.25407593 Z M5.29862906,2.88207799 C5.8844155,2.29629155 6.83416297,2.29629155 7.41994941,2.88207799 C8.00573585,3.46786443 8.00573585,4.4176119 7.41994941,5.00339833 L5.29862906,7.12471868 C4.71284263,7.71050512 3.76309516,7.71050512 3.17730872,7.12471868 C2.59152228,6.53893224 2.59152228,5.58918477 3.17730872,5.00339833 L5.29862906,2.88207799 Z" fill="#000000" opacity="0.3"/>
                                                <path d="M11.9630156,7.5 L12.0475062,7.5 C12.3043819,7.5 12.5194647,7.69464724 12.5450248,7.95024814 L13,12.5 L16.2480695,14.3560397 C16.403857,14.4450611 16.5,14.6107328 16.5,14.7901613 L16.5,15 C16.5,15.2109164 16.3290185,15.3818979 16.1181021,15.3818979 C16.0841582,15.3818979 16.0503659,15.3773725 16.0176181,15.3684413 L11.3986612,14.1087258 C11.1672824,14.0456225 11.0132986,13.8271186 11.0316926,13.5879956 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000"/>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                                <span class="navi-text font-size-lg"><?php //echo $this->lang->line('schedule_of_work');?></span>
                            </a>
                        </div>
                        <div class="navi-item mb-2">
                            <a href="<?php //echo base_url()."permissions/profile/custody";?>" class="navi-link py-4" style="padding-right:5px; padding-left:5px;">
                                <span class="navi-icon mr-2">
                                    <span class="svg-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <path d="M15.9497475,3.80761184 L13.0246125,6.73274681 C12.2435639,7.51379539 12.2435639,8.78012535 13.0246125,9.56117394 L14.4388261,10.9753875 C15.2198746,11.7564361 16.4862046,11.7564361 17.2672532,10.9753875 L20.1923882,8.05025253 C20.7341101,10.0447871 20.2295941,12.2556873 18.674559,13.8107223 C16.8453326,15.6399488 14.1085592,16.0155296 11.8839934,14.9444337 L6.75735931,20.0710678 C5.97631073,20.8521164 4.70998077,20.8521164 3.92893219,20.0710678 C3.1478836,19.2900192 3.1478836,18.0236893 3.92893219,17.2426407 L9.05556629,12.1160066 C7.98447038,9.89144078 8.36005124,7.15466739 10.1892777,5.32544095 C11.7443127,3.77040588 13.9552129,3.26588995 15.9497475,3.80761184 Z" fill="#000000"/>
                                                <path d="M16.6568542,5.92893219 L18.0710678,7.34314575 C18.4615921,7.73367004 18.4615921,8.36683502 18.0710678,8.75735931 L16.6913928,10.1370344 C16.3008685,10.5275587 15.6677035,10.5275587 15.2771792,10.1370344 L13.8629656,8.7228208 C13.4724413,8.33229651 13.4724413,7.69913153 13.8629656,7.30860724 L15.2426407,5.92893219 C15.633165,5.5384079 16.26633,5.5384079 16.6568542,5.92893219 Z" fill="#000000" opacity="0.3"/>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                                <span class="navi-text font-size-lg"><?php //echo $this->lang->line('profile_custody');?></span>
                            </a>
                        </div>
                        <div class="navi-item mb-2">
                            <a href="<?php //echo base_url()."permissions/profile/financial_custody";?>" class="navi-link py-4" style="padding-right:5px; padding-left:5px;">
                                <span class="navi-icon mr-2">
                                    <span class="svg-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <rect fill="#000000" opacity="0.3" x="11.5" y="2" width="2" height="4" rx="1"/>
                                                <rect fill="#000000" opacity="0.3" x="11.5" y="16" width="2" height="5" rx="1"/>
                                                <path d="M15.493,8.044 C15.2143319,7.68933156 14.8501689,7.40750104 14.4005,7.1985 C13.9508311,6.98949895 13.5170021,6.885 13.099,6.885 C12.8836656,6.885 12.6651678,6.90399981 12.4435,6.942 C12.2218322,6.98000019 12.0223342,7.05283279 11.845,7.1605 C11.6676658,7.2681672 11.5188339,7.40749914 11.3985,7.5785 C11.2781661,7.74950085 11.218,7.96799867 11.218,8.234 C11.218,8.46200114 11.2654995,8.65199924 11.3605,8.804 C11.4555005,8.95600076 11.5948324,9.08899943 11.7785,9.203 C11.9621676,9.31700057 12.1806654,9.42149952 12.434,9.5165 C12.6873346,9.61150047 12.9723317,9.70966616 13.289,9.811 C13.7450023,9.96300076 14.2199975,10.1308324 14.714,10.3145 C15.2080025,10.4981676 15.6576646,10.7419985 16.063,11.046 C16.4683354,11.3500015 16.8039987,11.7268311 17.07,12.1765 C17.3360013,12.6261689 17.469,13.1866633 17.469,13.858 C17.469,14.6306705 17.3265014,15.2988305 17.0415,15.8625 C16.7564986,16.4261695 16.3733357,16.8916648 15.892,17.259 C15.4106643,17.6263352 14.8596698,17.8986658 14.239,18.076 C13.6183302,18.2533342 12.97867,18.342 12.32,18.342 C11.3573285,18.342 10.4263378,18.1741683 9.527,17.8385 C8.62766217,17.5028317 7.88033631,17.0246698 7.285,16.404 L9.413,14.238 C9.74233498,14.6433354 10.176164,14.9821653 10.7145,15.2545 C11.252836,15.5268347 11.7879973,15.663 12.32,15.663 C12.5606679,15.663 12.7949989,15.6376669 13.023,15.587 C13.2510011,15.5363331 13.4504991,15.4540006 13.6215,15.34 C13.7925009,15.2259994 13.9286662,15.0740009 14.03,14.884 C14.1313338,14.693999 14.182,14.4660013 14.182,14.2 C14.182,13.9466654 14.1186673,13.7313342 13.992,13.554 C13.8653327,13.3766658 13.6848345,13.2151674 13.4505,13.0695 C13.2161655,12.9238326 12.9248351,12.7908339 12.5765,12.6705 C12.2281649,12.5501661 11.8323355,12.420334 11.389,12.281 C10.9583312,12.141666 10.5371687,11.9770009 10.1255,11.787 C9.71383127,11.596999 9.34650161,11.3531682 9.0235,11.0555 C8.70049838,10.7578318 8.44083431,10.3968355 8.2445,9.9725 C8.04816568,9.54816454 7.95,9.03200304 7.95,8.424 C7.95,7.67666293 8.10199848,7.03700266 8.406,6.505 C8.71000152,5.97299734 9.10899753,5.53600171 9.603,5.194 C10.0970025,4.85199829 10.6543302,4.60183412 11.275,4.4435 C11.8956698,4.28516587 12.5226635,4.206 13.156,4.206 C13.9160038,4.206 14.6918294,4.34533194 15.4835,4.624 C16.2751706,4.90266806 16.9686637,5.31433061 17.564,5.859 L15.493,8.044 Z" fill="#000000"/>
                                            </g>
                                        </svg>
                                    </span>
                                </span>
                                <span class="navi-text font-size-lg"><?php //echo $this->lang->line('profile_financial_custody');?></span>
                            </a>
                        </div>
                        -->
                    </div>
                    <!--end::Nav-->
                </div>
                <!--end::Body-->
            </div>
            <!--end::Profile Card-->
        </div>
        <!--end::Aside-->
        <!--begin::Content-->
        <div class="flex-row-fluid ml-lg-8 d-block" id="kt_inbox_list">
            <!--begin::Advance Table: Widget 7-->
            <div class="card card-custom gutter-b" style="padding: 15px;">
                
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">
                            <?php
                            if($Segment3 == "serv_message")
                            {
                                echo $this->lang->line('serv_message')." : ".$this->lang->line('serv_message_index');
                            }
                            else
                            {
                                echo $this->lang->line('serv_message')." : ".$this->lang->line('serv_message_sent');
                            }
                            ?>
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Dropdown-->
                            <div class="dropdown dropdown-inline mr-2">
                                <button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="svg-icon svg-icon-md">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
                                            <path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span><?php echo $this->lang->line('serv_message_filter');?></button>
                                <!--begin::Dropdown Menu-->
                                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                    <!--begin::Navigation-->
                                    <ul class="navi flex-column navi-hover py-2">
                                        <li class="navi-item">
                                            <a href="<?php echo base_url().$Segment1."/".$Segment2."/serv_message";?>" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="la la-arrow-circle-down"></i>
                                                </span>
                                                <span class="navi-text"><?php echo $this->lang->line('serv_message_index');?></span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="<?php echo base_url().$Segment1."/".$Segment2."/serv_message_sent";?>" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="la la-arrow-circle-up"></i>
                                                </span>
                                                <span class="navi-text"><?php echo $this->lang->line('serv_message_sent');?></span>
                                            </a>
                                        </li>
                                    </ul>
                                    <!--end::Navigation-->
                                </div>
                                <!--end::Dropdown Menu-->
                            </div>
                            <!--end::Dropdown-->
                            <!--begin::Button-->
                            <a data-toggle="modal" data-target="#Modal_SendEmail" class="btn btn-success font-weight-bolder">
                                <span class="svg-icon svg-icon-md">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"/>
                                            <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
                                            <path d="M11,11 L11,7 C11,6.44771525 11.4477153,6 12,6 C12.5522847,6 13,6.44771525 13,7 L13,11 L17,11 C17.5522847,11 18,11.4477153 18,12 C18,12.5522847 17.5522847,13 17,13 L13,13 L13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 L11,13 L7,13 C6.44771525,13 6,12.5522847 6,12 C6,11.4477153 6.44771525,11 7,11 L11,11 Z" fill="#000000"/>
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span><?php echo $this->lang->line('serv_message_new');?>
                            </a>
                            <!--end::Button-->
                        </div>
                    </div>

                    

                    <div class="card-body">
                        <!--begin: Search Form-->
                        <!--begin::Search Form-->
                        <div class="mb-7">
                            <div class="row align-items-center">
                                <div class="col-lg-9 col-xl-8">
                                    <div class="row align-items-center">
                                        <div class="col-12">
                                            <div class="input-icon">
                                                <input type="text" class="form-control" placeholder="<?php echo lang('Search');?>..." id="kt_datatable_search_query" />
                                                <span>
                                                    <i class="flaticon2-search-1 text-muted"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Search Form-->
                        <!--end: Search Form-->
                        <!--begin: Datatable-->
                        <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
                            <thead>
                                <tr>
                                    <th><?php echo lang('serv_message_subject');?></th>
                                    <th><?php echo lang('serv_message_from_user');?></th>
                                    <th><?php echo lang('serv_message_thedate');?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach($DataRows as $DataRows_Row) {
                                    $readed = $DataRows_Row->readed;
                                    $marked = $DataRows_Row->marked;
                                    $from_user = $DataRows_Row->from_user;
                                ?>
                                <tr>
                                    <td>
                                    <?php
                                    if($Segment3 == "serv_message") {
                                    ?>
                                    <a data-toggle="modal" href="#Modal_SendEmail" data-target="#Modal_SendEmail" data-id="<?php echo $from_user; ?>" id="reply">
                                        <i class="icon-md text-dark-50 flaticon-reply"></i>
                                    </a>
                                    <?php } ?>
                                    <?php
                                    if($readed == 1)
                                    {
                                        echo "<a data-toggle='modal' data-id='".$from_user."' href='#view".$DataRows_Row->id."' data-id='".$DataRows_Row->id."'>";
                                        echo $DataRows_Row->subject;
                                        echo "</a>";
                                    }
                                    else
                                    {
                                        echo "<strong>";
                                        echo "<a data-toggle='modal' href='#view".$DataRows_Row->id."' data-id='".$DataRows_Row->id."'>";
                                        echo $DataRows_Row->subject;
                                        echo "</a>";
                                        echo "</strong>";
                                    }
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                    
                                    $UserData = $this->M_usr_users->GetRow($from_user);
                                    if($readed == 1)
                                    {
                                        echo $UserData->fullname;
                                    }
                                    else
                                    {
                                        echo "<strong>".$UserData->fullname."</strong>";
                                    }
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                    if($readed == 1)
                                    {
                                        echo $DataRows_Row->thedate." ".$DataRows_Row->thetime;
                                    }
                                    else
                                    {
                                        echo "<strong>".$DataRows_Row->thedate." ".$DataRows_Row->thetime."</strong>";
                                    }
                                    ?>
                                    </td>
                                </tr>

                                <div id="view<?php echo $DataRows_Row->id;?>" class="modal fade" tabindex="-1" role="dialog" style="background: #000;" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 style="font-size: 24px; color: #17919e; text-shadow: 1px 1px #ccc;">
                                                <i class="fa fa-folder"></i> <?php echo $DataRows_Row->subject;?>
                                            </h3>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-3"><strong><?php echo lang('serv_message_from_user');?>:</strong></div>
                                                <div class="col-md-9" id="fullname"><?php echo $UserData->fullname;?></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3"><strong><?php echo lang('thedate');?>:</strong></div>
                                                <div class="col-md-9" id="thedate"><?php echo $DataRows_Row->thedate." ".$DataRows_Row->thetime;?></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12"><strong><?php echo lang('serv_message_message');?>:</strong></div>
                                                <div class="col-md-12" id="message"><?php echo $DataRows_Row->message;?></div>
                                            </div>
                                            <?php if($DataRows_Row->image != "") {?>
                                            <div class="row">
                                                <div class="col-md-3"><strong><?php echo lang('serv_message_image');?>:</strong></div>
                                                <div class="col-md-9" id="image">
                                                    <a target="_blank" href="<?php echo base_url()."upload/serv_message/".$DataRows_Row->image;?>">
                                                    <i class="icon-2x text-dark-50 flaticon-attachment"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo lang('Close');?></button>
                                            <button type="button" class="btn btn-info" onclick="printSelection(document.getElementById('view<?php echo $DataRows_Row->id;?>'));return false;"><i class="fa fa-print"></i> <?php echo $this->lang->line('Print');?></button>
                                        </div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->

            </div>
            <!--end::Advance Table Widget 7-->
        </div>
        <!--end::Content-->
    </div>
    <!--end::Profile Overview-->
</div>
<!--end::Container-->
<?php
include("profile_Modals.php");
?>


