					<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
						<thead>
							<tr>
								<th>id</th>
								<th><?php echo $this->lang->line('GroupName');?></th>
								<th><?php echo $this->lang->line('fullname');?></th>
								<th><?php echo $this->lang->line('email');?></th>
								<th><?php echo $this->lang->line('active');?></th>
								<th><?php echo $this->lang->line('branches_lookup');?></th>
								<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
								<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
								<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
								<th style="text-align:center"><?php echo $this->lang->line('View');?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($MultiRows as $MultiRows_Row) {
								$UserID = $MultiRows_Row->id;
							?>
							<tr>
								<td><?php echo $MultiRows_Row->id;?></td>
								<td><?php
								$GroupID = $MultiRows_Row->group_id;
								$MyGroup = $this->M_usr_usersgroup->GetRow($GroupID);
								if ($this->session->userdata('lang') == "ar")
								{
									echo $MyGroup->ar_title;
								}
								else
								{
									echo $MyGroup->en_title;
								}
								?></td>
								<td><?php echo $MultiRows_Row->fullname; ?></td>
								<td><?php echo $MultiRows_Row->email;?></td>
								<td><?php echo $MultiRows_Row->active;?></td>
								<td>
								<?php
								$branches_lookup = $MultiRows_Row->branches_lookup;
								if($branches_lookup == 1)
								{
									echo $this->lang->line('Yes');
								}
								else
								{
									echo $this->lang->line('No');
								}
								?>
								</td>
								<td style="text-align:center">
									<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$MultiRows_Row->id;?>">
										<i class="flaticon-edit-1 text-primary icon-lg"></i> 
									</a>
								</td>
								<td style="text-align:center">
									<?php
										if(($MultiRows_Row->deleted == 0) && ($UserID > 1)){
									?>
									<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$MultiRows_Row->id;?>">
										<i class="flaticon-delete text-danger icon-lg"></i> 
									</a>
									<?php } ?>
								</td>
								<td style="text-align:center">
									<?php
										if($MultiRows_Row->deleted == 1) {
									?>
										<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$MultiRows_Row->id;?>">
											<i class="flaticon-refresh text-success icon-lg"></i> 
										</a>
									<?php } ?>
								</td>
								<td style="text-align:center">
									<a href="<?php echo base_url().$Segment1."/".$Segment2."/report/".$MultiRows_Row->id;?>">
										<i class="flaticon-search text-success icon-lg"></i> 
									</a>
								</td>
							</tr>
							<?php
							}
							?>
						</tbody>
					</table>