                <div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
                        echo form_open_multipart($FormPath);
                    ?>
                        <div class="form-body">
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label><?php echo $this->lang->line('GroupName');?>:</label>
                                    <select id="group_id" name="group_id" class="form-control" required>
                                        <?php
                                        foreach($AllGroups as $AllGroups_Row) {
                                            $Title = "";
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $Title = $AllGroups_Row->ar_title;
                                            }
                                            else
                                            {
                                                $Title = $AllGroups_Row->en_title;
                                            }
                                        ?>
                                        <option value="<?php echo $AllGroups_Row->id;?>"><?php echo $Title;?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label><?php echo $this->lang->line('fullname');?>:</label>
                                    <input type="text" id="fullname" name="fullname" class="form-control" required>
                                </div>
                                <div class="col-md-4">
                                    <label><?php echo $this->lang->line('email');?>:</label>
                                    <input type="email" id="email" name="email" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label><?php echo $this->lang->line('username');?>:</label>
                                    <input type="text" id="username" name="username" class="form-control" required>
                                </div>
                                <div class="col-md-4">
                                    <label><?php echo $this->lang->line('password');?>:</label>
                                    <input type="password" id="password" name="password" class="form-control" required>
                                </div>
                                <div class="col-md-4">
                                    <label><?php echo $this->lang->line('active');?>:</label>
                                    <select id="active" name="active" class="form-control" required>
                                        <option value="True"><?php echo $this->lang->line('Yes');?></option>
                                        <option value="False"><?php echo $this->lang->line('No');?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group col-md-4">
                                    <label><?php echo $this->lang->line('image');?>:</label>
                                    <input type="file" name="image" id="image" class="form-control" dir="ltr" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label><?php echo $this->lang->line('UserStore');?>:</label>
                                    <select id="UserStore[]" name="UserStore[]" class="form-control kt-selectpicker" data-live-search="true" data-size="8" multiple="multiple" size="8" required>
                                        <?php
                                        $StoreList = $this->M_store->GetMultiRow();
                                        foreach($StoreList as $StoreList_Row) {
                                            $Title = "";
                                            if ($this->session->userdata('lang') == "ar")
                                            {
                                                $Title = $StoreList_Row->title;
                                            }
                                            else
                                            {
                                                $Title = $StoreList_Row->title_en;
                                            }
                                        ?>
                                        <option value="<?php echo $StoreList_Row->id;?>"><?php echo $Title;?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
								<div class="form-group col-md-4">
                                    <label><?php echo $this->lang->line('UserAccounts');?>:</label>
                                    <select id="UserAccounts[]" name="UserAccounts[]" class="form-control kt-selectpicker" data-live-search="true" data-size="8" multiple="multiple" size="8" required>
                                        <?php
										$acc_treasury = intval($this->session->userdata('acc_treasury'));
										$toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_treasury);
										foreach($toaccount as $toaccount_Row) {
											if ($this->session->userdata('lang') == "ar")
											{
												$ToTitle = $toaccount_Row->title;
											}
											else
											{
												$ToTitle = $toaccount_Row->title_en;
											}
										?>
										<option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
										<?php } ?>
										<?php
										$acc_bank = intval($this->session->userdata('acc_bank'));
										$toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_bank);
										foreach($toaccount as $toaccount_Row) {
											if ($this->session->userdata('lang') == "ar")
											{
												$ToTitle = $toaccount_Row->title;
											}
											else
											{
												$ToTitle = $toaccount_Row->title_en;
											}
										?>
										<option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
										<?php } ?>
                                    </select>
                                </div>
								<div class="col-md-4">
                                    <label><?php echo $this->lang->line('invoice_sale_start');?>:</label>
                                    <input type="text" id="sale_start" name="sale_start" class="form-control">
                                </div>
								<div class="col-md-4">
                                    <label><?php echo $this->lang->line('Branch');?>:</label>
                                    <input type="text" id="branch" name="branch" class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label><?php echo $this->lang->line('branches_lookup');?>:</label>
                                    <select id="branches_lookup" name="branches_lookup" class="form-control" required>
                                        <option value="1"><?php echo $this->lang->line('Yes');?></option>
                                        <option value="0"><?php echo $this->lang->line('No');?></option>
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-actions right" style="text-align: right; font-weight: bold !important; font-size: 18px;">
                            <input type="hidden" name="deleted" value="0">
                            <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                        </div>
                    <?php
                        echo form_close();
                    ?>
                </div>
            