<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
    <thead>
        <tr>
            <th>id</th>
            <th><?php echo $this->lang->line('users_group_ar_title');?></th>
            <th><?php echo $this->lang->line('users_group_en_title');?></th>
            <th><?php echo $this->lang->line('users_group_users');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('View');?></th>
        </tr>
    </thead>
    <tbody>
    <?php
    $GroupID = 0;
    foreach($MultiRows as $MultiRows_Row) {
        $GroupID = intval($MultiRows_Row->id);
        if($GroupID != 18 || ($GroupID == 18 && $this->session->userdata('GroupID') == "18")){
    ?>
    <tr>
        <td><?php echo $MultiRows_Row->id;?></td>
        <td><?php echo $MultiRows_Row->ar_title;?></td>
        <td><?php echo $MultiRows_Row->en_title;?></td>
        <td>
        <?php
        $UserCount = $this->M_usr_users->GroupCount($GroupID);
        echo $UserCount;
        ?>
        </td>
        <td style="text-align:center">
            <a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$MultiRows_Row->id;?>">
                <i class="flaticon-edit-1 text-primary icon-lg"></i>
            </a>
        </td>
        <td style="text-align:center">
            <?php
                if($MultiRows_Row->deleted == 0) {
            ?>
            <a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$MultiRows_Row->id;?>">
                <i class="flaticon-delete text-danger icon-lg"></i>
            </a>
            <?php } ?>
        </td>
        <td style="text-align:center">
            <?php
                if($MultiRows_Row->deleted == 1) {
            ?>
                <a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$MultiRows_Row->id;?>">
                    <i class="flaticon-refresh text-success icon-lg"></i>
                </a>
            <?php } ?>
        </td>
        <td style="text-align:center">
            <a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$MultiRows_Row->id;?>">
                <i class="flaticon-search text-success icon-lg"></i>
            </a>
        </td>
    </tr>
    <?php
    }
    }
    ?>
    </tbody>
</table>
