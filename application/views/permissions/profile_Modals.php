<!-- Modal-->
<div class="modal fade" id="Modal_SendEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?php echo lang('serv_message_new');?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
            
            <form id="kt_inbox_reply_form" action="<?php echo base_url()."/permissions/profile/serv_message_insert";?>" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                <!--begin::Body-->
                <div class="form-body" style="padding-right:15px !important; padding-left:15px !important;">
                    <!--begin::To-->
                    <div class="form-group row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <label class="col-form-label"><?php echo lang('serv_message_to_user');?> : </label>
                            <select name="to_user[]" id="to_user[]" class="form-control" data-live-search="true" multiple="multiple" size="3" required>
                                <?php
                                $UsersList = $this->M_usr_users->GetMultiRow();
                                foreach($UsersList as $UsersList_Row) {
                                    $UserFullName = $UsersList_Row->fullname;
                                ?>
                                <option value="<?php echo $UsersList_Row->id;?>"><?php echo $UserFullName;?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <label class="col-form-label"><?php echo lang('serv_message_subject');?> : </label>
                            <input type="text" name="subject" id="subject" class="form-control" value="" required>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <label class="col-form-label"><?php echo lang('serv_message_message');?> : </label>
                            <textarea name="message" class="form-control" rows="5"></textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line('serv_message_image');?>:</label>
                            <input type="file" name="image" id="image" class="form-control" dir="ltr">
                        </div>
                    </div>
                    <!--end::To-->
                </div>
                <!--end::Body-->
                <div class="kt-portlet__foot" style="padding-right:15px !important; padding-left:15px !important;">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-lg-12 ml-lg-auto">
                                <input type="hidden" name="id" value="0">
                                <input type="hidden" name="deleted" value="0">
                                <a href="<?php echo base_url().$Segment1."/".$Segment2."/".$Segment3;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                                <button type="submit" class="btn btn-primary mr-2" name="action" value="Send"><?php echo $this->lang->line('serv_message_submit');?></button>
                            </div>
                        </div>
                    </div>
                </div><br><br>
            </form>
            
            </div>
        </div>
    </div>
</div>