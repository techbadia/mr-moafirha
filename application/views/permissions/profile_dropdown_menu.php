<ul class="navi navi-hover py-5">
    <li class="navi-item" style="cursor:pointer;">
        <a data-toggle="modal" data-target="#Modal_SendEmail" class="navi-link">
            <span class="navi-icon">
                <i class="flaticon-email"></i>
            </span>
            <span class="navi-text"><?php echo $this->lang->line('profile_menu_send_email');?></span>
        </a>
    </li>
    <!--
    <li class="navi-item" style="cursor:pointer;">
        <a href="#" class="navi-link">
            <span class="navi-icon">
                <i class="flaticon2-list-3"></i>
            </span>
            <span class="navi-text"><?php //echo $this->lang->line('profile_menu_send_action');?></span>
        </a>
    </li>
    <li class="navi-item" style="cursor:pointer;">
        <a href="#" class="navi-link">
            <span class="navi-icon">
                <i class="flaticon-calendar"></i>
            </span>
            <span class="navi-text"><?php //echo $this->lang->line('profile_menu_send_vacation');?></span>
        </a>
    </li>
    <li class="navi-item" style="cursor:pointer;">
        <a href="#" class="navi-link">
            <span class="navi-icon">
                <i class="flaticon-coins"></i>
            </span>
            <span class="navi-text"><?php //echo $this->lang->line('profile_menu_send_advance');?></span>
        </a>
    </li>
    -->
</ul>