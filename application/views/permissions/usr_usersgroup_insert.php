<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <?php
    $GroupID = intval($this->session->userdata('GroupID'));
    $PackageID = intval($this->session->userdata('PackageID'));
    $PackageModules = $this->M_app_package_module->GetMainModules($PackageID);

    $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
    echo form_open_multipart($FormPath);
    ?>
        <div class="form-body">
            <div class="form-group row">
                <div class="col-md-6">
                    <label><?php echo $this->lang->line('users_group_ar_title');?></label>
                    <input type="text" id="ar_title" name="ar_title" value="" class="form-control">
                </div>
                <div class="col-md-6">
                    <label><?php echo $this->lang->line('users_group_en_title');?></label>
                    <input type="text" id="en_title" name="en_title" value="" class="form-control">
                </div>

            </div>
        </div>

        <div class="portlet box blue-chambray">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-comments"></i><?php echo $this->lang->line('Privileges');?>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-scrollable">
                <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>
                            <?php echo $this->lang->line('PageName');?>
                    </th>
                    <th style="text-align: center">
                            <?php echo $this->lang->line('AddNew');?>
                    </th>
                    <th style="text-align: center">
                            <?php echo $this->lang->line('Edit');?>
                    </th>
                    <th style="text-align: center">
                            <?php echo $this->lang->line('Delete');?>
                    </th>
                    <th style="text-align: center">
                            <?php echo $this->lang->line('View');?>
                    </th>
                    <th style="text-align: center">
                            <?php echo $this->lang->line('Export');?>
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php
                $Title = "";
                foreach($PackageModules as $PackageModules_Row) {
                    $app_package_module_id = $PackageModules_Row->id;
                    $module_id = $PackageModules_Row->module_id;
                    $ModuleData = $this->M_app_module->GetRow($module_id);
                    $folder = $ModuleData->folder;
                    $icon_name = $ModuleData->icon_name;
                    $ModuleTitle = "";
                    if ($this->session->userdata('lang') == "ar")
                    {
                        $ModuleTitle = $ModuleData->ar_title;
                    }
                    else
                    {
                        $ModuleTitle = $ModuleData->en_title;
                    }

                    $Segment1 = $this->uri->segment(1);
                    $ItemOpen = "";
                    if($Segment1 == $folder)
                    {
                        $ItemOpen = "menu-item menu-item-submenu menu-item-open";
                    }
                    else
                    {
                        $ItemOpen = "menu-item menu-item-submenu";
                    }
                ?>
                <tr>
                    <td colspan="6" style="color:darkviolet; font-size:16px; font-weight:bold">
                        <input type="checkbox" class="select_all_module_items" data-module_id="<?=$module_id?>" checked >
                        <?php echo $ModuleTitle;?>
                    </td>
                </tr>
                <?php
                $CheckSubModules = $this->M_app_package_module->GetSubModulesCount($PackageID, $module_id);
                if($CheckSubModules > 0) {
                    $SubModules = $this->M_app_package_module->GetSubModules($PackageID, $module_id);
                    foreach($SubModules as $SubModules_Row) {
                        $app_package_Sub_module_id = $SubModules_Row->id;
                        $Sub_module_id = $SubModules_Row->module_id;
                        $ModuleData = $this->M_app_module->GetRow($Sub_module_id);
                        $folder = $ModuleData->folder;
                        $icon_name = $ModuleData->icon_name;
                        $ModuleTitle = "";
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $ModuleTitle = $ModuleData->ar_title;
                        }
                        else
                        {
                            $ModuleTitle = $ModuleData->en_title;
                        }

                        $Segment1 = $this->uri->segment(1);
                        $ItemOpen = "";
                        if($Segment1 == $folder)
                        {
                            $ItemOpen = "menu-item menu-item-submenu menu-item-open";
                        }
                        else
                        {
                            $ItemOpen = "menu-item menu-item-submenu";
                        }
                ?>
                <tr>
                    <td colspan="6" style="color:purple; font-size:14px; font-weight:bold">&nbsp;&nbsp;
                        <input type="checkbox" class="module_item_<?=$module_id?> select_all_sub_module_items" data-sub_module_id="<?=$Sub_module_id?>" checked >
                        <?php echo $ModuleTitle;?>
                    </td>
                </tr>
                <?php
                    $ModulePages = $this->M_app_package_module_page->GetByModule($app_package_Sub_module_id);
                    foreach($ModulePages as $ModulePages_Row) {
                        $PageID = $ModulePages_Row->page_id;
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $PageTitle = $ModulePages_Row->ar_title;
                        }
                        else
                        {
                            $PageTitle = $ModulePages_Row->en_title;
                        }
                        $PageData = $this->M_app_module_page->GetRow($PageID);
                        $ModuleID = $PageData->module_id;
                        $class_name = $PageData->class_name;
                        $target_blank = $PageData->target_blank;
                        $privileges = $this->M_usr_usersprivileges->GetData($GroupID, $PageID);
                    ?>
                    <tr>
                        <td>
                            <strong>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $PageTitle;?></strong>
                            <input type="hidden" id="id<?php echo $PageID?>" name="id<?php echo $PageID?>" value="<?php echo $PageID?>">
                            <input type="hidden" id="group_id<?php echo $PageID?>" name="group_id<?php echo $PageID?>" value="<?php echo $PageID?>">
                            <input type="hidden" id="page_id<?php echo $PageID?>" name="page_id<?php echo $PageID?>" value="<?php echo $PageID?>">
                        </td>

                        <?php if($ModulePages_Row->ar_title != "شجرة الحسابات" ){

                           ?>
                        <td class="active" style="text-align: center">
                            <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_add<?php echo $privileges->id?>" name="enabled_add<?php echo $privileges->id?>" value="1" <?php if ($privileges->enabled_add == "1") {?>checked<?php }?>>
                        </td>
                        <td class="success" style="text-align: center">
                            <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_edit<?php echo $privileges->id?>" name="enabled_edit<?php echo $privileges->id?>" value="1" <?php if ($privileges->enabled_edit == "1") {?>checked<?php }?> >
                        </td>
                        <td class="danger" style="text-align: center">
                            <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_delete<?php echo $privileges->id?>" name="enabled_delete<?php echo $privileges->id?>" value="1" <?php if ($privileges->enabled_delete == "1") {?>checked<?php }?>>
                        </td>
                      <?php } ?>
                        <?php if($ModulePages_Row->ar_title == "شجرة الحسابات"  && $this->session->userdata('GroupID') == "18"){ ?>
                          <td class="active" style="text-align: center">
                              <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_add<?php echo $privileges->id?>" name="enabled_add<?php echo $privileges->id?>" value="1" <?php if ($privileges->enabled_add == "1") {?>checked<?php }?>>
                          </td>
                          <td class="success" style="text-align: center">
                              <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_edit<?php echo $privileges->id?>" name="enabled_edit<?php echo $privileges->id?>" value="1" <?php if ($privileges->enabled_edit == "1") {?>checked<?php }?> >
                          </td>
                          <td class="danger" style="text-align: center">
                              <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_delete<?php echo $privileges->id?>" name="enabled_delete<?php echo $privileges->id?>" value="1" <?php if ($privileges->enabled_delete == "1") {?>checked<?php }?>>
                          </td>
                        <?php }elseif($ModulePages_Row->ar_title == "شجرة الحسابات"  && $this->session->userdata('GroupID') != "18") {

                          ?>
                          <td></td>
                          <td></td>
                          <td></td>
                          <?php
                        } ?>
                        <td class="warning" style="text-align: center">
                            <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_view<?php echo $PageID?>" name="enabled_view<?php echo $PageID?>" value="1" <?php if ($privileges->enabled_view == "1") {?>checked<?php }?>>
                        </td>
                        <td class="success" style="text-align: center">
                            <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_export<?php echo $PageID?>" name="enabled_export<?php echo $PageID?>" value="1" <?php if ($privileges->enabled_export == "1") {?>checked<?php }?>>
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                <?php
                    }
                }
                ?>
                    <?php
                    $ModulePages = $this->M_app_package_module_page->GetByModule($app_package_module_id);
                    foreach($ModulePages as $ModulePages_Row) {
                        $PageID = $ModulePages_Row->page_id;
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $PageTitle = $ModulePages_Row->ar_title;
                        }
                        else
                        {
                            $PageTitle = $ModulePages_Row->en_title;
                        }
                        $PageData = $this->M_app_module_page->GetRow($PageID);
                        $ModuleID = $PageData->module_id;
                        $class_name = $PageData->class_name;
                        $target_blank = $PageData->target_blank;
                        $privileges = $this->M_usr_usersprivileges->GetData($GroupID, $PageID);
                        //echo "group id = ".$GroupID." :::: page id = ".$PageID;
                    ?>
                    <tr>
                        <td>
                            <strong><?php echo $PageTitle;?></strong>
                            <input type="hidden" id="id<?php echo $PageID?>" name="id<?php echo $PageID?>" value="<?php echo $PageID?>">
                            <input type="hidden" id="group_id<?php echo $PageID?>" name="group_id<?php echo $PageID?>" value="<?php echo $PageID?>">
                            <input type="hidden" id="page_id<?php echo $PageID?>" name="page_id<?php echo $PageID?>" value="<?php echo $PageID?>">
                        </td>
                        <?php if($ModulePages_Row->ar_title != "شجرة الحسابات" ){

                           ?>
                        <td class="active" style="text-align: center">
                            <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_add<?php echo $privileges->id?>" name="enabled_add<?php echo $privileges->id?>" value="1" <?php if ($privileges->enabled_add == "1") {?>checked<?php }?>>
                        </td>
                        <td class="success" style="text-align: center">
                            <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_edit<?php echo $privileges->id?>" name="enabled_edit<?php echo $privileges->id?>" value="1" <?php if ($privileges->enabled_edit == "1") {?>checked<?php }?> >
                        </td>
                        <td class="danger" style="text-align: center">
                            <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_delete<?php echo $privileges->id?>" name="enabled_delete<?php echo $privileges->id?>" value="1" <?php if ($privileges->enabled_delete == "1") {?>checked<?php }?>>
                        </td>
                      <?php } ?>
                        <?php if($ModulePages_Row->ar_title == "شجرة الحسابات"  && $this->session->userdata('GroupID') == "18"){ ?>
                          <td class="active" style="text-align: center">
                              <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_add<?php echo $privileges->id?>" name="enabled_add<?php echo $privileges->id?>" value="1" <?php if ($privileges->enabled_add == "1") {?>checked<?php }?>>
                          </td>
                          <td class="success" style="text-align: center">
                              <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_edit<?php echo $privileges->id?>" name="enabled_edit<?php echo $privileges->id?>" value="1" <?php if ($privileges->enabled_edit == "1") {?>checked<?php }?> >
                          </td>
                          <td class="danger" style="text-align: center">
                              <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_delete<?php echo $privileges->id?>" name="enabled_delete<?php echo $privileges->id?>" value="1" <?php if ($privileges->enabled_delete == "1") {?>checked<?php }?>>
                          </td>
                        <?php }elseif($ModulePages_Row->ar_title == "شجرة الحسابات"  && $this->session->userdata('GroupID') != "18") {

                          ?>
                          <td></td>
                          <td></td>
                          <td></td>
                          <?php
                        } ?>
                        <td class="warning" style="text-align: center">
                            <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_view<?php echo $PageID?>" name="enabled_view<?php echo $PageID?>" value="1" <?php if ($privileges->enabled_view == "1") {?>checked<?php }?>>
                        </td>
                        <td class="success" style="text-align: center">
                            <input class="module_item_<?=$module_id?> sub_module_item_<?=$Sub_module_id?>" type="checkbox" id="enabled_export<?php echo $PageID?>" name="enabled_export<?php echo $PageID?>" value="1" <?php if ($privileges->enabled_export == "1") {?>checked<?php }?>>
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                ?>


                </tbody>
                </table>
            </div>
        </div>
    </div>


        <div class="form-actions right" style="text-align: right; font-weight: bold !important; font-size: 18px;">
            <input type="hidden" id="id" name="id" value="<?php //echo $DataRow_id;?>" />
            <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
            <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
        </div>
    <?php
        echo form_close();
    ?>
</div>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

        <script type="text/javascript">
			$(document).ready(function() {
                $('.select_all_module_items').click(function() {
                    var module_id = $(this).data("module_id");

                    // var id = $(this).attr('relid'); //get the attribute value
                    // var checked = this.checked;
                    if(this.checked) {
                        // Iterate each checkbox
                        $(':checkbox.module_item_'+module_id).each(function() {
                            this.checked = true;
                        });
                    } else {
                        $(':checkbox.module_item_'+module_id).each(function() {
                            this.checked = false;
                        });
                    }
                })
                $('.select_all_sub_module_items').click(function() {
                    var sub_module_id = $(this).data("sub_module_id");

                    // var id = $(this).attr('relid'); //get the attribute value
                    // var checked = this.checked;
                    if(this.checked) {
                        // Iterate each checkbox
                        $(':checkbox.sub_module_item_'+sub_module_id).each(function() {
                            this.checked = true;
                        });
                    } else {
                        $(':checkbox.sub_module_item_'+sub_module_id).each(function() {
                            this.checked = false;
                        });
                    }
                })



			});
		</script>
