
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo $this->lang->line('set_teamwork_title');?></th>
												<th><?php echo $this->lang->line('set_teamwork_items_title');?></th>
												<th><?php echo $this->lang->line('set_teamwork_items_details');?></th>
												<th><?php echo $this->lang->line('Other_revenues');?></th>
												<th><?php echo $this->lang->line('Other_expenses');?></th>
												<th><?php echo lang('active');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$ItemID = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
													<?php
													$teamwork_id = $DataRows_Row->teamwork_id;
													$TeamWork = $this->M_set_teamwork->GetRow($teamwork_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $TeamWork->ar_title;
													}
													else
													{
														echo $TeamWork->en_title;
													}
													?>
												</td>
                        						<td><?php echo $DataRows_Row->title; ?></td>
                        						<td><?php echo $DataRows_Row->details; ?></td>
												<td>
													<?php
													$GetRevenues = $this->M_set_teamwork_items->GetRevenues($ItemID);
													$TotalRevenues = 0;
													foreach($GetRevenues as $GetRevenues_Row) {
														$TotalRevenues += doubleval($GetRevenues_Row->amount);
													}
													echo $TotalRevenues;
													echo " | ";
													echo "<a href='".base_url()."index.php/data_teamwork_items_revenues/insertform/".$ItemID."'>".$this->lang->line('AddNew')."</a>";
													?>
												</td>
												<td>
													<?php
													$GetExpenses = $this->M_set_teamwork_items->GetExpenses($ItemID);
													$TotalExpenses = 0;
													foreach($GetExpenses as $GetExpenses_Row) {
														$TotalExpenses += doubleval($GetExpenses_Row->amount);
													}
													echo $TotalExpenses;
													echo " | ";
													echo "<a href='".base_url()."index.php/data_teamwork_items_expenses/insertform/".$ItemID."'>".$this->lang->line('AddNew')."</a>";
													?>
												</td>
												<td>
												<?php
												if($DataRows_Row->deleted == 0)
												{
													echo lang('Yes');
												}
												else
												{
													echo lang('No');
												}
												?>
												</td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

								