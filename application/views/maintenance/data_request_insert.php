<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <?php
        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
        echo form_open_multipart($FormPath);
    ?>
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="form-group col-md-3">
                    <label><?php echo $this->lang->line('data_request_area_id');?></label>
                    <select id="area_id" name="area_id" class="form-control" required>
                        <?php
                        foreach($Area as $Area_Row) {
                            if ($this->session->userdata('lang') == "ar")
                            {
                                $Title = $Area_Row->ar_title;
                            }
                            else
                            {
                                $Title = $Area_Row->en_title;
                            }
                        ?>
                        <option value="<?php echo $Area_Row->id;?>"><?php echo $Title;?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label><?php echo $this->lang->line('sale_fullname')." (".$this->lang->line('ar_title').")";?></label>
                    <input type="text" id="ar_title" name="ar_title" class="form-control" readonly required>
                </div>
                <div class="form-group col-md-3">
                    <label><?php echo $this->lang->line('sale_fullname')." (".$this->lang->line('en_title').")";?></label>
                    <input type="text" id="en_title" name="en_title" class="form-control" readonly dir="ltr" required>
                </div>
                <div class="form-group col-md-3">
                    <label>
                    <?php echo $this->lang->line('phone');?>
                    <small style="color:#5D78FF">(<?php echo $this->lang->line('customers_phone_comment');?>)</small>
                    </label>
                    <input type="text" id="phone" name="phone" class="form-control" 
                    onfocus="GetArabicName(); GetEnglishName(); GetMobile(); GetWhatsApp(); GetAddress(); GetEmail();" 
                    onblur="GetArabicName(); GetEnglishName(); GetMobile(); GetWhatsApp(); GetAddress(); GetEmail();" style="background-color:#D9DFFF" required>
                </div>
                <div class="col-md-3">
                        <div class="form-group">
                        <label><?php echo $this->lang->line('mobile1');?></label>
                        <input type="text" id="mobile1" name="mobile1" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label><?php echo $this->lang->line('mobile2');?></label>
                        <input type="text" id="mobile2" name="mobile2" class="form-control" required>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label><?php echo $this->lang->line('address');?></label>
                    <input type="text" id="address" name="address" class="form-control" required>
                </div>
                
                <div class="form-group col-md-3">
                    <label><?php echo $this->lang->line('Email');?></label>
                    <input type="email" id="email" name="email" class="form-control">
                </div>
                <div class="form-group col-md-3">
                    <label><?php echo $this->lang->line('data_request_status_id');?></label>
                    <select id="status_id" name="status_id" class="form-control" required>
                        <?php
                        foreach($Status as $Status_Row) {
                            if ($this->session->userdata('lang') == "ar")
                            {
                                $Title = $Status_Row->ar_title;
                            }
                            else
                            {
                                $Title = $Status_Row->en_title;
                            }
                        ?>
                        <option value="<?php echo $Status_Row->id;?>"><?php echo $Title;?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label><?php echo $this->lang->line('data_request_thedate');?></label>
                    <input type="date" id="thedate" name="thedate" class="form-control" value="<?php echo date("Y-m-d");?>" required>
                </div>
                <div class="form-group col-md-3">
                    <label><?php echo $this->lang->line('data_request_thetime');?></label>
                    <input type="time" id="thetime" name="thetime" class="form-control" value="<?php echo date("h:m");?>" required>
                </div>
                <div class="form-group col-md-12">
                    <label><?php echo $this->lang->line('data_request_details');?></label>
                    <input type="text" id="details" name="details" class="form-control" required>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-form-label"><?php echo $this->lang->line('set_service');?></label>
                    <select id="ServiceList[]" name="ServiceList[]" class="form-control kt-selectpicker" data-live-search="true" data-size="8" multiple="multiple" size="8" required>
                        <?php
                        $ServiceList = $this->M_services->GetMultiRow();
                        foreach($ServiceList as $ServiceList_Row) {
                            $Title = "";
                            if ($this->session->userdata('lang') == "ar")
                            {
                                $Title = $ServiceList_Row->ar_title;
                            }
                            else
                            {
                                $Title = $ServiceList_Row->en_title;
                            }
                        ?>
                        <option value="<?php echo $ServiceList_Row->id;?>"><?php echo $Title;?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php //echo $id;?>">
                        <input type="hidden" name="deleted" value="0">
                        <input type="hidden" name="insert_date" value="<?php echo date("Y-m-d");?>">
                        <input type="hidden" name="insert_time" value="<?php echo date("h:m");?>">

                        <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                        <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                    </div>
                </div>
            </div>
        </div>
    <?php
        echo form_close();
    ?>
</div>