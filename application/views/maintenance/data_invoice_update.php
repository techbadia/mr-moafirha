<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <?php
        $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
        echo form_open_multipart($FormPath);
    ?>
    <div class="kt-portlet__body">
        <div class="form-group row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <label><?php echo $this->lang->line('data_request_status_id');?></label>
                <select id="status_id" name="status_id" class="form-control" required>
                    <?php
                    foreach($Status as $Status_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $Status_Row->ar_title;
                        }
                        else
                        {
                            $Title = $Status_Row->en_title;
                        }
                    ?>
                    <option <?php if($status_id == $Status_Row->id) {?>selected<?php }?> value="<?php echo $Status_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <label><?php echo lang('data_invoice_Done');?></label>
                <select name="tree_id" class="form-control" class="form-control" required>
                    <?php
                    $acc_treasury = intval($this->session->userdata('acc_treasury'));
                    $toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_treasury);
                    foreach($toaccount as $toaccount_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $ToTitle = $toaccount_Row->title;
                        }
                        else
                        {
                            $ToTitle = $toaccount_Row->title_en;
                        }
                    ?>
                    <option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
                    <?php } ?>
                    <?php
                    $acc_bank = intval($this->session->userdata('acc_bank'));
                    $toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_bank);
                    foreach($toaccount as $toaccount_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $ToTitle = $toaccount_Row->title;
                        }
                        else
                        {
                            $ToTitle = $toaccount_Row->title_en;
                        }
                    ?>
                    <option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            <div class="row">
                <div class="col-lg-12 ml-lg-auto">
                    <input type="hidden" name="id" value="<?php echo $id;?>">
                    <input type="hidden" name="source" value="<?php echo $source;?>">
                    <input type="hidden" name="insert_date" value="<?php echo $insert_date;?>">
                    <input type="hidden" name="insert_time" value="<?php echo $insert_time;?>">
                    <input type="hidden" name="deleted" value="<?php echo $deleted;?>">
                    <?php
                    $company_id = intval($this->session->userdata('company_id'));
                    ?>
                    <input type="hidden" name="company_id" value="<?php echo $company_id;?>">

                    <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                    <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                </div>
            </div>
        </div>
    </div>
    <?php
        echo form_close();
    ?>
</div>