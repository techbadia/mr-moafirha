<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <?php
        $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
        echo form_open_multipart($FormPath);
    ?>
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('data_request_customer_id');?></label>
                    <select id="customer_id" name="customer_id" class="form-control" required>
                        <?php
                        foreach($Customer as $Customer_Row) {
                            if ($this->session->userdata('lang') == "ar")
                            {
                                $Title = $Customer_Row->ar_title;
                            }
                            else
                            {
                                $Title = $Customer_Row->en_title;
                            }
                        ?>
                        <option <?php if($customer_id == $Customer_Row->id) {?>selected<?php } ?> value="<?php echo $Customer_Row->id;?>"><?php echo $Title;?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('data_request_status_id');?></label>
                    <select id="status_id" name="status_id" class="form-control" required>
                        <?php
                        $S_ID = 0;
                        foreach($Status as $Status_Row) {
                            $S_ID = $Status_Row->id;
                            if($S_ID > $status_id) {
                            if ($this->session->userdata('lang') == "ar")
                            {
                                $Title = $Status_Row->ar_title;
                            }
                            else
                            {
                                $Title = $Status_Row->en_title;
                            }
                        ?>
                        <option <?php if($status_id == $Status_Row->id) {?>selected<?php } ?> value="<?php echo $Status_Row->id;?>"><?php echo $Title;?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('data_request_area_id');?></label>
                    <select id="area_id" name="area_id" class="form-control" required>
                        <?php
                        foreach($Area as $Area_Row) {
                            if ($this->session->userdata('lang') == "ar")
                            {
                                $Title = $Area_Row->ar_title;
                            }
                            else
                            {
                                $Title = $Area_Row->en_title;
                            }
                        ?>
                        <option <?php if($area_id == $Area_Row->id) {?>selected<?php } ?> value="<?php echo $Area_Row->id;?>"><?php echo $Title;?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('data_request_thedate');?></label>
                    <input type="date" id="thedate" name="thedate" class="form-control" value="<?php echo $thedate;?>" required>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('data_request_thetime');?></label>
                    <input type="time" id="thetime" name="thetime" class="form-control" value="<?php echo $thetime;?>" required>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('data_request_source');?></label>
                    <select id="source" name="source" class="form-control" required>
                        <option <?php if($source == 1) {?>selected<?php } ?> value="1"><?php echo $this->lang->line('data_request_source1');?></option>
                        <option <?php if($source == 2) {?>selected<?php } ?> value="2"><?php echo $this->lang->line('data_request_source2');?></option>
                        <option <?php if($source == 3) {?>selected<?php } ?> value="3"><?php echo $this->lang->line('data_request_source3');?></option>
                    </select>
                </div>
                <div class="form-group col-md-12">
                    <label><?php echo $this->lang->line('data_request_details');?></label>
                    <input type="text" id="details" name="details" class="form-control" value="<?php echo $details;?>" required>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-form-label"><?php echo $this->lang->line('set_service');?></label>
                    <select id="ServiceList[]" name="ServiceList[]" class="form-control kt-selectpicker" data-live-search="true" data-size="8" multiple="multiple" size="8" required>
                        <?php
                        $ServiceList = $this->M_services->GetMultiRow();
                        foreach($ServiceList as $ServiceList_Row) {
                            $Title = "";
                            if ($this->session->userdata('lang') == "ar")
                            {
                                $Title = $ServiceList_Row->ar_title;
                            }
                            else
                            {
                                $Title = $ServiceList_Row->en_title;
                            }
                        ?>
                        <option <?php
                        $invoice_id = $id;
                        $service_id = $ServiceList_Row->id;
                        $GetOrderService = $this->M_data_request_items->GetOrderService($request_id, $service_id);
                        if($GetOrderService >0) { echo "selected"; }
                        ?> value="<?php echo $ServiceList_Row->id;?>"><?php echo $Title;?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="company_id" value="<?php echo $company_id;?>">
                        <input type="hidden" name="user_id" value="<?php echo $user_id;?>">
                        <input type="hidden" name="affiliate_id" value="<?php echo $affiliate_id;?>">
                        <input type="hidden" name="deleted" value="<?php echo $deleted;?>">
                        <input type="hidden" name="insert_date" value="<?php echo $insert_date;?>">
                        <input type="hidden" name="insert_time" value="<?php echo $insert_time;?>">

                        <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                        <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                    </div>
                </div>
            </div>
        </div>
    <?php
        echo form_close();
    ?>
</div>