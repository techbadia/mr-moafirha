<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <?php
        $FormPath = base_url().$Segment1."/".$Segment2."/convert_update";
        echo form_open_multipart($FormPath);
    ?>
    <div class="kt-portlet__body">
        <div class="form-group row">
            <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('data_request_area_id');?></label>
                <select id="area_id" name="area_id" class="form-control" required>
                    <?php
                    foreach($Area as $Area_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $Area_Row->ar_title;
                        }
                        else
                        {
                            $Title = $Area_Row->en_title;
                        }
                    ?>
                    <option <?php if($area_id == $Area_Row->id) {?>selected<?php }?> value="<?php echo $Area_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('customers_fullname');?></label>
                <select id="customer_id" name="customer_id" class="form-control" required>
                    <?php
                    foreach($Customer as $Customer_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $fullname = $Customer_Row->ar_title;
                        }
                        else
                        {
                            $fullname = $Customer_Row->en_title;
                        }
                    ?>
                    <option <?php if($customer_id == $Customer_Row->id) {?>selected<?php }?> value="<?php echo $Customer_Row->id;?>"><?php echo $fullname;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('AssignToTeamWork');?></label>
                <select id="teamwork_id" name="teamwork_id" class="form-control" required>
                    <?php
                    foreach($Teamwork as $Teamwork_Row) {
                        $Title = "";
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $Teamwork_Row->ar_title;
                        }
                        else
                        {
                            $Title = $Teamwork_Row->en_title;
                        }
                    ?>
                    <option value="<?php echo $Teamwork_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('data_request_status_id');?></label>
                <select id="status_id" name="status_id" class="form-control" required>
                    <?php
                    foreach($Status as $Status_Row) {
                        $Row_ID = $Status_Row->id;
                        if($Row_ID == 2) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $Status_Row->ar_title;
                        }
                        else
                        {
                            $Title = $Status_Row->en_title;
                        }
                    ?>
                    <option <?php if($status_id == $Status_Row->id) {?>selected<?php }?> value="<?php echo $Status_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('data_request_thedate');?></label>
                <input type="date" id="thedate" name="thedate" class="form-control" value="<?php echo $thedate;?>" required>
            </div>
            <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('data_request_thetime');?></label>
                <input type="time" id="thetime" name="thetime" class="form-control" value="<?php echo $thetime;?>" required>
            </div>
            <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('data_request_details');?></label>
                <input type="text" id="details" name="details" class="form-control" value="<?php echo $details;?>" required>
            </div>
            <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('schedule');?></label>
                <input type="number" name="schedule" min="0" step="1" class="form-control" value="0" required>
            </div>
        </div>
    </div>
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            <div class="row">
                <div class="col-lg-12 ml-lg-auto">
                    <input type="hidden" name="id" value="<?php echo $id;?>">
                    <input type="hidden" name="source" value="<?php echo $source;?>">
                    <input type="hidden" name="insert_date" value="<?php echo $insert_date;?>">
                    <input type="hidden" name="insert_time" value="<?php echo $insert_time;?>">
                    <input type="hidden" name="deleted" value="<?php echo $deleted;?>">
                    <?php
                    $company_id = intval($this->session->userdata('company_id'));
                    ?>
                    <input type="hidden" name="company_id" value="<?php echo $company_id;?>">

                    <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                    <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                </div>
            </div>
        </div>
    </div>
    <?php
        echo form_close();
    ?>
</div>