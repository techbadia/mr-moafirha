<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <form class="kt-form kt-form--label-right" method="post" action="<?php echo base_url().$Segment1."/".$Segment2."/filter";?>" enctype="multipart/form-data">
        <div class="row">
            <div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <label><?php echo $this->lang->line('hr_holidays_fromdate');?></label>
                <input type="date" name="fromdate" class="form-control" value="<?php echo $fromdate;?>" required>
            </div>
            <div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <label><?php echo $this->lang->line('hr_holidays_todate');?></label>
                <input type="date" name="todate" class="form-control" value="<?php echo $todate;?>" required>
            </div>
            <div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <label><?php echo $this->lang->line('data_request_area_id');?></label>
                <select id="area_id" name="area_id" class="form-control" required>
                    <option <?php if($area_id ==0) {?>selected<?php }?> value="0"><?php echo $this->lang->line('data_request_All_area');?></option>
                    <?php
                    foreach($Area as $Area_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $Area_Row->ar_title;
                        }
                        else
                        {
                            $Title = $Area_Row->en_title;
                        }
                    ?>
                    <option <?php if($area_id ==$Area_Row->id) {?>selected<?php }?> value="<?php echo $Area_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <label><?php echo $this->lang->line('data_request_customer_id');?></label>
                <select id="customer_id" name="customer_id" class="form-control" required>
                    <option <?php if($customer_id ==0) {?>selected<?php }?> value="0"><?php echo $this->lang->line('data_request_All_customers');?></option>
                    <?php
                    foreach($Customer as $Customer_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $Customer_Row->ar_title;
                        }
                        else
                        {
                            $Title = $Customer_Row->en_title;
                        }
                    ?>
                    <option <?php if($customer_id == $Customer_Row->id) {?>selected<?php } ?> value="<?php echo $Customer_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <label><?php echo $this->lang->line('data_request_status_id');?></label>
                <select id="status_id" name="status_id" class="form-control" required>
                <option <?php if($status_id ==0) {?>selected<?php }?> value="0"><?php echo $this->lang->line('data_request_All_status');?></option>
                    <?php
                    foreach($Status as $Status_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $Status_Row->ar_title;
                        }
                        else
                        {
                            $Title = $Status_Row->en_title;
                        }
                    ?>
                    <option <?php if($status_id == $Status_Row->id) {?>selected<?php } ?> value="<?php echo $Status_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <label><?php echo $this->lang->line('data_invoice_teamwork_id');?></label>
                <select id="teamwork_id" name="teamwork_id" class="form-control" required>
                <option <?php if($teamwork_id ==0) {?>selected<?php }?> value="0"><?php echo $this->lang->line('data_request_All_TeamWork');?></option>
                    <?php
                    foreach($TeamWork as $TeamWork_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $TeamWork_Row->ar_title;
                        }
                        else
                        {
                            $Title = $TeamWork_Row->en_title;
                        }
                    ?>
                    <option <?php if($teamwork_id == $TeamWork_Row->id) {?>selected<?php } ?> value="<?php echo $TeamWork_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <button type="submit" class="btn btn-success btn-custom" id="kt_sweetalert_demo_3_3" style="margin-top:25px;"><?php echo lang('Search');?></button>
            </div>
        </div>
    </form>
</div>
<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
    <thead>
        <tr>
            <th>id</th>
            <th><?php echo $this->lang->line('data_request_user_id');?></th>
            <th><?php echo $this->lang->line('data_request_customer_id');?></th>
            <th><?php echo $this->lang->line('data_request_status_id');?></th>
            <th><?php echo $this->lang->line('data_request_area_id');?></th>
            <th><?php echo $this->lang->line('data_invoice_teamwork_id');?></th>
            <th><?php echo $this->lang->line('data_request_details');?></th>
            <th></th>
            <th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($DataRows as $DataRows_Row) {
        ?>
        <tr>
            <td>
            <button class="btn btn-primary view_detail" relid="<?php echo $DataRows_Row->id;?>"><?php echo $DataRows_Row->id;?></button>
            
            </td>
            <td>
                <?php
                $user_id = $DataRows_Row->user_id;
                if($user_id > 0)
                {
                    $UserData = $this->M_usr_users->GetRow($user_id);
                    echo $UserData->fullname;
                }
                ?>
            </td>
            <td>
                <?php
                $customer_id = $DataRows_Row->customer_id;
                $CustomerData = $this->M_sale_customer->GetRow($customer_id);
                if ($this->session->userdata('lang') == "ar")
                {
                    echo $CustomerData->ar_title;
                }
                else
                {
                    echo $CustomerData->en_title;
                }
                ?>
            </td>
            <td>
                <?php
                $status_id = $DataRows_Row->status_id;
                if($status_id > 0)
                {
                    $StatusData = $this->M_set_request_status->GetRow($status_id);
                    if ($this->session->userdata('lang') == "ar")
                    {
                        echo $StatusData->ar_title;
                    }
                    else
                    {
                        echo $StatusData->en_title;
                    }
                }
                ?>
            </td>
            <td>
                <?php
                $area_id = $DataRows_Row->area_id;
                $AreaData = $this->M_set_area->GetRow($area_id);
                if ($this->session->userdata('lang') == "ar")
                {
                    echo $AreaData->ar_title;
                }
                else
                {
                    echo $AreaData->en_title;
                }
                ?>
            </td>
            <td>
                <?php
                $teamwork_id = $DataRows_Row->teamwork_id;
                $TeamWorkData = $this->M_set_teamwork->GetRow($teamwork_id);
                if ($this->session->userdata('lang') == "ar")
                {
                    echo $TeamWorkData->ar_title;
                }
                else
                {
                    echo $TeamWorkData->en_title;
                }
                ?>
            </td>
            <td><?php echo $DataRows_Row->details; ?></td>
            <td>
                <?php
                if($status_id <> 5) {
                ?>
                <a href="<?php echo base_url().$Segment1."/".$Segment2."/expenses/".$DataRows_Row->id;?>" style="font-size: 12px;">
                <?php echo $this->lang->line('Add_Cost');?>
                </a>
                <?php } ?>
            </td>
            <td style="text-align:center">
                <a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
                    <i class="flaticon-edit-1 text-primary icon-lg"></i> 
                </a>
            </td>
            <td style="text-align:center">
                <?php
                    if($DataRows_Row->deleted == 0) {
                ?>
                <a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
                    <i class="flaticon-delete text-danger icon-lg"></i> 
                </a>
                <?php } ?>
            </td>
            <td style="text-align:center">
                <?php
                    if($DataRows_Row->deleted == 1) {
                ?>
                    <a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
                        <i class="flaticon-refresh text-success icon-lg"></i> 
                    </a>
                <?php } ?>
            </td>
        </tr>
        <?php
        }
        ?>
    </tbody>
</table>


<div id="show_modal" class="modal fade" role="dialog" style="background: #000;">
    <div class="modal-dialog" style="min-width: 600px;">
        <div class="modal-content">
            <div class="modal-header">
                <h3 style="font-size: 24px; color: #17919e; text-shadow: 1px 1px #ccc;">
                <i class="fa fa-folder"></i><?php echo lang('data_request');?>
                </h3>
            </div>
            <div class="modal-body">
				<div class="row">
					<div class="col-md-3">ID</div>
					<div class="col-md-3" id="id"></div>
                    <div class="col-md-3"><?php echo $this->lang->line('data_invoice_teamwork_id');?></div>
					<div class="col-md-3" id="teamwork_name"></div>
				</div>
                <div class="row">
					<div class="col-md-3"><?php echo lang('data_request_thedate');?></div>
					<div class="col-md-3" id="thedate"></div>
					<div class="col-md-3"><?php echo lang('data_request_customer_id');?></div>
					<div class="col-md-3" id="customer_name"></div>
				</div>
				<div class="row">
					<div class="col-md-3"><?php echo lang('data_request_status_id');?></div>
                    <div class="col-md-3" id="status_id"></div>
					<div class="col-md-3"><?php echo lang('data_request_area_id');?></div>
                    <div class="col-md-3" id="area_field_id"></div>
				</div>
				<div class="row">
					<div class="col-md-12" id="details"></div>
				</div>
				<table id="ModalTable" class="table table-bordered table-striped">
					<thead class="btn-primary">
						<tr>
							<th align='right'><?php echo lang('set_service_title');?></th>
							<th style="text-align:center"><?php echo lang('qunatity');?></th>
							<th style="text-align:center"><?php echo lang('set_service_price');?></th>
                            <th style="text-align:center"><?php echo lang('Total');?></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
            </div>
            <div class="modal-footer noPrint">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo lang('Close');?></button>
                <button type="button" class="btn btn-info" onclick="printSelection(document.getElementById('show_modal'));return false;"><i class="fa fa-print"></i> <?php echo $this->lang->line('Print');?></button>
            </div>
        </div>
    </div>
</div>