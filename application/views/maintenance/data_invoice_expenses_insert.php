<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
    $FormPath = base_url().$Segment1."/".$Segment2."/expenses_insert";
    echo form_open_multipart($FormPath);
?>
    <div class="kt-portlet__body">
        <div class="form-group row">
            <div class="form-group col-md-3">
                    <label><?php echo $this->lang->line('amount_tax');?></label>
                    <input type="number" min="1" step="0.001" id="amount" name="amount" class="form-control" required readonly>
                </div>
                <div class="form-group col-md-3">
                    <label><?php echo $this->lang->line('Tax');?></label>
                    <input type="number" min="0.00" max="1" step="0.01" id="tax" name="tax" class="form-control" onblur="Calculate()" onkeydown="Calculate()" required>
                </div>
                <div class="form-group col-md-3">
                    <label><?php echo $this->lang->line('data_request_thedate');?></label>
                    <input type="date" name="thedate" value="<?php echo date("Y-m-d");?>" class="form-control" required>
                </div>
                <div class="form-group col-md-3">
                    <label><?php echo $this->lang->line('data_request_thetime');?></label>
                    <input type="time" name="thetime" value="<?php echo date("h:m");?>" class="form-control" required>
                </div>
                <div class="form-group col-md-12">
                    <label><?php echo $this->lang->line('data_request_details');?></label>
                    <input type="text" name="details" class="form-control" required>
                </div>
            </div>
            <div class="row">
                <hr>
            </div>
            <div class="row alert alert-info" style="height: 45px">
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('data_invoice_services_title');?></label>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('data_invoice_services_price');?></label>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('qunatity');?></label>
                </div>
            </div>
            <?php
            $i = 0;
            $ServiceID = 0;
            $ServiceTitle = "";
            foreach($Service as $Service_Row) {
                $ServiceID = $Service_Row->id;
                if ($this->session->userdata('lang') == "ar")
                {
                    $ServiceTitle = $Service_Row->ar_title;
                }
                else
                {
                    $ServiceTitle = $Service_Row->en_title;
                }
                $i +=1;
                if (fmod($i,2) ==1)
                {
                    $ClassName = " alert alert-warning";
                }
                else {
                    $ClassName = " alert alert-success";
                }
            ?>
            <div class="row <?php echo $ClassName;?>" style="height: 65px">
                <div class="form-group col-md-4">
                    <input type="text" min="0" step="0.001" name="service_title<?php echo $ServiceID;?>" value="<?php echo $ServiceTitle;?>" class="form-control" readonly>
                    <input type="hidden" name="service_id<?php echo $ServiceID;?>" class="form-control" value="<?php echo $ServiceID;?>">
                </div>
                <div class="form-group col-md-4">
                    <input type="number" min="0" step="0.001" id="unit_price<?php echo $ServiceID;?>" name="unit_price<?php echo $ServiceID;?>" value="<?php echo $Service_Row->price;?>" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <input type="number" min="0" step="1" id="qunatity<?php echo $ServiceID;?>" name="qunatity<?php echo $ServiceID;?>" value="0" onblur="Calculate()" onkeydown="Calculate()" class="form-control">
                </div>
            </div>
            <?php
            }
            ?>
    </div>
    
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            <div class="row">
                <div class="col-lg-12 ml-lg-auto">
                    <input type="hidden" name="id" value="<?php //echo $id;?>">
                    <input type="hidden" name="deleted" value="0">
                    <input type="hidden" name="insert_date" value="<?php echo date("Y-m-d");?>">
                    <input type="hidden" name="insert_time" value="<?php echo date("h:m");?>">
                    <input type="hidden" name="invoice_id" value="<?php echo $invoice_id;?>">
                    
                    <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                    <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                </div>
            </div>
        </div>
    </div>
    <?php
        echo form_close();
    ?>
</div>