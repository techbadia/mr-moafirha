
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('tasks_table_title');?></th>
												<th><?php echo lang('tasks_table_fromuser');?></th>
												<th><?php echo lang('tasks_table_touser');?></th>
												<th><?php echo lang('tasks_table_levels');?></th>
												<th><?php echo lang('tasks_table_start');?></th>
												<th><?php echo lang('tasks_table_end');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('tasks_Failed');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('tasks_cancel');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$store_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
												<?php
												$task_id = $DataRows_Row->task_id;
												$TaskData = $this->M_tas_tasks->GetRow($task_id);
												echo $TaskData->title;
												?>
												</td>
												<td>
												<?php
												$from_user = $DataRows_Row->from_user;
												$UserData = $this->M_usr_users->GetRow($from_user);
												echo $UserData->fullname;
												?>
												</td>
												<td>
												<?php
												$to_user = $DataRows_Row->to_user;
												$UserData = $this->M_usr_users->GetRow($to_user);
												echo $UserData->fullname;
												?>
												</td>
												<td><?php echo $DataRows_Row->details;?></td>
												<td><?php echo $DataRows_Row->start_date." ".$DataRows_Row->start_time;?></td>
												<td><?php echo $DataRows_Row->end_date." ".$DataRows_Row->end_time;?></td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/task_action/".$DataRows_Row->id."/2";?>">
														<i class="flaticon2-sort-down text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/task_action/".$DataRows_Row->id."/3";?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

								