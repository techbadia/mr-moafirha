
<div class="card card-custom gutter-b example example-compact">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
                        echo form_open_multipart($FormPath);
                    ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('tasks_title');?></label>
												<input type="text" name="title" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('tasks_task_type');?></label>
												<select name="task_type" class="form-control">
													<option value="1">تنفذ مرة واحدة</option>
													<option value="2">تنفذ عدد محدد متكرر</option>
													<option value="3">تنفذ عدد لانهائي من المرات</option>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('tasks_levels');?></label>
												<input type="number" name="levels" min="1" step="1" class="form-control" value="1" required>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<label class="col-form-label"><?php echo lang('tasks_details');?></label>
												<input type="text" name="details" class="form-control" value="" required>
											</div>

										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<?php
													$company_id = intval($this->session->userdata('company_id'));
													?>
													<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
													<input type="hidden" name="web_store" value="0">
													<input type="hidden" name="deleted" value="0">

													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>