
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('tasks_table_title');?></th>
												<th><?php echo lang('tasks_table_fromuser');?></th>
												<th><?php echo lang('tasks_table_touser');?></th>
												<th><?php echo lang('tasks_table_levels');?></th>
												<th><?php echo lang('tasks_table_start');?></th>
												<th><?php echo lang('tasks_table_end');?></th>
												<th style="text-align:center"><?php echo lang('tasks_Time_spent');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$store_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
												<?php
												$task_id = $DataRows_Row->task_id;
												$TaskData = $this->M_tas_tasks->GetRow($task_id);
												echo $TaskData->title;
												?>
												</td>
												<td>
												<?php
												$from_user = $DataRows_Row->from_user;
												$UserData = $this->M_usr_users->GetRow($from_user);
												echo $UserData->fullname;
												?>
												</td>
												<td>
												<?php
												$to_user = $DataRows_Row->to_user;
												$UserData = $this->M_usr_users->GetRow($to_user);
												echo $UserData->fullname;
												?>
												</td>
												<td><?php echo $DataRows_Row->details;?></td>
												<td><?php echo $DataRows_Row->start_date." ".$DataRows_Row->start_time;?></td>
												<td><?php echo $DataRows_Row->end_date." ".$DataRows_Row->end_time;?></td>
												<td style="text-align:center">
												<?php
												$UserStart = $DataRows_Row->user_start_date." ".$DataRows_Row->user_start_time;
												$UserEnd = $DataRows_Row->user_end_date." ".$DataRows_Row->user_end_time;
												$date1 = strtotime($UserStart);  
												$date2 = strtotime($UserEnd);
												$diff = abs($date2 - $date1);  
												$years = floor($diff / (365*60*60*24));  
												$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 
												$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)); 
												$hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
												$minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60); 
												printf("%d يوم, %d ساعة, %d دقيقة", $days, $hours, $minutes); 
												?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

								