
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('tasks_from_user');?></th>
												<th><?php echo lang('tasks_title');?></th>
												<th><?php echo lang('tasks_details');?></th>
												<th><?php echo lang('tasks_task_type');?></th>
												<th><?php echo lang('tasks_status');?></th>
												<th><?php echo lang('tasks_levels');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$store_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
												<?php
												$user_id = $DataRows_Row->from_user;
												$UserData = $this->M_usr_users->GetRow($user_id);
												echo $UserData->fullname;
												?>
												</td>
												<td><?php echo $DataRows_Row->title;?></td>
												<td><?php echo $DataRows_Row->details;?></td>
												<td>
												<?php
												if($DataRows_Row->task_type == 1)
												{
													echo "تنفذ مرة واحدة";
												}
												else if($DataRows_Row->task_type == 2)
												{
													echo "تنفذ عدد محدد متكرر";
												}
												else
												{
													echo "تنفذ عدد لانهائي من المرات";
												}
												?>
												</td>
												<td><?php echo $DataRows_Row->status;?></td>
												<td><?php echo $DataRows_Row->levels;?></td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

								