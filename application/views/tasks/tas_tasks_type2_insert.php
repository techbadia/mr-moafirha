
<div class="card card-custom gutter-b example example-compact">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_type2_data";
                        echo form_open_multipart($FormPath);
                    ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-md-6">
												<label class="col-form-label"><?php echo lang('tasks_days');?></label>
												<input type="number" name="days" min="1" step="1" class="form-control" value="1" required>
											</div>
											<div class="col-md-6">
												<label class="col-form-label"><?php echo lang('tasks_doloop');?></label>
												<input type="number" name="doloop" min="1" step="1" class="form-control" value="1" required>
											</div>
										</div>
										<?php
										for ($i = 1; $i <= $levels; $i++) {
										?>
										<div class="form-group row">
											<div class="col-md-6">
												<label class="col-form-label"><?php echo lang('tasks_details');?></label>
												<input type="text" name="details<?php echo $i;?>" class="form-control" value="" required>
											</div>
											<div class="col-md-6">
												<label class="col-form-label"><?php echo lang('tasks_to_user');?></label>
												<select name="to_user<?php echo $i;?>" class="form-control">
													<?php
														$UsersList = $this->M_usr_users->GetMultiRow();
														foreach($UsersList as $UsersList_Row) {
														?>
														<option value="<?php echo $UsersList_Row->id;?>"><?php echo $UsersList_Row->fullname;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-md-3">
												<label class="col-form-label"><?php echo lang('tasks_start_date');?></label>
												<input type="date" name="start_date<?php echo $i;?>" class="form-control" value="<?php echo date("Y-m-d");?>" required>
											</div>
											<div class="col-md-3">
												<label class="col-form-label"><?php echo lang('tasks_start_time');?></label>
												<input type="time" name="start_time<?php echo $i;?>" class="form-control" value="<?php echo date("H:m");?>" required>
											</div>
											<div class="col-md-3">
												<label class="col-form-label"><?php echo lang('tasks_end_date');?></label>
												<input type="date" name="end_date<?php echo $i;?>" class="form-control" value="<?php echo date("Y-m-d");?>" required>
											</div>
											<div class="col-md-3">
												<label class="col-form-label"><?php echo lang('tasks_end_time');?></label>
												<input type="time" name="end_time<?php echo $i;?>" class="form-control" value="<?php echo date("H:m");?>" required>
											</div>
										</div>
										<?php
										}
										?>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<?php
													$company_id = intval($this->session->userdata('company_id'));
													?>
													<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
													<input type="hidden" name="levels" value="<?php echo $levels;?>">
													<input type="hidden" name="deleted" value="0">

													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>