<div class="card card-custom gutter-b example example-compact">
                    <?php
                    $FormPath = base_url().$Segment1."/".$Segment2."/update_data";
                    echo form_open_multipart($FormPath);
                    ?>
                        <div class="form-body">
                            
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('ar_title');?></label>
                                        <input type="text" id="ar_title" name="ar_title" class="form-control" value="<?php echo $DataRow_ar_title;?>" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('en_title');?></label>
                                        <input type="text" id="en_title" name="en_title" class="form-control" value="<?php echo $DataRow_en_title;?>" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('phone');?></label>
                                        <input type="text" id="phone" name="phone" class="form-control" value="<?php echo $DataRow_phone;?>" required>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('mobile1');?></label>
                                        <input type="text" id="mobile1" name="mobile1" class="form-control" value="<?php echo $DataRow_mobile1;?>" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('mobile2');?></label>
                                        <input type="text" id="mobile2" name="mobile2" class="form-control" value="<?php echo $DataRow_mobile2;?>" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('address');?></label>
                                        <input type="text" id="address" name="address" class="form-control" value="<?php echo $DataRow_address;?>" required>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('email');?></label>
                                        <input type="email" id="email" name="email" class="form-control" value="<?php echo $DataRow_email;?>" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('credit_limit');?></label>
                                        <input type="number" min="0" step="1" id="credit_limit" name="credit_limit" class="form-control" value="<?php echo $DataRow_credit_limit;?>" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                    	<?php
                                    	$ActiveCustomer = trim($DataRow_active);
                                    	?>
                                        <label><?php echo $this->lang->line('active');?></label>
                                        <select id="active" name="active" class="form-control" required>
                                        	<option value="True" <?php if ($ActiveCustomer =="True") {?> selected<?php }?>><?php echo $this->lang->line('active');?></option>
                                        	<option value="False" <?php if ($ActiveCustomer =="False") {?> selected<?php }?>><?php echo $this->lang->line('Notactive');?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('commercial_register');?></label>
                                        <input type="text" name="commercial_register" class="form-control" value="<?php echo $DataRow_commercial_register;?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('tax_card');?></label>
                                        <input type="text" name="tax_card" class="form-control" value="<?php echo $DataRow_tax_card;?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('person');?></label>
                                        <input type="text" name="person" class="form-control" value="<?php echo $DataRow_person;?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('person_phone');?></label>
                                        <input type="text" name="person_phone" class="form-control" value="<?php echo $DataRow_person_phone;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions right" style="text-align: right; font-weight: bold !important; font-size: 18px;">
                            <input type="hidden" id="id" name="id" value="<?php echo $DataRow_id;?>">
                            <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                        </div>
                    <?php
                        echo form_close();
                    ?>
                </div>
            