<div class="card card-custom gutter-b example example-compact">
    <?php
    $FormPath = base_url() . $Segment1 . "/" . $Segment2 . "/insert_data";
    $attributes = array('id' => 'SendData', 'name' => 'SendData');
    echo form_open_multipart($FormPath, $attributes);
    ?>
    <div class="form-body">
        <div class="form-group row">
            <div class="col-lg-3 col-md-3 col-sm-12">
                <label class="col-form-label"><?php echo lang('sale_offer_customer_id'); ?></label>
                <select name="customer_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true"
                        required>
                    <?php
                    $CustomersList = $this->M_sale_customer->GetMultiRow();
                    foreach ($CustomersList as $CustomersList_Row) {
                        if ($this->session->userdata('lang') == "ar") {
                            $CustomerTitle = $CustomersList_Row->ar_title;
                        } else {
                            $CustomerTitle = $CustomersList_Row->en_title;
                        }
                        ?>
                        <option value="<?php echo $CustomersList_Row->id; ?>"><?php echo $CustomerTitle; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <label class="col-form-label"><?php echo lang('sale_offer_thedate'); ?></label>
                <input type="date" name="thedate" class="form-control" value="<?php echo date("Y-m-d"); ?>" required>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <label class="col-form-label"><?php echo lang('buy_offer_expired_date'); ?></label>
                <input type="date" name="expired_date" class="form-control" value="<?php echo date("Y-m-d"); ?>"
                       required>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <label class="col-form-label"><?php echo lang('proj_project_title'); ?></label>
                <select name="project_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true"
                        required>
                    <option value="0">-</option>
                    <?php
                    $ProjectsList = $this->M_proj_project->GetMultiRow();
                    foreach ($ProjectsList as $ProjectsList_Row) {
                        if ($this->session->userdata('lang') == "ar") {
                            $ProjectTitle = $ProjectsList_Row->ar_title;
                        } else {
                            $ProjectTitle = $ProjectsList_Row->en_title;
                        }
                        ?>
                        <option value="<?php echo $ProjectsList_Row->id; ?>"><?php echo $ProjectTitle; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <label class="col-form-label"><?php echo lang('sale_offer_image'); ?></label>
                <input type="file" name="image" class="form-control" dir="ltr" >
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
                <label class="col-form-label" style="color:#FFFFFF"><?php echo lang('product_barcode'); ?></label>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 p-3 mb-2 bg-primary text-white">
                <label class="col-form-label"
                       style="color:#FFFFFF"><?php echo lang('sale_offer_items_store_type_id'); ?></label>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
                <label class="col-form-label"
                       style="color:#FFFFFF"><?php echo lang('sale_offer_items_quantity'); ?></label>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
                <label class="col-form-label"
                       style="color:#FFFFFF"><?php echo lang('sale_offer_items_unitprice'); ?></label>
            </div>
            <?php
            $InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
            $BackgroundColor = "p-3 mb-2 bg-light text-dark";
            for ($i = 1; $i <= $InvoiceItems; $i++) {
                if ($BackgroundColor == "p-3 mb-2 bg-light text-dark") {
                    $BackgroundColor = "p-3 mb-2 bg-white text-dark";
                } else {
                    $BackgroundColor = "p-3 mb-2 bg-light text-dark";
                }
                ?>
                <div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor; ?>">
                    <input type="text" name="barcode<?php echo $i; ?>" id="barcode<?php echo $i; ?>"
                           onfocus="GetName(<?php echo $i; ?>)" onblur="GetName(<?php echo $i; ?>)" class="form-control"
                           value="">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 <?php echo $BackgroundColor; ?>">
                    <input type="text" name="title<?php echo $i; ?>" id="title<?php echo $i; ?>" class="form-control"
                           value="">
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor; ?>">
                    <input type="number" min="0" step="1" name="quantity<?php echo $i; ?>"
                           id="quantity<?php echo $i; ?>" onchange="SubValue(); AllTotal(); Remaing();"
                           onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();"
                           class="form-control" value="0">
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor; ?>">
                    <input type="number" min="0" step="1" name="unitprice<?php echo $i; ?>"
                           id="unitprice<?php echo $i; ?>" onchange="SubValue(); AllTotal(); Remaing();"
                           onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();"
                           class="form-control" value="0">
                    <input type="hidden" name="subtotal<?php echo $i; ?>" id="subtotal<?php echo $i; ?>">
                    <input type="hidden" name="location_id<?php echo $i; ?>" value="0">
                </div>
                <?php
            }
            ?>
        </div>
        <div class="append-row">

        </div>

        <div class="form-group row">
            <div class="col-lg-3 col-md-3 col-sm-12">
                <div class="form-group">
                    <span class="svg-icon svg-icon-md add" style="float:right; margin-top:-5px;" id="submitbtn">
                        <i class="fa fa-plus-circle fa-3x"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-3 col-md-3 col-sm-12">
                <div class="form-group">
                    <label class="col-form-label"><?php echo lang('buy_offer_tax'); ?></label>
                    <div class="input-group">
                        <input type="number" min="0" name="tax" id="tax" class="form-control" onchange="AllTotal();"
                               onblur="AllTotal();" onkeyup="AllTotal();" value="0" required>
                        <div class="input-group-prepend">
                            <span class="input-group-text">%</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <label class="col-form-label"><?php echo lang('buy_offer_over_cost'); ?></label>
                <input type="number" min="0" name="over_cost" id="over_cost" class="form-control" onchange="AllTotal();"
                       onblur="AllTotal();" onkeyup="AllTotal();" value="0" required>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <label class="col-form-label"><?php echo lang('sale_offer_discount'); ?></label>
                <input type="number" min="0" name="discount" id="discount" class="form-control" onchange="AllTotal();"
                       onblur="AllTotal();" onkeyup="AllTotal();" value="" required>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <label class="col-form-label"><?php echo lang('sale_offer_totalvalue'); ?></label>
                <input type="number" min="0" name="totalvalue" id="totalvalue" class="form-control" value="" required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <label class="col-form-label"><?php echo lang('buy_offer_notes'); ?></label>
                <input type="text" name="notes" class="form-control" value="" required>
            </div>
        </div>
    </div>
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            <div class="row">
                <div class="col-lg-12 ml-lg-auto">
                    <input type="hidden" name="id" value="<?php //echo $id;?>">
                    <input type="hidden" name="deleted" value="0">
                    <input type="hidden" name="tree_id" value="0">
                    <input type="hidden" min="0" name="paid" id="paid" onchange="Remaing();" onblur="Remaing();"
                           onkeyup="Remaing();" value="0">
                    <input type="hidden" min="0" name="remaining" id="remaining" value="">
                    <input type="hidden" name="RowsCount" id="RowsCount" value="<?php echo $InvoiceItems; ?>">
                    <a href="<?php echo base_url() . $Segment1 . "/" . $Segment2; ?>" class="btn btn-secondary"
                       accesskey="b"><?php echo $this->lang->line('Back'); ?></a>
                    <button type="submit" class="btn btn-primary mr-2"
                            accesskey="s"><?php echo $this->lang->line('Save'); ?></button>
                </div>
            </div>
        </div>
    </div>
    <?php
    echo form_close();
    ?>
</div>



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>





<script>



    $(document).ready(function () {

        var count = 1;



        $(document).on('click', '#submitbtn', function () {
            var SelectBox = "";

            var BackgroundColor = "p-3 mb-2 bg-success-o-40";



            if (BackgroundColor == "p-3 mb-2 bg-success-o-40") {

                BackgroundColor = "p-3 mb-2 bg-success-o-20";

            } else {

                BackgroundColor = "p-3 mb-2 bg-success-o-40";

            }



            count++;

            var html = '';

            html += '<div class="form-group row">';

            html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';

            html += '<input type="text" name="barcode' + count + '" id="barcode' + count + '" class="form-control" value="" onfocus="GetName(' + count + '); GetSalePrice(' + count + ');  GetSaleMinPrice(' + count + ');  GetStores(' + count + ')" onblur="GetName(' + count + '); GetSalePrice(' + count + ');  GetSaleMinPrice(' + count + ');  GetStores(' + count + ')" >';

            html += '</div>';

            html += '<div class="col-lg-6 col-md-6 col-sm-12 ' + BackgroundColor + '">';

            html += '<input type="text" name="title' + count + '" id="title' + count + '" class="form-control" value="">';

            html += '</div>';

            html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';

            html += '<input type="number" step="1" min="0" name="quantity' + count + '" id="quantity' + count + '" class="form-control" value="0" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();">';

            html += '</div>';

            html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';

            html += '<input type="number" step="1" min="0" name="unitprice' + count + '" id="unitprice' + count + '" class="form-control" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();" value="0">';

            html += '<input type="hidden" name="subtotal' + count + '" id="subtotal' + count + '" class="form-control" value="0">';

            html += '<input type="hidden" name="location_id' + count + '" value="0"';
            html += '</div>';

            html += '</div>';


            $('#RowsCount').val(count);

            $('.append-row').append(html);

        });



        $(document).on('click', '.remove', function () {

            $(this).closest('tr').remove();

        });



    });

</script>