<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <?php
        $FormPath = base_url()."sales/sale_customer/scheduling_insert";
        echo form_open_multipart($FormPath);
    ?>
        <div class="form-body">
            <div class="form-group row">
                <div class="form-group col-md-12">
                    <label><?php echo $this->lang->line('sale_fullname');?>:</label>
                    <select id="customer_id" name="customer_id" class="form-control" required>
                        <?php
                        $CustomersList = $this->M_sale_customer->GetMultiRow();
                        foreach($CustomersList as $CustomersList_Row) {
                            $Title = "";
                            if ($this->session->userdata('lang') == "ar")
                            {
                                $Title = $CustomersList_Row->ar_title;
                            }
                            else
                            {
                                $Title = $CustomersList_Row->en_title;
                            }
                        ?>
                        <option value="<?php echo $CustomersList_Row->id;?>"><?php echo $Title;?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <?php
            for ($i = 1; $i <= 12; $i++) {
            ?>
            <div class="form-group row">
                <div class="col-md-6">
                    <label><?php echo $this->lang->line('thedate');?>:</label>
                    <input type="date" id="thedate<?php echo $i;?>" name="thedate<?php echo $i;?>" value="<?php echo date("Y-m-d");?>" class="form-control" required>
                </div>
                <div class="col-md-6">
                    <label><?php echo $this->lang->line('payment_amount');?>:</label>
                    <input type="number" min="0" step="0.001" id="amount<?php echo $i;?>" name="amount<?php echo $i;?>" value="0" class="form-control" required>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
        <div class="form-actions right" style="text-align: right; font-weight: bold !important; font-size: 18px;">
            <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
        </div>
    <?php
        echo form_close();
    ?>
</div>
            