<div class="card card-custom gutter-b example example-compact">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
                        $attributes = array('id' => 'SendData', 'name' => 'SendData');
                        echo form_open_multipart($FormPath, $attributes);
                    ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_offer_supplier_id');?></label>
												<select name="customer_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
												<?php
													$CustomersList = $this->M_sale_customer->GetMultiRow();
													foreach($CustomersList as $CustomersList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$CustomerTitle = $CustomersList_Row->ar_title;
														}
														else
														{
															$CustomerTitle = $CustomersList_Row->en_title;
														}
													?>
													<option <?php if($customer_id == $CustomersList_Row->id) {?>selected<?php }?> value="<?php echo $SuppliersList_Row->id;?>"><?php echo $SupplierTitle;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_offer_thedate');?></label>
												<input type="date" name="thedate" class="form-control" value="<?php echo $thedate;?>" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_offer_expired_date');?></label>
												<input type="date" name="expired_date" class="form-control" value="<?php echo $expired_date;?>" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_title');?></label>
												<select name="project_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
													<option <?php if($project_id == 0) {?>selected<?php }?> value="0">-</option>
													<?php
													$ProjectsList = $this->M_proj_project->GetMultiRow();
													foreach($ProjectsList as $ProjectsList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$ProjectTitle = $ProjectsList_Row->ar_title;
														}
														else
														{
															$ProjectTitle = $ProjectsList_Row->en_title;
														}
													?>
													<option <?php if($project_id == $ProjectsList_Row->id) {?>selected<?php }?> value="<?php echo $ProjectsList_Row->id;?>"><?php echo $ProjectTitle;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<label class="col-form-label">
													<input type="checkbox" id="ChangeImage" name="ChangeImage" value="True" />&nbsp;
													<?php echo $this->lang->line('sale_offer_ChangeImage');?>:
												</label>
												<input type="file" name="image" class="form-control" dir="ltr">
											</div>
										</div>

										<div class="form-group row">
											<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
												<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('product_barcode');?></label>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12 p-3 mb-2 bg-primary text-white">
												<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('sale_offer_items_store_type_id');?></label>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
												<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('sale_offer_items_quantity');?></label>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
												<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('sale_offer_items_unitprice');?></label>
											</div>
											<?php
											$InvoiceItems = 0;
											$BackgroundColor = "p-3 mb-2 bg-light text-dark";
											$i = 0;
											foreach($DetailsRows as $DetailsRows_Row) {
												$InvoiceItems += 1;
												$i += 1;
												$store_type_id = $DetailsRows_Row->store_type_id;
												$quantity = $DetailsRows_Row->quantity;
												$unitprice = $DetailsRows_Row->unitprice;
												$ProductData = $this->M_product->GetRow($store_type_id);
												$barcode = $ProductData->barcode;
												if ($this->session->userdata('lang') == "ar")
												{
													$ProductTitle = $ProductData->title;
												}
												else
												{
													$ProductTitle = $ProductData->title_en;
												}
												if($BackgroundColor == "p-3 mb-2 bg-light text-dark")
												{
													$BackgroundColor = "p-3 mb-2 bg-white text-dark";
												}
												else
												{
													$BackgroundColor = "p-3 mb-2 bg-light text-dark";
												}
											?>
												<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
													<input type="text" name="barcode<?php echo $i;?>" id="barcode<?php echo $i;?>" onfocus="GetName(<?php echo $i;?>)" onblur="GetName(<?php echo $i;?>)" class="form-control" value="<?php echo $barcode;?>">
												</div>
												<div class="col-lg-6 col-md-6 col-sm-12 <?php echo $BackgroundColor;?>">
													<input type="text" name="title<?php echo $i;?>" id="title<?php echo $i;?>" class="form-control" value="<?php echo $ProductTitle;?>">
												</div>
												<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
													<input type="number" min="0" step="1" name="quantity<?php echo $i;?>" id="quantity<?php echo $i;?>" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();" class="form-control" value="<?php echo $quantity;?>">
												</div>
												<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
													<input type="number" min="0" step="1" name="unitprice<?php echo $i;?>" id="unitprice<?php echo $i;?>" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();" class="form-control" value="<?php echo $unitprice;?>">
													<input type="hidden" name="subtotal<?php echo $i;?>" id="subtotal<?php echo $i;?>">
													<input type="hidden" name="item<?php echo $i?>" id="item<?php echo $i;?>" value="<?php echo $DetailsRows_Row->id;?>">
													<input type="hidden" name="location_id<?php echo $i;?>" value="0">
												</div>
											<?php
											}
											?>
										</div>

										<div class="form-group row">
											<div class="col-lg-3 col-md-3 col-sm-12">
												<div class="form-group">
													<label class="col-form-label"><?php echo lang('buy_offer_tax');?></label>
													<div class="input-group">
														<input type="number" min="0" name="tax" id="tax" class="form-control" onchange="AllTotal();" onblur="AllTotal();" onkeyup="AllTotal();" value="<?php echo $tax;?>" required>
														<div class="input-group-prepend">
															<span class="input-group-text">%</span>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_offer_over_cost');?></label>
												<input type="number" min="0" name="over_cost" id="over_cost" class="form-control" onchange="AllTotal();" onblur="AllTotal();" onkeyup="AllTotal();" value="<?php echo $over_cost; ?>" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('sale_offer_discount');?></label>
												<input type="number" min="0" name="discount" id="discount" class="form-control" onchange="AllTotal();" onblur="AllTotal();" onkeyup="AllTotal();" value="<?php echo $discount;?>" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('sale_offer_totalvalue');?></label>
												<input type="number" min="0" name="totalvalue" id="totalvalue" class="form-control" value="<?php echo $totalvalue;?>" required>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<label class="col-form-label"><?php echo lang('buy_offer_notes');?></label>
												<input type="text" name="notes" class="form-control" value="<?php echo $notes;?>" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php echo $id;?>">
													<input type="hidden" name="deleted" value="<?php echo $deleted;?>">
													<input type="hidden" name="tree_id" value="<?php echo $tree_id;?>">
													<input type="hidden" min="0" name="paid" id="paid" onchange="Remaing();" onblur="Remaing();" onkeyup="Remaing();" value="<?php echo $paid;?>">
													<input type="hidden" min="0" name="remaining" id="remaining" value="<?php echo $remaining;?>">
													<input type="hidden" name="RowsCount" id="RowsCount" value="<?php echo $InvoiceItems;?>">
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>

								