<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
$attributes = array('id' => 'kt_form_2', 'name' => 'kt_form_2');
	$FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
	echo form_open_multipart($FormPath, $attributes);
?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-3 col-md-3 col-sm-12 bg-primary text-white">
				<label class="col-form-label"><?php echo lang('fin_journal_debit');?></label>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 bg-primary text-white">
				<label class="col-form-label"><?php echo lang('fin_journal_account_id');?></label>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 bg-primary text-white">
				<label class="col-form-label"><?php echo lang('buy_bill_remaining');?></label>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 bg-primary text-white">
				<label class="col-form-label"><?php echo lang('buy_bill_paid');?></label>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 bg-primary text-white">
				<label class="col-form-label"><?php echo lang('expenses_bill_id');?></label>
			</div>
		</div>
		<div class="form-group row">
		<?php
		$BackgroundColor = "p-3 mb-2 bg-light text-dark";
		$InvoiceItems = $this->M_sale_bill->GetNotFullPaidInvoice($customer_id);
		foreach($InvoiceItems as $MultiRows_Row) {
			$i = $MultiRows_Row->id;
			if($BackgroundColor == "p-3 mb-2 bg-success-o-40")
			{
				$BackgroundColor = "p-3 mb-2 bg-success-o-20";
			}
			else
			{
				$BackgroundColor = "p-3 mb-2 bg-success-o-40";
			}
		?>
			<div class="col-lg-3 col-md-3 col-sm-12 <?php echo $BackgroundColor;?>">
				<input type="number" step="0.001" min="0" name="debit<?php echo $i?>" id="debit<?php echo $i?>" class="form-control" onfocus="TotalDebitValue()" onblur="TotalDebitValue()" value="0">
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 <?php echo $BackgroundColor;?>">
				<select name="fromaccount<?php echo $i?>" class="form-control kt-selectpicker" data-size="5" data-live-search="true">
					<?php
					$acc_treasury = intval($this->session->userdata('acc_treasury'));
					$fromaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_treasury);
					foreach($fromaccount as $fromaccount_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$FromTitle = $fromaccount_Row->title;
						}
						else
						{
							$FromTitle = $fromaccount_Row->title_en;
						}
					?>
					<option value="<?php echo $fromaccount_Row->id;?>"><?php echo $FromTitle;?></option>
					<?php } ?>
					<?php
					$acc_bank = intval($this->session->userdata('acc_bank'));
					$fromaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_bank);
					foreach($fromaccount as $fromaccount_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$FromTitle = $fromaccount_Row->title;
						}
						else
						{
							$FromTitle = $fromaccount_Row->title_en;
						}
					?>
					<option value="<?php echo $fromaccount_Row->id;?>"><?php echo $FromTitle;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
				<input type="number" step="0.001" min="0" name="remaining<?php echo $i?>" id="remaining<?php echo $i?>" class="form-control" value="<?php echo $MultiRows_Row->remaining;?>">
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
				<input type="number" step="0.001" min="0" name="creditor<?php echo $i?>" id="creditor<?php echo $i?>" class="form-control" onfocus="TotalCreditorValue()" onblur="TotalCreditorValue()" value="0">
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
				<input type="text" name="bill_id<?php echo $i?>" id="bill_id<?php echo $i?>" class="form-control" value="<?php echo $MultiRows_Row->id;?>" readonly>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 <?php echo $BackgroundColor;?>">
				<input type="text" name="details<?php echo $i?>" id="details<?php echo $i?>" class="form-control" value="" placeholder="<?php echo lang('fin_journal_details');?>">
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<hr>
			</div>
		<?php } ?>
		</div>
		<div class="form-group row">
			<div class="col-lg-3 col-md-3 col-sm-12">
				<label class="col-form-label"><?php echo lang('fin_journal_debit_total');?></label>
				<input type="number" step="0.001" min="0" name="debit_total" id="debit_total" class="form-control" value="0">
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12"></div>
			<div class="col-lg-3 col-md-3 col-sm-12">
				<label class="col-form-label"><?php echo lang('fin_journal_creditor_total');?></label>
				<input type="number" step="0.001" min="0" name="creditor_total" id="creditor_total" class="form-control" value="0">
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12"></div>
		</div>
		<div class="form-group row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<label class="col-form-label"><?php echo lang('fin_journal_details');?></label>
				<input type="text" name="details" id="details" class="form-control" value="" required>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12">
				<label class="col-form-label"><?php echo lang('fin_journal_thedate');?></label>
				<input type="date" name="thedate" class="form-control" value="<?php echo date('Y-m-d');?>" required>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-12">
				<label class="col-form-label"><?php echo lang('fin_journal_image');?></label>
				<input type="file" name="image" id="image" class="form-control" dir="ltr" required>
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php //echo $id;?>">
					<?php
					$InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
					?>
					<input type="hidden" name="acc_invoice_items" id="acc_invoice_items" value="<?php echo $InvoiceItems;?>">
					<input type="hidden" name="deleted" value="0">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s" onclick="return Check_Debit_Creditor()"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>

</div>