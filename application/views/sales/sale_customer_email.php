<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
    <thead>
        <tr>
            <th>id</th>
            <th><?php echo $this->lang->line('sale_fullname');?></th>
            <th><?php echo $this->lang->line('thedate');?></th>
            <th><?php echo $this->lang->line('thetime');?></th>
            <th><?php echo $this->lang->line('serv_message_subject');?></th>
            <th><?php echo $this->lang->line('serv_message_message');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('View');?></th>
        </tr>
    </thead>
    <tbody>
    <?php
    $CustomersEmail = $this->M_sale_customer_email->GetMultiRow();
    foreach($CustomersEmail as $CustomersEmail_Row) {
    ?>
    <tr>
        <td><?php echo $CustomersEmail_Row->id;?></td>
        <td>
        <?php
        $customer_id = $CustomersEmail_Row->customer_id;
        $CustomerData = $this->M_sale_customer->GetRow($customer_id);
        if ($this->session->userdata('lang') == "ar")
        {
            $CustomerName = $CustomerData->ar_title;
        }
        else
        {
            $CustomerName = $CustomerData->en_title;
        }
        echo $CustomerName;
        ?>
        </td>
        <td><?php echo $CustomersEmail_Row->thedate;?></td>
        <td><?php echo $CustomersEmail_Row->thetime;?></td>
        <td><?php echo $CustomersEmail_Row->subject;?></td>
        <td><?php echo $CustomersEmail_Row->message;?></td>
        <td style="text-align:center">
        <button class="btn btn-primary mr-2 btn-sm view_detail" relid="<?php echo $CustomersEmail_Row->id;?>"><i class="fa fa-eye icon-sm"></i></button>
        </td>
    </tr>
    <?php
    }
    ?>
    </tbody>
</table>



<div id="show_modal" class="modal fade" role="dialog" style="background: #000;">
    <div class="modal-dialog" style="min-width: 600px;">
        <div class="modal-content">
        <div class="modal-header">
            <h3 style="font-size: 24px; color: #17919e; text-shadow: 1px 1px #ccc;">
            <i class="fa fa-folder"></i><?php echo lang('sale_customer_email');?>
            </h3>
        </div>
        <div class="modal-body">
        <div class="row">
            <div class="col-md-3"><?php echo lang('sale_fullname');?></div>
            <div class="col-md-3" id="customer_name"></div>
            <div class="col-md-3"><?php echo lang('phone');?></div>
            <div class="col-md-3" id="phone"></div>
        </div>
        <div class="row">
            <div class="col-md-3"><?php echo lang('thedate');?></div>
            <div class="col-md-3" id="thedate"></div>
            <div class="col-md-3"><?php echo lang('thetime');?></div>
            <div class="col-md-3" id="thetime"></div>
        </div>
        <div class="row">
            <div class="col-md-3"><?php echo lang('serv_message_subject');?></div>
            <div class="col-md-9" id="subject"></div>
        </div>
        <div class="row">
            <div class="col-md-3"><?php echo lang('serv_message_message');?></div>
            <div class="col-md-9" id="message"></div>
        </div>
        
        </div>
        <div class="modal-footer noPrint">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo lang('Close');?></button>
        </div>
        </div>
    </div>
</div>