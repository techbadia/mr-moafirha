<br>
	<div class="d-flex align-items-center p-4 bg-dark" style="text-align:center !important">
		<?php
		$all = "";
		$deleted = "";
		$full_paid = "";
		$part_paid = "";
		$not_paid = "";
		if($Segment3 == "")
		{
			$all = "active";
		}
		else if($Segment3 == "deleted")
		{
			$deleted = "active";
		}
		else
		{
			if($Segment4 == 1)
			{
				$full_paid = "active";
			}
			else if($Segment4 == 2)
			{
				$part_paid = "active";
			}
			else
			{
				$not_paid = "active";
			}
		}
		?>
		<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-transparent-primary <?php echo $all;?> font-weight-bold mr-2">
			<?php echo lang('buy_bill_all');?>
		</a>
		<a href="<?php echo base_url().$Segment1."/".$Segment2;?>/payment/1" class="btn btn-transparent-success <?php echo $full_paid;?> font-weight-bold mr-2">
			<?php echo lang('buy_returns_full_paid');?>
		</a>
		<a href="<?php echo base_url().$Segment1."/".$Segment2."/payment/2";?>" class="btn btn-transparent-warning <?php echo $part_paid;?> font-weight-bold mr-2">
			<?php echo lang('buy_returns_part_paid');?>
		</a>
		<a href="<?php echo base_url().$Segment1."/".$Segment2."/payment/3";?>" class="btn btn-transparent-danger <?php echo $not_paid;?> font-weight-bold mr-2">
			<?php echo lang('buy_returns_not_paid');?>
		</a>
		<!--
		<a href="#" class="btn btn-transparent-warning font-weight-bold mr-2">Warning</a>
		<a href="#" class="btn btn-transparent-white font-weight-bold">White</a>
		-->
		
	</div>
<br>
<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('sale_returns_customer_id');?></th>
												<th><?php echo lang('sale_returns_thedate');?></th>
												<th><?php echo lang('sale_returns_image');?></th>
												<th><?php echo lang('sale_returns_tree_id');?></th>
												<th><?php echo lang('sale_returns_totalvalue');?></th>
												<th><?php echo lang('sale_returns_paid');?></th>
												<th><?php echo lang('sale_returns_remaining');?></th>
												<th><?php echo lang('Pay');?></th>
												<th><?php echo lang('View');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												
												$sale_returns_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
													<?php
													$customer_id = $DataRows_Row->customer_id;
													$CustomerData = $this->M_sale_customer->GetRow($customer_id);
													if ($this->session->userdata('lang') == "ar")
													{
														$CustomerTitle = $CustomerData->ar_title;
													}
													else
													{
														$CustomerTitle = $CustomerData->en_title;
													}
													echo $CustomerTitle;
													?>
												</td>
												<td><?php echo $DataRows_Row->thedate;?></td>
												<td>
													<img src="<?php echo base_url();?>upload/sale_returns/<?php echo $DataRows_Row->image;?>" style="max-width:100px;">
												</td>
												<td>
													<?php
													$tree_id = $DataRows_Row->tree_id;
													$AccountData = $this->M_fin_treeaccount->GetRow($tree_id);
													if ($this->session->userdata('lang') == "ar")
													{
														$AccountTitle = $AccountData->title;
													}
													else
													{
														$AccountTitle = $AccountData->title_en;
													}
													echo $AccountTitle;
													?>
												</td>
												<td><?php echo $DataRows_Row->totalvalue;?></td>
												<td><?php echo $DataRows_Row->paid;?></td>
												<td><?php echo $DataRows_Row->remaining;?></td>
												<td>
												<?php
												$totalvalue = $DataRows_Row->totalvalue;
												$paid = $DataRows_Row->paid;
												if($totalvalue > $paid)
												{
													//echo "<a data-toggle='modal' href='#buy_bill_pay' relid='".$DataRows_Row->id."'>".lang('Pay')."</a>";
												?>
													<button class="btn btn-success btn-sm sale_returns_pay" relid="<?php echo $DataRows_Row->id;?>"><i class="flaticon-coins"></i></button>
												<?php
												}
												?>
												</td>
												<td>
												    <a class="btn btn-primary" href="<?=base_url("sales/sale_returns/view_pdf/".$DataRows_Row->id)?>" target="_blank"><i class="fa fa-file"></i></a>
												<button class="btn btn-primary view_detail" relid="<?php echo $DataRows_Row->id;?>"><i class="fa fa-eye"></i></button>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

									

		<div id="show_modal" class="modal fade" role="dialog" style="background: #000;">
			<div class="modal-dialog" style="min-width: 600px;">
				<div class="modal-content">
				<div class="modal-header">
					<h3 style="font-size: 24px; color: #17919e; text-shadow: 1px 1px #ccc;">
					<i class="fa fa-folder"></i><?php echo lang('sale_returns');?>
					</h3>
				</div>
				<div class="modal-body">
				<div class="row">
					<div class="col-md-3"><?php echo lang('expenses_bill_id');?></div>
					<div class="col-md-3" id="id"></div>
					<div class="col-md-3"><?php echo lang('expenses_thedate');?></div>
					<div class="col-md-3" id="thedate"></div>
				</div>
				<div class="row">
					<div class="col-md-3"><?php echo lang('sale_returns_customer_id');?></div>
					<div class="col-md-9" id="customer"></div>
				</div>
				<div class="row">
					<div class="col-md-3"><?php echo lang('sale_returns_totalvalue');?></div>
					<div class="col-md-3" id="totalvalue"></div>
					<div class="col-md-3"><?php echo lang('sale_returns_paid');?></div>
					<div class="col-md-3" id="paid"></div>
				</div>
				<div class="row">
					<div class="col-md-3"><?php echo lang('sale_returns_remaining');?></div>
					<div class="col-md-3" id="remaining"></div>
					<div class="col-md-3"><?php echo lang('sale_returns_discount');?></div>
					<div class="col-md-3" id="discount"></div>
				</div>
				<div class="row">
					<div class="col-md-3"><?php echo lang('sale_returns_tree_id');?></div>
					<div class="col-md-9" id="account_title"></div>
				</div>
				<div class="row">
					<div class="col-md-12" id="notes"></div>
				</div>
				<table id="ModalTable" class="table table-bordered table-striped">
					<thead class="btn-primary">
						<tr>
							<th align='right'><?php echo lang('sale_returns_items_store_type_id');?></th>
							<th style="text-align:center"><?php echo lang('sale_returns_items_quantity');?></th>
							<th style="text-align:center"><?php echo lang('sale_returns_items_unitprice');?></th>
							<th style="text-align:center"><?php echo lang('fin_journal_Total');?></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>

					

				</div>
				<div class="modal-footer noPrint">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo lang('Close');?></button>
					<button type="button" class="btn btn-info" onclick="printSelection(document.getElementById('show_modal'));return false;"><i class="fa fa-print"></i> <?php echo $this->lang->line('Print');?></button>
				</div>
				</div>
			</div>
		</div>




<div id="modalDiv">
	<div class="modal fade" id="sale_returns_pay" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: #c0edf1; text-align:<?php echo $this->session->userdata('Alignment');?> !important">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title" style="direction:<?php echo $this->session->userdata('Direction');?> !important">
						<?php echo $this->lang->line('sale_returns_pay');?>
					</h4>
				</div>
				<?php
				$FormName = "sale_returns_pay";
				$Target = base_url().$Segment1."/".$Segment2."/close_invoice";
					echo form_open_multipart($Target, "id=$FormName name=$FormName");
				?>
				<div id="modalDiv">
					<div class="modal-body">
						<div class="row">               
							<div class="col-lg-4 col-md-4 col-sm-4">
								<label><?php echo $this->lang->line('buy_bill_totalvalue');?></label>
								<input type="number" id="totalvalue" name="totalvalue" value="" readonly class="form-control" required>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<label><?php echo $this->lang->line('buy_bill_remaining');?></label>
								<input type="number" min="0" id="remaining" name="remaining" readonly value="" class="form-control" required>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<label><?php echo $this->lang->line('buy_bill_paid');?></label>
								<input type="number" min="0" step="0.001" id="paid" name="paid" readonly value="" class="form-control" required>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 15px; margin-bottom: 15px;">
								
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4 col-md-6 col-sm-6">
								<label><?php echo $this->lang->line('To_be_paid');?></label>
								<input type="number" min="0" step="0.001" id="paid_value" name="paid_value" class="form-control" value="" required autofocus>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-6">
								<label><?php echo $this->lang->line('Debit_from_customer_account');?></label>
								<select id="from_customer_account" name="from_customer_account" class="form-control" required>
									<option value="1"><?php echo $this->lang->line('Yes');?></option>
									<option value="0"><?php echo $this->lang->line('No');?></option>
								</select>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-6">
								<label><?php echo $this->lang->line('sale_returns_tree_id');?></label>
								<select id="tree_id" name="tree_id" class="form-control" required>
									<?php
									$acc_treasury = intval($this->session->userdata('acc_treasury'));
									$toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_treasury);
									foreach($toaccount as $toaccount_Row) {
										if ($this->session->userdata('lang') == "ar")
										{
											$ToTitle = $toaccount_Row->title;
										}
										else
										{
											$ToTitle = $toaccount_Row->title_en;
										}
									?>
									<option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
									<?php } ?>
									<?php
									$acc_bank = intval($this->session->userdata('acc_bank'));
									$toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_bank);
									foreach($toaccount as $toaccount_Row) {
										if ($this->session->userdata('lang') == "ar")
										{
											$ToTitle = $toaccount_Row->title;
										}
										else
										{
											$ToTitle = $toaccount_Row->title_en;
										}
									?>
									<option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<label class="col-form-label"><?php echo lang('buy_bill_image');?></label>
								<input type="file" name="image" class="form-control" dir="ltr" required>
							</div>
						</div>
						<div class="form-actions right noPrint" id="SaveButtons" style="text-align: right; font-weight: bold !important; font-size: 18px; margin-top: 10px;">
							<input type="hidden" name="id" id="id" value="">
							<button type="submit" class="btn btn-success" accesskey="s"><?php echo $this->lang->line('Pay');?></button>
						</div>
					</div>
				</div>
				<div class="modal-footer" id="FooterButtons">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $this->lang->line('Close');?></button>
					<button type="button" class="btn btn-info" onclick="printSelection(document.getElementById('buy_bill_pay'));return false;"><i class="fa fa-print"></i> <?php echo $this->lang->line('Print');?></button>
				</div>
				<?php
					echo form_close();
				?>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>