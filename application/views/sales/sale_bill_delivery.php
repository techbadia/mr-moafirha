<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
	$FormPath = base_url().$Segment1."/".$Segment2."/Delivery_Update";
	$attributes = array('id' => 'SendData', 'name' => 'SendData');
	echo form_open_multipart($FormPath, $attributes);
?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
				<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('product_barcode');?></label>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 p-3 mb-2 bg-primary text-white">
				<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('sale_bill_items_store_type_id');?></label>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
				<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('sale_bill_items_location_id');?></label>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
				<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('sale_bill_items_quantity');?></label>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
				<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('received');?></label>
			</div>

			<?php
			$InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
			$BackgroundColor = "p-3 mb-2 bg-success-o-40";
			foreach($DataRows as $DataRows_Row) {
				if($BackgroundColor == "p-3 mb-2 bg-success-o-40")
				{
					$BackgroundColor = "p-3 mb-2 bg-success-o-20";
				}
				else
				{
					$BackgroundColor = "p-3 mb-2 bg-success-o-40";
				}
				$i = $DataRows_Row->id;
				$store_type_id = $DataRows_Row->store_type_id;
				$ProductData = $this->M_product->GetRow($store_type_id);
				$barcode = $ProductData->barcode;
				if ($this->session->userdata('lang') == "ar")
				{
					$Product_title = $ProductData->title;
				}
				else
				{
					$Product_title = $ProductData->title_en;
				}
				
			?>
				<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
					<input type="text" name="barcode<?php echo $i;?>" id="barcode<?php echo $i;?>" readonly class="form-control" value="<?php echo $barcode;?>">
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 <?php echo $BackgroundColor;?>">
					<input type="text" name="title<?php echo $i;?>" id="title<?php echo $i;?>" readonly class="form-control" value="<?php echo $Product_title;?>">
				</div>
				<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
					<select name="location_id<?php echo $i;?>" id="location_id<?php echo $i;?>" onchange="GetStores(<?php echo $i;?>);" class="form-control">
						<?php
						$StoreList = $this->M_store->GetMultiRow();
						foreach($StoreList as $StoreList_Row) {
							$store_id = $StoreList_Row->id;
							$user_id = intval($this->session->userdata('StaffID'));
							$CheckUserStore = $this->M_usr_users_store->CheckStore($user_id, $store_id);
							if($CheckUserStore > 0) {
								if ($this->session->userdata('lang') == "ar")
								{
									$FromStore = $StoreList_Row->title;
								}
								else
								{
									$FromStore = $StoreList_Row->title_en;
								}
								$parent_id = $StoreList_Row->parent_id;
								if($parent_id > 0)
								{
									$StoreData = $this->M_store->GetRow($parent_id);
									if ($this->session->userdata('lang') == "ar")
									{
										$MainStore = $StoreData->title." : ";
									}
									else
									{
										$MainStore = $StoreData->title_en." : ";
									}
								}
								else
								{
									$MainStore = "";
								}
						?>
						<option <?php if($DataRows_Row->location_id == $StoreList_Row->id) {?>selected<?php }?> value="<?php echo $StoreList_Row->id;?>"><?php echo $MainStore.$FromStore;?></option>
						<?php
							}
						}
						?>
					</select>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
					<input type="number" min="0" step="1" name="quantity<?php echo $i;?>" id="quantity<?php echo $i;?>" readonly class="form-control" value="<?php echo $DataRows_Row->quantity;?>">
				</div>
				<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
					<input type="number" min="0" max="<?php echo $DataRows_Row->quantity;?>" step="1" name="received<?php echo $i;?>" id="received<?php echo $i;?>" class="form-control" value="0">
					<input type="hidden" name="bill_id<?php echo $i;?>" value="<?php echo $DataRows_Row->bill_id;?>">
					<input type="hidden" name="row_id<?php echo $i;?>" value="<?php echo $DataRows_Row->id;?>">
				</div>

			<?php
			}
			?>
		</div>

	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="0">
					<input type="hidden" name="deleted" value="0">
					<input type="hidden" name="rid" value="<?php echo $rid;?>">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>
</div>

							