<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
    <thead>
        <tr>
            <th>id</th>
            <th><?php echo $this->lang->line('ar_title');?></th>
            <th><?php echo $this->lang->line('fin_treeaccount_account_no');?></th>
            <th><?php echo $this->lang->line('fin_journal_account_id');?></th>
            <th><?php echo $this->lang->line('phone');?></th>
            <th><?php echo $this->lang->line('startamount');?></th>
            <th><?php echo $this->lang->line('Debit');?></th>
            <th><?php echo $this->lang->line('Creditor');?></th>
            <th><?php echo $this->lang->line('Balance');?></th>
            <th><?php echo $this->lang->line('sale_customer_Debt_scheduling');?></th>
            <th><?php echo $this->lang->line('Pay');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('View');?></th>
        </tr>
    </thead>
    <tbody>
    <?php
    foreach($MultiRows as $MultiRows_Row) {
        $AccountID = $MultiRows_Row->id;
    ?>
    <tr>
        <td><?php echo $MultiRows_Row->id;?></td>
        <td>
            <a href="<?php echo base_url();?>account/fin_treeaccount/view_account/<?php echo $MultiRows_Row->id;?>" target="_blank"><?php echo $MultiRows_Row->ar_title;?></a>
        </td>
        <td><?php echo $MultiRows_Row->account_no;?></td>
          <td><?php echo $MultiRows_Row->title;?></td>
        <td><?php echo $MultiRows_Row->phone." ".$MultiRows_Row->mobile1;?></td>
        <td>
            <?php
            $StartAmount = 0;
            $Total_debit = 0;
            $Total_creditor = 0;
            $AccountInfo = $this->M_fin_treeaccount->GetRow($AccountID);
            echo $AccountInfo->startamount;
            $StartAmount = doubleval($AccountInfo->startamount);
            $AccountTransaction = $this->M_fin_journal->GetDetailsByAccount($AccountID);
            
            foreach($AccountTransaction as $AccountTransaction_Row) {
                $Total_debit += doubleval($AccountTransaction_Row->debit);
                $Total_creditor += doubleval($AccountTransaction_Row->creditor);
            }
            ?>
        </td>
        <td><?php echo $Total_debit; ?></td>
        <td><?php echo $Total_creditor; ?></td>
        <td>
            <?php
            $CurrentBalance = $StartAmount + $Total_debit - $Total_creditor;
            echo $CurrentBalance;
            ?>
        </td>
        <td style="text-align:center">
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/schedule_data/".$MultiRows_Row->id;?>" class="btn btn-warning mr-2 btn-sm customer_schedule">
            <i class="flaticon-coins icon-sm"></i>
        </a>
        </td>
        <td style="text-align:center">
            <a href="<?php echo base_url().$Segment1."/sale_bill/pay/".$MultiRows_Row->id;?>">
            <?php echo $this->lang->line('Pay');?> 
            </a>
        </td>
        <td style="text-align:center">
            <a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$MultiRows_Row->id;?>">
                <i class="flaticon-edit-1 text-primary icon-lg"></i> 
            </a>
        </td>
        
        <td style="text-align:center">
        <button class="btn btn-primary mr-2 btn-sm view_detail" relid="<?php echo $MultiRows_Row->id;?>"><i class="fa fa-eye icon-sm"></i></button>
        </td>
    </tr>
    <?php
    }
    ?>
    </tbody>
</table>



<div id="show_modal" class="modal fade" role="dialog" style="background: #000;">
    <div class="modal-dialog" style="min-width: 600px;">
        <div class="modal-content">
        <div class="modal-header">
            <h3 style="font-size: 24px; color: #17919e; text-shadow: 1px 1px #ccc;">
            <i class="fa fa-folder"></i><?php echo lang('sale_customer');?>
            </h3>
        </div>
        <div class="modal-body">
        <div class="row">
            <div class="col-md-3"><?php echo lang('sale_fullname');?></div>
            <div class="col-md-3" id="customer_name"></div>
            <div class="col-md-3"><?php echo lang('phone');?></div>
            <div class="col-md-3" id="phone"></div>
        </div>
        <div class="row">
            <div class="col-md-3"><?php echo lang('mobile1');?></div>
            <div class="col-md-3" id="mobile1"></div>
            <div class="col-md-3"><?php echo lang('mobile2');?></div>
            <div class="col-md-3" id="mobile2"></div>
        </div>
        <div class="row">
            <div class="col-md-3"><?php echo lang('address');?></div>
            <div class="col-md-9" id="address"></div>
        </div>
        <div class="row">
            <div class="col-md-3"><?php echo lang('email');?></div>
            <div class="col-md-3" id="email"></div>
            <div class="col-md-3"><?php echo lang('commercial_register');?></div>
            <div class="col-md-3" id="commercial_register"></div>
        </div>
        <div class="row">
            <div class="col-md-3"><?php echo lang('tax_card');?></div>
            <div class="col-md-3" id="tax_card"></div>
            <div class="col-md-3"><?php echo lang('person');?></div>
            <div class="col-md-3" id="person"></div>
        </div>
        <div class="row">
            <div class="col-md-3"><?php echo lang('person_phone');?></div>
            <div class="col-md-3" id="person_phone"></div>
            <div class="col-md-3"><?php echo lang('credit_limit');?></div>
            <div class="col-md-3" id="credit_limit"></div>
        </div>
        
        </div>
        <div class="modal-footer noPrint">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo lang('Close');?></button>
        </div>
        </div>
    </div>
</div>