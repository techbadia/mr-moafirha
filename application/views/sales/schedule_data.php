<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
    <thead>
        <tr>
            <th>id</th>
            <th><?php echo $this->lang->line('sale_fullname');?></th>
            <th><?php echo $this->lang->line('thedate');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('payment_amount');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('buy_bill_paid');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('buy_bill_remaining');?></th>
            <th style="text-align:center"><?php echo $this->lang->line('Pay');?></th>
        </tr>
    </thead>
    <tbody>
    <?php
    foreach($MultiRows as $MultiRows_Row) {
        $AccountID = $MultiRows_Row->id;
    ?>
    <tr>
        <td><?php echo $MultiRows_Row->id;?></td>
        <td>
        <?php
            $customer_id = $MultiRows_Row->customer_id;
            $CustomersList = $this->M_sale_customer->GetRow($customer_id);
            $Title = "";
            if ($this->session->userdata('lang') == "ar")
            {
                $Title = $CustomersList->ar_title;
            }
            else
            {
                $Title = $CustomersList->en_title;
            }
        ?>
            <a href="<?php echo base_url();?>account/fin_treeaccount/view_account/<?php echo $customer_id;?>" target="_blank"><?php echo $Title;?></a>
        </td>
        <td><?php echo $MultiRows_Row->thedate;?></td>
        <td style="text-align:center"><?php echo $MultiRows_Row->amount;?></td>
        <td style="text-align:center"><?php echo $MultiRows_Row->paid;?></td>
        <td style="text-align:center"><?php echo $MultiRows_Row->remaining;?></td>
        
        <td style="text-align:center">
        <a href="<?php echo base_url().$Segment1."/".$Segment2."/schedule_paid/".$MultiRows_Row->id;?>" class="btn btn-warning mr-2 btn-sm customer_schedule">
            <i class="flaticon-coins icon-sm"></i>
        </a>
        </td>
    </tr>
    <?php
    }
    ?>
    </tbody>
</table>
