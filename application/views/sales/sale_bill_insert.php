<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">



    <?php

    $StoreList = $this->M_store->GetMultiRow();

    $output = '';

    foreach ($StoreList as $StoreList_Row) {

        $store_id = $StoreList_Row->id;

        $user_id = intval($this->session->userdata('StaffID'));

        $CheckUserStore = $this->M_usr_users_store->CheckStore($user_id, $store_id);

        if ($CheckUserStore > 0) {

            if ($this->session->userdata('lang') == "ar") {

                $FromStore = $StoreList_Row->title;

            } else {

                $FromStore = $StoreList_Row->title_en;

            }





            $parent_id = $StoreList_Row->parent_id;

            if ($parent_id > 0) {

                $StoreData = $this->M_store->GetRow($parent_id);

                if ($this->session->userdata('lang') == "ar") {

                    $MainStore = $StoreData->title . " : ";

                } else {

                    $MainStore = $StoreData->title_en . " : ";

                }

            } else {

                $MainStore = "";

            }

            $output .= "<option value=$StoreList_Row->id>" . $MainStore . $FromStore . "</option>";



        }

    }

    ?>



    <?php

    $FormPath = base_url() . $Segment1 . "/" . $Segment2 . "/insert_data";

    $attributes = array('id' => 'SendData', 'name' => 'SendData');

    echo form_open_multipart($FormPath, $attributes);

    ?>

    <div class="form-body">

        <div class="form-group row">

            <div class="col-lg-3 col-md-3 col-sm-12">

                <label class="col-form-label"><?php echo lang('sale_bill_customer_id'); ?></label>

                <select name="customer_id" id="customer_id" class="form-control kt-selectpicker"

                        onchange="GetCustomerBalance();" data-size="5" data-live-search="true" required>

                    <?php

                    $CustomersList = $this->M_sale_customer->GetMultiRow();

                    foreach ($CustomersList as $CustomersList_Row) {

                        if ($this->session->userdata('lang') == "ar") {

                            $CustomerTitle = $CustomersList_Row->ar_title;

                        } else {

                            $CustomerTitle = $CustomersList_Row->en_title;

                        }

                        $Phone = $CustomersList_Row->phone;

                        ?>

                        <option value="<?php echo $CustomersList_Row->id; ?>"><?php echo $CustomerTitle . " : " . $Phone; ?></option>

                        <?php

                        $customer_id = $CustomersList_Row->id;

                        $CustomerSubAccounts = $this->M_fin_treeaccount->GetSubAccounts($customer_id);

                        foreach ($CustomerSubAccounts as $CustomerSubAccounts_Row) {

                            ?>

                            <option value="<?php echo $CustomerSubAccounts_Row->id; ?>">

                                &nbsp;&nbsp;<?php echo $CustomerSubAccounts_Row->title; ?></option>

                            <?php

                        }

                        ?>

                    <?php } ?>

                </select>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-12">

                <label class="col-form-label"><?php echo lang('sale_bill_thedate'); ?></label>

                <input type="date" name="thedate" class="form-control" value="<?php echo date("Y-m-d"); ?>" required>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-12">

                <label class="col-form-label"><?php echo lang('Branches'); ?></label>

                <select name="company_id" class="form-control">

                    <?php

                    $MemberID = intval($this->session->userdata('StaffID'));

                    $CheckMember = $this->M_usr_users->GetRow($MemberID);

                    $branches_lookup = $CheckMember->branches_lookup;

                    $User_company_id = $CheckMember->company_id;

                    if ($branches_lookup == 1) {

                        $CompaniesList = $this->M_app_company->GetMultiRow();

                    } else {

                        $CompaniesList = $this->M_app_company->GetUserCompany($User_company_id);

                    }

                    foreach ($CompaniesList as $CompaniesList_Row) {

                        if ($this->session->userdata('lang') == "ar") {

                            $Title = $CompaniesList_Row->ar_title;

                        } else {

                            $Title = $CompaniesList_Row->en_title;

                        }

                        ?>

                        <option <?php if ($User_company_id == $CompaniesList_Row->id) { ?>selected<?php } ?>

                                value="<?php echo $CompaniesList_Row->id; ?>"><?php echo $Title; ?></option>

                        <?php

                    }

                    ?>

                </select>

            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">

                <label class="col-form-label"><?php echo lang('salesperson_id'); ?></label>

                <select name="salesperson_id" class="form-control" data-size="5" data-live-search="true" required>

                    <option value="0">-</option>

                    <?php

                    $acc_sales_commission = intval($this->session->userdata('acc_sales_commission'));

                    $AccountList = $this->M_fin_treeaccount->GetSubAccounts($acc_sales_commission);

                    foreach ($AccountList as $AccountList_Row) {

                        if ($this->session->userdata('lang') == "ar") {

                            $AccountTitle = $AccountList_Row->title;

                        } else {

                            $AccountTitle = $AccountList_Row->title_en;

                        }

                        ?>

                        <option value="<?php echo $AccountList_Row->id; ?>"><?php echo $AccountTitle; ?></option>

                        <?php

                    }

                    ?>

                </select>

            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
              <label class="col-form-label"><?php echo lang('sub_center_id');?></label>
              <select name="sub_center_id" class="form-control select2">
                <option  value=""><?php echo lang('Choose');?></option>

                <?php
                foreach($centers as $center) {
                  if ($this->session->userdata('lang') == "ar")
                  {
                    $ToTitle = $center->name_ar;
                  }
                  else
                  {
                    $ToTitle = $center->name_en;
                  }
                ?>
                  <option class="optionGroup" value="main_<?php echo $center->id;?>"><?php echo $ToTitle;?></option>
                  <?php foreach ($center->subs as $sub) {
                    if ($this->session->userdata('lang') == "ar")
                    {
                      $subTitle = $sub->name_ar;
                    }
                    else
                    {
                      $subTitle = $sub->name_en;
                    }
                   ?>
                <option class="optionChild" value="sub_<?php echo $sub->id;?>">-- <?php echo $subTitle;?></option>
                <?php } ?>
              <?php } ?>
              </select>
            </div>
        </div>

        <div class="form-group row">

            <?php

            $project_available = $this->session->userdata('project_available');

            if ($project_available == 0) {

                ?>

                <div class="col-lg-6 col-md-6 col-sm-12">

                    <label class="col-form-label"><?php echo lang('sale_bill_customer_name') . " (" . lang('sale_bill_Cash_sales') . ")"; ?></label>

                    <input type="text" name="customer_name" id="customer_name" class="form-control" value="">

                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">

                    <label class="col-form-label"><?php echo lang('sale_bill_customer_phone'); ?></label>

                    <input type="text" name="customer_phone" id="customer_phone" class="form-control" value="">

                </div>

                <?php

            } else {

                ?>

                <div class="col-lg-3 col-md-3 col-sm-12">

                    <label class="col-form-label"><?php echo lang('proj_project_title'); ?></label>

                    <select name="project_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true"

                            required>

                        <?php

                        $ProjectList = $this->M_proj_project->GetMultiRow();

                        foreach ($ProjectList as $ProjectList_Row) {

                            if ($this->session->userdata('lang') == "ar") {

                                $ProjectTitle = $ProjectList_Row->ar_title;

                            } else {

                                $ProjectTitle = $ProjectList_Row->en_title;

                            }

                            ?>

                            <option value="<?php echo $ProjectList_Row->id; ?>"><?php echo $ProjectTitle; ?></option>

                            <?php

                        }

                        ?>

                    </select>

                </div>

                <div class="col-lg-5 col-md-5 col-sm-12">

                    <label class="col-form-label"><?php echo lang('sale_bill_customer_name') . " (" . lang('sale_bill_Cash_sales') . ")"; ?></label>

                    <input type="text" name="customer_name" id="customer_name" class="form-control" value="">

                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">

                    <label class="col-form-label"><?php echo lang('sale_bill_customer_phone'); ?></label>

                    <input type="text" name="customer_phone" id="customer_phone" class="form-control" value="">

                </div>

            <?php } ?>

        </div>

        <div class="form-group row ">

            <div class="col-lg-1 col-md-1 col-sm-12 p-3 mb-2 bg-primary text-white">

                <label class="col-form-label" style="color:#FFFFFF"><?php echo lang('product_barcode'); ?></label>

            </div>

            <div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">

                <label class="col-form-label"

                       style="color:#FFFFFF"><?php echo lang('sale_bill_items_store_type_id'); ?></label>

            </div>

            <div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">

                <label class="col-form-label"

                       style="color:#FFFFFF"><?php echo lang('sale_bill_items_location_id'); ?></label>

            </div>

            <div class="col-lg-1 col-md-1 col-sm-12 p-3 mb-2 bg-primary text-white">

                <label class="col-form-label"

                       style="color:#FFFFFF"><?php echo lang('sale_bill_items_available'); ?></label>

            </div>

            <div class="col-lg-1 col-md-1 col-sm-12 p-3 mb-2 bg-primary text-white">

                <label class="col-form-label"

                       style="color:#FFFFFF"><?php echo lang('sale_bill_items_quantity'); ?></label>

            </div>

            <div class="col-lg-1 col-md-1 col-sm-12 p-3 mb-2 bg-primary text-white">

                <label class="col-form-label" style="color:#FFFFFF"><?php echo lang('received'); ?></label>

            </div>

            <div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">

                <label class="col-form-label" style="color:#FFFFFF"><?php echo lang('sale_bill_items_price'); ?></label>

            </div>

            <div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">

                <label class="col-form-label" style="color:#FFFFFF"><?php echo lang('sale_bill_items_total'); ?></label>

            </div>



            <?php

            $InvoiceItems = intval($this->session->userdata('acc_invoice_items'));

            $BackgroundColor = "p-3 mb-2 bg-success-o-40";

            for ($i = 1; $i <= $InvoiceItems; $i++) {

                if ($BackgroundColor == "p-3 mb-2 bg-success-o-40") {

                    $BackgroundColor = "p-3 mb-2 bg-success-o-20";

                } else {

                    $BackgroundColor = "p-3 mb-2 bg-success-o-40";

                }

                ?>



                <div class="col-lg-1 col-md-1 col-sm-12 <?php echo $BackgroundColor; ?>">

                    <input type="text" name="barcode<?php echo $i; ?>" id="barcode<?php echo $i; ?>"

                           onfocus="GetName(<?php echo $i; ?>); GetSalePrice(<?php echo $i; ?>);  GetSaleMinPrice(<?php echo $i; ?>);  GetStores(<?php echo $i; ?>)"

                           onblur="GetName(<?php echo $i; ?>); GetSalePrice(<?php echo $i; ?>);  GetSaleMinPrice(<?php echo $i; ?>);  GetStores(<?php echo $i; ?>)"

                           class="form-control" value="">

                </div>

                <div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor; ?>">

                    <input type="text" name="title<?php echo $i; ?>" id="title<?php echo $i; ?>" class="form-control"

                           value="">

                </div>

                <div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor; ?>">

                    <select name="location_id<?php echo $i; ?>" id="location_id<?php echo $i; ?>"

                            onchange="GetStores(<?php echo $i; ?>);" class="form-control">

                        <?php

                        $StoreList = $this->M_store->GetMultiRow();

                        foreach ($StoreList as $StoreList_Row) {




                                if ($this->session->userdata('lang') == "ar") {

                                    $FromStore = $StoreList_Row->title;

                                } else {

                                    $FromStore = $StoreList_Row->title_en;

                                }

                                $parent_id = $StoreList_Row->parent_id;

                                if ($parent_id > 0) {

                                    $StoreData = $this->M_store->GetRow($parent_id);

                                    if ($this->session->userdata('lang') == "ar") {

                                        $MainStore = $StoreData->title . " : ";

                                    } else {

                                        $MainStore = $StoreData->title_en . " : ";

                                    }

                                } else {

                                    $MainStore = "";

                                }

                                ?>

                                <option value="<?php echo $StoreList_Row->id; ?>"><?php echo $MainStore . $FromStore; ?></option>

                                <?php



                        }

                        ?>

                    </select>

                </div>

                <div class="col-lg-1 col-md-1 col-sm-12 <?php echo $BackgroundColor; ?>">

                    <input type="text" name="available<?php echo $i; ?>" id="available<?php echo $i; ?>"

                           class="form-control" style="background-color:#F5F5F9" readonly>

                </div>

                <div class="col-lg-1 col-md-1 col-sm-12 <?php echo $BackgroundColor; ?>">

                    <input type="number" min="0" step="1" name="quantity<?php echo $i; ?>"

                           id="quantity<?php echo $i; ?>" onchange="SubValue();  Remaing(); AllTotal();"

                           onblur="SubValue();  Remaing(); AllTotal();" onkeyup="SubValue();  Remaing(); AllTotal();"

                           class="form-control" value="0">

                </div>

                <div class="col-lg-1 col-md-1 col-sm-12 <?php echo $BackgroundColor; ?>">

                    <input type="number" min="0" step="1" name="received<?php echo $i; ?>"

                           id="received<?php echo $i; ?>" class="form-control" value="0">

                </div>

                <div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor; ?>">

                    <input type="number" min="0" step="0.001" name="unitprice<?php echo $i; ?>"

                           id="unitprice<?php echo $i; ?>" onchange="SubValue();  Remaing(); AllTotal();"

                           onblur="SubValue();  Remaing(); AllTotal();" onkeyup="SubValue();  Remaing(); AllTotal();"

                           class="form-control" value="0">

                    <!--<input type="hidden" name="subtotal<?php //echo $i;?>" id="subtotal<?php //echo $i;?>">-->

                </div>

                <div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor; ?>">

                    <input type="text" name="subtotal<?php echo $i; ?>" id="subtotal<?php echo $i; ?>"

                           class="form-control" readonly>

                </div>

                <?php

            }

            ?>

        </div>



        <div class="append-row">

        </div>



        <span class="svg-icon svg-icon-md add" style="float:right; margin-top:-5px;" id="submitbtn">

							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"

                                 width="24px" height="24px" viewBox="0 0 24 24" version="1.1">

								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">

									<rect x="0" y="0" width="24" height="24"/>

									<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>

									<path d="M11,11 L11,7 C11,6.44771525 11.4477153,6 12,6 C12.5522847,6 13,6.44771525 13,7 L13,11 L17,11 C17.5522847,11 18,11.4477153 18,12 C18,12.5522847 17.5522847,13 17,13 L13,13 L13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 L11,13 L7,13 C6.44771525,13 6,12.5522847 6,12 C6,11.4477153 6.44771525,11 7,11 L11,11 Z"

                                          fill="#000000"/>

								</g>

							</svg>

						</span>



        <div class="form-group row">

            <div class="col-lg-12">

                <!--begin::Card-->

                <div class="card card-custom">

                    <div class="card-header ribbon ribbon-clip ribbon-right">

                        <div class="ribbon-target" style="top: 12px;">

                            <span class="ribbon-inner bg-primary"></span><?php echo lang('Payment_options'); ?></div>

                        <h3 class="card-title"><?php echo lang('Payment_options_details'); ?></h3>

                    </div>

                    <div class="card-body">

                        <div class="form-group row">



                            <div class="col-lg-2 col-md-4 col-sm-12 p-3 mb-2 bg-warning-o-40">

                                <label class="col-form-label"><?php echo lang('sale_bill_paid'); ?></label>

                                <input type="number" min="0" step="0.001" name="paid1" id="paid1" onchange="Remaing();"

                                       onblur="Remaing(); AllTotal();" onkeyup="Remaing(); AllTotal();"

                                       class="form-control" value="0" required>

                            </div>

                            <div class="col-lg-2 col-md-4 col-sm-12 p-3 mb-2 bg-warning-o-40">

                                <label class="col-form-label"><?php echo lang('sale_bill_treasury'); ?></label>

                                <select name="tree_id" class="form-control" required>

                                    <?php

                                    //$MemberID

                                    $acc_treasury = intval($this->session->userdata('acc_treasury'));

                                    $toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_treasury);

                                    foreach ($toaccount as $toaccount_Row) {

                                        if ($this->session->userdata('lang') == "ar") {

                                            $ToTitle = $toaccount_Row->title;

                                        } else {

                                            $ToTitle = $toaccount_Row->title_en;

                                        }



                                        $user_id = intval($this->session->userdata('StaffID'));

                                        $account_id = $toaccount_Row->id;

                                        $CheckAccount = $this->M_usr_users_account->CheckAccount($user_id, $account_id);

                                        //	if($CheckAccount > 0) {

                                        ?>

                                        <option value="<?php echo $toaccount_Row->id; ?>"><?php echo $ToTitle; ?></option>

                                    <?php } ?>

                                </select>

                            </div>



                            <div class="col-lg-2 col-md-4 col-sm-12 p-3 mb-2 bg-warning-o-60">

                                <label class="col-form-label"><?php echo lang('sale_bill_paid'); ?></label>

                                <input type="number" min="0" step="0.001" name="paid2" id="paid2" onchange="Remaing();"

                                       onblur="Remaing(); AllTotal();" onkeyup="Remaing(); AllTotal();"

                                       class="form-control" value="0" required>

                            </div>

                            <div class="col-lg-2 col-md-4 col-sm-12 p-3 mb-2 bg-warning-o-60">

                                <label class="col-form-label"><?php echo lang('sale_bill_bank'); ?></label>

                                <select name="tree_id1" class="form-control" required>

                                    <?php

                                    $acc_bank = intval($this->session->userdata('acc_bank'));

                                    $toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_bank);

                                    foreach ($toaccount as $toaccount_Row) {

                                        if ($this->session->userdata('lang') == "ar") {

                                            $ToTitle = $toaccount_Row->title;

                                        } else {

                                            $ToTitle = $toaccount_Row->title_en;

                                        }



                                        $user_id = intval($this->session->userdata('StaffID'));

                                        $account_id = $toaccount_Row->id;

                                        $CheckAccount = $this->M_usr_users_account->CheckAccount($user_id, $account_id);

                                        //if($CheckAccount > 0) {

                                        ?>

                                        <option value="<?php echo $toaccount_Row->id; ?>"><?php echo $ToTitle; ?></option>

                                    <?php } ?>

                                </select>

                            </div>

                            <div class="col-lg-2 col-md-4 col-sm-12 p-3 mb-2 bg-warning-o-40">

                                <label class="col-form-label"><?php echo lang('sale_bill_paid'); ?></label>

                                <input type="number" min="0" step="0.001" name="paid3" id="paid3"

                                       onchange="Remaing(); AllTotal();" onblur="Remaing(); AllTotal();"

                                       onkeyup="Remaing(); AllTotal();" class="form-control" value="0" required>

                            </div>

                            <div class="col-lg-2 col-md-4 col-sm-12 p-3 mb-2 bg-warning-o-40">

                                <label class="col-form-label"><?php echo lang('sale_bill_customer_money'); ?></label>

                                <input type="number" min="0" step="0.001" name="customer_money" id="customer_money"

                                       class="form-control" value="0" readonly required>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div class="form-group row">

            <div class="col-lg-12 col-md-12 col-sm-12">

                <label class="col-form-label"><?php echo lang('sale_bill_image'); ?></label>

                <input type="file" name="image" class="form-control" dir="ltr">

            </div>

            <div class="col-lg-8 col-md-12 col-sm-12">

                <label class="col-form-label"><?php echo lang('sale_bill_notes'); ?></label>

                <input type="text" name="notes" class="form-control" value="">

            </div>

            <div class="col-lg-4 col-md-12 col-sm-12">

                <label class="col-form-label"><?php echo lang('SendMessageToStore'); ?></label>

                <select name="storekeeper_id" id="storekeeper" class="form-control">

                    <option value="0">-</option>

                    <?php

                    $UsersList = $this->M_usr_users->GetMultiRow();



                    foreach ($UsersList as $UsersList_Row) {

                        ?>

                        <option value="<?php echo $UsersList_Row->id; ?>"><?php echo $UsersList_Row->fullname; ?></option>

                    <?php } ?>

                </select>

            </div>



            <div class="col-lg-2 col-md-4 col-sm-12">

                <div class="form-group">

                    <label class="col-form-label"><?php echo lang('sale_bill_totalitemsvalue'); ?></label>

                    <input type="number" min="0" step="0.001" id="totalitemsvalue" class="form-control" value="0"

                           readonly required>

                </div>

            </div>



            <div class="col-lg-2 col-md-4 col-sm-12">

                <div class="form-group">

                    <label class="col-form-label"><?php echo lang('sale_bill_discount'); ?></label>

                    <input type="number" min="0" step="0.001" name="discount" id="discount" class="form-control"

                           onchange="Remaing(); AllTotal();" onblur="Remaing(); AllTotal();"

                           onkeyup="Remaing(); AllTotal();" value="0" required>

                </div>

            </div>



            <div class="col-lg-2 col-md-4 col-sm-12">

                <div class="form-group">

                    <label class="col-form-label"><?php echo lang('sale_bill_total_after_discount'); ?></label>

                    <input type="number" min="0" step="0.001" id="sale_bill_total_after_discount" class="form-control"

                           value="0" readonly>

                </div>

            </div>



            <div class="col-lg-2 col-md-4 col-sm-12">

                <div class="form-group">

                    <label class="col-form-label"><?php echo lang('buy_offer_tax'); ?></label>

                    <div class="input-group">

                        <?php

                        $acc_tax_sale_percent = doubleval($this->session->userdata('acc_tax_sale_percent'));

                        ?>

                        <input type="number" min="0" name="tax" id="tax" class="form-control"

                               onchange="Remaing(); AllTotal();" onblur="Remaing(); AllTotal();"

                               onkeyup="Remaing(); AllTotal();" value="<?php echo $acc_tax_sale_percent; ?>" required>

                        <div class="input-group-prepend">

                            <span class="input-group-text">%</span>

                        </div>

                    </div>

                </div>

            </div>



            <div class="col-lg-2 col-md-4 col-sm-12">

                <div class="form-group">

                    <label class="col-form-label"><?php echo lang('sale_bill_required_price'); ?></label>

                    <input type="number" min="0" step="0.001" id="sale_bill_required_price" class="form-control"

                           readonly>

                </div>

            </div>



            <div class="col-lg-2 col-md-4 col-sm-12">

                <label class="col-form-label"><?php echo lang('sale_bill_totalvalue'); ?></label>

                <input type="number" min="0" step="0.001" name="totalvalue" id="totalvalue" class="form-control"

                       value="" readonly required>

            </div>



            <div class="col-lg-2 col-md-4 col-sm-12">

                <div class="form-group">

                    <label class="col-form-label"><?php echo lang('sale_bill_total_paid'); ?></label>

                    <!--<input type="number" min="0" step="0.001" name="paid" id="paid" onchange="Remaing();"-->

                    <!--       onblur="Remaing(); AllTotal();" onkeyup="Remaing(); AllTotal();" class="form-control"-->

                    <!--       value="0" readonly required>-->


                    <input type="number" min="0" step="0.001" name="paid" id="paid" onchange="Remaing();"

                           onblur="Remaing(); " onkeyup="Remaing(); " class="form-control"

                           value="0">

                </div>

            </div>



            <div class="col-lg-2 col-md-4 col-sm-12">

                <div class="form-group">

                    <label class="col-form-label"><?php //echo lang('sale_bill_remaining'); ?> المبلغ النقدي</label>

                    <input type="number" min="0" step="0.001" name="cash" id="cash" class="form-control"
                           value="0" required   >

                </div>

            </div>


            <div class="col-lg-2 col-md-4 col-sm-12">

                <div class="form-group">

                    <label class="col-form-label"><?php echo lang('sale_bill_remaining'); ?></label>

                    <input type="number" min="0" step="0.001" name="remaining" id="remaining" class="form-control"

                            onkeyup="Remaing(); AllTotal();"
                           value="" required>

                </div>

            </div>

        </div>

    </div>

    <div class="kt-portlet__foot">

        <div class="kt-form__actions">

            <div class="row">

                <div class="col-lg-12 ml-lg-auto">

                    <input type="hidden" name="id" value="<?php //echo $id;?>">

                    <input type="hidden" name="deleted" value="0">

                    <?php

                    if ($project_available == 0) {

                        ?>

                        <input type="hidden" name="project_id" value="0">

                    <?php } ?>

                    <input type="hidden" name="RowsCount" id="RowsCount" value="<?php echo $InvoiceItems; ?>">

                    <a href="<?php echo base_url() . $Segment1 . "/" . $Segment2; ?>" class="btn btn-secondary"

                       accesskey="b"><?php echo $this->lang->line('Back'); ?></a>

                    <button type="submit" class="btn btn-primary mr-2"

                            accesskey="s"><?php echo $this->lang->line('Save'); ?></button>

                </div>

            </div>

        </div>

    </div>

    <?php

    echo form_close();

    ?>



    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>





    <script>



        $(document).ready(function () {

            var count = <?php echo $InvoiceItems?>;



            $(document).on('click', '#submitbtn', function () {

                var SelectBox = "<?= $output; ?>";

                var BackgroundColor = "p-3 mb-2 bg-success-o-40";



                if (BackgroundColor == "p-3 mb-2 bg-success-o-40") {

                    BackgroundColor = "p-3 mb-2 bg-success-o-20";

                } else {

                    BackgroundColor = "p-3 mb-2 bg-success-o-40";

                }



                count++;

                var html = '';

                html += '<div class="form-group row">';

                html += '<div class="col-lg-1 col-md-1 col-sm-12 ' + BackgroundColor + '">';

                html += '<input type="text" name="barcode' + count + '" id="barcode' + count + '" class="form-control" value="" onfocus="GetName(' + count + '); GetSalePrice(' + count + ');  GetSaleMinPrice(' + count + ');  GetStores(' + count + ')" onblur="GetName(' + count + '); GetSalePrice(' + count + ');  GetSaleMinPrice(' + count + ');  GetStores(' + count + ')" >';

                html += '</div>';



                html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';

                html += '<input type="text" name="title' + count + '" id="title' + count + '" class="form-control" value="">';

                html += '</div>';



                html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';

                html += '<select name="location_id' + count + '" id="location_id' + count + '" class="form-control" onchange="GetStores(' + count + ');">"' + SelectBox + '"</select>';

                html += '</div>';



                html += '<div class="col-lg-1 col-md-1 col-sm-12 ' + BackgroundColor + '">';

                html += '<input type="text" step="0.001" min="0" name="available' + count + '" id="available' + count + '" class="form-control" style="background-color:#F5F5F9" readonly>';

                html += '</div>';





                html += '<div class="col-lg-1 col-md-1 col-sm-12 ' + BackgroundColor + '">';

                html += '<input type="number" step="1" min="0" name="quantity' + count + '" id="quantity' + count + '" class="form-control" value="0" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();">';

                html += '</div>';



                html += '<div class="col-lg-1 col-md-1 col-sm-12 ' + BackgroundColor + '">';

                html += '<input type="number" step="1" min="0" name="received' + count + '" id="received' + count + '" class="form-control" value="0">';

                html += '</div>';



                html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';

                html += '<input type="number" step="1" min="0" name="unitprice' + count + '" id="unitprice' + count + '" class="form-control" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();" value="0">';

                html += '</div>';





                html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';

                html += '<input type="text" name="subtotal' + count + '" id="subtotal' + count + '" class="form-control" value="0">';

                html += '</div>';

                html += '</div>';



                $('#RowsCount').val(count);

                $('.append-row').append(html);

            });



            $(document).on('click', '.remove', function () {

                $(this).closest('tr').remove();

            });



        });

    </script>

</div>
