<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta charset="utf-8"/>
<title><?php echo $this->lang->line('SCI');?></title>
<style type='text/css'>
table {
    border-collapse: collapse;
}
table, th, td {
    border: 1px solid black;
    padding: 2px !important;
}
th {
    font: bold 16px 'Times New Roman', Times, serif;
    color: #000000 !important;
    text-align: right !important;
    background: #F5FAFA !important;
    background-color: #FFF !important;
    border-top-width: 1px !important;
    border-right-width: 1px !important;
    border-bottom-width: 1px !important;
    border-left-width: 1px !important;
    border-top-style: none !important;
    border-right-style: solid !important;
    border-bottom-style: solid !important;
    border-left-style: solid !important;
    border-top-color: #000 !important;
    border-right-color: #000 !important;
    border-bottom-color: #000 !important;
    border-left-color: #000 !important;
    padding: 5px !important;
}
hr {
    background-color: #000 !important;
    color: #000 !important;
}
td {
    color: #333333 !important;
    padding: 5px !important;
    background-color: #FFF !important;
    border-top-width: 1px !important;
    border-right-width: 1px !important;
    border-bottom-width: 1px !important;
    border-left-width: 1px !important;
    border-top-style: none !important;
    border-right-style: solid !important;
    border-bottom-style: solid !important;
    border-left-style: solid !important;
    border-top-color: #000 !important;
    border-right-color: #000 !important;
    border-bottom-color: #000 !important;
    border-left-color: #000 !important;
}

.td_without {
    color: #333333 !important;
    padding: 5px !important;
    background-color: #FFF !important;
    border:none !important;
}
td.alt {
    background: #F5FAFA !important;
    color: #B4AA9D !important;
}
table {
    border:solid #000 !important;
    border-width:1px 0 0 1px !important;
}
th, td {
    border:solid #000 !important;
    border-width:0 1px 1px 0 !important;
}



@media print {
.noPrint {
    display:none;
}
}
</style>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?php echo base_url();?>assets/global/css/components-md-rtl.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url();?>assets/global/css/plugins-md-rtl.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?php echo base_url();?>assets/layouts/layout/css/layout-rtl.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/themes/light-rtl.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?php echo base_url();?>assets/layouts/layout/css/custom-rtl.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->

<script src="<?php echo base_url();?>Highcharts/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>Highcharts/highcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>Highcharts/jquery.highchartTable.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>Highcharts/js/modules/exporting.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('table.highchart').highchartTable();
});
</script>

<script type="text/javascript">

function printSelection(node){

  var content=node.innerHTML
  var pwin=window.open('','print_content','width=100,height=100');

  pwin.document.open();
  pwin.document.write('<html><body onload="window.print()">'+content+'</body></html>');
  pwin.document.close();
 
  setTimeout(function(){pwin.close();},1000);

}
</script>
</head>
<body>
<br><br>
<div id="PrintArea"><center>
<center><strong style="color:#FFFFFF"><?php echo $this->lang->line('SCI');?> : <?php echo date('Y-m-d');?></strong></center>
<br><br>
<table class="table table-striped table-bordered table-hover" id="sample_1" cellspacing="0" cellpadding="2" border="1" width="96%" dir="rtl" align="center">
<thead class="flip-content">
<tr>
    <th><?php echo $this->lang->line('sale_fullname');?></th>
    <th><?php echo $this->lang->line('address');?></th>
    <th><?php echo $this->lang->line('phone');?></th>
    <th><?php echo $this->lang->line('Balance');?></th>
    <th><?php echo $this->lang->line('LPA');?></th>
    <th><?php echo $this->lang->line('thedate');?></th>
</tr>
</thead>
<tbody>
<?php
$Serial = 0;
$FinalTotalValue = 0;
foreach($MultiRows as $MultiRows_Row) {
	$StartAmount = 0;
	$Balance = 0;
	$CustomerID = $MultiRows_Row->id;
	$CustomerAccount = $this->M_fin_treeaccount->GetRow($CustomerID);
	$StartAmount = $CustomerAccount->startamount;
	$Balance += $StartAmount;
	$AllJournal = $this->M_fin_journal->GetRealtedJournal($CustomerID, $fromdate, $todate);
	$FromJournal = 0;
	$ToJournal = 0;
	foreach($AllJournal as $AllJournal_Row) {
		$FromJournal = $AllJournal_Row->fromaccount;
		$ToJournal = $AllJournal_Row->toaccount;
		$JoruanlValue = $AllJournal_Row->thevalue;
		if ($FromJournal == $CustomerID)
		{
			$Balance += $JoruanlValue;
		}
		else
			{
				$Balance -= $JoruanlValue;
			}
	}
	if ($Balance >0) {
		$FinalTotalValue += $Balance;
		$Serial += 1;
?>
<tr style="background-color:#EBEBEB; font-weight:bold;">
	<?php
	$LatestPaymentAmountRows = $this->M_fin_journal->GetLatestPaymentAmountRows($CustomerID);
	$Style = "";
	$TodayDate = date("Y-m-d");
	
	$AlarmDays = intval($this->session->userdata('moneyalarmday'));
	$AlarmDaysText = "-".$AlarmDays." days";
	$BeforeDate = date('Y-m-d', strtotime($AlarmDaysText));
	
	$dStart = new DateTime($BeforeDate);
	$dEnd  = new DateTime($TodayDate);
	
	$dDiff = $dStart->diff($dEnd);
	$Days = $dDiff->days;
	
	if ($LatestPaymentAmountRows >0)
	{
		if ($Days >= $AlarmDays)
		{
			$Style = " style='color: #FF0000 !important;'";
		}
		else
			{
				$Style = "";
			}
	}
	else if ($LatestPaymentAmountRows ==0)
	{
		if ($Days >= $AlarmDays)
		{
			$Style = " style='color: #FF0000 !important;'";
		}
		else
			{
				$Style = "";
			}
	}
	?>
	<td<?php echo $Style;?>><?php echo $MultiRows_Row->ar_title;?></td>
	<td<?php echo $Style;?>><?php echo $MultiRows_Row->address;?></td>
	<td<?php echo $Style;?>><?php echo $MultiRows_Row->phone;?></td>
	<td<?php echo $Style;?>><?php echo $Balance;?></td>
	<td<?php echo $Style;?>>
		<?php
		if ($LatestPaymentAmountRows >0)
		{
			$LatestPaymentAmount = $this->M_fin_journal->GetLatestPaymentAmount($CustomerID);
			echo $LatestPaymentAmount->thevalue;
		}
		?>
	</td>
	<td<?php echo $Style;?>>
		<?php
		if ($LatestPaymentAmountRows >0)
		{
			echo $LatestPaymentAmount->thedate;
		}
		?>
	</td>
</tr>
<?php
	}
}
?>
</tbody>
</table>
<br>
<strong style="color:#FFFFFF"><?php echo "إجمالي عدد العملاء ="." ".$Serial;?></strong>
<br>
<strong style="color:#FFFFFF"><?php echo "إجمالي مبلغ المديونيات ="." ".$FinalTotalValue;?></strong>

<br />
</center></div>
<table class="highchart" data-graph-container-before="1" data-graph-type="column" style="visibility: hidden">
	<thead>
		<tr>
		    <th><?php echo $this->lang->line('sale_fullname');?></th>
		    <th><?php echo $this->lang->line('Balance');?></th>
		</tr>
	</thead>
	<tbody>
	    <?php
	    foreach($MultiRows as $MultiRows_Row) {
		$StartAmount = 0;
		$Balance = 0;
		$CustomerID = $MultiRows_Row->id;
		$CustomerAccount = $this->M_fin_treeaccount->GetRow($CustomerID);
		$StartAmount = $CustomerAccount->startamount;
		$Balance += $StartAmount;
		$AllJournal = $this->M_fin_journal->GetRealtedJournal($CustomerID, $fromdate, $todate);
		$FromJournal = 0;
		$ToJournal = 0;
		foreach($AllJournal as $AllJournal_Row) {
			$FromJournal = $AllJournal_Row->fromaccount;
			$ToJournal = $AllJournal_Row->toaccount;
			$JoruanlValue = $AllJournal_Row->thevalue;
			if ($FromJournal == $CustomerID)
			{
				$Balance += $JoruanlValue;
			}
			else
				{
					$Balance -= $JoruanlValue;
				}
		}
		if ($Balance >0) {
	    ?>
	    <tr>
		    <td><?php echo $MultiRows_Row->ar_title;?></td>
		    <td><?php echo $Balance;?></td>
	    </tr>
	    <?php
		}
	    }
	    ?> 
	</tbody>
</table>


<center>
  <a href="" style="color:#FFFFFF; font-size: 18px;" class="noPrint" onClick="printSelection(document.getElementById('PrintArea'));return false"><?php echo $this->lang->line('Print');?></a>
  <br /><br /><br />
</center>
<br /><br />
<!-- <script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script> -->
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script charset="utf8" src="<?php echo base_url();?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script charset="utf8" src="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script charset="utf8" src="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script charset="utf8" src="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url();?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script charset="utf8" src="<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo base_url();?>assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script>

</script>
</body>

<!-- END BODY -->

<!-- Mirrored from www.keenthemes.com/preview/metronic/theme_rtl/templates/admin/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Jun 2015 00:11:15 GMT -->
</html>