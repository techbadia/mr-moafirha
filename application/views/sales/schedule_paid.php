
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
$attributes = array('id' => 'kt_form_2', 'name' => 'kt_form_2');
	$FormPath = base_url().$Segment1."/".$Segment2."/schedule_paid_insert";
	echo form_open_multipart($FormPath, $attributes);
?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-4 col-md-4 col-sm-12 bg-primary text-white">
				<label class="col-form-label"><?php echo lang('buy_bill_paid');?></label>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 bg-primary text-white">
				<label class="col-form-label"><?php echo lang('sale_fullname');?></label>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 bg-primary text-white">
				<label class="col-form-label"><?php echo lang('sale_bill_tree_id');?></label>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-lg-4 col-md-4 col-sm-12 p-3 mb-2 bg-success-o-40">
				<input type="number" step="0.001" min="0" max="<?php echo $amount?>" name="paid" id="paid" class="form-control" value="<?php echo $amount?>">
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 p-3 mb-2 bg-success-o-40">
				<select name="toaccount" class="form-control kt-selectpicker" data-size="5" data-live-search="true">
					<?php
					$AccountList = $this->M_fin_treeaccount->GetMultiRow();
					$AccountID = 0;
					foreach($AccountList as $AccountList_Row) {
						$AccountID = $AccountList_Row->id;
						if($customer_id == $AccountID) {
						if ($this->session->userdata('lang') == "ar")
						{
							$AccountTitle = $AccountList_Row->title;
						}
						else
						{
							$AccountTitle = $AccountList_Row->title_en;
						}
					?>
					<option value="<?php echo $AccountList_Row->id;?>"><?php echo $AccountTitle;?></option>
					<?php
						}
					}
					?>
				</select>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 p-3 mb-2 bg-success-o-40">
				<select name="fromaccount" class="form-control kt-selectpicker" data-size="5" data-live-search="true">
				<?php
				//$MemberID
				$acc_treasury = intval($this->session->userdata('acc_treasury'));
				$toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_treasury);
				foreach($toaccount as $toaccount_Row) {
					if ($this->session->userdata('lang') == "ar")
					{
						$ToTitle = $toaccount_Row->title;
					}
					else
					{
						$ToTitle = $toaccount_Row->title_en;
					}
					
					$user_id = intval($this->session->userdata('StaffID'));
					$account_id = $toaccount_Row->id;
					$CheckAccount = $this->M_usr_users_account->CheckAccount($user_id, $account_id);
					if($CheckAccount > 0) {
				?>
				<option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
				<?php } } ?>
				<?php
				$acc_bank = intval($this->session->userdata('acc_bank'));
				$toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_bank);
				foreach($toaccount as $toaccount_Row) {
					if ($this->session->userdata('lang') == "ar")
					{
						$ToTitle = $toaccount_Row->title;
					}
					else
					{
						$ToTitle = $toaccount_Row->title_en;
					}
					
					$user_id = intval($this->session->userdata('StaffID'));
					$account_id = $toaccount_Row->id;
					$CheckAccount = $this->M_usr_users_account->CheckAccount($user_id, $account_id);
					if($CheckAccount > 0) {
				?>
				<option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
				<?php } } ?>
				</select>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<hr>
			</div>
			
		</div>
		
		<div class="form-group row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<label class="col-form-label"><?php echo lang('fin_journal_details');?></label>
				<input type="text" name="details" id="details" class="form-control" value="" required>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12">
				<label class="col-form-label"><?php echo lang('fin_journal_thedate');?></label>
				<input type="date" name="thedate" class="form-control" value="<?php echo date('Y-m-d');?>" required>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-12">
				<label class="col-form-label"><?php echo lang('fin_journal_image');?></label>
				<input type="file" name="image" id="image" class="form-control" dir="ltr" required>
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php echo $id;?>">
					<input type="hidden" name="amount" value="<?php echo $amount;?>">
					<input type="hidden" name="remaining" value="<?php echo $remaining;?>">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s" onclick="return Check_Debit_Creditor()"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>

</div>