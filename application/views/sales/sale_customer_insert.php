                <div class="card card-custom gutter-b example example-compact">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
                        echo form_open_multipart($FormPath);
                    ?>
                        <div class="form-body">
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('ar_title');?></label>
                                        <input type="text" id="ar_title" name="ar_title" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('en_title');?></label>
                                        <input type="text" id="en_title" name="en_title" class="form-control" required>
                                    </div>
                                </div>
                                <!-- <div class="form-group col-md-4"> -->
                                    <?php
                                    $CustomersOweID = intval($this->session->userdata('acc_customer'));
                                    $AccountCount = $this->M_fin_treeaccount->GetSubAccountsCount($CustomersOweID);
                                    $AccountCount += 1;
                                    ?>
                                    <!-- <label><?php echo $this->lang->line('BankNumber');?></label> -->
                                    <input type="hidden" name="account_no" class="form-control" value="<?php echo $CustomersOweID.$AccountCount;?>" required>
                                <!-- </div> -->
                            </div>
                            
                            <div class="row">
                                <div class="col-md-4">
                                   <div class="form-group">
                                        <label><?php echo $this->lang->line('phone');?></label>
                                        <input type="text" id="phone" name="phone" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                     <div class="form-group">
                                        <label><?php echo $this->lang->line('mobile1');?></label>
                                        <input type="text" id="mobile1" name="mobile1" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('mobile2');?></label>
                                        <input type="text" id="mobile2" name="mobile2" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('address');?></label>
                                        <input type="text" id="address" name="address" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('email');?></label>
                                        <input type="text" id="email" name="email" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('startamount');?></label>
                                        <input type="number" min="0" step="0.001" id="thevalue" name="thevalue" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('credit_limit');?></label>
                                        <input type="number" min="0" step="1" id="credit_limit" name="credit_limit" value="0" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('active');?></label>
                                        <select id="active" name="active" class="form-control" required>
                                        	<option value="True" selected><?php echo $this->lang->line('active');?></option>
                                        	<option value="False"><?php echo $this->lang->line('Notactive');?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('commercial_register');?></label>
                                        <input type="text" name="commercial_register" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('tax_card');?></label>
                                        <input type="text" name="tax_card" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('person');?></label>
                                        <input type="text" name="person" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><?php echo $this->lang->line('person_phone');?></label>
                                        <input type="text" name="person_phone" class="form-control">
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-actions right" style="text-align: right; font-weight: bold !important; font-size: 18px;">
                            <input type="hidden" name="deleted" value="0">
                            <a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
                        </div>
                        <?php
                        echo form_close();
                    ?>
                </div>