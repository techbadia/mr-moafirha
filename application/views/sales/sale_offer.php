									<br>
										<div class="d-flex align-items-center p-4 bg-dark" style="text-align:center !important">
											<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-transparent-success font-weight-bold mr-2">
												<?php echo lang('buy_offer_date_all');?>
											</a>
											<a href="<?php echo base_url().$Segment1."/".$Segment2."/date_not_expired/";?>" class="btn btn-transparent-primary font-weight-bold mr-2">
												<?php echo lang('buy_offer_date_not_expired');?>
											</a>
											<a href="<?php echo base_url().$Segment1."/".$Segment2."/date_expired/";?>" class="btn btn-transparent-danger font-weight-bold mr-2">
												<?php echo lang('buy_offer_date_expired');?>
											</a>
											<!--
											<a href="#" class="btn btn-transparent-warning font-weight-bold mr-2">Warning</a>
											<a href="#" class="btn btn-transparent-white font-weight-bold">White</a>
											-->
											<a href="<?php echo base_url().$Segment1."/".$Segment2."/deleted/";?>" class="btn btn-transparent-white font-weight-bold" style="position: absolute !important; <?php echo $AnotherAlign;?>: 25px !important;">
												<i class="flaticon-delete"></i> <?php echo lang('Deleted');?>
											</a>
										</div>
									<br>

									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('sale_offer_customer_id');?></th>
												<th><?php echo lang('sale_offer_thedate');?></th>
												<th><?php echo lang('buy_offer_expired_date');?></th>
												<th><?php echo lang('sale_offer_image');?></th>
												<th><?php echo lang('sale_offer_totalvalue');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('View');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												
												$sale_offer_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
													<?php
													$customer_id = $DataRows_Row->customer_id;
													$CustomerData = $this->M_sale_customer->GetRow($customer_id);
													if ($this->session->userdata('lang') == "ar")
													{
														$CustomerTitle = $CustomerData->ar_title;
													}
													else
													{
														$CustomerTitle = $CustomerData->en_title;
													}
													echo $CustomerTitle;
													?>
												</td>
												<td><?php echo $DataRows_Row->thedate;?></td>
												<td><?php echo $DataRows_Row->expired_date;?></td>
												<td>
													<img src="<?php echo base_url();?>upload/sale_offer/<?php echo $DataRows_Row->image;?>" style="max-width:100px;">
												</td>
												<td><?php echo $DataRows_Row->totalvalue;?></td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
												<td>
												<button class="btn btn-primary view_detail" relid="<?php echo $DataRows_Row->id;?>"><i class="fa fa-eye"></i></button>
                                                    <a href="<?=base_url('sales/sale_offer/view_pdf/'.$DataRows_Row->id)?>"  class="btn btn-info"><i class="fa fa-print"></i>
                                                        <?php /*echo $this->lang->line('Print');*/?>
                                                    </a>

                                                </td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

									

						
		<div id="show_modal" class="modal fade" role="dialog" style="background: #000;">
			<div class="modal-dialog" style="min-width: 600px;">
				<div class="modal-content">
				<div class="modal-header">
					<h3 style="font-size: 24px; color: #17919e; text-shadow: 1px 1px #ccc;">
					<i class="fa fa-folder"></i> <?php echo $PageName;?>
					</h3>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-3"><?php echo lang('expenses_bill_id');?></div>
						<div class="col-md-3" id="id"></div>
						<div class="col-md-3"><?php echo lang('expenses_thedate');?></div>
						<div class="col-md-3" id="thedate"></div>
					</div>
					<div class="row">
						<div class="col-md-3"><?php echo lang('sale_offer_customer_id');?></div>
						<div class="col-md-3" id="customer"></div>
						<div class="col-md-3"><?php echo lang('buy_offer_totalvalue');?></div>
						<div class="col-md-3" id="totalvalue"></div>
					</div>
					<div class="row">
						<div class="col-md-3"><?php echo lang('buy_offer_over_cost');?></div>
						<div class="col-md-3" id="over_cost"></div>
						<div class="col-md-3"><?php echo lang('buy_offer_tax');?></div>
						<div class="col-md-3" id="tax"></div>
					</div>
					<div class="row">
						<div class="col-md-3"><?php echo lang('sale_offer_discount');?></div>
						<div class="col-md-3" id="discount"></div>
						<div class="col-md-6"></div>
					</div>
					<div class="row">
						<div class="col-md-12" id="notes"></div>
					</div>
					<table id="ModalTable" class="table table-bordered table-striped">
						<thead class="btn-primary">
							<tr>
								<th align='right'><?php echo lang('sale_offer_items_store_type_id');?></th>
								<th style="text-align:center"><?php echo lang('sale_offer_items_quantity');?></th>
								<th style="text-align:center"><?php echo lang('sale_offer_items_unitprice');?></th>
								<th style="text-align:center"><?php echo lang('fin_journal_Total');?></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<div class="modal-footer noPrint">
					<div id="paid" style="visibility:hidden"></div>
					<div id="remaining" style="visibility:hidden"></div>
					<div id="account_title" style="visibility:hidden"></div>
					<button href="<?=base_url('sales/sale_offer/relais')?>" type="button" class="btn btn-danger" id="btnRelais"><i class="fa fa-file-invoice-dollar"></i>ترحيل</button>
					<button type="button" class="btn btn-info" onclick="printSelection(document.getElementById('show_modal'));return false;"><i class="fa fa-print"></i> <?php echo $this->lang->line('Print');?></button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo lang('Close');?></button>
                </div>
				</div>
			</div>
		</div>
