
								<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
									<?php
										$FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
										echo form_open_multipart($FormPath);
									?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_category_id');?></label>
												<select class="form-control" name="category_id" data-size="5" data-live-search="true" required>
													<?php
													$CategoryList = $this->M_fin_treecategory->GetMultiRow();
													foreach($CategoryList as $CategoryList_Row) {
														if($CategoryList_Row->id == 5) {
														if ($this->session->userdata('lang') == "ar")
														{
															$CategoryTitle = $CategoryList_Row->title;
														}
														else
														{
															$CategoryTitle = $CategoryList_Row->title_en;
														}
													?>
													<option <?php if($category_id == $CategoryList_Row->id) {?>selected<?php }?> value="<?php echo $CategoryList_Row->id;?>"><?php echo $CategoryTitle;?></option>
													<?php } } ?>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_parent_id');?></label>
												<select name="parent_id" class="form-control" data-size="5" data-live-search="true" required>
													<?php
													$acc_sales_commission = intval($this->session->userdata('acc_sales_commission'));
													$AccountList = $this->M_fin_treeaccount->GetParentList();
													foreach($AccountList as $AccountList_Row) {
														$AccountID = $AccountList_Row->id;
														if($acc_sales_commission == $AccountID) {
														if ($this->session->userdata('lang') == "ar")
														{
															$AccountTitle = $AccountList_Row->title;
														}
														else
														{
															$AccountTitle = $AccountList_Row->title_en;
														}
													?>
													<option <?php if($parent_id == $AccountList_Row->id) {?>selected<?php }?> value="<?php echo $AccountList_Row->id;?>"><?php echo $AccountTitle;?></option>
													<?php
														}
													}
													?>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_account_no');?></label>
												<input type="text" name="account_no" class="form-control" value="<?php echo $account_no;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_title');?></label>
												<input type="text" name="title" class="form-control" value="<?php echo $title;?>" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_title_en');?></label>
												<input type="text" name="title_en" class="form-control" value="<?php echo $title_en;?>" required>
											</div>
											<div class="col-lg-2 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('fin_treeaccount_startamount');?></label>
												<input type="number" min="0" step="0.001" name="startamount" class="form-control" value="<?php echo $startamount;?>" dir="ltr" required>
											</div>
											<div class="col-lg-2 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('Commission');?></label>
												<input type="number" min="0" step="0.001" name="commission" class="form-control" value="<?php echo $commission;?>" dir="ltr" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php echo $id;?>">
													<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
													<input type="hidden" name="basic" value="<?php echo $basic;?>">
													<input type="hidden" name="level_no" value="<?php echo $level_no;?>">
													<input type="hidden" name="deleted" value="<?php echo $deleted;?>">
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
										echo form_close();
									?>
							</div>
								