<div class="card card-custom gutter-b example example-compact">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
                        $attributes = array('id' => 'SendData', 'name' => 'SendData');
                        echo form_open_multipart($FormPath, $attributes);
                    ?>
								<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('sale_returns_customer_id');?></label>
												<select name="customer_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
													<?php
													$CustomersList = $this->M_sale_customer->GetMultiRow();
													foreach($CustomersList as $CustomersList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$CustomerTitle = $CustomersList_Row->ar_title;
														}
														else
														{
															$CustomerTitle = $CustomersList_Row->en_title;
														}
													?>
													<option value="<?php echo $CustomersList_Row->id;?>"><?php echo $CustomerTitle;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('sale_returns_thedate');?></label>
												<input type="date" name="thedate" class="form-control" value="<?php echo date("Y-m-d");?>" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('sale_returns_image');?></label>
												<input type="file" name="image" class="form-control" dir="ltr" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('sale_returns_tree_id');?></label>
												<select name="tree_id" class="form-control">
													<?php
													$acc_treasury = intval($this->session->userdata('acc_treasury'));
													$toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_treasury);
													foreach($toaccount as $toaccount_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$ToTitle = $toaccount_Row->title;
														}
														else
														{
															$ToTitle = $toaccount_Row->title_en;
														}
													?>
													<option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
													<?php } ?>
													<?php
													$acc_bank = intval($this->session->userdata('acc_bank'));
													$toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_bank);
													foreach($toaccount as $toaccount_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$ToTitle = $toaccount_Row->title;
														}
														else
														{
															$ToTitle = $toaccount_Row->title_en;
														}
													?>
													<option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
													<?php } ?>
												</select>
											</div>
										</div>

										<div class="form-group row">
											<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
												<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('product_barcode');?></label>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12 p-3 mb-2 bg-primary text-white">
												<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('sale_returns_items_store_type_id');?></label>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
												<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('sale_returns_items_location_id');?></label>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
												<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('sale_returns_items_quantity');?></label>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
												<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('sale_returns_items_unitprice');?></label>
											</div>
											<?php
											$InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
											$BackgroundColor = "p-3 mb-2 bg-light text-dark";
											for ($i = 1; $i <= $InvoiceItems; $i++) {
												if($BackgroundColor == "p-3 mb-2 bg-light text-dark")
												{
													$BackgroundColor = "p-3 mb-2 bg-white text-dark";
												}
												else
												{
													$BackgroundColor = "p-3 mb-2 bg-light text-dark";
												}
											?>
												<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
													<input type="text" name="barcode<?php echo $i;?>" id="barcode<?php echo $i;?>" onfocus="GetName(<?php echo $i;?>)" onblur="GetName(<?php echo $i;?>)" class="form-control" value="">
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12 <?php echo $BackgroundColor;?>">
													<input type="text" name="title<?php echo $i;?>" id="title<?php echo $i;?>" class="form-control" value="">
												</div>
												<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
													<select name="location_id<?php echo $i;?>" class="form-control">
														<?php
														$StoreList = $this->M_store->GetMultiRow();
														foreach($StoreList as $StoreList_Row) {
															if ($this->session->userdata('lang') == "ar")
															{
																$FromStore = $StoreList_Row->title;
															}
															else
															{
																$FromStore = $StoreList_Row->title_en;
															}
														?>
														<option value="<?php echo $StoreList_Row->id;?>"><?php echo $FromStore;?></option>
														<?php } ?>
													</select>
												</div>
												<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
													<input type="number" min="0" step="1" name="quantity<?php echo $i;?>" id="quantity<?php echo $i;?>" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();" class="form-control" value="0">
												</div>
												<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
													<input type="number" min="0" step="1" name="unitprice<?php echo $i;?>" id="unitprice<?php echo $i;?>" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();" class="form-control" value="0">
													<input type="hidden" name="subtotal<?php echo $i;?>" id="subtotal<?php echo $i;?>">
												</div>
											<?php
											}
											?>
										</div>
										<div class="append-row">
										    </div>
										 <div class="form-group row">
                                            <div class="col-lg-3 col-md-3 col-sm-12">
                                                <div class="form-group">
                                                    <span class="svg-icon svg-icon-md add" style="float:right; margin-top:-5px;" id="submitbtn">
                                                        <i class="fa fa-plus-circle fa-3x"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

										<div class="form-group row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<label class="col-form-label"><?php echo lang('sale_returns_notes');?></label>
												<input type="text" name="notes" class="form-control" value="" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('sale_returns_totalvalue');?></label>
												<input type="number" min="0" step="0.001" name="totalvalue" id="totalvalue" class="form-control" value="" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('sale_returns_paid');?></label>
												<input type="number" min="0" step="0.001" name="paid" id="paid" onchange="Remaing();" onblur="Remaing();" onkeyup="Remaing();" class="form-control" value="0" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('sale_returns_remaining');?></label>
												<input type="number" min="0" step="0.001" name="remaining" id="remaining" class="form-control" value="" required>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('sale_returns_discount');?></label>
												<input type="number" min="0" step="0.001" name="discount" id="discount" class="form-control" value="0" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<input type="hidden" name="deleted" value="0">
													<input type="hidden" name="RowsCount" id="RowsCount" value="<?php echo $InvoiceItems;?>">
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>
								
	

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>





<script>



    $(document).ready(function () {

        var count = 1;



        $(document).on('click', '#submitbtn', function () {
            var SelectBox = "";

            var BackgroundColor = "p-3 mb-2 bg-success-o-40";



            if (BackgroundColor == "p-3 mb-2 bg-success-o-40") {

                BackgroundColor = "p-3 mb-2 bg-success-o-20";

            } else {

                BackgroundColor = "p-3 mb-2 bg-success-o-40";

            }



            count++;

            var html = '';

            html += '<div class="form-group row">';

            html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';

            html += '<input type="text" name="barcode' + count + '" id="barcode' + count + '" onfocus="GetName(' + count + ')" onblur="GetName(' + count + ')" class="form-control" value="">';
            
            html += '</div>';


            html += '<div class="col-lg-4 col-md-4 col-sm-12 ' + BackgroundColor + '">';

            html += '<input type="text" name="title' + count + '" id="title' + count + '" class="form-control" value="">';

            html += '</div>';
            
            html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';
            
			html += '<select name="location_id' + count + '" class="form-control">';
			
			<?php
			    $StoreList = $this->M_store->GetMultiRow();
			    foreach($StoreList as $StoreList_Row) {
			        if ($this->session->userdata('lang') == "ar")
			        {$FromStore = $StoreList_Row->title;}
			        else{$FromStore = $StoreList_Row->title_en;}
			?>
			html += '<option value="<?php echo $StoreList_Row->id;?>"><?php echo $FromStore;?></option>';
			<?php } ?>
			
			html += '</select> </div>';
			
            html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';

            html += '<input type="number" step="1" min="0" name="quantity' + count + '" id="quantity' + count + '" class="form-control" value="0" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();">';

            html += '</div>';

            html += '<div class="col-lg-2 col-md-2 col-sm-12 ' + BackgroundColor + '">';
            
            html += '<input type="number" step="1" min="0" name="unitprice' + count + '" id="unitprice' + count + '" class="form-control" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();" value="0">';

            html += '<input type="hidden" name="subtotal' + count + '" id="subtotal' + count + '" class="form-control" value="0">';

            html += '</div>';

            html += '</div>';


            $('#RowsCount').val(count);

            $('.append-row').append(html);

        });



        $(document).on('click', '.remove', function () {

            $(this).closest('tr').remove();

        });



    });

</script>