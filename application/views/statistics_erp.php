<div class="col-lg-12">
<div class="d-flex align-items-center p-4 bg-dark">
    <a href="<?php echo base_url();?>home/push_messages" class="btn btn-transparent-success font-weight-bold mr-2">
        <?php echo $this->lang->line('Send_Message_To_Customers');?>
    </a>
    <a href="<?php echo base_url();?>home/push_notifications" class="btn btn-transparent-primary font-weight-bold mr-2">
        <?php echo $this->lang->line('Send_Notification_To_Customers');?>
    </a>
    <a href="<?php echo base_url();?>sales/sale_customer/sale_customer_email" class="btn btn-transparent-danger font-weight-bold mr-2">
        <?php echo $this->lang->line('sale_customer_email');?>
    </a>
    <a href="<?php echo base_url();?>sales/sale_customer/scheduling" class="btn btn-transparent-warning font-weight-bold mr-2">
        <?php echo $this->lang->line('sale_customer_Debt_scheduling');?>
    </a>
    <a href="<?php echo base_url();?>maintenance/data_request_web" class="btn btn-transparent-light font-weight-bold mr-2">
        <?php echo $this->lang->line('data_request_web');?>
    </a>
</div>
</div>

<div class="col-lg-12">
    <!--begin::Advance Table Widget 4-->
    <div class="card card-custom card-stretch gutter-b">
        <!--begin::Header-->
        <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark">
                <?php echo $this->lang->line('sale_customer_email');?>
                </span>
            </h3>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body pt-0 pb-3">
            <div class="tab-content">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                            <tr class="text-left text-uppercase">
                                <th><?php echo $this->lang->line('sale_fullname');?></th>
                                <th><?php echo $this->lang->line('thedate');?></th>
                                <th><?php echo $this->lang->line('thetime');?></th>
                                <th><?php echo $this->lang->line('serv_message_subject');?></th>
                                <th><?php echo $this->lang->line('serv_message_message');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $CustomersEmail = $this->M_sale_customer_email->GetToday();
                            foreach($CustomersEmail as $CustomersEmail_Row) {
                            ?>
                            <tr>
                                <td>
                                <?php
                                $customer_id = $CustomersEmail_Row->customer_id;
                                $CustomerData = $this->M_sale_customer->GetRow($customer_id);
                                if ($this->session->userdata('lang') == "ar")
                                {
                                    $CustomerName = $CustomerData->ar_title;
                                }
                                else
                                {
                                    $CustomerName = $CustomerData->en_title;
                                }
                                echo $CustomerName;
                                ?>
                                </td>
                                <td><?php echo $CustomersEmail_Row->thedate;?></td>
                                <td><?php echo $CustomersEmail_Row->thetime;?></td>
                                <td><?php echo $CustomersEmail_Row->subject;?></td>
                                <td><?php echo $CustomersEmail_Row->message;?></td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <!--end::Table-->
            </div>
        </div>
        <!--end::Body-->
    </div>
    <!--end::Advance Table Widget 4-->
</div>

<!-- -->
<div class="col-lg-12">
    <div class="card card-custom card-stretch gutter-b">
        <!--begin::Header-->
        <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark">
                <?php echo $this->lang->line('set_teamwork_tasks');?>
                </span>
            </h3>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body pt-0 pb-3">
            <div class="tab-content">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                            <tr>
                                <th><?php echo $this->lang->line('set_teamwork');?></th>
                                <th><?php echo $this->lang->line('data_account_invoice_id');?></th>
                                <th><?php echo $this->lang->line('thedate');?></th>
                                <th><?php echo $this->lang->line('thetime');?></th>
                                <th><?php echo $this->lang->line('closed');?></th>
                                <th><?php echo $this->lang->line('close_date');?></th>
                                <th><?php echo $this->lang->line('close_time');?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $MultiRows = $this->M_set_teamwork_tasks->GetOPened();
                        foreach($MultiRows as $MultiRows_Row) {
                        ?>
                        <tr>
                            <td>
                                <?php
                                $teamwork_id = $MultiRows_Row->teamwork_id;
                                $TeamWorkData = $this->M_set_teamwork->GetRow($teamwork_id);
                                echo $TeamWorkData->title;
                                ?>
                            </td>
                            <td>
                                <a target="_blank" href="<?php echo base_url();?>data_invoice/view/<?php echo $MultiRows_Row->invoice_id;?>"><?php echo $MultiRows_Row->invoice_id;?></a>
                            </td>
                            <td><?php echo $MultiRows_Row->thedate; ?></td>
                            <td><?php echo $MultiRows_Row->thetime; ?></td>
                            <td><?php
                            $closed = $MultiRows_Row->closed;
                            if($closed == 0)
                            {
                                echo $this->lang->line('No');
                            }
                            else
                            {
                                echo $this->lang->line('Yes');
                            }
                            ?></td>
                            <td><?php echo $MultiRows_Row->close_date; ?></td>
                            <td><?php echo $MultiRows_Row->close_time; ?></td>
                        </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <!--end::Table-->
            </div>
        </div>
    </div>
</div>
<!-- -->
<div class="col-lg-12">
    <div class="card card-custom card-stretch gutter-b">
        <!--begin::Header-->
        <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark">
                <?php echo $this->lang->line('schedule');?>
                </span>
            </h3>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body pt-0 pb-3">
            <div class="tab-content">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                            <tr>
                                <th><?php echo $this->lang->line('set_teamwork');?></th>
                                <th><?php echo $this->lang->line('data_customer_fullname');?></th>
                                <th><?php echo $this->lang->line('data_customer_phone');?></th>
                                <th><?php echo $this->lang->line('data_customer_address');?></th>
                                <th><?php echo $this->lang->line('thedate');?></th>
                                <th><?php echo $this->lang->line('schedule');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $Schedule = $this->M_set_teamwork_schedule->GetMultiRow();
                            foreach($Schedule as $Schedule_Row) {
                            ?>
                            <tr>
                                <td>
                                    <?php
                                    $teamwork_id = $Schedule_Row->teamwork_id;
                                    $TeamWorkData = $this->M_set_teamwork->GetRow($teamwork_id);
                                    echo $TeamWorkData->ar_title;
                                    ?>
                                </td>
                                <td><?php echo $Schedule_Row->customer; ?></td>
                                <td><?php echo $Schedule_Row->phone; ?></td>
                                <td><?php echo $Schedule_Row->address; ?></td>
                                <td><?php echo $Schedule_Row->thedate; ?></td>
                                <td><?php echo $Schedule_Row->schedule; ?></td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <!--end::Table-->
            </div>
        </div>
    </div>
</div>
<!-- -->

<div class="col-xl-4">
    <div class="card card-custom card-stretch gutter-b">
        <figure class="highcharts-figure" <?php if ($this->session->userdata('lang') == "ar"){ ?>style="font-family: 'Droid Arabic Kufi', serif;"<?php }?>>
            <div id="container"></div>
            <table id="SalesChart" style="width:98%">
                <thead>
                    <tr>
                        <th><?php echo $this->lang->line('Year');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('highcharts_sales');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('highcharts_sales_paid');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('highcharts_sales_remaining');?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                        <?php
                        echo date("Y");
                        $StartData = date("Y-01-01");
                        $EndDate = date("Y-12-31");
                        ?>
                        </td>
                        <td style="text-align:center">
                        <?php
                        $TotalSale = $this->M_statistics->GetTotalSales_Month($StartData, $EndDate);
                        echo doubleval($TotalSale->totalvalue);
                        ?>
                        </td>
                        <td style="text-align:center">
                        <?php
                        $TotalPaid = $this->M_statistics->GetTotalSales_Paid_Month($StartData, $EndDate);
                        echo doubleval($TotalPaid->paid);
                        ?>
                        </td>
                        <td style="text-align:center">
                        <?php
                        $TotalRemaining = $this->M_statistics->GetTotalSales_Remaining_Month($StartData, $EndDate);
                        echo doubleval($TotalRemaining->remaining);
                        ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </figure>
    </div>
</div>
<div class="col-xl-4">
    <div class="card card-custom card-stretch gutter-b">
        <figure class="highcharts-figure" <?php if ($this->session->userdata('lang') == "ar"){ ?>style="font-family: 'Droid Arabic Kufi', serif;"<?php }?>>
            <div id="Purchase_container"></div>
            <table id="PurchaseChart" style="width:98%">
                <thead>
                    <tr>
                        <th><?php echo $this->lang->line('Year');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('highcharts_Purchase');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('highcharts_sales_paid');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('highcharts_sales_remaining');?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                        <?php
                        echo date("Y");
                        $StartData = date("Y-01-01");
                        $EndDate = date("Y-12-31");
                        ?>
                        </td>
                        <td style="text-align:center">
                        <?php
                        $TotalSale = $this->M_statistics->GetTotalPurchase_Month($StartData, $EndDate);
                        echo doubleval($TotalSale->totalvalue);
                        ?>
                        </td>
                        <td style="text-align:center">
                        <?php
                        $TotalPaid = $this->M_statistics->GetTotalPurchase_Paid_Month($StartData, $EndDate);
                        echo doubleval($TotalPaid->paid);
                        ?>
                        </td>
                        <td style="text-align:center">
                        <?php
                        $TotalRemaining = $this->M_statistics->GetTotalPurchase_Remaining_Month($StartData, $EndDate);
                        echo doubleval($TotalRemaining->remaining);
                        ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </figure>
    </div>
</div>
<div class="col-xl-4">
    <div class="card card-custom card-stretch gutter-b">
        <figure class="highcharts-figure" <?php if ($this->session->userdata('lang') == "ar"){ ?>style="font-family: 'Droid Arabic Kufi', serif;"<?php }?>>
            <div id="Service_container"></div>
            <table id="ServiceChart" style="width:98%">
                <thead>
                    <tr>
                        <th><?php echo $this->lang->line('Year');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('set_teamwork_revenues');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('set_teamwork_expenses');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('set_teamwork_net');?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                        <?php
                        echo date("Y");
                        $StartData = date("Y-01-01");
                        $EndDate = date("Y-12-31");
                        ?>
                        </td>
                        <td style="text-align:center">
                        <?php
                        $TotalRevenues = $this->M_statistics->GetTotalService_Revenues($StartData, $EndDate);
                        $qunatity = 0;
                        $unit_price = 0;
                        $TotalRevenuesValue = 0;
                        foreach($TotalRevenues as $TotalRevenues_Row) {
                            $qunatity = $TotalRevenues_Row->qunatity;
                            $unit_price = $TotalRevenues_Row->unit_price;
                            $TotalRevenuesValue += ($qunatity * $unit_price);
                        }
                        echo doubleval($TotalRevenuesValue);
                        ?>
                        </td>
                        <td style="text-align:center">
                        <?php
                        $TotalExpenses = $this->M_statistics->GetTotalService_Expenses($StartData, $EndDate);
                        echo doubleval($TotalExpenses->amount);
                        $TotalExpensesValue = $TotalExpenses->amount;
                        ?>
                        </td>
                        <td style="text-align:center">
                        <?php
                        $NetServiceIncome = $TotalRevenuesValue - $TotalExpensesValue;
                        echo doubleval($NetServiceIncome);
                        ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </figure>
    </div>
</div>
<div class="col-xl-4">
    <div class="card card-custom card-stretch gutter-b">
        <figure class="highcharts-figure" <?php if ($this->session->userdata('lang') == "ar"){ ?>style="font-family: 'Droid Arabic Kufi', serif;"<?php }?>>
            <div id="Treasury_container"></div>
            <table id="TreasuryChart" style="width:98%">
                <thead>
                    <tr>
                        <th><?php echo $this->lang->line('sale_bill_treasury');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('data_account_depit');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('data_account_credit');?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $StartData = date("Y-01-01");
                    $EndDate = date("Y-12-31");
                    $acc_treasury = intval($this->session->userdata('acc_treasury'));
                    $Treasury = $this->M_fin_treeaccount->GetMain_Sub($acc_treasury);
                    foreach($Treasury as $Treasury_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $TreasuryTitle = $Treasury_Row->title;
                        }
                        else
                        {
                            $TreasuryTitle = $Treasury_Row->title_en;
                        }
                        $TreasuryID = $Treasury_Row->id;
                    ?>
                    <tr>
                        <td><?php echo $TreasuryTitle;?></td>
                        <td style="text-align:center">
                            <?php
                            $TotalDebit = $this->M_fin_journal->GetSum_Debit_Year($TreasuryID, $StartData, $EndDate);
                            echo doubleval($TotalDebit->debit);
                            ?>
                        </td>
                        <td style="text-align:center">
                            <?php
                            $TotalCreditor = $this->M_fin_journal->GetSum_Creditor_Year($TreasuryID, $StartData, $EndDate);
                            echo doubleval($TotalCreditor->creditor);
                            ?>
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </figure>
    </div>
</div>
<div class="col-xl-4">
    <div class="card card-custom card-stretch gutter-b">
        <figure class="highcharts-figure" <?php if ($this->session->userdata('lang') == "ar"){ ?>style="font-family: 'Droid Arabic Kufi', serif;"<?php }?>>
            <div id="Bank_container"></div>
            <table id="BankChart" style="width:98%">
                <thead>
                    <tr>
                        <th><?php echo $this->lang->line('acc_bank');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('data_account_depit');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('data_account_credit');?></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $StartData = date("Y-01-01");
                    $EndDate = date("Y-12-31");
                    $acc_bank = intval($this->session->userdata('acc_bank'));
                    $Bank = $this->M_fin_treeaccount->GetMain_Sub($acc_bank);
                    foreach($Bank as $Bank_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $BankTitle = $Bank_Row->title;
                        }
                        else
                        {
                            $BankTitle = $Bank_Row->title_en;
                        }
                        $BankID = $Bank_Row->id;
                    ?>
                    <tr>
                        <td><?php echo $BankTitle;?></td>
                        <td style="text-align:center">
                            <?php
                            $TotalDebit = $this->M_fin_journal->GetSum_Debit_Year($BankID, $StartData, $EndDate);
                            echo doubleval($TotalDebit->debit);
                            ?>
                        </td>
                        <td style="text-align:center">
                            <?php
                            $TotalCreditor = $this->M_fin_journal->GetSum_Creditor_Year($BankID, $StartData, $EndDate);
                            echo doubleval($TotalCreditor->creditor);
                            ?>
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </figure>
    </div>
</div>
<div class="col-xl-4">
    <div class="card card-custom card-stretch gutter-b">
        <figure class="highcharts-figure" <?php if ($this->session->userdata('lang') == "ar"){ ?>style="font-family: 'Droid Arabic Kufi', serif;"<?php }?>>
            <div id="Custody_container"></div>
            <table id="CustodyChart" style="width:98%">
                <thead>
                    <tr>
                        <th><?php echo $this->lang->line('acc_custody');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('data_account_depit');?></th>
                        <th style="text-align:center"><?php echo $this->lang->line('data_account_credit');?></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $StartData = date("Y-01-01");
                    $EndDate = date("Y-12-31");
                    $acc_custody = intval($this->session->userdata('acc_custody'));
                    $Custody = $this->M_fin_treeaccount->GetMain_Sub($acc_custody);
                    foreach($Custody as $Custody_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $CustodyTitle = $Custody_Row->title;
                        }
                        else
                        {
                            $CustodyTitle = $Custody_Row->title_en;
                        }
                        $CustodyID = $Custody_Row->id;
                    ?>
                    <tr>
                        <td><?php echo $CustodyTitle;?></td>
                        <td style="text-align:center">
                            <?php
                            $TotalDebit = $this->M_fin_journal->GetSum_Debit_Year($CustodyID, $StartData, $EndDate);
                            echo doubleval($TotalDebit->debit);
                            ?>
                        </td>
                        <td style="text-align:center">
                            <?php
                            $TotalCreditor = $this->M_fin_journal->GetSum_Creditor_Year($CustodyID, $StartData, $EndDate);
                            echo doubleval($TotalCreditor->creditor);
                            ?>
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </figure>
    </div>
</div>
<div class="col-xl-4">
    <!--begin::Stats Widget 4-->
    <div class="card card-custom card-stretch gutter-b">
        <!--begin::Body-->
        <div class="card-body d-flex align-items-center py-0 mt-8">
            <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                <a href="<?php echo base_url();?>permissions/usr_users" class="card-title font-weight-bolder text-dark-75 font-size-h5 mb-2 text-hover-primary">
                <?php echo $this->lang->line('statistics_Users');?> : <?php echo $this->M_statistics->TotalUsers();?>
                </a>
                <span class="font-weight-bold text-muted font-size-lg">
                <?php echo $this->lang->line('statistics_Users_note');?>
                </span>
            </div>
            <img src="<?php echo base_url();?>assets/media/svg/avatars/001-boy.svg" alt="" class="align-self-end h-100px" />
        </div>
        <!--end::Body-->
    </div>
    <!--end::Stats Widget 4-->
</div>
<div class="col-xl-4">
    <!--begin::Stats Widget 5-->
    <div class="card card-custom card-stretch gutter-b">
        <!--begin::Body-->
        <div class="card-body d-flex align-items-center py-0 mt-8">
            <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                <a href="<?php echo base_url();?>sales/sale_customer" class="card-title font-weight-bolder text-dark-75 font-size-h5 mb-2 text-hover-primary">
                <?php echo $this->lang->line('statistics_Customers');?> : <?php echo $this->M_statistics->TotalCustomers();?>
                </a>
                <span class="font-weight-bold text-muted font-size-lg">
                <?php echo $this->lang->line('statistics_Customers_note');?>
                <?php
                $CustomersAccount = intval($this->session->userdata('acc_customer'));
                $AllCustomers = $this->M_fin_treeaccount->GetSubAccounts($CustomersAccount);
                $Final = 0;
                foreach($AllCustomers as $AllCustomers_Row) {
                    $CustomerID = $AllCustomers_Row->id;
                    $startamount = $AllCustomers_Row->startamount;
                    $DebitData = $this->M_fin_journal->GetSum_Debit($CustomerID);
                    $Debit = $DebitData->debit;

                    $CreditorData = $this->M_fin_journal->GetSum_Creditor($CustomerID);
                    $Creditor = $CreditorData->creditor;

                    $Final += ($startamount + $Debit - $Creditor);
                }
                echo $Final;
                ?>
                </span>
            </div>
            <img src="<?php echo base_url();?>assets/media/svg/avatars/024-boy-9.svg" alt="" class="align-self-end h-100px" />
        </div>
        <!--end::Body-->
    </div>
    <!--end::Stats Widget 5-->
</div>
<div class="col-xl-4">
    <!--begin::Stats Widget 6-->
    <div class="card card-custom card-stretch gutter-b">
        <!--begin::Body-->
        <div class="card-body d-flex align-items-center py-0 mt-8">
            <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                <a href="<?php echo base_url();?>purchase/buy_manufacturer" class="card-title font-weight-bolder text-dark-75 font-size-h5 mb-2 text-hover-primary">
                <?php echo $this->lang->line('statistics_Suppliers');?> : <?php echo $this->M_statistics->TotalSuppliers();?>
                </a>
                <span class="font-weight-bold text-muted font-size-lg">
                <?php echo $this->lang->line('statistics_Suppliers_note');?>
                <?php
                $SuppliersAccount = intval($this->session->userdata('acc_supplier'));
                $AllSuppliers = $this->M_fin_treeaccount->GetSubAccounts($SuppliersAccount);
                $Final = 0;
                foreach($AllSuppliers as $AllSuppliers_Row) {
                    $SupplierID = $AllSuppliers_Row->id;
                    $startamount = $AllSuppliers_Row->startamount;
                    $DebitData = $this->M_fin_journal->GetSum_Debit($SupplierID);
                    $Debit = $DebitData->debit;

                    $CreditorData = $this->M_fin_journal->GetSum_Creditor($SupplierID);
                    $Creditor = $CreditorData->creditor;

                    $Final += ($startamount + $Creditor - $Debit);
                }
                if($Final < 0)
                {
                    echo "(".$Final.")";
                }
                else
                {
                    echo $Final;
                }
                ?>
                </span>
            </div>
            <img src="<?php echo base_url();?>assets/media/svg/avatars/004-boy-1.svg" alt="" class="align-self-end h-100px" />
        </div>
        <!--end::Body-->
    </div>
    <!--end::Stats Widget 6-->
</div>

<div class="col-xl-4">
    <!--begin::Stats Widget 13-->
    <a href="<?php echo base_url();?>sales/sale_bill" class="card card-custom bg-danger bg-hover-state-danger card-stretch gutter-b">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                        <path d="M3.5,9 L20.5,9 C21.0522847,9 21.5,9.44771525 21.5,10 C21.5,10.132026 21.4738562,10.2627452 21.4230769,10.3846154 L17.7692308,19.1538462 C17.3034221,20.271787 16.2111026,21 15,21 L9,21 C7.78889745,21 6.6965779,20.271787 6.23076923,19.1538462 L2.57692308,10.3846154 C2.36450587,9.87481408 2.60558331,9.28934029 3.11538462,9.07692308 C3.23725479,9.02614384 3.36797398,9 3.5,9 Z M12,17 C13.1045695,17 14,16.1045695 14,15 C14,13.8954305 13.1045695,13 12,13 C10.8954305,13 10,13.8954305 10,15 C10,16.1045695 10.8954305,17 12,17 Z" fill="#000000" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-inverse-danger font-weight-bolder font-size-h5 mb-2 mt-5">
                <?php echo $this->lang->line('statistics_Sales');?> : 
                <?php
                $TotalSales = $this->M_statistics->GetTotalSales();
                echo $TotalSales->totalvalue;
                ?>
            </div>
            <div class="font-weight-bold text-inverse-danger font-size-sm">
            <?php
            if($this->session->userdata('statistics_filter') == "Today")
            {
                echo $this->lang->line('statistics_Sales_note_today');
            }
            else if($this->session->userdata('statistics_filter') == "Month")
            {
                echo $this->lang->line('statistics_Sales_note_month');
            }
            else
            {
                echo $this->lang->line('statistics_Sales_note_year');
            }
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 13-->
</div>

<div class="col-xl-4">
    <!--begin::Stats Widget 13-->
    <a href="<?php echo base_url();?>sales/sale_bill" class="card card-custom bg-primary bg-hover-state-primary card-stretch gutter-b">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                        <path d="M3.5,9 L20.5,9 C21.0522847,9 21.5,9.44771525 21.5,10 C21.5,10.132026 21.4738562,10.2627452 21.4230769,10.3846154 L17.7692308,19.1538462 C17.3034221,20.271787 16.2111026,21 15,21 L9,21 C7.78889745,21 6.6965779,20.271787 6.23076923,19.1538462 L2.57692308,10.3846154 C2.36450587,9.87481408 2.60558331,9.28934029 3.11538462,9.07692308 C3.23725479,9.02614384 3.36797398,9 3.5,9 Z M12,17 C13.1045695,17 14,16.1045695 14,15 C14,13.8954305 13.1045695,13 12,13 C10.8954305,13 10,13.8954305 10,15 C10,16.1045695 10.8954305,17 12,17 Z" fill="#000000" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-inverse-danger font-weight-bolder font-size-h5 mb-2 mt-5">
                <?php echo $this->lang->line('statistics_Sales_paid');?> : 
                <?php
                $TotalSales = $this->M_statistics->GetTotalSales_Paid();
                echo $TotalSales->paid;
                ?>
            </div>
            <div class="font-weight-bold text-inverse-danger font-size-sm">
            <?php
            if($this->session->userdata('statistics_filter') == "Today")
            {
                echo $this->lang->line('statistics_Sales_note_today');
            }
            else if($this->session->userdata('statistics_filter') == "Month")
            {
                echo $this->lang->line('statistics_Sales_note_month');
            }
            else
            {
                echo $this->lang->line('statistics_Sales_note_year');
            }
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 13-->
</div>

<div class="col-xl-4">
    <!--begin::Stats Widget 13-->
    <a href="<?php echo base_url();?>sales/sale_bill" class="card card-custom bg-success bg-hover-state-success card-stretch gutter-b">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                        <path d="M3.5,9 L20.5,9 C21.0522847,9 21.5,9.44771525 21.5,10 C21.5,10.132026 21.4738562,10.2627452 21.4230769,10.3846154 L17.7692308,19.1538462 C17.3034221,20.271787 16.2111026,21 15,21 L9,21 C7.78889745,21 6.6965779,20.271787 6.23076923,19.1538462 L2.57692308,10.3846154 C2.36450587,9.87481408 2.60558331,9.28934029 3.11538462,9.07692308 C3.23725479,9.02614384 3.36797398,9 3.5,9 Z M12,17 C13.1045695,17 14,16.1045695 14,15 C14,13.8954305 13.1045695,13 12,13 C10.8954305,13 10,13.8954305 10,15 C10,16.1045695 10.8954305,17 12,17 Z" fill="#000000" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-inverse-danger font-weight-bolder font-size-h5 mb-2 mt-5">
                <?php echo $this->lang->line('statistics_Sales_remaining');?> : 
                <?php
                $TotalSales = $this->M_statistics->GetTotalSales_Remaining();
                echo $TotalSales->remaining;
                ?>
            </div>
            <div class="font-weight-bold text-inverse-danger font-size-sm">
            <?php
            if($this->session->userdata('statistics_filter') == "Today")
            {
                echo $this->lang->line('statistics_Sales_note_today');
            }
            else if($this->session->userdata('statistics_filter') == "Month")
            {
                echo $this->lang->line('statistics_Sales_note_month');
            }
            else
            {
                echo $this->lang->line('statistics_Sales_note_year');
            }
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 13-->
</div>

<div class="col-xl-4">
    <!--begin::Stats Widget 16-->
    <a href="<?php echo base_url();?>purchase/buy_bill" class="card card-custom card-stretch gutter-b bg-gray-900">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-info svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                        <path d="M3.5,9 L20.5,9 C21.0522847,9 21.5,9.44771525 21.5,10 C21.5,10.132026 21.4738562,10.2627452 21.4230769,10.3846154 L17.7692308,19.1538462 C17.3034221,20.271787 16.2111026,21 15,21 L9,21 C7.78889745,21 6.6965779,20.271787 6.23076923,19.1538462 L2.57692308,10.3846154 C2.36450587,9.87481408 2.60558331,9.28934029 3.11538462,9.07692308 C3.23725479,9.02614384 3.36797398,9 3.5,9 Z M12,17 C13.1045695,17 14,16.1045695 14,15 C14,13.8954305 13.1045695,13 12,13 C10.8954305,13 10,13.8954305 10,15 C10,16.1045695 10.8954305,17 12,17 Z" fill="#000000" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
            <?php echo $this->lang->line('statistics_Purchase');?> : 
            <?php
            $TotalPurchase = $this->M_statistics->GetTotalPurchase();
            echo $TotalPurchase->totalvalue;
            ?>
            </div>
            <div class="font-weight-bold text-white font-size-sm">
            <?php
            if($this->session->userdata('statistics_filter') == "Today")
            {
                echo $this->lang->line('statistics_Purchase_note_today');
            }
            else if($this->session->userdata('statistics_filter') == "Month")
            {
                echo $this->lang->line('statistics_Purchase_note_month');
            }
            else
            {
                echo $this->lang->line('statistics_Purchase_note_year');
            }
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>

<div class="col-xl-4">
    <!--begin::Stats Widget 16-->
    <a href="<?php echo base_url();?>purchase/buy_bill" class="card card-custom card-stretch gutter-b bg-gray-800">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-info svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                        <path d="M3.5,9 L20.5,9 C21.0522847,9 21.5,9.44771525 21.5,10 C21.5,10.132026 21.4738562,10.2627452 21.4230769,10.3846154 L17.7692308,19.1538462 C17.3034221,20.271787 16.2111026,21 15,21 L9,21 C7.78889745,21 6.6965779,20.271787 6.23076923,19.1538462 L2.57692308,10.3846154 C2.36450587,9.87481408 2.60558331,9.28934029 3.11538462,9.07692308 C3.23725479,9.02614384 3.36797398,9 3.5,9 Z M12,17 C13.1045695,17 14,16.1045695 14,15 C14,13.8954305 13.1045695,13 12,13 C10.8954305,13 10,13.8954305 10,15 C10,16.1045695 10.8954305,17 12,17 Z" fill="#000000" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
            <?php echo $this->lang->line('statistics_Purchase_paid');?> : 
            <?php
            $TotalPurchasePaid = $this->M_statistics->GetTotalPurchase_Paid();
            echo $TotalPurchasePaid->paid;
            ?>
            </div>
            <div class="font-weight-bold text-white font-size-sm">
            <?php
            if($this->session->userdata('statistics_filter') == "Today")
            {
                echo $this->lang->line('statistics_Purchase_note_today');
            }
            else if($this->session->userdata('statistics_filter') == "Month")
            {
                echo $this->lang->line('statistics_Purchase_note_month');
            }
            else
            {
                echo $this->lang->line('statistics_Purchase_note_year');
            }
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>

<div class="col-xl-4">
    <!--begin::Stats Widget 16-->
    <a href="<?php echo base_url();?>purchase/buy_bill" class="card card-custom card-stretch gutter-b bg-gray-700">
        <!--begin::Body-->
        <div class="card-body">
            <span class="svg-icon svg-icon-info svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                        <path d="M3.5,9 L20.5,9 C21.0522847,9 21.5,9.44771525 21.5,10 C21.5,10.132026 21.4738562,10.2627452 21.4230769,10.3846154 L17.7692308,19.1538462 C17.3034221,20.271787 16.2111026,21 15,21 L9,21 C7.78889745,21 6.6965779,20.271787 6.23076923,19.1538462 L2.57692308,10.3846154 C2.36450587,9.87481408 2.60558331,9.28934029 3.11538462,9.07692308 C3.23725479,9.02614384 3.36797398,9 3.5,9 Z M12,17 C13.1045695,17 14,16.1045695 14,15 C14,13.8954305 13.1045695,13 12,13 C10.8954305,13 10,13.8954305 10,15 C10,16.1045695 10.8954305,17 12,17 Z" fill="#000000" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
            <?php echo $this->lang->line('statistics_Purchase_remaining');?> : 
            <?php
            $TotalPurchasePaid = $this->M_statistics->GetTotalPurchase_Remaining();
            echo $TotalPurchasePaid->remaining;
            ?>
            </div>
            <div class="font-weight-bold text-white font-size-sm">
            <?php
            if($this->session->userdata('statistics_filter') == "Today")
            {
                echo $this->lang->line('statistics_Purchase_note_today');
            }
            else if($this->session->userdata('statistics_filter') == "Month")
            {
                echo $this->lang->line('statistics_Purchase_note_month');
            }
            else
            {
                echo $this->lang->line('statistics_Purchase_note_year');
            }
            ?>
            </div>
        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>