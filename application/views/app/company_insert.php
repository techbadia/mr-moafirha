<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <?php
        $FormPath = base_url()."branche/insert_data";
        echo form_open_multipart($FormPath);
    ?>
        <div class="form-body">
            <div class="form-group row">
				<div class="col-md-12">
                <label><?php echo $this->lang->line('AddBrancheMessage');?>:</label>
				</div>
            </div>
            <div class="form-group row">
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('users_group_ar_title');?>:</label>
                    <input type="text" id="ar_title" name="ar_title" class="form-control" required>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('users_group_en_title');?>:</label>
                    <input type="text" id="en_title" name="en_title" class="form-control" dir="ltr" required>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('buy_manufacturer_tax_card');?>:</label>
                    <input type="text" id="tax_number" name="tax_number" class="form-control" dir="ltr" required>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('email');?>:</label>
                    <input type="email" id="email" name="email" class="form-control" required>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('buy_manufacturer_person');?>:</label>
                    <input type="text" id="person" name="person" class="form-control" required>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('buy_manufacturer_phone');?>:</label>
                    <input type="text" id="phone" name="phone" class="form-control" required>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('invoice_sale_start');?>:</label>
                    <input type="text" id="invoice_sale_start" name="invoice_sale_start" class="form-control" value="" required>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('invoice_sale_return_start');?>:</label>
                    <input type="text" id="invoice_sale_return_start" name="invoice_sale_return_start" class="form-control" value="" required>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('invoice_buy_start');?>:</label>
                    <input type="text" id="invoice_buy_start" name="invoice_buy_start" class="form-control" value="" required>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('invoice_buy_return_start');?>:</label>
                    <input type="text" id="invoice_buy_return_start" name="invoice_buy_return_start" class="form-control" value="" required>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('invoice_service_start');?>:</label>
                    <input type="text" id="invoice_service_start" name="invoice_service_start" class="form-control" value="" required>
                </div>
            </div>
        </div>
        <div class="form-actions right" style="text-align: right; font-weight: bold !important; font-size: 18px;">
            <input type="hidden" name="deleted" value="0">
            <a href="<?php echo base_url()."branche";?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
            <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
        </div>
    <?php
        echo form_close();
    ?>
</div>
            