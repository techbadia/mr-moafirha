<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
	<thead>
		<tr>
			<th>id</th>
			<th><?php echo $this->lang->line('users_group_ar_title');?></th>
			<th><?php echo $this->lang->line('users_group_en_title');?></th>
			<th><?php echo $this->lang->line('email');?></th>
			<th><?php echo $this->lang->line('buy_manufacturer_person');?></th>
			<th><?php echo $this->lang->line('buy_manufacturer_phone');?></th>
			<th><?php echo $this->lang->line('buy_manufacturer_tax_card');?></th>
			<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach($MultiRows as $MultiRows_Row) {
			$UserID = $MultiRows_Row->id;
		?>
		<tr>
			<td><?php echo $MultiRows_Row->id;?></td>
			<td><?php echo $MultiRows_Row->ar_title;?></td>
			<td><?php echo $MultiRows_Row->en_title; ?></td>
			<td><?php echo $MultiRows_Row->email;?></td>
			<td><?php echo $MultiRows_Row->person;?></td>
			<td><?php echo $MultiRows_Row->phone;?></td>
			<td><?php echo $MultiRows_Row->tax_number;?></td>
			<td style="text-align:center">
				<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$MultiRows_Row->id;?>">
					<i class="flaticon-edit-1 text-primary icon-lg"></i> 
				</a>
			</td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>