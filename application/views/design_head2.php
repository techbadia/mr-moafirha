<base href="">
<meta charset="utf-8" />
<title><?php echo $this->session->userdata('package_title');?> | <?php echo $this->lang->line('Dashboard');?></title>
<meta name="description" content="LBI-ERP System | <?php echo $this->lang->line('Dashboard');?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<link rel="canonical" href="https://www.lbi-egypt.com/" />
<!--begin::Fonts-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
<!--end::Fonts-->

<!--end::Layout Themes-->
<link rel="shortcut icon" href="<?php echo base_url();?>/assets/media/logos/favicon.ico" />




	<link href="<?php echo base_url();?>/assets/2/assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>/assets/2/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>/assets/2/assets/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet" />
	<!-- loader-->
	<link href="<?php echo base_url();?>/assets/2/assets/css/pace.min.css" rel="stylesheet" />
	<script src="<?php echo base_url();?>/assets/2/assets/js/pace.min.js"></script>
	<!-- Bootstrap CSS -->
	<link href="<?php echo base_url();?>/assets/2/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
	<link href="<?php echo base_url();?>/assets/2/assets/css/app.css" rel="stylesheet">
	<link href="<?php echo base_url();?>/assets/2/assets/css/icons.css" rel="stylesheet">
	<!-- Theme Style CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/2/assets/css/dark-theme.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/2/assets/css/semi-dark.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/2/assets/css/header-colors.css" />


	<link href="<?php echo base_url();?>/assets/2/assets/plugins/highcharts/css/highcharts.css" rel="stylesheet" />









<?php

$Segment1 = $this->uri->segment(1);
$Segment2 = $this->uri->segment(2);
$Segment3 = $this->uri->segment(3);
$Segment4 = $this->uri->segment(4);
$Segment5 = $this->uri->segment(5);

if ($this->session->userdata('lang') == "ar"){
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/arabic_font.css" />
<?php
}
?>
<?php
$assets = base_url().'application/views/assets/';
?>

<?php
$Align = $this->session->userdata('Alignment');
$AnotherAlign = "";
$AnotherDirection = "";
if($Align == "right")
{
    $AnotherAlign = "left";
}
else
{
    $AnotherAlign = "right";
}
?>

<?php
if(($Segment1 == "account") && ($Segment2 == "fin_liquidity_risk") || ($Segment1 == "") || ($Segment1 == "erp") || 
($Segment1 == "cars") || ($Segment1 == "car_rental"))
{
?>
<style type="text/css">
#container {
    height: 300px;
}
#Purchase_container {
    height: 300px;
}
#Service_container {
    height: 300px;
}
#Treasury_container {
    height: 300px;
}
#Bank_container {
    height: 300px;
}
#Custody_container {
    height: 300px;
}
.highcharts-figure, .highcharts-data-table table {
    min-width: 100%;
    max-width: 800px;
    margin: 1em auto;
}

#datatable {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #EBEBEB;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
}
#datatable caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
#datatable th {
	font-weight: 600;
    padding: 0.5em;
}
#datatable td, #datatable th, #datatable caption {
    padding: 0.5em;
}
#datatable thead tr, #datatable tr:nth-child(even) {
    background: #f8f8f8;
}
#datatable tr:hover {
    background: #f1f7ff;
}
</style>




	

<?php
}
?>

<?php
if(($Segment1 == "mydata"))
{
?>
<style type="text/css">
.progress.active .progress-bar {
    -webkit-transition: none !important;
    transition: none !important;
}
</style>
<?php
}
?>