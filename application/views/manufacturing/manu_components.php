									<br>
									<div class="d-flex align-items-center p-4 bg-dark" style="text-align:center !important">
										<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-transparent-success font-weight-bold mr-2">
											<?php echo lang('manu_components_date_all');?>
										</a>
										
										<a href="<?php echo base_url().$Segment1."/".$Segment2."/deleted/";?>" class="btn btn-transparent-white font-weight-bold" style="position: absolute !important; <?php echo $AnotherAlign;?>: 25px !important;">
											<i class="flaticon-delete"></i> <?php echo lang('Deleted');?>
										</a>
									</div>
									<br>
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('manu_components_user_id');?></th>
												<th><?php echo lang('manu_components_thedate');?></th>
												<th><?php echo lang('manu_components_over_cost');?></th>
												<th><?php echo lang('manu_components_notes');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('View');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												
												$manu_components_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
													<?php
													$user_id = $DataRows_Row->user_id;
													$UserData = $this->M_usr_users->GetRow($user_id);
													echo $UserData->fullname;
													?>
												</td>
												<td><?php echo $DataRows_Row->thedate;?></td>
												<td><?php echo $DataRows_Row->over_cost;?></td>
												<td><?php echo $DataRows_Row->notes;?></td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
												<td>
												<button class="btn btn-light-success view_detail" relid="<?php echo $DataRows_Row->id;?>"><i class="fa fa-eye"></i></button>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

									
					

		<div id="show_modal" class="modal fade" role="dialog" style="background: #000;">
			<div class="modal-dialog" style="min-width: 800px;">
				<div class="modal-content">
					<div class="modal-header">
						<h3 style="font-size: 24px; color: #17919e; text-shadow: 1px 1px #ccc;">
							<i class="fa fa-folder"></i> <?php echo $PageName;?>
						</h3>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-3"><?php echo lang('expenses_bill_id');?></div>
							<div class="col-md-3" id="id"></div>
							<div class="col-md-3"></div>
							<div class="col-md-3"></div>
						</div>
						<div class="row">
							<div class="col-md-3"><?php echo lang('manu_components_over_cost');?></div>
							<div class="col-md-3" id="over_cost"></div>
							<div class="col-md-3"><?php echo lang('expenses_thedate');?></div>
							<div class="col-md-3" id="thedate"></div>
						</div>
						<div class="row">
							<div class="col-md-12" id="notes"></div>
						</div>
						<table id="ModalTable" class="table table-bordered table-striped">
							<thead class="btn-primary">
								<tr>
									<th align='right'><?php echo lang('buy_offer_items_store_type_id');?></th>
									<th style="text-align:center"><?php echo lang('buy_offer_items_quantity');?></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
					<div class="modal-footer noPrint">
						<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo lang('Close');?></button>
						<button type="button" class="btn btn-info" onclick="printSelection(document.getElementById('show_modal'));return false;"><i class="fa fa-print"></i> <?php echo $this->lang->line('Print');?></button>
					</div>
				</div>
			</div>
		</div>