<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
	<?php
		$FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
		$attributes = array('id' => 'SendData', 'name' => 'SendData');
		echo form_open_multipart($FormPath, $attributes);
	?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('manu_components_thedate');?></label>
				<input type="date" name="thedate" class="form-control" value="<?php echo date("Y-m-d");?>" required>
			</div>
			<div class="col-lg-2 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('manu_components_over_cost');?></label>
				<input type="number" min="0" step="0.001" name="over_cost" class="form-control" value="0" required>
			</div>
			<div class="col-lg-7 col-md-12 col-sm-12">
				<label class="col-form-label"><?php echo lang('manu_components_notes');?></label>
				<input type="text" name="notes" class="form-control" value="" required>
			</div>
		</div>

		<div class="form-group row">
			<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
				<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('product_barcode');?></label>
			</div>
			<div class="col-lg-8 col-md-6 col-sm-12 p-3 mb-2 bg-primary text-white">
				<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('buy_offer_items_store_type_id');?></label>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
				<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('buy_offer_items_quantity');?></label>
			</div>
			<?php
			$InvoiceItems = intval($this->session->userdata('acc_invoice_items'));
			$BackgroundColor = "p-3 mb-2 bg-light text-dark";
			for ($i = 1; $i <= $InvoiceItems; $i++) {
				if($BackgroundColor == "p-3 mb-2 bg-light text-dark")
				{
					$BackgroundColor = "p-3 mb-2 bg-white text-dark";
				}
				else
				{
					$BackgroundColor = "p-3 mb-2 bg-light text-dark";
				}
			?>
				<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
					<input type="text" name="barcode<?php echo $i;?>" id="barcode<?php echo $i;?>" onfocus="GetName(<?php echo $i;?>)" onblur="GetName(<?php echo $i;?>)" class="form-control" value="">
				</div>
				<div class="col-lg-8 col-md-6 col-sm-12 <?php echo $BackgroundColor;?>">
					<input type="text" name="title<?php echo $i;?>" id="title<?php echo $i;?>" class="form-control" value="">
				</div>
				<div class="col-lg-2 col-md-2 col-sm-12 <?php echo $BackgroundColor;?>">
					<input type="number" min="0" step="1" name="quantity<?php echo $i;?>" id="quantity<?php echo $i;?>" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();" class="form-control" value="0">
					<input type="hidden" min="0" step="1" name="unitprice<?php echo $i;?>" id="unitprice<?php echo $i;?>" onchange="SubValue(); AllTotal(); Remaing();" onblur="SubValue(); AllTotal(); Remaing();" onkeyup="SubValue(); AllTotal(); Remaing();" class="form-control" value="0">
					<input type="hidden" name="subtotal<?php echo $i;?>" id="subtotal<?php echo $i;?>">
					<input type="hidden" name="location_id<?php echo $i;?>" value="0">
				</div>
			<?php
			}
			?>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php //echo $id;?>">
					<input type="hidden" name="deleted" value="0">
					<input type="hidden" name="tree_id" value="0">
					<input type="hidden" min="0" name="paid" id="paid" onchange="Remaing();" onblur="Remaing();" onkeyup="Remaing();" value="0">
					<input type="hidden" min="0" name="remaining" id="remaining" value="">
					<input type="hidden" name="RowsCount" id="RowsCount" value="<?php echo $InvoiceItems;?>">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>
</div>

								