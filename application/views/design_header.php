<div id="kt_header" class="header">
	<!--begin::Container-->
	<div class="container-fluid d-flex align-items-stretch justify-content-between">
		<!--begin::Header Menu Wrapper-->
		<div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
			<!--begin::Header Menu-->
			<div id="kt_header_menu" class="header-menu header-menu-mobile header-menu-layout-default">
				<!--begin::Header Nav-->
				<ul class="menu-nav">
					<li class="menu-item menu-item-open menu-item-here menu-item-submenu menu-item-rel menu-item-open menu-item-here menu-item-active" data-menu-toggle="click" aria-haspopup="true">
						<a href="javascript:;" class="menu-link menu-toggle">
							<span class="menu-text"><?php echo $this->lang->line('ShortCut');?></span>
							<i class="menu-arrow"></i>
						</a>
						<div class="menu-submenu menu-submenu-classic menu-submenu-left">
							<ul class="menu-subnav subnav-no-bg">
								<?php
								$GroupID = intval($this->session->userdata('GroupID'));
								$CheckView = intval($this->M_usr_usersprivileges->CheckView($GroupID, 9));
								if($CheckView > 0) {
								?>
								<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
									<a href="javascript:;" class="menu-link menu-toggle">
										<span class="svg-icon menu-icon">
											<!--begin::Svg Icon | path:assets/media/svg/icons/Code/CMD.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z" fill="#000000" opacity="0.3" transform="translate(11.500000, 12.000000) rotate(-345.000000) translate(-11.500000, -12.000000) "/>
													<path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z M11.5,14 C12.6045695,14 13.5,13.1045695 13.5,12 C13.5,10.8954305 12.6045695,10 11.5,10 C10.3954305,10 9.5,10.8954305 9.5,12 C9.5,13.1045695 10.3954305,14 11.5,14 Z" fill="#000000"/>
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_account');?></span>
										<i class="menu-arrow"></i>
									</a>
									<div class="menu-submenu menu-submenu-classic menu-submenu-right">
										<ul class="menu-subnav subnav-no-bg">
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>/account/mon_exchange_bonds/insertform" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_account_mon_exchange_bonds');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/mon_receipts/insertform" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_account_mon_receipts');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/ban_exchange_bonds/insertform" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_account_ban_exchange_bonds');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/ban_receipts/insertform" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_account_ban_receipts');?></span>
												</a>
											</li>
										</ul>
									</div>
								</li>
								<?php } ?>

								<?php
								$CheckView = intval($this->M_usr_usersprivileges->CheckView($GroupID, 16));
								if($CheckView > 0) {
								?>
								<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
									<a href="#" class="menu-link menu-toggle">
										<span class="svg-icon menu-icon">
											<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-box.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M18.1446364,11.84388 L17.4471627,16.0287218 C17.4463569,16.0335568 17.4455155,16.0383857 17.4446387,16.0432083 C17.345843,16.5865846 16.8252597,16.9469884 16.2818833,16.8481927 L4.91303792,14.7811299 C4.53842737,14.7130189 4.23500006,14.4380834 4.13039941,14.0719812 L2.30560137,7.68518803 C2.28007524,7.59584656 2.26712532,7.50338343 2.26712532,7.4104669 C2.26712532,6.85818215 2.71484057,6.4104669 3.26712532,6.4104669 L16.9929851,6.4104669 L17.606173,3.78251876 C17.7307772,3.24850086 18.2068633,2.87071314 18.7552257,2.87071314 L20.8200821,2.87071314 C21.4717328,2.87071314 22,3.39898039 22,4.05063106 C22,4.70228173 21.4717328,5.23054898 20.8200821,5.23054898 L19.6915238,5.23054898 L18.1446364,11.84388 Z" fill="#000000" opacity="0.3"/>
													<path d="M6.5,21 C5.67157288,21 5,20.3284271 5,19.5 C5,18.6715729 5.67157288,18 6.5,18 C7.32842712,18 8,18.6715729 8,19.5 C8,20.3284271 7.32842712,21 6.5,21 Z M15.5,21 C14.6715729,21 14,20.3284271 14,19.5 C14,18.6715729 14.6715729,18 15.5,18 C16.3284271,18 17,18.6715729 17,19.5 C17,20.3284271 16.3284271,21 15.5,21 Z" fill="#000000"/>
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_purchase');?></span>
										<i class="menu-arrow"></i>
									</a>
									<div class="menu-submenu menu-submenu-classic menu-submenu-right">
										<ul class="menu-subnav subnav-no-bg">
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>purchase/buy_bill/insertform" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_purchase_buy');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>purchase/buy_returns/insertform" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_purchase_buy_return');?></span>
												</a>
											</li>
										</ul>
									</div>
								</li>
								<?php } ?>

								<?php
								$CheckView = intval($this->M_usr_usersprivileges->CheckView($GroupID, 16));
								if($CheckView > 0) {
								?>
								<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
									<a href="#" class="menu-link menu-toggle">
										<span class="svg-icon menu-icon">
											<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-box.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M18.1446364,11.84388 L17.4471627,16.0287218 C17.4463569,16.0335568 17.4455155,16.0383857 17.4446387,16.0432083 C17.345843,16.5865846 16.8252597,16.9469884 16.2818833,16.8481927 L4.91303792,14.7811299 C4.53842737,14.7130189 4.23500006,14.4380834 4.13039941,14.0719812 L2.30560137,7.68518803 C2.28007524,7.59584656 2.26712532,7.50338343 2.26712532,7.4104669 C2.26712532,6.85818215 2.71484057,6.4104669 3.26712532,6.4104669 L16.9929851,6.4104669 L17.606173,3.78251876 C17.7307772,3.24850086 18.2068633,2.87071314 18.7552257,2.87071314 L20.8200821,2.87071314 C21.4717328,2.87071314 22,3.39898039 22,4.05063106 C22,4.70228173 21.4717328,5.23054898 20.8200821,5.23054898 L19.6915238,5.23054898 L18.1446364,11.84388 Z" fill="#000000" opacity="0.3"/>
													<path d="M6.5,21 C5.67157288,21 5,20.3284271 5,19.5 C5,18.6715729 5.67157288,18 6.5,18 C7.32842712,18 8,18.6715729 8,19.5 C8,20.3284271 7.32842712,21 6.5,21 Z M15.5,21 C14.6715729,21 14,20.3284271 14,19.5 C14,18.6715729 14.6715729,18 15.5,18 C16.3284271,18 17,18.6715729 17,19.5 C17,20.3284271 16.3284271,21 15.5,21 Z" fill="#000000"/>
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_sales');?></span>
										<i class="menu-arrow"></i>
									</a>
									<div class="menu-submenu menu-submenu-classic menu-submenu-right">
										<ul class="menu-subnav subnav-no-bg">
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>sales/sale_bill/insertform" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_sales_add');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>sales/sale_returns/insertform" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_sales_return');?></span>
												</a>
											</li>
										</ul>
									</div>
								</li>
								<?php } ?>

								<?php
								$CheckView = intval($this->M_usr_usersprivileges->CheckView($GroupID, 42));
								if($CheckView > 0) {
								?>
								<!--
								<li class="menu-item" aria-haspopup="true">
									<a href="<?php //echo base_url();?>project/proj_project" class="menu-link">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M14.8520384,9 L15.7780576,12 L8.22196243,12 L9.14797495,9 L14.8520384,9 Z M13.9260192,6 L10.0739875,6 L10.7050601,3.95551581 C10.8804029,3.38745846 11.4054966,3 12,3 C12.5945036,3 13.1195978,3.38745798 13.2949418,3.95551522 L13.9260192,6 Z M16.7040768,15 L17.9387691,19 L6.06126654,19 L7.2959499,15 L16.7040768,15 Z" fill="#000000"/>
													<rect fill="#000000" opacity="0.3" x="3" y="20" width="18" height="2" rx="1"/>
												</g>
											</svg>
										</span>
										<span class="menu-text"><?php //echo $this->lang->line('ShortCut_project');?></span>
									</a>
								</li>
								-->
								<?php } ?>
							</ul>
						</div>
					</li>
					<!--Start::Statistics-->
					<?php
					$ActiveMenu = "";
					$Segment1 = $this->uri->segment(1);
					if(($Segment1 == "") || ($Segment1 == "erp"))
					{
						$ActiveMenu = "";
					}
					else
					{
						$ActiveMenu = "menu-item-open menu-item-here menu-item-active";
					}
					?>
					<li class="menu-item menu-item-submenu menu-item-rel <?php echo $ActiveMenu;?>" data-menu-toggle="click" aria-haspopup="true">
						<a href="javascript:;" class="menu-link menu-toggle">
							<span class="menu-text"><?php echo $this->lang->line('ShortCut_Statistics');?></span>
							<i class="menu-arrow"></i>
						</a>
						<div class="menu-submenu menu-submenu-classic menu-submenu-left">
							<ul class="menu-subnav subnav-no-bg">
								<?php
								$GroupID = intval($this->session->userdata('GroupID'));
								if($GroupID == 1) {
								?>
								<li class="menu-item" data-menu-toggle="hover" aria-haspopup="true">
									<a href="<?php echo base_url();?>statistics_account" class="menu-link">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z" fill="#000000" opacity="0.3" transform="translate(11.500000, 12.000000) rotate(-345.000000) translate(-11.500000, -12.000000) "/>
													<path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z M11.5,14 C12.6045695,14 13.5,13.1045695 13.5,12 C13.5,10.8954305 12.6045695,10 11.5,10 C10.3954305,10 9.5,10.8954305 9.5,12 C9.5,13.1045695 10.3954305,14 11.5,14 Z" fill="#000000"/>
												</g>
											</svg>
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Statistics_Account');?></span>
									</a>
								</li>
								<li class="menu-item" data-menu-toggle="hover" aria-haspopup="true">
									<a href="<?php echo base_url();?>statistics_purchase" class="menu-link">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M18.1446364,11.84388 L17.4471627,16.0287218 C17.4463569,16.0335568 17.4455155,16.0383857 17.4446387,16.0432083 C17.345843,16.5865846 16.8252597,16.9469884 16.2818833,16.8481927 L4.91303792,14.7811299 C4.53842737,14.7130189 4.23500006,14.4380834 4.13039941,14.0719812 L2.30560137,7.68518803 C2.28007524,7.59584656 2.26712532,7.50338343 2.26712532,7.4104669 C2.26712532,6.85818215 2.71484057,6.4104669 3.26712532,6.4104669 L16.9929851,6.4104669 L17.606173,3.78251876 C17.7307772,3.24850086 18.2068633,2.87071314 18.7552257,2.87071314 L20.8200821,2.87071314 C21.4717328,2.87071314 22,3.39898039 22,4.05063106 C22,4.70228173 21.4717328,5.23054898 20.8200821,5.23054898 L19.6915238,5.23054898 L18.1446364,11.84388 Z" fill="#000000" opacity="0.3"/>
													<path d="M6.5,21 C5.67157288,21 5,20.3284271 5,19.5 C5,18.6715729 5.67157288,18 6.5,18 C7.32842712,18 8,18.6715729 8,19.5 C8,20.3284271 7.32842712,21 6.5,21 Z M15.5,21 C14.6715729,21 14,20.3284271 14,19.5 C14,18.6715729 14.6715729,18 15.5,18 C16.3284271,18 17,18.6715729 17,19.5 C17,20.3284271 16.3284271,21 15.5,21 Z" fill="#000000"/>
												</g>
											</svg>
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Statistics_Purchase');?></span>
									</a>
								</li>
								<li class="menu-item" data-menu-toggle="hover" aria-haspopup="true">
									<a href="<?php echo base_url();?>statistics_sales" class="menu-link">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<polygon fill="#000000" opacity="0.3" points="12 20.0218549 8.47346039 21.7286168 6.86905972 18.1543453 3.07048824 17.1949849 4.13894342 13.4256452 1.84573388 10.2490577 5.08710286 8.04836581 5.3722735 4.14091196 9.2698837 4.53859595 12 1.72861679 14.7301163 4.53859595 18.6277265 4.14091196 18.9128971 8.04836581 22.1542661 10.2490577 19.8610566 13.4256452 20.9295118 17.1949849 17.1309403 18.1543453 15.5265396 21.7286168"/>
													<polygon fill="#000000" points="14.0890818 8.60255815 8.36079737 14.7014391 9.70868621 16.049328 15.4369707 9.950447"/>
													<path d="M10.8543431,9.1753866 C10.8543431,10.1252593 10.085524,10.8938719 9.13585777,10.8938719 C8.18793881,10.8938719 7.41737243,10.1252593 7.41737243,9.1753866 C7.41737243,8.22551387 8.18793881,7.45690126 9.13585777,7.45690126 C10.085524,7.45690126 10.8543431,8.22551387 10.8543431,9.1753866" fill="#000000" opacity="0.3"/>
													<path d="M14.8641422,16.6221564 C13.9162233,16.6221564 13.1456569,15.8535438 13.1456569,14.9036711 C13.1456569,13.9520555 13.9162233,13.1851857 14.8641422,13.1851857 C15.8138085,13.1851857 16.5826276,13.9520555 16.5826276,14.9036711 C16.5826276,15.8535438 15.8138085,16.6221564 14.8641422,16.6221564 Z" fill="#000000" opacity="0.3"/>
												</g>
											</svg>
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Statistics_Sales');?></span>
									</a>
								</li>
								<li class="menu-item" data-menu-toggle="hover" aria-haspopup="true">
									<a href="<?php echo base_url();?>statistics_store" class="menu-link">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M3.5,3 L9.5,3 C10.3284271,3 11,3.67157288 11,4.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L3.5,20 C2.67157288,20 2,19.3284271 2,18.5 L2,4.5 C2,3.67157288 2.67157288,3 3.5,3 Z M9,9 C8.44771525,9 8,9.44771525 8,10 L8,12 C8,12.5522847 8.44771525,13 9,13 C9.55228475,13 10,12.5522847 10,12 L10,10 C10,9.44771525 9.55228475,9 9,9 Z" fill="#000000" opacity="0.3"/>
													<path d="M14.5,3 L20.5,3 C21.3284271,3 22,3.67157288 22,4.5 L22,18.5 C22,19.3284271 21.3284271,20 20.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,4.5 C13,3.67157288 13.6715729,3 14.5,3 Z M20,9 C19.4477153,9 19,9.44771525 19,10 L19,12 C19,12.5522847 19.4477153,13 20,13 C20.5522847,13 21,12.5522847 21,12 L21,10 C21,9.44771525 20.5522847,9 20,9 Z" fill="#000000" transform="translate(17.500000, 11.500000) scale(-1, 1) translate(-17.500000, -11.500000) "/>
												</g>
											</svg>
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Statistics_Store');?></span>
									</a>
								</li>
								<li class="menu-item" data-menu-toggle="hover" aria-haspopup="true">
									<a href="<?php echo base_url();?>statistics_hr" class="menu-link">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<polygon points="0 0 24 0 24 24 0 24"/>
													<path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
													<path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
												</g>
											</svg>
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Statistics_HR');?></span>
									</a>
								</li>
								<?php
								if($this->session->userdata('PackageName') == "erp")
								{
								?>
								<li class="menu-item" data-menu-toggle="hover" aria-haspopup="true">
									<a href="<?php echo base_url();?>statistics_manufacturing" class="menu-link">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M6.2,9.73333333 L8.7,6.4 C8.88885438,6.14819416 9.1852427,6 9.5,6 L14.5,6 C14.8147573,6 15.1111456,6.14819416 15.3,6.4 L17.8,9.73333333 C17.9298221,9.9064295 18,10.1169631 18,10.3333333 L18,21 C18,22.1045695 17.1045695,23 16,23 L8,23 C6.8954305,23 6,22.1045695 6,21 L6,10.3333333 C6,10.1169631 6.07017787,9.9064295 6.2,9.73333333 Z M9,12 C8.44771525,12 8,12.4477153 8,13 L8,20 C8,20.5522847 8.44771525,21 9,21 L10,21 C10.5522847,21 11,20.5522847 11,20 L11,13 C11,12.4477153 10.5522847,12 10,12 L9,12 Z" fill="#000000"/>
													<rect fill="#000000" opacity="0.3" x="9" y="1" width="6" height="3" rx="1"/>
												</g>
											</svg>
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Statistics_Manufacturing');?></span>
									</a>
								</li>

								<li class="menu-item" data-menu-toggle="hover" aria-haspopup="true">
									<a href="<?php echo base_url();?>statistics_service" class="menu-link">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"></rect>
													<path d="M15.9497475,3.80761184 L13.0246125,6.73274681 C12.2435639,7.51379539 12.2435639,8.78012535 13.0246125,9.56117394 L14.4388261,10.9753875 C15.2198746,11.7564361 16.4862046,11.7564361 17.2672532,10.9753875 L20.1923882,8.05025253 C20.7341101,10.0447871 20.2295941,12.2556873 18.674559,13.8107223 C16.8453326,15.6399488 14.1085592,16.0155296 11.8839934,14.9444337 L6.75735931,20.0710678 C5.97631073,20.8521164 4.70998077,20.8521164 3.92893219,20.0710678 C3.1478836,19.2900192 3.1478836,18.0236893 3.92893219,17.2426407 L9.05556629,12.1160066 C7.98447038,9.89144078 8.36005124,7.15466739 10.1892777,5.32544095 C11.7443127,3.77040588 13.9552129,3.26588995 15.9497475,3.80761184 Z" fill="#000000"></path>
													<path d="M16.6568542,5.92893219 L18.0710678,7.34314575 C18.4615921,7.73367004 18.4615921,8.36683502 18.0710678,8.75735931 L16.6913928,10.1370344 C16.3008685,10.5275587 15.6677035,10.5275587 15.2771792,10.1370344 L13.8629656,8.7228208 C13.4724413,8.33229651 13.4724413,7.69913153 13.8629656,7.30860724 L15.2426407,5.92893219 C15.633165,5.5384079 16.26633,5.5384079 16.6568542,5.92893219 Z" fill="#000000" opacity="0.3"></path>
												</g>
											</svg>
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Statistics_Service');?></span>
									</a>
								</li>
								<li class="menu-item" data-menu-toggle="hover" aria-haspopup="true">
									<a href="<?php echo base_url();?>statistics_technical" class="menu-link">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M21.9999843,15.009808 L22.0249378,15 L22.0249378,19.5857864 C22.0249378,20.1380712 21.5772226,20.5857864 21.0249378,20.5857864 C20.7597213,20.5857864 20.5053674,20.4804296 20.317831,20.2928932 L18.0249378,18 L6,18 C4.34314575,18 3,16.6568542 3,15 L3,6 C3,4.34314575 4.34314575,3 6,3 L19,3 C20.6568542,3 22,4.34314575 22,6 L22,15 C22,15.0032706 21.9999948,15.0065399 21.9999843,15.009808 Z" fill="#000000" opacity="0.3"/>
													<path d="M7.5,12 C6.67157288,12 6,11.3284271 6,10.5 C6,9.67157288 6.67157288,9 7.5,9 C8.32842712,9 9,9.67157288 9,10.5 C9,11.3284271 8.32842712,12 7.5,12 Z M12.5,12 C11.6715729,12 11,11.3284271 11,10.5 C11,9.67157288 11.6715729,9 12.5,9 C13.3284271,9 14,9.67157288 14,10.5 C14,11.3284271 13.3284271,12 12.5,12 Z M17.5,12 C16.6715729,12 16,11.3284271 16,10.5 C16,9.67157288 16.6715729,9 17.5,9 C18.3284271,9 19,9.67157288 19,10.5 C19,11.3284271 18.3284271,12 17.5,12 Z" fill="#000000" opacity="0.3"/>
												</g>
											</svg>
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Statistics_Technical');?></span>
									</a>
								</li>
								<?php
								}
								?>
								<?php } ?>
							</ul>
						</div>
					</li>
					<!--end::Statistics-->
					<!-- Start::Reports-->
					<li class="menu-item menu-item-open menu-item-here menu-item-submenu menu-item-rel menu-item-open menu-item-here menu-item-active" data-menu-toggle="click" aria-haspopup="true">
						<a href="javascript:;" class="menu-link menu-toggle">
							<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports');?></span>
							<i class="menu-arrow"></i>
						</a>
						<div class="menu-submenu menu-submenu-classic menu-submenu-left">
							<ul class="menu-subnav subnav-no-bg">
								<?php
								$GroupID = intval($this->session->userdata('GroupID'));
								$CheckView = intval($this->M_usr_usersprivileges->CheckView($GroupID, 9));
								if($CheckView > 0) {
								?>
								<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
									<a href="javascript:;" class="menu-link menu-toggle">
										<span class="svg-icon menu-icon">
											<!--begin::Svg Icon | path:assets/media/svg/icons/Code/CMD.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z" fill="#000000" opacity="0.3" transform="translate(11.500000, 12.000000) rotate(-345.000000) translate(-11.500000, -12.000000) "/>
													<path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z M11.5,14 C12.6045695,14 13.5,13.1045695 13.5,12 C13.5,10.8954305 12.6045695,10 11.5,10 C10.3954305,10 9.5,10.8954305 9.5,12 C9.5,13.1045695 10.3954305,14 11.5,14 Z" fill="#000000"/>
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Account');?></span>
										<i class="menu-arrow"></i>
									</a>
									<div class="menu-submenu menu-submenu-classic menu-submenu-right" style="min-width:300px;">
										<ul class="menu-subnav subnav-no-bg">
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/account_1" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Account_1');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/account_2" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Account_2');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/account_3" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Account_3');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/account_4" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Account_4');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/account_5" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Account_5');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/account_6" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Account_6');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/account_7" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Account_7');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/account_8" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Account_8');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/account_9" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Account_9');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/account_10" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Account_10');?></span>
												</a>
											</li>
										</ul>
									</div>
								</li>
								<?php } ?>

								<?php
								$CheckView = intval($this->M_usr_usersprivileges->CheckView($GroupID, 16));
								if($CheckView > 0) {
								?>
								<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
									<a href="javascript:;" class="menu-link menu-toggle">
										<span class="svg-icon menu-icon">
											<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-box.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M18.1446364,11.84388 L17.4471627,16.0287218 C17.4463569,16.0335568 17.4455155,16.0383857 17.4446387,16.0432083 C17.345843,16.5865846 16.8252597,16.9469884 16.2818833,16.8481927 L4.91303792,14.7811299 C4.53842737,14.7130189 4.23500006,14.4380834 4.13039941,14.0719812 L2.30560137,7.68518803 C2.28007524,7.59584656 2.26712532,7.50338343 2.26712532,7.4104669 C2.26712532,6.85818215 2.71484057,6.4104669 3.26712532,6.4104669 L16.9929851,6.4104669 L17.606173,3.78251876 C17.7307772,3.24850086 18.2068633,2.87071314 18.7552257,2.87071314 L20.8200821,2.87071314 C21.4717328,2.87071314 22,3.39898039 22,4.05063106 C22,4.70228173 21.4717328,5.23054898 20.8200821,5.23054898 L19.6915238,5.23054898 L18.1446364,11.84388 Z" fill="#000000" opacity="0.3"/>
													<path d="M6.5,21 C5.67157288,21 5,20.3284271 5,19.5 C5,18.6715729 5.67157288,18 6.5,18 C7.32842712,18 8,18.6715729 8,19.5 C8,20.3284271 7.32842712,21 6.5,21 Z M15.5,21 C14.6715729,21 14,20.3284271 14,19.5 C14,18.6715729 14.6715729,18 15.5,18 C16.3284271,18 17,18.6715729 17,19.5 C17,20.3284271 16.3284271,21 15.5,21 Z" fill="#000000"/>
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Purchase');?></span>
										<i class="menu-arrow"></i>
									</a>
									<div class="menu-submenu menu-submenu-classic menu-submenu-right">
										<ul class="menu-subnav subnav-no-bg">
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/purchase_1" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Purchase_1');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/purchase_2" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Purchase_2');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/purchase_3" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Purchase_3');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/purchase_4" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Purchase_4');?></span>
												</a>
											</li>
											<!--
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php //echo base_url();?>account/reports/purchase_5" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php //echo $this->lang->line('ShortCut_Reports_Purchase_5');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php //echo base_url();?>account/reports/purchase_6" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php //echo $this->lang->line('ShortCut_Reports_Purchase_6');?></span>
												</a>
											</li>
											-->
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/purchase_7" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Purchase_7');?></span>
												</a>
											</li>
											<!--
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php //echo base_url();?>account/reports/purchase_8" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php //echo $this->lang->line('ShortCut_Reports_Purchase_8');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php //echo base_url();?>account/reports/purchase_9" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php //echo $this->lang->line('ShortCut_Reports_Purchase_9');?></span>
												</a>
											</li>
											-->
										</ul>
									</div>
								</li>
								<?php } ?>

								<?php
								if($GroupID == 1) {
								?>
								<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
									<a href="javascript:;" class="menu-link menu-toggle">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<rect x="0" y="0" width="24" height="24"/>
												<polygon fill="#000000" opacity="0.3" points="12 20.0218549 8.47346039 21.7286168 6.86905972 18.1543453 3.07048824 17.1949849 4.13894342 13.4256452 1.84573388 10.2490577 5.08710286 8.04836581 5.3722735 4.14091196 9.2698837 4.53859595 12 1.72861679 14.7301163 4.53859595 18.6277265 4.14091196 18.9128971 8.04836581 22.1542661 10.2490577 19.8610566 13.4256452 20.9295118 17.1949849 17.1309403 18.1543453 15.5265396 21.7286168"/>
												<polygon fill="#000000" points="14.0890818 8.60255815 8.36079737 14.7014391 9.70868621 16.049328 15.4369707 9.950447"/>
												<path d="M10.8543431,9.1753866 C10.8543431,10.1252593 10.085524,10.8938719 9.13585777,10.8938719 C8.18793881,10.8938719 7.41737243,10.1252593 7.41737243,9.1753866 C7.41737243,8.22551387 8.18793881,7.45690126 9.13585777,7.45690126 C10.085524,7.45690126 10.8543431,8.22551387 10.8543431,9.1753866" fill="#000000" opacity="0.3"/>
												<path d="M14.8641422,16.6221564 C13.9162233,16.6221564 13.1456569,15.8535438 13.1456569,14.9036711 C13.1456569,13.9520555 13.9162233,13.1851857 14.8641422,13.1851857 C15.8138085,13.1851857 16.5826276,13.9520555 16.5826276,14.9036711 C16.5826276,15.8535438 15.8138085,16.6221564 14.8641422,16.6221564 Z" fill="#000000" opacity="0.3"/>
											</g>
											</svg>
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Sales');?></span>
										<i class="menu-arrow"></i>
									</a>
									<div class="menu-submenu menu-submenu-classic menu-submenu-right">
										<ul class="menu-subnav subnav-no-bg">
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/sales_1" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Sales_1');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/sales_2" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Sales_2');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/sales_3" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Sales_3');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/sales_4" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Sales_4');?></span>
												</a>
											</li>

											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/sales_5" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Sales_5');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/sales_6" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Sales_6');?></span>
												</a>
											</li>

											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/sales_7" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Sales_7');?></span>
												</a>
											</li>
											<!--
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php //echo base_url();?>account/reports/sales_8" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php //echo $this->lang->line('ShortCut_Reports_Sales_8');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php //echo base_url();?>account/reports/sales_9" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php //echo $this->lang->line('ShortCut_Reports_Sales_9');?></span>
												</a>
											</li>
											-->
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/sales_10" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Sales_10');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/sales_11" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Sales_11');?></span>
												</a>
											</li>
										</ul>
									</div>
								</li>
								<?php } ?>

								<?php
								if($GroupID == 1) {
								?>
								<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
									<a href="javascript:;" class="menu-link menu-toggle">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M3.5,3 L9.5,3 C10.3284271,3 11,3.67157288 11,4.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L3.5,20 C2.67157288,20 2,19.3284271 2,18.5 L2,4.5 C2,3.67157288 2.67157288,3 3.5,3 Z M9,9 C8.44771525,9 8,9.44771525 8,10 L8,12 C8,12.5522847 8.44771525,13 9,13 C9.55228475,13 10,12.5522847 10,12 L10,10 C10,9.44771525 9.55228475,9 9,9 Z" fill="#000000" opacity="0.3"/>
													<path d="M14.5,3 L20.5,3 C21.3284271,3 22,3.67157288 22,4.5 L22,18.5 C22,19.3284271 21.3284271,20 20.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,4.5 C13,3.67157288 13.6715729,3 14.5,3 Z M20,9 C19.4477153,9 19,9.44771525 19,10 L19,12 C19,12.5522847 19.4477153,13 20,13 C20.5522847,13 21,12.5522847 21,12 L21,10 C21,9.44771525 20.5522847,9 20,9 Z" fill="#000000" transform="translate(17.500000, 11.500000) scale(-1, 1) translate(-17.500000, -11.500000) "/>
												</g>
											</svg>
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Store');?></span>
										<i class="menu-arrow"></i>
									</a>
									<div class="menu-submenu menu-submenu-classic menu-submenu-right">
										<ul class="menu-subnav subnav-no-bg">
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/store_1" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Store_1');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/store_2" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Store_2');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/store_3" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Store_3');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/store_4" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Store_4');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/store_5" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Store_5');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/store_6" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Store_6');?></span>
												</a>
											</li>
										</ul>
									</div>
								</li>
								<?php } ?>

								<?php
								if($GroupID == 1) {
								?>
								<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
									<a href="javascript:;" class="menu-link menu-toggle">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<polygon points="0 0 24 0 24 24 0 24"/>
													<path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
													<path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
												</g>
											</svg>
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_HR');?></span>
										<i class="menu-arrow"></i>
									</a>
									<div class="menu-submenu menu-submenu-classic menu-submenu-right">
										<ul class="menu-subnav subnav-no-bg">
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/hr_1" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_HR_1');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/hr_2" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_HR_2');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/hr_3" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_HR_3');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>account/reports/hr_4" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_HR_4');?></span>
												</a>
											</li>
										</ul>
									</div>
								</li>
								<?php } ?>

								<?php
								if($GroupID == 1) {
									if($this->session->userdata('PackageName') == "erp")
									{
								?>
								<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
									<a href="<?php echo base_url();?>account/reports/manufacturing" class="menu-link">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M6.2,9.73333333 L8.7,6.4 C8.88885438,6.14819416 9.1852427,6 9.5,6 L14.5,6 C14.8147573,6 15.1111456,6.14819416 15.3,6.4 L17.8,9.73333333 C17.9298221,9.9064295 18,10.1169631 18,10.3333333 L18,21 C18,22.1045695 17.1045695,23 16,23 L8,23 C6.8954305,23 6,22.1045695 6,21 L6,10.3333333 C6,10.1169631 6.07017787,9.9064295 6.2,9.73333333 Z M9,12 C8.44771525,12 8,12.4477153 8,13 L8,20 C8,20.5522847 8.44771525,21 9,21 L10,21 C10.5522847,21 11,20.5522847 11,20 L11,13 C11,12.4477153 10.5522847,12 10,12 L9,12 Z" fill="#000000"/>
													<rect fill="#000000" opacity="0.3" x="9" y="1" width="6" height="3" rx="1"/>
												</g>
											</svg>
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Manufacturing');?></span>
									</a>
								</li>
								<?php
									}
								}
								?>

								<?php
								if($GroupID == 1) {
									if($this->session->userdata('PackageName') == "erp")
									{
								?>
								<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
									<a href="javascript:;" class="menu-link menu-toggle">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"></rect>
													<path d="M15.9497475,3.80761184 L13.0246125,6.73274681 C12.2435639,7.51379539 12.2435639,8.78012535 13.0246125,9.56117394 L14.4388261,10.9753875 C15.2198746,11.7564361 16.4862046,11.7564361 17.2672532,10.9753875 L20.1923882,8.05025253 C20.7341101,10.0447871 20.2295941,12.2556873 18.674559,13.8107223 C16.8453326,15.6399488 14.1085592,16.0155296 11.8839934,14.9444337 L6.75735931,20.0710678 C5.97631073,20.8521164 4.70998077,20.8521164 3.92893219,20.0710678 C3.1478836,19.2900192 3.1478836,18.0236893 3.92893219,17.2426407 L9.05556629,12.1160066 C7.98447038,9.89144078 8.36005124,7.15466739 10.1892777,5.32544095 C11.7443127,3.77040588 13.9552129,3.26588995 15.9497475,3.80761184 Z" fill="#000000"></path>
													<path d="M16.6568542,5.92893219 L18.0710678,7.34314575 C18.4615921,7.73367004 18.4615921,8.36683502 18.0710678,8.75735931 L16.6913928,10.1370344 C16.3008685,10.5275587 15.6677035,10.5275587 15.2771792,10.1370344 L13.8629656,8.7228208 C13.4724413,8.33229651 13.4724413,7.69913153 13.8629656,7.30860724 L15.2426407,5.92893219 C15.633165,5.5384079 16.26633,5.5384079 16.6568542,5.92893219 Z" fill="#000000" opacity="0.3"></path>
												</g>
											</svg>
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Service');?></span>
										<i class="menu-arrow"></i>
									</a>
									<div class="menu-submenu menu-submenu-classic menu-submenu-right">
										<ul class="menu-subnav subnav-no-bg">
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>maintenance/data_request" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Service_1');?></span>
												</a>
											</li>
											<li class="menu-item" aria-haspopup="true">
												<a href="<?php echo base_url();?>maintenance/data_invoice" class="menu-link">
													<i class="menu-bullet menu-bullet-dot">
														<span></span>
													</i>
													<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Service_2');?></span>
												</a>
											</li>
										</ul>
									</div>
								</li>
								<?php
									}
								}
								?>

								<?php
								if($GroupID == 1) {
									if($this->session->userdata('PackageName') == "erp")
									{
								?>
								<li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
									<a href="<?php echo base_url();?>support/tic_ticket" class="menu-link">
										<span class="svg-icon menu-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M21.9999843,15.009808 L22.0249378,15 L22.0249378,19.5857864 C22.0249378,20.1380712 21.5772226,20.5857864 21.0249378,20.5857864 C20.7597213,20.5857864 20.5053674,20.4804296 20.317831,20.2928932 L18.0249378,18 L6,18 C4.34314575,18 3,16.6568542 3,15 L3,6 C3,4.34314575 4.34314575,3 6,3 L19,3 C20.6568542,3 22,4.34314575 22,6 L22,15 C22,15.0032706 21.9999948,15.0065399 21.9999843,15.009808 Z" fill="#000000" opacity="0.3"/>
													<path d="M7.5,12 C6.67157288,12 6,11.3284271 6,10.5 C6,9.67157288 6.67157288,9 7.5,9 C8.32842712,9 9,9.67157288 9,10.5 C9,11.3284271 8.32842712,12 7.5,12 Z M12.5,12 C11.6715729,12 11,11.3284271 11,10.5 C11,9.67157288 11.6715729,9 12.5,9 C13.3284271,9 14,9.67157288 14,10.5 C14,11.3284271 13.3284271,12 12.5,12 Z M17.5,12 C16.6715729,12 16,11.3284271 16,10.5 C16,9.67157288 16.6715729,9 17.5,9 C18.3284271,9 19,9.67157288 19,10.5 C19,11.3284271 18.3284271,12 17.5,12 Z" fill="#000000" opacity="0.3"/>
												</g>
											</svg>
										</span>
										<span class="menu-text"><?php echo $this->lang->line('ShortCut_Reports_Technical');?></span>
									</a>
								</li>
								<?php
									}
								}
								?>

							</ul>
						</div>
					</li>
					<!--end::Reports-->
				</ul>
				<!--end::Header Nav-->
			</div>
			<!--end::Header Menu-->
		</div>
		<!--end::Header Menu Wrapper-->
		<!--begin::Topbar-->
		<div class="topbar">
			<!--begin::Notifications-->
			<?php //include("design_panel_notifications.php");?>
			<!--end::Notifications-->
			<!--begin::Quick Actions-->
			<?php //include("design_panel_quick_actions.php");?>
			<!--end::Quick Actions-->
			<!--begin::Quick panel-->

			<!--
			<div class="topbar-item">
				<div class="btn btn-icon btn-clean btn-lg mr-1" id="kt_quick_panel_toggle">
					<span class="svg-icon svg-icon-xl svg-icon-primary">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect x="0" y="0" width="24" height="24" />
								<rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5" />
								<path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3" />
							</g>
						</svg>
					</span>
				</div>
			</div>
			-->

			<!--end::Quick panel-->
			<!--begin::Chat-->

			<!--
			<div class="topbar-item">
				<div class="btn btn-icon btn-clean btn-lg mr-1" data-toggle="modal" data-target="#kt_chat_modal">
					<span class="svg-icon svg-icon-xl svg-icon-primary">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect x="0" y="0" width="24" height="24" />
								<path d="M16,15.6315789 L16,12 C16,10.3431458 14.6568542,9 13,9 L6.16183229,9 L6.16183229,5.52631579 C6.16183229,4.13107011 7.29290239,3 8.68814808,3 L20.4776218,3 C21.8728674,3 23.0039375,4.13107011 23.0039375,5.52631579 L23.0039375,13.1052632 L23.0206157,17.786793 C23.0215995,18.0629336 22.7985408,18.2875874 22.5224001,18.2885711 C22.3891754,18.2890457 22.2612702,18.2363324 22.1670655,18.1421277 L19.6565168,15.6315789 L16,15.6315789 Z" fill="#000000" />
								<path d="M1.98505595,18 L1.98505595,13 C1.98505595,11.8954305 2.88048645,11 3.98505595,11 L11.9850559,11 C13.0896254,11 13.9850559,11.8954305 13.9850559,13 L13.9850559,18 C13.9850559,19.1045695 13.0896254,20 11.9850559,20 L4.10078614,20 L2.85693427,21.1905292 C2.65744295,21.3814685 2.34093638,21.3745358 2.14999706,21.1750444 C2.06092565,21.0819836 2.01120804,20.958136 2.01120804,20.8293182 L2.01120804,18.32426 C1.99400175,18.2187196 1.98505595,18.1104045 1.98505595,18 Z M6.5,14 C6.22385763,14 6,14.2238576 6,14.5 C6,14.7761424 6.22385763,15 6.5,15 L11.5,15 C11.7761424,15 12,14.7761424 12,14.5 C12,14.2238576 11.7761424,14 11.5,14 L6.5,14 Z M9.5,16 C9.22385763,16 9,16.2238576 9,16.5 C9,16.7761424 9.22385763,17 9.5,17 L11.5,17 C11.7761424,17 12,16.7761424 12,16.5 C12,16.2238576 11.7761424,16 11.5,16 L9.5,16 Z" fill="#000000" opacity="0.3" />
							</g>
						</svg>
					</span>
				</div>
			</div>
			-->


			<!--end::Chat-->
			<!--begin::Languages-->
			<div class="dropdown">
				<!--begin::Toggle-->
				<div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
					<div class="btn btn-icon btn-clean btn-dropdown btn-lg mr-1">
						<img class="h-20px w-20px rounded-sm" src="<?php echo base_url();?>assets/media/svg/flags/226-united-states.svg" alt="" />
					</div>
				</div>
				<!--end::Toggle-->
				<!--begin::Dropdown-->
				<div class="dropdown-menu p-0 m-0 dropdown-menu-anim-up dropdown-menu-sm dropdown-menu-right">
					<!--begin::Nav-->
					<ul class="navi navi-hover py-4">
						<!--begin::Item-->
						<li class="navi-item">
							<a href="<?php echo base_url()."home/ChangeLanguage/en";?>" class="navi-link">
								<span class="symbol symbol-20 mr-3">
									<img src="<?php echo base_url();?>assets/media/svg/flags/226-united-states.svg" alt="English" />
								</span>
								<span class="navi-text">English</span>
							</a>
						</li>
						<!--end::Item-->
						<!--begin::Item-->
						<li class="navi-item active">
							<a href="<?php echo base_url()."home/ChangeLanguage/ar";?>" class="navi-link">
								<span class="symbol symbol-20 mr-3">
									<img src="<?php echo base_url();?>assets/media/svg/flags/133-saudi-arabia.svg" alt="عربي" />
								</span>
								<span class="navi-text">عربي</span>
							</a>
						</li>
						<!--end::Item-->
					</ul>
					<!--end::Nav-->
				</div>
				<!--end::Dropdown-->
			</div>
			<!--end::Languages-->
			<!--begin::User-->
			<div class="topbar-item">
				<?php
                $StaffID = intval($this->session->userdata('StaffID'));
                $StaffData = $this->M_usr_users->GetRow($StaffID);
                ?>
				<div class="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
					<span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1"><?php echo $this->lang->line('Welcome');?>,</span>
					<span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3"><?php echo $StaffData->fullname;?></span>
					<span class="symbol symbol-lg-35 symbol-25 symbol-light-success">
						<span class="symbol-label font-size-h5 font-weight-bold"><?php echo mb_substr($StaffData->fullname, 0, 1,'utf-8');?></span>
					</span>
				</div>
			</div>
			<!--end::User-->
		</div>
		<!--end::Topbar-->
	</div>
	<!--end::Container-->
</div>
