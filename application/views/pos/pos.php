<!DOCTYPE html>
<html lang="ar" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="aTjFfssViOb8VRyNR3Q9c39GhKTghwwVp0J3QKjk">

    <title>نقطة بيع - باديا العرب لتقنية المعلومات</title>

    <link rel="stylesheet" href="<?= base_url('assets/pos/css/vendor.css') ?>">

    <link rel="stylesheet" href="<?= base_url('assets/pos/css/rtl.css?v=44') ?>">

    <!-- include module css -->

    <!-- app css -->
    <link rel="stylesheet" href="<?= base_url('assets/pos/css/app.css?v=44') ?>">

    <style type="text/css">
        .content {
            padding-bottom: 0px !important;
        }
    </style>
    <style type="text/css">
        .patt-wrap {
            z-index: 10;
        }

        .patt-circ.hovered {
            background-color: #cde2f2;
            border: none;
        }

        .patt-circ.hovered .patt-dots {
            display: none;
        }

        .patt-circ.dir {
            background-image: url("http://ultimatepos.2030saudivision.com/img/pattern-directionicon-arrow.png");
            background-position: center;
            background-repeat: no-repeat;
        }

        .patt-circ.e {
            -webkit-transform: rotate(0);
            transform: rotate(0);
        }

        .patt-circ.s-e {
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        .patt-circ.s {
            -webkit-transform: rotate(90deg);
            transform: rotate(90deg);
        }

        .patt-circ.s-w {
            -webkit-transform: rotate(135deg);
            transform: rotate(135deg);
        }

        .patt-circ.w {
            -webkit-transform: rotate(180deg);
            transform: rotate(180deg);
        }

        .patt-circ.n-w {
            -webkit-transform: rotate(225deg);
            transform: rotate(225deg);
        }

        .patt-circ.n {
            -webkit-transform: rotate(270deg);
            transform: rotate(270deg);
        }

        .patt-circ.n-e {
            -webkit-transform: rotate(315deg);
            transform: rotate(315deg);
        }
    </style>

    <!-- include module css -->
</head>

<body class=" hold-transition lockscreen ">
    <div class="wrapper thetop">
        <script type="text/javascript">
            if (localStorage.getItem("upos_sidebar_collapse") == 'true') {
                var body = document.getElementsByTagName("body")[0];
                body.className += " sidebar-collapse";
            }
        </script>
        <!-- default value -->

        <input type="hidden" name="transaction_sub_type" id="transaction_sub_type" value="">
        <div class="col-md-12 no-print pos-header">
            <input type="hidden" id="pos_redirect_url" value="<?= base_url() ?>/pos/pos/create">
            <div class="row">
                <div class="col-md-6">
                    <div class="m-6 mt-5" style="display: flex;">
                        <p>
                            &nbsp; <?= date('Y-m-d h:i A') ?>
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <a href="<?= base_url() ?>" title="رجوع" class="btn btn-info btn-flat m-6 btn-xs m-5 pull-right">
                        <strong><i class="fa fa-backward fa-lg"></i></strong>
                    </a>
                    <!-- <button type="button" id="close_register" title="انهاء الوردية" class="btn btn-danger btn-flat m-6 btn-xs m-5 btn-modal pull-right" data-container=".close_register_modal" data-href="<?= base_url() ?>/cash-register/close-register">
                        <strong><i class="fa fa-window-close fa-lg"></i></strong>
                    </button> -->

                    <!-- <button type="button" id="register_details" title="تفاصيل الوردية" class="btn btn-success btn-flat m-6 btn-xs m-5 btn-modal pull-right" data-container=".register_details_modal" data-href="<?= base_url() ?>/cash-register/register-details">
                        <strong><i class="fa fa-briefcase fa-lg" aria-hidden="true"></i></strong>
                    </button> -->

                    <button title="آلة حاسبة" id="btnCalculator" type="button" class="btn btn-success btn-flat pull-right m-5 btn-xs mt-10 popover-default" data-toggle="popover" data-trigger="click" data-content='<div id="calculator">
  <div class="row text-center" id="calc">
    <div class="calcBG col-md-12 text-center">
      <div class="row" id="result">
        <form name="calc">
          <input type="text" class="screen text-right" name="result" readonly>
        </form>
      </div>
      <div class="row">
        <button id="allClear" type="button" class="btn btn-danger" onclick="clearScreen()">AC</button>
        <button id="clear" type="button" class="btn btn-warning" onclick="clearScreen()">CE</button>
        <button id="%" type="button" class="btn" onclick="calEnterVal(this.id)">%</button>
        <button id="/" type="button" class="btn" onclick="calEnterVal(this.id)">÷</button>
      </div>
      <div class="row">
        <button id="7" type="button" class="btn" onclick="calEnterVal(this.id)">7</button>
        <button id="8" type="button" class="btn" onclick="calEnterVal(this.id)">8</button>
        <button id="9" type="button" class="btn" onclick="calEnterVal(this.id)">9</button>
        <button id="*" type="button" class="btn" onclick="calEnterVal(this.id)">x</button>
      </div>
      <div class="row">
        <button id="4" type="button" class="btn" onclick="calEnterVal(this.id)">4</button>
        <button id="5" type="button" class="btn" onclick="calEnterVal(this.id)">5</button>
        <button id="6" type="button" class="btn" onclick="calEnterVal(this.id)">6</button>
        <button id="-" type="button" class="btn" onclick="calEnterVal(this.id)">-</button>
      </div>
      <div class="row">
        <button id="1" type="button" class="btn" onclick="calEnterVal(this.id)">1</button>
        <button id="2" type="button" class="btn" onclick="calEnterVal(this.id)">2</button>
        <button id="3" type="button" class="btn" onclick="calEnterVal(this.id)">3</button>
        <button id="+" type="button" class="btn" onclick="calEnterVal(this.id)">+</button>
      </div>
      <div class="row">
        <button id="0" type="button" class="btn" onclick="calEnterVal(this.id)">0</button>
        <button id="." type="button" class="btn" onclick="calEnterVal(this.id)">.</button>
        <button id="equals" type="button" class="btn btn-success" onclick="calculate()">=</button>
        <button id="blank" type="button" class="btn">&nbsp;</button>
      </div>
    </div>
  </div>
</div>' data-html="true" data-placement="bottom">
                        <strong><i class="fa fa-calculator fa-lg" aria-hidden="true"></i></strong>
                    </button>

                    <button type="button" title="شاشة كاملة" class="btn btn-primary btn-flat m-6 hidden-xs btn-xs m-5 pull-right" id="full_screen">
                        <strong><i class="fa fa-window-maximize fa-lg"></i></strong>
                    </button>

                    <!-- <button type="button" id="view_suspended_sales" title="عرض المبيعات المعلقه" class="btn bg-yellow btn-flat m-6 btn-xs m-5 btn-modal pull-right" data-container=".view_modal" data-href="<?= base_url() ?>/sells?suspended=1">
                        <strong><i class="fa fa-pause-circle fa-lg"></i></strong>
                    </button> -->



                </div>

            </div>
        </div>


        <!-- Content Wrapper. Contains page content -->
        <div class="">
            <!-- empty div for vuejs -->
            <div id="app">
            </div>
            <!-- Add currency related field-->
            <!-- End of currency related field-->

            <section class="content no-print">
                <input type="hidden" id="amount_rounding_method" value="">
                <form method="POST" action="<?= base_url() ?>/pos/pos/create" id="add_pos_sell_form">
                    <div class="row mb-12">
                        <div class="col-md-12">
                            <div class="row">
                                <div class=" col-md-7  no-padding pr-12">
                                    <div class="box box-solid mb-12">
                                        <div class="box-body pb-0">
                                            <input id="location_id" data-receipt_printer_type="browser" data-default_payment_accounts="{&quot;cash&quot;:{&quot;is_enabled&quot;:1,&quot;account&quot;:null},&quot;card&quot;:{&quot;is_enabled&quot;:1,&quot;account&quot;:null},&quot;cheque&quot;:{&quot;is_enabled&quot;:1,&quot;account&quot;:null},&quot;bank_transfer&quot;:{&quot;is_enabled&quot;:1,&quot;account&quot;:null},&quot;other&quot;:{&quot;is_enabled&quot;:1,&quot;account&quot;:null},&quot;custom_pay_1&quot;:{&quot;is_enabled&quot;:1,&quot;account&quot;:null},&quot;custom_pay_2&quot;:{&quot;is_enabled&quot;:1,&quot;account&quot;:null},&quot;custom_pay_3&quot;:{&quot;is_enabled&quot;:1,&quot;account&quot;:null},&quot;custom_pay_4&quot;:{&quot;is_enabled&quot;:1,&quot;account&quot;:null},&quot;custom_pay_5&quot;:{&quot;is_enabled&quot;:1,&quot;account&quot;:null},&quot;custom_pay_6&quot;:{&quot;is_enabled&quot;:1,&quot;account&quot;:null},&quot;custom_pay_7&quot;:{&quot;is_enabled&quot;:1,&quot;account&quot;:null}}" name="location_id" type="hidden" value="1">
                                            <!-- sub_type -->
                                            <input name="sub_type" type="hidden">
                                            <input type="hidden" id="item_addition_method" value="1">
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-btn">
                                                                <button type="button" class="btn btn-default bg-white btn-flat" data-toggle="modal" data-target="#configure_search_modal" title="اعدادت البحث عن المنتج"><i class="fas fa-search-plus"></i></button>
                                                            </div>
                                                            <input class="form-control mousetrap" id="search_product" placeholder="أدخل اسم المنتج / SKU / مسح الباركود" autofocus name="search_product" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <input type="hidden" name="pay_term_number" id="pay_term_number" value="">
                                                <input type="hidden" name="pay_term_type" id="pay_term_type" value="">

                                                <input id="price_group" name="price_group" type="hidden" value="0">


                                                <!-- Call restaurant module if defined -->

                                            </div>
                                            <!-- include module fields -->
                                            <div class="row">
                                                <div class="col-sm-12 pos_product_div">
                                                    <input type="hidden" name="sell_price_tax" id="sell_price_tax" value="includes">

                                                    <!-- Keeps count of product rows -->
                                                    <input type="hidden" id="product_row_count" value="0">
                                                    <table class="table table-condensed table-bordered table-striped table-responsive" id="pos_table">
                                                        <thead>
                                                            <tr>
                                                                <th class="tex-center  col-md-4 ">
                                                                    منتج <i class="fa fa-info-circle text-info hover-q no-print " aria-hidden="true" data-container="body" data-toggle="popover" data-placement="auto bottom" data-content="انقر <i>اسم المنتج</i> لتحرير السعر والخصم والضرائب. <br/>انقر <i>أيقونة التعليق</i> لإدخال الرقم التسلسلي / IMEI أو ملاحظة إضافية.<br/><br/>انقر <i>رمز التعديل</i>(إذا تم تمكين) الاضافات" data-html="true" data-trigger="hover"></i> </th>
                                                                <th class="text-center col-md-3">
                                                                    الكمية </th>
                                                                <th class="text-center col-md-2 hide">
                                                                    السعر شامل الضريبة </th>
                                                                <th class="text-center col-md-2">
                                                                    المجموع </th>
                                                                <th class="text-center"><i class="fas fa-times" aria-hidden="true"></i></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="row pos_form_totals">
                                                <div class="col-md-12">
                                                    <table class="table table-condensed">
                                                        <tr>
                                                            <td><b>عناصر:</b>&nbsp;
                                                                <span class="total_quantity">0</span>
                                                            </td>
                                                            <td>
                                                                <b>المجموع:</b> &nbsp;
                                                                <span class="price_total">0</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <b>
                                                                    الخصم <i class="fa fa-info-circle text-info hover-q no-print " aria-hidden="true" data-container="body" data-toggle="popover" data-placement="auto bottom" data-content="قم بتعيين الخصم على البيع الافتراضي لجميع المبيعات في إعدادات الشركة .اضغط على أيقونة التعديل أدناه لإضافة / تحديث الخصم." data-html="true" data-trigger="hover"></i> (-):
                                                                    <i class="fas fa-edit cursor-pointer" id="pos-edit-discount" title="تعديل الخصم" aria-hidden="true" data-toggle="modal" data-target="#posEditDiscountModal"></i>
                                                                    <span id="total_discount">0</span>
                                                                    <input type="hidden" name="discount_type" id="discount_type" value="percentage" data-default="percentage">

                                                                    <input type="hidden" name="discount_amount" id="discount_amount" value=" 0.00 " data-default="0.00">

                                                                    <input type="hidden" name="rp_redeemed" id="rp_redeemed" value="0">

                                                                    <input type="hidden" name="rp_redeemed_amount" id="rp_redeemed_amount" value="0">

                                                                    </span>
                                                                </b>
                                                            </td>
                                                            <td class="">
                                                                <span>
                                                                    <b>ضريبة الطلبية(+): <i class="fa fa-info-circle text-info hover-q no-print " aria-hidden="true" data-container="body" data-toggle="popover" data-placement="auto bottom" data-content="عيّن ضريبة المبيعات الافتراضية لجميع المبيعات في إعدادات الشركة ، انقر على رمز التعديل أدناه لإضافة / تحديث ضريبة الطلبات. " data-html="true" data-trigger="hover"></i></b>
                                                                    <i class="fas fa-edit cursor-pointer" title="تعديل ضريبة الطلبية" aria-hidden="true" data-toggle="modal" data-target="#posEditOrderTaxModal" id="pos-edit-tax"></i>
                                                                    <span id="order_tax">
                                                                        0
                                                                    </span>

                                                                    <input type="hidden" name="tax_rate_id" id="tax_rate_id" value="  " data-default="">

                                                                    <input type="hidden" name="tax_calculation_amount" id="tax_calculation_amount" value=" 0.00 " data-default="">

                                                                </span>
                                                            </td>
                                                            <td class="">
                                                                <span>

                                                                    <b>شحن(+): <i class="fa fa-info-circle text-info hover-q no-print " aria-hidden="true" data-container="body" data-toggle="popover" data-placement="auto bottom" data-content="Set shipping details and shipping charges. Click on the edit icon below to add/update shipping details and charges." data-html="true" data-trigger="hover"></i></b>
                                                                    <i class="fas fa-edit cursor-pointer" title="شحن" aria-hidden="true" data-toggle="modal" data-target="#posShippingModal"></i>
                                                                    <span id="shipping_charges_amount">0</span>
                                                                    <input type="hidden" name="shipping_details" id="shipping_details" value="" data-default="">

                                                                    <input type="hidden" name="shipping_address" id="shipping_address" value="">

                                                                    <input type="hidden" name="shipping_status" id="shipping_status" value="">

                                                                    <input type="hidden" name="delivered_to" id="delivered_to" value="">

                                                                    <input type="hidden" name="shipping_charges" id="shipping_charges" value="0.00 " data-default="0.00">
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal fade" tabindex="-1" role="dialog" id="modal_payment">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title">دفع</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12 mb-12">
                                                                    <strong>الرصيد المسبق:</strong> <span id="advance_balance_text"></span>
                                                                    <input id="advance_balance" data-error-msg="الرصيد المقدم المطلوب غير متوفر" name="advance_balance" type="hidden">
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div class="row">
                                                                        <div id="payment_rows_div">


                                                                            <div class="col-md-12">
                                                                                <div class="box box-solid payment_row bg-lightgray">


                                                                                    <div class="box-body">
                                                                                        <div class="row">
                                                                                            <input type="hidden" class="payment_row_index" value="0">
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label for="amount_0">المبلغ:*</label>
                                                                                                    <div class="input-group">
                                                                                                        <span class="input-group-addon">
                                                                                                            <i class="fas fa-money-bill-alt"></i>
                                                                                                        </span>
                                                                                                        <input class="form-control payment-amount input_number" required id="amount_0" placeholder="المبلغ" name="payment_amount" type="text" value="0.00">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label for="method_0">طريقة الدفع:*</label>
                                                                                                    <div class="input-group">
                                                                                                        <span class="input-group-addon">
                                                                                                            <i class="fas fa-money-bill-alt"></i>
                                                                                                        </span>
                                                                                                        <select class="form-control col-md-12 payment_types_dropdown" required id="method_0" style="width:100%;" name="payment_method">
                                                                                                            <option value="advance">الدفع المسبق</option>
                                                                                                            <option value="cash" selected="selected">نقدا</option>
                                                                                                            <option value="card">بطاقة</option>
                                                                                                            <option value="cheque">شيك مصرفي</option>
                                                                                                            <option value="bank_transfer">تحويل مصرفي</option>
                                                                                                            <option value="other">آخر</option>
                                                                                                            <!-- <option value="custom_pay_1">الدفع المخصص 1</option>
                                                                                                            <option value="custom_pay_2">الدفع المخصص 2</option>
                                                                                                            <option value="custom_pay_3">الدفع المخصص 3</option>
                                                                                                            <option value="custom_pay_4">الدفع المخصص 4</option>
                                                                                                            <option value="custom_pay_5">الدفع المخصص 5</option>
                                                                                                            <option value="custom_pay_6">الدفع المخصص 6</option>
                                                                                                            <option value="custom_pay_7">الدفع المخصص 7</option> -->
                                                                                                        </select>

                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                         
                                                                                            <div class="clearfix"></div>
                                                                                            <div class="payment_details_div  hide " data-type="card">
                                                                                               
                                                                                                <div class="clearfix"></div>
                                                                                                <div class="col-md-12">
                                                                                                    <div class="form-group">
                                                                                                        <label for="card_type_0">نوع البطاقة</label>
                                                                                                        <select class="form-control" id="card_type_0" name="payment_card_type">
                                                                                                            <option value="credit">Credit Card</option>
                                                                                                            <option value="debit">Debit Card</option>
                                                                                                            <option value="visa">Visa</option>
                                                                                                            <option value="master">MasterCard</option>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                       
                                                                                                <div class="clearfix"></div>
                                                                                            </div>
                                                                                            <div class="payment_details_div  hide " data-type="cheque">
                                                                                                <div class="col-md-12">
                                                                                                    <div class="form-group">
                                                                                                        <label for="cheque_number_0">رقم الشيك.</label>
                                                                                                        <input class="form-control" placeholder="رقم الشيك." id="cheque_number_0" name="payment_cheque_number" type="text" value="">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="payment_details_div  hide " data-type="bank_transfer">
                                                                                                <div class="col-md-12">
                                                                                                    <div class="form-group">
                                                                                                        <label for="bank_account_number_0">رقم الحساب البنكي</label>
                                                                                                        <input class="form-control" placeholder="رقم الحساب البنكي" id="bank_account_number_0" name="payment_bank_account_number" type="text" value="">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="col-md-12">
                                                                                                <div class="form-group">
                                                                                                    <label for="note_0">ملاحظة الدفع:</label>
                                                                                                    <textarea class="form-control" rows="3" id="note_0" name="payment_note" cols="50"></textarea>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" id="payment_row_index" value="1">
                                                                    </div>
                                                                    <!-- <div class="row">
                                                                        <div class="col-md-12">
                                                                            <button type="button" class="btn btn-primary btn-block" id="add-payment-row">أضف صف دفع</button>
                                                                        </div>
                                                                    </div> -->
                                                                    <br>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="sale_note">ملاحظة البيع:</label>
                                                                                <textarea class="form-control" rows="3" placeholder="ملاحظة البيع" name="sale_note" cols="50" id="sale_note"></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="staff_note">ملاحظة الموظفين:</label>
                                                                                <textarea class="form-control" rows="3" placeholder="ملاحظة الموظفين" name="staff_note" cols="50" id="staff_note"></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="box box-solid bg-orange">
                                                                        <div class="box-body">
                                                                            <div class="col-md-12">
                                                                                <strong>
                                                                                    مجموع العناصر:
                                                                                </strong>
                                                                                <br />
                                                                                <span class="lead text-bold total_quantity">0</span>
                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <hr>
                                                                                <strong>
                                                                                    الاجمالى:
                                                                                </strong>
                                                                                <br />
                                                                                <span class="lead text-bold total_payable_span">0</span>
                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <hr>
                                                                                <strong>
                                                                                    المدفوع:
                                                                                </strong>
                                                                                <br />
                                                                                <span class="lead text-bold total_paying">0</span>
                                                                                <input type="hidden" id="total_paying_input">
                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <hr>
                                                                                <strong>
                                                                                    الباقى:
                                                                                </strong>
                                                                                <br />
                                                                                <span class="lead text-bold change_return_span">0</span>
                                                                                <input class="form-control change_return input_number" required id="change_return" name="change_return" type="hidden" value="0">
                                                                                <!-- <span class="lead text-bold total_quantity">0</span> -->
                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <hr>
                                                                                <strong>
                                                                                    الرصيد:
                                                                                </strong>
                                                                                <br />
                                                                                <span class="lead text-bold balance_due">0</span>
                                                                                <input type="hidden" id="in_balance_due" value=0>
                                                                            </div>



                                                                        </div>
                                                                        <!-- /.box-body -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                                                            <button type="submit" class="btn btn-primary" id="pos-save">انهاء المبيعة</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->

                                            <!-- Used for express checkout card transaction -->
                                            
                                            <div class="modal fade" tabindex="-1" role="dialog" id="confirmSuspendModal">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title">تعليق بيع</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div class="form-group">
                                                                        <label for="additional_notes">ملاحظة التعليق:</label>
                                                                        <textarea class="form-control" rows="4" name="additional_notes" cols="50" id="additional_notes"></textarea>
                                                                        <input id="is_suspend" name="is_suspend" type="hidden" value="0">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" id="pos-suspend">حفظ</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->
                                            <!-- Edit discount Modal -->
                                            <div class="modal fade" tabindex="-1" role="dialog" id="recurringInvoiceModal">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title">اشتراك </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="recur_interval">فترة الاشتراك:*</label>
                                                                        <div class="input-group">
                                                                            <input class="form-control" required style="width: 50%;" name="recur_interval" type="number" id="recur_interval">

                                                                            <select class="form-control" required style="width: 50%;" id="recur_interval_type" name="recur_interval_type">
                                                                                <option value="days" selected="selected">الأيام</option>
                                                                                <option value="months">الأشهر</option>
                                                                                <option value="years">سنوات</option>
                                                                            </select>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="recur_repetitions">عدد التكرار:</label>
                                                                        <input class="form-control" name="recur_repetitions" type="number" id="recur_repetitions">
                                                                        <p class="help-block">إذا تم إنشاء فاتورة فارغة مرات لا حصر لها</p>
                                                                    </div>
                                                                </div>
                                                                <div class="subscription_repeat_on_div col-md-6  hide ">
                                                                    <div class="form-group">
                                                                        <label for="subscription_repeat_on">كرر على:</label>
                                                                        <select class="form-control" id="subscription_repeat_on" name="subscription_repeat_on">
                                                                            <option selected="selected" value="">يرجى الاختيار</option>
                                                                            <option value="1">1st</option>
                                                                            <option value="2">2nd</option>
                                                                            <option value="3">3rd</option>
                                                                            <option value="4">4th</option>
                                                                            <option value="5">5th</option>
                                                                            <option value="6">6th</option>
                                                                            <option value="7">7th</option>
                                                                            <option value="8">8th</option>
                                                                            <option value="9">9th</option>
                                                                            <option value="10">10th</option>
                                                                            <option value="11">11th</option>
                                                                            <option value="12">12th</option>
                                                                            <option value="13">13th</option>
                                                                            <option value="14">14th</option>
                                                                            <option value="15">15th</option>
                                                                            <option value="16">16th</option>
                                                                            <option value="17">17th</option>
                                                                            <option value="18">18th</option>
                                                                            <option value="19">19th</option>
                                                                            <option value="20">20th</option>
                                                                            <option value="21">21st</option>
                                                                            <option value="22">22nd</option>
                                                                            <option value="23">23rd</option>
                                                                            <option value="24">24th</option>
                                                                            <option value="25">25th</option>
                                                                            <option value="26">26th</option>
                                                                            <option value="27">27th</option>
                                                                            <option value="28">28th</option>
                                                                            <option value="29">29th</option>
                                                                            <option value="30">30th</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 no-padding">
                                    <div class="row" id="featured_products_box" style="display: none;">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4" id="product_category_div">
                                            <select class="select2" id="product_category" style="width:100% !important">

                                                <option value="all">جميع الاصناف</option>

                                                <?php foreach ($categories as $category) {
                                                    echo " <option value='$category->id'>$category->title</option>";
                                                } ?>
                                            </select>
                                        </div>

                                        <div class="col-sm-4" id="product_brand_div">
                                            <select id="product_brand" class="select2" name="size" style="width:100% !important">
                                                <option value="all">جميع العلامات التجارية</option>
                                                <?php foreach ($brandes as $brand) {
                                                    echo " <option value='$brand->id'>$brand->title</option>";
                                                } ?>
                                            </select>
                                        </div>

                                        <!-- used in repair : filter for service/product -->
                                        <div class="col-md-6 hide" id="product_service_div">
                                            <select id="is_enabled_stock" class="select2" name="is_enabled_stock" style="width:100% !important">
                                                <option value="" selected="selected">كل</option>
                                                <option value="product">منتج</option>
                                                <option value="service">الخدمات</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-4  hide " id="feature_product_div">
                                            <button type="button" class="btn btn-primary btn-flat" id="show_featured_products">منتجات مميزة</button>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <input type="hidden" id="suggestion_page" value="1">
                                        <div class="col-md-12">
                                            <div class="eq-height-row" id="product_list_body"></div>
                                        </div>
                                        <div class="col-md-12 text-center" id="suggestion_page_loader" style="display: none;">
                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="pos-form-actions">
                            <div class="col-md-12">
                                <button type="button" class=" btn bg-info text-white btn-default btn-flat " id="pos-draft"><i class="fas fa-edit"></i> مسودة</button>
                                <button type="button" class="btn btn-default bg-yellow btn-flat " id="pos-quotation"><i class="fas fa-edit"></i> بيان سعر</button>

                                <!-- <button type="button" class=" btn bg-red btn-default btn-flat no-print pos-express-finalize" data-pay_method="suspend" title="إيقاف المبيعات (إيقاف مؤقت)">
                                    <i class="fas fa-pause" aria-hidden="true"></i>
                                    تعليق </button> -->

                                <input type="hidden" name="is_credit_sale" value="0" id="is_credit_sale">
                                <!-- <button type="button" class="btn bg-purple btn-default btn-flat no-print pos-express-finalize " data-pay_method="credit_sale" title="السحب كبيع الائتمان">
                                    <i class="fas fa-check" aria-hidden="true"></i> بيع الائتمان </button>
                               -->

                                <button type="button" class="btn  btn-success   btn-flat no-print  " id="pos-finalize" title="التحصيل بطرق دفع متعددة"><i class="fas fa-money-check-alt" aria-hidden="true"></i> اتمام الطلب </button>

                                <!-- <button type="button" class="btn btn-success   btn-flat no-print  pos-express-finalize " data-pay_method="cash" title="حدد الدفع السريع"> <i class="fas fa-money-bill-alt" aria-hidden="true"></i> الدفع السريع</button> -->
                                &nbsp;&nbsp;
                                <b>الاجمالى:</b>
                                <input type="hidden" name="final_total" id="final_total_input" value=0>
                                <span id="total_payable" class="text-success lead text-bold">0</span>
                                &nbsp;&nbsp;
                                <button type="button" class="btn btn-danger btn-flat  btn-xs " id="pos-cancel"> <i class="fas fa-window-close"></i> إلغاء</button>

                                <button type="button" class="btn btn-primary btn-flat pull-right " data-toggle="modal" data-target="#recent_transactions_modal" id="recent-transactions"> <i class="fas fa-clock"></i> التحويلات الاخيرة</button>

                            </div>
                        </div>
                    </div>
                    <!-- Edit discount Modal -->
                    <div class="modal fade" tabindex="-1" role="dialog" id="posEditDiscountModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">
                                        الخصم </h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row ">
                                        <div class="col-md-12">
                                            <h4 class="modal-title">تعديل الخصم:</h4>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="discount_type_modal">نوع الخصم:*</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-info"></i>
                                                    </span>
                                                    <select class="form-control" required id="discount_type_modal" name="discount_type_modal">
                                                        <option value="">يرجى الاختيار</option>
                                                        <option value="fixed">ثابت</option>
                                                        <option value="percentage" selected="selected">النسبة المئوية</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="discount_amount_modal">مبلغ الخصم:*</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-info"></i>
                                                    </span>
                                                    <input class="form-control input_number" data-max-discount="" data-max-discount-error_msg="يمكنك إعطاء حد أقصى % خصم لكل عملية بيع" name="discount_amount_modal" type="text" value="0.00" id="discount_amount_modal">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row  hide ">
                                        <div class="well well-sm bg-light-gray col-md-12">
                                            <div class="col-md-12">
                                                <h4 class="modal-title">:</h4>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="rp_redeemed_modal">مستردة:</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-gift"></i>
                                                        </span>
                                                        <input class="form-control" data-amount_per_unit_point="1.0000" data-max_points="0" min="0" data-min_order_total="1.0000" name="rp_redeemed_modal" type="number" value="0" id="rp_redeemed_modal">
                                                        <input type="hidden" id="rp_name" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <p><strong>متاح:</strong> <span id="available_rp">0</span></p>
                                                <h5><strong>المبلغ المسترد:</strong> <span id="rp_redeemed_amount_text">0.00</span></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" id="posEditDiscountModalUpdate">تحديث</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">إلغاء</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    <!-- Edit Order tax Modal -->
                    <div class="modal fade" tabindex="-1" role="dialog" id="posEditOrderTaxModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">تعديل ضريبة الطلبية</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="order_tax_modal">ضريبة الطلبية:*</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-info"></i>
                                                    </span>
                                                    <select class="form-control" id="order_tax_modal" name="order_tax_modal">
                                                        <option selected="selected" value="">يرجى الاختيار</option>
                                                        <option value="" selected="selected">لا احد</option>
                                                        <option value="1" data-rate="15.0000">الضريبه المضافه على المبيعات</option>
                                                        <option value="2" data-rate="15.0000">test</option>
                                                        <option value="3" data-rate="15.0000">الضريبه المضافه على المشتريات</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" id="posEditOrderTaxModalUpdate">تحديث</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">إلغاء</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    <!-- Edit Shipping Modal -->
                    <div class="modal fade" tabindex="-1" role="dialog" id="posShippingModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">شحن</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="shipping_details_modal">تفاصيل الشحن:*</label>
                                                <textarea class="form-control" placeholder="تفاصيل الشحن" required rows="4" name="shipping_details_modal" cols="50" id="shipping_details_modal"></textarea>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="shipping_address_modal">عنوان الشحن:</label>
                                                <textarea class="form-control" placeholder="عنوان الشحن" rows="4" name="shipping_address_modal" cols="50" id="shipping_address_modal"></textarea>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="shipping_charges_modal">رسوم الشحن:*</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-info"></i>
                                                    </span>
                                                    <input class="form-control input_number" placeholder="رسوم الشحن" name="shipping_charges_modal" type="text" value="0" id="shipping_charges_modal">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="shipping_status_modal">حالة الشحن:</label>
                                                <select class="form-control" id="shipping_status_modal" name="shipping_status_modal">
                                                    <option selected="selected" value="">يرجى الاختيار</option>
                                                    <option value="ordered">تم الطلب</option>
                                                    <option value="packed">معباه</option>
                                                    <option value="shipped">شحنت</option>
                                                    <option value="delivered">تم التوصيل</option>
                                                    <option value="cancelled">ألغيت</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="delivered_to_modal">سلمت لـ:</label>
                                                <input class="form-control" placeholder="سلمت لـ" name="delivered_to_modal" type="text" id="delivered_to_modal">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" id="posShippingModalUpdate">تحديث</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">إلغاء</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </form>
            </section>

            <!-- This will be printed -->
            <section class="invoice print_section" id="receipt_section">
            </section>
            <div class="modal fade contact_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <form method="POST" action="<?= base_url() ?>/contacts" accept-charset="UTF-8" id="quick_add_contact"><input name="_token" type="hidden" value="aTjFfssViOb8VRyNR3Q9c39GhKTghwwVp0J3QKjk">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">اضافة جهة اتصال</h4>
                            </div>

                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-4 contact_type_div">
                                        <div class="form-group">
                                            <label for="type">نوع جهة الاتصال:*</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </span>
                                                <select class="form-control" id="contact_type" required name="type">
                                                    <option selected="selected" value="">يرجى الاختيار</option>
                                                    <option value="supplier">الموردين</option>
                                                    <option value="customer">العملاء</option>
                                                    <option value="both">مورد وعميل</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 mt-15">
                                        <label class="radio-inline">
                                            <input type="radio" name="contact_type_radio" id="inlineRadio1" value="individual">
                                            فردي </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="contact_type_radio" id="inlineRadio2" value="business">
                                            النشاط </label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="contact_id">معرف الاتصال:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-id-badge"></i>
                                                </span>
                                                <input class="form-control" placeholder="معرف الاتصال" name="contact_id" type="text" id="contact_id">
                                            </div>
                                            <p class="help-block">
                                                اتركه فارغًا للإنشاء التلقائي </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 customer_fields">
                                        <div class="form-group">
                                            <label for="customer_group_id">مجموعة العملاء:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-users"></i>
                                                </span>
                                                <select class="form-control" id="customer_group_id" name="customer_group_id">
                                                    <option value="" selected="selected">لا احد</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix customer_fields"></div>
                                    <div class="col-md-4 business" style="display: none;">
                                        <div class="form-group">
                                            <label for="supplier_business_name">اسم النشاط:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-briefcase"></i>
                                                </span>
                                                <input class="form-control" placeholder="اسم النشاط" name="supplier_business_name" type="text" id="supplier_business_name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="col-md-3 individual" style="display: none;">
                                        <div class="form-group">
                                            <label for="prefix">اللقب:</label>
                                            <input class="form-control" placeholder="السيد السيدة الآنسة" name="prefix" type="text" id="prefix">
                                        </div>
                                    </div>
                                    <div class="col-md-3 individual" style="display: none;">
                                        <div class="form-group">
                                            <label for="first_name">الاسم:*</label>
                                            <input class="form-control" required placeholder="الاسم" name="first_name" type="text" id="first_name">
                                        </div>
                                    </div>
                                    <div class="col-md-3 individual" style="display: none;">
                                        <div class="form-group">
                                            <label for="middle_name">الاسم الوسطى:</label>
                                            <input class="form-control" placeholder="الاسم الوسطى" name="middle_name" type="text" id="middle_name">
                                        </div>
                                    </div>
                                    <div class="col-md-3 individual" style="display: none;">
                                        <div class="form-group">
                                            <label for="last_name">اسم العائلة:</label>
                                            <input class="form-control" placeholder="اسم العائلة" name="last_name" type="text" id="last_name">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="mobile">الموبايل:*</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-mobile"></i>
                                                </span>
                                                <input class="form-control" required placeholder="الموبايل" name="mobile" type="text" id="mobile">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="alternate_number">الموبايل البديل:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                                <input class="form-control" placeholder="الموبايل البديل" name="alternate_number" type="text" id="alternate_number">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="landline">الهاتف:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                                <input class="form-control" placeholder="الهاتف" name="landline" type="text" id="landline">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="email">البريد الإلكتروني:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-envelope"></i>
                                                </span>
                                                <input class="form-control" placeholder="البريد الإلكتروني" name="email" type="email" id="email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-4 individual" style="display: none;">
                                        <div class="form-group">
                                            <label for="dob">تاريخ الولادة:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>

                                                <input class="form-control dob-date-picker" placeholder="تاريخ الولادة" readonly name="dob" type="text" id="dob">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- lead additional field -->
                                    <div class="col-md-4 lead_additional_div">
                                        <div class="form-group">
                                            <label for="crm_source">Source:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fas fa fa-search"></i>
                                                </span>
                                                <select class="form-control" id="crm_source" name="crm_source">
                                                    <option selected="selected" value="">يرجى الاختيار</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 lead_additional_div">
                                        <div class="form-group">
                                            <label for="crm_life_stage">Life Stage:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fas fa fa-life-ring"></i>
                                                </span>
                                                <select class="form-control" id="crm_life_stage" name="crm_life_stage">
                                                    <option selected="selected" value="">يرجى الاختيار</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 lead_additional_div">
                                        <div class="form-group">
                                            <label for="user_id">Assigned to:*</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </span>
                                                <select class="form-control select2" id="user_id" multiple required style="width: 100%;" name="user_id[]"></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-primary center-block more_btn" data-target="#more_div">مزيد من المعلومات <i class="fa fa-chevron-down"></i></button>
                                    </div>

                                    <div id="more_div" class="hide">
                                        <input id="position" name="position" type="hidden">
                                        <div class="col-md-12">
                                            <hr />
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="tax_number">الرقم الضريبي:</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-info"></i>
                                                    </span>
                                                    <input class="form-control" placeholder="الرقم الضريبي" name="tax_number" type="text" id="tax_number">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 opening_balance">
                                            <div class="form-group">
                                                <label for="opening_balance">الرصيد الافتتاحي:</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fas fa-money-bill-alt"></i>
                                                    </span>
                                                    <input class="form-control input_number" name="opening_balance" type="text" value="0" id="opening_balance">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 pay_term">
                                            <div class="form-group">
                                                <div class="multi-input">
                                                    <label for="pay_term_number">فترة الدفع:</label> <i class="fa fa-info-circle text-info hover-q no-print " aria-hidden="true" data-container="body" data-toggle="popover" data-placement="auto bottom" data-content="المدفوعات التي يتعين دفعها مقابل المشتريات في الفترة الزمنية المحددة. <br/> <small class = 'text-muted'> سيتم عرض جميع الدفعات المعلقة والحالية في الرئيسية - القسم المستحق للدفع </small>" data-html="true" data-trigger="hover"></i> <br />
                                                    <input class="form-control width-40 pull-left" placeholder="فترة الدفع" name="pay_term_number" type="number" id="pay_term_number">

                                                    <select class="form-control width-60 pull-left" name="pay_term_type">
                                                        <option selected="selected" value="">يرجى الاختيار</option>
                                                        <option value="months">الأشهر</option>
                                                        <option value="days">الأيام</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-4 customer_fields">
                                            <div class="form-group">
                                                <label for="credit_limit">الحد الائتماني:</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fas fa-money-bill-alt"></i>
                                                    </span>
                                                    <input class="form-control input_number" name="credit_limit" type="text" id="credit_limit">
                                                </div>
                                                <p class="help-block">تبقى فارغة من دون حدود</p>
                                            </div>
                                        </div>


                                        <div class="col-md-12">
                                            <hr />
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="address_line_1">العنوان الأول:</label>
                                                <input class="form-control" placeholder="العنوان الأول" rows="3" name="address_line_1" type="text" id="address_line_1">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="address_line_2">سطر العنوان 2:</label>
                                                <input class="form-control" placeholder="سطر العنوان 2" rows="3" name="address_line_2" type="text" id="address_line_2">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="city">المدينة:</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-map-marker"></i>
                                                    </span>
                                                    <input class="form-control" placeholder="المدينة" name="city" type="text" id="city">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="state">الولاية:</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-map-marker"></i>
                                                    </span>
                                                    <input class="form-control" placeholder="الولاية" name="state" type="text" id="state">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="country">البلد:</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-globe"></i>
                                                    </span>
                                                    <input class="form-control" placeholder="البلد" name="country" type="text" id="country">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="zip_code">الرمز البريدي:</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-map-marker"></i>
                                                    </span>
                                                    <input class="form-control" placeholder="الرمز البريدي" name="zip_code" type="text" id="zip_code">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <hr />
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="custom_field1">حقل مخصص 1:</label>
                                                <input class="form-control" placeholder="حقل مخصص 1" name="custom_field1" type="text" id="custom_field1">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="custom_field2">حقل مخصص 2:</label>
                                                <input class="form-control" placeholder="حقل مخصص 2" name="custom_field2" type="text" id="custom_field2">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="custom_field3">حقل مخصص 3:</label>
                                                <input class="form-control" placeholder="حقل مخصص 3" name="custom_field3" type="text" id="custom_field3">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="custom_field4">حقل مخصص 4:</label>
                                                <input class="form-control" placeholder="حقل مخصص 4" name="custom_field4" type="text" id="custom_field4">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="custom_field5">حقل مخصص 5:</label>
                                                <input class="form-control" placeholder="حقل مخصص 5" name="custom_field5" type="text" id="custom_field5">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="custom_field6">حقل مخصص 6:</label>
                                                <input class="form-control" placeholder="حقل مخصص 6" name="custom_field6" type="text" id="custom_field6">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="custom_field7">حقل مخصص 7:</label>
                                                <input class="form-control" placeholder="حقل مخصص 7" name="custom_field7" type="text" id="custom_field7">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="custom_field8">حقل مخصص 8:</label>
                                                <input class="form-control" placeholder="حقل مخصص 8" name="custom_field8" type="text" id="custom_field8">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="custom_field9">حقل مخصص 9:</label>
                                                <input class="form-control" placeholder="حقل مخصص 9" name="custom_field9" type="text" id="custom_field9">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="custom_field10">حقل مخصص 10:</label>
                                                <input class="form-control" placeholder="حقل مخصص 10" name="custom_field10" type="text" id="custom_field10">
                                            </div>
                                        </div>
                                        <div class="col-md-12 shipping_addr_div">
                                            <hr>
                                        </div>
                                        <div class="col-md-8 col-md-offset-2 shipping_addr_div mb-10">
                                            <strong>عنوان الشحن</strong><br>
                                            <input class="form-control" placeholder="عنوان البحث" id="shipping_address" name="shipping_address" type="text">
                                            <div class="mb-10" id="map"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">حفظ</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                            </div>

                        </form>

                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <!-- /.content -->
            <div class="modal fade register_details_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
            </div>
            <div class="modal fade close_register_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
            </div>

            <div class="modal fade" id="configure_search_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">
                                البحث عن منتجات بواسطة </h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input class="input-icheck search_fields" checked="checked" name="search_fields[]" type="checkbox" value="name"> اسم المنتج (التعيين) </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input class="input-icheck search_fields" checked="checked" name="search_fields[]" type="checkbox" value="sku"> SKU </label>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input class="input-icheck search_fields" name="search_fields[]" type="checkbox" value="product_custom_field1"> حقل مخصص 1
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input class="input-icheck search_fields" name="search_fields[]" type="checkbox" value="product_custom_field2"> حقل مخصص 2
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input class="input-icheck search_fields" name="search_fields[]" type="checkbox" value="product_custom_field3"> حقل مخصص 3
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input class="input-icheck search_fields" name="search_fields[]" type="checkbox" value="product_custom_field4"> حقل مخصص 4
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade no-print" id="recent_transactions_modal" tabindex="-1" role="dialog" aria-labelledby="modalTitle">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">التحويلات الاخيرة</h4>
                        </div>
                        <div class="modal-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_final" data-toggle="tab" aria-expanded="true"><b><i class="fa fa-check"></i> انهاء</b></a></li>

                                    <li class=""><a href="#tab_quotation" data-toggle="tab" aria-expanded="false"><b><i class="fa fa-terminal"></i> بيان سعر</b></a></li>

                                    <li class=""><a href="#tab_draft" data-toggle="tab" aria-expanded="false"><b><i class="fa fa-terminal"></i> مسودة</b></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_final">
                                    </div>
                                    <div class="tab-pane" id="tab_quotation">
                                    </div>
                                    <div class="tab-pane" id="tab_draft">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>


            <div class='scrolltop no-print'>
                <div class='scroll icon'><i class="fas fa-angle-up"></i></div>
            </div>


            <!-- This will be printed -->
            <section class="invoice print_section" id="receipt_section">
            </section>

        </div>
        <div class="modal fade" id="todays_profit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">ربح اليوم</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="modal_today" value="2021-11-18">
                        <div class="row">
                            <div id="todays_profit">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                    </div>
                </div>
            </div>
        </div> <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="no-print text-center text-info">
            <!-- To the right -->
            <!-- <div class="pull-right hidden-xs">
      Anything you want
    </div> -->
            <!-- Default to the left -->
            <small>
                <b>POS - V4.4 | Copyright &copy; 2021 All rights reserved.</b>
            </small>
        </footer>
        <audio id="success-audio">
            <source src="<?= base_url('assets') ?>/audio/success.ogg?v=44" type="audio/ogg">
            <source src="<?= base_url('assets') ?>/audio/success.mp3?v=44" type="audio/mpeg">
        </audio>
        <audio id="error-audio">
            <source src="<?= base_url('assets') ?>/audio/error.ogg?v=44" type="audio/ogg">
            <source src="<?= base_url('assets') ?>/audio/error.mp3?v=44" type="audio/mpeg">
        </audio>
        <audio id="warning-audio">
            <source src="<?= base_url('assets') ?>/audio/warning.ogg?v=44" type="audio/ogg">
            <source src="<?= base_url('assets') ?>/audio/warning.mp3?v=44" type="audio/mpeg">
        </audio>
    </div>


    <script type="text/javascript">
        base_path = "<?= base_url() ?>";
        //used for push notification
        APP = {};
        APP.PUSHER_APP_KEY = '';
        APP.PUSHER_APP_CLUSTER = '';
        APP.INVOICE_SCHEME_SEPARATOR = '-';
        //variable from app service provider
        APP.PUSHER_ENABLED = '';
        APP.USER_ID = "2";
    </script>

    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="<?= base_url('assets/pos/js/vendor.js') ?>"></script>

    <script src="<?= base_url('assets/pos/js/ar.js') ?>"></script>

    <script>
        moment.tz.setDefault('Asia/Riyadh');
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });

        var financial_year = {
            start: moment('2021-01-01'),
            end: moment('2021-12-31'),
        }

        var datepicker_date_format = "mm/dd/yyyy";
        var moment_date_format = "MM/DD/YYYY";
        var moment_time_format = "hh:mm A";

        var app_locale = "ar";

        var non_utf8_languages = [
            "ar",
            "hi",
            "ps",
        ];

        var __default_datatable_page_entries = "25";

        var __new_notification_count_interval = "60000";
    </script>


    <script src="<?= base_url('assets/pos') ?>/js/functions.js"></script>
    <script src="<?= base_url('assets/pos') ?>/js/common.js"></script>
    <script src="<?= base_url('assets/pos') ?>/js/app.js"></script>
    <script src="<?= base_url('assets/pos') ?>/js/help-tour.js"></script>
    <script src="<?= base_url('assets/pos') ?>/js/documents_and_note.js"></script>

    <!-- TODO -->

    <script src="<?= base_url('assets/pos') ?>/js/pos.js"></script>
    <script src="<?= base_url('assets/pos') ?>/js/printer.js"></script>
    <script src="<?= base_url('assets/pos') ?>/js/product.js"></script>
    <script src="<?= base_url('assets/pos') ?>/js/opening_stock.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            //shortcut for express checkout
            Mousetrap.bind('shift+e', function(e) {
                e.preventDefault();
                $('button.pos-express-finalize[data-pay_method="cash"]').trigger('click');
            });

            //shortcut for cancel checkout
            Mousetrap.bind('shift+c', function(e) {
                e.preventDefault();
                $('#pos-cancel').trigger('click');
            });

            //shortcut for draft checkout
            Mousetrap.bind('shift+d', function(e) {
                e.preventDefault();
                $('#pos-draft').trigger('click');
            });

            //shortcut for draft pay & checkout
            Mousetrap.bind('shift+p', function(e) {
                e.preventDefault();
                $('#pos-finalize').trigger('click');
            });

            //shortcut for edit discount
            Mousetrap.bind('shift+i', function(e) {
                e.preventDefault();
                $('#pos-edit-discount').trigger('click');
            });

            //shortcut for edit tax
            Mousetrap.bind('shift+t', function(e) {
                e.preventDefault();
                $('#pos-edit-tax').trigger('click');
            });

            //shortcut for add payment row
            var payment_modal = document.querySelector('#modal_payment');
            Mousetrap.bind('shift+r', function(e, combo) {
                if ($('#modal_payment').is(':visible')) {
                    e.preventDefault();
                    $('#add-payment-row').trigger('click');
                }
            });

            //shortcut for add finalize payment
            var payment_modal = document.querySelector('#modal_payment');
            Mousetrap(payment_modal).bind('shift+f', function(e, combo) {
                if ($('#modal_payment').is(':visible')) {
                    e.preventDefault();
                    $('#pos-save').trigger('click');
                }
            });

            //Shortcuts to go recent product quantity
            shortcut_length_prev = 0;
            shortcut_position_now = null;

            Mousetrap.bind('f2', function(e, combo) {
                var length_now = $('table#pos_table tr').length;

                if (length_now != shortcut_length_prev) {
                    shortcut_length_prev = length_now;
                    shortcut_position_now = length_now;
                } else {
                    shortcut_position_now = shortcut_position_now - 1;
                }

                var last_qty_field = $('table#pos_table tr').eq(shortcut_position_now - 1).contents().find('input.pos_quantity');
                if (last_qty_field.length >= 1) {
                    last_qty_field.focus().select();
                } else {
                    shortcut_position_now = length_now + 1;
                    Mousetrap.trigger('f2');
                }
            });

            //On focus of quantity field go back to search when stop typing
            var timeout = null;
            $('table#pos_table').on('focus', 'input.pos_quantity', function() {
                var that = this;

                $(this).on('keyup', function(e) {

                    if (timeout !== null) {
                        clearTimeout(timeout);
                    }

                    var code = e.keyCode || e.which;
                    if (code != '9') {
                        timeout = setTimeout(function() {
                            $('input#search_product').focus().select();
                        }, 5000);
                    }
                });
            });

            //shortcut to go to add new products
            Mousetrap.bind('f4', function(e) {
                $('input#search_product').focus().select();
            });

            //shortcut for weighing scale
        });
    </script>
    <!-- Call restaurant module if defined -->
    <!-- include module js -->


    <script type="text/javascript">
        $(document).ready(function() {
            var locale = "ar";
            var isRTL = true;
            $('#calendar').fullCalendar('option', {
                locale: locale,
                isRTL: isRTL
            });
        });
    </script>
    <div class="modal fade view_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel"></div>

</body>

</html>