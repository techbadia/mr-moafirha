<style>
    .card-toolbar {
    display: none !important;
}
</style>
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="DataTable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('product_barcode');?></th>
												<th><?php echo lang('product_category_id');?></th>
												<th><?php echo lang('product_brand_id');?></th>
												<th><?php echo lang('product_thetitle');?></th>
												<th><?php echo lang('buy_bill_items_quantity');?></th>
												<!-- <th><?php echo lang('product_cost_price');?></th> -->
												<th><?php echo lang('product_Price');?></th>
												<th><?php echo lang('product_lowest_price');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td><?php echo $DataRows_Row->barcode;?></td>
												<td>
													<?php
													$category_id = $DataRows_Row->category_id;
													$CategoryData = $this->M_store_category->GetRow($category_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $CategoryData->title;
													}
													else
													{
														echo $CategoryData->title_en;
													}
													?>
												</td>
												<td>
													<?php
													$brand_id = $DataRows_Row->brand_id;
													$BrandData = $this->M_store_brand->GetRow($brand_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $BrandData->title;
													}
													else
													{
														echo $BrandData->title_en;
													}
													?>
												</td>
												<td>
												<?php
												if ($this->session->userdata('lang') == "ar")
												{
													echo $DataRows_Row->title;
												}
												else
												{
													echo $DataRows_Row->title_en;
												}
												?>
												</td>
												<td><?php echo $DataRows_Row->quantity;?></td>
												<!-- <td><?php echo $DataRows_Row->cost_price;?></td> -->
												<td><?php echo $DataRows_Row->price;?></td>
												<td><?php echo $DataRows_Row->lowest_price;?></td>
												
											</tr>
											<?php
											}
											?>
										</tbody>
									</table>

								