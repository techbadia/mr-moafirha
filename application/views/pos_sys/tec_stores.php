<!--begin: Datatable -->
<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
	<thead>
		<tr>
			<th>ID</th>
			<th><?php echo lang('pos_name');?></th>
			<th><?php echo lang('pos_code');?></th>
			<th><?php echo lang('pos_email');?></th>
			<th><?php echo lang('pos_phone');?></th>
			<th><?php echo lang('pos_address1');?></th>
			<th><?php echo lang('pos_address2');?></th>
			<th><?php echo lang('tec_users');?></th>
			<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
			<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach($DataRows as $DataRows_Row) {
			$store_id = $DataRows_Row->id;
		?>
		<tr>
			<td><?php echo $DataRows_Row->id;?></td>
			<td><?php echo $DataRows_Row->name;?></td>
			<td><?php echo $DataRows_Row->code;?></td>
			<td><?php echo $DataRows_Row->email;?></td>
			<td><?php echo $DataRows_Row->phone;?></td>
			<td><?php echo $DataRows_Row->address1;?></td>
			<td><?php echo $DataRows_Row->address2;?></td>
			<td>
				<a href="<?php echo base_url().$Segment1."/".$Segment2."/tec_users/".$DataRows_Row->id;?>">
				<?php echo lang('tec_users');?> 
				</a>
			</td>
			<td style="text-align:center">
				<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
					<i class="flaticon-edit-1 text-primary icon-lg"></i> 
				</a>
			</td>
			<td style="text-align:center">
				<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
					<i class="flaticon-delete text-danger icon-lg"></i> 
				</a>
			</td>
		</tr>
		<?php
		}
		?>
	</tbody>

</table>

								