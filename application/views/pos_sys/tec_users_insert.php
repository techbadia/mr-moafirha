
<div class="card card-custom gutter-b example example-compact">
	<?php
		$FormPath = base_url().$Segment1."/".$Segment2."/tec_users_insert_data";
		echo form_open_multipart($FormPath);
	?>
	<div class="form-body">
		<div class="form-group row">
		<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('username');?></label>
				<input type="text" name="username" class="form-control" value="" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('password');?></label>
				<input type="password" name="password" class="form-control" value="" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_email');?></label>
				<input type="email" name="email" class="form-control" value="" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('tec_users_first_name');?></label>
				<input type="text" name="first_name" class="form-control" value="" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('tec_users_last_name');?></label>
				<input type="text" name="last_name" class="form-control" value="" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('tec_users_phone');?></label>
				<input type="text" name="phone" class="form-control" value="">
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('tec_users_gender');?></label>
				<select name="gender" class="form-control">
					<option value="male"><?php echo lang('tec_users_gender_male');?></option>
					<option value="female"><?php echo lang('tec_users_gender_female');?></option>
				</select>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_name');?></label>
				<select name="store_id" class="form-control">
					<?php
					$StoreList = $this->M_Tec_stores->GetMultiRow();
					foreach($StoreList as $StoreList_Row) {
					?>
					<option value="<?php echo $StoreList_Row->id;?>"><?php echo $StoreList_Row->name;?></option>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php //echo $id;?>">
					<?php
					$NowDate = date('Y-m-d H:i:s');
					$timestamp = strtotime($NowDate);
					?>
					<input type="hidden" name="created_on" value="<?php echo $timestamp;?>">
					<input type="hidden" name="last_login" value="<?php echo $timestamp;?>">
					<input type="hidden" name="active" value="1">
					<input type="hidden" name="group_id" value="2">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>
</div>