
<div class="card card-custom gutter-b example example-compact">
	<?php
		$FormPath = base_url().$Segment1."/".$Segment2."/tec_users_update_data";
		echo form_open_multipart($FormPath);
	?>
	<div class="form-body">
		<div class="form-group row">
		<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('username');?></label>
				<input type="text" name="username" class="form-control" value="<?php echo $username;?>" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label">
					<input type="checkbox" id="ChangePassword" name="ChangePassword" value="True" />&nbsp;
					<?php echo $this->lang->line('ChangePassword');?>
				</label>
				<input type="password" name="password" class="form-control" value="" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_email');?></label>
				<input type="email" name="email" class="form-control" value="<?php echo $email;?>" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('tec_users_first_name');?></label>
				<input type="text" name="first_name" class="form-control" value="<?php echo $first_name;?>" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('tec_users_last_name');?></label>
				<input type="text" name="last_name" class="form-control" value="<?php echo $last_name;?>" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('tec_users_phone');?></label>
				<input type="text" name="phone" class="form-control" value="<?php echo $phone;?>">
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('tec_users_gender');?></label>
				<select name="gender" class="form-control">
					<option value="male" <?php if($gender == "male") {?>selected<?php }?>><?php echo lang('tec_users_gender_male');?></option>
					<option value="female" <?php if($gender == "female") {?>selected<?php }?>><?php echo lang('tec_users_gender_female');?></option>
				</select>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_name');?></label>
				<select name="store_id" class="form-control">
					<?php
					$StoreList = $this->M_Tec_stores->GetMultiRow();
					foreach($StoreList as $StoreList_Row) {
					?>
					<option value="<?php echo $StoreList_Row->id;?>" <?php if($store_id == $StoreList_Row->id) {?>selected<?php }?>><?php echo $StoreList_Row->name;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('active');?></label>
				<select name="active" class="form-control">
					<option value="1" <?php if($active == 1) {?>selected<?php }?>><?php echo lang('Yes');?></option>
					<option value="0" <?php if($active == 0) {?>selected<?php }?>><?php echo lang('No');?></option>
				</select>
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php echo $id;?>">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>
</div>