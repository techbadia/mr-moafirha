<!--begin: Datatable -->
<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
	<thead>
		<tr>
			<th>ID</th>
			<th><?php echo lang('tec_users_first_name');?></th>
			<th><?php echo lang('tec_users_last_name');?></th>
			<th><?php echo lang('tec_users_active');?></th>
			<th><?php echo lang('tec_users_phone');?></th>
			<th><?php echo lang('Email');?></th>
			<th><?php echo lang('tec_users_gender');?></th>
			<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach($DataRows as $DataRows_Row) {
			$store_id = $DataRows_Row->id;
		?>
		<tr>
			<td><?php echo $DataRows_Row->id;?></td>
			<td><?php echo $DataRows_Row->first_name;?></td>
			<td><?php echo $DataRows_Row->last_name;?></td>
			<td><?php echo $DataRows_Row->active;?></td>
			<td><?php echo $DataRows_Row->phone;?></td>
			<td><?php echo $DataRows_Row->email;?></td>
			<td><?php echo $DataRows_Row->gender;?></td>
			<td style="text-align:center">
				<a href="<?php echo base_url().$Segment1."/".$Segment2."/tec_users_updateform/".$DataRows_Row->id;?>">
					<i class="flaticon-edit-1 text-primary icon-lg"></i> 
				</a>
			</td>
		</tr>
		<?php
		}
		?>
	</tbody>

</table>

								