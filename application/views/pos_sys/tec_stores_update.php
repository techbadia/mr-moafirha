<div class="card card-custom gutter-b example example-compact">
	<?php
	$FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
	echo form_open_multipart($FormPath);
	?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_name');?></label>
				<input type="text" name="name" class="form-control" value="<?php echo $name;?>" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_code');?></label>
				<input type="text" name="code" class="form-control" value="<?php echo $code;?>" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_email');?></label>
				<input type="email" name="email" class="form-control" value="<?php echo $email;?>" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_phone');?></label>
				<input type="text" name="phone" class="form-control" value="<?php echo $phone;?>" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_address1');?></label>
				<input type="text" name="address1" class="form-control" value="<?php echo $address1;?>" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_address2');?></label>
				<input type="text" name="address2" class="form-control" value="<?php echo $address2;?>">
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_city');?></label>
				<input type="text" name="city" class="form-control" value="<?php echo $city;?>" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_state');?></label>
				<input type="text" name="state" class="form-control" value="<?php echo $state;?>" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_postal_code');?></label>
				<input type="text" name="postal_code" class="form-control" value="<?php echo $postal_code;?>">
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_country');?></label>
				<input type="text" name="country" class="form-control" value="<?php echo $country;?>">
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_currency_code');?></label>
				<input type="text" name="currency_code" class="form-control" value="<?php echo $currency_code;?>">
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label>
					<input type="checkbox" id="ChangeImage" name="ChangeImage" value="True" />&nbsp;
					<?php echo $this->lang->line('ChangeFile');?>
				</label>
				<input type="file" name="image" class="form-control" dir="ltr">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_receipt_header');?></label>
				<input type="text" name="receipt_header" class="form-control" value="<?php echo $receipt_header;?>">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('pos_receipt_footer');?></label>
				<input type="text" name="receipt_footer" class="form-control" value="<?php echo $receipt_footer;?>">
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php echo $id;?>">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>
</div>