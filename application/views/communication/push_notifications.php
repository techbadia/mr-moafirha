                <div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
                    <?php
                        $FormPath = base_url()."home/push_notifications_send";
                        echo form_open_multipart($FormPath);
                    ?>
                        <div class="form-body">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label><?php echo $this->lang->line('serv_message_subject');?>:</label>
                                    <input type="text" id="subject" name="subject" class="form-control" required>
                                </div>
                                <div class="col-md-12">
                                    <label><?php echo $this->lang->line('serv_message_message');?>:</label>
                                    <input type="text" id="message" name="message" class="form-control" required>
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-actions right" style="text-align: right; font-weight: bold !important; font-size: 18px;">
                            <button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Send_Notification_To_Customers');?></button>
                        </div>
                    <?php
                        echo form_close();
                    ?>
                </div>
            