<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <form class="kt-form kt-form--label-right" method="post" action="<?php echo base_url().$Segment1."/".$Segment2."/filter";?>" enctype="multipart/form-data">
        <div class="row">
            <div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <label><?php echo $this->lang->line('hr_holidays_fromdate');?></label>
                <input type="date" name="fromdate" class="form-control" value="<?php echo $fromdate;?>" required>
            </div>
            <div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <label><?php echo $this->lang->line('hr_holidays_todate');?></label>
                <input type="date" name="todate" class="form-control" value="<?php echo $todate;?>" required>
            </div>
            <div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <label><?php echo $this->lang->line('tic_ticket_category_id');?></label>
                <select id="category_id" name="category_id" class="form-control" required>
                    <option <?php if($category_id ==0) {?>selected<?php }?> value="0"><?php echo $this->lang->line('data_request_All_Categories');?></option>
                    <?php
                    foreach($Category as $Category_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $Category_Row->ar_title;
                        }
                        else
                        {
                            $Title = $Category_Row->en_title;
                        }
                    ?>
                    <option <?php if($category_id ==$Category_Row->id) {?>selected<?php }?> value="<?php echo $Category_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <label><?php echo $this->lang->line('data_request_customer_id');?></label>
                <select id="customer_id" name="customer_id" class="form-control" required>
                    <option <?php if($customer_id ==0) {?>selected<?php }?> value="0"><?php echo $this->lang->line('data_request_All_customers');?></option>
                    <?php
                    foreach($Customer as $Customer_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $Customer_Row->ar_title;
                        }
                        else
                        {
                            $Title = $Customer_Row->en_title;
                        }
                    ?>
                    <option <?php if($customer_id == $Customer_Row->id) {?>selected<?php } ?> value="<?php echo $Customer_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <label><?php echo $this->lang->line('data_request_status_id');?></label>
                <select id="status_id" name="status_id" class="form-control" required>
                <option <?php if($status_id ==0) {?>selected<?php }?> value="0"><?php echo $this->lang->line('data_request_All_status');?></option>
                    <?php
                    foreach($Status as $Status_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $Status_Row->ar_title;
                        }
                        else
                        {
                            $Title = $Status_Row->en_title;
                        }
                    ?>
                    <option <?php if($status_id == $Status_Row->id) {?>selected<?php } ?> value="<?php echo $Status_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <label><?php echo $this->lang->line('tic_ticket_priority_id');?></label>
                <select id="priority_id" name="priority_id" class="form-control" required>
                <option <?php if($priority_id ==0) {?>selected<?php }?> value="0"><?php echo $this->lang->line('data_request_All_Priorities');?></option>
                    <?php
                    foreach($Priority as $Priority_Row) {
                        if ($this->session->userdata('lang') == "ar")
                        {
                            $Title = $Priority_Row->ar_title;
                        }
                        else
                        {
                            $Title = $Priority_Row->en_title;
                        }
                    ?>
                    <option <?php if($priority_id == $Priority_Row->id) {?>selected<?php } ?> value="<?php echo $Priority_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
			<div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <label><?php echo $this->lang->line('tic_ticket_user_id');?></label>
                <select id="user_id" name="user_id" class="form-control" required>
                <option <?php if($user_id ==0) {?>selected<?php }?> value="0"><?php echo $this->lang->line('data_request_All_Users');?></option>
                    <?php
                    foreach($User as $User_Row) {
                        $Title = $User_Row->fullname;
                    ?>
                    <option <?php if($user_id == $User_Row->id) {?>selected<?php } ?> value="<?php echo $User_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
			<div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <label><?php echo $this->lang->line('tic_ticket_assign_to');?></label>
                <select id="assign_to" name="assign_to" class="form-control" required>
                <option <?php if($assign_to ==0) {?>selected<?php }?> value="0"><?php echo $this->lang->line('data_request_All_Users');?></option>
                    <?php
                    foreach($User as $User_Row) {
                        $Title = $User_Row->fullname;
                    ?>
                    <option <?php if($assign_to == $User_Row->id) {?>selected<?php } ?> value="<?php echo $User_Row->id;?>"><?php echo $Title;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-6 col-xs-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <button type="submit" class="btn btn-success btn-custom" id="kt_sweetalert_demo_3_3" style="margin-top:25px;"><?php echo lang('Search');?></button>
            </div>
        </div>
    </form>
</div>
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('tic_ticket_category_id');?></th>
												<th><?php echo lang('tic_ticket_priority_id');?></th>
												<th><?php echo lang('tic_ticket_status_id');?></th>
												<th><?php echo lang('tic_ticket_customer_id');?></th>
												<th><?php echo lang('tic_ticket_user_id');?></th>
												<th><?php echo lang('tic_ticket_assign_to');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$tic_status_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
												<?php
												$category_id = $DataRows_Row->category_id;
												$CategoryData = $this->M_tic_category->GetRow($category_id);
												if ($this->session->userdata('lang') == "ar")
												{
													echo $CategoryData->ar_title;
												}
												else
												{
													echo $CategoryData->en_title;
												}
												?>
												</td>
												<td>
												<?php
												$priority_id = $DataRows_Row->priority_id;
												$PriorityData = $this->M_tic_priority->GetRow($priority_id);
												if ($this->session->userdata('lang') == "ar")
												{
													echo $PriorityData->ar_title;
												}
												else
												{
													echo $PriorityData->en_title;
												}
												?>
												</td>
												<td>
												<?php
												$status_id = $DataRows_Row->status_id;
												$StatusData = $this->M_tic_status->GetRow($status_id);
												if ($this->session->userdata('lang') == "ar")
												{
													echo $StatusData->ar_title;
												}
												else
												{
													echo $StatusData->en_title;
												}
												?>
												</td>
												<td>
												<?php
												$customer_id = $DataRows_Row->customer_id;
												$CustomerData = $this->M_sale_customer->GetRow($customer_id);
												if ($this->session->userdata('lang') == "ar")
												{
													echo $CustomerData->ar_title;
												}
												else
												{
													echo $CustomerData->en_title;
												}
												?>
												</td>
												<td>
												<?php
												$user_id = $DataRows_Row->user_id;
												$UserData = $this->M_usr_users->GetRow($user_id);
												echo $UserData->fullname;
												?>
												</td>
												<td>
												<?php
												$assign_to = $DataRows_Row->assign_to;
												$UserData = $this->M_usr_users->GetRow($assign_to);
												echo $UserData->fullname;
												?>
												</td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

								