<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
	<?php
		$FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
		echo form_open_multipart($FormPath);
	?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<label class="col-form-label"><?php echo lang('tic_ticket_category_id');?></label>
				<select id="category_id" name="category_id" class="form-control" required>
					<?php
					foreach($Category as $Category_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Title = $Category_Row->ar_title;
						}
						else
						{
							$Title = $Category_Row->en_title;
						}
					?>
					<option <?php if($category_id == $Category_Row->id) {?>selected<?php }?> value="<?php echo $Category_Row->id;?>"><?php echo $Title;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<label class="col-form-label"><?php echo lang('tic_ticket_priority_id');?></label>
				<select id="priority_id" name="priority_id" class="form-control" required>
					<?php
					foreach($Priority as $Priority_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Title = $Priority_Row->ar_title;
						}
						else
						{
							$Title = $Priority_Row->en_title;
						}
					?>
					<option <?php if($priority_id == $Priority_Row->id) {?>selected<?php }?> value="<?php echo $Priority_Row->id;?>"><?php echo $Title;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<label class="col-form-label"><?php echo lang('tic_ticket_status_id');?></label>
				<select id="status_id" name="status_id" class="form-control" required>
					<?php
					foreach($Status as $Status_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Title = $Status_Row->ar_title;
						}
						else
						{
							$Title = $Status_Row->en_title;
						}
					?>
					<option <?php if($status_id == $Status_Row->id) {?>selected<?php }?> value="<?php echo $Status_Row->id;?>"><?php echo $Title;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<label class="col-form-label"><?php echo lang('tic_ticket_customer_id');?></label>
				<select id="customer_id" name="customer_id" class="form-control kt-selectpicker" data-size="5" data-live-search="true" required>
					<?php
					foreach($Customer as $Customer_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Title = $Customer_Row->ar_title;
						}
						else
						{
							$Title = $Customer_Row->en_title;
						}
					?>
					<option <?php if($customer_id == $Customer_Row->id) {?>selected<?php }?> value="<?php echo $Customer_Row->id;?>"><?php echo $Title." : ".$Customer_Row->mobile2;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<label class="col-form-label"><?php echo lang('tic_ticket_assign_to');?></label>
				<select id="assign_to" name="assign_to" class="form-control" required>
					<?php
					foreach($User as $User_Row) {
						$Title = $User_Row->fullname;
					?>
					<option <?php if($assign_to == $User_Row->id) {?>selected<?php }?> value="<?php echo $User_Row->id;?>"><?php echo $Title;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<label class="col-form-label"><?php echo lang('data_request_details');?></label>
				<input type="text" name="details" class="form-control" value="<?php echo $details;?>" required>
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php echo $id;?>">
					<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
					<input type="hidden" name="deleted" value="<?php echo $deleted;?>">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s" name="action" value="Save">
						<?php echo $this->lang->line('Save');?>
					</button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>
</div>