<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head>
		<base href="">
		<meta charset="utf-8" />
		<title>LBI-ERP System | 404</title>
		<meta name="description" content="LBI-ERP System | 404" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<link rel="canonical" href="https://www.lbi-egypt.com/" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Vendors Styles(used by this page)-->
		<link href="<?php echo base_url();?>/assets/plugins/custom/fullcalendar/fullcalendar.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors Styles-->
		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="<?php echo base_url();?>/assets/plugins/global/plugins.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>/assets/plugins/custom/prismjs/prismjs.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>/assets/css/style.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
		<link href="<?php echo base_url();?>/assets/css/themes/layout/header/base/light<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>/assets/css/themes/layout/header/menu/light<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>/assets/css/themes/layout/brand/dark<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>/assets/css/themes/layout/aside/dark<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
		<!--end::Layout Themes-->
		<link rel="shortcut icon" href="<?php echo base_url();?>/assets/media/logos/favicon.ico" />
		<?php
		if ($this->session->userdata('lang') == "ar"){
		?>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/arabic_font.css" />
		<?php
		}
		?>
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading" <?php if ($this->session->userdata('lang') == "ar"){ ?>style="font-family: 'Droid Arabic Kufi', serif; letter-spacing: 0px;"<?php }?>>
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Error-->
			<div class="d-flex flex-row-fluid flex-column bgi-size-cover bgi-position-center bgi-no-repeat p-10 p-sm-30" style="background-image: url(<?php echo base_url();?>/assets/media/error/bg1.jpg);">
				<!--begin::Content-->
				<h1 class="font-weight-boldest text-dark-75 mt-15" style="font-size: 10rem">
				قريباً
				</h1>
				<p class="font-size-h3 font-weight-normal">
				قريباً سوف تكون هذه الصفحة متاحة للإستخدام <br> نحن في مرحلة التطوير
				</p>
				<!--end::Content-->
			</div>
			<!--end::Error-->
		</div>
		<!--end::Main-->

		<!--begin::Global Config(global config for global JS scripts)-->
		<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
		<!--end::Global Config-->
		<!--begin::Global Theme Bundle(used by all pages)-->
		<script src="<?php echo base_url();?>/assets/plugins/global/plugins.bundle.js"></script>
		<script src="<?php echo base_url();?>/assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
		<script src="<?php echo base_url();?>/assets/js/scripts.bundle.js"></script>
		<!--end::Global Theme Bundle-->
	</body>
	<!--end::Body-->
</html>