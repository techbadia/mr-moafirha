
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
	<?php
		$FormPath = base_url().$Segment1."/".$Segment2."/DeleteRow";
		echo form_open_multipart($FormPath);
	?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-9 col-md-9 col-sm-12">
				<label class="col-form-label"><?php echo lang('TableName');?></label>
				<select name="table" class="form-control">
					<option value="app_company"><?php echo lang('Branches');?></option>
					<option value="usr_users"><?php echo lang('usr_users');?></option>
					<option value="fin_treeaccount"><?php echo lang('fin_treeaccount');?></option>
					<option value="fin_journal_delete_all"><?php echo lang('fin_journal_delete_all');?></option>
					<option value="0" disabled><?php echo lang('ShortCut_purchase');?></option>
					<option value="buy_manufacturer">&nbsp;&nbsp;:&nbsp;<?php echo lang('buy_manufacturer');?></option>
					<option value="buy_bill">&nbsp;&nbsp;:&nbsp;<?php echo lang('buy_bill');?></option>
					<option value="buy_offer">&nbsp;&nbsp;:&nbsp;<?php echo lang('buy_offer');?></option>
					<option value="buy_returns">&nbsp;&nbsp;:&nbsp;<?php echo lang('buy_returns');?></option>
					<option value="0" disabled><?php echo lang('ShortCut_sales');?></option>
					<option value="sale_customer">&nbsp;&nbsp;:&nbsp;<?php echo lang('sale_customer');?></option>
					<option value="sale_bill">&nbsp;&nbsp;:&nbsp;<?php echo lang('sale_bill');?></option>
					<option value="sale_offer">&nbsp;&nbsp;:&nbsp;<?php echo lang('sale_offer');?></option>
					<option value="sale_returns">&nbsp;&nbsp;:&nbsp;<?php echo lang('sale_returns');?></option>
					<option value="0" disabled><?php echo lang('store');?></option>
					<option value="store">&nbsp;&nbsp;:&nbsp;<?php echo lang('store_quantity_store_id');?></option>
					<option value="store_category">&nbsp;&nbsp;:&nbsp;<?php echo lang('product_category_id');?></option>
					<option value="store_brand">&nbsp;&nbsp;:&nbsp;<?php echo lang('product_brand_id');?></option>
					<option value="product">&nbsp;&nbsp;:&nbsp;<?php echo lang('product_thetitle');?></option>
					<?php
					if($this->session->userdata('PackageName') == "cars")
					{
					?>
					<option value="cars_cars"><?php echo lang('cars_cars');?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12">
				<label class="col-form-label"><?php echo lang('RecordNumber');?></label>
				<input type="number" min="1" name="id" class="form-control" value="1" required>
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<button type="submit" class="btn btn-primary mr-2" accesskey="d"><?php echo $this->lang->line('Delete');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>
	
</div>

<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-danger-o-20">
	<div class="form-body">
		<div class="form-group row">
			<div class="col-12">
				<h3><?php echo lang('Alerts');?></h3>
				<p><?php echo lang('Delete_app_company');?></p>
				<p><?php echo lang('Delete_usr_users');?></p>
				<p><?php echo lang('Delete_fin_treeaccount');?></p>
				<p><?php echo lang('Delete_buy_manufacturer');?></p>
				<p><?php echo lang('Delete_buy_bill');?></p>
				<p><?php echo lang('Delete_buy_offer');?></p>
				<p><?php echo lang('Delete_buy_returns');?></p>
				<p><?php echo lang('Delete_sale_customer');?></p>
				<p><?php echo lang('Delete_sale_bill');?></p>
				<p><?php echo lang('Delete_sale_offer');?></p>
				<p><?php echo lang('Delete_sale_returns');?></p>
				<p><?php echo lang('Delete_store');?></p>
				<p><?php echo lang('Delete_store_category');?></p>
				<p><?php echo lang('Delete_store_brand');?></p>
				<p><?php echo lang('Delete_product');?></p>
				<?php
				if($this->session->userdata('PackageName') == "cars")
				{
				?>
				<p><?php echo lang('Delete_cars_cars');?></p>
				<?php
				}
				?>
			</div>
		</div>
	</div>
</div>