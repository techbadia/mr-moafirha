    <div class="col-12">
        <h3><?php echo $this->lang->line('general_statistics');?></h3>
    </div>
    <div class="col-2">
        <!--begin::Stats Widget 16-->
        <a href="<?php echo base_url();?>maintenance/set_area" class="card card-custom card-stretch gutter-b" style="background-color: #880E4F">
            <!--begin::Body-->
            <div class="card-body">
                <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
                <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
                <?php
                $Area = $this->Astatistics->Count_set_area();
                echo $Area->records;
                ?>
                </div>
                <div class="font-weight-bold text-white font-size-sm">
                    <?php echo $this->lang->line('set_area');?>
                </div>
            </div>
            <!--end::Body-->
        </a>
        <!--end::Stats Widget 16-->
    </div>
    <div class="col-2">
        <!--begin::Stats Widget 16-->
        <a href="<?php echo base_url();?>maintenance/set_area" class="card card-custom card-stretch gutter-b" style="background-color: #880E4F">
            <!--begin::Body-->
            <div class="card-body">
                <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
                <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
                <?php
                $request_status = $this->Astatistics->Count_set_request_status();
                echo $request_status->records;
                ?>
                </div>
                <div class="font-weight-bold text-white font-size-sm">
                    <?php echo $this->lang->line('set_request_status');?>
                </div>
            </div>
            <!--end::Body-->
        </a>
        <!--end::Stats Widget 16-->
    </div>
    <div class="col-2">
        <!--begin::Stats Widget 16-->
        <a href="<?php echo base_url();?>maintenance/set_area" class="card card-custom card-stretch gutter-b" style="background-color: #880E4F">
            <!--begin::Body-->
            <div class="card-body">
                <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
                <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
                <?php
                $service = $this->Astatistics->Count_set_service();
                echo $service->records;
                ?>
                </div>
                <div class="font-weight-bold text-white font-size-sm">
                    <?php echo $this->lang->line('services');?>
                </div>
            </div>
            <!--end::Body-->
        </a>
        <!--end::Stats Widget 16-->
    </div>
    <div class="col-2">
        <!--begin::Stats Widget 16-->
        <a href="<?php echo base_url();?>maintenance/set_area" class="card card-custom card-stretch gutter-b" style="background-color: #880E4F">
            <!--begin::Body-->
            <div class="card-body">
                <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
                <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
                <?php
                $set_teamwork = $this->Astatistics->Count_set_teamwork();
                echo $set_teamwork->records;
                ?>
                </div>
                <div class="font-weight-bold text-white font-size-sm">
                    <?php echo $this->lang->line('set_teamwork');?>
                </div>
            </div>
            <!--end::Body-->
        </a>
        <!--end::Stats Widget 16-->
    </div>
    <div class="col-2">
        <!--begin::Stats Widget 16-->
        <a href="<?php echo base_url();?>maintenance/set_area" class="card card-custom card-stretch gutter-b" style="background-color: #880E4F">
            <!--begin::Body-->
            <div class="card-body">
                <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
                <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
                <?php
                $set_teamwork_items = $this->Astatistics->Count_set_teamwork_items();
                echo $set_teamwork_items->records;
                ?>
                </div>
                <div class="font-weight-bold text-white font-size-sm">
                    <?php echo $this->lang->line('set_teamwork_items');?>
                </div>
            </div>
            <!--end::Body-->
        </a>
        <!--end::Stats Widget 16-->
    </div>
    <div class="col-2">
        <!--begin::Stats Widget 16-->
        <a href="<?php echo base_url();?>maintenance/set_area" class="card card-custom card-stretch gutter-b" style="background-color: #880E4F">
            <!--begin::Body-->
            <div class="card-body">
                <span class="svg-icon svg-icon-light svg-icon-3x ml-n1">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
                <div class="text-white font-weight-bolder font-size-h5 mb-2 mt-5">
                <?php
                $usr_users = $this->Astatistics->Count_usr_users();
                echo $usr_users->records;
                ?>
                </div>
                <div class="font-weight-bold text-white font-size-sm">
                    <?php echo $this->lang->line('usr_users');?>
                </div>
            </div>
            <!--end::Body-->
        </a>
        <!--end::Stats Widget 16-->
    </div>

    
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="kt-portlet__body">
        <h2 class="page-title" style="margin-top: 15px;">
            <?php echo $this->lang->line('set_teamwork_tasks');?>
        </h2>
        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
            <thead>
                <tr>
                    <th><?php echo $this->lang->line('set_teamwork');?></th>
                    <th><?php echo $this->lang->line('expenses_bill_id');?></th>
                    <th><?php echo $this->lang->line('data_invoice_thedate');?></th>
                    <th><?php echo $this->lang->line('data_invoice_thetime');?></th>
                    <th><?php echo $this->lang->line('closed');?></th>
                    <th><?php echo $this->lang->line('close_date');?></th>
                    <th><?php echo $this->lang->line('close_time');?></th>
                </tr>
            </thead>
            <tbody>
            <?php
            $MultiRows = $this->M_set_teamwork_tasks->GetOPened();
            foreach($MultiRows as $MultiRows_Row) {
            ?>
            <tr>
                <td>
                    <?php
                    $teamwork_id = $MultiRows_Row->teamwork_id;
                    $TeamWorkData = $this->M_set_teamwork->GetRow($teamwork_id);
                    echo $TeamWorkData->title;
                    ?>
                </td>
                <td>
                    <a target="_blank" href="<?php echo base_url();?>index.php/data_invoice/view/<?php echo $MultiRows_Row->invoice_id;?>"><?php echo $MultiRows_Row->invoice_id;?></a>
                </td>
                <td><?php echo $MultiRows_Row->thedate; ?></td>
                <td><?php echo $MultiRows_Row->thetime; ?></td>
                <td><?php
                $closed = $MultiRows_Row->closed;
                if($closed == 0)
                {
                    echo $this->lang->line('No');
                }
                else
                {
                    echo $this->lang->line('Yes');
                }
                ?></td>
                <td><?php echo $MultiRows_Row->close_date; ?></td>
                <td><?php echo $MultiRows_Row->close_time; ?></td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>



<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="kt-portlet__body">
        <h2 class="page-title" style="margin-top: 15px;">
            <?php echo $this->lang->line('schedule');?>
        </h2>
        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
            <thead>
                <tr>
                    <th><?php echo $this->lang->line('set_teamwork');?></th>
                    <th><?php echo $this->lang->line('data_customer_fullname');?></th>
                    <th><?php echo $this->lang->line('data_customer_phone');?></th>
                    <th><?php echo $this->lang->line('data_customer_address');?></th>
                    <th><?php echo $this->lang->line('thedate');?></th>
                    <th><?php echo $this->lang->line('schedule');?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $Schedule = $this->M_set_teamwork_schedule->GetMultiRow();
                foreach($Schedule as $Schedule_Row) {
                ?>
                <tr>
                    <td>
                        <?php
                        $teamwork_id = $Schedule_Row->teamwork_id;
                        $TeamWorkData = $this->M_set_teamwork->GetRow($teamwork_id);
                        if ($this->session->userdata('lang') == "ar")
                        {
                            echo $TeamWorkData->ar_title;
                        }
                        else
                        {
                            echo $TeamWorkData->en_title;
                        }
                        ?>
                    </td>
                    <td><?php echo $Schedule_Row->customer; ?></td>
                    <td><?php echo $Schedule_Row->phone; ?></td>
                    <td><?php echo $Schedule_Row->address; ?></td>
                    <td><?php echo $Schedule_Row->thedate; ?></td>
                    <td><?php echo $Schedule_Row->schedule; ?></td>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    
</div>
