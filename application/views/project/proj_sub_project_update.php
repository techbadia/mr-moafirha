<?php
	$FormPath = base_url().$Segment1."/".$Segment2."/Update_Data_sup";
	echo form_open_multipart($FormPath);
?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_sup_project_customer_type');?></label>
				<label for="">
				<input type="radio" name="customer_type" class="customer_type" <?php echo ($customer_type == "individual")?"checked":"" ?> value="individual" >
				<?php echo lang('individual');?></label>
				<label for="">
				<input type="radio" name="customer_type" class="customer_type" <?php echo ($customer_type == "company")?"checked":"" ?>   value="company" >
				<?php echo lang('company');?></label>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_sup_project_customer_name');?></label>
				<input type="text" name="customer_name" class="form-control" value="<?php echo $customer_name ?>" required>
			</div>
			<input type="hidden" name="customer_id_number" class="form-control" value="" >
			<input type="hidden" name="customer_national" class="form-control" value="" >
			<input type="hidden" name="customer_address" class="form-control" value="" >
			<input type="hidden" name="customer_mobile" class="form-control" value="" >
			<input type="hidden" name="id" value="<?php echo $id;?>">

			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_sup_project_customer_id_number');?></label>
				<input type="text" name="customer_id_number" class="form-control" value="<?php echo $customer_id_number ?>" >
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 showMe" style='display:<?php echo ($customer_type == "individual")?"none":"" ?>'>
				<label class="col-form-label"><?php echo lang('proj_sup_project_customer_national_number');?></label>
				<input type="text" name="customer_national" class="form-control" value="<?php echo $customer_national ?>" >
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 showMe" style='display:<?php echo ($customer_type == "individual")?"none":"" ?>'>
				<label class="col-form-label"><?php echo lang('proj_sup_project_customer_address');?></label>
				<input type="text" name="customer_address" class="form-control" value="<?php echo $customer_address ?>" >
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 showMe" style='display:<?php echo ($customer_type == "individual")?"none":"" ?>'>
				<label class="col-form-label"><?php echo lang('proj_sup_project_customer_mobile');?></label>
				<input type="text" name="customer_mobile" class="form-control" value="<?php echo $customer_mobile ?>" >
			</div>
			<input type="hidden" name="project_id" value="<?php echo $_GET["pro_id"] ?>">
			<?php /* ?>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_project_project_id');?></label>
				<select name="project_id" class="form-control">
					<?php
					$ProjectList = $this->M_proj_project->GetMultiRow();
					foreach($ProjectList as $ProjectList_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Project = $ProjectList_Row->ar_title;
						}
						else
						{
							$Project = $ProjectList_Row->en_title;
						}
					?>
					<option <?php echo ($project_id == $ProjectList_Row->id)?"selected":"" ?> value="<?php echo $ProjectList_Row->id;?>"><?php echo $Project;?></option>
					<?php } ?>
				</select>
			</div>
			<?php */ ?>

			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_sup_project_name');?></label>
				<input type="text" name="sup_proj_name" class="form-control" value="<?php echo $sup_proj_name ?>" required>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_sub_project_date_start');?></label>
				<input type="date" name="date_start" class="form-control" value="<?php echo $date_start ?>" required>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_sub_project_date_end');?></label>
				<input type="date" name="date_end" class="form-control" value="<?php echo $date_end ?>" required>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_project_details');?></label>
				<input type="text" name="details" class="form-control" value="<?php echo $details ?>" required>
			</div>
		</div>
		<div class="append">
			<?php
			if(!empty($item_amount)){
			$olditems = explode("|",trim($item_amount,"|"));
			$counter = 1;
					foreach ($olditems as $single) {
						$class = ($counter == 1)?"clone":"";
					$single = explode("_",$single);
			 ?>
		<div class="form-group row <?php echo $class ?>">

			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('expense_item');?></label>
				<select name="items[]" class="form-control">
					<?php foreach ($items as $item) { ?>
							<?php
							if ($this->session->userdata('lang') == "ar")
							{
								$itemsss = $item->item_name;
							}
							else
							{
								$itemsss = $item->item_name_en;
							}
							 ?>
					<option <?php echo ($single[0] == $item->id)?"selected":"" ?> value="<?php echo $item->id;?>"><?php echo $itemsss;?></option>
					<?php } ?>
				</select>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('amount');?></label>
				<input type="text" name="amount[]" class="form-control" value="<?php echo $single[1] ?>" required>
			</div>
			<div class="col-lg-1 col-md-1 col-sm-12">
				<label class="col-form-label"><?php echo lang('Add_new');?></label>

				<button type="button" class="btn btn-success  form-control" onclick="addNewItem()"> <i class="fa fa-plus"></i> </button>
			</div>
			<div class="col-lg-1 col-md-1 col-sm-12">
				<label class="col-form-label"><?php echo lang('Delete');?></label>

				<button type="button" class="btn btn-danger delete form-control" onclick="deleteItem(this)"> <i class="fa fa-minus"></i> </button>
			</div>
		</div>
		<?php 	$counter++; } ?>
		<?php }
		if(empty($item_amount)){
		 ?>

		<div class="form-group row clone">

			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('expense_item');?></label>
				<select name="items[]" class="form-control">
					<?php foreach ($items as $item) { ?>
							<?php
							if ($this->session->userdata('lang') == "ar")
							{
								$itemsss = $item->item_name;
							}
							else
							{
								$itemsss = $item->item_name_en;
							}
							 ?>
					<option value="<?php echo $item->id;?>"><?php echo $itemsss;?></option>
					<?php } ?>
				</select>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('amount');?></label>
				<input type="text" name="amount[]" class="form-control" value="" required>
			</div>
			<div class="col-lg-1 col-md-1 col-sm-12">
				<label class="col-form-label"><?php echo lang('Add_new');?></label>

				<button type="button" class="btn btn-success  form-control" onclick="addNewItem()"> <i class="fa fa-plus"></i> </button>
			</div>
			<div class="col-lg-1 col-md-1 col-sm-12">
				<label class="col-form-label"><?php echo lang('Delete');?></label>

				<button type="button" class="btn btn-danger delete form-control" onclick="deleteItem(this)"> <i class="fa fa-minus"></i> </button>
			</div>
		</div>
<?php } ?>
		</div>
		<div class="form-group row">
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_sub_project_total');?></label>
				<input type="text" name="total" class="form-control total" value="<?php echo $total ?>" required>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_sub_project_paid');?></label>
				<input type="text" name="paid" class="form-control paid" value="<?php echo $paid ?>" required>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_sub_project_remain');?></label>
				<input type="text" name="remain" class="form-control remain" value="<?php echo $remain ?>" required>
			</div>
		</div>

		<div class="form-group row">
			<label><?php echo lang('proj_project_files');?> : <small><?php echo lang('multiple_files');?></small></label>
			<div></div>
			<div class="custom-file">

				<input type='file' name='files[]' multiple="" class="custom-file-input" id="customFile" />
				<label class="custom-file-label" for="customFile"><?php echo lang('Choose_files');?></label>

			</div>
			</div>
		<div class="form-group row">

				<?php if(!empty($files)){
					foreach ($files as $DataRows_Row) {
						// code...

					?>
					<div class="col-lg-6 col-md-6 col-sm-12">
					<?php echo lang('file');?>	:<a href="<?php echo base_url()."upload/project/".$DataRows_Row->image;?>" target="_blank"><i class="fa fa-file"></i> </a>
					</div>
				<?php }} ?>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12">
			<label class="col-form-label"><?php echo lang('proj_sup_end');?></label>
			<label for="">
			<input type="radio" name="closed" class="closed" <?php echo ($closed == "1")?"checked":"" ?> value="1" >
			<?php echo lang('yes');?></label>
			<label for="">
			<input type="radio" name="closed" class="closed" <?php echo ($closed == "0")?"checked":"" ?>   value="0" >
			<?php echo lang('no');?></label>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<a href="<?php echo base_url()."/project/proj_project/proj_sub_projects/".$project_id;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>

					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
<?php
	echo form_close();
?>
