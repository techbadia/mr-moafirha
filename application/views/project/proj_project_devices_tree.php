<br>
	<div class="d-flex align-items-center p-4 bg-dark" style="text-align:center !important">
		<a href="<?php echo base_url()."project/proj_project/proj_project_devices/".$project_id;?>" class="btn btn-transparent-success font-weight-bold mr-2">
			<?php echo lang('fin_treeaccount_table');?>
		</a>
		<a href="<?php echo base_url()."project/proj_project/proj_project_devices_tree/".$project_id;?>" class="btn btn-transparent-success font-weight-bold mr-2">
			<?php echo lang('fin_treeaccount_treeview');?>
		</a>
	</div>
<br>
	<div class="card-body">
		<div id="kt_tree_1" class="tree-demo">
			<ul>
				<?php
				$Building = $this->M_proj_project_devices->GetByProject_Building($project_id);
				foreach($Building as $Building_Row) {
				?>
				<li>
					<?php echo lang('proj_project_building_no')." : ".$Building_Row->building_no;?>
					<ul>
						<?php
						$Floor = $this->M_proj_project_devices->GetByProject_Floor($project_id);
						foreach($Floor as $Floor_Row) {
						?>
						<li>
							<?php echo lang('proj_project_floor_no')." : ".$Floor_Row->floor_no;?>
							<ul>
								<?php
								$Room = $this->M_proj_project_devices->GetByProject_Room($project_id);
								foreach($Room as $Room_Row) {
									$room_no = $Room_Row->room_no;
								?>
								<li>
									<?php echo lang('proj_project_room_no')." : ".$Room_Row->room_no;?>
									<ul>
									<?php
									$Devices = $this->M_proj_project_devices->GetByRoom($room_no);
									foreach($Devices as $Devices_Row) {
									?>
										<li><?php echo $Devices_Row->barcode." : ".$Devices_Row->details;?></li>
									<?php
									}
									?>
									</ul>
								</li>
								<?php
								}
								?>
							</ul>
						</li>
						<?php
						}
						?>
					</ul>
				</li>
				<?php
				}
				?>
			</ul>
		</div>
	</div>
								