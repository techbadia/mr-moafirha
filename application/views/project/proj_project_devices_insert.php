<?php
	$FormPath = base_url().$Segment1."/".$Segment2."/devices_insert_data";
	echo form_open_multipart($FormPath);
?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_project_project_id');?></label>
				<select name="project_id" class="form-control">
					<?php
					$ProjectList = $this->M_proj_project->GetMultiRow();
					foreach($ProjectList as $ProjectList_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Project = $ProjectList_Row->ar_title;
						}
						else
						{
							$Project = $ProjectList_Row->en_title;
						}
					?>
					<option value="<?php echo $ProjectList_Row->id;?>"><?php echo $Project;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_project_barcode');?></label>
				<input type="text" name="barcode" class="form-control" value="" required>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_project_plugin_date');?></label>
				<input type="date" name="plugin_date" class="form-control" value="<?php echo date("Y-m-d");?>" required>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_project_details');?></label>
				<input type="text" name="details" class="form-control" value="" required>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_project_building_no');?></label>
				<input type="text" name="building_no" class="form-control" value="" required>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_project_floor_no');?></label>
				<input type="text" name="floor_no" class="form-control" value="" required>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<label class="col-form-label"><?php echo lang('proj_project_room_no');?></label>
				<input type="text" name="room_no" class="form-control" value="" required>
			</div>
		</div>
		
		<div class="form-group row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<label class="col-form-label"><?php echo lang('image');?></label>
				<input type="file" name="image" class="form-control" dir="ltr" required>
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php //echo $id;?>">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
<?php
	echo form_close();
?>
                