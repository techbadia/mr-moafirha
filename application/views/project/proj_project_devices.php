<br>
	<div class="d-flex align-items-center p-4 bg-dark" style="text-align:center !important">
		<a href="<?php echo base_url()."project/proj_project/proj_project_devices/".$project_id;?>" class="btn btn-transparent-success font-weight-bold mr-2">
			<?php echo lang('fin_treeaccount_table');?>
		</a>
		<a href="<?php echo base_url()."project/proj_project/proj_project_devices_tree/".$project_id;?>" class="btn btn-transparent-success font-weight-bold mr-2">
			<?php echo lang('fin_treeaccount_treeview');?>
		</a>
	</div>
<br>
<!--begin: Datatable -->
<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
	<thead>
		<tr>
			<th>ID</th>
			<th><?php echo lang('proj_project_customer_id');?></th>
			<th><?php echo lang('proj_project_project_id');?></th>
			<th><?php echo lang('proj_project_barcode');?></th>
			<th><?php echo lang('proj_project_details');?></th>
			<th><?php echo lang('proj_project_plugin_date');?></th>
			<th><?php echo lang('proj_project_building_no');?></th>
			<th><?php echo lang('proj_project_floor_no');?></th>
			<th><?php echo lang('proj_project_room_no');?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$Today = date("Y-m-d");
		foreach($DataRows as $DataRows_Row) {
		?>
		<tr>
			<td><?php echo $DataRows_Row->id;?></td>
			<td>
			<?php
			$project_id = $DataRows_Row->project_id;
			$ProjectData = $this->M_proj_project->GetRow($project_id);
			$customer_id = $ProjectData->customer_id;
			$CustomerData = $this->M_sale_customer->GetRow($customer_id);
			if ($this->session->userdata('lang') == "ar")
			{
				echo $CustomerData->ar_title;
			}
			else
			{
				echo $CustomerData->en_title;
			}
			?>
			</td>
			<td>
			<?php
			$project_id = $DataRows_Row->project_id;
			$ProjectData = $this->M_proj_project->GetRow($project_id);
			if ($this->session->userdata('lang') == "ar")
			{
				$ProjectTitle = $ProjectData->ar_title;
			}
			else
			{
				$ProjectTitle = $ProjectData->en_title;
			}
			echo "<a href='".base_url()."project/proj_project/view_project/".$project_id."'>".$ProjectTitle."</a>";
			?>
			</td>
			<td><?php echo $DataRows_Row->barcode;?></td>
			<td><?php echo $DataRows_Row->details;?></td>
			<td><?php echo $DataRows_Row->plugin_date;?></td>
			<td><?php echo lang('proj_project_building_no')." ".$DataRows_Row->building_no;?></td>
			<td><?php echo $DataRows_Row->floor_no;?></td>
			<td><?php echo $DataRows_Row->room_no;?></td>
		</tr>
		<?php
		}
		?>
	</tbody>
	
</table>

								