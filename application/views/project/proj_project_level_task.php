
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('proj_project_level_task_project_id');?></th>
												<th><?php echo lang('proj_project_level_task_level_id');?></th>
												<th><?php echo lang('proj_project_level_task_user_id');?></th>
												<th><?php echo lang('proj_project_level_task_status_id');?></th>
												<th><?php echo lang('proj_project_level_task_ar_details');?></th>
												<th><?php echo lang('proj_project_level_task_en_details');?></th>
												<th><?php echo lang('proj_project_level_task_start_date');?></th>
												<th><?php echo lang('proj_project_level_task_end_date');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
												<?php
												$project_id = $DataRows_Row->project_id;
												$ProjectData = $this->M_proj_project->GetRow($project_id);
												if ($this->session->userdata('lang') == "ar")
												{
													echo $ProjectData->ar_title;
												}
												else
												{
													echo $ProjectData->en_title;
												}
												?>
												</td>
												<td>
												<?php
												$level_id = $DataRows_Row->level_id;
												$LevelData = $this->M_proj_project_level->GetRow($level_id);
												if ($this->session->userdata('lang') == "ar")
												{
													echo $LevelData->ar_title;
												}
												else
												{
													echo $LevelData->en_title;
												}
												?>
												</td>
												<td>
												<?php
												$user_id = $DataRows_Row->user_id;
												$ManagerData = $this->M_usr_users->GetRow($user_id);
												echo $ManagerData->fullname;
												?>
												</td>
												<td>
												<?php
												$status_id = $DataRows_Row->status_id;
												$StatusData = $this->M_proj_status->GetRow($status_id);
												if ($this->session->userdata('lang') == "ar")
												{
													echo $StatusData->ar_title;
												}
												else
												{
													echo $StatusData->en_title;
												}
												?>
												</td>
												<td><?php echo $DataRows_Row->ar_details;?></td>
												<td><?php echo $DataRows_Row->en_details;?></td>
												<td><?php echo $DataRows_Row->start_date;?></td>
												<td><?php echo $DataRows_Row->end_date;?></td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

								