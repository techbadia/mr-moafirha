
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('proj_project_files_project_id');?></th>
												<th><?php echo lang('proj_project_files_project_sub_id');?></th>
												<th><?php echo lang('proj_project_files_level_id');?></th>
												<th><?php echo lang('proj_project_files_task_id');?></th>
												<th><?php echo lang('proj_project_files_ar_title');?></th>
												<th><?php echo lang('proj_project_files_en_title');?></th>
												<th><?php echo lang('proj_project_files_image');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$store_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
												<?php
												$project_id = $DataRows_Row->project_id;
												$ProjectData = $this->M_proj_project->GetRow($project_id);
												if(!empty($ProjectData)){
												if ($this->session->userdata('lang') == "ar")
												{
													echo $ProjectData->ar_title;
												}
												else
												{
													echo $ProjectData->en_title;
												}}
												else {
													echo "---";
												}
												?>
												</td>
												<td>
												<?php
												// $this->load->model("project/M_proj_project_files");

												$project_id = $DataRows_Row->sub_project_id;
												$ProjectData = $this->M_proj_sub_projects->GetRow($project_id);
												if(!empty($ProjectData)){
													echo $ProjectData->sup_proj_name;
												}
												else {
													echo "---";
												}
												?>
												</td>
												<td>
												<?php
												$level_id = $DataRows_Row->level_id;
												$LevelData = $this->M_proj_project_level->GetRow($level_id);
												if(!empty($LevelData)){

												if ($this->session->userdata('lang') == "ar")
												{
													echo $LevelData->ar_title;
												}
												else
												{
													echo $LevelData->en_title;
												}
												}
												else {
													echo "---";
												}
												?>
												</td>
												<td>
												<?php
												$task_id = $DataRows_Row->task_id;
												$TaskData = $this->M_proj_project_level_task->GetRow($task_id);
												if(!empty($TaskData)){

												if ($this->session->userdata('lang') == "ar")
												{
													echo $TaskData->ar_details;
												}
												else
												{
													echo $TaskData->en_details;
												}
											}else {
												echo "---";
											}
												?>
												</td>
												<td><?php
												if(!empty($DataRows_Row->ar_title)){

												 echo $DataRows_Row->ar_title;
											 }
												 else {
 													echo "---";
 												}
												 ?></td>
												<td><?php
												if(!empty($DataRows_Row->en_title)){

												 echo $DataRows_Row->en_title;
											 }
												 else {
 													echo "---";
 												}
												 ?></td>
												<td>
												<a href="<?php echo base_url()."upload/project/".$DataRows_Row->image;?>" target="_blank"><?php echo lang('proj_project_files_image');?></a>
												</td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i>
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i>
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i>
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>

									</table>
