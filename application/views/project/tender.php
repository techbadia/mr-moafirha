
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('proj_project_customer_id');?></th>
												<th><?php echo lang('proj_project_status_id');?></th>
												<th><?php echo lang('proj_project_manager_id');?></th>
												<th><?php echo lang('proj_project_title');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('proj_project_convert');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$store_id = $DataRows_Row->id;
												$status_id = $DataRows_Row->status_id;
												if($status_id == 1) {
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
												<?php
												$customer_id = $DataRows_Row->customer_id;
												$CustomerData = $this->M_sale_customer->GetRow($customer_id);
												if ($this->session->userdata('lang') == "ar")
												{
													echo $CustomerData->ar_title;
												}
												else
												{
													echo $CustomerData->en_title;
												}
												?>
												</td>
												<td>
												<?php
												$status_id = $DataRows_Row->status_id;
												$StatusData = $this->M_proj_status->GetRow($status_id);
												if ($this->session->userdata('lang') == "ar")
												{
													echo $StatusData->ar_title;
												}
												else
												{
													echo $StatusData->en_title;
												}
												?>
												</td>
												<td>
												<?php
												$manager_id = $DataRows_Row->manager_id;
												$ManagerData = $this->M_usr_users->GetRow($manager_id);
												echo $ManagerData->fullname;
												?>
												</td>
												<td>
												<?php
												if ($this->session->userdata('lang') == "ar")
												{
													$ProjectName = $DataRows_Row->ar_title;
												}
												else
												{
													$ProjectName = $DataRows_Row->en_title;
												}
												?>
												<a target="_blank" href="<?php echo base_url().$Segment1."/proj_project/view_project/".$DataRows_Row->id;?>"><?php echo $ProjectName;?></a>
												</td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/convert/".$DataRows_Row->id;?>">
														<i class="flaticon2-start-up text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
												}
											}
											?>
										</tbody>
										
									</table>

								