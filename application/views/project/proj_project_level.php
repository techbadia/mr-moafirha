
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('proj_project_level_project_id');?></th>
												<th><?php echo lang('proj_project_level_manager_id');?></th>
												<th><?php echo lang('proj_project_level_status_id');?></th>
												<th><?php echo lang('proj_project_files_ar_title');?></th>
												<th><?php echo lang('proj_project_files_en_title');?></th>
												<th><?php echo lang('proj_project_level_start_date');?></th>
												<th><?php echo lang('proj_project_level_end_date');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
												<?php
												$project_id = $DataRows_Row->project_id;
												$ProjectData = $this->M_proj_project->GetRow($project_id);
												if ($this->session->userdata('lang') == "ar")
												{
													echo $ProjectData->ar_title;
												}
												else
												{
													echo $ProjectData->en_title;
												}
												?>
												</td>
												<td>
												<?php
												$manager_id = $DataRows_Row->manager_id;
												$ManagerData = $this->M_usr_users->GetRow($manager_id);
												echo $ManagerData->fullname;
												?>
												</td>
												<td>
												<?php
												$status_id = $DataRows_Row->status_id;
												$StatusData = $this->M_proj_status->GetRow($status_id);
												if ($this->session->userdata('lang') == "ar")
												{
													echo $StatusData->ar_title;
												}
												else
												{
													echo $StatusData->en_title;
												}
												?>
												</td>
												<td><?php echo $DataRows_Row->ar_title;?></td>
												<td><?php echo $DataRows_Row->en_title;?></td>
												<td><?php echo $DataRows_Row->start_date;?></td>
												<td><?php echo $DataRows_Row->end_date;?></td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

								