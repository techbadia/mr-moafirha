<?php
if ($this->session->userdata('lang') == "ar")
{
    $ProjectName = $ar_title;
    $ProjectDetails = $ar_details;
    $CustomerName = $Customer_ar_title;
    $StatusName = $Status_ar_title;
}
else
{
    $ProjectName = $en_title;
    $ProjectDetails = $en_details;
    $CustomerName = $Customer_en_title;
    $StatusName = $Status_en_title;
}
$ProjectImage = $image;
function formatcurrencys($floatcurr, $curr = "SAR"){
        $currencies['ARS'] = array(2,',','.');          //  Argentine Peso
        $currencies['AMD'] = array(2,'.',',');          //  Armenian Dram
        $currencies['AWG'] = array(2,'.',',');          //  Aruban Guilder
        $currencies['AUD'] = array(2,'.',' ');          //  Australian Dollar
        $currencies['BSD'] = array(2,'.',',');          //  Bahamian Dollar
        $currencies['BHD'] = array(3,'.',',');          //  Bahraini Dinar
        $currencies['BDT'] = array(2,'.',',');          //  Bangladesh, Taka
        $currencies['BZD'] = array(2,'.',',');          //  Belize Dollar
        $currencies['BMD'] = array(2,'.',',');          //  Bermudian Dollar
        $currencies['BOB'] = array(2,'.',',');          //  Bolivia, Boliviano
        $currencies['BAM'] = array(2,'.',',');          //  Bosnia and Herzegovina, Convertible Marks
        $currencies['BWP'] = array(2,'.',',');          //  Botswana, Pula
        $currencies['BRL'] = array(2,',','.');          //  Brazilian Real
        $currencies['BND'] = array(2,'.',',');          //  Brunei Dollar
        $currencies['CAD'] = array(2,'.',',');          //  Canadian Dollar
        $currencies['KYD'] = array(2,'.',',');          //  Cayman Islands Dollar
        $currencies['CLP'] = array(0,'','.');           //  Chilean Peso
        $currencies['CNY'] = array(2,'.',',');          //  China Yuan Renminbi
        $currencies['COP'] = array(2,',','.');          //  Colombian Peso
        $currencies['CRC'] = array(2,',','.');          //  Costa Rican Colon
        $currencies['HRK'] = array(2,',','.');          //  Croatian Kuna
        $currencies['CUC'] = array(2,'.',',');          //  Cuban Convertible Peso
        $currencies['CUP'] = array(2,'.',',');          //  Cuban Peso
        $currencies['CYP'] = array(2,'.',',');          //  Cyprus Pound
        $currencies['CZK'] = array(2,'.',',');          //  Czech Koruna
        $currencies['DKK'] = array(2,',','.');          //  Danish Krone
        $currencies['DOP'] = array(2,'.',',');          //  Dominican Peso
        $currencies['XCD'] = array(2,'.',',');          //  East Caribbean Dollar
        $currencies['EGP'] = array(2,'.',',');          //  Egyptian Pound
        $currencies['SVC'] = array(2,'.',',');          //  El Salvador Colon
        $currencies['ATS'] = array(2,',','.');          //  Euro
        $currencies['BEF'] = array(2,',','.');          //  Euro
        $currencies['DEM'] = array(2,',','.');          //  Euro
        $currencies['EEK'] = array(2,',','.');          //  Euro
        $currencies['ESP'] = array(2,',','.');          //  Euro
        $currencies['EUR'] = array(2,',','.');          //  Euro
        $currencies['FIM'] = array(2,',','.');          //  Euro
        $currencies['FRF'] = array(2,',','.');          //  Euro
        $currencies['GRD'] = array(2,',','.');          //  Euro
        $currencies['IEP'] = array(2,',','.');          //  Euro
        $currencies['ITL'] = array(2,',','.');          //  Euro
        $currencies['LUF'] = array(2,',','.');          //  Euro
        $currencies['NLG'] = array(2,',','.');          //  Euro
        $currencies['PTE'] = array(2,',','.');          //  Euro
        $currencies['GHC'] = array(2,'.',',');          //  Ghana, Cedi
        $currencies['GIP'] = array(2,'.',',');          //  Gibraltar Pound
        $currencies['GTQ'] = array(2,'.',',');          //  Guatemala, Quetzal
        $currencies['HNL'] = array(2,'.',',');          //  Honduras, Lempira
        $currencies['HKD'] = array(2,'.',',');          //  Hong Kong Dollar
        $currencies['HUF'] = array(0,'','.');           //  Hungary, Forint
        $currencies['ISK'] = array(0,'','.');           //  Iceland Krona
        $currencies['INR'] = array(2,'.',',');          //  Indian Rupee
        $currencies['IDR'] = array(2,',','.');          //  Indonesia, Rupiah
        $currencies['IRR'] = array(2,'.',',');          //  Iranian Rial
        $currencies['JMD'] = array(2,'.',',');          //  Jamaican Dollar
        $currencies['JPY'] = array(0,'',',');           //  Japan, Yen
        $currencies['JOD'] = array(3,'.',',');          //  Jordanian Dinar
        $currencies['KES'] = array(2,'.',',');          //  Kenyan Shilling
        $currencies['KWD'] = array(3,'.',',');          //  Kuwaiti Dinar
        $currencies['LVL'] = array(2,'.',',');          //  Latvian Lats
        $currencies['LBP'] = array(0,'',' ');           //  Lebanese Pound
        $currencies['LTL'] = array(2,',',' ');          //  Lithuanian Litas
        $currencies['MKD'] = array(2,'.',',');          //  Macedonia, Denar
        $currencies['MYR'] = array(2,'.',',');          //  Malaysian Ringgit
        $currencies['MTL'] = array(2,'.',',');          //  Maltese Lira
        $currencies['MUR'] = array(0,'',',');           //  Mauritius Rupee
        $currencies['MXN'] = array(2,'.',',');          //  Mexican Peso
        $currencies['MZM'] = array(2,',','.');          //  Mozambique Metical
        $currencies['NPR'] = array(2,'.',',');          //  Nepalese Rupee
        $currencies['ANG'] = array(2,'.',',');          //  Netherlands Antillian Guilder
        $currencies['ILS'] = array(2,'.',',');          //  New Israeli Shekel
        $currencies['TRY'] = array(2,'.',',');          //  New Turkish Lira
        $currencies['NZD'] = array(2,'.',',');          //  New Zealand Dollar
        $currencies['NOK'] = array(2,',','.');          //  Norwegian Krone
        $currencies['PKR'] = array(2,'.',',');          //  Pakistan Rupee
        $currencies['PEN'] = array(2,'.',',');          //  Peru, Nuevo Sol
        $currencies['UYU'] = array(2,',','.');          //  Peso Uruguayo
        $currencies['PHP'] = array(2,'.',',');          //  Philippine Peso
        $currencies['PLN'] = array(2,'.',' ');          //  Poland, Zloty
        $currencies['GBP'] = array(2,'.',',');          //  Pound Sterling
        $currencies['OMR'] = array(3,'.',',');          //  Rial Omani
        $currencies['RON'] = array(2,',','.');          //  Romania, New Leu
        $currencies['ROL'] = array(2,',','.');          //  Romania, Old Leu
        $currencies['RUB'] = array(2,',','.');          //  Russian Ruble
        $currencies['SAR'] = array(2,'.',',');          //  Saudi Riyal
        $currencies['SGD'] = array(2,'.',',');          //  Singapore Dollar
        $currencies['SKK'] = array(2,',',' ');          //  Slovak Koruna
        $currencies['SIT'] = array(2,',','.');          //  Slovenia, Tolar
        $currencies['ZAR'] = array(2,'.',' ');          //  South Africa, Rand
        $currencies['KRW'] = array(0,'',',');           //  South Korea, Won
        $currencies['SZL'] = array(2,'.',', ');         //  Swaziland, Lilangeni
        $currencies['SEK'] = array(2,',','.');          //  Swedish Krona
        $currencies['CHF'] = array(2,'.','\'');         //  Swiss Franc
        $currencies['TZS'] = array(2,'.',',');          //  Tanzanian Shilling
        $currencies['THB'] = array(2,'.',',');          //  Thailand, Baht
        $currencies['TOP'] = array(2,'.',',');          //  Tonga, Paanga
        $currencies['AED'] = array(2,'.',',');          //  UAE Dirham
        $currencies['UAH'] = array(2,',',' ');          //  Ukraine, Hryvnia
        $currencies['USD'] = array(2,'.',',');          //  US Dollar
        $currencies['VUV'] = array(0,'',',');           //  Vanuatu, Vatu
        $currencies['VEF'] = array(2,',','.');          //  Venezuela Bolivares Fuertes
        $currencies['VEB'] = array(2,',','.');          //  Venezuela, Bolivar
        $currencies['VND'] = array(0,'','.');           //  Viet Nam, Dong
        $currencies['ZWD'] = array(2,'.',' ');          //  Zimbabwe Dollar





            return number_format($floatcurr,$currencies[$curr][0],$currencies[$curr][1],$currencies[$curr][2]);

    }

    function formatinrs($input){
        //CUSTOM FUNCTION TO GENERATE ##,##,###.##
        $dec = "";
        $pos = strpos($input, ".");
        if ($pos === false){
            //no decimals
        } else {
            //decimals
            $dec = substr(round(substr($input,$pos),2),1);
            $input = substr($input,0,$pos);
        }
        $num = substr($input,-3); //get the last 3 digits
        $input = substr($input,0, -3); //omit the last 3 digits already stored in $num
        while(strlen($input) > 0) //loop the process - further get digits 2 by 2
        {
            $num = substr($input,-2).",".$num;
            $input = substr($input,0,-2);
        }
        return $num . $dec;
    }

    // echo formatcurrencys(39174.00000000001);             //1,000,045.25 (USD)

?>

<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid" style="padding-right: 0px; padding-left: 0px;">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <div class="card-body" style="padding-right: 5px; padding-left: 5px;">
                <div class="d-flex">
                    <!--begin: Pic-->
                    <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                        <div class="symbol symbol-50 symbol-lg-120">
                            <img alt="<?php echo $ProjectName;?>" src="<?php echo base_url()."upload/project/".$ProjectImage;?>" />
                        </div>
                        <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                            <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
                        </div>
                    </div>
                    <!--end: Pic-->
                    <!--begin: Info-->
                    <div class="flex-grow-1">
                        <!--begin: Title-->
                        <div class="d-flex align-items-center justify-content-between flex-wrap">
                            <div class="mr-3">
                                <!--begin::Name-->
                                <a href="#" class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">
                                <?php echo $ProjectName;?>
                                <i class="flaticon2-correct text-success icon-md ml-2"></i>
                                </a>
                                <!--end::Name-->
                                <!--begin::Contacts-->
                                <div class="d-flex flex-wrap my-2">
                                    <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-notification.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24" />
                                                <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000" />
                                                <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5" />
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span><?php echo $CustomerName." : ".$Customer_email;?></a>
                                    <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/General/Lock.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <mask fill="white">
                                                    <use xlink:href="#path-1" />
                                                </mask>
                                                <g />
                                                <path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000" />
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span><?php echo lang('proj_project_manager_id')." : ".$Manager;?></a>
                                    <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Map/Marker2.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <path d="M4.00246329,12.2004927 L13,14 L13,4.06189375 C16.9463116,4.55399184 20,7.92038235 20,12 C20,16.418278 16.418278,20 12,20 C7.64874861,20 4.10886412,16.5261253 4.00246329,12.2004927 Z" fill="#000000" opacity="0.3"/>
                                                <path d="M3.0603968,10.0120794 C3.54712466,6.05992157 6.91622084,3 11,3 L11,11.6 L3.0603968,10.0120794 Z" fill="#000000"/>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span><?php echo $StatusName;?></a>
                                    <?php /* ?>

                                    <a href="<?php echo base_url().$Segment1."/proj_project/proj_project_devices/".$project_id;?>" target="_blank" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Map/Marker2.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <path d="M4,7 L20,7 C21.1045695,7 22,7.8954305 22,9 L22,19 C22,20.1045695 21.1045695,21 20,21 L4,21 C2.8954305,21 2,20.1045695 2,19 L2,9 C2,7.8954305 2.8954305,7 4,7 Z M16,18 C18.209139,18 20,16.209139 20,14 C20,11.790861 18.209139,10 16,10 C13.790861,10 12,11.790861 12,14 C12,16.209139 13.790861,18 16,18 Z" fill="#000000" opacity="0.3"/>
                                                <path d="M20.6185961,7.09750181 C20.4238038,7.03420884 20.2158968,7 20,7 L17.2014013,7 L8.01276516,1.87327098 C7.53047091,1.60417861 7.35763664,0.995059403 7.62672902,0.512765158 C7.89582139,0.0304709116 8.5049406,-0.142363355 8.98723484,0.126729018 L20.1117466,6.33356496 C20.4052818,6.49734071 20.5841864,6.78706979 20.6185961,7.09750181 Z" fill="#000000"/>
                                                <path d="M16,16 C17.1045695,16 18,15.1045695 18,14 C18,12.8954305 17.1045695,12 16,12 C14.8954305,12 14,12.8954305 14,14 C14,15.1045695 14.8954305,16 16,16 Z" fill="#000000"/>
                                                <rect fill="#000000" opacity="0.3" x="4" y="11" width="5" height="2" rx="1"/>
                                                <rect fill="#000000" opacity="0.3" x="4" y="15" width="5" height="2" rx="1"/>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span><?php echo lang('proj_project_devices');?></a>
                                    <?php */ ?>
                                    <a href="<?php echo base_url().$Segment1."/proj_project/proj_sub_projects/".$project_id;?>" target="_blank" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Map/Marker2.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"></rect>
        <path d="M14.8520384,9 L15.7780576,12 L8.22196243,12 L9.14797495,9 L14.8520384,9 Z M13.9260192,6 L10.0739875,6 L10.7050601,3.95551581 C10.8804029,3.38745846 11.4054966,3 12,3 C12.5945036,3 13.1195978,3.38745798 13.2949418,3.95551522 L13.9260192,6 Z M16.7040768,15 L17.9387691,19 L6.06126654,19 L7.2959499,15 L16.7040768,15 Z" fill="#000000"></path>
        <rect fill="#000000" opacity="0.3" x="3" y="20" width="18" height="2" rx="1"></rect>
    </g>
</svg>
                                        <!--end::Svg Icon-->
                                    </span><?php echo lang('proj_sub_projects');?></a>
                                </div>
                                <!--end::Contacts-->
                            </div>
                            <!--
                            <div class="my-lg-0 my-1">
                                <a href="<?php //echo base_url().$Segment1."/proj_project/earning/".$project_id;?>" target="_blank" class="btn btn-sm btn-success font-weight-bolder">
                                    <?php //echo lang('proj_project_Earnings');?>
                                </a>
                                <a href="<?php //echo base_url().$Segment1."/proj_project/expenses/".$project_id;?>" target="_blank" class="btn btn-sm btn-info font-weight-bolder">
                                    <?php //echo lang('proj_project_Expenses');?>
                                </a>
                                <a href="<?php //echo base_url().$Segment1."/proj_project/labor/".$project_id;?>" target="_blank" class="btn btn-sm btn-dark font-weight-bolder">
                                    <?php //echo lang('proj_project_Labor');?>
                                </a>
                                <a href="<?php //echo base_url().$Segment1."/proj_project/material/".$project_id;?>" target="_blank" class="btn btn-sm btn-warning font-weight-bolder">
                                    <?php //echo lang('proj_project_Material');?>
                                </a>
                            </div>
                            -->
                        </div>
                        <!--end: Title-->
                        <!--begin: Content-->
                        <div class="d-flex align-items-center flex-wrap justify-content-between">
                            <div class="flex-grow-1 font-weight-bold text-dark-50 py-5 py-lg-2 mr-5">
                            <?php echo $ProjectDetails;?>
                            </div>
                            <div class="d-flex flex-wrap align-items-center py-2">
                                <div class="d-flex align-items-center mr-10">
                                    <div class="mr-6">
                                        <div class="font-weight-bold mb-2"><?php echo lang('proj_project_start_date');?></div>
                                        <span class="btn btn-sm btn-text btn-light-primary text-uppercase font-weight-bold">
                                            <?php echo $start_date;?>
                                        </span>
                                    </div>
                                    <div class="">
                                        <div class="font-weight-bold mb-2"><?php echo lang('proj_project_end_date');?></div>
                                        <span class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold">
                                            <?php echo $end_date;?>
                                        </span>
                                    </div>
                                </div>
                                <!--
                                <div class="flex-grow-1 flex-shrink-0 w-150px w-xl-300px mt-4 mt-sm-0">
                                    <span class="font-weight-bold"><?php //echo lang('Progress');?></span>
                                    <div class="progress progress-xs mt-2 mb-2">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 63%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="font-weight-bolder text-dark">78%</span>
                                </div>
                                -->

                            </div>
                        </div>
                        <!--end: Content-->
                    </div>
                    <!--end: Info-->
                </div>
                <div class="separator separator-solid my-7"></div>
                <!--begin: Items-->
                <div class="d-flex align-items-center flex-wrap">
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                        <span class="mr-4">
                            <i class="flaticon-coins icon-2x text-muted font-weight-bold"></i>
                        </span>
                        <div class="d-flex flex-column text-dark-75">
                            <span class="font-weight-bolder font-size-sm"><?php echo lang('proj_project_Earnings');?></span>
                            <span class="font-weight-bolder font-size-h5">
                            <?php
                            // setlocale(LC_ALL,"ar_SA");

                          echo  formatcurrencys($total_paid)." ".lang('SAR');
                            ?>
                            </span>
                        </div>
                    </div>
                    <!--end: Item-->
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                        <span class="mr-4">
                            <i class="flaticon-confetti icon-2x text-muted font-weight-bold"></i>
                        </span>
                        <div class="d-flex flex-column text-dark-75">
                            <span class="font-weight-bolder font-size-sm"><?php echo lang('proj_project_TotalExpenses');?></span>
                            <span class="font-weight-bolder font-size-h5">
                            <?php
                            echo formatcurrencys($total_expenses)." ".lang('SAR');

                            ?>
                            </span>
                        </div>
                    </div>
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                        <span class="mr-4">
                            <i class="flaticon-confetti icon-2x text-muted font-weight-bold"></i>
                        </span>
                        <div class="d-flex flex-column text-dark-75">
                            <span class="font-weight-bolder font-size-sm"><?php echo lang('proj_sub_project_TotalExpenses');?></span>
                            <span class="font-weight-bolder font-size-h5">
                            <?php
                            echo formatcurrencys($total_sup_expenses)." ".lang('SAR');
                            ?>
                            </span>
                        </div>
                    </div>
                    <!--end: Item-->
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                        <span class="mr-4">
                            <i class="flaticon-pie-chart icon-2x text-muted font-weight-bold"></i>
                        </span>
                        <div class="d-flex flex-column text-dark-75">
                            <span class="font-weight-bolder font-size-sm"><?php echo lang('proj_project_Net');?></span>
                            <span class="font-weight-bolder font-size-h5">
                            <?php
                            $totalEx = $total_sup_expenses + $total_expenses;
                            $Net = doubleval($total_paid) - doubleval($totalEx);
                            echo formatcurrencys(doubleval($Net))." ".lang('SAR');
                            ?>
                            </span>
                        </div>
                    </div>
                    <!--end: Item-->
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                        <span class="mr-4">
                            <i class="flaticon-coins icon-2x text-muted font-weight-bold"></i>
                        </span>
                        <div class="d-flex flex-column text-dark-75">
                            <span class="font-weight-bolder font-size-sm"><?php echo lang('proj_project_amount');?></span>
                            <span class="font-weight-bolder font-size-h5">
                            <?php
                              echo formatcurrencys($amount)." ".lang('SAR');
                            ?>
                            </span>
                        </div>
                    </div>
                    <!--end: Item-->
                    <!--begin: Item-->
                    <?php /* ?>
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                        <span class="mr-4">
                            <i class="flaticon-file-2 icon-2x text-muted font-weight-bold"></i>
                        </span>
                        <div class="d-flex flex-column flex-lg-fill">
                            <span class="text-dark-75 font-weight-bolder font-size-sm">
                            <?php
                            $HowManyTasks = $this->M_proj_project_level_task->GetCountByProject($project_id);
                            echo $HowManyTasks;
                            ?> <?php echo lang('tasks');?>
                            </span>
                            <a href="<?php echo base_url().$Segment1."/proj_project_level_task/project/".$project_id;?>" target="_blank" class="text-primary font-weight-bolder"><?php echo lang('View');?></a>
                        </div>
                    </div>
                    <?php */ ?>
                    <!--end: Item-->
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                        <span class="mr-4">
                            <i class="flaticon-layer icon-2x text-muted font-weight-bold"></i>
                        </span>
                        <div class="d-flex flex-column">
                            <span class="text-dark-75 font-weight-bolder font-size-sm">
                            <?php
                            $HowManyLevel = $this->M_proj_project_level->GetCountByProject($project_id);
                            echo $HowManyLevel;
                            ?> <?php echo lang('tasks_levels');?>
                            </span>
                            <a href="<?php echo base_url().$Segment1."/proj_project_level/project/".$project_id;?>" target="_blank" class="text-primary font-weight-bolder"><?php echo lang('View');?></a>
                        </div>
                    </div>

                    <!--end: Item-->
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                        <span class="mr-4">
                            <i class="flaticon2-image-file icon-2x text-muted font-weight-bold"></i>
                        </span>
                        <div class="d-flex flex-column">
                            <span class="text-dark-75 font-weight-bolder font-size-sm">
                            <?php
                            $HowManyFiles = $this->M_proj_project_files->GetFileCount($project_id);
                            echo $HowManyFiles;
                            ?> <?php echo lang('proj_project_files');?>
                            </span>
                            <a href="<?php echo base_url().$Segment1."/proj_project_files/project/".$project_id;?>" target="_blank" class="text-primary font-weight-bolder"><?php echo lang('View');?></a>
                        </div>
                    </div>
                    <!--end: Item-->
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill my-1">
                        <span class="mr-4">
                            <i class="flaticon-network icon-2x text-muted font-weight-bold"></i>
                        </span>
                        <div class="symbol-group symbol-hover">
                            <?php
                            $GetUsers = $this->M_proj_project_level_task->GetUsersByProject($project_id);
                            foreach($GetUsers as $GetUsers_Row) {
                                $UserID = $GetUsers_Row->user_id;
                                $UserData = $this->M_usr_users->GetRow($UserID);
                                $UserImage = $UserData->image;
                            ?>
                            <div class="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="<?php echo $UserData->fullname;?>">
                                <img alt="Pic" src="<?php echo base_url();?>upload/usr_users/<?php echo $UserImage;?>" />
                            </div>
                            <?php
                            }
                            ?>
                            <?php
                            $DisplayNo = 0;
                            $GetUsersCount = $this->M_proj_project_level_task->GetCountUsersByProject($project_id);
                            if($GetUsersCount > 3) {
                            ?>
                            <div class="symbol symbol-30 symbol-circle symbol-light">
                                <span class="symbol-label font-weight-bold">
                                <?php
                                    $DisplayNo = $GetUsersCount - 3;
                                    echo $DisplayNo."+";
                                ?>
                                </span>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <!--end: Item-->
                </div>
                <!--begin: Items-->
            </div>
        </div>
        <!--end::Card-->
        <!--begin::Row-->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Advance Table Widget 3-->
                <div class="card card-custom card-stretch gutter-b">
                    <!--begin::Header-->
                    <div class="card-header border-0 py-5">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label font-weight-bolder text-dark"><?php echo lang('proj_sub_projects');?></span>
                        </h3>
                        <div class="card-toolbar">
                            <!--
                            <a href="<?php //echo base_url().$Segment1."/proj_project/addstaff/".$project_id;?>" class="btn btn-danger font-weight-bolder font-size-sm">
                            <span class="svg-icon svg-icon-md svg-icon-white">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"/>
                                        <path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z" fill="#000000" opacity="0.3" transform="translate(11.500000, 12.000000) rotate(-345.000000) translate(-11.500000, -12.000000) "/>
                                        <path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z M11.5,14 C12.6045695,14 13.5,13.1045695 13.5,12 C13.5,10.8954305 12.6045695,10 11.5,10 C10.3954305,10 9.5,10.8954305 9.5,12 C9.5,13.1045695 10.3954305,14 11.5,14 Z" fill="#000000"/>
                                    </g>
                                </svg>
                            </span><?php //echo lang('mon_exchange_bonds');?></a>&nbsp;
                            <a href="<?php //echo base_url().$Segment1."/proj_project/addstaff/".$project_id;?>" class="btn btn-success font-weight-bolder font-size-sm">
                            <span class="svg-icon svg-icon-md svg-icon-white">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"/>
                                        <path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z" fill="#000000" opacity="0.3" transform="translate(11.500000, 12.000000) rotate(-345.000000) translate(-11.500000, -12.000000) "/>
                                        <path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z M11.5,14 C12.6045695,14 13.5,13.1045695 13.5,12 C13.5,10.8954305 12.6045695,10 11.5,10 C10.3954305,10 9.5,10.8954305 9.5,12 C9.5,13.1045695 10.3954305,14 11.5,14 Z" fill="#000000"/>
                                    </g>
                                </svg>
                            </span><?php //echo lang('mon_receipts');?></a>
                            -->
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-0 pb-3">
                        <!--begin::Table-->
                        <div class="table-responsive">
                            <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th><?php echo lang('proj_sup_project_name');?></th>
                                        <th><?php echo lang('proj_sub_project_incomes');?></th>
                                        <th><?php echo lang('proj_sub_project_expenses');?></th>
                                        <th><?php echo lang('proj_sub_project_net');?></th>
                                        <th><?php echo lang('files');?></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $counter = 1;
                                    foreach($subs as $sub) {
                                      $net = $sub->paid - $sub->total_sup_expenses;
                                      $HowManyFiles = $this->M_proj_project_files->GetSubFileCount($sub->id);
                                      $totalIncomes += $sub->paid;
                                      $totalExp += $sub->total_sup_expenses;
                                      $totalnet += $net;
                                    ?>
                                    <tr>
                                        <td>
                                        <?php echo $counter;?>
                                        </td>
                                        <td><?php echo $sub->sup_proj_name;?></td>
                                        <td><?php echo $sub->paid;?></td>
                                        <td><?php echo $sub->total_sup_expenses;?></td>
                                        <td><?php echo $sub->paid - $sub->total_sup_expenses;?></td>
                                        <td><a href="<?php echo base_url().$Segment1."/proj_project_files/sub_project/".$sub->id;?>" target="_blank" class="text-primary font-weight-bolder"><?php echo lang('View')." (".$HowManyFiles.")";?></a></td>

                                    </tr>
                                    <?php
                                    $counter++;
                                    }
                                    ?>
                                    <tr style="
    background: #f3f6f9 ;font-weight: 600;
    color: #181c32 !important;
    font-size: 1rem;
    text-transform: none;
    letter-spacing: 0px !important;
">
                                      <td colspan="2"><?php echo lang('Total');?></td>
                                      <td><?php echo $totalIncomes ?></td>
                                      <td><?php echo $totalExp ?></td>
                                      <td colspan="2"><?php echo $totalnet ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--end::Table-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Advance Table Widget 3-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Advance Table Widget 3-->
                <div class="card card-custom card-stretch gutter-b">
                    <!--begin::Header-->
                    <div class="card-header border-0 py-5">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label font-weight-bolder text-dark"><?php echo lang('fin_journal');?></span>
                        </h3>
                        <div class="card-toolbar">
                            <!--
                            <a href="<?php //echo base_url().$Segment1."/proj_project/addstaff/".$project_id;?>" class="btn btn-danger font-weight-bolder font-size-sm">
                            <span class="svg-icon svg-icon-md svg-icon-white">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"/>
                                        <path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z" fill="#000000" opacity="0.3" transform="translate(11.500000, 12.000000) rotate(-345.000000) translate(-11.500000, -12.000000) "/>
                                        <path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z M11.5,14 C12.6045695,14 13.5,13.1045695 13.5,12 C13.5,10.8954305 12.6045695,10 11.5,10 C10.3954305,10 9.5,10.8954305 9.5,12 C9.5,13.1045695 10.3954305,14 11.5,14 Z" fill="#000000"/>
                                    </g>
                                </svg>
                            </span><?php //echo lang('mon_exchange_bonds');?></a>&nbsp;
                            <a href="<?php //echo base_url().$Segment1."/proj_project/addstaff/".$project_id;?>" class="btn btn-success font-weight-bolder font-size-sm">
                            <span class="svg-icon svg-icon-md svg-icon-white">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"/>
                                        <path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z" fill="#000000" opacity="0.3" transform="translate(11.500000, 12.000000) rotate(-345.000000) translate(-11.500000, -12.000000) "/>
                                        <path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z M11.5,14 C12.6045695,14 13.5,13.1045695 13.5,12 C13.5,10.8954305 12.6045695,10 11.5,10 C10.3954305,10 9.5,10.8954305 9.5,12 C9.5,13.1045695 10.3954305,14 11.5,14 Z" fill="#000000"/>
                                    </g>
                                </svg>
                            </span><?php //echo lang('mon_receipts');?></a>
                            -->
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-0 pb-3">
                        <!--begin::Table-->
                        <div class="table-responsive">
                            <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th><?php echo lang('fin_journal_details');?></th>
                                        <th><?php echo lang('fin_journal_thedate');?></th>
                                        <th><?php echo lang('fin_journal_bill_id');?></th>
                                        <th><?php echo lang('fin_journal_table_name');?></th>
                                        <th><?php echo lang('fin_journal_automatic');?></th>
                                        <th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
                                        <th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
                                        <th style="text-align:center"><?php echo $this->lang->line('View');?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $DataRows = $this->M_fin_journal_main->GetAccountAction($fin_treeaccount_id);
                                    foreach($DataRows as $DataRows_Row) {
                                        $fin_journal_id = $DataRows_Row->id;
                                    ?>
                                    <tr>
                                        <td>
                                        <?php echo $DataRows_Row->id;?>
                                        </td>
                                        <td><?php echo $DataRows_Row->details;?></td>
                                        <td><?php echo $DataRows_Row->thedate;?></td>
                                        <td><?php echo $DataRows_Row->bill_id;?></td>
                                        <td>
                                        <?php
                                        $table_name = $DataRows_Row->table_name;
                                        if($table_name == "buy_bill")
                                        {
                                            echo lang('buy_bill');
                                        }
                                        else if($table_name == "buy_returns")
                                        {
                                            echo lang('buy_returns');
                                        }
                                        else if($table_name == "expenses")
                                        {
                                            echo lang('expenses');
                                        }
                                        else if($table_name == "sale_bill")
                                        {
                                            echo lang('sale_bill');
                                        }
                                        else if($table_name == "sale_returns")
                                        {
                                            echo lang('sale_returns');
                                        }
                                        else
                                        {
                                            echo lang('fin_journal_manual');
                                        }
                                        ?>
                                        </td>
                                        <td>
                                            <?php
                                            $automatic = $DataRows_Row->automatic;
                                            if($automatic == 1)
                                            {
                                                echo lang('Yes');
                                            }
                                            else
                                            {
                                                echo lang('No');
                                            }
                                            ?>
                                        </td>
                                        <td style="text-align:center">
                                            <?php
                                                if($DataRows_Row->deleted == 0) {
                                            ?>
                                            <a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
                                                <i class="flaticon-delete text-danger icon-lg"></i>
                                            </a>
                                            <?php } ?>
                                        </td>
                                        <td style="text-align:center">
                                            <?php
                                                if($DataRows_Row->deleted == 1) {
                                            ?>
                                                <a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
                                                    <i class="flaticon-refresh text-success icon-lg"></i>
                                                </a>
                                            <?php } ?>
                                        </td>
                                        <td style="text-align:center">
                                        <a class="btn btn-danger font-weight-bold mr-2 btn-sm" target="_blank" href="<?php echo base_url().$Segment1."/".$Segment2."/view_pdf/".$DataRows_Row->id;?>/1">PDF</a>
                                        <button class="btn btn-primary view_detail" relid="<?php echo $DataRows_Row->id;?>"><i class="fa fa-eye"></i></button>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <!--end::Table-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Advance Table Widget 3-->
            </div>
          </div>
        <!--end::Row-->
    </div>
    <!--end::Container-->
</div>
<!--end::Entry-->


<div id="show_modal" class="modal fade" role="dialog" style="background: #000;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h3 style="font-size: 24px; color: #17919e; text-shadow: 1px 1px #ccc;">
				<i class="fa fa-folder"></i><?php echo lang('fin_journal');?>
				</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3"><?php echo lang('fin_journal_details');?></div>
					<div class="col-md-9" id="details"></div>
				</div>
				<div class="row">
					<div class="col-md-3"><?php echo lang('fin_journal_thedate');?></div>
					<div class="col-md-9" id="thedate"></div>
				</div>
				<table id="ModalTable" class="table table-bordered table-striped">
					<thead class="btn-primary">
						<tr>
							<th align='right'><?php echo lang('fin_journal_account_id');?></th>
							<th style="text-align:center"><?php echo lang('fin_journal_debit');?></th>
							<th style="text-align:center"><?php echo lang('fin_journal_creditor');?></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo lang('Close');?></button>
				<button type="button" class="btn btn-info" onclick="printSelection(document.getElementById('show_modal'));return false;"><i class="fa fa-print"></i> <?php echo $this->lang->line('Print');?></button>
			</div>
		</div>
	</div>
</div>
