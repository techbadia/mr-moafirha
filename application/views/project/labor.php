
									<div><h3>
									<?php
									if ($this->session->userdata('lang') == "ar")
									{
										$ProjectName = $ar_title;
									}
									else
									{
										$ProjectName = $en_title;
									}
									echo $ProjectName." :: ".lang('proj_project_Labor');
									?>
									</h3></div>
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('fin_treeaccount_title');?></th>
												<th><?php echo lang('fin_treeaccount_title_en');?></th>
												<th><?php echo lang('fin_journal_details');?></th>
												<th><?php echo lang('fin_journal_thedate');?></th>
												<th><?php echo lang('fin_journal_debit');?></th>
												<th><?php echo lang('fin_journal_creditor');?></th>
												<th style="text-align:center">...</th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$debit = $DataRows_Row->creditor;
												$creditor = $DataRows_Row->creditor;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td><?php echo $DataRows_Row->title;?></td>
												<td><?php echo $DataRows_Row->title_en;?></td>
												<td><?php echo $DataRows_Row->details;?></td>
												<td><?php echo $DataRows_Row->thedate;?></td>
												<td><?php echo $DataRows_Row->debit;?></td>
												<td><?php echo $DataRows_Row->creditor;?></td>
												<td style="text-align:center"></td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

								