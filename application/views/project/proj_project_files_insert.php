
<div class="card card-custom gutter-b example example-compact">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
                        echo form_open_multipart($FormPath);
                    ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_files_project_id');?></label>
												<select name="project_id" id="project_id" class="form-control">
													<?php
													$ProjectList = $this->M_proj_project->GetMultiRow();
													foreach($ProjectList as $ProjectList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$Project = $ProjectList_Row->ar_title;
														}
														else
														{
															$Project = $ProjectList_Row->en_title;
														}
													?>
													<option value="<?php echo $ProjectList_Row->id;?>"><?php echo $Project;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_files_level_id');?></label>
												<select name="level_id" id="level_id" class="form-control">
													<?php
													$LevelList = $this->M_proj_project_level->GetMultiRow();
													foreach($LevelList as $LevelList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$Level = $LevelList_Row->ar_title;
														}
														else
														{
															$Level = $LevelList_Row->en_title;
														}
													?>
													<option value="<?php echo $LevelList_Row->id;?>" title="<?php echo $LevelList_Row->project_id;?>"><?php echo $Level;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_files_task_id');?></label>
												<select name="task_id" id="task_id" class="form-control">
													<?php
													$TasksList = $this->M_proj_project_level_task->GetMultiRow();
													foreach($TasksList as $TasksList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$Task = $TasksList_Row->ar_details;
														}
														else
														{
															$Task = $TasksList_Row->en_details;
														}
													?>
													<option value="<?php echo $TasksList_Row->id;?>" title="<?php echo $TasksList_Row->level_id;?>"><?php echo $Task;?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_status_ar_title');?></label>
												<input type="text" name="ar_title" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_status_en_title');?></label>
												<input type="text" name="en_title" class="form-control" value="" required>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('Choose_files');?></label>
												<input type="file" name="image" class="form-control" dir="ltr" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<?php
													$company_id = intval($this->session->userdata('company_id'));
													?>
													<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
													<input type="hidden" name="deleted" value="0">

													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>