<?php
$FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
	echo form_open_multipart($FormPath);
?>
	<div class="form-body">
		<div class="form-group row">


			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('expense_items_name');?></label>
				<input type="text" name="item_name" class="form-control" value="<?php echo $item_name ?>" >
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('expense_items_name_en');?></label>
				<input type="text" name="item_name_en" class="form-control" value="<?php echo $item_name_en ?>" >
			</div>


			<input type="hidden" name="id" value="<?php echo $id;?>">







	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<a href="<?php echo base_url()."project/expense_items";?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
<?php
	echo form_close();
?>
