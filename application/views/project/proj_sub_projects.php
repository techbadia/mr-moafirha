<style media="screen">
	#example
	{
		dir:ltr;
	}
</style>
<!--begin: Datatable -->
<table class="table table-separate table-head-custom table-checkable" id="example">
	<thead>
		<tr>
			<th>#</th>
			<th><?php echo lang('proj_sup_project_name');?></th>
			<th><?php echo lang('proj_sub_project_customer');?></th>
			<th><?php echo lang('proj_project_project_id');?></th>
			<th><?php echo lang('proj_sub_project_date_start');?></th>
			<th><?php echo lang('proj_sub_project_date_end');?></th>
			<th><?php echo lang('proj_sub_project_total');?></th>
			<th><?php echo lang('proj_sub_project_paid');?></th>
			<th><?php echo lang('proj_sub_project_remain');?></th>
			<th><?php echo lang('finished');?></th>
			<th><?php echo lang('Actions');?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$Today = date("Y-m-d");
		foreach($DataRows as $DataRows_Row) {
		?>
		<tr>
			<td></td>
			<td><?php echo $DataRows_Row->customer_name;?></td>
			<td><?php echo lang($DataRows_Row->customer_type);?> : <?php echo $DataRows_Row->sup_proj_name;?></td>

			<td>
			<?php
			$project_id = $DataRows_Row->project_id;
			$ProjectData = $this->M_proj_project->GetRow($project_id);
			if ($this->session->userdata('lang') == "ar")
			{
				$ProjectTitle = $ProjectData->ar_title;
			}
			else
			{
				$ProjectTitle = $ProjectData->en_title;
			}
			echo "<a href='".base_url()."project/proj_project/view_project/".$project_id."'>".$ProjectTitle."</a>";
			?>
			</td>
			<td><?php echo $DataRows_Row->date_start;?></td>
			<td><?php echo $DataRows_Row->date_end;?></td>
			<td  style="text-align:center"><?php echo $DataRows_Row->total;?></td>
			<td  style="text-align:center"><?php echo $DataRows_Row->paid;?></td>
			<td  style="text-align:center"><?php echo $DataRows_Row->remain;?></td>
			<td  style="text-align:center"><?php echo ($DataRows_Row->closed == 1)?lang("yes"):lang("no");?></td>
			<td style="text-align:center">
				<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateformSupProj/".$DataRows_Row->id."?pro_id=".$DataRows_Row->project_id;?>">
					<i class="flaticon-edit-1 text-primary icon-lg"></i>
				</a>


				<a href="<?php echo base_url().$Segment1."/".$Segment2."/deleteSupProj/".$DataRows_Row->id;?>" onclick="return confirm('<?php echo lang("Are you sure?") ?>')">
					<i class="flaticon-delete text-danger icon-lg"></i>
				</a>
			</td>
		</tr>
		<?php
		}
		?>
	</tbody>

</table>
