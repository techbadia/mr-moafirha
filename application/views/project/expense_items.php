<style media="screen">
	#example
	{
		dir:ltr;
	}
</style>
<!--begin: Datatable -->
<table class="table table-separate table-head-custom table-checkable" id="example">
	<thead>
		<tr>
			<th>#</th>
			<th><?php echo lang('expense_items_name');?></th>

			<th><?php echo lang('Actions');?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$Today = date("Y-m-d");
		foreach($DataRows as $DataRows_Row) {
		?>
		<tr>
			<td></td>
			<td><?php echo $DataRows_Row->item_name;?></td>



			<td style="text-align:center">
				<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
					<i class="flaticon-edit-1 text-primary icon-lg"></i>
				</a>


				<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>" onclick="return confirm('<?php echo lang("Are you sure?") ?>')">
					<i class="flaticon-delete text-danger icon-lg"></i>
				</a>
			</td>
		</tr>
		<?php
		}
		?>
	</tbody>

</table>
