

                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
                        echo form_open_multipart($FormPath);
                    ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_customer_id');?></label>
												<select name="customer_id" class="form-control">
													<?php
													$CustomerList = $this->M_sale_customer->GetMultiRow();
													foreach($CustomerList as $CustomerList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$Customer = $CustomerList_Row->ar_title;
														}
														else
														{
															$Customer = $CustomerList_Row->en_title;
														}
													?>
													<option <?php if($customer_id == $CustomerList_Row->id) {?>selected<?php }?> value="<?php echo $CustomerList_Row->id;?>"><?php echo $Customer;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_status_id');?></label>
												<select name="status_id" class="form-control">
													<?php
													$StatusList = $this->M_proj_status->GetMultiRow();
													foreach($StatusList as $StatusList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$Status = $StatusList_Row->ar_title;
														}
														else
														{
															$Status = $StatusList_Row->en_title;
														}
													?>
													<option <?php if($status_id == $StatusList_Row->id) {?>selected<?php }?> value="<?php echo $StatusList_Row->id;?>"><?php echo $Status;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_manager_id');?></label>
												<select name="manager_id" class="form-control">
													<?php
													$UsersList = $this->M_usr_users->GetMultiRow();
													foreach($UsersList as $UsersList_Row) {
													?>
													<option <?php if($manager_id == $UsersList_Row->id) {?>selected<?php }?> value="<?php echo $UsersList_Row->id;?>"><?php echo $UsersList_Row->fullname;?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_status_ar_title');?></label>
												<input type="text" name="ar_title" class="form-control" value="<?php echo $ar_title;?>" required>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_status_en_title');?></label>
												<input type="text" name="en_title" class="form-control" value="<?php echo $en_title;?>" required>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_ar_details');?></label>
												<textarea name="ar_details" class="form-control" rows="5" required><?php echo $ar_details;?></textarea>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_en_details');?></label>
												<textarea name="en_details" class="form-control" rows="5" required><?php echo $en_details;?></textarea>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_start_date');?></label>
												<input type="date" name="start_date" class="form-control" value="<?php echo $start_date;?>" required>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_end_date');?></label>
												<input type="date" name="end_date" class="form-control" value="<?php echo $end_date;?>" required>
											</div>
										</div>
                    <div class="append">
                      <?php
                      if(!empty($item_amount)){
                      $olditems = explode("|",trim($item_amount,"|"));
                      $counter = 1;
                          foreach ($olditems as $single) {
                            $class = ($counter == 1)?"clone":"";
                          $single = explode("_",$single);
                       ?>
                    <div class="form-group row <?php echo $class ?>">

                      <div class="col-lg-4 col-md-4 col-sm-12">
                        <label class="col-form-label"><?php echo lang('expense_item');?></label>
                        <select name="items[]" class="form-control">
                          <?php foreach ($items as $item) { ?>
                              <?php
                              if ($this->session->userdata('lang') == "ar")
                              {
                                $itemsss = $item->item_name;
                              }
                              else
                              {
                                $itemsss = $item->item_name_en;
                              }
                               ?>
                          <option <?php echo ($single[0] == $item->id)?"selected":"" ?> value="<?php echo $item->id;?>"><?php echo $itemsss;?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="col-lg-4 col-md-4 col-sm-12">
                        <label class="col-form-label"><?php echo lang('amount');?></label>
                        <input type="text" name="amounts[]" class="form-control" value="<?php echo $single[1] ?>" required>
                      </div>
                      <div class="col-lg-1 col-md-1 col-sm-12">
                        <label class="col-form-label"><?php echo lang('Add_new');?></label>

                        <button type="button" class="btn btn-success  form-control" onclick="addNewItem()"> <i class="fa fa-plus"></i> </button>
                      </div>
                      <div class="col-lg-1 col-md-1 col-sm-12">
                        <label class="col-form-label"><?php echo lang('Delete');?></label>

                        <button type="button" class="btn btn-danger delete form-control" onclick="deleteItem(this)"> <i class="fa fa-minus"></i> </button>
                      </div>
                    </div>
                    <?php 	$counter++; } ?>
                    <?php }
                    if(empty($item_amount)){
                     ?>

                    <div class="form-group row clone">

                      <div class="col-lg-4 col-md-4 col-sm-12">
                        <label class="col-form-label"><?php echo lang('expense_item');?></label>
                        <select name="items[]" class="form-control">
                          <?php foreach ($items as $item) { ?>
                              <?php
                              if ($this->session->userdata('lang') == "ar")
                              {
                                $itemsss = $item->item_name;
                              }
                              else
                              {
                                $itemsss = $item->item_name_en;
                              }
                               ?>
                          <option value="<?php echo $item->id;?>"><?php echo $itemsss;?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="col-lg-4 col-md-4 col-sm-12">
                        <label class="col-form-label"><?php echo lang('amount');?></label>
                        <input type="text" name="amounts[]" class="form-control" value="" required>
                      </div>
                      <div class="col-lg-1 col-md-1 col-sm-12">
                        <label class="col-form-label"><?php echo lang('Add_new');?></label>

                        <button type="button" class="btn btn-success  form-control" onclick="addNewItem()"> <i class="fa fa-plus"></i> </button>
                      </div>
                      <div class="col-lg-1 col-md-1 col-sm-12">
                        <label class="col-form-label"><?php echo lang('Delete');?></label>

                        <button type="button" class="btn btn-danger delete form-control" onclick="deleteItem(this)"> <i class="fa fa-minus"></i> </button>
                      </div>
                    </div>
                <?php } ?>
                    </div>
										<div class="form-group row">
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_labor_cost');?></label>
												<input type="number" name="labor_cost" min="0" step="0.001" class="form-control" value="<?php echo $labor_cost;?>" required>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_material_cost');?></label>
												<input type="number" name="material_cost" min="0" step="0.001" class="form-control" value="<?php echo $material_cost;?>" required>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_total_cost');?></label>
												<input type="number" name="total_cost" min="0" step="0.001" class="form-control" value="<?php echo $total_cost;?>" required>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_amount');?></label>
												<input type="number" name="amount" min="0" step="0.001" class="form-control" value="<?php echo $amount;?>" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php echo $id;?>">
													<?php
													$company_id = intval($this->session->userdata('company_id'));
													?>
													<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
													<input type="hidden" name="deleted" value="<?php echo $id;?>">

													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
