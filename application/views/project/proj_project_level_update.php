

                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
                        echo form_open_multipart($FormPath);
                    ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_level_project_id');?></label>
												<select name="project_id" class="form-control">
													<?php
													$ProjectList = $this->M_proj_project->GetMultiRow();
													foreach($ProjectList as $ProjectList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$Project = $ProjectList_Row->ar_title;
														}
														else
														{
															$Project = $ProjectList_Row->en_title;
														}
													?>
													<option <?php if($project_id == $ProjectList_Row->id) {?>selected<?php }?> value="<?php echo $ProjectList_Row->id;?>"><?php echo $Project;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_level_status_id');?></label>
												<select name="status_id" class="form-control">
													<?php
													$StatusList = $this->M_proj_status->GetMultiRow();
													foreach($StatusList as $StatusList_Row) {
														$status_id = $StatusList_Row->id;
														if($status_id > 1) {
														if ($this->session->userdata('lang') == "ar")
														{
															$Status = $StatusList_Row->ar_title;
														}
														else
														{
															$Status = $StatusList_Row->en_title;
														}
													?>
													<option <?php if($status_id == $StatusList_Row->id) {?>selected<?php }?> value="<?php echo $StatusList_Row->id;?>"><?php echo $Status;?></option>
													<?php
														}
													}
													?>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_level_manager_id');?></label>
												<select name="manager_id" class="form-control">
													<?php
													$UsersList = $this->M_usr_users->GetMultiRow();
													foreach($UsersList as $UsersList_Row) {
													?>
													<option <?php if($manager_id == $UsersList_Row->id) {?>selected<?php }?> value="<?php echo $UsersList_Row->id;?>"><?php echo $UsersList_Row->fullname;?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_status_ar_title');?></label>
												<input type="text" name="ar_title" class="form-control" value="<?php echo $ar_title;?>" required>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_status_en_title');?></label>
												<input type="text" name="en_title" class="form-control" value="<?php echo $en_title;?>" required>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_level_start_date');?></label>
												<input type="date" name="start_date" class="form-control" value="<?php echo $start_date;?>" required>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_level_end_date');?></label>
												<input type="date" name="end_date" class="form-control" value="<?php echo $end_date;?>" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php echo $id;?>">
													<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
													<input type="hidden" name="deleted" value="<?php echo $deleted;?>">

													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                