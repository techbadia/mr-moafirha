<style media="screen">
#overlay{
  position: fixed;
  top: 0;
  z-index: 100;
  width: 100%;
  height:100%;
  display: none;
  background: rgba(0,0,0,0.6);
}
.cv-spinner {
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
}
.spinner {
  width: 40px;
  height: 40px;
  border: 4px #ddd solid;
  border-top: 4px #2e93e6 solid;
  border-radius: 50%;
  animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
  100% {
    transform: rotate(360deg);
  }
}
.is-hide{
  display:none;
}
</style>
<!--begin: Datatable -->
<?php
$_GET["showInCards"] = (!empty($_GET["showInCards"]))?$_GET["showInCards"]:2;
if($_GET["showInCards"] == 2){
 ?>
<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
	<thead>
		<tr>
			<th>ID</th>
			<th><?php echo lang('proj_project_customer_id');?></th>
			<th><?php echo lang('proj_project_status_id');?></th>
			<th><?php echo lang('proj_project_manager_id');?></th>
			<th><?php echo lang('proj_project_title');?></th>
			<th><?php echo lang('proj_project_start_date');?></th>
			<th><?php echo lang('proj_project_end_date');?></th>
			<th><?php echo lang('proj_project_remaining');?></th>
			<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
			<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
			<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$Today = date("Y-m-d");
		foreach($DataRows as $DataRows_Row) {
			$status_id = $DataRows_Row->status_id;
			if($status_id > 1) {
		?>
		<tr>
			<td><?php echo $DataRows_Row->id;?></td>
			<td>
			<?php
			$customer_id = $DataRows_Row->customer_id;
			$CustomerData = $this->M_sale_customer->GetRow($customer_id);
			if ($this->session->userdata('lang') == "ar")
			{
				echo $CustomerData->ar_title;
			}
			else
			{
				echo $CustomerData->en_title;
			}
			?>
			</td>
			<td>
			<?php

			$StatusData = $this->M_proj_status->GetRow($status_id);
			if ($this->session->userdata('lang') == "ar")
			{
				echo $StatusData->ar_title;
			}
			else
			{
				echo $StatusData->en_title;
			}
			?>
			</td>
			<td>
			<?php
			$manager_id = $DataRows_Row->manager_id;
			$ManagerData = $this->M_usr_users->GetRow($manager_id);
			echo $ManagerData->fullname;
			?>
			</td>
			<td>
			<?php
			if ($this->session->userdata('lang') == "ar")
			{
				$ProjectName = $DataRows_Row->ar_title;
			}
			else
			{
				$ProjectName = $DataRows_Row->en_title;
			}
			?>
			<a target="_blank" href="<?php echo base_url().$Segment1."/proj_project/view_project/".$DataRows_Row->id;?>"><?php echo $ProjectName;?></a>
			</td>
			<td><?php echo $DataRows_Row->start_date;?></td>
			<td><?php echo $DataRows_Row->end_date;?></td>
			<td>
			<?php
			$StartDate = $DataRows_Row->start_date;
			$EndDate = $DataRows_Row->end_date;
			if($EndDate >= $Today)
			{
				$diff = abs(strtotime($EndDate) - strtotime($Today));
			}
			else
			{
				$diff = abs(strtotime($Today) - strtotime($EndDate));
			}
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			if($EndDate >= $Today)
			{
				printf("%d يوم", $days);
			}
			else
			{
				echo $this->lang->line('closed');
			}
			?>
			</td>
			<td style="text-align:center">
				<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
					<i class="flaticon-edit-1 text-primary icon-lg"></i>
				</a>
			</td>
			<td style="text-align:center">
				<?php
					if($DataRows_Row->deleted == 0) {
				?>
				<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
					<i class="flaticon-delete text-danger icon-lg"></i>
				</a>
				<?php } ?>
			</td>
			<td style="text-align:center">
				<?php
					if($DataRows_Row->deleted == 1) {
				?>
					<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
						<i class="flaticon-refresh text-success icon-lg"></i>
					</a>
					<a href="<?php echo base_url().$Segment1."/".$Segment2."/hardDelete/".$DataRows_Row->id;?>" onclick="return confirm('<?php echo lang("Are you sure?") ?>')">
						<i class="flaticon-delete text-danger icon-lg"></i>
					</a>
				<?php } ?>
			</td>
		</tr>
		<?php
			}
		}
		?>
	</tbody>

</table>
<?php } else {?>
    <style type="text/css">
        .project-card-class .full-block-a{
            position:absolute;
            width:100%;
            height:100%;
            left:0;
            top:0;
            z-index: 999;
        }
        .project-card-class{
            position:relative;
            margin-bottom: 30px;
        }
        .project-card-class .card{
            margin-top: 30px;
            box-shadow: 0px 1px 4px rgb(0 0 0 / 25%);
            border-radius: 8px;
            background: #fbfbfb;
        }
        .project-card-class .card .card-title{
            margin-bottom:20px;
        }
    </style>
	<div class="row">
    <?php
    foreach ($DataRows as $row){
	    $status_id = $row->status_id;
	    if($status_id > 1) {?>
	    <div class="col-xl-3 project-card-class">
		    <a target="_blank" href="<?php echo base_url().$Segment1."/proj_project/view_project/".$row->id;?>" class="full-block-a"></a>
	        <!--begin::List Widget 9-->
	        <div class="card card-custom card-stretch gutter-b">
	        <!--begin::Header-->
	            <div class="card-header align-items-center border-0 mt-4">
	            <h3 class="card-title align-items-start flex-column">
	                <span class="font-weight-bolder text-dark">
	                    <?php
	                    if ($this->session->userdata('lang') == "ar"){
	                        echo $row->ar_title;
	                    }else{
	                        echo $row->en_title;
	                    }?>
	               </span>
	            </h3>
	            <span class="text-muted">
	                <img src="<?php echo base_url()."upload/project/".$row->image;?>" width="150px" alt="">
	            </span>
	        </div>
	        <!--end::Header-->
	        <!--begin::Body-->

	        <!--end: Card Body-->
	    </div>
	    <!--end: List Widget 9-->
	    </div>
	    <?php
        }
    }?>
    </div>
<?php
} ?>
