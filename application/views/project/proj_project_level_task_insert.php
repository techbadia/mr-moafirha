

                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
                        echo form_open_multipart($FormPath);
                    ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_level_task_project_id');?></label>
												<select name="project_id" id="project_id" class="form-control">
													<?php
													$ProjectList = $this->M_proj_project->GetMultiRow();
													foreach($ProjectList as $ProjectList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$Project = $ProjectList_Row->ar_title;
														}
														else
														{
															$Project = $ProjectList_Row->en_title;
														}
													?>
													<option value="<?php echo $ProjectList_Row->id;?>"><?php echo $Project;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_level_task_level_id');?></label>
												<select name="level_id" id="level_id" class="form-control">
													<?php
													$LevelList = $this->M_proj_project_level->GetMultiRow();
													foreach($LevelList as $LevelList_Row) {
														if ($this->session->userdata('lang') == "ar")
														{
															$Level = $LevelList_Row->ar_title;
														}
														else
														{
															$Level = $LevelList_Row->en_title;
														}
													?>
													<option value="<?php echo $LevelList_Row->id;?>" title="<?php echo $LevelList_Row->project_id;?>"><?php echo $Level;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_level_task_status_id');?></label>
												<select name="status_id" class="form-control">
													<?php
													$StatusList = $this->M_proj_status->GetMultiRow();
													foreach($StatusList as $StatusList_Row) {
														$status_id = $StatusList_Row->id;
														if($status_id > 1) {
														if ($this->session->userdata('lang') == "ar")
														{
															$Status = $StatusList_Row->ar_title;
														}
														else
														{
															$Status = $StatusList_Row->en_title;
														}
													?>
													<option value="<?php echo $StatusList_Row->id;?>"><?php echo $Status;?></option>
													<?php
														}
													}
													?>
												</select>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_level_task_user_id');?></label>
												<select name="user_id" class="form-control">
													<?php
													$UsersList = $this->M_usr_users->GetMultiRow();
													foreach($UsersList as $UsersList_Row) {
													?>
													<option value="<?php echo $UsersList_Row->id;?>"><?php echo $UsersList_Row->fullname;?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_level_task_ar_details');?></label>
												<input type="text" name="ar_details" class="form-control" value="" required>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_level_task_en_details');?></label>
												<input type="text" name="en_details" class="form-control" value="" required>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_level_start_date');?></label>
												<input type="date" name="start_date" class="form-control" value="<?php echo date("Y-m-d");?>" required>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('proj_project_level_end_date');?></label>
												<input type="date" name="end_date" class="form-control" value="<?php echo date("Y-m-d");?>" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<?php
													$company_id = intval($this->session->userdata('company_id'));
													?>
													<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
													<input type="hidden" name="deleted" value="0">

													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                