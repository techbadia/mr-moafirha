<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
$Segment1 = $this->uri->segment(1);
$Segment2 = $this->uri->segment(2);
$FormPath = base_url().$Segment1."/".$Segment2."/";
echo form_open_multipart($FormPath);
?>
<div class="row">
  <div class="form-group col-md-2">
    <label  label=""><?php echo lang('company_id'); ?></label >

      <select name="company_id" class="form-control select2 " >
        <option <?php echo ($_POST['company_id'] == "all")?"selected":"" ?> value="all"><?php echo lang("All");?></option>

        <?php
        foreach($companies as $company) {
          if ($this->session->userdata('lang') == "ar")
          {
            $ToTitle = $company->ar_title;
          }
          else
          {
            $ToTitle = $company->en_title;
          }
          ?>

          <option <?php echo ($_POST['company_id'] == $company->id)?"selected":"" ?> value="<?php echo $company->id;?>"><?php echo $ToTitle;?></option>
        <?php } ?>
      </select>
    </div>

	<div class="col-md-3">
	        	   <label class="col-form-label"><?php echo lang('Month')."/".lang('Year');?></label>
		<input type="month" name="fromdate" class="form-control" value="<?php echo $date;?>" required>

	</div>

		<div class="col-md-3">
		<label class="col-form-label"><?php echo lang('hr_workgrouptime_workgroup_id');?></label>
        	<select name="workgroup_id" class="form-control" required="">
				<option value="-1" <?php
				if($workgroup_id == -1) echo "selected";
				?>><?php echo lang('all');?></option>
				<?php
				$WorkGroupName = lang('all');
				foreach($DataWorkgroups as $DataWorkgroup ){
						if ($this->session->userdata('lang') == "ar")
						{
							$WorkGroup = $DataWorkgroup->ar_title;
						}
						else
						{
							$WorkGroup = $DataWorkgroup->en_title;
						}
					?>
					<option value="<?php echo $DataWorkgroup->id;?>"<?php
					if($workgroup_id == $DataWorkgroup->id) {echo "selected"; $WorkGroupName=$WorkGroup;}
					?>><?php echo $WorkGroup;?></option>
					<?php
				}

				?>

			</select>

	</div>

	<div class="col-md-2">
		<button type="submit" class="btn btn-primary mr-2" style="margin-top:35px;"><?php echo $this->lang->line('Search');?></button>
	</div>



</div>
<?php
echo form_close();
?>
</div>



	<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
                        <th><?php echo lang('company_id');?></th
                          >
												<th><?php echo lang('hr_employee_id');?></th>

												<th><?php echo lang('employee_job_id');?></th>
												<th><?php echo lang('employee_basic_salary');?></th>
											</tr>
										</thead>
										<tbody>
											<?php

											foreach($DataRows as $DataRows_Row) {
												$employee_id = $DataRows_Row->id;
                                				$basic_salary = $DataRows_Row->basic_salary;
                                				$salary_days = $DataRows_Row->salary_days;
                                				$DailySalary = $basic_salary / $salary_days;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
                        <td>
                          <?php
                          $supplier_id = $DataRows_Row->company_id;
                          $SupplierData = $this->M_app_company->GetRow($supplier_id);
                          if ($this->session->userdata('lang') == "ar")
                          {
                            $SupplierTitle = $SupplierData->ar_title;
                          }
                          else
                          {
                            $SupplierTitle = $SupplierData->en_title;
                          }
                          echo $SupplierTitle;
                          ?>
                        </td>

												<td>
												<?php
												if ($this->session->userdata('lang') == "ar")
												{
													echo $DataRows_Row->ar_name;
												}
												else
												{
													echo $DataRows_Row->en_name;
												}
												?>
												</td>
												<td>
													<?php
													$job_id = $DataRows_Row->job_id;
													$JobData = $this->M_hr_job->GetRow($job_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $JobData->ar_title;
													}
													else
													{
														echo $JobData->en_title;
													}
													?>
												</td>
											<td>
													<?php
													$TotalAction1 = $this->M_hr_employee_subsalary->GetSumAction1($employee_id, $fromdate, $todate);
													$TotalAction2 = $this->M_hr_employee_subsalary->GetSumAction2($employee_id, $fromdate, $todate);
													$TotalAction3 = $this->M_hr_employee_subsalary->GetSumAction3($employee_id, $fromdate, $todate);
													$WorkDays = intval($this->M_hr_inouttimedata->CheckEmployee($employee_id, $fromdate, $todate));
													$WorkSalary = $DailySalary * $WorkDays;
													$FinalSalary = $WorkSalary + ($TotalAction1->thevalue) - ($TotalAction2->thevalue) - ($TotalAction3->thevalue);
													echo $FinalSalary2=number_format($FinalSalary, 2, '.', '');

													?>
												</td>


											</tr>
											<?php
											}
											?>
										</tbody>

									</table>
