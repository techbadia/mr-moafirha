<style media="screen">
#overlay{
  position: fixed;
  top: 0;
  z-index: 100;
  width: 100%;
  height:100%;
  display: none;
  background: rgba(0,0,0,0.6);
}
.cv-spinner {
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
}
.spinner {
  width: 40px;
  height: 40px;
  border: 4px #ddd solid;
  border-top: 4px #2e93e6 solid;
  border-radius: 50%;
  animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
  100% {
    transform: rotate(360deg);
  }
}
.is-hide{
  display:none;
}
</style>
<div id="overlay">
<div class="cv-spinner">
<span class="spinner"></span>
</div>
</div>
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <form class="kt-form kt-form--label-right" method="get" action="<?php echo base_url().$Segment1."/".$Segment2."/".$Segment3;?>" enctype="multipart/form-data">
        <div class="row">
          <div class="form-group col-md-2">
            <label  label=""><?php echo lang('company_id'); ?></label >

              <select name="company_id" class="form-control select2 " >
                <option <?php echo ($_GET['company_id'] == "all")?"selected":"" ?> value="all"><?php echo lang("All");?></option>

                <?php
                foreach($companies as $company) {
                  if ($this->session->userdata('lang') == "ar")
                  {
                    $ToTitle = $company->ar_title;
                  }
                  else
                  {
                    $ToTitle = $company->en_title;
                  }
                  ?>

                  <option <?php echo ($_GET['company_id'] == $company->id)?"selected":"" ?> value="<?php echo $company->id;?>"><?php echo $ToTitle;?></option>
                <?php } ?>
              </select>
            </div>


            <div class="form-group col-md-2">
                <label><?php echo $this->lang->line('Name \ phone');?></label>
                <input type="text" name="keyword" class="form-control" value="<?php echo $_GET["keyword"];?>" >
            </div>



            <div class="form-group col-md-2">
                <button type="submit" class="btn btn-success btn-custom" id="kt_sweetalert_demo_3_3" style="margin-top:25px;"><?php echo lang('Search');?></button>
            </div>
        </div>
    </form>
</div>
<style media="screen">
div.dt-buttons {
position: relative;
float: left;
}
</style>
  <table class="table table-separate table-head-custom table-checkable" id="examples">
    <thead>
        <tr>
            <th>id</th>
            <th><?php echo $this->lang->line('ar_title');?></th>
            <th><?php echo lang('company_id');?></th>
            <th><?php echo $this->lang->line('phone');?></th>
            <th><?php echo $this->lang->line('startamount');?></th>
            <th><?php echo $this->lang->line('Debit');?></th>
            <th><?php echo $this->lang->line('Creditor');?></th>
            <th><?php echo $this->lang->line('Net');?></th>

        </tr>
    </thead>
    <tbody>
    <?php
    foreach($DataRows as $MultiRows_Row) {
        $AccountID = $MultiRows_Row->id;
    ?>
    <tr>
        <td><?php echo $MultiRows_Row->id;?></td>
        <td>
            <a href="<?php echo base_url();?>account/fin_treeaccount/view_account/<?php echo $MultiRows_Row->id;?>" target="_blank"><?php echo $MultiRows_Row->ar_title;?></a>
        </td>
        <td>
          <?php
          $supplier_id = $MultiRows_Row->company_id;
          $SupplierData = $this->M_app_company->GetRow($supplier_id);
          if ($this->session->userdata('lang') == "ar")
          {
            $SupplierTitle = $SupplierData->ar_title;
          }
          else
          {
            $SupplierTitle = $SupplierData->en_title;
          }
          echo $SupplierTitle;
          ?>
        </td>
        <td><?php echo $MultiRows_Row->phone." ".$MultiRows_Row->mobile1;?></td>
        <td>
            <?php
            $StartAmount = 0;
            $Total_debit = 0;
            $Total_creditor = 0;
            $AccountInfo = $this->M_fin_treeaccount->GetRow($AccountID);
            echo $AccountInfo->startamount;
            $StartAmount = doubleval($AccountInfo->startamount);
            $AccountTransaction = $this->M_fin_journal->GetDetailsByAccount($AccountID);

            foreach($AccountTransaction as $AccountTransaction_Row) {
                $Total_debit += doubleval($AccountTransaction_Row->debit);
                $Total_creditor += doubleval($AccountTransaction_Row->creditor);
            }
            ?>
        </td>
        <td><?php echo $Total_debit; ?></td>
        <td><?php echo $Total_creditor; ?></td>
        <td>
            <?php
            $CurrentBalance = $StartAmount + $Total_debit - $Total_creditor;
            echo $CurrentBalance;
            ?>
        </td>

    </tr>
    <?php
    }
    ?>
    </tbody>
</table>



<div id="show_modal" class="modal fade" role="dialog" style="background: #000;">
    <div class="modal-dialog" style="min-width: 600px;">
        <div class="modal-content">
        <div class="modal-header">
            <h3 style="font-size: 24px; color: #17919e; text-shadow: 1px 1px #ccc;">
            <i class="fa fa-folder"></i><?php echo lang('sale_customer');?>
            </h3>
        </div>
        <div class="modal-body">
        <div class="row">
            <div class="col-md-3"><?php echo lang('sale_fullname');?></div>
            <div class="col-md-3" id="customer_name"></div>
            <div class="col-md-3"><?php echo lang('phone');?></div>
            <div class="col-md-3" id="phone"></div>
        </div>
        <div class="row">
            <div class="col-md-3"><?php echo lang('mobile1');?></div>
            <div class="col-md-3" id="mobile1"></div>
            <div class="col-md-3"><?php echo lang('mobile2');?></div>
            <div class="col-md-3" id="mobile2"></div>
        </div>
        <div class="row">
            <div class="col-md-3"><?php echo lang('address');?></div>
            <div class="col-md-9" id="address"></div>
        </div>
        <div class="row">
            <div class="col-md-3"><?php echo lang('email');?></div>
            <div class="col-md-3" id="email"></div>
            <div class="col-md-3"><?php echo lang('commercial_register');?></div>
            <div class="col-md-3" id="commercial_register"></div>
        </div>
        <div class="row">
            <div class="col-md-3"><?php echo lang('tax_card');?></div>
            <div class="col-md-3" id="tax_card"></div>
            <div class="col-md-3"><?php echo lang('person');?></div>
            <div class="col-md-3" id="person"></div>
        </div>
        <div class="row">
            <div class="col-md-3"><?php echo lang('person_phone');?></div>
            <div class="col-md-3" id="person_phone"></div>
            <div class="col-md-3"><?php echo lang('credit_limit');?></div>
            <div class="col-md-3" id="credit_limit"></div>
        </div>

        </div>
        <div class="modal-footer noPrint">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo lang('Close');?></button>
        </div>
        </div>
    </div>
</div>
