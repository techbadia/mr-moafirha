
<?php $base_url = base_url(); ?>
                <!--begin: Datatable -->
                <div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
                    <form class="kt-form kt-form--label-right" method="get" action="<?php echo base_url().$Segment1."/".$Segment2."/".$Segment3;?>" enctype="multipart/form-data">
                        <div class="row">
                          <div class="form-group col-md-2">
                            <label  label=""><?php echo lang('company_id'); ?></label >

                              <select name="company_id" class="form-control select2 " >
                                <option <?php echo ($_GET['company_id'] == "all")?"selected":"" ?> value="all"><?php echo lang("All");?></option>

                                <?php
                                foreach($companies as $company) {
                                  if ($this->session->userdata('lang') == "ar")
                                  {
                                    $ToTitle = $company->ar_title;
                                  }
                                  else
                                  {
                                    $ToTitle = $company->en_title;
                                  }
                                  ?>

                                  <option <?php echo ($_GET['company_id'] == $company->id)?"selected":"" ?> value="<?php echo $company->id;?>"><?php echo $ToTitle;?></option>
                                <?php } ?>
                              </select>
                            </div>


                            <div class="form-group col-md-2">
                                <label><?php echo $this->lang->line('fin_expected_expenses_account_id');?></label>
                                <input type="text" name="keyword" class="form-control" value="<?php echo $_GET["keyword"];?>" >
                            </div>




                            <div class="form-group col-md-2">
                                <button type="submit" class="btn btn-success btn-custom" id="kt_sweetalert_demo_3_3" style="margin-top:25px;"><?php echo lang('Search');?></button>
                            </div>
                        </div>
                    </form>
                </div>
                <style media="screen">
                div.dt-buttons {
    position: relative;
    float: left;
  }
                </style>

                									<!--begin: Datatable -->
                									<table class="table table-separate table-head-custom table-checkable" id="examples">
                										<thead>
                											<tr>
                												<th>ID</th>
                												<th><?php echo lang('fin_expected_revenue_account_id');?></th>
                												<th><?php echo lang('fin_expected_revenue_amount');?></th>
                												<th><?php echo lang('fin_expected_revenue_actual');?></th>
                												<th><?php echo lang('store_active');?></th>
                												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
                												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
                												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
                											</tr>
                										</thead>
                										<tbody>
                											<?php
                											foreach($DataRows as $DataRows_Row) {
                												$store_id = $DataRows_Row->id;
                											?>
                											<tr>
                												<td><?php echo $DataRows_Row->id;?></td>
                												<td>
                												<?php
                												$account_id = $DataRows_Row->account_id;
                												$AccountData = $this->M_fin_treeaccount->GetRow($account_id);
                												if($this->session->userdata('lang') == "ar")
                												{
                													echo $AccountData->title;
                												}
                												else
                												{
                													echo $AccountData->title_en;
                												}
                												?>
                												</td>
                												<td><?php echo $DataRows_Row->amount;?></td>
                												<td>
                													<?php //echo $DataRows_Row->actual;?>
                												</td>
                												<td>
                												<?php
                												if($DataRows_Row->deleted == 0)
                												{
                													echo lang('Yes');
                												}
                												else
                												{
                													echo lang('No');
                												}
                												?>
                												</td>
                												<td style="text-align:center">
                													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
                														<i class="flaticon-edit-1 text-primary icon-lg"></i>
                													</a>
                												</td>
                												<td style="text-align:center">
                													<?php
                														if($DataRows_Row->deleted == 0) {
                													?>
                													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
                														<i class="flaticon-delete text-danger icon-lg"></i>
                													</a>
                													<?php } ?>
                												</td>
                												<td style="text-align:center">
                													<?php
                														if($DataRows_Row->deleted == 1) {
                													?>
                														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
                															<i class="flaticon-refresh text-success icon-lg"></i>
                														</a>
                													<?php } ?>
                												</td>
                											</tr>
                											<?php
                											}
                											?>
                										</tbody>

                									</table>





		<div id="show_modal" class="modal fade" role="dialog" style="background: #000;">
			<div class="modal-dialog" style="min-width: 600px;">
				<div class="modal-content">
					<div class="modal-header">
						<h3 style="font-size: 24px; color: #17919e; text-shadow: 1px 1px #ccc;">
						<i class="fa fa-folder"></i><?php echo lang('buy_bill');?>
						</h3>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-3"><?php echo lang('expenses_bill_id');?></div>
							<div class="col-md-3" id="id"></div>
							<div class="col-md-3"><?php echo lang('expenses_thedate');?></div>
							<div class="col-md-3" id="thedate"></div>
						</div>
						<div class="row">
							<div class="col-md-3"><?php echo lang('buy_bill_supplier_id');?></div>
							<div class="col-md-9" id="supplier"></div>
						</div>
						<div class="row">
							<div class="col-md-3"><?php echo lang('buy_bill_totalvalue');?></div>
							<div class="col-md-3" id="totalvalue"></div>
							<div class="col-md-3"><?php echo lang('buy_bill_paid');?></div>
							<div class="col-md-3" id="paid"></div>
						</div>
						<div class="row">
							<div class="col-md-3"><?php echo lang('buy_bill_remaining');?></div>
							<div class="col-md-3" id="remaining"></div>
							<div class="col-md-3"><?php echo lang('buy_bill_discount');?></div>
							<div class="col-md-3" id="discount"></div>
						</div>
						<div class="row">
							<div class="col-md-3"><?php echo lang('buy_bill_tree_id');?></div>
							<div class="col-md-3" id="account_title"></div>
							<div class="col-md-3"><?php echo lang('buy_offer_tax');?></div>
							<div class="col-md-3" id="tax"></div>
						</div>
						<div class="row">
							<div class="col-md-12" id="notes"></div>
						</div>
						<table id="ModalTable" class="table table-bordered table-striped">
							<thead class="btn-primary">
								<tr>
									<th align='right'><?php echo lang('buy_bill_items_store_type_id');?></th>
									<th style="text-align:center"><?php echo lang('buy_bill_items_quantity');?></th>
									<th style="text-align:center"><?php echo lang('buy_bill_items_unitprice');?></th>
									<th style="text-align:center"><?php echo lang('fin_journal_Total');?></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
					<div class="modal-footer noPrint">
						<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo lang('Close');?></button>
						<button type="button" class="btn btn-info" onclick="printSelection(document.getElementById('show_modal'));return false;"><i class="fa fa-print"></i> <?php echo $this->lang->line('Print');?></button>
					</div>
				</div>
			</div>
		</div>





<div id="modalDiv">
	<div class="modal fade" id="buy_bill_pay" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: #c0edf1; text-align:<?php echo $this->session->userdata('Alignment');?> !important">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title" style="direction:<?php echo $this->session->userdata('Direction');?> !important">
						<?php echo $this->lang->line('buy_bill_Pay');?>
					</h4>
				</div>
				<?php
				$FormName = "buy_bill_pay";
				$Target = base_url().$Segment1."/".$Segment2."/close_invoice";
					echo form_open_multipart($Target, "id=$FormName name=$FormName");
				?>
				<div id="modalDiv">
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4">
								<label><?php echo $this->lang->line('buy_bill_totalvalue');?></label>
								<input type="number" id="totalvalue" name="totalvalue" value="" readonly class="form-control" required>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<label><?php echo $this->lang->line('buy_bill_remaining');?></label>
								<input type="number" min="0" id="remaining" name="remaining" readonly value="" class="form-control" required>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<label><?php echo $this->lang->line('buy_bill_paid');?></label>
								<input type="number" min="0" step="0.001" id="paid" name="paid" readonly value="" class="form-control" required>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 15px; margin-bottom: 15px;">

							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6">
								<label><?php echo $this->lang->line('To_be_paid');?></label>
								<input type="number" min="0" step="0.001" id="paid_value" name="paid_value" class="form-control" value="" required autofocus>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6">
								<label><?php echo $this->lang->line('buy_bill_tree_id');?></label>
								<select id="tree_id" name="tree_id" class="form-control" required>
									<?php
									$acc_treasury = intval($this->session->userdata('acc_treasury'));
									$toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_treasury);
									foreach($toaccount as $toaccount_Row) {
										if ($this->session->userdata('lang') == "ar")
										{
											$ToTitle = $toaccount_Row->title;
										}
										else
										{
											$ToTitle = $toaccount_Row->title_en;
										}
									?>
									<option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
									<?php } ?>
									<?php
									$acc_bank = intval($this->session->userdata('acc_bank'));
									$toaccount = $this->M_fin_treeaccount->GetMain_Sub($acc_bank);
									foreach($toaccount as $toaccount_Row) {
										if ($this->session->userdata('lang') == "ar")
										{
											$ToTitle = $toaccount_Row->title;
										}
										else
										{
											$ToTitle = $toaccount_Row->title_en;
										}
									?>
									<option value="<?php echo $toaccount_Row->id;?>"><?php echo $ToTitle;?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<label class="col-form-label"><?php echo lang('buy_bill_image');?></label>
								<input type="file" name="image" class="form-control" dir="ltr" required>
							</div>
						</div>
						<div class="form-actions right noPrint" id="SaveButtons" style="text-align: right; font-weight: bold !important; font-size: 18px; margin-top: 10px;">
							<input type="hidden" name="id" id="id" value="">
							<button type="submit" class="btn btn-success" accesskey="s"><?php echo $this->lang->line('Pay');?></button>
						</div>
					</div>
				</div>
				<div class="modal-footer" id="FooterButtons">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $this->lang->line('Close');?></button>
					<button type="button" class="btn btn-info" onclick="printSelection(document.getElementById('buy_bill_pay'));return false;"><i class="fa fa-print"></i> <?php echo $this->lang->line('Print');?></button>
				</div>
				<?php
					echo form_close();
				?>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>
