<style media="screen">
#overlay{
  position: fixed;
  top: 0;
  z-index: 100;
  width: 100%;
  height:100%;
  display: none;
  background: rgba(0,0,0,0.6);
}
.cv-spinner {
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
}
.spinner {
  width: 40px;
  height: 40px;
  border: 4px #ddd solid;
  border-top: 4px #2e93e6 solid;
  border-radius: 50%;
  animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
  100% {
    transform: rotate(360deg);
  }
}
.is-hide{
  display:none;
}
</style>
<div id="overlay">
<div class="cv-spinner">
<span class="spinner"></span>
</div>
</div>
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
    <form class="kt-form kt-form--label-right" method="get" action="<?php echo base_url().$Segment1."/".$Segment2."/".$Segment3;?>" enctype="multipart/form-data">
        <div class="row">
          <div class="form-group col-md-2">
            <label  label=""><?php echo lang('company_id'); ?></label >

              <select name="company_id" class="form-control select2 " >
                <option <?php echo ($_GET['company_id'] == "all")?"selected":"" ?> value="all"><?php echo lang("All");?></option>

                <?php
                foreach($companies as $company) {
                  if ($this->session->userdata('lang') == "ar")
                  {
                    $ToTitle = $company->ar_title;
                  }
                  else
                  {
                    $ToTitle = $company->en_title;
                  }
                  ?>

                  <option <?php echo ($_GET['company_id'] == $company->id)?"selected":"" ?> value="<?php echo $company->id;?>"><?php echo $ToTitle;?></option>
                <?php } ?>
              </select>
            </div>


            <div class="form-group col-md-6">
                <label><?php echo $this->lang->line('Name \ phone \ mobile \ email \ P name \ P mobile');?></label>
                <input type="text" name="keyword" class="form-control" value="<?php echo $_GET["keyword"];?>" >
            </div>



            <div class="form-group col-md-2">
                <button type="submit" class="btn btn-success btn-custom" id="kt_sweetalert_demo_3_3" style="margin-top:25px;"><?php echo lang('Search');?></button>
            </div>
        </div>
    </form>
</div>
<style media="screen">
div.dt-buttons {
position: relative;
float: left;
}
</style>


									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="examples">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('buy_manufacturer_title');?></th>
												<th><?php echo lang('buy_manufacturer_title_en');?></th>
                        <th><?php echo lang('company_id');?></th>
                        <th><?php echo lang('buy_manufacturer_credit_limit');?></th>


												<th><?php echo lang('buy_manufacturer_phone');?></th>
												<th><?php echo lang('buy_manufacturer_mobile');?></th>
												<th><?php echo lang('buy_manufacturer_email');?></th>
												<th><?php echo lang('buy_manufacturer_person');?></th>
												<th><?php echo lang('buy_manufacturer_person_phone');?></th>
                        <th><?php echo lang('Debit');?></th>
                        <th><?php echo lang('Creditor');?></th>
                        <th><?php echo lang('Net');?></th>

											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$buy_manufacturer_id = $DataRows_Row->id;
											?>
											<tr>
												<td>
												<a href="<?php echo base_url();?>account/fin_treeaccount/view_account/<?php echo $DataRows_Row->id;?>" target="_blank"><?php echo $DataRows_Row->id;?></a>
												</td>
												<td><?php echo $DataRows_Row->title;?></td>
												<td><?php echo $DataRows_Row->title_en;?></td>
                        <td>
                          <?php
                          $supplier_id = $DataRows_Row->company_id;
                          $SupplierData = $this->M_app_company->GetRow($supplier_id);
                          if ($this->session->userdata('lang') == "ar")
                          {
                            $SupplierTitle = $SupplierData->ar_title;
                          }
                          else
                          {
                            $SupplierTitle = $SupplierData->en_title;
                          }
                          echo $SupplierTitle;
                          ?>
                        </td>
												<td><?php echo $DataRows_Row->phone;?></td>
												<td><?php echo $DataRows_Row->credit_limit;?></td>
												<td><?php echo $DataRows_Row->mobile;?></td>
												<td><?php echo $DataRows_Row->email;?></td>
												<td><?php echo $DataRows_Row->person;?></td>
												<td><?php echo $DataRows_Row->person_phone;?></td>
												<td><?php echo $DataRows_Row->debt;?></td>
												<td><?php echo $DataRows_Row->credit;?></td>
												<td><?php echo $DataRows_Row->net;?></td>

											</tr>
											<?php
											}
											?>
										</tbody>
									</table>
