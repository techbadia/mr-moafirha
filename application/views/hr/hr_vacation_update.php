<div class="card card-custom gutter-b example example-compact">
	<?php
	$FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
	echo form_open_multipart($FormPath);
	?>
	<div class="form-body">
		<div class="form-group row">
		<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('hr_vacation_employee_id');?></label>
				<select name="employee_id" class="form-control" required>
				<?php
					$EmployeeData = $this->M_hr_employee->GetMultiRow();
					foreach($EmployeeData as $EmployeeData_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Employee = $EmployeeData_Row->ar_name;
						}
						else
						{
							$Employee = $EmployeeData_Row->en_name;
						}
					?>
					<option <?php if($employee_id == $EmployeeData_Row->id) {?>selected<?php }?> value="<?php echo $EmployeeData_Row->id;?>"><?php echo $Employee;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('hr_vacation_type_id');?></label>
				<select name="type_id" class="form-control" required>
					<?php
					$VacationType = $this->M_hr_vacation_types->GetMultiRow();
					foreach($VacationType as $VacationType_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$VacationTypeTitle = $VacationType_Row->ar_title;
						}
						else
						{
							$VacationTypeTitle = $VacationType_Row->en_title;
						}
					?>
					<option <?php if($type_id == $VacationType_Row->id) {?>selected<?php }?> value="<?php echo $VacationType_Row->id;?>"><?php echo $VacationTypeTitle;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('hr_vacation_from_date');?></label>
				<input type="time" name="from_date" class="form-control" value="<?php echo $from_date;?>" required>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('hr_vacation_to_date');?></label>
				<input type="time" name="to_date" class="form-control" value="<?php echo $to_date;?>" required>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('hr_vacation_accept');?></label>
				<select name="accept" class="form-control" required>
					<option <?php if($accept == 1) {?>selected<?php }?> value="1"><?php echo lang('Yes');?></option>
					<option <?php if($accept == 0) {?>selected<?php }?> selected value="0"><?php echo lang('No');?></option>
				</select>
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php echo $id;?>">
					<input type="hidden" name="deleted" value="<?php echo $deleted;?>">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>
</div>