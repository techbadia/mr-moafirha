<div class="card card-custom gutter-b example example-compact">
                    <?php
                    $FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
                    echo form_open_multipart($FormPath);
                    ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('hr_weekend_title');?></label>
												<select name="title" class="form-control">
													<option value="1" <?php if($ar_title == "السبت") {?>selected<?php }?>><?php echo lang('Saturday');?></a>
													<option value="2" <?php if($ar_title == "الأحد") {?>selected<?php }?>><?php echo lang('Sunday');?></a>
													<option value="3" <?php if($ar_title == "الإثنين") {?>selected<?php }?>><?php echo lang('Monday');?></a>
													<option value="4" <?php if($ar_title == "الثلاثاء") {?>selected<?php }?>><?php echo lang('Tuesday');?></a>
													<option value="5" <?php if($ar_title == "الأربعاء") {?>selected<?php }?>><?php echo lang('Wednesday');?></a>
													<option value="6" <?php if($ar_title == "الخميس") {?>selected<?php }?>><?php echo lang('Thursday');?></a>
													<option value="7" <?php if($ar_title == "الجمعة") {?>selected<?php }?>><?php echo lang('Friday');?></a>
												</select>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('hr_weekend_dayoffer');?></label>
												<input type="number" min="1" name="dayoffer" class="form-control" value="<?php echo $dayoffer;?>" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php echo $id;?>">
													<input type="hidden" name="deleted" value="<?php echo $deleted;?>">
													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>