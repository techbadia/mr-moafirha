
<div class="card card-custom gutter-b example example-compact">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
                        echo form_open_multipart($FormPath);
                    ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('hr_weekend_title');?></label>
												<select name="title" class="form-control">
													<option value="1"><?php echo lang('Saturday');?></a>
													<option value="2"><?php echo lang('Sunday');?></a>
													<option value="3"><?php echo lang('Monday');?></a>
													<option value="4"><?php echo lang('Tuesday');?></a>
													<option value="5"><?php echo lang('Wednesday');?></a>
													<option value="6"><?php echo lang('Thursday');?></a>
													<option value="7"><?php echo lang('Friday');?></a>
												</select>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('hr_weekend_dayoffer');?></label>
												<input type="number" min="1" value="1" name="dayoffer" class="form-control" value="" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<?php
													$company_id = intval($this->session->userdata('company_id'));
													?>
													<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
													<input type="hidden" name="deleted" value="0">

													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>