<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
<?php
$Segment1 = $this->uri->segment(1);
$Segment2 = $this->uri->segment(2);
$FormPath = base_url().$Segment1."/".$Segment2."/";
echo form_open_multipart($FormPath);
?>
<div class="row">
	<div class="col-md-3">
	        	   <label class="col-form-label"><?php echo lang('Month')."/".lang('Year');?></label>
		<input type="month" name="fromdate" class="form-control" value="<?php echo $date;?>" required>

	</div>

		<div class="col-md-3">
		<label class="col-form-label"><?php echo lang('hr_workgrouptime_workgroup_id');?></label>
        	<select name="workgroup_id" class="form-control" required="">
				<option value="-1" <?php
				if($workgroup_id == -1) echo "selected";
				?>><?php echo lang('all');?></option>
				<?php
				$WorkGroupName = lang('all');
				foreach($DataWorkgroups as $DataWorkgroup ){
						if ($this->session->userdata('lang') == "ar")
						{
							$WorkGroup = $DataWorkgroup->ar_title;
						}
						else
						{
							$WorkGroup = $DataWorkgroup->en_title;
						}
					?>
					<option value="<?php echo $DataWorkgroup->id;?>"<?php
					if($workgroup_id == $DataWorkgroup->id) {echo "selected"; $WorkGroupName=$WorkGroup;}
					?>><?php echo $WorkGroup;?></option>
					<?php
				}

				?>

			</select>

	</div>

	<div class="col-md-2">
		<button type="submit" class="btn btn-primary mr-2" style="margin-top:35px;"><?php echo $this->lang->line('Search');?></button>
	</div>



</div>
<?php
echo form_close();
?>
</div>
<?php
if(isset($date) && !empty($date)){
  if(count($DataRows)>1){
      	$a_date = $date."-23";
		$f_date = $date."-1";
		$fromdate =date("Y-m-d", strtotime($f_date));
		$todate =date("Y-m-t", strtotime($a_date));
        $month=date("m", strtotime($a_date));
	    $year=date("Y", strtotime($a_date));
?>
	<div class="col-md-12">
	    <div class="row">
	        	<div class="col-md-4">
	        	   <label class="col-form-label"><?php echo lang('Month')."/".lang('Year');?></label>
	        	   <span class="form-control"><?=$date?></span>

	        	</div>

	        		<div class="col-md-4">
	        		    <label class="col-form-label"><?php echo lang('hr_workgrouptime_workgroup_id');?></label>
	        	   <span class="form-control"><?=$WorkGroupName?></span>

	        	</div>

	        		<div class="col-md-4">
    	        	<?php
                        echo form_open_multipart($FormPath."insertMultiDisbursement");
                    ?>
	        		    <input type="hidden" name="month" id="month" value="<?=$month?>">
    	        	    <input type="hidden" name="year" id="year" value="<?=$year?>">
    	        	    <input type="hidden" name="workgroup" id="workgroup" value="<?=$workgroup_id?>">
    	        	    <button type="submit" class="btn btn-primary mr-2" style="margin-top:35px;"><?php echo $this->lang->line('PayToAll');?></button>
    	        	<?php
                        echo form_close();
                    ?>
</div>
	    </div>


	</div>

	<?php
  }
	?>
	<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('hr_employee_id');?></th>

												<th><?php echo lang('employee_job_id');?></th>
												<th><?php echo lang('employee_basic_salary');?></th>
												<th><?php echo lang('salary_disbursement');?></th>
											</tr>
										</thead>
										<tbody>
											<?php

											foreach($DataRows as $DataRows_Row) {
												$employee_id = $DataRows_Row->id;
                                				$basic_salary = $DataRows_Row->basic_salary;
                                				$salary_days = $DataRows_Row->salary_days;
                                				$DailySalary = $basic_salary / $salary_days;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>


												<td>
												<?php
												if ($this->session->userdata('lang') == "ar")
												{
													echo $DataRows_Row->ar_name;
												}
												else
												{
													echo $DataRows_Row->en_name;
												}
												?>
												</td>
												<td>
													<?php
													$job_id = $DataRows_Row->job_id;
													$JobData = $this->M_hr_job->GetRow($job_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $JobData->ar_title;
													}
													else
													{
														echo $JobData->en_title;
													}
													?>
												</td>
											<td>
													<?php
													$TotalAction1 = $this->M_hr_employee_subsalary->GetSumAction1($employee_id, $fromdate, $todate);
													$TotalAction2 = $this->M_hr_employee_subsalary->GetSumAction2($employee_id, $fromdate, $todate);
													$TotalAction3 = $this->M_hr_employee_subsalary->GetSumAction3($employee_id, $fromdate, $todate);
													$WorkDays = intval($this->M_hr_inouttimedata->CheckEmployee($employee_id, $fromdate, $todate));
													$WorkSalary = $DailySalary * $WorkDays;
													$FinalSalary = $WorkSalary + ($TotalAction1->thevalue) - ($TotalAction2->thevalue) - ($TotalAction3->thevalue);
													echo $FinalSalary2=number_format($FinalSalary, 2, '.', '');

													?>
												</td>

													<td>
													<?php
													$month=date("m", strtotime($a_date));
													$year=date("Y", strtotime($a_date));
													$cnt = $this->M_hr_salary_disbursement->GetCountRow($employee_id,$month,$year);
													if ($cnt->cnt >0){
													    echo lang('Paid');
													}else{
													   echo " <button type='button' class='btn btn-primary payy' id='btn_".$employee_id."' data-user='".$employee_id."'>".lang('payy')."</button>";
													}
													?>
												</td>

											</tr>
											<?php
											}
											?>
										</tbody>

									</table>

									<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
									<script>

                                         $(document).on('click','.payy',function(){

                                             $(this).attr('disabled','disabled');
                                             var month = $('#month').val();
                                             var year = $('#year').val();
                                             var user = $(this).attr('data-user');
                                             var id = $(this).attr('id');
                                             var btn=$("#"+id);
                                              $.ajax({
                                                    type: "post",
                                                    url: '<?=$FormPath?>insertDisbursement',
                                                    data: {
                                                        'user': user,
                                                        'month': month,
                                                        'year': year,

                                                    },
                                                    dataType: 'json',
                                                    success: function (dt) {
                                                        console.log(dt);
                                                        if(dt.status ==1)
                                                       btn.parent().html('<?=lang('Paid')?>');
                                                    }

                                                });


                                             });
									</script>
<?php
}
?>
