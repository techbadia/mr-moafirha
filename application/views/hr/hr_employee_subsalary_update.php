<div class="card card-custom gutter-b example example-compact">
	<?php
	$FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
	echo form_open_multipart($FormPath);
	?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('hr_vacation_employee_id');?></label>
				<select name="employee_id" class="form-control" required>
				<?php
					$EmployeeData = $this->M_hr_employee->GetMultiRow();
					foreach($EmployeeData as $EmployeeData_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Employee = $EmployeeData_Row->ar_name;
						}
						else
						{
							$Employee = $EmployeeData_Row->en_name;
						}
					?>
					<option <?php if($employee_id == $EmployeeData_Row->id) {?>selected<?php }?> value="<?php echo $EmployeeData_Row->id;?>"><?php echo $Employee;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('hr_employee_subsalary_actionid');?></label>
				<select name="actionid" class="form-control" required>
					<option <?php if($actionid == 1) {?>selected<?php }?> value="1"><?php echo lang('hr_employee_subsalary_action1');?></option>
					<option <?php if($actionid == 2) {?>selected<?php }?> value="2"><?php echo lang('hr_employee_subsalary_action2');?></option>
					<option <?php if($actionid == 3) {?>selected<?php }?> value="3"><?php echo lang('hr_employee_subsalary_action3');?></option>
				</select>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('hr_employee_subsalary_thedate');?></label>
				<input type="date" name="thedate" class="form-control" value="<?php echo $thedate;?>" required>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('hr_employee_subsalary_thevalue');?></label>
				<input type="number" min="1" step="0.001" name="thevalue" class="form-control" value="<?php echo $thevalue;?>" required>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<label class="col-form-label"><?php echo lang('hr_employee_subsalary_notes');?></label>
				<input type="text" name="notes" class="form-control" value="<?php echo $notes;?>" required>
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php echo $id;?>">
					<input type="hidden" name="deleted" value="<?php echo $deleted;?>">
					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>
</div>