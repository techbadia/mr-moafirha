
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('hr_employee_id');?></th>
												<th><?php echo lang('hr_in_date');?></th>
												<th><?php echo lang('hr_in_time');?></th>
												<th><?php echo lang('hr_out_date');?></th>
												<th><?php echo lang('hr_out_time');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$store_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
												<?php
												$user_id = $DataRows_Row->employee_id;
												$UserData = $this->M_hr_employee->GetRow($user_id);
												if ($this->session->userdata('lang') == "ar")
                        							{
                        								$Employee_name = $UserData->ar_name;
                        							}
                        							else
                        							{
                        								$Employee_name = $UserData->en_name;
                        							}
												echo $Employee_name;
												?>
												</td>
												<td><?php echo $DataRows_Row->in_date;?></td>
												<td><?php echo $DataRows_Row->in_time;?></td>
												<td><?php echo $DataRows_Row->out_date;?></td>
												<td><?php echo $DataRows_Row->out_time;?></td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

								