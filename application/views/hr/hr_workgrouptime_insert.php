<div class="card card-custom gutter-b example example-compact">
	<?php
		$FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
		echo form_open_multipart($FormPath);
	?>
		<div class="form-body">
			<div class="form-group row">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<label class="col-form-label"><?php echo lang('hr_workgrouptime_workgroup_id');?></label>
					<select name="workgroup_id" class="form-control" required>
						<?php
						$WorkGroup = $this->M_hr_workgroup->GetMultiRow();
						foreach($WorkGroup as $WorkGroup_Row) {
							if ($this->session->userdata('lang') == "ar")
							{
								$WorkGroupTitle = $WorkGroup_Row->ar_title;
							}
							else
							{
								$WorkGroupTitle = $WorkGroup_Row->en_title;
							}
						?>
						<option value="<?php echo $WorkGroup_Row->id;?>"><?php echo $WorkGroupTitle;?></option>
						<?php
						}
						?>
					</select>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12">
					<label class="col-form-label"><?php echo lang('hr_workgrouptime_worktime_id');?></label>
					<select name="worktime_id" class="form-control" required>
						<?php
						$WorkTime = $this->M_hr_worktime->GetMultiRow();
						foreach($WorkTime as $WorkTime_Row) {
							if ($this->session->userdata('lang') == "ar")
							{
								$WorkTimeTitle = $WorkTime_Row->ar_title;
							}
							else
							{
								$WorkTimeTitle = $WorkTime_Row->en_title;
							}
						?>
						<option value="<?php echo $WorkTime_Row->id;?>"><?php echo $WorkTimeTitle;?></option>
						<?php
						}
						?>
					</select>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12">
					<label class="col-form-label"><?php echo lang('hr_workgrouptime_fromhoure');?></label>
					<input type="time" name="fromhoure" class="form-control" value="" required>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12">
					<label class="col-form-label"><?php echo lang('hr_workgrouptime_tohoure');?></label>
					<input type="time" name="tohoure" class="form-control" value="" required>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12">
					<label class="col-form-label"><?php echo lang('hr_workgrouptime_timevalue');?></label>
					<input type="number" step="0.01" name="timevalue" class="form-control" required>
				</div>
			</div>
		</div>
		<div class="kt-portlet__foot">
			<div class="kt-form__actions">
				<div class="row">
					<div class="col-lg-12 ml-lg-auto">
						<input type="hidden" name="id" value="<?php //echo $id;?>">
						<?php
						$company_id = intval($this->session->userdata('company_id'));
						?>
						<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
						<input type="hidden" name="deleted" value="0">

						<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
					</div>
				</div>
			</div>
		</div>
	<?php
		echo form_close();
	?>
</div>