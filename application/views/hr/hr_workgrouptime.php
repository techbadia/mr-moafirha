
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('hr_workgrouptime_workgroup_id');?></th>
												<th><?php echo lang('hr_workgrouptime_worktime_id');?></th>
												<th><?php echo lang('hr_workgrouptime_fromhoure');?></th>
												<th><?php echo lang('hr_workgrouptime_tohoure');?></th>
												<th><?php echo lang('hr_workgrouptime_timevalue');?></th>
												<th><?php echo lang('active');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$hr_workgrouptime_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
												<?php
												$workgroup_id = $DataRows_Row->workgroup_id;
												$WorkGroup = $this->M_hr_workgroup->GetRow($workgroup_id);
												if ($this->session->userdata('lang') == "ar")
												{
													echo $WorkGroup->ar_title;
												}
												else
												{
													echo $WorkGroup->en_title;
												}
												?>
												</td>
												<td>
												<?php
												$worktime_id = $DataRows_Row->worktime_id;
												$WorkTime = $this->M_hr_worktime->GetRow($worktime_id);
												if ($this->session->userdata('lang') == "ar")
												{
													echo $WorkTime->ar_title;
												}
												else
												{
													echo $WorkTime->en_title;
												}
												?>
												</td>
												<td><?php echo $DataRows_Row->fromhoure;?></td>
												<td><?php echo $DataRows_Row->tohoure;?></td>
												<td><?php echo $DataRows_Row->timevalue;?></td>
												<td>
												<?php
												if($DataRows_Row->deleted == 0)
												{
													echo lang('Yes');
												}
												else
												{
													echo lang('No');
												}
												?>
												</td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

								