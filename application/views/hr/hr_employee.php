<style media="screen">
#overlay{
  position: fixed;
  top: 0;
  z-index: 100;
  width: 100%;
  height:100%;
  display: none;
  background: rgba(0,0,0,0.6);
}
.cv-spinner {
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
}
.spinner {
  width: 40px;
  height: 40px;
  border: 4px #ddd solid;
  border-top: 4px #2e93e6 solid;
  border-radius: 50%;
  animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
  100% {
    transform: rotate(360deg);
  }
}
.is-hide{
  display:none;
}
</style>
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('employee_country_id');?></th>
												<th><?php echo lang('employee_education_id');?></th>
												<th><?php echo lang('employee_job_id');?></th>
												<th><?php echo lang('employee_job_type_id');?></th>
												<th><?php echo lang('employee_workgroup_id');?></th>
												<th><?php echo lang('hr_employee_id');?></th>
												<th><?php echo lang('employee_phone');?></th>
												<th><?php echo lang('employee_loan');?></th>
												<th><?php echo lang('employee_custody');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$employee_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
													<?php
													$country_id = $DataRows_Row->country_id;
													$CountryData = $this->M_hr_country->GetRow($country_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $CountryData->ar_title;
													}
													else
													{
														echo $CountryData->en_title;
													}
													?>
												</td>
												<td>
													<?php
													$education_id = $DataRows_Row->education_id;
													$EducationData = $this->M_hr_education->GetRow($education_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $EducationData->ar_title;
													}
													else
													{
														echo $EducationData->en_title;
													}
													?>
												</td>
												<td>
													<?php
													$job_id = $DataRows_Row->job_id;
													$JobData = $this->M_hr_job->GetRow($job_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $JobData->ar_title;
													}
													else
													{
														echo $JobData->en_title;
													}
													?>
												</td>
												<td>
													<?php
													$job_type_id = $DataRows_Row->job_type_id;
													$JobTypeData = $this->M_hr_job_type->GetRow($job_type_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $JobTypeData->ar_title;
													}
													else
													{
														echo $JobTypeData->en_title;
													}
													?>
												</td>
												<td>
													<?php
													$workgroup_id = $DataRows_Row->workgroup_id;
													$WorkGroupData = $this->M_hr_workgroup->GetRow($workgroup_id);
													if ($this->session->userdata('lang') == "ar")
													{
														echo $WorkGroupData->ar_title;
													}
													else
													{
														echo $WorkGroupData->en_title;
													}
													?>
												</td>
												<td>
												<?php
												if ($this->session->userdata('lang') == "ar")
												{
													echo $DataRows_Row->ar_name;
												}
												else
												{
													echo $DataRows_Row->en_name;
												}
												?>
												</td>
												<td><?php echo $DataRows_Row->phone;?></td>
												<td>
												<?php
												$CheckAccount = $this->M_fin_treeaccount->CheckLoanFind($employee_id);
												if($CheckAccount > 0)
												{
													$Debit = 0;
													$Creditor = 0;

													$LoanAccount = $this->M_fin_treeaccount->GetLoanAccount($employee_id);
													$LoanAccountID = $LoanAccount->id;

													$MultiRows = $this->M_fin_journal->GetDetailsByAccount($LoanAccountID);
													foreach($MultiRows as $MultiRows_Row) {
														$Debit += doubleval($MultiRows_Row->debit);
														$Creditor += doubleval($MultiRows_Row->creditor);
													}
													$LoanValue = $Debit - $Creditor;
													$Link = base_url()."account/fin_treeaccount/view_account/".$LoanAccountID;
													echo "<a target='_blank' href='".$Link."'>".$LoanValue."</a>";
												}
												?>
												</td>
												<td>
												<?php
												$CheckAccount = $this->M_fin_treeaccount->CheckCustodyFind($employee_id);
												if($CheckAccount > 0)
												{
													$Debit = 0;
													$Creditor = 0;

													$CustodyAccount = $this->M_fin_treeaccount->GetCustodyAccount($employee_id);
													$CustodyAccountID = $CustodyAccount->id;

													$MultiRows = $this->M_fin_journal->GetDetailsByAccount($CustodyAccountID);
													foreach($MultiRows as $MultiRows_Row) {
														$Debit += doubleval($MultiRows_Row->debit);
														$Creditor += doubleval($MultiRows_Row->creditor);
													}
													$CustodyValue = $Debit - $Creditor;
													$Link = base_url()."account/fin_treeaccount/view_account/".$CustodyAccountID;
													echo "<a target='_blank' href='".$Link."'>".$CustodyValue."</a>";
												}
												?>
												</td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i>
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i>
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i>
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>

									</table>
