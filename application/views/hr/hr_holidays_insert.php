
<div class="card card-custom gutter-b example example-compact">
                    <?php
                        $FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
                        echo form_open_multipart($FormPath);
                    ?>
									<div class="form-body">
										<div class="form-group row">
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('hr_holidays_ar_title');?></label>
												<input type="text" name="ar_title" class="form-control" value="" required>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('hr_holidays_en_title');?></label>
												<input type="text" name="en_title" class="form-control" value="" required>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('hr_holidays_fromdate');?></label>
												<input type="text" name="fromdate" class="form-control" value="<?php echo date("Y-m-d");?>" required>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-12">
												<label class="col-form-label"><?php echo lang('hr_holidays_todate');?></label>
												<input type="text" name="todate" class="form-control" value="<?php echo date("Y-m-d");?>" required>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot">
										<div class="kt-form__actions">
											<div class="row">
												<div class="col-lg-12 ml-lg-auto">
													<input type="hidden" name="id" value="<?php //echo $id;?>">
													<?php
													$company_id = intval($this->session->userdata('company_id'));
													?>
													<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
													<input type="hidden" name="deleted" value="0">

													<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
                            						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
												</div>
											</div>
										</div>
									</div>
									<?php
                        echo form_close();
                    ?>
                </div>