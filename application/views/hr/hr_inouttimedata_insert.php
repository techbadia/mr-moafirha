
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
	<?php
		$FormPath = base_url().$Segment1."/".$Segment2."/insert_data";
		echo form_open_multipart($FormPath);
	?>
		<div class="form-body">
			<div class="form-group row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<label class="col-form-label"><?php echo lang('hr_vacation_employee_id');?></label>
					<select name="employee_id" class="form-control" required>
						<?php
						$fromdate = date('Y-m-01');
						$todate = date('Y-m-t');
						$EmployeeData = $this->M_hr_employee->GetMultiRow();
						foreach($EmployeeData as $EmployeeData_Row) {
							$employee_id = $EmployeeData_Row->id;
							$CheckEmployee = $this->M_hr_inouttimedata->CheckEmployee($employee_id, $fromdate, $todate);
							if($CheckEmployee == 0) {
							if ($this->session->userdata('lang') == "ar")
							{
								$Employee = $EmployeeData_Row->ar_name;
							}
							else
							{
								$Employee = $EmployeeData_Row->en_name;
							}
						?>
						<option value="<?php echo $EmployeeData_Row->id;?>"><?php echo $Employee;?></option>
						<?php
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-lg-2 col-md-2 col-sm-12 p-3 mb-2 bg-primary text-white">
					<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('Absence_Vacation');?></label>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 p-3 mb-2 bg-primary text-white">
					<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('sale_offer_thedate');?></label>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 p-3 mb-2 bg-primary text-white">
					<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('hr_in_time');?></label>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 p-3 mb-2 bg-primary text-white">
					<label class="col-form-label" style="color:#FFFFFF"><?php echo lang('hr_out_time');?></label>
				</div>
			</div>
			<?php
			$InvoiceItems  = date("t");
			$StartDate = date('Y-m-01');
			$BackgroundColor = "p-3 mb-2 bg-success-o-40";
			$WorkTimeCount = $this->M_hr_workgrouptime->GetRowsCount();
			if($WorkTimeCount > 0)
			{
				$LoginData = $this->M_hr_workgrouptime->GetLogin();
				$LoginTime = $LoginData->fromhoure;
				$LogoutData = $this->M_hr_workgrouptime->GetLogout();
				$LogoutTime = $LogoutData->fromhoure;
			}
			else
			{
				$LoginTime = "09:00:00";
				$LogoutTime = "17:00:00";
			}

			for ($i = 1; $i <= $InvoiceItems; $i++) {
				if($BackgroundColor == "p-3 mb-2 bg-success-o-40")
				{
					$BackgroundColor = "p-3 mb-2 bg-success-o-20";
				}
				else
				{
					$BackgroundColor = "p-3 mb-2 bg-success-o-40";
				}
				$LoopDate = date('Y-m-d',strtotime($StartDate) + (24*60*60*($i - 1)));

				$ThisDayName = date('l', strtotime($LoopDate));
				$ThisDayWeekEnd = $this->M_hr_weekend->CheckWeekEnd($ThisDayName);
				//if($ThisDayWeekEnd == 0) {
			?>
			<div class="form-group row <?php echo $BackgroundColor;?>">	
				<div class="col-lg-2 col-md-2 col-sm-12">
					<select name="Absence<?php echo $i;?>" class="form-control">
						<option value="0"><?php echo lang('No');?></option>
						<option value="1"><?php echo lang('Yes');?></option>
					</select>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12">
					<input type="date" name="thedate<?php echo $i;?>" class="form-control" value="<?php echo $LoopDate;?>" required>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12">
					<input type="time" name="in_time<?php echo $i;?>" class="form-control" value="<?php echo $LoginTime;?>" required>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12">
					<input type="time" name="out_time<?php echo $i;?>" class="form-control" value="<?php echo $LogoutTime;?>" required>
				</div>

			</div>
			<?php
				//}
			}
			?>
		</div>
		<div class="kt-portlet__foot">
			<div class="kt-form__actions">
				<div class="row">
					<div class="col-lg-12 ml-lg-auto">
						<input type="hidden" name="id" value="<?php //echo $id;?>">
						<?php
						$company_id = intval($this->session->userdata('company_id'));
						?>
						<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
						<input type="hidden" name="deleted" value="0">

						<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
						<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
					</div>
				</div>
			</div>
		</div>
	<?php
		echo form_close();
	?>
</div>