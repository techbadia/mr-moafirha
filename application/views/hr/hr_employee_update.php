
<div class="card card-custom gutter-b example example-compact p-3 mb-2 bg-success-o-20">
	<?php
		$FormPath = base_url().$Segment1."/".$Segment2."/Update_Data";
		echo form_open_multipart($FormPath);
	?>
	<div class="form-body">
		<div class="form-group row">
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_country_id');?></label>
				<select name="country_id" class="form-control" required>
					<?php
					$CountryData = $this->M_hr_country->GetMultiRow();
					foreach($CountryData as $CountryData_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Country = $CountryData_Row->ar_title;
						}
						else
						{
							$Country = $CountryData_Row->en_title;
						}
					?>
					<option <?php if($country_id == $CountryData_Row->id) {?>selected<?php }?> value="<?php echo $CountryData_Row->id;?>"><?php echo $Country;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_education_id');?></label>
				<select name="education_id" class="form-control" required>
					<?php
					$EducationData = $this->M_hr_education->GetMultiRow();
					foreach($EducationData as $EducationData_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Education = $EducationData_Row->ar_title;
						}
						else
						{
							$Education = $EducationData_Row->en_title;
						}
					?>
					<option <?php if($education_id == $EducationData_Row->id) {?>selected<?php }?> value="<?php echo $EducationData_Row->id;?>"><?php echo $Education;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_job_id');?></label>
				<select name="job_id" class="form-control" required>
					<?php
					$JobData = $this->M_hr_job->GetMultiRow();
					foreach($JobData as $JobData_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$Job = $JobData_Row->ar_title;
						}
						else
						{
							$Job = $JobData_Row->en_title;
						}
					?>
					<option <?php if($job_id == $JobData_Row->id) {?>selected<?php }?> value="<?php echo $JobData_Row->id;?>"><?php echo $Job;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_job_type_id');?></label>
				<select name="job_type_id" class="form-control" required>
					<?php
					$JobTypeData = $this->M_hr_job_type->GetMultiRow();
					foreach($JobTypeData as $JobTypeData_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$JobType = $JobTypeData_Row->ar_title;
						}
						else
						{
							$JobType = $JobTypeData_Row->en_title;
						}
					?>
					<option <?php if($job_type_id == $JobTypeData_Row->id) {?>selected<?php }?> value="<?php echo $JobTypeData_Row->id;?>"><?php echo $JobType;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_workgroup_id');?></label>
				<select name="workgroup_id" class="form-control" required>
					<?php
					$WorkGroupData = $this->M_hr_workgroup->GetMultiRow();
					foreach($WorkGroupData as $WorkGroupData_Row) {
						if ($this->session->userdata('lang') == "ar")
						{
							$WorkGroup = $WorkGroupData_Row->ar_title;
						}
						else
						{
							$WorkGroup = $WorkGroupData_Row->en_title;
						}
					?>
					<option <?php if($workgroup_id == $WorkGroupData_Row->id) {?>selected<?php }?> value="<?php echo $WorkGroupData_Row->id;?>"><?php echo $WorkGroup;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_pasport_no');?></label>
				<input type="text" name="pasport_no" class="form-control" value="<?php echo $pasport_no;?>" required>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_ar_name');?></label>
				<input type="text" name="ar_name" class="form-control" value="<?php echo $ar_name;?>" required>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_en_name');?></label>
				<input type="text" name="en_name" class="form-control" value="<?php echo $en_name;?>" required>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_phone');?></label>
				<input type="text" name="phone" class="form-control" value="<?php echo $phone;?>" required>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_email');?></label>
				<input type="text" name="email" class="form-control" value="<?php echo $email;?>" required>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_basic_salary');?></label>
				<input type="number" min="0" step="0.001" name="basic_salary" class="form-control" value="<?php echo $basic_salary;?>" required>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_salary_days');?></label>
				<input type="number" min="1" step="1" name="salary_days" class="form-control" value="<?php echo $salary_days;?>" required>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_add_money');?></label>
				<input type="number" min="1" step="1" name="add_money" class="form-control" value="<?php echo $add_money;?>" required>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_deviceno');?></label>
				<input type="number" min="1" step="1" name="employee_deviceno" class="form-control" value="<?php echo $deviceno;?>" required>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_deleted');?></label>
				<select name="deleted" class="form-control">
					<option <?php if($deleted ==0) {?>selected<?php }?> value="0"><?php echo lang('Yes');?></option>
					<option <?php if($deleted ==1) {?>selected<?php }?> value="1"><?php echo lang('No');?></option>
				</select>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_another_address');?></label>
				<input type="text" name="another_address" class="form-control" value="<?php echo $another_address;?>">
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_another_phone');?></label>
				<input type="text" name="another_phone" class="form-control" value="<?php echo $another_phone;?>">
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label"><?php echo lang('employee_another_email');?></label>
				<input type="text" name="another_email" class="form-control" value="<?php echo $another_email;?>">
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label">انتهاء الاقامة</label>
				<input type="date" name="finish_date1" class="form-control" value="<?php echo $finish_date1;?>">
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label">انتهاء المديونيات</label>
				<input type="date" name="finish_date2" class="form-control" value="<?php echo $finish_date2;?>">
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label">انتهاء الكارت الصحي</label>
				<input type="date" name="finish_date3" class="form-control" value="<?php echo $finish_date3;?>">
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label">انتهاء التأمين الصحي</label>
				<input type="date" name="finish_date4" class="form-control" value="<?php echo $finish_date4;?>">
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label">انتهاء رخصة القيادة</label>
				<input type="date" name="finish_date5" class="form-control" value="<?php echo $finish_date5;?>">
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label">انتهاء العقد</label>
				<input type="date" name="finish_date6" class="form-control" value="<?php echo $finish_date6;?>">
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label">انتهاء جواز السفر</label>
				<input type="date" name="finish_date7" class="form-control" value="<?php echo $finish_date7;?>">
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label">انتهاء التأشيرات</label>
				<input type="date" name="finish_date8" class="form-control" value="<?php echo $finish_date8;?>">
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<label class="col-form-label">انتهاء أجير</label>
				<input type="date" name="finish_date9" class="form-control" value="<?php echo $finish_date9;?>">
			</div>
		</div>
	</div>
	<div class="kt-portlet__foot">
		<div class="kt-form__actions">
			<div class="row">
				<div class="col-lg-12 ml-lg-auto">
					<input type="hidden" name="id" value="<?php echo $id;?>">
					<?php
					$company_id = intval($this->session->userdata('company_id'));
					?>
					<input type="hidden" name="company_id" value="<?php echo $company_id;?>">

					<a href="<?php echo base_url().$Segment1."/".$Segment2;?>" class="btn btn-secondary" accesskey="b"><?php echo $this->lang->line('Back');?></a>
					<button type="submit" class="btn btn-primary mr-2" accesskey="s"><?php echo $this->lang->line('Save');?></button>
				</div>
			</div>
		</div>
	</div>
	<?php
		echo form_close();
	?>
</div>