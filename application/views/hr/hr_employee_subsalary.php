
									<!--begin: Datatable -->
									<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
										<thead>
											<tr>
												<th>ID</th>
												<th><?php echo lang('hr_employee_subsalary_employee_id');?></th>
												<th><?php echo lang('hr_employee_subsalary_actionid');?></th>
												<th><?php echo lang('hr_employee_subsalary_thedate');?></th>
												<th><?php echo lang('hr_employee_subsalary_thevalue');?></th>
												<th><?php echo lang('hr_employee_subsalary_notes');?></th>
												<th><?php echo lang('active');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Edit');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Delete');?></th>
												<th style="text-align:center"><?php echo $this->lang->line('Undo');?></th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach($DataRows as $DataRows_Row) {
												$hr_vacation_id = $DataRows_Row->id;
											?>
											<tr>
												<td><?php echo $DataRows_Row->id;?></td>
												<td>
													<?php
													$employee_id = $DataRows_Row->employee_id;
													$EmployeeData = $this->M_hr_employee->GetRow($employee_id);
													echo $EmployeeData->ar_name;
													?>
												</td>
												<td>
													<?php
													$actionid = $DataRows_Row->actionid;
													if($actionid == 1)
													{
														echo $this->lang->line('hr_employee_subsalary_action1');
													}
													else if($actionid == 2)
													{
														echo $this->lang->line('hr_employee_subsalary_action2');
													}
													else
													{
														echo $this->lang->line('hr_employee_subsalary_action3');
													}
													?>
												</td>
												<td><?php echo $DataRows_Row->thedate;?></td>
												<td><?php echo $DataRows_Row->thevalue;?></td>
												<td><?php echo $DataRows_Row->notes;?></td>
												<td>
												<?php
												if($DataRows_Row->deleted == 0)
												{
													echo lang('Yes');
												}
												else
												{
													echo lang('No');
												}
												?>
												</td>
												<td style="text-align:center">
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/updateform/".$DataRows_Row->id;?>">
														<i class="flaticon-edit-1 text-primary icon-lg"></i> 
													</a>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 0) {
													?>
													<a href="<?php echo base_url().$Segment1."/".$Segment2."/delete/".$DataRows_Row->id;?>">
														<i class="flaticon-delete text-danger icon-lg"></i> 
													</a>
													<?php } ?>
												</td>
												<td style="text-align:center">
													<?php
														if($DataRows_Row->deleted == 1) {
													?>
														<a href="<?php echo base_url().$Segment1."/".$Segment2."/undelete/".$DataRows_Row->id;?>">
															<i class="flaticon-refresh text-success icon-lg"></i> 
														</a>
													<?php } ?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										
									</table>

								