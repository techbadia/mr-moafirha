	<div class="sidebar-wrapper" data-simplebar="true">
			<div class="sidebar-header">
				<div>
					<img src="assets/images/logo-icon.png" class="logo-icon" alt=" ">
				</div>
				<div>
					<h4 class="logo-text"></h4>
				</div>
				<div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
				</div>
			</div>
			<!--navigation-->
		 <ul class="metismenu" id="menu" dir="rtl" align="right">
            
            
            <li dir="rtl" align="right">
                <a href="javascript:;" class="has-arrow">
                    <div class="parent-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24"/>
                    <path d="M2,13 L22,13 L22,18 C22,19.1045695 21.1045695,20 20,20 L4,20 C2.8954305,20 2,19.1045695 2,18 L2,13 Z M18.5,18 C19.3284271,18 20,17.3284271 20,16.5 C20,15.6715729 19.3284271,15 18.5,15 C17.6715729,15 17,15.6715729 17,16.5 C17,17.3284271 17.6715729,18 18.5,18 Z M13.5,18 C14.3284271,18 15,17.3284271 15,16.5 C15,15.6715729 14.3284271,15 13.5,15 C12.6715729,15 12,15.6715729 12,16.5 C12,17.3284271 12.6715729,18 13.5,18 Z" fill="#000000"/>
                    <path d="M5.79268604,8 L18.207314,8 C18.5457897,8 18.8612922,8.17121884 19.0457576,8.45501165 L22,13 L2,13 L4.95424243,8.45501165 C5.13870775,8.17121884 5.45421032,8 5.79268604,8 Z" fill="#000000" opacity="0.3"/>
                </g>
            </svg>
            &nbsp;
</i>
                    </div>
                    <div class="menu-title"><?php echo lang('DataManagement');?></div>
                </a>
                <ul > 
                <li> <a href="#"><i class="bx bx-left-arrow-alt"></i><?php echo lang('DataManagement');?></a>
                </li>
                <li> <a href="<?php echo base_url()."mydata/deleterows/";?>"><i class="bx bx-right-arrow-alt"></i><?php echo lang('DataDelete');?></a>
                </li>
                
                    <li> <a href="<?php echo base_url()."mydata/repair/";?>"><i class="bx bx-right-arrow-alt"></i><?php echo lang('Databse_Repair');?></a>
                </li>
                    <li> <a href="<?php echo base_url()."mydata/optimize/";?>"><i class="bx bx-right-arrow-alt"></i><?php echo lang('Databse_Optimize');?></a>
                </li>
                    <li> <a href="<?php echo base_url()."mydata/backup/";?>"><i class="bx bx-right-arrow-alt"></i><?php echo lang('DatabaseBackeup');?></a>
                </li>
                   <?php
            $POS_available = intval($this->session->userdata('POS_available'));
            if($POS_available == 1) {
            ?>
                    <li> <a href="<?php echo base_url()."pos_sys/tec_stores/";?>"><i class="bx bx-right-arrow-alt"></i><?php echo lang('POS_Management');?></a>
                </li>
              <?php }?>  
                
                
            </ul>
            </li>
            
            
            
            
            <li>
                <a href="javascript:;" class="has-arrow">
                    <div class="parent-icon"><i class="bx bx-category"></i>
                    </div>
                    <div class="menu-title"><?php echo lang('Branches');?></div>
                </a>
                <ul>
                    
                      <?php
            $company_id = intval($this->session->userdata('company_id'));
            $CompaniesList = $this->M_app_company->GetMultiRow();
            foreach($CompaniesList as $CompaniesList_Row) {
                if ($this->session->userdata('lang') == "ar")
                {
                    $Title = $CompaniesList_Row->ar_title;
                }
                else
                {
                    $Title = $CompaniesList_Row->en_title;
                }
                $BrancheID = $CompaniesList_Row->id;
            ?>
                    
                    <li> <a href="<?php echo base_url()."branche/updateform/".$BrancheID;?>"><i class="bx bx-right-arrow-alt"></i><?php echo $Title;?></a>
                    </li>
                    <?php } ?>
                </ul>
            </li>
            
            
            
          
            
            
            
            <li>
                <a class="has-arrow" href="javascript:;">
                    <div class="parent-icon"><span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Media\Repeat.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <rect fill="#000000" opacity="0.3" x="11.5" y="2" width="2" height="4" rx="1"/>
        <rect fill="#000000" opacity="0.3" x="11.5" y="16" width="2" height="5" rx="1"/>
        <path d="M15.493,8.044 C15.2143319,7.68933156 14.8501689,7.40750104 14.4005,7.1985 C13.9508311,6.98949895 13.5170021,6.885 13.099,6.885 C12.8836656,6.885 12.6651678,6.90399981 12.4435,6.942 C12.2218322,6.98000019 12.0223342,7.05283279 11.845,7.1605 C11.6676658,7.2681672 11.5188339,7.40749914 11.3985,7.5785 C11.2781661,7.74950085 11.218,7.96799867 11.218,8.234 C11.218,8.46200114 11.2654995,8.65199924 11.3605,8.804 C11.4555005,8.95600076 11.5948324,9.08899943 11.7785,9.203 C11.9621676,9.31700057 12.1806654,9.42149952 12.434,9.5165 C12.6873346,9.61150047 12.9723317,9.70966616 13.289,9.811 C13.7450023,9.96300076 14.2199975,10.1308324 14.714,10.3145 C15.2080025,10.4981676 15.6576646,10.7419985 16.063,11.046 C16.4683354,11.3500015 16.8039987,11.7268311 17.07,12.1765 C17.3360013,12.6261689 17.469,13.1866633 17.469,13.858 C17.469,14.6306705 17.3265014,15.2988305 17.0415,15.8625 C16.7564986,16.4261695 16.3733357,16.8916648 15.892,17.259 C15.4106643,17.6263352 14.8596698,17.8986658 14.239,18.076 C13.6183302,18.2533342 12.97867,18.342 12.32,18.342 C11.3573285,18.342 10.4263378,18.1741683 9.527,17.8385 C8.62766217,17.5028317 7.88033631,17.0246698 7.285,16.404 L9.413,14.238 C9.74233498,14.6433354 10.176164,14.9821653 10.7145,15.2545 C11.252836,15.5268347 11.7879973,15.663 12.32,15.663 C12.5606679,15.663 12.7949989,15.6376669 13.023,15.587 C13.2510011,15.5363331 13.4504991,15.4540006 13.6215,15.34 C13.7925009,15.2259994 13.9286662,15.0740009 14.03,14.884 C14.1313338,14.693999 14.182,14.4660013 14.182,14.2 C14.182,13.9466654 14.1186673,13.7313342 13.992,13.554 C13.8653327,13.3766658 13.6848345,13.2151674 13.4505,13.0695 C13.2161655,12.9238326 12.9248351,12.7908339 12.5765,12.6705 C12.2281649,12.5501661 11.8323355,12.420334 11.389,12.281 C10.9583312,12.141666 10.5371687,11.9770009 10.1255,11.787 C9.71383127,11.596999 9.34650161,11.3531682 9.0235,11.0555 C8.70049838,10.7578318 8.44083431,10.3968355 8.2445,9.9725 C8.04816568,9.54816454 7.95,9.03200304 7.95,8.424 C7.95,7.67666293 8.10199848,7.03700266 8.406,6.505 C8.71000152,5.97299734 9.10899753,5.53600171 9.603,5.194 C10.0970025,4.85199829 10.6543302,4.60183412 11.275,4.4435 C11.8956698,4.28516587 12.5226635,4.206 13.156,4.206 C13.9160038,4.206 14.6918294,4.34533194 15.4835,4.624 C16.2751706,4.90266806 16.9686637,5.31433061 17.564,5.859 L15.493,8.044 Z" fill="#000000"/>
    </g>
</svg><!--end::Svg Icon--></span><i >  
                    &nbsp;
                    </i>
                    </div>
                    <div class="menu-title">إدارة العهدة</div>
                </a>
                <ul
                    
                            
                    <li> <a href="https://2030saudivision.com/apps/car_maintenace/custody/usr_usersgroup"><i class="bx bx-right-arrow-alt"></i> مجموعات العمل  </a>
                    </li>
                      <li> <a href="https://2030saudivision.com/apps/car_maintenace/custody/usr_usersgroup"><i class="bx bx-right-arrow-alt"></i> بيانات المستخدمين   </a>
                    </li>
          
            <li> <a href="https://2030saudivision.com/apps/car_maintenace/custody/usr_usersgroup"><i class="bx bx-right-arrow-alt"></i>  الملف الشخصي  </a>
                    </li>
          
          
          
          
        
          
          
          
          
                </ul>
            </li>
            
            
            
            
            
            
            
            
            
            
            
            <!-- ------------------------------------------- -->
             <li>
                <a class="has-arrow" href="javascript:;">
                    <div class="parent-icon">
                        
                        
                        
                        <i >  
                        
                        <span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Cupboard.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M3.5,3 L9.5,3 C10.3284271,3 11,3.67157288 11,4.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L3.5,20 C2.67157288,20 2,19.3284271 2,18.5 L2,4.5 C2,3.67157288 2.67157288,3 3.5,3 Z M9,9 C8.44771525,9 8,9.44771525 8,10 L8,12 C8,12.5522847 8.44771525,13 9,13 C9.55228475,13 10,12.5522847 10,12 L10,10 C10,9.44771525 9.55228475,9 9,9 Z" fill="#000000" opacity="0.3"/>
        <path d="M14.5,3 L20.5,3 C21.3284271,3 22,3.67157288 22,4.5 L22,18.5 C22,19.3284271 21.3284271,20 20.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,4.5 C13,3.67157288 13.6715729,3 14.5,3 Z M20,9 C19.4477153,9 19,9.44771525 19,10 L19,12 C19,12.5522847 19.4477153,13 20,13 C20.5522847,13 21,12.5522847 21,12 L21,10 C21,9.44771525 20.5522847,9 20,9 Z" fill="#000000" transform="translate(17.500000, 11.500000) scale(-1, 1) translate(-17.500000, -11.500000) "/>
    </g>
</svg><!--end::Svg Icon--></span>
                    &nbsp;
                    </i>
                    </div>
                    <div class="menu-title">إدارة المخازن</div>
                </a>
                <ul
                    
                     
             
                      
                    <li> <a href="https://2030saudivision.com/apps/car_maintenace/store/store"><i class="bx bx-right-arrow-alt"></i>  قائمة المخازن  </a>
                    </li>
                      <li> <a href="https://2030saudivision.com/apps/car_maintenace/store/store_move"><i class="bx bx-right-arrow-alt"></i>  نقل من مخزن إلى مخزن   </a>
                    </li>
          
            <li> <a href="https://2030saudivision.com/apps/car_maintenace/store/store_category"><i class="bx bx-right-arrow-alt"></i>   تصنيفات المنتجات  </a>
                    </li>
          
          
          
            <li> <a href="https://2030saudivision.com/apps/car_maintenace/store/store_brand"><i class="bx bx-right-arrow-alt"></i>   موديلات المنتجات  </a>
                    </li>
          
          
            <li> <a href="https://2030saudivision.com/apps/car_maintenace/store/product"><i class="bx bx-right-arrow-alt"></i>    بيانات الأصناف  </a>
                    </li>
          
          
            <li> <a href="https://2030saudivision.com/apps/car_maintenace/store/store_quantity"><i class="bx bx-right-arrow-alt"></i>    كمية الأصناف  </a>
                    </li>
          
          
          
          
                </ul>
            </li>
            
               <!-- ------------------------------------------- -->
             <li>
                <a class="has-arrow" href="javascript:;">
                    <div class="parent-icon">
                        
                        
                        
                        <i >  
                        
                        <span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Cupboard.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M3.5,3 L9.5,3 C10.3284271,3 11,3.67157288 11,4.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L3.5,20 C2.67157288,20 2,19.3284271 2,18.5 L2,4.5 C2,3.67157288 2.67157288,3 3.5,3 Z M9,9 C8.44771525,9 8,9.44771525 8,10 L8,12 C8,12.5522847 8.44771525,13 9,13 C9.55228475,13 10,12.5522847 10,12 L10,10 C10,9.44771525 9.55228475,9 9,9 Z" fill="#000000" opacity="0.3"/>
        <path d="M14.5,3 L20.5,3 C21.3284271,3 22,3.67157288 22,4.5 L22,18.5 C22,19.3284271 21.3284271,20 20.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,4.5 C13,3.67157288 13.6715729,3 14.5,3 Z M20,9 C19.4477153,9 19,9.44771525 19,10 L19,12 C19,12.5522847 19.4477153,13 20,13 C20.5522847,13 21,12.5522847 21,12 L21,10 C21,9.44771525 20.5522847,9 20,9 Z" fill="#000000" transform="translate(17.500000, 11.500000) scale(-1, 1) translate(-17.500000, -11.500000) "/>
    </g>
</svg><!--end::Svg Icon--></span>
                    &nbsp;
                    </i>
                    </div>
                    <div class="menu-title">إدارة التصنيع</div>
                </a>
                <ul
                    
                     
             
                      
                    <li> <a href="https://2030saudivision.com/apps/car_maintenace/manufacturing/manu_manufacturing_orders"><i class="bx bx-right-arrow-alt"></i>  أوامر التصنيع   </a>
                    </li>
                      <li> <a href="https://2030saudivision.com/apps/car_maintenace/manufacturing/manu_components"><i class="bx bx-right-arrow-alt"></i>   مكونات التصنيع      </a>
                    </li>
    
          
          
          
                </ul>
            </li>
            
                          <!-- ------------------------------------------- -->
             <li>
                <a class="has-arrow" href="javascript:;">
                    <div class="parent-icon">
                        
                        
                        
                        <i >  
                        
                       <span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Media\Repeat.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		<rect x="0" y="0" width="24" height="24"></rect>
		<path d="M15.9497475,3.80761184 L13.0246125,6.73274681 C12.2435639,7.51379539 12.2435639,8.78012535 13.0246125,9.56117394 L14.4388261,10.9753875 C15.2198746,11.7564361 16.4862046,11.7564361 17.2672532,10.9753875 L20.1923882,8.05025253 C20.7341101,10.0447871 20.2295941,12.2556873 18.674559,13.8107223 C16.8453326,15.6399488 14.1085592,16.0155296 11.8839934,14.9444337 L6.75735931,20.0710678 C5.97631073,20.8521164 4.70998077,20.8521164 3.92893219,20.0710678 C3.1478836,19.2900192 3.1478836,18.0236893 3.92893219,17.2426407 L9.05556629,12.1160066 C7.98447038,9.89144078 8.36005124,7.15466739 10.1892777,5.32544095 C11.7443127,3.77040588 13.9552129,3.26588995 15.9497475,3.80761184 Z" fill="#000000"></path>
		<path d="M16.6568542,5.92893219 L18.0710678,7.34314575 C18.4615921,7.73367004 18.4615921,8.36683502 18.0710678,8.75735931 L16.6913928,10.1370344 C16.3008685,10.5275587 15.6677035,10.5275587 15.2771792,10.1370344 L13.8629656,8.7228208 C13.4724413,8.33229651 13.4724413,7.69913153 13.8629656,7.30860724 L15.2426407,5.92893219 C15.633165,5.5384079 16.26633,5.5384079 16.6568542,5.92893219 Z" fill="#000000" opacity="0.3"></path>
	</g>
</svg><!--end::Svg Icon--></span>
                    &nbsp;
                    </i>
                    </div>
                    <div class="menu-title">إدارة الخدمات</div>
                </a>
                <ul
                    
                     
             
                      
                    <li> <a href="https://2030saudivision.com/apps/car_maintenace/custody/usr_usersgroup"><i class="bx bx-right-arrow-alt"></i>   إعدادت الخدمات   </a>
                    </li>
                      <li> <a href="https://2030saudivision.com/apps/car_maintenace/maintenance/data_request"><i class="bx bx-right-arrow-alt"></i>    طلبات الخدمات      </a>
                    </li>
    
                <li> <a href="https://2030saudivision.com/apps/car_maintenace/maintenance/data_invoice"><i class="bx bx-right-arrow-alt"></i>    فواتير الخدمات       </a>
                    </li>
    
          
          
          
                </ul>
            </li>
            
                          <!-- ------------------------------------------- -->
             <li>
                <a class="has-arrow" href="javascript:;">
                    <div class="parent-icon">
                        
                        
                        
                        <i >  
                        
                        <span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Cupboard.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M3.5,3 L9.5,3 C10.3284271,3 11,3.67157288 11,4.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L3.5,20 C2.67157288,20 2,19.3284271 2,18.5 L2,4.5 C2,3.67157288 2.67157288,3 3.5,3 Z M9,9 C8.44771525,9 8,9.44771525 8,10 L8,12 C8,12.5522847 8.44771525,13 9,13 C9.55228475,13 10,12.5522847 10,12 L10,10 C10,9.44771525 9.55228475,9 9,9 Z" fill="#000000" opacity="0.3"/>
        <path d="M14.5,3 L20.5,3 C21.3284271,3 22,3.67157288 22,4.5 L22,18.5 C22,19.3284271 21.3284271,20 20.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,4.5 C13,3.67157288 13.6715729,3 14.5,3 Z M20,9 C19.4477153,9 19,9.44771525 19,10 L19,12 C19,12.5522847 19.4477153,13 20,13 C20.5522847,13 21,12.5522847 21,12 L21,10 C21,9.44771525 20.5522847,9 20,9 Z" fill="#000000" transform="translate(17.500000, 11.500000) scale(-1, 1) translate(-17.500000, -11.500000) "/>
    </g>
</svg><!--end::Svg Icon--></span>
                    &nbsp;
                    </i>
                    </div>
                    <div class="menu-title">الدعم الفني </div>
                </a>
                <ul
                    
                     
             
                      
                    <li> <a href="https://2030saudivision.com/apps/car_maintenace/support/tic_category"><i class="bx bx-right-arrow-alt"></i>   تصنيفات التذاكر    </a>
                    </li>
                      <li> <a href="https://2030saudivision.com/apps/car_maintenace/support/tic_priority"><i class="bx bx-right-arrow-alt"></i>    درجات الاهمية      </a>
                    </li>
    
              <li> <a href="https://2030saudivision.com/apps/car_maintenace/support/tic_status"><i class="bx bx-right-arrow-alt"></i>     حالات التذاكر       </a>
                    </li>
    
            <li> <a href="https://2030saudivision.com/apps/car_maintenace/support/tic_ticket"><i class="bx bx-right-arrow-alt"></i>      تذاكر الدعم        </a>
                    </li>
             <li> <a href="https://2030saudivision.com/apps/car_maintenace/support/whatsapp_chat"><i class="bx bx-right-arrow-alt"></i>      محادثة واتس اب         </a>
                    </li>
          
                </ul>
            </li>
            
            
            
                   
                          <!-- ------------------------------------------- -->
             <li>
                <a class="has-arrow" href="javascript:;">
                    <div class="parent-icon">
                        
                        
                        
                        <i >  
                        
                        <span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Cupboard.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M3.5,3 L9.5,3 C10.3284271,3 11,3.67157288 11,4.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L3.5,20 C2.67157288,20 2,19.3284271 2,18.5 L2,4.5 C2,3.67157288 2.67157288,3 3.5,3 Z M9,9 C8.44771525,9 8,9.44771525 8,10 L8,12 C8,12.5522847 8.44771525,13 9,13 C9.55228475,13 10,12.5522847 10,12 L10,10 C10,9.44771525 9.55228475,9 9,9 Z" fill="#000000" opacity="0.3"/>
        <path d="M14.5,3 L20.5,3 C21.3284271,3 22,3.67157288 22,4.5 L22,18.5 C22,19.3284271 21.3284271,20 20.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,4.5 C13,3.67157288 13.6715729,3 14.5,3 Z M20,9 C19.4477153,9 19,9.44771525 19,10 L19,12 C19,12.5522847 19.4477153,13 20,13 C20.5522847,13 21,12.5522847 21,12 L21,10 C21,9.44771525 20.5522847,9 20,9 Z" fill="#000000" transform="translate(17.500000, 11.500000) scale(-1, 1) translate(-17.500000, -11.500000) "/>
    </g>
</svg><!--end::Svg Icon--></span>
                    &nbsp;
                    </i>
                    </div>
                    <div class="menu-title"> الإعدادت</div>
                </a>
                <ul
                    
                     
             
                      
                    <li> <a href="https://2030saudivision.com/apps/car_maintenace/settings/settings_acceptance"><i class="bx bx-right-arrow-alt"></i>   إعدادت القبول   </a>
                    </li>
                      <li> <a href="https://2030saudivision.com/apps/car_maintenace/settings/fin_settings"><i class="bx bx-right-arrow-alt"></i>      إعدادت الحسابات   </a>
                
                    </li>
    
          
             <li> <a href="https://2030saudivision.com/apps/car_maintenace/settings/settings_notifications"><i class="bx bx-right-arrow-alt"></i>      إعدادت التنبيهات   </a>
                
                    </li>
    
             <li> <a href="https://2030saudivision.com/apps/car_maintenace/settings/settings_print"><i class="bx bx-right-arrow-alt"></i>      إعدادت الطباعة   </a>
                
                    </li>
    
          
          
                </ul>
            </li>
 
            
                   
                          <!-- ------------------------------------------- -->
             <li>
                <a class="has-arrow" href="javascript:;">
                    <div class="parent-icon">
                        
                        
                        
                        <i >  
                        
                        <span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Cupboard.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M3.5,3 L9.5,3 C10.3284271,3 11,3.67157288 11,4.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L3.5,20 C2.67157288,20 2,19.3284271 2,18.5 L2,4.5 C2,3.67157288 2.67157288,3 3.5,3 Z M9,9 C8.44771525,9 8,9.44771525 8,10 L8,12 C8,12.5522847 8.44771525,13 9,13 C9.55228475,13 10,12.5522847 10,12 L10,10 C10,9.44771525 9.55228475,9 9,9 Z" fill="#000000" opacity="0.3"/>
        <path d="M14.5,3 L20.5,3 C21.3284271,3 22,3.67157288 22,4.5 L22,18.5 C22,19.3284271 21.3284271,20 20.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,4.5 C13,3.67157288 13.6715729,3 14.5,3 Z M20,9 C19.4477153,9 19,9.44771525 19,10 L19,12 C19,12.5522847 19.4477153,13 20,13 C20.5522847,13 21,12.5522847 21,12 L21,10 C21,9.44771525 20.5522847,9 20,9 Z" fill="#000000" transform="translate(17.500000, 11.500000) scale(-1, 1) translate(-17.500000, -11.500000) "/>
    </g>
</svg><!--end::Svg Icon--></span>
                    &nbsp;
                    </i>
                    </div>
                    <div class="menu-title">إدارة التصنيع</div>
                </a>
                <ul
                    
                     
             
                      
                    <li> <a href="https://2030saudivision.com/apps/car_maintenace/custody/usr_usersgroup"><i class="bx bx-right-arrow-alt"></i>  أوامر التصنيع   </a>
                    </li>
                      <li> <a href="https://2030saudivision.com/apps/car_maintenace/custody/usr_usersgroup"><i class="bx bx-right-arrow-alt"></i>   مكونات التصنيع      </a>
                    </li>
    
                     
                          <!-- ------------------------------------------- -->
             <li>
                <a class="has-arrow" href="javascript:;">
                    <div class="parent-icon">
                        
                        
                        
                        <i >  
                        
                        <span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Cupboard.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M3.5,3 L9.5,3 C10.3284271,3 11,3.67157288 11,4.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L3.5,20 C2.67157288,20 2,19.3284271 2,18.5 L2,4.5 C2,3.67157288 2.67157288,3 3.5,3 Z M9,9 C8.44771525,9 8,9.44771525 8,10 L8,12 C8,12.5522847 8.44771525,13 9,13 C9.55228475,13 10,12.5522847 10,12 L10,10 C10,9.44771525 9.55228475,9 9,9 Z" fill="#000000" opacity="0.3"/>
        <path d="M14.5,3 L20.5,3 C21.3284271,3 22,3.67157288 22,4.5 L22,18.5 C22,19.3284271 21.3284271,20 20.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,4.5 C13,3.67157288 13.6715729,3 14.5,3 Z M20,9 C19.4477153,9 19,9.44771525 19,10 L19,12 C19,12.5522847 19.4477153,13 20,13 C20.5522847,13 21,12.5522847 21,12 L21,10 C21,9.44771525 20.5522847,9 20,9 Z" fill="#000000" transform="translate(17.500000, 11.500000) scale(-1, 1) translate(-17.500000, -11.500000) "/>
    </g>
</svg><!--end::Svg Icon--></span>
                    &nbsp;
                    </i>
                    </div>
                    <div class="menu-title">إدارة الصلاحيات</div>
                </a>
                <ul
                    
                     
             
                      
                    <li> <a href="https://2030saudivision.com/apps/car_maintenace/custody/usr_usersgroup"><i class="bx bx-right-arrow-alt"></i>   مجموعات العمل    </a>
                    </li>
                      <li> <a href="https://2030saudivision.com/apps/car_maintenace/custody/usr_usersgroup"><i class="bx bx-right-arrow-alt"></i>   بيانات المستخدمين       </a>
                    </li>
                  <li> <a href="https://2030saudivision.com/apps/car_maintenace/custody/usr_usersgroup"><i class="bx bx-right-arrow-alt"></i>    الملف الشخصي        </a>
                    </li>
               
                          <!-- ------------------------------------------- -->
             <li>
                <a class="has-arrow" href="javascript:;">
                    <div class="parent-icon">
                        
                        
                        
                        <i >  
                        
                        <span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Cupboard.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M3.5,3 L9.5,3 C10.3284271,3 11,3.67157288 11,4.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L3.5,20 C2.67157288,20 2,19.3284271 2,18.5 L2,4.5 C2,3.67157288 2.67157288,3 3.5,3 Z M9,9 C8.44771525,9 8,9.44771525 8,10 L8,12 C8,12.5522847 8.44771525,13 9,13 C9.55228475,13 10,12.5522847 10,12 L10,10 C10,9.44771525 9.55228475,9 9,9 Z" fill="#000000" opacity="0.3"/>
        <path d="M14.5,3 L20.5,3 C21.3284271,3 22,3.67157288 22,4.5 L22,18.5 C22,19.3284271 21.3284271,20 20.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,4.5 C13,3.67157288 13.6715729,3 14.5,3 Z M20,9 C19.4477153,9 19,9.44771525 19,10 L19,12 C19,12.5522847 19.4477153,13 20,13 C20.5522847,13 21,12.5522847 21,12 L21,10 C21,9.44771525 20.5522847,9 20,9 Z" fill="#000000" transform="translate(17.500000, 11.500000) scale(-1, 1) translate(-17.500000, -11.500000) "/>
    </g>
</svg><!--end::Svg Icon--></span>
                    &nbsp;
                    </i>
                    </div>
                    <div class="menu-title">إدارة التصنيع</div>
                </a>
                <ul
                    
                     
             
                      
                    <li> <a href="https://2030saudivision.com/apps/car_maintenace/custody/usr_usersgroup"><i class="bx bx-right-arrow-alt"></i>  أوامر التصنيع   </a>
                    </li>
                      <li> <a href="https://2030saudivision.com/apps/car_maintenace/custody/usr_usersgroup"><i class="bx bx-right-arrow-alt"></i>   مكونات التصنيع      </a>
                    </li>
    
          
          
                </ul>
            </li>
            
            
            
            
            
              
        </ul>
        

			<!--end navigation-->
		</div>