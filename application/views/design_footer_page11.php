<!--begin::Global Config(global config for global JS scripts)-->
<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
<!--end::Global Config-->
<!--begin::Global Theme Bundle(used by all pages)-->
<script src="<?php echo base_url();?>assets/plugins/global/plugins.bundle.js"></script>
<script src="<?php echo base_url();?>assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
<script src="<?php echo base_url();?>assets/js/scripts.bundle.js"></script>
<!--end::Global Theme Bundle-->
<?php
if(($Segment1 == "service") && ($Segment2 == "serv_message") && ($Segment3 == "view") || 
($Segment1 == "service") && ($Segment2 == "serv_message") && ($Segment3 == "insertform") || 
($Segment1 == "service") && ($Segment2 == "serv_message") && ($Segment3 == "updateform") || 
($Segment1 == "service") && ($Segment2 == "serv_message") && ($Segment3 == "reply") || 
($Segment1 == "service") && ($Segment2 == "serv_events") && ($Segment3 == "insertform") || 
($Segment1 == "service") && ($Segment2 == "serv_events") && ($Segment3 == "updateform"))
{
?>
<script src="<?php echo base_url();?>assets/plugins/custom/tinymce/tinymce.bundle.js"></script>
<script src="<?php echo base_url();?>assets/js/pages/crud/forms/editors/tinymce.js"></script>
<?php } ?>

<?php
if(($Segment1 == "permissions") && ($Segment2 == "profile") && ($Segment3 == "serv_message") || 
($Segment1 == "permissions") && ($Segment2 == "profile") && ($Segment3 == "serv_message_sent") || 
($Segment1 == "permissions") && ($Segment2 == "profile") && ($Segment3 == "serv_action") || 
($Segment1 == "permissions") && ($Segment2 == "profile") && ($Segment3 == "serv_action_outbound") || 
($Segment1 == "permissions") && ($Segment2 == "profile") && ($Segment3 == "tas_tasks") || 
($Segment1 == "permissions") && ($Segment2 == "profile") && ($Segment3 == "proj_project_level_task") || 
($Segment1 == "permissions") && ($Segment2 == "profile") && ($Segment3 == "schedule_of_work") || 
($Segment1 == "permissions") && ($Segment2 == "profile") && ($Segment3 == "custody") || 
($Segment1 == "permissions") && ($Segment2 == "profile") && ($Segment3 == "financial_custody"))
{
?>
<script src="<?php echo base_url();?>assets/js/pages/crud/ktdatatable/base/html-table.js"></script>
<?php } ?>
<script src="<?php echo base_url();?>assets/js/pages/crud/ktdatatable/base/html-table.js"></script>
<?php
if(($Segment1 == "cars") && ($Segment2 == "cars_cars"))
{
?>
<script src="<?php echo base_url();?>assets/js/pages/crud/datatables/advanced/row-grouping.js"></script>
<?php } ?>


<?php
if(($Segment3 != "insertform") && ($Segment3 != "updateform"))
{
?>
<!--begin::Page Vendors(used by this page)-->
<script src="<?php echo base_url();?>assets/plugins/custom/datatables/datatables.bundle.js"></script>
<!--end::Page Vendors-->
<!--begin::Page Scripts(used by this page)-->
<script src="<?php echo base_url();?>assets/js/pages/crud/datatables/extensions/buttons.js"></script>
<?php } ?>
<!--begin::Page Scripts(used by this page)-->
<script src="<?php echo base_url();?>assets/js/pages/crud/forms/widgets/select2.js"></script>
<script src="<?php echo base_url();?>assets/js/pages/crud/forms/widgets/bootstrap-select.js"></script>
<!--end::Page Scripts-->
<?php
if(($Segment2 == "fin_journal") && ($Segment3 == ""))
{
?>
<script type="text/javascript">
			$(document).ready(function() {

				$('.view_detail').click(function(){
					
					var id = $(this).attr('relid'); //get the attribute value
					
					$.ajax({
						url : "<?php echo base_url(); ?>account/fin_journal/view_main",
						data:{id : id},
						method:'GET',
						dataType:'json',
						success:function(response) {
							$('#details').html(response.details); //hold the response in id and show on popup
							$('#thedate').html(response.thedate);
							$('#bill_id').html(response.bill_id);
							$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
							records = JSON.parse(data);
						}
					});


					$.ajax({
						url: '<?php echo base_url(); ?>account/fin_journal/view_details',
						data:{id : id},
						type: 'get',
						dataType: 'JSON',
						success: function(response){
							var len = response.length;
							$("#ModalTable").find("tbody").empty();
							for(var i=0; i<len; i++){
								var id = response[i].id;
								var title = response[i].title;
								var title_en = response[i].title_en;
								var debit = response[i].debit;
								var creditor = response[i].creditor;
								<?php
								if ($this->session->userdata('lang') == "ar")
								{
								?>
								var tr_str = "<tr>" +
									"<td align='right'>" + title + "</td>" +
									"<td align='center'>" + debit + "</td>" +
									"<td align='center'>" + creditor + "</td>" +
									"</tr>";
								<?php
								}
								else
								{
								?>
								var tr_str = "<tr>" +
									"<td align='right'>" + title_en + "</td>" +
									"<td align='center'>" + debit + "</td>" +
									"<td align='center'>" + creditor + "</td>" +
									"</tr>";
								<?php
								}
								?>
								$("#ModalTable tbody").append(tr_str);
							}

						}
					});

				});

				
			});

		</script>

		<script type="text/javascript">
		function printSelection(node){
			var content=node.innerHTML;
			var pwin=window.open('','print_content','width=600,height=600');

			pwin.document.open();
			pwin.document.write('<html><head><title></title>');
			pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
			pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
			pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
			pwin.document.write('</head>');
			pwin.document.write('<body onload="window.print()" style="direction: rtl; text-align: right">');
			pwin.document.write(content);
			pwin.document.write('<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></' + 'script>');
			pwin.document.write('<script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></' + 'script>');
			pwin.document.write('</body></html>');
			pwin.document.close();
		
			pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
			pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
			pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

			setTimeout(function(){pwin.close();},1000);

		}
		</script>
<?php
}
?>
<?php
if(($Segment2 == "mon_exchange_bonds") && ($Segment3 == ""))
{
?>
<script type="text/javascript">
			$(document).ready(function() {

				$('.view_detail').click(function(){
					
					var id = $(this).attr('relid'); //get the attribute value
					
					$.ajax({
						url : "<?php echo base_url(); ?>account/mon_exchange_bonds/view_main",
						data:{id : id},
						method:'GET',
						dataType:'json',
						success:function(response) {
							$('#details').html(response.details); //hold the response in id and show on popup
							$('#thedate').html(response.thedate);
							$('#bill_id').html(response.bill_id);
							$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
							records = JSON.parse(data);
						}
					});


					$.ajax({
						url: '<?php echo base_url(); ?>account/mon_exchange_bonds/view_details',
						data:{id : id},
						type: 'get',
						dataType: 'JSON',
						success: function(response){
							var len = response.length;
							$("#ModalTable").find("tbody").empty();
							for(var i=0; i<len; i++){
								var id = response[i].id;
								var title = response[i].title;
								var title_en = response[i].title_en;
								var debit = response[i].debit;
								var creditor = response[i].creditor;
								<?php
								if ($this->session->userdata('lang') == "ar")
								{
								?>
								var tr_str = "<tr>" +
									"<td align='right'>" + title + "</td>" +
									"<td align='center'>" + debit + "</td>" +
									"<td align='center'>" + creditor + "</td>" +
									"</tr>";
								<?php
								}
								else
								{
								?>
								var tr_str = "<tr>" +
									"<td align='right'>" + title_en + "</td>" +
									"<td align='center'>" + debit + "</td>" +
									"<td align='center'>" + creditor + "</td>" +
									"</tr>";
								<?php
								}
								?>
								$("#ModalTable tbody").append(tr_str);
							}

						}
					});

				});

				
			});

		</script>

		<script type="text/javascript">
		function printSelection(node){
			var content=node.innerHTML;
			var pwin=window.open('','print_content','width=600,height=600');

			pwin.document.open();
			pwin.document.write('<html><head><title></title>');
			pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
			pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
			pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
			pwin.document.write('</head>');
			pwin.document.write('<body onload="window.print()" style="direction: rtl; text-align: right">');
			pwin.document.write(content);
			pwin.document.write('<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></' + 'script>');
			pwin.document.write('<script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></' + 'script>');
			pwin.document.write('</body></html>');
			pwin.document.close();
		
			pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
			pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
			pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

			setTimeout(function(){pwin.close();},1000);

		}
		</script>
<?php
}
?>
<?php
if(($Segment2 == "mon_receipts") && ($Segment3 == ""))
{
?>
<script type="text/javascript">
			$(document).ready(function() {

				$('.view_detail').click(function(){
					
					var id = $(this).attr('relid'); //get the attribute value
					
					$.ajax({
						url : "<?php echo base_url(); ?>account/fin_journal/view_main",
						data:{id : id},
						method:'GET',
						dataType:'json',
						success:function(response) {
							$('#details').html(response.details); //hold the response in id and show on popup
							$('#thedate').html(response.thedate);
							$('#bill_id').html(response.bill_id);
							$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
							records = JSON.parse(data);
						}
					});


					$.ajax({
						url: '<?php echo base_url(); ?>account/fin_journal/view_details',
						data:{id : id},
						type: 'get',
						dataType: 'JSON',
						success: function(response){
							var len = response.length;
							$("#ModalTable").find("tbody").empty();
							for(var i=0; i<len; i++){
								var id = response[i].id;
								var title = response[i].title;
								var title_en = response[i].title_en;
								var debit = response[i].debit;
								var creditor = response[i].creditor;
								<?php
								if ($this->session->userdata('lang') == "ar")
								{
								?>
								var tr_str = "<tr>" +
									"<td align='right'>" + title + "</td>" +
									"<td align='center'>" + debit + "</td>" +
									"<td align='center'>" + creditor + "</td>" +
									"</tr>";
								<?php
								}
								else
								{
								?>
								var tr_str = "<tr>" +
									"<td align='right'>" + title_en + "</td>" +
									"<td align='center'>" + debit + "</td>" +
									"<td align='center'>" + creditor + "</td>" +
									"</tr>";
								<?php
								}
								?>
								$("#ModalTable tbody").append(tr_str);
							}

						}
					});

				});

				
			});

		</script>

		<script type="text/javascript">
		function printSelection(node){
			var content=node.innerHTML;
			var pwin=window.open('','print_content','width=600,height=600');

			pwin.document.open();
			pwin.document.write('<html><head><title></title>');
			pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
			pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
			pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
			pwin.document.write('</head>');
			pwin.document.write('<body onload="window.print()" style="direction: rtl; text-align: right">');
			pwin.document.write(content);
			pwin.document.write('<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></' + 'script>');
			pwin.document.write('<script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></' + 'script>');
			pwin.document.write('</body></html>');
			pwin.document.close();
		
			pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
			pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
			pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

			setTimeout(function(){pwin.close();},1000);

		}
		</script>
<?php
}
?>
<?php
if(($Segment2 == "ban_exchange_bonds") && ($Segment3 == ""))
{
?>
<script type="text/javascript">
			$(document).ready(function() {

				$('.view_detail').click(function(){
					
					var id = $(this).attr('relid'); //get the attribute value
					
					$.ajax({
						url : "<?php echo base_url(); ?>account/ban_exchange_bonds/view_main",
						data:{id : id},
						method:'GET',
						dataType:'json',
						success:function(response) {
							$('#details').html(response.details); //hold the response in id and show on popup
							$('#thedate').html(response.thedate);
							$('#bill_id').html(response.bill_id);
							$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
							records = JSON.parse(data);
						}
					});


					$.ajax({
						url: '<?php echo base_url(); ?>account/ban_exchange_bonds/view_details',
						data:{id : id},
						type: 'get',
						dataType: 'JSON',
						success: function(response){
							var len = response.length;
							$("#ModalTable").find("tbody").empty();
							for(var i=0; i<len; i++){
								var id = response[i].id;
								var title = response[i].title;
								var title_en = response[i].title_en;
								var debit = response[i].debit;
								var creditor = response[i].creditor;
								<?php
								if ($this->session->userdata('lang') == "ar")
								{
								?>
								var tr_str = "<tr>" +
									"<td align='right'>" + title + "</td>" +
									"<td align='center'>" + debit + "</td>" +
									"<td align='center'>" + creditor + "</td>" +
									"</tr>";
								<?php
								}
								else
								{
								?>
								var tr_str = "<tr>" +
									"<td align='right'>" + title_en + "</td>" +
									"<td align='center'>" + debit + "</td>" +
									"<td align='center'>" + creditor + "</td>" +
									"</tr>";
								<?php
								}
								?>
								$("#ModalTable tbody").append(tr_str);
							}

						}
					});

				});

				
			});

		</script>

		<script type="text/javascript">
		function printSelection(node){
			var content=node.innerHTML;
			var pwin=window.open('','print_content','width=600,height=600');

			pwin.document.open();
			pwin.document.write('<html><head><title></title>');
			pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
			pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
			pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
			pwin.document.write('</head>');
			pwin.document.write('<body onload="window.print()" style="direction: rtl; text-align: right">');
			pwin.document.write(content);
			pwin.document.write('<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></' + 'script>');
			pwin.document.write('<script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></' + 'script>');
			pwin.document.write('</body></html>');
			pwin.document.close();
		
			pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
			pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
			pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

			setTimeout(function(){pwin.close();},1000);

		}
		</script>
<?php
}
?>
<?php
if(($Segment2 == "ban_receipts") && ($Segment3 == ""))
{
?>
<script type="text/javascript">
			$(document).ready(function() {

				$('.view_detail').click(function(){
					
					var id = $(this).attr('relid'); //get the attribute value
					
					$.ajax({
						url : "<?php echo base_url(); ?>account/ban_receipts/view_main",
						data:{id : id},
						method:'GET',
						dataType:'json',
						success:function(response) {
							$('#details').html(response.details); //hold the response in id and show on popup
							$('#thedate').html(response.thedate);
							$('#bill_id').html(response.bill_id);
							$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
							records = JSON.parse(data);
						}
					});


					$.ajax({
						url: '<?php echo base_url(); ?>account/ban_receipts/view_details',
						data:{id : id},
						type: 'get',
						dataType: 'JSON',
						success: function(response){
							var len = response.length;
							$("#ModalTable").find("tbody").empty();
							for(var i=0; i<len; i++){
								var id = response[i].id;
								var title = response[i].title;
								var title_en = response[i].title_en;
								var debit = response[i].debit;
								var creditor = response[i].creditor;
								<?php
								if ($this->session->userdata('lang') == "ar")
								{
								?>
								var tr_str = "<tr>" +
									"<td align='right'>" + title + "</td>" +
									"<td align='center'>" + debit + "</td>" +
									"<td align='center'>" + creditor + "</td>" +
									"</tr>";
								<?php
								}
								else
								{
								?>
								var tr_str = "<tr>" +
									"<td align='right'>" + title_en + "</td>" +
									"<td align='center'>" + debit + "</td>" +
									"<td align='center'>" + creditor + "</td>" +
									"</tr>";
								<?php
								}
								?>
								$("#ModalTable tbody").append(tr_str);
							}

						}
					});

				});

				
			});

		</script>

		<script type="text/javascript">
		function printSelection(node){
			var content=node.innerHTML;
			var pwin=window.open('','print_content','width=600,height=600');

			pwin.document.open();
			pwin.document.write('<html><head><title></title>');
			pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
			pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
			pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
			pwin.document.write('</head>');
			pwin.document.write('<body onload="window.print()" style="direction: rtl; text-align: right">');
			pwin.document.write(content);
			pwin.document.write('<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></' + 'script>');
			pwin.document.write('<script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></' + 'script>');
			pwin.document.write('</body></html>');
			pwin.document.close();
		
			pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
			pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
			pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

			setTimeout(function(){pwin.close();},1000);

		}
		</script>
<?php
}
?>
<?php
if(($Segment2 == "fin_journal") && ($Segment3 == "insertform") || ($Segment2 == "mon_exchange_bonds") && ($Segment3 == "insertform") 
|| ($Segment2 == "mon_receipts") && ($Segment3 == "insertform") || ($Segment2 == "ban_exchange_bonds") && ($Segment3 == "insertform") 
|| ($Segment2 == "ban_receipts") && ($Segment3 == "insertform") || ($Segment2 == "sale_bill") && ($Segment3 == "pay"))
{
?>
<script type="text/javascript">
			$(document).ready(function()
			{
				KTFormControls.init();
			});

			var KTFormControls = function () {

			var demo2 = function () {
				$( "#kt_form_2" ).validate({
					// define validation rules
					rules: {
						//= Client Information(step 3)
						// Billing Information
						billing_card_name: {
							required: true
						},
						billing_card_number: {
							required: true,
							creditcard: true
						},
						billing_card_exp_month: {
							required: true
						},
						billing_card_exp_year: {
							required: true
						},
						billing_card_cvv: {
							required: true,
							minlength: 2,
							maxlength: 3
						},

						// Billing Address
						billing_address_1: {
							required: true
						},
						billing_address_2: {

						},
						billing_city: {
							required: true
						},
						billing_state: {
							required: true
						},
						billing_zip: {
							required: true,
							number: true
						},

						billing_delivery: {
							required: true
						}
					},

					//display error alert on form submit
					invalidHandler: function(event, validator) {
						swal.fire({
							"title": "",
							"text": "<?php echo lang('ErrorDetails');?>",
							"type": "error",
							"confirmButtonClass": "btn btn-secondary",
							"onClose": function(e) {
								console.log('on close event fired!');
							}
						});

						event.preventDefault();
					},

					submitHandler: function (form) {
						form[0].submit(); // submit the form
						swal.fire({
							"title": "",
							"text": "<?php echo lang('GoodJobDetails');?>",
							"type": "success",
							"confirmButtonClass": "btn btn-secondary"
						});

						return false;
					}
				});
			}

			return {
				// public functions
				init: function() {
					demo2();
				}
			};
		}();
		</script>

		<script type="text/javascript">
			function TotalDebitValue()
			{
				acc_invoice_items = new Number(document.getElementById("acc_invoice_items").value);
				DebitTotal = 0;
				FieldName = "debit";
				for (i = 1; i <= acc_invoice_items; i++) { 
					FieldName = "debit" + i;
					DebitTotal += new Number(document.getElementById(FieldName).value);
				}
				document.kt_form_2.debit_total.value = DebitTotal.toFixed(2);
			}

			function TotalCreditorValue()
			{
				acc_invoice_items = new Number(document.getElementById("acc_invoice_items").value);
				DebitTotal = 0;
				FieldName = "creditor";
				for (i = 1; i <= acc_invoice_items; i++) { 
					FieldName = "creditor" + i;
					DebitTotal += new Number(document.getElementById(FieldName).value);
				}
				document.kt_form_2.creditor_total.value = DebitTotal.toFixed(2);
			}

			function Check_Debit_Creditor()
			{
				TotalDebit = new Number(document.getElementById("debit_total").value);
				TotalCreditor = new Number(document.getElementById("creditor_total").value);
				details = document.getElementById("details").value;
				if (Number(document.getElementById('debit_total').value) == Number(document.getElementById('creditor_total').value) && 
				(Number(document.getElementById('debit_total').value) > 0) && (Number(document.getElementById('creditor_total').value) > 0)) 
				{
					//document.kt_form_2.submit();
				}
				else
				{
					alert("<?php echo lang('fin_journal_Alert');?>");
					return false; 
				}
			}
		</script>
<?php
}
if(($Segment1 == "purchase") && ($Segment2 == "buy_bill") && ($Segment3 == "") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "purchase_1_search") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "purchase_1") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "purchase_5_search") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "purchase_5"))
{
?>
<script type="text/javascript">
	$(document).ready(function() {

		$('.view_detail').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value
			
			$.ajax({
				url : "<?php echo base_url(); ?>purchase/buy_bill/view_main",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					<?php
					if ($this->session->userdata('lang') == "ar")
					{
					?>
					$('#id').html(response.id);
					$('#thedate').html(response.thedate);
					$('#supplier').html(response.supplier_title);
					$('#totalvalue').html(response.totalvalue);
					$('#paid').html(response.paid);
					$('#remaining').html(response.remaining);
					$('#discount').html(response.discount);
					$('#tax').html(response.tax);
					$('#account_title').html(response.account_title);
					$('#notes').html(response.notes);
					<?php
					}
					else
					{
					?>
					$('#id').html(response.id);
					$('#thedate').html(response.thedate);
					$('#supplier').html(response.supplier_title_en);
					$('#totalvalue').html(response.totalvalue);
					$('#paid').html(response.paid);
					$('#remaining').html(response.remaining);
					$('#discount').html(response.discount);
					$('#tax').html(response.tax);
					$('#account_title').html(response.account_title_en);
					$('#notes').html(response.notes);
					<?php
					}
					?>
					$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});


			$.ajax({
				url: '<?php echo base_url(); ?>purchase/buy_bill/view_details',
				data:{id : id},
				type: 'get',
				dataType: 'JSON',
				success: function(response){
					var len = response.length;
					$("#ModalTable").find("tbody").empty();
					for(var i=0; i<len; i++){
						var id = response[i].id;
						var title = response[i].title;
						var title_en = response[i].title_en;
						var quantity = response[i].quantity;
						var unitprice = response[i].unitprice;
						var subtotal = response[i].subtotal;
						<?php
						if ($this->session->userdata('lang') == "ar")
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + title + "</td>" +
							"<td align='center'>" + quantity + "</td>" +
							"<td align='center'>" + unitprice + "</td>" +
							"<td align='center'>" + subtotal + "</td>" +
							"</tr>";
						<?php
						}
						else
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + title_en + "</td>" +
							"<td align='center'>" + quantity + "</td>" +
							"<td align='center'>" + unitprice + "</td>" +
							"<td align='center'>" + subtotal + "</td>" +
							"</tr>";
						<?php
						}
						?>
						$("#ModalTable tbody").append(tr_str);
					}

				}
			});

		});


		$('.buy_bill_pay').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value
			
			$.ajax({
				url : "<?php echo base_url(); ?>purchase/buy_bill/view_main",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					document.buy_bill_pay.id.value = response.id;
					document.buy_bill_pay.totalvalue.value = response.totalvalue;
					document.buy_bill_pay.remaining.value = response.remaining;
					document.buy_bill_pay.paid.value = response.paid;
					document.buy_bill_pay.paid_value.value = response.remaining;
					$('#buy_bill_pay').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});

		});

		
	});

</script>

<script type="text/javascript">
function printSelection(node){
var content=node.innerHTML;
var pwin=window.open('','print_content','width=600,height=600');

pwin.document.open();
pwin.document.write('<html><head><title></title>');
pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
pwin.document.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">');
pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
pwin.document.write('</head>');
pwin.document.write('<body onload="window.print()" style="direction: <?php echo $this->session->userdata('Direction');?>; text-align: <?php echo $this->session->userdata('Alignment');?>">');
pwin.document.write(content);
pwin.document.write('<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></' + 'script>');
pwin.document.write('<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></' + 'script>');
pwin.document.write('<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></' + 'script>');
pwin.document.write('</body></html>');
pwin.document.close();

pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

setTimeout(function(){pwin.close();},1000);

}
</script>
<?php
}
if(($Segment1 == "purchase") && ($Segment2 == "buy_bill") && ($Segment3 == "insertform"))
{
?>
<script type="text/javascript">
	function GetName(RowSerial) {
		barcode = "barcode" + RowSerial;
		barcode = document.getElementById(barcode).value;
		theUrl = decodeURIComponent("<?php echo base_url();?>purchase/buy_bill/FillProductsList/" + barcode);
		//window.location.href = theUrl;
		$.ajax({url: theUrl, success: function(result){
			//$("#div1").html(result);
			title = "title" + RowSerial;
			document.getElementById(title).value = result;
		}});
	}

	function SubValue() {
		var RowsCount = document.getElementById("RowsCount").value;
		quantity = "quantity";
		unitprice = "unitprice";
		subtotal = "subtotal";
		for (i = 1; i <= RowsCount; i++) { 
			Quantity = "quantity" + i;
			Unitprice = "unitprice" + i;
			Q = new Number(document.getElementById(Quantity).value);
			U = new Number(document.getElementById(Unitprice).value);
			S = new Number(Q * U);
			
			SubtotalField = "subtotal" + i;
			FinalValue = S;

			document.getElementById(SubtotalField).value = FinalValue.toFixed(2);
		}
	}

	function Remaing() {
		var RowsCount = document.getElementById("RowsCount").value;
		InvoiceTotal = 0;
		for (i = 1; i <= RowsCount; i++) { 
			FieldName = "subtotal" + i;
			SubTotal = new Number(document.getElementById(FieldName).value);
			InvoiceTotal += new Number(SubTotal);
		}
		tax = new Number(document.getElementById("tax").value);
		if(tax > 1)
		{
			tax = tax / 100;
		}
		TaxAmount = InvoiceTotal * tax;
		InvoiceTotalWithTax = InvoiceTotal + TaxAmount;
		//InvoiceTotal = new Number(document.SendData.totalvalue.value);
		
		Paid = new Number(document.SendData.paid.value);
		Discount = new Number(document.SendData.discount.value);
		
		RemainingValue = (InvoiceTotalWithTax - Paid - Discount) ;
		InvoiceTotal = (InvoiceTotalWithTax - Discount) ;
		document.SendData.remaining.value = RemainingValue.toFixed(2);
		document.SendData.totalvalue.value = InvoiceTotalWithTax.toFixed(2);
	}

	function AllTotal() {
		var RowsCount = document.getElementById("RowsCount").value;
		InvoiceTotal = 0;
		for (i = 1; i <= RowsCount; i++) { 
			FieldName = "subtotal" + i;
			SubTotal = new Number(document.getElementById(FieldName).value);
			InvoiceTotal += new Number(SubTotal);
		}
		paid = new Number(document.getElementById("paid").value);
		discount = new Number(document.getElementById("discount").value);
		tax = new Number(document.getElementById("tax").value);
		if(tax > 1)
		{
			tax = tax / 100;
		}
		TaxAmount = InvoiceTotal * tax;
		TotalValueBeforeDiscount = InvoiceTotal - discount;
		
		InvoiceTotal = TotalValueBeforeDiscount + TaxAmount;
		
		document.SendData.totalvalue.value = InvoiceTotal.toFixed(2);
	}
</script>
<?php
}
if(($Segment1 == "purchase") && ($Segment2 == "buy_returns") && ($Segment3 == "") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "purchase_4") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "purchase_4_search"))
{
?>
<script type="text/javascript">
	$(document).ready(function() {

		$('.view_detail').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value
			
			$.ajax({
				url : "<?php echo base_url(); ?>purchase/buy_returns/view_main",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					<?php
					if ($this->session->userdata('lang') == "ar")
					{
					?>
					$('#id').html(response.id);
					$('#thedate').html(response.thedate);
					$('#supplier').html(response.supplier_title);
					$('#totalvalue').html(response.totalvalue);
					$('#paid').html(response.paid);
					$('#remaining').html(response.remaining);
					$('#discount').html(response.discount);
					$('#account_title').html(response.account_title);
					$('#notes').html(response.notes);
					<?php
					}
					else
					{
					?>
					$('#id').html(response.id);
					$('#thedate').html(response.thedate);
					$('#supplier').html(response.supplier_title_en);
					$('#totalvalue').html(response.totalvalue);
					$('#paid').html(response.paid);
					$('#remaining').html(response.remaining);
					$('#discount').html(response.discount);
					$('#account_title').html(response.account_title_en);
					$('#notes').html(response.notes);
					<?php
					}
					?>
					$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});


			$.ajax({
				url: '<?php echo base_url(); ?>purchase/buy_returns/view_details',
				data:{id : id},
				type: 'get',
				dataType: 'JSON',
				success: function(response){
					var len = response.length;
					$("#ModalTable").find("tbody").empty();
					for(var i=0; i<len; i++){
						var id = response[i].id;
						var title = response[i].title;
						var title_en = response[i].title_en;
						var quantity = response[i].quantity;
						var unitprice = response[i].unitprice;
						var subtotal = response[i].subtotal;
						<?php
						if ($this->session->userdata('lang') == "ar")
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + title + "</td>" +
							"<td align='center'>" + quantity + "</td>" +
							"<td align='center'>" + unitprice + "</td>" +
							"<td align='center'>" + subtotal + "</td>" +
							"</tr>";
						<?php
						}
						else
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + title_en + "</td>" +
							"<td align='center'>" + quantity + "</td>" +
							"<td align='center'>" + unitprice + "</td>" +
							"<td align='center'>" + subtotal + "</td>" +
							"</tr>";
						<?php
						}
						?>
						$("#ModalTable tbody").append(tr_str);
					}

				}
			});

		});

		$('.buy_returns_pay').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value
			
			$.ajax({
				url : "<?php echo base_url(); ?>purchase/buy_returns/view_main",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					document.buy_returns_pay.id.value = response.id;
					document.buy_returns_pay.totalvalue.value = response.totalvalue;
					document.buy_returns_pay.remaining.value = response.remaining;
					document.buy_returns_pay.paid.value = response.paid;
					document.buy_returns_pay.paid_value.value = response.remaining;
					$('#buy_returns_pay').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});

		});
		
	});

		</script>

		<script type="text/javascript">
		function printSelection(node){
			var content=node.innerHTML;
			var pwin=window.open('','print_content','width=600,height=600');

			pwin.document.open();
			pwin.document.write('<html><head><title></title>');
			pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
			pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
			pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
			pwin.document.write('</head>');
			pwin.document.write('<body onload="window.print()" style="direction: <?php echo $this->session->userdata('Direction');?>; text-align: <?php echo $this->session->userdata('Alignment');?>">');
			pwin.document.write(content);
			pwin.document.write('<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('</body></html>');
			pwin.document.close();
		
			pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
			pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
			pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

			setTimeout(function(){pwin.close();},1000);

		}
		</script>
<?php
}
if(($Segment1 == "purchase") && ($Segment2 == "buy_returns") && ($Segment3 == "insertform"))
{
?>
<script type="text/javascript">
			function GetName(RowSerial) {
				barcode = "barcode" + RowSerial;
    			barcode = document.getElementById(barcode).value;
				theUrl = decodeURIComponent("<?php echo base_url();?>purchase/buy_returns/FillProductsList/" + barcode);
				//window.location.href = theUrl;
				$.ajax({url: theUrl, success: function(result){
					//$("#div1").html(result);
					title = "title" + RowSerial;
					document.getElementById(title).value = result;
				}});
			}

			function SubValue() {
				var RowsCount = document.getElementById("RowsCount").value;
				quantity = "quantity";
				unitprice = "unitprice";
				subtotal = "subtotal";
				for (i = 1; i <= RowsCount; i++) { 
					Quantity = "quantity" + i;
					Unitprice = "unitprice" + i;
					Q = new Number(document.getElementById(Quantity).value);
					U = new Number(document.getElementById(Unitprice).value);
					S = new Number(Q * U);
					
					SubtotalField = "subtotal" + i;
					FinalValue = S;

					document.getElementById(SubtotalField).value = FinalValue.toFixed(2);
				}
			}

			function Remaing() {
				var RowsCount = document.getElementById("RowsCount").value;
				InvoiceTotal = 0;
				for (i = 1; i <= RowsCount; i++) { 
					FieldName = "subtotal" + i;
					SubTotal = new Number(document.getElementById(FieldName).value);
					InvoiceTotal += new Number(SubTotal);
				}
				//InvoiceTotal = new Number(document.SendData.totalvalue.value);
				
				Paid = new Number(document.SendData.paid.value);
				Discount = new Number(document.SendData.discount.value);
				
				RemainingValue = (InvoiceTotal - Paid - Discount) ;
				InvoiceTotal = (InvoiceTotal - Discount) ;
				document.SendData.remaining.value = RemainingValue.toFixed(2);
				document.SendData.totalvalue.value = InvoiceTotal.toFixed(2);
			}

			function AllTotal() {
				var RowsCount = document.getElementById("RowsCount").value;
				InvoiceTotal = 0;
				for (i = 1; i <= RowsCount; i++) { 
					FieldName = "subtotal" + i;
					SubTotal = new Number(document.getElementById(FieldName).value);
					InvoiceTotal += new Number(SubTotal);
				}
				paid = new Number(document.getElementById("paid").value);
				discount = new Number(document.getElementById("discount").value);
				InvoiceTotal -= discount;
				
				document.SendData.totalvalue.value = InvoiceTotal.toFixed(2);
			}
		</script>
<?php
}
if(($Segment1 == "purchase") && ($Segment2 == "buy_offer") && ($Segment3 == ""))
{
?>
<script type="text/javascript">
			$(document).ready(function() {

				$('.view_detail').click(function(){
					
					var id = $(this).attr('relid'); //get the attribute value

					$.ajax({
						url : "<?php echo base_url(); ?>purchase/buy_offer/view_main",
						data:{id : id},
						method:'GET',
						dataType:'json',
						success:function(response) {
							<?php
							if ($this->session->userdata('lang') == "ar")
							{
							?>
							$('#id').html(response.id);
							$('#thedate').html(response.thedate);
							$('#supplier').html(response.supplier_title);
							$('#totalvalue').html(response.totalvalue);
							$('#paid').html(response.paid);
							$('#remaining').html(response.remaining);
							$('#discount').html(response.discount);
							$('#over_cost').html(response.over_cost);
							$('#tax').html(response.tax);
							$('#account_title').html(response.account_title);
							$('#notes').html(response.notes);
							<?php
							}
							else
							{
							?>
							$('#id').html(response.id);
							$('#thedate').html(response.thedate);
							$('#supplier').html(response.supplier_title_en);
							$('#totalvalue').html(response.totalvalue);
							$('#paid').html(response.paid);
							$('#remaining').html(response.remaining);
							$('#discount').html(response.discount);
							$('#over_cost').html(response.over_cost);
							$('#tax').html(response.tax);
							$('#account_title').html(response.account_title_en);
							$('#notes').html(response.notes);
							<?php
							}
							?>
							$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
							records = JSON.parse(data);
						}
					});


					$.ajax({
						url: '<?php echo base_url(); ?>purchase/buy_offer/view_details',
						data:{id : id},
						type: 'get',
						dataType: 'JSON',
						success: function(response){
							var len = response.length;
							$("#ModalTable").find("tbody").empty();
							for(var i=0; i<len; i++){
								var id = response[i].id;
								var title = response[i].title;
								var title_en = response[i].title_en;
								var quantity = response[i].quantity;
								var unitprice = response[i].unitprice;
								var subtotal = response[i].subtotal;
								<?php
								if ($this->session->userdata('lang') == "ar")
								{
								?>
								var tr_str = "<tr>" +
									"<td align='right'>" + title + "</td>" +
									"<td align='center'>" + quantity + "</td>" +
									"<td align='center'>" + unitprice + "</td>" +
									"<td align='center'>" + subtotal + "</td>" +
									"</tr>";
								<?php
								}
								else
								{
								?>
								var tr_str = "<tr>" +
									"<td align='right'>" + title_en + "</td>" +
									"<td align='center'>" + quantity + "</td>" +
									"<td align='center'>" + unitprice + "</td>" +
									"<td align='center'>" + subtotal + "</td>" +
									"</tr>";
								<?php
								}
								?>
								$("#ModalTable tbody").append(tr_str);
							}

						}
					});

				});

				
			});

		</script>

		<script type="text/javascript">
		function printSelection(node){
			var content=node.innerHTML;
			var pwin=window.open('','print_content','width=600,height=600');

			pwin.document.open();
			pwin.document.write('<html><head><title></title>');
			pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
			pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
			pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
			pwin.document.write('</head>');
			pwin.document.write('<body onload="window.print()" style="direction: <?php echo $this->session->userdata('Direction');?>; text-align: <?php echo $this->session->userdata('Alignment');?>">');
			pwin.document.write(content);
			pwin.document.write('<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('</body></html>');
			pwin.document.close();
		
			pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
			pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
			pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

			setTimeout(function(){pwin.close();},1000);

		}
		</script>
<?php
}
if(($Segment1 == "purchase") && ($Segment2 == "buy_offer") && ($Segment3 == "insertform") || 
($Segment1 == "purchase") && ($Segment2 == "buy_offer") && ($Segment3 == "updateform") || 
($Segment1 == "manufacturing") && ($Segment2 == "manu_components") && ($Segment3 == "insertform"))
{
?>
<script type="text/javascript">
	function GetName(RowSerial) {
		barcode = "barcode" + RowSerial;
		barcode = document.getElementById(barcode).value;
		theUrl = decodeURIComponent("<?php echo base_url();?>purchase/buy_offer/FillProductsList/" + barcode);
		//window.location.href = theUrl;
		$.ajax({url: theUrl, success: function(result){
			//$("#div1").html(result);
			title = "title" + RowSerial;
			document.getElementById(title).value = result;
		}});
	}

	function SubValue() {
		var RowsCount = document.getElementById("RowsCount").value;
		quantity = "quantity";
		unitprice = "unitprice";
		subtotal = "subtotal";
		for (i = 1; i <= RowsCount; i++) { 
			Quantity = "quantity" + i;
			Unitprice = "unitprice" + i;
			Q = new Number(document.getElementById(Quantity).value);
			U = new Number(document.getElementById(Unitprice).value);
			S = new Number(Q * U);
			
			SubtotalField = "subtotal" + i;
			FinalValue = S;

			document.getElementById(SubtotalField).value = FinalValue.toFixed(2);
		}
	}

	function Remaing() {
		var RowsCount = document.getElementById("RowsCount").value;
		InvoiceTotal = 0;
		for (i = 1; i <= RowsCount; i++) { 
			FieldName = "subtotal" + i;
			SubTotal = new Number(document.getElementById(FieldName).value);
			InvoiceTotal += new Number(SubTotal);
		}
		//InvoiceTotal = new Number(document.SendData.totalvalue.value);
		
		Paid = new Number(document.SendData.paid.value);
		Discount = new Number(document.SendData.discount.value);

		RemainingValue = (InvoiceTotal - Paid - Discount) ;
		InvoiceTotal = (InvoiceTotal - Discount) ;
		document.SendData.remaining.value = RemainingValue.toFixed(2);
		document.SendData.totalvalue.value = InvoiceTotal.toFixed(2);
	}

	function AllTotal() {
		var RowsCount = document.getElementById("RowsCount").value;
		var TotalValueBeforeDiscount = 0;
		var TaxAmount = 0;
		var FinalTotalValue = 0;
		InvoiceTotal = 0;
		for (i = 1; i <= RowsCount; i++) { 
			FieldName = "subtotal" + i;
			SubTotal = new Number(document.getElementById(FieldName).value);
			InvoiceTotal += new Number(SubTotal);
		}

		paid = new Number(document.getElementById("paid").value);
		discount = new Number(document.getElementById("discount").value);
		over_cost = new Number(document.getElementById("over_cost").value);
		tax = new Number(document.getElementById("tax").value);
		if(tax > 1)
		{
			tax = tax / 100;
		}

		TotalValueBeforeDiscount = InvoiceTotal + over_cost;
		TaxAmount = TotalValueBeforeDiscount * tax;
		FinalTotalValue = TotalValueBeforeDiscount + TaxAmount - discount;
		
		document.SendData.totalvalue.value = FinalTotalValue.toFixed(2);
	}
</script>
<?php
}
if(($Segment1 == "sales") && ($Segment2 == "sale_bill") && ($Segment3 == "") || 
($Segment2 == "sales") && ($Segment3 == "sale_bill") && ($Segment4 == "") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "sales_1") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "sales_1_search") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "sales_5") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "sales_5_search") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "sales_6") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "sales_6_search"))
{
?>
<!--end::Page Scripts -->
<script type="text/javascript">
	$(document).ready(function() {

		$('.view_detail').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value
			
			$.ajax({
				url : "<?php echo base_url(); ?>sales/sale_bill/view_main",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					<?php
					if ($this->session->userdata('lang') == "ar")
					{
					?>
					$('#id').html(response.id);
					$('#thedate').html(response.thedate);
					$('#customer').html(response.customer_title);
					$('#totalvalue').html(response.totalvalue);
					$('#paid').html(response.paid);
					$('#remaining').html(response.remaining);
					$('#discount').html(response.discount);
					$('#over_cost').html(response.over_cost);
					$('#tax').html(response.tax);
					$('#account_title').html(response.account_title);
					$('#notes').html(response.notes);
					<?php
					}
					else
					{
					?>
					$('#id').html(response.id);
					$('#thedate').html(response.thedate);
					$('#customer').html(response.customer_title_en);
					$('#totalvalue').html(response.totalvalue);
					$('#paid').html(response.paid);
					$('#remaining').html(response.remaining);
					$('#discount').html(response.discount);
					$('#over_cost').html(response.over_cost);
					$('#tax').html(response.tax);
					$('#account_title').html(response.account_title_en);
					$('#notes').html(response.notes);
					<?php
					}
					?>
					$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});


			$.ajax({
				url: '<?php echo base_url(); ?>sales/sale_bill/view_details',
				data:{id : id},
				type: 'get',
				dataType: 'JSON',
				success: function(response){
					var len = response.length;
					$("#ModalTable").find("tbody").empty();
					for(var i=0; i<len; i++){
						var id = response[i].id;
						var title = response[i].title;
						var title_en = response[i].title_en;
						var quantity = response[i].quantity;
						var unitprice = response[i].unitprice;
						var subtotal = response[i].subtotal;
						<?php
						if ($this->session->userdata('lang') == "ar")
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + title + "</td>" +
							"<td align='center'>" + quantity + "</td>" +
							"<td align='center'>" + unitprice + "</td>" +
							"<td align='center'>" + subtotal + "</td>" +
							"</tr>";
						<?php
						}
						else
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + title_en + "</td>" +
							"<td align='center'>" + quantity + "</td>" +
							"<td align='center'>" + unitprice + "</td>" +
							"<td align='center'>" + subtotal + "</td>" +
							"</tr>";
						<?php
						}
						?>
						$("#ModalTable tbody").append(tr_str);
					}

				}
			});

		});

		$('.sale_bill_pay').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value
			
			$.ajax({
				url : "<?php echo base_url(); ?>sales/sale_bill/view_main",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					document.sale_bill_pay.id.value = response.id;
					document.sale_bill_pay.totalvalue.value = response.totalvalue;
					document.sale_bill_pay.remaining.value = response.remaining;
					document.sale_bill_pay.paid.value = response.paid;
					document.sale_bill_pay.paid_value.value = response.remaining;
					$('#sale_bill_pay').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});

		});
	
	});

</script>

		<script type="text/javascript">
		function printSelection(node){
			var content=node.innerHTML;
			var pwin=window.open('','print_content','width=600,height=600');

			pwin.document.open();
			pwin.document.write('<html><head><title></title>');
			pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
			pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
			pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
			pwin.document.write('</head>');
			pwin.document.write('<body onload="window.print()" style="direction: <?php echo $this->session->userdata('Direction');?>; text-align: <?php echo $this->session->userdata('Alignment');?>">');
			pwin.document.write(content);
			pwin.document.write('<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('</body></html>');
			pwin.document.close();
		
			pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
			pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
			pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

			setTimeout(function(){pwin.close();},1000);

		}
		</script>
<?php
}
if(($Segment1 == "sales") && ($Segment2 == "sale_bill") && ($Segment3 == "insertform"))
{
?>
<script type="text/javascript">
			function GetName(RowSerial) {
				barcode = "barcode" + RowSerial;
    			barcode = document.getElementById(barcode).value;
				theUrl = decodeURIComponent("<?php echo base_url();?>sales/sale_bill/FillProductsList/" + barcode);
				//window.location.href = theUrl;
				$.ajax({url: theUrl, success: function(result){
					//$("#div1").html(result);
					title = "title" + RowSerial;
					document.getElementById(title).value = result;
				}});
			}

			function GetSalePrice(RowSerial) {
				barcode = "barcode" + RowSerial;
    			barcode = document.getElementById(barcode).value;
				theUrl = decodeURIComponent("<?php echo base_url();?>sales/sale_bill/FillProductsPrice/" + barcode);
				//window.location.href = theUrl;
				$.ajax({url: theUrl, success: function(result){
					//$("#div1").html(result);
					unitprice = "unitprice" + RowSerial;
					document.getElementById(unitprice).value = result;
				}});
			}

			function GetSaleMinPrice(RowSerial) {
				barcode = "barcode" + RowSerial;
    			barcode = document.getElementById(barcode).value;
				theUrl = decodeURIComponent("<?php echo base_url();?>sales/sale_bill/FillProductsMinPrice/" + barcode);
				//window.location.href = theUrl;
				$.ajax({url: theUrl, success: function(result){
					//$("#div1").html(result);
					unitprice = "unitprice" + RowSerial;
					document.getElementById(unitprice).min = result;
				}});
			}

			function SubValue() {
				var RowsCount = document.getElementById("RowsCount").value;
								quantity = "quantity";
				unitprice = "unitprice";
				subtotal = "subtotal";
				received = "received";
				for (i = 1; i <= RowsCount; i++) { 
					Quantity = "quantity" + i;
					Unitprice = "unitprice" + i;
					received = "received" + i;
					Q = new Number(document.getElementById(Quantity).value);
					Rec = new Number(document.getElementById(Quantity).value);
					U = new Number(document.getElementById(Unitprice).value);
					S = new Number(Q * U);
					
					SubtotalField = "subtotal" + i;
					FinalValue = S;

					document.getElementById(received).value = Rec;
					document.getElementById(received).max = Rec;
					document.getElementById(SubtotalField).value = FinalValue.toFixed(2);
				}
			}

			function Remaing() {
				var RowsCount = document.getElementById("RowsCount").value;
				InvoiceTotal = 0;
				for (i = 1; i <= RowsCount; i++) { 
					FieldName = "subtotal" + i;
					SubTotal = new Number(document.getElementById(FieldName).value);
					InvoiceTotal += new Number(SubTotal);
				}
				tax = new Number(document.getElementById("tax").value);
				if(tax > 1)
				{
					tax = tax / 100;
				}
				TaxAmount = InvoiceTotal * tax;
				// InvoiceTotalWithTax = InvoiceTotal + TaxAmount; by malikatiq
				//InvoiceTotal = new Number(document.SendData.totalvalue.value);
				paid1 = new Number(document.getElementById("paid1").value);
				paid2 = new Number(document.getElementById("paid2").value);
				paid3 = new Number(document.getElementById("paid3").value);
				TotalPaid = paid1 + paid2 + paid3;
				document.SendData.paid.value = TotalPaid.toFixed(2);

				Paid = new Number(document.SendData.paid.value);
				Discount = new Number(document.SendData.discount.value);
				
				// RemainingValue = (InvoiceTotalWithTax - Paid - Discount) ;
				// InvoiceTotal = (InvoiceTotalWithTax - Discount) ;


				// added by malikatiq
				RemainingValue = (InvoiceTotal - Paid - Discount) ;
				
				if(discount >1 || tax > 1)
				{
					InvoiceTotal = (paid - discount) ;
					var totalWithTaxAmount = InvoiceTotal * tax;
					InvoiceTotal = totalWithTaxAmount+InvoiceTotal;
				}
				


				document.SendData.remaining.value = RemainingValue.toFixed(2);
				document.SendData.totalvalue.value = InvoiceTotal.toFixed(2);
			}

			function AllTotal() {
				var RowsCount = document.getElementById("RowsCount").value;
				InvoiceTotal = 0;
				for (i = 1; i <= RowsCount; i++) { 
					FieldName = "subtotal" + i;
					SubTotal = new Number(document.getElementById(FieldName).value);
					InvoiceTotal += new Number(SubTotal);
				}
				paid = new Number(document.getElementById("paid").value);
				discount = new Number(document.getElementById("discount").value);
				tax = new Number(document.getElementById("tax").value);
				if(tax > 1)
				{
					tax = tax / 100;
				}

				// TaxAmount = InvoiceTotal * tax;
				// TotalValueBeforeDiscount = InvoiceTotal - discount;
				
				// InvoiceTotal = TotalValueBeforeDiscount + TaxAmount; by malikatiq

				// added by malikatiq
				RemainingValue = (InvoiceTotal - paid - discount) ;

				if(discount >1 || tax > 1)
				{
					InvoiceTotal = (paid - discount) ;
					var totalWithTaxAmount = InvoiceTotal * tax;
					InvoiceTotal = totalWithTaxAmount+InvoiceTotal;
				}
				
				
				document.SendData.totalvalue.value = InvoiceTotal.toFixed(2);
			}

			function GetStores(RowSerial) {
				location_id = "location_id" + RowSerial;
				location_id = document.getElementById(location_id).value;
				barcode = "barcode" + RowSerial;
				barcode = document.getElementById(barcode).value;
				available = "available" + RowSerial;
				available = document.getElementById(available).value;

				theUrl = decodeURIComponent("<?php echo base_url();?>sales/sale_bill/LoadStores/" + location_id + "/" + barcode);
				//window.location.href = theUrl;
				$.ajax({url: theUrl, success: function(result){
					//$("#div1").html(result);
					available = "available" + RowSerial;
					document.getElementById(available).value = result;
				}});
			}

			function GetCustomerBalance() {
				customer_id = "customer_id";
				customer_id = document.getElementById(customer_id).value;

				theUrl = decodeURIComponent("<?php echo base_url();?>sales/sale_bill/GetCustomerBalance/" + customer_id);
				//window.location.href = theUrl;
				$.ajax({url: theUrl, success: function(result){
					//$("#div1").html(result);
					customer_money = "customer_money";
					paid3 = "paid3";
					document.getElementById(customer_money).value = result;
					document.getElementById(paid3).max = result;
				}});
			}
		</script>
<?php
}
if(($Segment1 == "sales") && ($Segment2 == "sale_offer") && ($Segment3 == ""))
{
?>
<script type="text/javascript">
			$(document).ready(function() {

				$('.view_detail').click(function(){
					
					var id = $(this).attr('relid'); //get the attribute value

					$.ajax({
						url : "<?php echo base_url(); ?>sales/sale_offer/view_main",
						data:{id : id},
						method:'GET',
						dataType:'json',
						success:function(response) {
							<?php
							if ($this->session->userdata('lang') == "ar")
							{
							?>
							$('#id').html(response.id);
							$('#thedate').html(response.thedate);
							$('#supplier').html(response.supplier_title);
							$('#totalvalue').html(response.totalvalue);
							$('#paid').html(response.paid);
							$('#remaining').html(response.remaining);
							$('#discount').html(response.discount);
							$('#over_cost').html(response.over_cost);
							$('#tax').html(response.tax);
							$('#account_title').html(response.account_title);
							$('#notes').html(response.notes);
							<?php
							}
							else
							{
							?>
							$('#id').html(response.id);
							$('#thedate').html(response.thedate);
							$('#supplier').html(response.supplier_title_en);
							$('#totalvalue').html(response.totalvalue);
							$('#paid').html(response.paid);
							$('#remaining').html(response.remaining);
							$('#discount').html(response.discount);
							$('#over_cost').html(response.over_cost);
							$('#tax').html(response.tax);
							$('#account_title').html(response.account_title_en);
							$('#notes').html(response.notes);
							<?php
							}
							?>
							$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
							records = JSON.parse(data);
						}
					});


					$.ajax({
						url: '<?php echo base_url(); ?>sales/sale_offer/view_details',
						data:{id : id},
						type: 'get',
						dataType: 'JSON',
						success: function(response){
							var len = response.length;
							$("#ModalTable").find("tbody").empty();
							for(var i=0; i<len; i++){
								var id = response[i].id;
								var title = response[i].title;
								var title_en = response[i].title_en;
								var quantity = response[i].quantity;
								var unitprice = response[i].unitprice;
								var subtotal = response[i].subtotal;
								<?php
								if ($this->session->userdata('lang') == "ar")
								{
								?>
								var tr_str = "<tr>" +
									"<td align='right'>" + title + "</td>" +
									"<td align='center'>" + quantity + "</td>" +
									"<td align='center'>" + unitprice + "</td>" +
									"<td align='center'>" + subtotal + "</td>" +
									"</tr>";
								<?php
								}
								else
								{
								?>
								var tr_str = "<tr>" +
									"<td align='right'>" + title_en + "</td>" +
									"<td align='center'>" + quantity + "</td>" +
									"<td align='center'>" + unitprice + "</td>" +
									"<td align='center'>" + subtotal + "</td>" +
									"</tr>";
								<?php
								}
								?>
								$("#ModalTable tbody").append(tr_str);
							}

						}
					});

				});

				
			});

		</script>

		<script type="text/javascript">
		function printSelection(node){
			var content=node.innerHTML;
			var pwin=window.open('','print_content','width=600,height=600');

			pwin.document.open();
			pwin.document.write('<html><head><title></title>');
			pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
			pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
			pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
			pwin.document.write('</head>');
			pwin.document.write('<body onload="window.print()" style="direction: <?php echo $this->session->userdata('Direction');?>; text-align: <?php echo $this->session->userdata('Alignment');?>">');
			pwin.document.write(content);
			pwin.document.write('<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('</body></html>');
			pwin.document.close();
		
			pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
			pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
			pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

			setTimeout(function(){pwin.close();},1000);

		}
		</script>
<?php
}
if(($Segment1 == "sales") && ($Segment2 == "sale_offer") && ($Segment3 == "insertform") || 
($Segment1 == "sales") && ($Segment2 == "sale_offer") && ($Segment3 == "updateform"))
{
?>
<script type="text/javascript">
	function GetName(RowSerial) {
		barcode = "barcode" + RowSerial;
		barcode = document.getElementById(barcode).value;
		theUrl = decodeURIComponent("<?php echo base_url();?>sales/sale_offer/FillProductsList/" + barcode);
		//window.location.href = theUrl;
		$.ajax({url: theUrl, success: function(result){
			//$("#div1").html(result);
			title = "title" + RowSerial;
			document.getElementById(title).value = result;
		}});
	}

	function SubValue() {
		var RowsCount = document.getElementById("RowsCount").value;
		quantity = "quantity";
		unitprice = "unitprice";
		subtotal = "subtotal";
		for (i = 1; i <= RowsCount; i++) { 
			Quantity = "quantity" + i;
			Unitprice = "unitprice" + i;
			Q = new Number(document.getElementById(Quantity).value);
			U = new Number(document.getElementById(Unitprice).value);
			S = new Number(Q * U);
			
			SubtotalField = "subtotal" + i;
			FinalValue = S;

			document.getElementById(SubtotalField).value = FinalValue.toFixed(2);
		}
	}

	function Remaing() {
		var RowsCount = document.getElementById("RowsCount").value;
		InvoiceTotal = 0;
		for (i = 1; i <= RowsCount; i++) { 
			FieldName = "subtotal" + i;
			SubTotal = new Number(document.getElementById(FieldName).value);
			InvoiceTotal += new Number(SubTotal);
		}
		//InvoiceTotal = new Number(document.SendData.totalvalue.value);
		
		Paid = new Number(document.SendData.paid.value);
		Discount = new Number(document.SendData.discount.value);

		RemainingValue = (InvoiceTotal - Paid - Discount) ;
		InvoiceTotal = (InvoiceTotal - Discount) ;
		document.SendData.remaining.value = RemainingValue.toFixed(2);
		document.SendData.totalvalue.value = InvoiceTotal.toFixed(2);
	}

	function AllTotal() {
		var RowsCount = document.getElementById("RowsCount").value;
		var TotalValueBeforeDiscount = 0;
		var TaxAmount = 0;
		var FinalTotalValue = 0;
		InvoiceTotal = 0;
		for (i = 1; i <= RowsCount; i++) { 
			FieldName = "subtotal" + i;
			SubTotal = new Number(document.getElementById(FieldName).value);
			InvoiceTotal += new Number(SubTotal);
		}

		paid = new Number(document.getElementById("paid").value);
		discount = new Number(document.getElementById("discount").value);
		over_cost = new Number(document.getElementById("over_cost").value);
		tax = new Number(document.getElementById("tax").value);
		if(tax > 1)
		{
			tax = tax / 100;
		}

		TotalValueBeforeDiscount = InvoiceTotal + over_cost;
		TaxAmount = TotalValueBeforeDiscount * tax;
		FinalTotalValue = TotalValueBeforeDiscount + TaxAmount - discount;
		
		document.SendData.totalvalue.value = FinalTotalValue.toFixed(2);
	}
</script>
<?php
}
if(($Segment1 == "sales") && ($Segment2 == "sale_returns") && ($Segment3 == "") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "sales_4") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "sales_4_search") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "sales_7") || 
($Segment1 == "account") && ($Segment2 == "reports") && ($Segment3 == "sales_7_search"))
{
?>
<!--end::Page Scripts -->
<script type="text/javascript">
	$(document).ready(function() {

		$('.view_detail').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value
			
			$.ajax({
				url : "<?php echo base_url(); ?>sales/sale_returns/view_main",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					<?php
					if ($this->session->userdata('lang') == "ar")
					{
					?>
					$('#id').html(response.id);
					$('#thedate').html(response.thedate);
					$('#customer').html(response.customer_title);
					$('#totalvalue').html(response.totalvalue);
					$('#paid').html(response.paid);
					$('#remaining').html(response.remaining);
					$('#discount').html(response.discount);
					$('#account_title').html(response.account_title);
					$('#notes').html(response.notes);
					<?php
					}
					else
					{
					?>
					$('#id').html(response.id);
					$('#thedate').html(response.thedate);
					$('#customer').html(response.customer_title_en);
					$('#totalvalue').html(response.totalvalue);
					$('#paid').html(response.paid);
					$('#remaining').html(response.remaining);
					$('#discount').html(response.discount);
					$('#account_title').html(response.account_title_en);
					$('#notes').html(response.notes);
					<?php
					}
					?>
					$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});


			$.ajax({
				url: '<?php echo base_url(); ?>sales/sale_returns/view_details',
				data:{id : id},
				type: 'get',
				dataType: 'JSON',
				success: function(response){
					var len = response.length;
					$("#ModalTable").find("tbody").empty();
					for(var i=0; i<len; i++){
						var id = response[i].id;
						var title = response[i].title;
						var title_en = response[i].title_en;
						var quantity = response[i].quantity;
						var unitprice = response[i].unitprice;
						var subtotal = response[i].subtotal;
						<?php
						if ($this->session->userdata('lang') == "ar")
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + title + "</td>" +
							"<td align='center'>" + quantity + "</td>" +
							"<td align='center'>" + unitprice + "</td>" +
							"<td align='center'>" + subtotal + "</td>" +
							"</tr>";
						<?php
						}
						else
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + title_en + "</td>" +
							"<td align='center'>" + quantity + "</td>" +
							"<td align='center'>" + unitprice + "</td>" +
							"<td align='center'>" + subtotal + "</td>" +
							"</tr>";
						<?php
						}
						?>
						$("#ModalTable tbody").append(tr_str);
					}

				}
			});

		});

		$('.sale_returns_pay').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value
			
			$.ajax({
				url : "<?php echo base_url(); ?>sales/sale_returns/view_main",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					document.sale_returns_pay.id.value = response.id;
					document.sale_returns_pay.totalvalue.value = response.totalvalue;
					document.sale_returns_pay.remaining.value = response.remaining;
					document.sale_returns_pay.paid.value = response.paid;
					document.sale_returns_pay.paid_value.value = response.remaining;
					$('#sale_returns_pay').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});

		});
		
	});

</script>

		<script type="text/javascript">
		function printSelection(node){
			var content=node.innerHTML;
			var pwin=window.open('','print_content','width=600,height=600');

			pwin.document.open();
			pwin.document.write('<html><head><title></title>');
			pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
			pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
			pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
			pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
			pwin.document.write('</head>');
			pwin.document.write('<body onload="window.print()" style="direction: <?php echo $this->session->userdata('Direction');?>; text-align: <?php echo $this->session->userdata('Alignment');?>">');
			pwin.document.write(content);
			pwin.document.write('<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></' + 'script>');
			pwin.document.write('</body></html>');
			pwin.document.close();
		
			pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
			pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
			pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

			setTimeout(function(){pwin.close();},1000);

		}
		</script>
<?php
}
if(($Segment1 == "sales") && ($Segment2 == "sale_returns") && ($Segment3 == "insertform"))
{
?>
<script type="text/javascript">
			function GetName(RowSerial) {
				barcode = "barcode" + RowSerial;
    			barcode = document.getElementById(barcode).value;
				theUrl = decodeURIComponent("<?php echo base_url();?>sales/sale_returns/FillProductsList/" + barcode);
				//window.location.href = theUrl;
				$.ajax({url: theUrl, success: function(result){
					//$("#div1").html(result);
					title = "title" + RowSerial;
					document.getElementById(title).value = result;
				}});
			}

			function SubValue() {
				var RowsCount = document.getElementById("RowsCount").value;
				quantity = "quantity";
				unitprice = "unitprice";
				subtotal = "subtotal";
				for (i = 1; i <= RowsCount; i++) { 
					Quantity = "quantity" + i;
					Unitprice = "unitprice" + i;
					Q = new Number(document.getElementById(Quantity).value);
					U = new Number(document.getElementById(Unitprice).value);
					S = new Number(Q * U);
					
					SubtotalField = "subtotal" + i;
					FinalValue = S;

					document.getElementById(SubtotalField).value = FinalValue.toFixed(2);
				}
			}

			function Remaing() {
				var RowsCount = document.getElementById("RowsCount").value;
				InvoiceTotal = 0;
				for (i = 1; i <= RowsCount; i++) { 
					FieldName = "subtotal" + i;
					SubTotal = new Number(document.getElementById(FieldName).value);
					InvoiceTotal += new Number(SubTotal);
				}
				//InvoiceTotal = new Number(document.SendData.totalvalue.value);
				
				Paid = new Number(document.SendData.paid.value);
				Discount = new Number(document.SendData.discount.value);
				
				RemainingValue = (InvoiceTotal - Paid - Discount) ;
				InvoiceTotal = (InvoiceTotal - Discount) ;
				document.SendData.remaining.value = RemainingValue.toFixed(2);
				document.SendData.totalvalue.value = InvoiceTotal.toFixed(2);
			}

			function AllTotal() {
				var RowsCount = document.getElementById("RowsCount").value;
				InvoiceTotal = 0;
				for (i = 1; i <= RowsCount; i++) { 
					FieldName = "subtotal" + i;
					SubTotal = new Number(document.getElementById(FieldName).value);
					InvoiceTotal += new Number(SubTotal);
				}
				paid = new Number(document.getElementById("paid").value);
				discount = new Number(document.getElementById("discount").value);
				InvoiceTotal -= discount;
				
				document.SendData.totalvalue.value = InvoiceTotal.toFixed(2);
			}
		</script>
<?php
}
if(($Segment1 == "project") && ($Segment3 == "insertform"))
{
?>
<script type="text/javascript">
	function calc() {
		var labor_cost = document.getElementById("labor_cost").value;
		var labor_cost = parseInt(labor_cost, 10);
		var material_cost = document.getElementById("material_cost").value;
		var material_cost = parseInt(material_cost, 10);
		var total = labor_cost + material_cost;
		document.getElementById("total_cost").value = total;
	}
</script>
<?php
}
if(($Segment1 == "project") && ($Segment2 == "proj_project_files") && ($Segment3 == "insertform"))
{
?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		var $project_id = $( '#project_id' ),
			$level_id = $( '#level_id' ),
		$options = $level_id.find( 'option' );

		$project_id.on( 'change', function() {
			$level_id.html( $options.filter( '[title="' + this.value + '"]' ) );
		} ).trigger( 'change' );
	});	

	jQuery(document).ready(function() {
		var $level_id = $( '#level_id' ),
			$task_id = $( '#task_id' ),
		$options = $task_id.find( 'option' );

		$level_id.on( 'change', function() {
			$task_id.html( $options.filter( '[title="' + this.value + '"]' ) );
		} ).trigger( 'change' );
	});	

</script>
<?php
}
if(($Segment1 == "project") && ($Segment2 == "proj_project_level_task") && ($Segment3 == "insertform"))
{
?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		var $project_id = $( '#project_id' ),
			$level_id = $( '#level_id' ),
		$options = $level_id.find( 'option' );

		$project_id.on( 'change', function() {
			$level_id.html( $options.filter( '[title="' + this.value + '"]' ) );
		} ).trigger( 'change' );
	});	
</script>
<?php
}
if(($Segment1 == "account") && ($Segment2 == "fin_treeaccount") && ($Segment3 == "tree") || 
($Segment1 == "project") && ($Segment2 == "proj_project") && ($Segment3 == "proj_project_devices_tree"))
{
?>
<script src="<?php echo base_url();?>assets/plugins/custom/jstree/jstree.bundle.js" type="text/javascript"></script>
<!--begin::Page Scripts(used by this page) -->
<script src="<?php echo base_url();?>assets/js/pages/features/miscellaneous/treeview.js" type="text/javascript"></script>
<?php
}
if(($Segment1 == "permissions") && ($Segment2 == "profile") && ($Segment3 == "serv_message") || 
($Segment1 == "permissions") && ($Segment2 == "profile") && ($Segment3 == "serv_message_sent"))
{
?>
<script type="text/javascript">
	$('#exampleModalCenter').on('show.bs.modal', function (e) {
		// get information to update quickly to modal view as loading begins
		var opener=e.relatedTarget;//this holds the element who called the modal
		//we get details from attributes
		var to_user=$(opener).attr('data-id');
		//set what we got to our form
		$('#kt_inbox_reply_form').find('[name="to_user[]"]').val(to_user);
	});
</script>

<script type="text/javascript">
function printSelection(node){
	var content=node.innerHTML;
	var pwin=window.open('','print_content','width=600,height=600');

	pwin.document.open();
	pwin.document.write('<html><head><title></title>');
	pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
	pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
	pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
	pwin.document.write('<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
	pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
	pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
	pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
	pwin.document.write('</head>');
	pwin.document.write('<body onload="window.print()" style="direction: rtl; text-align: right">');
	pwin.document.write(content);
	pwin.document.write('<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></' + 'script>');
	pwin.document.write('<script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></' + 'script>');
	pwin.document.write('</body></html>');
	pwin.document.close();

	pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
	pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
	pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

	setTimeout(function(){pwin.close();},1000);

}
</script>
<?php
}
?>

<div style="direction: rtl; text-align:right; margin-right:auto; margin-left:auto">
<?php
	$this->load->view('alert');
?>
</div>

<?php
if(($Segment1 == "account") && ($Segment2 == "fin_liquidity_risk") || ($Segment1 == "") || ($Segment1 == "erp"))
{
?>
<script type="text/javascript">
Highcharts.chart('container', {
    data: {
        table: 'datatable'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '<?php echo lang('fin_liquidity_risk_Pointer');?>'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Units'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});
</script>
<?php
}
if(($Segment1 == "manufacturing") && ($Segment2 == "manu_components") && ($Segment3 == ""))
{
?>
<script type="text/javascript">
	$(document).ready(function() {

		$('.view_detail').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value

			$.ajax({
				url : "<?php echo base_url(); ?>manufacturing/manu_components/view_main",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					$('#id').html(response.id);
					$('#thedate').html(response.thedate);
					$('#over_cost').html(response.over_cost);
					$('#notes').html(response.notes);
					$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});


			$.ajax({
				url: '<?php echo base_url(); ?>manufacturing/manu_components/view_details',
				data:{id : id},
				type: 'get',
				dataType: 'JSON',
				success: function(response){
					var len = response.length;
					$("#ModalTable").find("tbody").empty();
					for(var i=0; i<len; i++){
						var id = response[i].id;
						var title = response[i].title;
						var title_en = response[i].title_en;
						var quantity = response[i].quantity;
						<?php
						if ($this->session->userdata('lang') == "ar")
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + title + "</td>" +
							"<td align='center'>" + quantity + "</td>" +
							"</tr>";
						<?php
						}
						else
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + title_en + "</td>" +
							"<td align='center'>" + quantity + "</td>" +
							"</tr>";
						<?php
						}
						?>
						$("#ModalTable tbody").append(tr_str);
					}

				}
			});

		});

		
	});

</script>

<script type="text/javascript">
function printSelection(node){
	var content=node.innerHTML;
	var pwin=window.open('','print_content','width=600,height=600');

	pwin.document.open();
	pwin.document.write('<html><head><title></title>');
	pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
	pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
	pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
	pwin.document.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">');
	pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
	pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
	pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
	pwin.document.write('</head>');
	pwin.document.write('<body onload="window.print()" style="direction: <?php echo $this->session->userdata('Direction');?>; text-align: <?php echo $this->session->userdata('Alignment');?>">');
	pwin.document.write(content);
	pwin.document.write('<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></' + 'script>');
	pwin.document.write('<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></' + 'script>');
	pwin.document.write('<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></' + 'script>');
	pwin.document.write('</body></html>');
	pwin.document.close();

	pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
	pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
	pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

	setTimeout(function(){pwin.close();},1000);

}
</script>
<?php
}
if(($Segment1 == "manufacturing") && ($Segment2 == "manu_manufacturing_orders") && ($Segment3 == ""))
{
?>
<script type="text/javascript">
	$(document).ready(function() {

		$('.view_detail').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value

			$.ajax({
				url : "<?php echo base_url(); ?>manufacturing/manu_manufacturing_orders/view_main",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					$('#id').html(response.id);
					$('#thedate').html(response.thedate);
					$('#over_cost').html(response.over_cost);
					$('#notes').html(response.notes);
					$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});


			$.ajax({
				url: '<?php echo base_url(); ?>manufacturing/manu_manufacturing_orders/view_details',
				data:{id : id},
				type: 'get',
				dataType: 'JSON',
				success: function(response){
					var len = response.length;
					$("#ModalTable").find("tbody").empty();
					for(var i=0; i<len; i++){
						var id = response[i].id;
						var title = response[i].title;
						var title_en = response[i].title_en;
						var quantity = response[i].quantity;
						<?php
						if ($this->session->userdata('lang') == "ar")
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + title + "</td>" +
							"<td align='center'>" + quantity + "</td>" +
							"</tr>";
						<?php
						}
						else
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + title_en + "</td>" +
							"<td align='center'>" + quantity + "</td>" +
							"</tr>";
						<?php
						}
						?>
						$("#ModalTable tbody").append(tr_str);
					}

				}
			});

		});

		
	});

</script>

<script type="text/javascript">
function printSelection(node){
	var content=node.innerHTML;
	var pwin=window.open('','print_content','width=600,height=600');

	pwin.document.open();
	pwin.document.write('<html><head><title></title>');
	pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
	pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
	pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
	pwin.document.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">');
	pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
	pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
	pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
	pwin.document.write('</head>');
	pwin.document.write('<body onload="window.print()" style="direction: <?php echo $this->session->userdata('Direction');?>; text-align: <?php echo $this->session->userdata('Alignment');?>">');
	pwin.document.write(content);
	pwin.document.write('<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></' + 'script>');
	pwin.document.write('<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></' + 'script>');
	pwin.document.write('<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></' + 'script>');
	pwin.document.write('</body></html>');
	pwin.document.close();

	pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
	pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
	pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

	setTimeout(function(){pwin.close();},1000);

}
</script>
<?php
}
?>

<?php
if(($Segment1 == "maintenance") && ($Segment2 == "data_request") && ($Segment3 == "insertform"))
{
?>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('#kt_sweetalert_demo_3_3').click(function(e) {
			swal.fire("<?php echo lang('GoodJob');?>", "<?php echo lang('GoodJobDetails');?>", "success");
		});
	});

	function GetArabicName() {
		phone = "phone";
		phone = document.getElementById(phone).value;
		theUrl = decodeURIComponent("<?php echo base_url().$Segment1."/".$Segment2;?>/GetArabicName/" + phone);
		//window.location.href = theUrl;
		$.ajax({url: theUrl, success: function(result){
			//$("#div1").html(result);
			ar_title = "ar_title";
			document.getElementById(ar_title).value = result;
		}});
	}

	function GetEnglishName() {
		phone = "phone";
		phone = document.getElementById(phone).value;
		theUrl = decodeURIComponent("<?php echo base_url().$Segment1."/".$Segment2;?>/GetEnglishName/" + phone);
		//window.location.href = theUrl;
		$.ajax({url: theUrl, success: function(result){
			//$("#div1").html(result);
			en_title = "en_title";
			document.getElementById(en_title).value = result;
		}});
	}

	function GetMobile() {
		phone = "phone";
		phone = document.getElementById(phone).value;
		theUrl = decodeURIComponent("<?php echo base_url().$Segment1."/".$Segment2;?>/GetMobile/" + phone);
		//window.location.href = theUrl;
		$.ajax({url: theUrl, success: function(result){
			//$("#div1").html(result);
			mobile1 = "mobile1";
			document.getElementById(mobile1).value = result;
		}});
	}

	function GetWhatsApp() {
		phone = "phone";
		phone = document.getElementById(phone).value;
		theUrl = decodeURIComponent("<?php echo base_url().$Segment1."/".$Segment2;?>/GetWhatsApp/" + phone);
		//window.location.href = theUrl;
		$.ajax({url: theUrl, success: function(result){
			//$("#div1").html(result);
			mobile2 = "mobile2";
			document.getElementById(mobile2).value = result;
		}});
	}

	function GetAddress() {
		phone = "phone";
		phone = document.getElementById(phone).value;
		theUrl = decodeURIComponent("<?php echo base_url().$Segment1."/".$Segment2;?>/GetAddress/" + phone);
		//window.location.href = theUrl;
		$.ajax({url: theUrl, success: function(result){
			//$("#div1").html(result);
			address = "address";
			document.getElementById(address).value = result;
		}});
	}

	function GetEmail() {
		phone = "phone";
		phone = document.getElementById(phone).value;
		theUrl = decodeURIComponent("<?php echo base_url().$Segment1."/".$Segment2;?>/GetEmail/" + phone);
		//window.location.href = theUrl;
		$.ajax({url: theUrl, success: function(result){
			//$("#div1").html(result);
			email = "email";
			document.getElementById(email).value = result;
		}});
	}
</script>
<?php
}
?>

<?php
if(($Segment1 == "maintenance") && ($Segment2 == "data_request") && ($Segment3 == "") || 
($Segment1 == "maintenance") && ($Segment2 == "data_request") && ($Segment3 == "filter"))
{
?>
<script type="text/javascript">
	$(document).ready(function() {

		$('.view_detail').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value
			
			$.ajax({
				url : "<?php echo base_url(); ?>maintenance/data_request/view_main",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					$('#id').html(response.id);
					$('#thedate').html(response.thedate);
					$('#details').html(response.details);
					<?php
					if ($this->session->userdata('lang') == "ar")
					{
					?>
					$('#customer_id').html(response.customer_ar_title);
					$('#status_id').html(response.status_ar_title);
					$('#area_field_id').html(response.area_ar_title);
					<?php
					}
					else
					{
					?>
					$('#customer_id').html(response.customer_en_title);
					$('#status_id').html(response.status_en_title);
					$('#area_field_id').html(response.area_en_title);
					<?php
					}
					?>
					$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});


			$.ajax({
				url: '<?php echo base_url(); ?>maintenance/data_request/view_details',
				data:{id : id},
				type: 'get',
				dataType: 'JSON',
				success: function(response){
					var len = response.length;
					$("#ModalTable").find("tbody").empty();
					for(var i=0; i<len; i++){
						var id = response[i].id;
						var ar_title = response[i].ar_title;
						var en_title = response[i].en_title;
						var r_quantity = response[i].r_quantity;
						var unit_price = response[i].unit_price;
						var subtotal = r_quantity * unit_price;
						<?php
						if ($this->session->userdata('lang') == "ar")
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + ar_title + "</td>" +
							"<td align='center'>" + r_quantity + "</td>" +
							"<td align='center'>" + unit_price + "</td>" +
							"<td align='center'>" + subtotal + "</td>" +
							"</tr>";
						<?php
						}
						else
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + en_title + "</td>" +
							"<td align='center'>" + r_quantity + "</td>" +
							"<td align='center'>" + unitprice + "</td>" +
							"<td align='center'>" + subtotal + "</td>" +
							"</tr>";
						<?php
						}
						?>
						$("#ModalTable tbody").append(tr_str);
					}

				}
			});

		});
	});
</script>
<?php
}
?>

<?php
if(($Segment1 == "maintenance") && ($Segment2 == "data_invoice") && ($Segment3 == "") || 
($Segment1 == "maintenance") && ($Segment2 == "data_invoice") && ($Segment3 == "filter"))
{
?>
<script type="text/javascript">
	$(document).ready(function() {

		$('.view_detail').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value
			
			$.ajax({
				url : "<?php echo base_url(); ?>maintenance/data_invoice/view_main",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					$('#id').html(response.id);
					$('#thedate').html(response.thedate);
					$('#details').html(response.details);
					<?php
					if ($this->session->userdata('lang') == "ar")
					{
					?>
					$('#customer_name').html(response.customer_ar_title);
					$('#status_id').html(response.status_ar_title);
					$('#area_field_id').html(response.area_ar_title);
					$('#teamwork_name').html(response.teamwork_ar_title);
					<?php
					}
					else
					{
					?>
					$('#customer_name').html(response.customer_en_title);
					$('#status_id').html(response.status_en_title);
					$('#area_field_id').html(response.area_en_title);
					$('#teamwork_name').html(response.teamwork_en_title);
					<?php
					}
					?>
					$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});


			$.ajax({
				url: '<?php echo base_url(); ?>maintenance/data_invoice/view_details',
				data:{id : id},
				type: 'get',
				dataType: 'JSON',
				success: function(response){
					var len = response.length;
					$("#ModalTable").find("tbody").empty();
					for(var i=0; i<len; i++){
						var id = response[i].id;
						var ar_title = response[i].ar_title;
						var en_title = response[i].en_title;
						var invoice_quantity = response[i].invoice_quantity;
						var unit_price = response[i].unit_price;
						var subtotal = invoice_quantity * unit_price;
						<?php
						if ($this->session->userdata('lang') == "ar")
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + ar_title + "</td>" +
							"<td align='center'>" + invoice_quantity + "</td>" +
							"<td align='center'>" + unit_price + "</td>" +
							"<td align='center'>" + subtotal + "</td>" +
							"</tr>";
						<?php
						}
						else
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + en_title + "</td>" +
							"<td align='center'>" + invoice_quantity + "</td>" +
							"<td align='center'>" + unitprice + "</td>" +
							"<td align='center'>" + subtotal + "</td>" +
							"</tr>";
						<?php
						}
						?>
						$("#ModalTable tbody").append(tr_str);
					}

				}
			});

		});
	});
</script>
<?php
}
?>

<?php
if(($Segment1 == "cars") && ($Segment2 == "cars_orders") && ($Segment3 == "") || 
($Segment1 == "cars") && ($Segment2 == "cars_orders") && ($Segment3 == "filter"))
{
?>
<script type="text/javascript">
	$(document).ready(function() {

		$('.view_detail').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value
			
			$.ajax({
				url : "<?php echo base_url(); ?>cars/cars_orders/view_main",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					$('#id').html(response.id);
					$('#thedate').html(response.thedate);
					$('#details').html(response.details);
					$('#closed_date').html(response.closed_date);
					$('#closed_time').html(response.closed_time);
					<?php
					if ($this->session->userdata('lang') == "ar")
					{
					?>
					$('#customer_name').html(response.customer_ar_title);
					$('#status_field_id').html(response.status_ar_title);
					$('#area_field_id').html(response.area_ar_title);
					$('#teamwork_name').html(response.teamwork_ar_title);
					<?php
					}
					else
					{
					?>
					$('#customer_name').html(response.customer_en_title);
					$('#status_field_id').html(response.status_en_title);
					$('#area_field_id').html(response.area_en_title);
					$('#teamwork_name').html(response.teamwork_en_title);
					<?php
					}
					?>
					$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});


			$.ajax({
				url: '<?php echo base_url(); ?>cars/cars_orders/view_details',
				data:{id : id},
				type: 'get',
				dataType: 'JSON',
				success: function(response){
					var len = response.length;
					$("#ModalTable").find("tbody").empty();
					for(var i=0; i<len; i++){
						var id = response[i].id;
						var ar_title = response[i].ar_title;
						var en_title = response[i].en_title;
						var quantity = response[i].a_qunatity;
						var unit_price = response[i].unit_price;
						var subtotal = quantity * unit_price;
						<?php
						if ($this->session->userdata('lang') == "ar")
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + ar_title + "</td>" +
							"<td align='center'>" + quantity + "</td>" +
							"<td align='center'>" + unit_price + "</td>" +
							"<td align='center'>" + subtotal + "</td>" +
							"</tr>";
						<?php
						}
						else
						{
						?>
						var tr_str = "<tr>" +
							"<td align='right'>" + en_title + "</td>" +
							"<td align='center'>" + quantity + "</td>" +
							"<td align='center'>" + unitprice + "</td>" +
							"<td align='center'>" + subtotal + "</td>" +
							"</tr>";
						<?php
						}
						?>
						$("#ModalTable tbody").append(tr_str);
					}

				}
			});

		});
	});
</script>
<?php
}
?>

<?php
if(($Segment1 == "support") && ($Segment2 == "whatsapp_chat") && ($Segment3 == ""))
{
?>
<script type="text/javascript">
$(function(){
    $(".heading-compose").click(function() {
      $(".side-two").css({
        "left": "0"
      });
    });

    $(".newMessage-back").click(function() {
      $(".side-two").css({
        "left": "-100%"
      });
    });
}) 
</script>
<?php
}
?>

<?php
if(($Segment1 == "account") && ($Segment2 == "fin_treeaccount") && ($Segment3 == "view_account") || 
($Segment1 == "account") && ($Segment2 == "reports"))
{
?>
<script> 
	function printDiv() { 
		var divContents = document.getElementById("DivForPrint").innerHTML; 
		var a = window.open('', '', 'height=500, width=800'); 
		<?php
		if ($this->session->userdata('lang') == "ar") {
		?>
		a.document.write('<html dir="rtl">'); 
		<?php
		}
		else
		{
		?>
		a.document.write('<html dir="rtl">'); 
		<?php
		}
		?>
		
		a.document.write('<style type="text/css">'); 
		a.document.write('table th, table td {');
		a.document.write('border:1px solid #4a4a4a;');
		a.document.write('padding:0.2em;');
		a.document.write('}'); 
		a.document.write('</style>'); 

		a.document.write('<body>'); 
		a.document.write(divContents); 
		a.document.write('</body></html>'); 
		a.document.close(); 
		a.print(); 
	} 
</script>
<?php
}
?>

<?php
if(($Segment1 == "mydata") && ($Segment2== "repair"))
{
?>
<script> 
$(".progress-bar").animate({
    width: "100%"
}, 9500, function() {
	alert('<?php echo $this->lang->line('Databse_Repair_Message');?>');
  });
</script>
<?php
}
?>

<?php
if(($Segment1 == "mydata") && ($Segment2== "optimize"))
{
?>
<script> 
$(".progress-bar").animate({
    width: "100%"
}, 9500, function() {
	alert('<?php echo $this->lang->line('Databse_Optimize_Message');?>');
  });
</script>
<?php
}
?>

<?php
if(($Segment1 == "mydata") && ($Segment2== "deleterows"))
{
?>
<script> 
$(".progress-bar").animate({
    width: "100%"
}, 9500, function() {
	alert('<?php echo $this->lang->line('DataDelete_Message');?>');
  });
</script>
<?php
}
?>

<?php
if(($Segment1 == "mydata") && ($Segment2== "backup"))
{
?>
<script> 
$(".progress-bar").animate({
    width: "100%"
}, 9500, function() {
	alert('<?php echo $this->lang->line('DatabaseBackeup_Message');?>');
  });
</script>
<?php
}
?>

<?php
if(($Segment1 == "sales") && ($Segment2 == "sale_customer") && ($Segment3 == ""))
{
?>
<script type="text/javascript">
	$(document).ready(function() {

		$('.view_detail').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value
			
			$.ajax({
				url : "<?php echo base_url(); ?>sales/sale_customer/view_data",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					<?php
					if ($this->session->userdata('lang') == "ar")
					{
					?>
					$('#customer_name').html(response.ar_title);
					<?php
					}
					else
					{
					?>
					$('#customer_name').html(response.en_title);
					<?php
					}
					?>
					$('#id').html(response.id);
					$('#phone').html(response.phone);
					$('#mobile1').html(response.mobile1);
					$('#mobile2').html(response.mobile2);
					$('#address').html(response.address);
					$('#email').html(response.email);
					$('#commercial_register').html(response.commercial_register);
					$('#tax_card').html(response.tax_card);
					$('#person').html(response.person);
					$('#person_phone').html(response.person_phone);
					$('#credit_limit').html(response.credit_limit);

					$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});

		});
	});
</script>

<script type="text/javascript">
	function printSelection(node){
		var content=node.innerHTML;
		var pwin=window.open('','print_content','width=600,height=600');

		pwin.document.open();
		pwin.document.write('<html><head><title></title>');
		pwin.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
		pwin.document.write('<meta content="width=device-width, initial-scale=1" name="viewport" />');
		pwin.document.write('<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />');
		pwin.document.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">');
		pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom<?php echo $this->session->userdata('CSSFile');?>.min.css" rel="stylesheet" type="text/css" />');
		pwin.document.write('<link href="<?php echo base_url();?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />');
		pwin.document.write('<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/layouts/layout/css/arabic_font.css" />');
		pwin.document.write('</head>');
		pwin.document.write('<body onload="window.print()" style="direction: <?php echo $this->session->userdata('Direction');?>; text-align: <?php echo $this->session->userdata('Alignment');?>">');
		pwin.document.write(content);
		pwin.document.write('<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></' + 'script>');
		pwin.document.write('<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></' + 'script>');
		pwin.document.write('<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></' + 'script>');
		pwin.document.write('</body></html>');
		pwin.document.close();
	
		pwin.document.getElementById("SaveButtons").style.visibility = "hidden";
		pwin.document.getElementById("FooterButtons").style.visibility = "hidden";
		pwin.document.getElementById("ImageDiv").style.visibility = "hidden";

		setTimeout(function(){pwin.close();},1000);

	}
</script>
<?php } ?>


<?php
if(($Segment1 == "sales") && ($Segment2 == "sale_customer") && ($Segment3 == "sale_customer_email"))
{
?>
<script type="text/javascript">
	$(document).ready(function() {

		$('.view_detail').click(function(){
			
			var id = $(this).attr('relid'); //get the attribute value
			
			$.ajax({
				url : "<?php echo base_url(); ?>sales/sale_customer/view_email",
				data:{id : id},
				method:'GET',
				dataType:'json',
				success:function(response) {
					<?php
					if ($this->session->userdata('lang') == "ar")
					{
					?>
					$('#customer_name').html(response.ar_title);
					<?php
					}
					else
					{
					?>
					$('#customer_name').html(response.en_title);
					<?php
					}
					?>
					$('#id').html(response.id);
					$('#phone').html(response.phone);
					$('#thedate').html(response.thedate);
					$('#thetime').html(response.thetime);
					$('#subject').html(response.subject);
					$('#message').html(response.message);

					$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
					records = JSON.parse(data);
				}
			});

		});
	});
</script>
<?php } ?>