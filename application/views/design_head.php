<base href="">
<meta charset="utf-8" />
<title><?php echo $this->session->userdata('package_title');?> | <?php echo $this->lang->line('Dashboard');?></title>
<meta name="description" content="LBI-ERP System | <?php echo $this->lang->line('Dashboard');?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<link rel="canonical" href="https://www.lbi-egypt.com/" />
<!--begin::Fonts-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
<!--end::Fonts-->
<!--begin::Page Vendors Styles(used by this page)-->
<link href="<?php echo base_url();?>/assets/plugins/custom/fullcalendar/fullcalendar.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>/assets/plugins/custom/datatables/datatables.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles-->
<!--begin::Global Theme Styles(used by all pages)-->
<link href="<?php echo base_url();?>/assets/plugins/global/plugins.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>/assets/plugins/custom/prismjs/prismjs.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>/assets/css/style.bundle<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles-->
<!--begin::Layout Themes(used by all pages)-->
<link href="<?php echo base_url();?>/assets/css/themes/layout/header/base/light<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>/assets/css/themes/layout/header/menu/light<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>/assets/css/themes/layout/brand/dark<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>/assets/css/themes/layout/aside/dark<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
<!--end::Layout Themes-->
<link href="<?php echo base_url();?>/assets/css/main<?php echo $this->session->userdata('CSSFile');?>.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?php echo base_url();?>/assets/media/logos/favicon.ico" />
<?php
$Segment1 = $this->uri->segment(1);
$Segment2 = $this->uri->segment(2);
$Segment3 = $this->uri->segment(3);
$Segment4 = $this->uri->segment(4);
$Segment5 = $this->uri->segment(5);

if ($this->session->userdata('lang') == "ar"){
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/arabic_font.css" />
<?php
}
?>
<?php
$assets = base_url().'application/views/assets/';
?>

<?php
$Align = $this->session->userdata('Alignment');
$AnotherAlign = "";
$AnotherDirection = "";
if($Align == "right")
{
    $AnotherAlign = "left";
}
else
{
    $AnotherAlign = "right";
}
?>

<?php
if(($Segment1 == "account") && ($Segment2 == "fin_liquidity_risk") || ($Segment1 == "") || ($Segment1 == "erp") || 
($Segment1 == "cars") || ($Segment1 == "car_rental"))
{
?>
<style type="text/css">
#container {
    height: 300px;
}
#Purchase_container {
    height: 300px;
}
#Service_container {
    height: 300px;
}
#Treasury_container {
    height: 300px;
}
#Bank_container {
    height: 300px;
}
#Custody_container {
    height: 300px;
}
.highcharts-figure, .highcharts-data-table table {
    min-width: 100%;
    max-width: 800px;
    margin: 1em auto;
}

#datatable {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #EBEBEB;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
}
#datatable caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
#datatable th {
	font-weight: 600;
    padding: 0.5em;
}
#datatable td, #datatable th, #datatable caption {
    padding: 0.5em;
}
#datatable thead tr, #datatable tr:nth-child(even) {
    background: #f8f8f8;
}
#datatable tr:hover {
    background: #f1f7ff;
}
</style>

<script src="<?php echo base_url();?>/highcharts/highcharts.js"></script>
<script src="<?php echo base_url();?>/highcharts/modules/data.js"></script>
<script src="<?php echo base_url();?>/highcharts/modules/exporting.js"></script>
<script src="<?php echo base_url();?>/highcharts/modules/accessibility.js"></script>
<script src="<?php echo base_url();?>/highcharts/themes/sunset.js"></script>
<?php
}
?>

<?php
if(($Segment1 == "mydata"))
{
?>
<style type="text/css">
.progress.active .progress-bar {
    -webkit-transition: none !important;
    transition: none !important;
}
</style>
<?php
}
?>